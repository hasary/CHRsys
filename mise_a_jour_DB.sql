ALTER PROCEDURE [dbo].[CoSituationComerciale]
@UID int
AS
BEGIN

SELECT
[Date], 
Nom, 
Description,
MontantC,
MontantD, 
val1  as 'DernierVersement',
val3  as 'DateDernierVersement', 

val2 as 'DernierChargement',
val4 as 'DateDernierChargement',
Etat

From PrintTemp where UID =@UID 
ORDER BY abs(MontantD + MontantC) DESC
END