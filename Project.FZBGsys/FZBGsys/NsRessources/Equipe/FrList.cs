﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace FZBGsys.NsRessources.NsEquipe
{
    public partial class FrList : Form
    {
        ModelEntities db = new ModelEntities();

        public FrList()
        {
            InitializeComponent();
            InitialiseData();
           
        }

        private void InitialiseData()
        {
            dgv1.DataSource = db.Equipes.Where(g => g.ID > 0).OrderBy(u => u.Nom).Select(g => new { ID = g.ID, Equipe = g.Nom, Membres = g.Employers.Count, Section = (g.Section!=null)?g.Section.Nom:null, Responsable = (g.Employer!=null) ? g.Employer.Nom : null }).ToList();
            dgv1.Columns[0].Visible = false;
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
             new FZBGsys.NsRessources.NsEquipe.FrCreate().ShowDialog();
            InitialiseData();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            var selectedID = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
            var selected = db.Equipes.Single(g => g.ID == selectedID);

            db.Refresh(RefreshMode.StoreWins, selected);
            new FZBGsys.NsRessources.NsEquipe.FrEdit(selected).ShowDialog();
            InitialiseData();

        }

        private void buttonDel_Click(object sender, EventArgs e)
        {
            var selectedID = int.Parse(dgv1.SelectedRows[0].Cells[0].Value.ToString());
            var selected = db.Equipes.Single(g => g.ID == selectedID);

            if (this.ConfirmWarning("Supprimer l'Equipe '" + selected.Nom + "' ?"))
            {
                db.DeleteObject(selected);
                db.SaveChanges();
                InitialiseData();
            }
        }

        private void AdminGroupe_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
    }
}
