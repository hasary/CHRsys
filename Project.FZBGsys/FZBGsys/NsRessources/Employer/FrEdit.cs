﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using MarbreBLIDA;
namespace FZBGsys.NsRessources.NsEmployer
{
    public partial class Edit : Form
    {
        public Employer Model { get; set; }
        private ModelEntities db = new ModelEntities();
        public Edit(Employer Model)
        {
            this.Model = Model;
            InitializeComponent();
            InitializeData();
        }
        private void InitializeData()
        {
            var listActivite = db.Activites.OrderBy(u => u.Description).ToList();
            var listEquipe = db.Equipes.OrderBy(u => u.Nom).ToList();
            var listSection = db.Sections.OrderBy(u => u.Nom).ToList();

            cbActivite.DataSource = listActivite;
            cbActivite.SelectedItem = listActivite.SingleOrDefault(l => l.Code == Model.ActiviteID);
            cbActivite.DisplayMember = "Description";
            cbActivite.ValueMember = "Code";
            cbActivite.DropDownStyle = ComboBoxStyle.DropDownList;
/*
            cbEquipe.DataSource = listEquipe;
            cbEquipe.SelectedItem = listEquipe.SingleOrDefault(l => l.ID == Model.EquipeID);
            cbEquipe.DisplayMember = "Nom";
            cbEquipe.ValueMember = "ID";
            cbEquipe.DropDownStyle = ComboBoxStyle.DropDownList;
*/
            cbSection.DataSource = listSection;
            cbSection.SelectedItem = listSection.SingleOrDefault(l => l.ID == Model.SectionID);
            cbSection.DisplayMember = "Nom";
            cbSection.ValueMember = "ID";
            cbSection.DropDownStyle = ComboBoxStyle.DropDownList;

            txtMatricule.Text = Model.Matricule;
            txtNom.Text = Model.Nom;

        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNom.Text.Length > 69 || txtNom.Text.Length < 1)
            {
                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 70 caractères)";
            }

            

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var employer = db.Employers.Single(em => em.Matricule == Model.Matricule);

            if (isValidAll())
            {
                employer.Nom = txtNom.Text;
                employer.SectionID = ((Section)cbSection.SelectedItem).ID;
               // employer.EquipeID = ((Equipe)cbEquipe.SelectedItem).ID;
                employer.ActiviteID = ((Activite)cbActivite.SelectedItem).Code;

                db.SaveChanges();
                Dispose();

            }

        }

        private void AdminEmployerEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }


    }
}
