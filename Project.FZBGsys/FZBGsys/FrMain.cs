
﻿using FZBGsys.NsReports;
using FZBGsys.NsReports.Comercial;
using FZBGsys.NsReports.Production;
using FZBGsys.NsStock.StockProduitFini;
using FZBGsys.NsTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;


namespace FZBGsys
{
    public partial class FrMain : Form
    {

        public ApplicationUtilisateur CurrentUser { get; set; }
        private List<string> ActionsAutorisees = null;

        public int UserID
        {
            set
            {

                InitializeUser(value);
                Tools.CurrentUserID = value;

            }

        }

        private void InitializeUser(int userID)
        {

            using (ModelEntities db = new ModelEntities())
            {
                this.CurrentUser = db.ApplicationUtilisateurs.SingleOrDefault(u => u.ID == userID);

                if (CurrentUser == null) // user annonyme no connexion
                {
                    groupBoxInfos.Visible = false;
                    return;
                }
                Tools.CurrentUser = CurrentUser;
                Tools.isBoss = (bool?)CurrentUser.ApplicationGroupe.IsBoss;

                labelUserName.Text = CurrentUser.Employer.Nom;
                labeluserRole.Text = (CurrentUser.Employer.Activite != null) ? CurrentUser.Employer.Activite.Description : "";
                labelUserSection.Text = CurrentUser.Employer.Section.Description;
                groupBoxInfos.Visible = true;


                /*
                                if (CurrentUser.GroupeID == 1)
                                {  // Admin Got All Actions
                                    EnableAllActions();
                                    return;

                                }
                */

                this.ActionsAutorisees =
            CurrentUser.ApplicationGroupe.ApplicationAutorisations.Select(a => a.ApplicationAction.Nom).ToList();

                this.ActionsAutorisees =
                 CurrentUser.ApplicationGroupe.ApplicationAutorisations.Where(a => a.Autorisation).Select(a => a.ApplicationAction.Nom).ToList();

                // Autoriser le Groupe 0 (annonyme)
                /*  this.ActionsAutorisees.AddRange(
                     db.ApplicationGroupes.Single(g => g.ID == 0).ApplicationAutorisations.Where(a => a.Autorisation == true).Select(a => a.ApplicationAction.Nom).ToList()
                     );

  */


                ActionsAutorisees = this.ActionsAutorisees.Distinct().ToList();




                if (userID != 1 && userID != 30 && userID != 34)
                {
                    var ActionsRemove = db.ApplicationActions.Where(p => p.LinkedWindowsUsserName != null &&
                        p.LinkedWindowsUsserName != Environment.UserDomainName + @"\" + Environment.MachineName).ToList();
                    foreach (var item in ActionsRemove)
                    {
                        //Tools.ShowInformation(item.LinkedWindowsUsserName + " " + Environment.UserDomainName + @"\" + Environment.MachineName);
                        this.ActionsAutorisees.Remove(item.Nom);
                    }
                }

                #region Refuse
                /*
                                this.ActionsRefusees =
                                 CurrentUser.ApplicationGroupe.ApplicationAutorisations.Where(a => a.Autorisation == false).Select(a => a.ApplicationAction.Nom).ToList();

                                // Refuser le Groupe 0 (annonyme)
                                this.ActionsRefusees.AddRange(
                                    db.ApplicationGroupes.Single(g => g.ID == 0).ApplicationAutorisations.Where(a => a.Autorisation == false).Select(a => a.ApplicationAction.Nom).ToList()
                                   );
                                */

                #endregion

                Tools.ClearPrintTemp();
            }



            foreach (ToolStripMenuItem item in menuMain.Items)
            {
                processMenuItems(item);
            }

            menuMain.Enabled = true;

        }

        private void processMenuItems(ToolStripMenuItem item)
        {
            if (item.Name == "Fichier" || item.Name == "Aide")
            {

                item.Visible = true;
                item.Enabled = true;
                return;
            }

            if (ActionsAutorisees == null)
            {

                return;
            }

            if (ActionsAutorisees.Contains(item.Name))
            {
                item.Visible = true;
                item.Enabled = true;
                foreach (ToolStripItem subitem in item.DropDownItems)
                {
                    if (subitem is ToolStripMenuItem)
                    {
                        //processMenuItems((ToolStripMenuItem)subitem);
                        if (ActionsAutorisees.Contains(subitem.Name))
                        {
                            subitem.Visible = true;
                            subitem.Enabled = true;
                        }
                        else
                        {
                            subitem.Visible = false;
                            subitem.Enabled = false;

                        }
                    }
                }

            }
            else
            {
                item.Visible = false;
                item.Enabled = false;

            }
            /*if (CurrentUser.ID != 20 && CurrentUser.ID != 1 && Tools.isBoss == false)
                CaisseOpClient.Visible = false;*/


        }

        private static void TestConnexion()
        {
            try
            {
                var db2 = new ModelEntities();
                var test = db2.Activites.Take(2).ToList();
                test = null;
                db2 = null;



            }
            catch (Exception e)
            {
                Tools.ShowError("Erreur de connexion:\n" + e.Message);
                Application.Exit();
            }
        }

        private void TestExecution()
        {

            //  Process Proc_EnCours = Process.GetCurrentProcess();
            //  Process[] Les_Proc = Process.GetProcesses();

        }



        public FrMain()
        {


            TestExecution();
            TestConnexion();
            InitializeComponent();

            Tools.UdatedbAutorisations(this.menuMain);

            string sVersion = "";
            if (ApplicationDeployment.IsNetworkDeployed) //Car ne fonctionner pas en mode debug
            {
                ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
                sVersion = ad.CurrentVersion.ToString();
            }
            this.Text += " " + sVersion;



            groupBoxInfos.BringToFront();

            InitialiseAllActions(); // disable All

            this.UserID = 0;

            var userName = Environment.UserDomainName + '_' + Environment.UserName;
            if (true)
            {
                using (ModelEntities db = new ModelEntities())
                {
                    var usr = db.ApplicationUtilisateurs.SingleOrDefault(u => u.WindowsUserName == userName);
                    if (usr != null)
                    {

                        UserID = usr.ID;
                    }

                }

            }



        }


        #region Enable / Disable All

        private void InitialiseAllActions()
        {
            using (ModelEntities db = new ModelEntities())
            {
                List<ApplicationAction> itemsTexts = null; ;
                try
                {
                    itemsTexts = db.ApplicationActions.ToList();
                }
                catch (Exception e)
                {
                    string ErrorMessage = "";
                    while (e != null)
                    {
                        ErrorMessage += e.Message + "\n";
                        e = e.InnerException;
                    }
                    Tools.ShowError("Erreur de connexion:\n " + ErrorMessage);
                    Application.Exit();

                }

                foreach (var item in menuMain.Items)
                {
                    if (item is ToolStripMenuItem)
                        InitActions((ToolStripMenuItem)item, itemsTexts);
                }
            }
        }

        private void EnableAllActions()
        {
            foreach (var item in menuMain.Items)
            {
                if (item is ToolStripMenuItem)
                    EnableActions((ToolStripMenuItem)item);
            }
        }

        private void InitActions(ToolStripMenuItem item, List<ApplicationAction> list)
        {
            if (item.Name == "Fichier" || item.Name == "Aide")
                return;
            item.Enabled = false;
            item.Visible = false;

            var itemcorr = list.FirstOrDefault(l => l.Nom == item.Name);

            if (itemcorr != null)
            {
                item.Text = itemcorr.Text;
                if (itemcorr.ParentActionID != 0)
                {
                    item.Text += "";
                }

            }
            else
            {

                //Tools.ShowError("Erreur de chargement de l'interface,\nElement de menu Manquant: " + item.Name);
            }
            foreach (ToolStripItem subitem in item.DropDownItems)
            {
                if (subitem is ToolStripMenuItem)
                {
                    subitem.Enabled = false;
                    subitem.Visible = false;
                    //  InitActions((ToolStripMenuItem)subitem, list);
                }
            }

        }

        private void EnableActions(ToolStripMenuItem item)
        {
            if (item.Name == "Fichier")
                return;
            item.Enabled = true;
            item.Visible = true;
            foreach (ToolStripItem subitem in item.DropDownItems)
            {
                if (subitem is ToolStripMenuItem)
                {
                    item.Enabled = true;
                    item.Visible = true;
                    EnableActions((ToolStripMenuItem)subitem);
                }
            }

        }
        #endregion




        #region menu click
        private void matierePremiereToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrArrivage().ShowDialog();
        }
        private void nouveauToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrArrivage().ShowDialog();
        }
        private void utilisateursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsAdministration.NsUtilisateur.FrList().ShowDialog();
        }
        private void groupesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsAdministration.NsGroupe.FrList().ShowDialog();
        }
        #endregion
        private void FichierConnecter_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsApplication.FrLoginForm(this).ShowDialog();
        }
        private void FichierDeconnecter_Click(object sender, EventArgs e)
        {
            InitialiseAllActions();
            this.UserID = 0;
        }
        private void FichierQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void ArrivageList_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrListArrivage().ShowDialog();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var autorisation = db.ApplicationAutorisations.ToList();
                var actions = db.ApplicationActions.ToList();

                foreach (var action in actions)
                {
                    db.DeleteObject(action);

                }

                foreach (var action in autorisation)
                {
                    db.DeleteObject(action);

                }
                int position = 1;
                foreach (var item in menuMain.Items)
                {

                    int subposition = 0;
                    if (item is ToolStripMenuItem)
                    {
                        var menuItem = (ToolStripMenuItem)item;
                        subposition = 0;
                        var action = new ApplicationAction()
                        {
                            Nom = menuItem.Name,
                            Position = position,
                            Text = menuItem.Text,
                            Description = menuItem.Text,
                            // ParentActionID = 0,
                        };
                        action.ApplicationAutorisations.Add(new ApplicationAutorisation()
                        {
                            GroupeID = 1,
                            Autorisation = true

                        });

                        foreach (ToolStripItem subitem in menuItem.DropDownItems)
                        {
                            if (subitem is ToolStripMenuItem)
                            {
                                var menuSubItem = (ToolStripMenuItem)subitem;
                                var subAction = new ApplicationAction()
                                {
                                    Nom = menuSubItem.Name,
                                    Position = subposition,
                                    Text = menuSubItem.Text,
                                    Description = menuSubItem.Text,
                                };
                                subAction.ApplicationAutorisations.Add(new ApplicationAutorisation()
                                {
                                    GroupeID = 1,
                                    Autorisation = true

                                });
                                action.ApplicationAction1.Add(subAction);
                                subposition++;
                            }
                        }
                        db.AddToApplicationActions(action);
                        position++;
                    }
                }

                var result = db.ApplicationActions.Where(p => p.ParentActionID == null);
                foreach (var item in result)
                {



                }
                db.SaveChanges();
                this.ShowInformation("Done");
            }
        }
        private void menuMain_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            //UpdateApp();
        }
        private void ProductionPET_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsProduction.FrProductionPET().ShowDialog();

        }
        private void ProductionListPET_Click(object sender, EventArgs e)
        {

            new FZBGsys.NsProduction.FrListProduction().ShowDialog();
        }
        private void versUnitéPETToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrTransfertMatiere(EnSection.Unité_PET).ShowDialog();

        }
        private void versUnitéCanetteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrTransfertMatiere(EnSection.Unité_Canette).ShowDialog();
        }
        private void versAutresDéstinationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrTransfertMatiere(EnSection.Autre).ShowDialog();
        }
        private void TransfertListMP_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrListTransfertMatiere().ShowDialog();
        }
        private void TransfertPF_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrTransfertProduitFini().ShowDialog();
        }
        private void listTransfertsPF_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrListTransfertProduitFini().ShowDialog();
        }
        private void StockMP_Click(object sender, EventArgs e)
        {

        }
        private void StockPF_Click(object sender, EventArgs e)
        {
            //new FZBGsys.NsStock.FrStockProduitFini().ShowDialog();
            new FrStockProduitFiniManual().ShowDialog();
        }
        private void AdministrationMaintenance_Click(object sender, EventArgs e)
        {
            //new FZBGsys.NsAdministration.FrMaintenance().ShowDialog();
        }
        private void AideApropos_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsApplication.FrAboutBox().ShowDialog();
        }
        private void matièrePremièreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var list = db.StockMatieres.Where(p => p.EnStock == true).OrderBy(p => p.Ressource.TypeRessourceID).ToList();

                NsReports.Stock.StockReportController.PrintCustomStockMp(list, "Produits en Stock", db);


            }
        }
        private void listeDesClientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.NsClient.FrList().ShowDialog();
        }
        private void nouveauClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.NsClient.FrCreate().ShowDialog();
        }
        private void listeDesChauffeurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.NsChauffeur.FrList().ShowDialog();
        }
        private void ComercialBonLiv_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrBonLivraison().ShowDialog();
        }
        private void ComercialBonSort_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrBonSortie().ShowDialog();
        }
        private void ComercialBonAttrib_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrBonAttribution().ShowDialog();
        }
        private void CommercialListeBon_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrListBon().ShowDialog();
        }
        private void ComercialListVentes_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrListVentes().ShowDialog();
        }
        private void RapportEtatVente_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;
                if (from == to)
                {


                    NsReports.Comercial.ComercialReportController.PrintVenteJournee(from);

                }
                else
                {
                    using (var db = new ModelEntities())
                    {
                        var ventes = db.Ventes.Where(p =>
                            p.BonAttribution.Date <= from &&
                            p.BonAttribution.Date >= to &&
                            p.BonAttribution.Etat != (int)EnEtatBonAttribution.Annulé).ToList();

                        NsReports.Comercial.ComercialReportController.PrintCustomVentes(ventes, "Période du " + from.ToShortDateString() + " au " + to.ToShortDateString(), db);

                    }

                }

            }

        }
        private void RapportMP_Click(object sender, EventArgs e)
        {

        }
        private void RapportPF_Click(object sender, EventArgs e)
        {

        }
        private void RapportProduction_Click(object sender, EventArgs e)
        {

        }

        private void etatDesStocksToolStripMenuItem_Click(object sender, EventArgs e)
        {

            NsReports.Stock.StockReportController.PrintStockProduitFini();

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            new FZBGsys.NsRessources.NsFournisseur.FrCreateEdit().ShowDialog();
        }

        private void ArrivagelistFourni_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsRessources.NsFournisseur.FrList().ShowDialog();
        }

        private void operationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listeOperationToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void versementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrOperation().ShowDialog();
        }



        private void CaisseOpDivers_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrListOperation().ShowDialog();
        }

        private void journalDesVentesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;


                NsReports.Comercial.ComercialReportController.PrintJournalVentes(from, to);

            }
        }

        private void statistiquesDesClientsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriodeWilaya();
            var wilaya = FZBGsys.NsReports.FrSelectDatePeriode.W;


            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;

                


                if (wilaya == null || wilaya == "Tout")
                    NsReports.Comercial.ComercialReportController.PrintStatiClients(from, to);

                else
                    NsReports.Comercial.ComercialReportController.PrintStatiClients(from, to, wilaya);


            }
        }

        private void facturationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrFacture().ShowDialog();
        }

        private void transfertVersSageToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;


                FZBGsys.NsComercial.FrListBon.TransfertFactureSage(from, to);

            }



        }

        private void releverCompteClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrSelectDateClient().ShowDialog();
        }

        private void button2_Click_2(object sender, EventArgs e)
        {

        }
        private void situationComericaleToolStripMenuItem_Click(object sender, EventArgs e)
        {

            panTraitement.Visible = true;
            progressMain.Value = 0;
            using (var db = new ModelEntities())
            {
                var clientsCoount = db.Clients.Where(p => p.ID != 0 && p.ClientTypeID != 4).Count();
                progressMain.Maximum = clientsCoount;
            }

            bgwSituationCommercial.RunWorkerAsync();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (Tools.CurrentUserID != 1)
            {
                return;
            }



            return;
            var dateFrom = DateTime.Parse("2013/08/01");
            var dateTo = DateTime.Parse("2013/12/31");
            var maxPalette = 100;
            string[] ModePaiment = { "Chèque", "Espèces", "Virement", "Chèque", "Espèces", "Espèces" }; //%6
            string[] delaisLivraison = {
        "une semaine" ,"10 jours","10 jours", "15 jours" ,"20 jours","15 jours" ,"20 jours", "un mois",
        "une semaine" ,"10 jours","une semaine" ,"10 jours"};//%12

            string[] transport = { "Client", "Client", "Client", "Client", "Client", "Societe" }; //%6

            string[] delaisPaiment = { "En Avance", "En Avance", "En Avance", "Cache", "Cache", "3 jours", "3 jours", "3 jours" };//%8



            using (var db = new ModelEntities())
            {


                var bons = db.BonAttributions.Where(p => p.Date >= dateFrom && p.Date <= dateTo).ToList();
                List<Client> clients = bons.Select(p => p.Client).Distinct().ToList().Where(p => p.ClientTypeID == 1).ToList();

                foreach (var client in clients)
                {

                    var bonsIDs = bons.Where(p => p.ClientID == client.ID);
                    var listeVentes = new List<Vente>();
                    foreach (var bon in bonsIDs)
                    {
                        listeVentes.AddRange(bon.Ventes);
                    }

                    var nbrPal = listeVentes.Sum(p => p.NombrePalettes);
                    if (nbrPal < maxPalette)
                    {
                        continue;
                    }

                    if (client.CodeSage == "CL009609")
                    {

                    }

                    Random rd = new Random();
                    var exigence = new ExigenceClient();
                    client.ExigenceClients.Add(exigence);

                    exigence.DelaiLivraison = delaisLivraison.ElementAt(rd.Next(1000) % 12);
                    exigence.DelaiPaiment = delaisPaiment.ElementAt(rd.Next(1000) % 8);
                    exigence.Transport = transport.ElementAt(rd.Next(1000) % 6);
                    exigence.ModePaiment = ModePaiment.ElementAt(rd.Next(1000) % 6);



                    exigence.DateDebut = dateFrom;
                    exigence.DateFin = dateTo;
                    exigence.InscriptionUID = 1;
                    exigence.InscriptionDateTime = dateFrom.AddHours(-rd.Next(50, 150));
                    db.AddToExigenceClients(exigence);
                    db.SaveChanges();


                    var soda = listeVentes.Where(p => p.GoutProduit.TypeProduitID == 1).ToList();
                    var formats = soda.Select(p => p.FormatID).Distinct().ToList();


                    if (soda.Count() != 0)
                    {
                        foreach (var formatID in formats)
                        {
                            var qtt = soda.Where(p => p.FormatID == formatID).Sum(p => p.NombrePalettes);
                            if (qtt != 0)
                            {
                                qtt = 50 + qtt * 4;
                                qtt = qtt - (qtt % 50);
                                exigence.ExigenceClientQuantites.Add(
                               new ExigenceClientQuantite
                               {
                                   FormatID = formatID.Value,
                                   TypeProduitID = 1,
                                   QuantiteExigePalette = qtt
                               });
                            }


                        }


                        db.SaveChanges();

                    }

                    var d = exigence.ExigenceClientQuantites.Count;

                    var pulpe = listeVentes.Where(p => p.GoutProduit.TypeProduitID == 2).ToList();
                    formats = pulpe.Select(p => p.FormatID).Distinct().ToList();
                    if (pulpe.Count() != 0)
                    {
                        foreach (var formatID in formats)
                        {
                            var qtt = pulpe.Where(p => p.FormatID == formatID).Sum(p => p.NombrePalettes);
                            if (qtt != 0)
                            {
                                qtt = 50 + qtt * 4;
                                qtt = qtt - (qtt % 50);
                                exigence.ExigenceClientQuantites.Add(new ExigenceClientQuantite
                                                   {
                                                       FormatID = formatID.Value,
                                                       TypeProduitID = 2,
                                                       QuantiteExigePalette = qtt
                                                   });
                            }

                        }

                        db.SaveChanges();
                    }

                    var kl = exigence.ExigenceClientQuantites.Count;


                    ReportController.ExportPDF = true;
                    ReportController.ExportFileName = "EXIG_" + dateFrom.Year + "_" + exigence.Client.CodeSage;
                    ComercialReportController.PrintExigenceClient(exigence);
                    File.Move(ReportController.ExportFileName, "out\\EXIGENCES\\" + dateFrom.Year + "\\EXIG_" + dateFrom.Year + "_" + exigence.Client.CodeSage + ".pdf");


                }


            }


            //----------------------------------------------------- Commandes Auto from Livraisons


            return;
            using (var db = new ModelEntities())
            {
                var attributions = db.BonAttributions.Where(p => p.Date.Value.Year == 2013).ToList();
                foreach (var item in attributions)
                {
                    BonCommande com = new BonCommande();
                    com.Date = item.Date.Value.AddDays(-3);
                    if (com.Date.Value.DayOfWeek == DayOfWeek.Friday)
                    {
                        com.Date = com.Date.Value.AddDays(-1);
                    }

                    com.Client = item.Client;
                    com.BonAttributions.Add(item);
                    com.ApplicationUtilisateur = item.ApplicationUtilisateur;
                    com.EtatCommande = item.Etat == (int)EnEtatBonAttribution.Livré ? (int)EnEtatBonCommande.Solde : (int)EnEtatBonCommande.EnInstance;
                    com.InscriptionDate = com.Date.Value;

                    foreach (var v in item.Ventes)
                    {
                        com.CommandeProduits.Add(new CommandeProduit()
                        {
                            Format = v.Format,
                            GoutProduit = v.GoutProduit,
                            NombreFardeaux = v.NombreFardeaux,
                            NombrePalette = v.NombrePalettes,
                            TotalBouteille = v.TotalBouteilles,
                            TotalFardeaux = v.TotalFardeaux,
                            Unite = v.Unite

                        });
                    }

                }

                db.SaveChanges();
                Tools.ShowInformation("done");
            }
        }

        private void Caisse_Click(object sender, EventArgs e)
        {

        }

        private void Comercial_Click(object sender, EventArgs e)
        {

        }

        private void ComercialClient_Click(object sender, EventArgs e)
        {

        }

        private void activitéJournalièreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var date = DateTime.Parse("2012-08-13");

            ComercialReportController.PrintActiviteClientJournee(date);

        }

        private void RapportActivite_Click(object sender, EventArgs e)
        {
            new NsComercial.FrSelectMonth().ShowDialog();
        }



        private void button2_Click_3(object sender, EventArgs e)
        {

            NsReports.Comercial.ComercialReportController.PrepareSituationComerciale(DateTime.Now.Date);

        }

           private void button1_Click(object sender, EventArgs e)
        {
            //new FZBGsys.NsStock.StockMatierePremiere.FrFactureFournisseur().ShowDialog();
            new FZBGsys.NsAdministration.FrParemetre().ShowDialog();
            return;
            using (ModelEntities db = new ModelEntities())// ajouté au paramatreutilisateur
            {
                foreach (var paramtre in db.ApplicationParametres)
                {
                    foreach (var utilisateur in db.ApplicationUtilisateurs)
                    {
                        var existe = db.ParametreAutorisations.Where(p => p.UtilisateurID == utilisateur.ID).ToList();
                        if (existe.Count == 0)
                        {


                            db.AddToParametreAutorisations(new ParametreAutorisation
                            {
                                ParametreID = paramtre.ID,
                                UtilisateurID = utilisateur.ID,
                                Autorisation = false,
                            });
                        }
                    }
                }
                db.SaveChanges();
            }

            using (ModelEntities db = new ModelEntities())
            {


                DateTime to = DateTime.Parse("2015-01-01");


                DateTime from = DateTime.Parse("2012-07-23");

                foreach (var client in db.Clients.Where(p => p.ID > 0))
                {


                    var factures = client.Factures.Where(p => p.Date > from && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée);
                    int dd = factures.Count();
                    #region FilterFact


                    factures = factures.Where(p => p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré);

                    //   dd = factures.Count();

                    foreach (var fact in factures)
                    {
                        if (fact.Etat == null)
                        {
                            fact.Etat = (int)EnEtatFacture.Payée;
                        }
                    }
                    #endregion
                    //  var cc = factures.Count();

                    var retours = client.FactureRetours.Where(p => p.Date > from && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée && p.BonRetour.Etat != (int)EnEtatBonAttribution.Annulé);
                    //  int count = retours.Count();
                    var operationD = client.Operations.Where(p => p.Date > from && p.Sen == "D" && p.Date <= to &&
                        p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client);
                    //   count = operationD.Count();
                    var operationC = client.Operations.Where(p => p.Date > from && p.Sen == "C" && p.Date <= to &&
                        p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client);

                    //  count = operationC.Count();

                    client.DateHistory = DateTime.Parse("2012-07-23");
                    decimal? soldeHistory = null;

                    var clientHistory = db.Soldes23072012.SingleOrDefault(p => p.code == client.CodeSage);

                    if (clientHistory == null)
                    {
                        soldeHistory = 0;
                    }
                    else
                    {
                        if (clientHistory.debit != null && clientHistory.debit.ParseToDec() != 0)
                        {
                            soldeHistory = -clientHistory.debit.Replace(".", ",").ParseToDec();

                        }
                        else if (clientHistory.credit != null && clientHistory.credit.ParseToDec() != 0)
                        {
                            soldeHistory = clientHistory.credit.Replace(".", ",").ParseToDec();

                        }

                    }

                    if (soldeHistory == null)
                    {
                        soldeHistory = 0;
                    }
                    //client.SoldeHistory = soldeHistory;
                    soldeHistory = soldeHistory +
                            operationC.Sum(p => p.Montant) -
                            operationD.Sum(p => p.Montant) -
                            factures.Sum(p => p.MontantTTC) +
                            retours.Sum(p => p.MontantTTC);
                    Soldes01012015 Solde = new Soldes01012015();
                    Solde.Code = client.CodeSage;
                    Solde.Designation = client.Nom;

                    if (soldeHistory > 0)
                    {
                        Solde.credit = soldeHistory.ToString();
                        Solde.debit = "0";
                    }
                    else
                        if (soldeHistory < 0)
                        {
                            Solde.debit = (soldeHistory * (-1)).ToString();
                            Solde.credit = "0";
                        }

                        else
                        {
                            Solde.debit = "0";
                            Solde.credit = "0";
                        }

                    db.AddToSoldes01012015(Solde);





                }
                db.SaveChanges();

            }

            return;


            using (var db = new ModelEntities())
            {

                foreach (var gout in db.GoutProduits.Where(p => p.ID != 1))
                {

                    var refpd = db.ReferenceProduits.Where(p => p.GoutProduitID == gout.ID && p.FormatID == 12).ToList();
                    if (refpd.Count == 0)
                        db.AddToReferenceProduits(new ReferenceProduit
                        {
                            GoutProduit = gout,
                            Format = db.Formats.Single(p => p.ID == 12),
                            Reference = "G" + gout.ID.ToString() + "F12",
                        });


                }
                db.SaveChanges();

            }


            using (var db = new ModelEntities())
            {
                var date = DateTime.Now;
                var stockdb = db.StockProduitFiniManuals.Where(p => p.DateStock == date).ToList();
                int[] formats = db.Formats.Select(p => p.ID).ToArray(); //{ 3, 5, 8, 1, 6 };
                int[] gouts = db.GoutProduits.Select(p => p.ID).ToArray();
                foreach (var format in formats)
                {
                    foreach (var gout in gouts)
                    {
                        var exists = stockdb.Where(p => p.FormatID == format && p.GoutProduitID == gout);
                        if (exists.Count() == 0)
                        {
                            db.AddToStockProduitFiniManuals(new StockProduitFiniManual
                            {
                                DateStock = date.Date,
                                FormatID = format,
                                GoutProduitID = gout,
                                isValid = false,
                                NombreFardeaux = 0,
                                NombrePalettes = 0,
                                TotalBouteilles = 0,
                                take = false,
                                DateTimeValid = (format == 6 || format == 8 || format == 5) ? date.Date.AddMonths(9) : date.Date.AddMonths(6),


                            });
                        }
                    }
                }
                db.SaveChanges();
                Tools.ShowInformation("Done");
            }
            return;
            var listeFiles = Directory.GetFiles("C:\\PDF");
            List<FileInfo> f = new List<FileInfo>();


            MergeEx padfglobal = new MergeEx();
            padfglobal.SourceFolder = "C:\\PDF\\";

            foreach (var file in listeFiles)
            {
                f.Add(new FileInfo(file));

            }

            f = f.OrderBy(p => p.LastWriteTime).ToList();

            int ks = 0;


            foreach (var item in f)
            {
                ks++;
                padfglobal.AddFile(item.Name);
                if (ks % 159 == 0)
                {
                    padfglobal.DestinationFile = "C:\\ALL\\ALL" + ks + ".pdf";
                    padfglobal.Execute();

                    padfglobal = new MergeEx();
                    padfglobal.SourceFolder = "C:\\PDF\\";

                }

            }

            padfglobal.DestinationFile = "C:\\ALL\\ALL" + ks + ".pdf";
            padfglobal.Execute();






            return;

            using (var db = new ModelEntities())
            {
                var dateFin = DateTime.Parse("2013-01-01");
                var listeBons = db.BonLivraisons.Where(p => p.DateLivraison < dateFin).ToList();
                //var listeDesFichiers = new List<string>();
                // MergeEx padfglobal = new MergeEx();

                foreach (var bon in listeBons)
                {

                    padfglobal.AddFile("C:\\PDF\\" + bon.ID.ToString() + ".pdf");

                    // FZBGsys.NsRapports.FrViewer.ExportPDF = true;

                    //  
                    //  FZBGsys.NsRapports.FrViewer.ExportFileName = bon.ID.ToString() ;
                    //  FZBGsys.NsRapports.FrViewer.PrintBonLivraison(bon.ID);
                    //   if (!string.IsNullOrEmpty(FZBGsys.NsRapports.FrViewer.ExportFileName))
                    //   {
                    // padfglobal.AddFile(FZBGsys.NsRapports.FrViewer.ExportFileName); 
                    //   }

                }


                padfglobal.DestinationFile = "BL2012.pdf";
                padfglobal.Execute();



            }


            return;
            using (var db = new ModelEntities())
            {
                var d = db.tmpLivs.ToList();
                var listFiles = new List<string>();
                int i = 0;
                int n = 6;
                foreach (var blf in d)
                {

                    if (i > n)
                    {
                        //break;
                    }

                    var liv = db.BonLivraisons.Single(p => p.ID == blf.Etat);
                    var month = liv.DateLivraison.Value.ToString("MMMM yyyy");


                    if (!Directory.Exists("out\\" + month))
                    {
                        Directory.CreateDirectory("out\\" + month);
                    }
                    var destFile = "out\\" + month + "\\" + "BLF" + liv.ID.ToString() + ".pdf";
                    if (!File.Exists(destFile))
                    {
                        ReportController.ExportPDF = true;
                        ReportController.ExportFileName = "BLF" + blf.Etat.ToString();
                        ComercialReportController.PrintBonLivraison3(blf.Etat.Value);
                        //                        listFiles.Add(FZBGsys.NsRapports.FrViewer.ExportFileName);
                        File.Copy(ReportController.ExportFileName, destFile);
                    }
                    listFiles.Add(destFile);
                    var sortie = liv.BonAttribution.BonSorties.SingleOrDefault();
                    if (sortie != null)
                    {
                        var destf = "out\\" + month + "\\" + "BS" + sortie.ID.ToString() + ".pdf";
                        if (!File.Exists(destf))
                        {
                            ReportController.ExportFileName = "BS" + sortie.ID.ToString();
                            ComercialReportController.PrintBonSortie(sortie.ID);
                            //  listFiles.Add(FZBGsys.NsRapports.FrViewer.ExportFileName);
                            File.Copy(ReportController.ExportFileName, destf);
                        }
                        listFiles.Add(destf);
                    }
                    else
                    {

                    }

                    if (listFiles.Count >= 120000)
                    {
                        /* i++;
                         MergeEx ext = new MergeEx();

                         FileInfo fnt = null;// = new FileInfo(FZBGsys.NsRapports.FrViewer.ExportFileName);
                         //   ext.SourceFolder = fnt.Directory.ToString() + "\\";
                         ext.DestinationFile = "out\\" + "BLF_BS_PART_" + i.ToString() + ".pdf";

                         foreach (var file in listFiles)
                         {

                             fnt = new FileInfo(file);
                             ext.AddFile(fnt.FullName);

                         }
                         ext.Execute();
                         listFiles.Clear();*/
                    }
                }

                i++;

                MergeEx ex = new MergeEx();


                ex.DestinationFile = "out\\" + "ALL.pdf";
                FileInfo fn = null;
                foreach (var file in listFiles)
                {

                    fn = new FileInfo(file);
                    ex.AddFile(fn.FullName);

                }



                //      ex.Execute();

                var dir = Directory.GetDirectories("out");
                foreach (var dr in dir)
                {
                    DirectoryInfo ff = new DirectoryInfo(dr);
                    MergeEx ext = new MergeEx();

                    FileInfo fnt = null;// = new FileInfo(FZBGsys.NsRapports.FrViewer.ExportFileName);
                    //   ext.SourceFolder = fnt.Directory.ToString() + "\\";
                    ext.DestinationFile = "out\\" + "BLF_BS_" + ff.Name.ToString() + ".pdf";
                    var files = Directory.GetFiles(dr);
                    foreach (var file in files)
                    {

                        fnt = new FileInfo(file);
                        ext.AddFile(fnt.FullName);

                    }
                    ext.Execute();
                }



            }

            Tools.ShowInformation("Done");

        }
            void etatJournalierDesDépensesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectDay();

            if (periode != null)
            {
                var date = periode[0].Date;

                var yesterday = date.AddDays(-1);
                if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                {
                    yesterday = yesterday.AddDays(-2);
                }
                using (var db = new ModelEntities())
                {
                    var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == yesterday);
                    decimal? AnSolde = null;
                    if (hist != null)
                    {
                        AnSolde = hist.Solde.Value;

                    }
          FZBGsys.NsReports.Caisse.CaisseReportController.PrintSortieCaisseJournee(date, AnSolde, yesterday.ToShortDateString());


                }
            }
        }

        private void etatJournalierDesMouvementsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectDay();

            if (periode != null)
            {
                var date = periode[0].Date;

                var yesterday = date.AddDays(-1);
                if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                {
                    yesterday = yesterday.AddDays(-2);
                }
                using (var db = new ModelEntities())
                {
                    var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == yesterday);
                    decimal? AnSolde = null;
                    if (hist != null)
                    {
                        AnSolde = hist.Solde.Value;

                    }


                    FZBGsys.NsReports.Caisse.CaisseReportController.PrintMouvementCaisseJournee(date, AnSolde, yesterday.ToShortDateString());


                }
            }
        }

        private void etatRécapitulatifDesMouvementsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;



                FZBGsys.NsReports.Caisse.CaisseReportController.PrintOperationsPeriode(from, to);


            }
        }

        private void button2_Click_4(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var dateFin = DateTime.Parse("2012-09-01");
                var dateDebut = DateTime.Parse("2012-07-31");
                var stocksFin = db.StockMatiereHistories.Where(p => p.DateHistory == dateFin).ToList();

                foreach (var stock in stocksFin)
                {
                    decimal? arrivageQtt = db.Arrivages.Where(p => p.DateArrivage <= dateFin && p.DateArrivage > dateDebut).Sum(p => p.Quantite);
                    int? arrivagePce = db.Arrivages.Where(p => p.DateArrivage <= dateFin && p.DateArrivage > dateDebut).Sum(p => p.Piece);

                    decimal? consomationQtt = 0;
                    int? consomationPce = 0;
                    /*  switch ((EnTypeRessource)stock.Ressource.TypeRessourceID)
                       {
                           case EnTypeRessource.Preforme:
                            
                               consomationQtt = db.ProductionSoufflages.Where(p => p.Production.HeureDemarrage <= dateFin &&
                                   p.Production.HeureFinDeProd > dateDebut &&
                                   p.PreformeGammeID == stock.Ressource.TypePreformeID &&
                                   p.FournisseurID == stock.Ressource.FournisseurID

                                   ).Sum(p => p.QuantitePieces);

                               consomationQtt = db.ProductionSoufflages.Where(p => p.Production.HeureDemarrage <= dateFin &&
                                   p.Production.HeureFinDeProd > dateDebut &&
                                   p.PreformeGammeID == stock.Ressource.TypePreformeID &&
                                   p.FournisseurID == stock.Ressource.FournisseurID

                                   ).Sum(p => p.QuantitePieces);

                               break;
                           case EnTypeRessource.Sucre:
                               consomationQtt = db.ProductionSiroperies.Where(p => p.Production.HeureDemarrage <= dateFin &&
                                   p.Production.HeureFinDeProd > dateDebut &&
                                   p.TypeRessourceID == stock.Ressource.TypeRessourceID &&
                                   p.FournisseurID == stock.Ressource.FournisseurID

                                   ).Sum(p => p.QuantiteUM);
                               break;
                           case EnTypeRessource.Film_Thermofusible:
                               break;
                           case EnTypeRessource.Film_Etirable:
                               break;
                           case EnTypeRessource.Arrome:
                               break;
                           case EnTypeRessource.Etiquette:
                               break;
                           case EnTypeRessource.Bouchon:
                               break;
                           case EnTypeRessource.Cole:
                               break;
                           case EnTypeRessource.Bouteille_Verre:
                               break;
                           case EnTypeRessource.Cannete:
                               break;
                           case EnTypeRessource.Aspartam:
                               break;
                           case EnTypeRessource.Acide_Citrique:
                               break;
                           case EnTypeRessource.Consontre_Orange:
                               break;
                           case EnTypeRessource.CO2:
                               break;
                           case EnTypeRessource.InterCalaire:
                               break;
                           case EnTypeRessource.Colorant:
                               break;
                           case EnTypeRessource.Benzoate:
                               break;
                           case EnTypeRessource.Barquette:
                               break;
                           case EnTypeRessource.Produit_Chimique:
                               break;
                           case EnTypeRessource.Solvant:
                               break;
                           case EnTypeRessource.Encre:
                               break;
                           case EnTypeRessource.Prod_Chimique_Promain:
                               break;
                           case EnTypeRessource.Prod_Chimique_Proneige_AL:
                               break;
                           case EnTypeRessource.Prod_Chimique_Proneige_AC:
                               break;
                           case EnTypeRessource.Prod_Chimique_OXIPRO_AS:
                               break;
                           case EnTypeRessource.Prod_Chimique_Javel:
                               break;
                           case EnTypeRessource.Prod_Chimique_Proflow:
                               break;
                           case EnTypeRessource.Prod_Chimique_Acide_Chlorhydrique:
                               break;
                           default:
                               break;
                       }
                  


                       switch ((EnTypeRessource)stock.Ressource.TypeRessourceID)
                       {
                           case EnTypeRessource.Preforme:
                               consomationQtt = db.Productions.Where(p => p.HeureDemarrage <= dateFin && p.HeureDemarrage > dateDebut).Sum(p => p.NombrePreformeConsome);

                               break;
                           case EnTypeRessource.Sucre:
                               break;
                       consomationPce =db.ProductionSiroperies.ToList().Where(p=>p.Production.HeureDemarrage <=dateFin && p.Production.HeureDemarrage > dateDebut).Sum(
                           case EnTypeRessource.Film_Thermofusible:
                               break;
                           case EnTypeRessource.Film_Etirable:
                               break;
                           case EnTypeRessource.Arrome:
                               break;
                           case EnTypeRessource.Etiquette:
                               break;
                           case EnTypeRessource.Bouchon:
                               break;
                           case EnTypeRessource.Cole:
                               break;
                           case EnTypeRessource.Bouteille_Verre:
                               break;
                           case EnTypeRessource.Cannete:
                               break;
                           case EnTypeRessource.Aspartam:
                               break;
                           case EnTypeRessource.Acide_Citrique:
                               break;
                           case EnTypeRessource.Consontre_Orange:
                               break;
                           case EnTypeRessource.CO2:
                               break;
                           case EnTypeRessource.InterCalaire:
                               break;
                           case EnTypeRessource.Colorant:
                               break;
                           case EnTypeRessource.Benzoate:
                               break;
                           case EnTypeRessource.Barquette:
                               break;
                           case EnTypeRessource.Produit_Chimique:
                               break;
                           case EnTypeRessource.Solvant:
                               break;
                           case EnTypeRessource.Encre:
                               break;
                           case EnTypeRessource.Prod_Chimique_Promain:
                               break;
                           case EnTypeRessource.Prod_Chimique_Proneige_AL:
                               break;
                           case EnTypeRessource.Prod_Chimique_Proneige_AC:
                               break;
                           case EnTypeRessource.Prod_Chimique_OXIPRO_AS:
                               break;
                           case EnTypeRessource.Prod_Chimique_Javel:
                               break;
                           case EnTypeRessource.Prod_Chimique_Proflow:
                               break;
                           case EnTypeRessource.Prod_Chimique_Acide_Chlorhydrique:
                               break;
                           default:
                               break;
                       }
                       //         var consomation = db.consomation
                       //
                     * 
                     *   */
                }

            }
        }

        private void ProductionCan_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsProduction.FrProductionCAN().ShowDialog();
        }

        private void ComercialBonRetour_Click(object sender, EventArgs e)
        {
            new NsComercial.FrBonRetour().ShowDialog();
        }

        private void ComercialFactRetour_Click(object sender, EventArgs e)
        {
            new NsComercial.FrFactureRetour().ShowDialog();
        }

        private void etatDuStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrStockMatiere().ShowDialog();
        }

        private void demandeBesoinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsStock.FrBesoinMatiere().ShowDialog();
        }

        private void statistiquesDesArticlesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;

                if (Tools.CurrentUser.GroupeID != 1)
                {
                    while (from.DayOfWeek != DayOfWeek.Saturday)
                        from = from.AddDays(-1);
                }


                NsReports.Comercial.ComercialReportController.PrintStatArticles(from, to);


            }
        }

        private void productuionVer_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsProduction.FrProductionVER().ShowDialog();
        }

        private void releverCompteClientToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void releverCompteClientToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            new NsComercial.FrSelectDateClient().ShowDialog();
        }

        private void transfertMatièrePremièreToolStripMenuItem_Click(object sender, EventArgs e)
        {


            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date.AddDays(1);


                ProductionReportController.PrintProductionCustom(from, to, (int)EnSection.Unité_PET, "UNITE PET");


            }
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            var date = FZBGsys.NsReports.FrSelectDatePeriode.SelectDay()[0];

            NsReports.Comercial.ComercialReportController.PrepareSituationComerciale(date, bgwSituationCommercial);

        }

        private void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressMain.Value = e.ProgressPercentage;
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            panTraitement.Visible = false;

            NsReports.Comercial.ComercialReportController.PrintSituationCommerciale();
        }

        private void exportVersSageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;


                FZBGsys.NsComercial.FrListOperation.TransfertOperationSage(from, to);

            }


        }

        private void transToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date.AddDays(1);


               ProductionReportController.PrintProductionCustom(from, to, (int)EnSection.Unité_Canette, "UNITE Canette");


            }
        }

        private void button2_Click_5(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var factures = db.Factures.ToList();

                foreach (var facture in factures)
                {
                    facture.MontantHT = facture.BonAttribution.Ventes.Sum(p => p.PrixUnitaireHT * p.TotalBouteilles);
                    facture.MontantTVA = facture.MontantTTC - facture.MontantTVA;
                }

                db.SaveChanges();
            }

        }
        private void UpdateApp()
        {

            if (System.Diagnostics.Debugger.IsAttached)
            {
                return;
            }
            ApplicationDeployment updater = ApplicationDeployment.CurrentDeployment;
            bool verDepServer = updater.CheckForUpdate();

            if (verDepServer)
            {
                new FrUpdate(updater).ShowDialog();

                if (!FrUpdate.IsCanceled)
                {
                    Application.Restart();
                }

            }

        }
        private void transfertSageToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;


                FZBGsys.NsComercial.FrListBon.TransfertFactureSage(from, to);

            }

        }

        private void groupBoxInfos_Enter(object sender, EventArgs e)
        {

        }

        private void button2_Click_6(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var ventes = db.VenteRemboursse30052013.ToList();

                var clientsIDs = ventes.Select(p => p.ClientID).Distinct();

                foreach (var clientID in clientsIDs)
                {
                    var client = db.Clients.Single(p => p.ID == clientID);
                    var operation = new Operation();
                    var ventesRemboursse = db.VenteRemboursse30052013.Where(p => p.ClientID == clientID).ToList();
                    var nbrFactures = ventesRemboursse.Select(p => p.BonLivraisonID).Distinct().Count();


                    //operation.ApplicationUtilisateur = 1;
                    operation.ClientID = client.ID;
                    operation.Date = DateTime.Now;
                    operation.InscriptionDate = DateTime.Now;
                    operation.InscriptionUID = 1;
                    operation.Lib = "REMBOURS. AUGM. PRIX "
                        + nbrFactures + " FACTURES (" + ventesRemboursse.Sum(p => p.NombrePalettes) + " PALETTES)";
                    operation.ModePayement = (int)EnModePayement.Virement;
                    operation.Montant = ventesRemboursse.Sum(p => p.TotalBouteilles) * 2;
                    operation.Observation = " par MR. HASSAIRI NOTE DE MR BENFISSA";
                    operation.Sen = "C";
                    operation.TypeOperaitonID = (int)EnTypeOpration.Avance;
                    operation.FournisseurID = 0;

                    db.Operations.AddObject(operation);
                }

                db.SaveChanges();

            }

            Tools.ShowInformation("Done!");
        }

        private void evaluationFournisseursToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void evalusationFournisseursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsStock.StockMatierePremiere.FrEvaluationFournisseur().ShowDialog();
        }

        private void listeDesFournisseursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsRessources.NsFournisseur.FrList().ShowDialog();
        }

        private void produitFiniToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void evaluationFournissersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.StockMatierePremiere.FrSelectDateFournisseur().ShowDialog();
        }

        private void etatMensuelToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void satisfactionClientsFormulairesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void satisfactionClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrSatisfactionClient().ShowDialog();
        }

        private void satisfactionClientToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrSelectDateClientSatisfaction().ShowDialog();
        }

        private void listeDesDemandesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.FrListDemande().ShowDialog();

        }

        private void ComercialCommand_Click(object sender, EventArgs e)
        {

        }

        private void nouvelleCommandeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrBonCommande().ShowDialog();

        }

        private void listeDesCommandesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrListeCommande().ShowDialog();
        }

        private void etatDesMouvementsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void unitéVerreToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date.AddDays(1);


                ProductionReportController.PrintProductionCustom(from, to, (int)EnSection.Unité_Verre, "UNITE Verre");


            }
        }

        private void MainChangePasse_Click(object sender, EventArgs e)
        {
            if (Tools.CurrentUser == null)
            {
                Tools.ShowInformation("vous devez vous connecter pour changer le mot de passe");
            }
            else
            {
                new NsApplication.FrEditPass(Tools.CurrentUser).ShowDialog();

            }
        }

        private void listeDesChaffeursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.NsChauffeur.FrList().ShowDialog();
        }

        private void situationFlotteToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var dates = NsReports.FrSelectDatePeriode.SelectPeriode();
            ComercialReportController.PrintFlotte(dates[0], dates[1]);


        }

        private void gestionDesMatieresToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionDesProduitToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listeDesMatieresPrimièreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsStock.StockMatierePremiere.FrTypeRessourceList().ShowDialog();
        }

        private void listeDesArromesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsStock.StockMatierePremiere.FrListArrome().ShowDialog();
        }

        private void listeDesProduitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FrListProduit().ShowDialog();
        }

        private void listeDesForamtsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.StockMatierePremiere.FrListeFromat().ShowDialog();
        }

        private void bk1_DoWork(object sender, DoWorkEventArgs e)
        {

            ComercialReportController.PrintdummyReport();

        }

        private void FrMain_Shown(object sender, EventArgs e)
        {
            bk1.RunWorkerAsync();
        }

        private void listeDesPrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrPrixProduitList().ShowDialog();
        }

        private void mouvementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.StockProduitFini.FrDistribution().ShowDialog();
        }

        private void listeDesMouvementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsStock.StockProduitFini.FrListDistribution().ShowDialog();
        }

        private void etatJournalierDesMouvementsClientsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var From = periode[0].Date;
                var To = periode[1].Date;

                var yesterday = From.AddDays(-1);
                if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                {
                    yesterday = yesterday.AddDays(-2);
                }

               /* using (var db = new ModelEntities())
                {
                    var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == yesterday);
                    decimal? AnSolde = null;
                    if (hist != null)
                    {
                        AnSolde = hist.Solde.Value;

                    }

                    FZBGsys.NsRapports.FrViewer.PrintMouvementCaisseJournee(date, AnSolde, yesterday.ToShortDateString());

                }*/

                
                FZBGsys.NsReports.Caisse.CaisseReportController.PrintMouvementRecette(From, To);

            }
        }

        private void suivieDeStockToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();
            if (periode != null)
            {
                var From = periode[0].Date;
                var To = periode[1].Date;


                NsReports.Stock.StockReportController.PrintSuiviStockPeriode(From, To);



            }


        }

        private void releveCompteClientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new NsComercial.FrSelectDateClient().ShowDialog();
        }

        private void etatJournalierDesMouvementsClientsToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var From = periode[0].Date;
                var To = periode[1].Date;

                var yesterday = From.AddDays(-1);
                if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                {
                    yesterday = yesterday.AddDays(-2);
                }

               
                FZBGsys.NsReports.Caisse.CaisseReportController.PrintMouvementRecette(From, To);

            }
        }

        private void statistiquesDesArticlesToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            var periode = FZBGsys.NsReports.FrSelectDatePeriode.SelectPeriode();

            if (periode != null)
            {
                var from = periode[0].Date;
                var to = periode[1].Date;

                if (Tools.CurrentUser.GroupeID != 1)
                {
                    while (from.DayOfWeek != DayOfWeek.Saturday)
                        from = from.AddDays(-1);
                }


               ComercialReportController.PrintStatArticles(from, to);


            }
        }

        private void situationFlotteToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            var dates = NsReports.FrSelectDatePeriode.SelectPeriode();
            ComercialReportController.PrintFlotte(dates[0], dates[1]);

        }

        private void satisfactionClientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FZBGsys.NsComercial.FrSelectDateClientSatisfaction().ShowDialog();
        }

        private void situationComericaleToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            panTraitement.Visible = true;
            progressMain.Value = 0;
            using (var db = new ModelEntities())
            {
                var clientsCoount = db.Clients.Where(p => p.ID != 0 && p.ClientTypeID != 4).Count();
                progressMain.Maximum = clientsCoount;
            }

            bgwSituationCommercial.RunWorkerAsync();

        }

        private void parèmetresToolStripMenuItem_Click(object sender, EventArgs e)
        {

            new FZBGsys.NsAdministration.FrParemetre().ShowDialog();
        }

        private void backupDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.BackupDataBaseManual();
        }

        private void restoreDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tools.RestoreDataBaseManual();
        }

      


    }
}
