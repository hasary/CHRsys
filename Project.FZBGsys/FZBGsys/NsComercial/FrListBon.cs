﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using FZBGsys.NsTools;
using FZBGsys.NsReports.Comercial;


namespace FZBGsys.NsComercial
{
    public partial class FrListBon : Form
    {
        ModelEntities db = new ModelEntities();

        public List<BonAttribution> ListFoundAttribution { get; set; }

        public List<BonSortie> ListFoundSortie { get; set; }
        public List<BonLivraison> ListFoundLivraison { get; set; }
        public List<Facture> ListFoundFacture { get; set; }
        public List<BonRetour> ListFoundRetour { get; set; }
        public List<FactureRetour> ListFoundFactureRetour { get; set; }


        public bool ActiveAttribution { get; set; }
        public bool ActiveSortie { get; set; }
        public bool ActiveLivraison { get; set; }
        public bool ActiveFacture { get; set; }
        public bool ActiveFactureRetour { get; set; }
        public bool ActiveRetour { get; set; }

        public static void TransfertFactureSage(DateTime From, DateTime To)
        {

            if (true)
            {
                using (var db = new ModelEntities())
                {
                    var factures = db.Factures.Where(
                        p => p.Etat != (int)EnEtatFacture.Annulée &&
                        p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré &&
                        p.Date <= To && p.Date >= From).ToList();

                    StreamWriter sr = File.CreateText("fact_import.txt");
                    sr.WriteLine("NoFacture$NoLivraison$Date$CodeClient$ModePayement$RefArticle$Designation$QuantiteBouteilles$PrixUnitHT$PrixUnitTVA$PrixUnitTTC$TotalHT$TotalTVA$TotalTTC$TotalFactureHT$TotalFactureTva$TotalFactureTTC$TotalFactureRemise$TotalFactureTimbre$TotalFactureNetAPayer");
                    foreach (var facture in factures)
                    {
                        if (facture.BonAttributionID == 5774)
                        {

                        }

                        var ventes = facture.BonAttribution.Ventes.ToList();
                        foreach (var vente in ventes)
                        {
                            var pht = vente.PrixUnitaireHT;//+"$"+
                            var ptva = vente.PrixUnitaire - pht;
                            var reference = db.ReferenceProduits.SingleOrDefault(p => p.GoutProduitID == vente.GoutProduitID && p.FormatID == vente.FormatID);

                            if (reference == null)
                            {

                            }

                            var referenceTxt = (reference == null) ? "" : reference.Reference;
                            var referenceDes = (reference == null) ? "" : reference.GoutProduit.Nom + " " + reference.Format.Volume;
                            string blf = "";
                            if (facture.BonAttribution.BonLivraisons.Count() == 1)
                            {
                                blf = "BLF" + facture.BonAttribution.BonLivraisons.First().ID;
                            }
                            sr.WriteLine(
                                                "FZ" + facture.ID + "$" +
                                                 blf + "$" +
                                                facture.Date.Value.ToShortDateString() + "$" +
                                                facture.Client.CodeSage + "$" +
                                                facture.ModePayement.ToModePayementTxt() + "$" +
                                                referenceTxt + "$" +
                                                referenceDes + "$" +
                                                vente.TotalBouteilles + "$" +
                                                decimal.Round(pht.Value, 4) + "$" +
                                                decimal.Round(ptva.Value, 4) + "$" +
                                                vente.PrixUnitaire.Value + "$" +
                                                decimal.Round(vente.TotalBouteilles.Value * pht.Value, 4).ToString() + "$" +
                                                decimal.Round(vente.TotalBouteilles.Value * ptva.Value, 4).ToString() + "$" +
                                                decimal.Round(vente.TotalBouteilles.Value * vente.PrixUnitaire.Value, 4) + "$" +
                                                facture.MontantHT.ToString() + "$" +
                                                facture.MontantTVA.ToString() + "$" +
                                                facture.MontantTTC.ToString() + "$" +
                                                facture.MontantTotalRemise + "$" +
                                                facture.MontantTimbre + "$" +
                                                facture.MontantTotalFacture

                                               );
                        }
                    }
                    sr.Close();
                    System.Diagnostics.Process.Start("fact_import.txt");
                }
            }



        }

        private void UpdateTotal()
        {
            if (tcDocument.SelectedTab == tpAttribution && ListFoundAttribution != null) labTotalBon.Text = ListFoundAttribution.Count + " bons";
            if (tcDocument.SelectedTab == tpSortie && ListFoundSortie != null) labTotalBon.Text = ListFoundSortie.Count + " bons";
            if (tcDocument.SelectedTab == tpLivraison && ListFoundLivraison != null) labTotalBon.Text = ListFoundLivraison.Count + " bons";
            if (tcDocument.SelectedTab == tpFacture && ListFoundFacture != null) labTotalBon.Text = ListFoundFacture.Count + " factures";

            if (tcDocument.SelectedTab == tpRetour && ListFoundRetour != null) labTotalBon.Text = ListFoundRetour.Count + " bons";
            if (tcDocument.SelectedTab == tpFactureRetour && ListFoundFactureRetour != null) labTotalBon.Text = ListFoundFactureRetour.Count + " factures";
        }

        public FrListBon()
        {
            InitializeComponent();
            ActiveAttribution = true;
            ActiveFacture = true;
            ActiveLivraison = true;
            ActiveSortie = true;
            ActiveRetour = true;
            ActiveFactureRetour = true;
            InitiliseControls();


        }

        private void InitiliseControls()
        {
            cbDocument.Items.Clear();
            /*
                             [Tout]
                Attribution
                Sortie
                Livraison
                Facture
             
             
             */
            cbDocument.Items.Add("[Tout]");
            if (ActiveAttribution) cbDocument.Items.Add("Attribution");
            if (ActiveSortie) cbDocument.Items.Add("Sortie");
            if (ActiveLivraison) cbDocument.Items.Add("Livraison");
            if (ActiveFacture) cbDocument.Items.Add("Facture");

            if (ActiveRetour) cbDocument.Items.Add("BonRetour");
            if (ActiveFactureRetour) cbDocument.Items.Add("FactureRetour");


            cbDocument.SelectedIndex = 0;

            if (Tools.CurrentUser.GroupeID != 1) chRechAvancee.Enabled = false;


        }
        private void Recherche()
        {
            //using (var db = new ModelEntities())
            {
                IQueryable<BonAttribution> resultAttribution = db.BonAttributions;//.Where(p => p.Factures.Count != 0);
                IQueryable<BonSortie> resultSortie = db.BonSorties;
                IQueryable<BonLivraison> resultLivraison = db.BonLivraisons;
                IQueryable<Facture> resultFacture = db.Factures;
                IQueryable<FactureRetour> resultFactureRetour = db.FactureRetours;
                IQueryable<BonRetour> resultRetour = db.BonRetours;


                if (!chRechAvancee.Checked)
                {
                    var date = dtDate.Value.Date;

                    if (ActiveAttribution) resultAttribution = resultAttribution.Where(p => p.Date == date && p.Etat != (int)EnEtatBonAttribution.Annulé);
                    else resultAttribution = resultAttribution.Where(p => false);


                    if (ActiveSortie) resultSortie = resultSortie.Where(p => p.Date == date);
                    else resultSortie = resultSortie.Where(p => false);


                    if (ActiveLivraison) resultLivraison = resultLivraison.Where(p => p.DateLivraison == date);
                    else resultLivraison = resultLivraison.Where(p => false);


                    if (ActiveFacture) resultFacture = resultFacture.Where(p => p.Date == date);
                    else resultFacture = resultFacture.Where(p => false);

                    if (ActiveRetour) resultRetour = resultRetour.Where(p => p.Date == date);
                    else resultRetour = resultRetour.Where(p => false);

                    if (ActiveFactureRetour) resultFactureRetour = resultFactureRetour.Where(p => p.Date == date);
                    else resultFactureRetour = resultFactureRetour.Where(p => false);
                }
                else
                {
                    if (cbDocument.SelectedIndex != 0)
                    {
                        if (cbDocument.SelectedIndex != (int)EnTypeBon.Bon_Attribution) resultAttribution = resultAttribution.Where(p => false);
                        if (cbDocument.SelectedIndex != (int)EnTypeBon.Bon_Sortie) resultSortie = resultSortie.Where(p => false);
                        if (cbDocument.SelectedIndex != (int)EnTypeBon.Bon_Livraison) resultLivraison = resultLivraison.Where(p => false);
                        if (cbDocument.SelectedIndex != (int)EnTypeBon.Facture) resultFacture = resultFacture.Where(p => false);
                        if (cbDocument.SelectedIndex != (int)EnTypeBon.Bon_Retour) resultRetour = resultRetour.Where(p => false);
                        if (cbDocument.SelectedIndex != (int)EnTypeBon.Facture_Retour) resultFactureRetour = resultFactureRetour.Where(p => false);

                    }


                    var dateDu = dtDateDu.Value.Date;
                    var dateAu = dtDateAu.Value.Date;
                    resultAttribution = resultAttribution.Where(p => p.Date >= dateDu && p.Date <= dateAu);
                    resultSortie = resultSortie.Where(p => p.Date >= dateDu && p.Date <= dateAu);
                    resultLivraison = resultLivraison.Where(p => p.DateLivraison >= dateDu && p.DateLivraison <= dateAu);
                    resultFacture = resultFacture.Where(p => p.Date >= dateDu && p.Date <= dateAu);
                    resultRetour = resultRetour.Where(p => p.Date >= dateDu && p.Date <= dateAu);
                    resultFactureRetour = resultFactureRetour.Where(p => p.Date >= dateDu && p.Date <= dateAu);

                    var ID = txtNoBon.Text.ParseToInt();
                    if (ID != null)
                    {
                        resultAttribution = resultAttribution.Where(p => p.ID == ID);
                        resultSortie = resultSortie.Where(p => p.ID == ID);
                        resultLivraison = resultLivraison.Where(p => p.ID == ID);
                        resultFacture = resultFacture.Where(p => p.ID == ID);

                        resultRetour = resultRetour.Where(p => p.ID == ID);
                        resultFactureRetour = resultFactureRetour.Where(p => p.ID == ID);
                    }

                    var client = txtClient.Text.Trim();

                    if (Client != null)
                    {
                        resultAttribution = resultAttribution.Where(p => p.ClientID == Client.ID);
                        resultSortie = resultSortie.Where(p => p.BonAttribution.ClientID == Client.ID);
                        resultLivraison = resultLivraison.Where(p => p.BonAttribution.ClientID == Client.ID);
                        resultFacture = resultFacture.Where(p => p.ClientID == Client.ID);

                        resultRetour = resultRetour.Where(p => p.Client.Nom.Contains(client));
                        resultFactureRetour = resultFactureRetour.Where(p => p.Client.Nom.Contains(client));

                    }

                    if (gbEtat.Visible)
                    {
                        if (!chEnInstance.Checked)
                        {
                            resultAttribution = resultAttribution.Where(p => p.Etat != (int)EnEtatBonAttribution.EnInstance);
                            //      resultRetour = resultAttribution.Where(p => p.Etat != (int)EnEtatBonAttribution.EnInstance);
                        }

                        if (!chAnnulé.Checked)
                        {
                            resultAttribution = resultAttribution.Where(p => p.Etat != (int)EnEtatBonAttribution.Annulé);
                        }

                        if (!chCharge.Checked)
                        {
                            resultAttribution = resultAttribution.Where(p => p.Etat != (int)EnEtatBonAttribution.Chargé);
                        }

                        if (!chLivré.Checked)
                        {
                            resultAttribution = resultAttribution.Where(p => p.Etat != (int)EnEtatBonAttribution.Livré);
                        }
                    }

                }

                ListFoundAttribution = resultAttribution.ToList();
                ListFoundSortie = resultSortie.ToList();
                ListFoundLivraison = resultLivraison.ToList();
                ListFoundFacture = resultFacture.ToList();
                //----------------------------------------------------------------
                ListFoundRetour = resultRetour.ToList();
                ListFoundFactureRetour = resultFactureRetour.ToList();
            }

            RefreshGrid();
        }
        private void RefreshGrid()
        {
            dgvAttribution.DataSource = ListFoundAttribution.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Numero = p.ID.ToString().PadLeft(4, '0'),
                Client = p.Client.NomClientAndParent(),
                Quantite = ((p.Ventes.Sum(s => s.NombrePalettes) == 0) ? "" :
                p.Ventes.Sum(s => s.NombrePalettes).ToString().PadLeft(2, '0') + " P  ") + ((p.Ventes.Sum(s => s.NombreFardeaux) == 0) ? "" : p.Ventes.Sum(s => s.NombreFardeaux) + " F  "),
                Total = p.Ventes.Sum(s => s.TotalFardeaux) + " Fardeaux  " + p.Ventes.Sum(s => s.TotalBouteilles) + " Bouteilles",
                Montant = p.Ventes.Sum(r => r.PrixUnitaire * r.TotalBouteilles).Value.ToString("## ### ###"),
                Etat = p.Etat.ToEtatBonAttributionTxt(),
                Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID

            }).ToList();

            dgvAttribution.HideIDColumn();
            dgvAttribution.HideIDColumn("UID");
            //---------------------------------------------------------------------------------------------------------

            dgvRetour.DataSource = ListFoundRetour.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Numero = p.ID.ToString().PadLeft(4, '0'),
                Client = p.Client.NomClientAndParent(),
                Quantite = ((p.Ventes.Sum(s => s.NombrePalettes) == 0) ? "" :
                p.Ventes.Sum(s => s.NombrePalettes).ToString().PadLeft(2, '0') + " P  ") + ((p.Ventes.Sum(s => s.NombreFardeaux) == 0) ? "" : p.Ventes.Sum(s => s.NombreFardeaux) + " F  "),
                Total = p.Ventes.Sum(s => s.TotalFardeaux) + " Fardeaux  " + p.Ventes.Sum(s => s.TotalBouteilles) + " Bouteilles",
                Etat = p.Etat.ToEtatBonAttributionTxt(),
                Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID

            }).ToList();

            dgvRetour.HideIDColumn();
            dgvRetour.HideIDColumn("UID");
            //---------------------------------------------------------------------------------------------------------

            dgvSortie.DataSource = ListFoundSortie.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                Numero = p.ID.ToString().PadLeft(4, '0'),
                BonAttrib = p.BonAttribution.ID.ToString().PadLeft(4, '0'),
                Client = p.BonAttribution.Client.NomClientAndParent(),
                Quantite = ((p.BonAttribution.Ventes.Sum(s => s.NombrePalettes) == 0) ? "" : p.BonAttribution.Ventes.Sum(s => s.NombrePalettes).ToString().PadLeft(2, '0') + " P  ") + ((p.BonAttribution.Ventes.Sum(s => s.NombreFardeaux) == 0) ? "" : p.BonAttribution.Ventes.Sum(s => s.NombreFardeaux) + " F  "),
                Total = p.BonAttribution.Ventes.Sum(s => s.TotalFardeaux) + " Fardeaux  " + p.BonAttribution.Ventes.Sum(s => s.TotalBouteilles) + " Bouteilles",
                Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID
            }).ToList();

            dgvSortie.HideIDColumn();
            dgvSortie.HideIDColumn("UID");
            //------------------------------------------------------------------------------------------------------------------------------
            dgvLivraison.DataSource = ListFoundLivraison.Select(p => new
            {
                ID = p.ID,
                Date = p.DateLivraison.Value.ToShortDateString(),
                Numero = p.ID.ToString().PadLeft(4, '0'),
                BonAttrib = p.BonAttribution.ID.ToString().PadLeft(4, '0'),
                Client = p.BonAttribution.Client.NomClientAndParent(),
                Chauffeur = p.Chauffeur.Nom,
                Adresse = p.AdresseLivraison,
                Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                Modification = (p.ModificationTime != null) ? "le " + p.ModificationTime.Value.ToString("dd/MM/yyyy à hh:mm ") : "",
                UID = p.InscriptionUID
            }).ToList();

            dgvLivraison.HideIDColumn();
            dgvLivraison.HideIDColumn("UID");
            //------------------------------------------------------------------------------------------------------------
            dgvFacture.DataSource = ListFoundFacture.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                //  NoFact = p.ID.ToString().PadLeft(4, '0'),
                BonAttrib = p.BonAttributionID.ToString().PadLeft(4, '0'),
                Client = p.Client.NomClientAndParent(),
                //  MontantHT = p.MontantHT.ToAffInt(),
                // MontantTVA = p.MontantTVA.ToAffInt(),
                MontantTTC = p.MontantTTC.ToAffInt(),
                //   MontantTimbre = p.MontantTimbre.ToAffInt(),
                MontantTotal = p.MontantTotalFacture.ToAffInt(),
                Payement = p.ModePayement.ToModePayementTxt(),
                Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID,
            }).ToList();
            //--------------------------------------------------------------------------------------------------
            dgvFacture.HideIDColumn();
            dgvFacture.HideIDColumn("UID");

            dgvFactureRetour.DataSource = ListFoundFactureRetour.Select(p => new
            {
                ID = p.ID,
                Date = p.Date.Value.ToShortDateString(),
                NoFact = p.ID.ToString().PadLeft(4, '0'),
                BonRetour = p.BonRetourID.ToString().PadLeft(4, '0'),
                Client = p.Client.NomClientAndParent(),
                MontantHT = p.MontantHT.ToAffInt(),
                MontantTVA = p.MontantTTC.ToAffInt(),
                MontantTTC = p.MontantTTC.ToAffInt(),
                MontantTimbre = p.MontantTimbre.ToAffInt(),
                MontantTotal = p.MontantTotalFacture.ToAffInt(),
                Payement = p.ModePayement.ToModePayementTxt(),
                Inscription = "par " + p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionTime.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID,
            }).ToList();
            //--------------------------------------------------------------------------------------------------

            dgvFactureRetour.HideIDColumn();
            dgvFactureRetour.HideIDColumn("UID");

            UpdateControls();

        }
        private void UpdateControls()
        {
            tcDocument.TabPages.Clear();

            if (ListFoundAttribution.Count != 0) tcDocument.TabPages.Add(tpAttribution);
            if (ListFoundSortie.Count != 0) tcDocument.TabPages.Add(tpSortie);
            if (ListFoundLivraison.Count != 0) tcDocument.TabPages.Add(tpLivraison);
            //   if (ListFoundFacture.Count != 0) tcDocument.TabPages.Add(tpFacture);

            if (ListFoundRetour.Count != 0) tcDocument.TabPages.Add(tpRetour);
            //  if (ListFoundFactureRetour.Count != 0) tcDocument.TabPages.Add(tpFactureRetour);

        }


        private void ImprimerAttribution()
        {
            if (dgvAttribution.GetSelectedIndex() != null)
            {
                var ID = ListFoundAttribution.ElementAt(dgvAttribution.GetSelectedIndex().Value).ID;

                var fact = ListFoundAttribution.ElementAt(dgvAttribution.GetSelectedIndex().Value).Factures.First();

                FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonAttribution2(fact.ID);

            }

        }
        private void ImprimerSortie()
        {
            if (dgvSortie.GetSelectedIndex() != null)
            {
                var ID = ListFoundSortie.ElementAt(dgvSortie.GetSelectedIndex().Value).ID;



                FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonSortie(ID);

            }

        }
        private void ImprimerLivraison()
        {
            if (dgvLivraison.GetSelectedIndex() != null)
            {
                var ID = ListFoundLivraison.ElementAt(dgvLivraison.GetSelectedIndex().Value).ID;


                ComercialReportController.PrintBonLivraison3(ID);

            }

        }
        private void ImprimerFacture()
        {
            if (dgvFacture.GetSelectedIndex() != null)
            {
                var facture = ListFoundFacture.ElementAt(dgvFacture.GetSelectedIndex().Value);

                var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);
                var remiseTxt = (facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";

                ComercialReportController.PrintFacture(facture.ID, totalTxt, remiseTxt);


            }
        }
        private void ImprimerRetour()
        {
            if (dgvRetour.GetSelectedIndex() != null)
            {
                var ID = ListFoundRetour.ElementAt(dgvRetour.GetSelectedIndex().Value).ID;



                ComercialReportController.PrintBonRetour(ID);

            }

        }
        private void ImprimerFactureRetour()
        {
            if (dgvFactureRetour.GetSelectedIndex() != null)
            {
                var facture = ListFoundFactureRetour.ElementAt(dgvFactureRetour.GetSelectedIndex().Value);

                var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);
                var remiseTxt = (facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";

                ComercialReportController.PrintFactureRetour(facture.ID, totalTxt, remiseTxt);


            }
        }

        //-------------------------------------

        private void ModifieAttribution()
        {
            var id = dgvAttribution.GetSelectedID("Numero");
            var uid = dgvAttribution.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas modifier un document dont vous n'etes pas l'auteur");
                return;
            }

            var Attrib = db.BonAttributions.SingleOrDefault(p => p.ID == id);
            if (Attrib.Etat != (int)EnEtatBonAttribution.EnInstance)
            {
                this.ShowWarning("Vous ne pouvez pas modifier un Bon Attribution Chargé");
                return;
            }

            var date = DateTime.Parse(dgvAttribution.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }
            if (id != null)
            {
                new FrBonAttribution(id).ShowDialog();
            }
        }
        private void ModifieSortie()
        {
            var id = dgvSortie.GetSelectedID("Numero");
            var uid = dgvSortie.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID || Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas modifier un document dont vous n'etes pas l'auteur");
                return;
            }
            var date = DateTime.Parse(dgvSortie.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }
            var sortie = db.BonSorties.Single(p => p.ID == id);
            var Attrib = sortie.BonAttribution;
            if (Attrib.Etat != (int)EnEtatBonAttribution.Chargé)
            {
                this.ShowWarning("Vous ne pouvez pas modifier un Bon déja Livré");
                return;
            }

            if (id != null)
            {
                new FZBGsys.NsComercial.FrBonSortie(id).ShowDialog();
            }
        }
        private void ModifeLivraison()
        {
            var uid = dgvLivraison.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas modifier un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvLivraison.GetSelectedID("Numero");
            var date = DateTime.Parse(dgvLivraison.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }
            // var id = ListFoundLivraison.ElementAt().ID;
            if (id != null)
            {
                new FZBGsys.NsComercial.FrBonLivraison(id).ShowDialog();
            }
        }
        private void ModifieFacture()
        {
            var uid = dgvFacture.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas modifier un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvFacture.GetSelectedID();

            var date = DateTime.Parse(dgvFacture.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }

            var fact = db.Factures.Single(p => p.ID == id);
            var attrib = fact.BonAttribution;
            if (attrib.Etat != (int)EnEtatBonAttribution.EnInstance)
            {
                this.ShowWarning("vous ne pouvez pas modifier un bon chargé");
                return;
            }

            if (id != null)
            {
                new FZBGsys.NsComercial.FrFacture(id).ShowDialog();
            }
        }

        private void ModifieRetour()
        {
            var id = dgvRetour.GetSelectedID("Numero");
            var uid = dgvRetour.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas modifier un document dont vous n'etes pas l'auteur");
                return;
            }
            var date = DateTime.Parse(dgvRetour.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }



            if (id != null)
            {
                new FrBonRetour(id).ShowDialog();
            }
        }
        private void ModifieFactureRetour()
        {
            var uid = dgvFacture.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas modifier un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvFactureRetour.GetSelectedID("Numero");

            var date = DateTime.Parse(dgvFactureRetour.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }

            if (id != null)
            {
                new FZBGsys.NsComercial.FrFactureRetour(id).ShowDialog();
            }
        }

        private void AnnulerAttribution()
        {
            var uid = dgvAttribution.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas Annuler un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvAttribution.GetSelectedID();
            if (id == null)
            {
                return;
            }

            var selected = db.BonAttributions.Single(p => p.ID == id);
            if (selected.BonSorties.Count != 0)
            {

                {
                    this.ShowWarning("Ne peut pas supprimer un Bon déja " + selected.Etat.ToEtatBonAttributionTxt() + " ! ");
                    return;
                }
            }

            var Attrib = db.BonAttributions.SingleOrDefault(p => p.ID == id);
            if (Attrib.Etat != (int)EnEtatBonAttribution.EnInstance)
            {
                this.ShowWarning("Vous ne pouvez pas modifier un Bon Attribution Chargé");
                return;
            }

            var date = DateTime.Parse(dgvAttribution.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }



            if (this.ConfirmWarning("Confirmer annulation du Bon N° " + id + " ?"))
            {
                selected.Etat = (int)EnEtatBonAttribution.Annulé;
                //  int mode = 0;
                var fact = selected.Factures.SingleOrDefault();
                if (fact != null)
                {
                    fact.Etat = (int)EnEtatFacture.Annulée;

                    // mode = fact.ModePayement.Value;
                }
                if (selected.BonCommande != null && selected.BonCommande.EtatCommande == (int)EnEtatBonCommande.Solde)
                {
                    selected.BonCommande.EtatCommande = (int)EnEtatBonCommande.EnInstance;
                }
                db.SaveChanges();
                NsAdministration.FrMaintenance.GenerateSoldeClient(db, null, true, selected.Client);
                /* var liv = selected.BonLivraisons.SingleOrDefault();
                 if (liv != null)
                 {
                     db.DeleteObject(liv);
                
                // db.SaveChanges();
                 */
                //   fact.ModePayement = mode;
                db.SaveChanges();
                if (selected.Client.DateDernierMouvement == selected.Date)
                {
                    selected.Client.DateDernierMouvement = null;
                }

                if (selected.Client.DateDernierChargement == selected.Date)
                {
                    selected.Client.DateDernierChargement = null;
                }

            }
        }
        private void AnnulerSortie()
        {
            var uid = dgvSortie.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas Annuler un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvSortie.GetSelectedIndex();


            if (id == null)
            {
                return;
            }


            var sortie = ListFoundSortie.ElementAt(id.Value);
            var BonAttribution = sortie.BonAttribution;
            if (BonAttribution.Etat != (int)EnEtatBonAttribution.Chargé)
            {
                this.ShowWarning("Impossible de supprimer Bon Sortie car le Bon d'attribution associé est Livré");
                return;
            }

            /*   if (BonAttribution.Factures.Count != 0)
               {
                   this.ShowWarning("Impossible de supprimer Bon Sortie car le Bon d'attribution associé est Facturé,\nSupprimez d'abord le la Facture.");
                   return;
               }
   */
            var date = DateTime.Parse(dgvSortie.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }

            if (this.ConfirmWarning("Confirmer annulation Sortie N° " + sortie.ID + "?"))
            {
                var ventesDetails = sortie.VenteDetails.ToList();
                foreach (var details in ventesDetails)
                {
                    db.DeleteObject(details);
                }


                sortie.BonAttribution.Etat = (int)EnEtatBonAttribution.EnInstance;

                var listesortie = sortie.BonAttribution.Ventes.ToList();
                FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockautoajoute(db, listesortie);

                db.DeleteObject(sortie);
                db.SaveChanges();

            }
        }
        private void AnnulerFacture(Facture fact = null)
        {
            var uid = dgvFacture.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas Annuler un document dont vous n'etes pas l'auteur");
                return;
            }

            var index = dgvFacture.GetSelectedIndex();
            if (index == null)
            {
                return;
            }

            var facture = ListFoundFacture.ElementAt(index.Value);

            var date = DateTime.Parse(dgvFacture.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("Vous ne pouvez pas modifier un document qui date de plus de " +
                    Tools.DayBeforeCanModify + " jours.");
                return;
            }
            if (fact == null)
            {
                fact = db.Factures.Single(p => p.ID == facture.ID);
            }
            var attrib = fact.BonAttribution;
            if (attrib.Etat != (int)EnEtatBonAttribution.EnInstance)
            {
                this.ShowWarning("vous ne pouvez pas modifier un bon chargé");
                return;
            }

            if (/*this.ConfirmWarning("Confirmer suppression payement N° " + facture.ID + "?")*/ true)
            {
                facture.Etat = (int)EnEtatFacture.Annulée;
                db.SaveChanges();

                var clientID = facture.ClientID;

                NsAdministration.FrMaintenance.GenerateSoldeClient(db, null, true, facture.Client);
                db.SaveChanges();
            }
        }
        private void AnnulerLivraison()
        {
            this.ShowWarning("vous ne pouvez pas Annuler une livraison");
            return;

            var uid = dgvLivraison.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas Annuler un document dont vous n'etes pas l'auteur");
                return;
            }


            var id = dgvLivraison.GetSelectedIndex();
            if (id == null)
            {
                return;
            }

            var livraison = ListFoundLivraison.ElementAt(dgvLivraison.GetSelectedIndex().Value);

            var date = DateTime.Parse(dgvLivraison.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }


            if (this.ConfirmWarning("Confirmer suppression Livraison N° " + livraison.ID + "?"))
            {
                livraison.BonAttribution.Etat = (int)EnEtatBonAttribution.Chargé;
                db.DeleteObject(livraison);

                db.SaveChanges();

            }
        }
        private void AnnulerRetour()
        {
            var uid = dgvRetour.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas Annuler un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvRetour.GetSelectedID();
            if (id == null)
            {
                return;
            }

            var selected = db.BonRetours.Single(p => p.ID == id);
            if (selected.FactureRetours.Count != 0)
            {

                {
                    this.ShowWarning("Ne peut pas supprimer un Bon de retour déja facturé, supprimez d'abord la facture de retour  ! ");
                    return;
                }
            }

            var date = DateTime.Parse(dgvRetour.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }


            if (this.ConfirmWarning("Confirmer annulation du Bon N° " + id + " ?"))
            {
                selected.Etat = (int)EnEtatBonAttribution.Annulé;

                /* var fact = selected.FactureRetours.SingleOrDefault();
                 if (fact != null)
                 {
                     fact.Etat = (int)EnEtatFacture.Annulée;
                 }*/
                db.SaveChanges();

                /*  var liv = selected.BonLivraisons.SingleOrDefault();
                  if (liv != null)
                  {
                      db.DeleteObject(liv);
                  }*/

                db.SaveChanges();
            }
        }
        private void AnnulerFactureRetour()
        {
            var uid = dgvFactureRetour.GetSelectedID("UID");

            if (uid != Tools.CurrentUserID && Tools.CurrentUser.GroupeID != 1)
            {
                this.ShowWarning("vous ne pouvez pas Annuler un document dont vous n'etes pas l'auteur");
                return;
            }

            var id = dgvFactureRetour.GetSelectedIndex();
            if (id == null)
            {
                return;
            }

            var facture = ListFoundFactureRetour.ElementAt(dgvFactureRetour.GetSelectedIndex().Value);

            var date = DateTime.Parse(dgvFactureRetour.GetSelectedCulumns("Date"));
            if (!date.AllowModifyDate())
            {
                this.ShowWarning("vous ne pouvez pas modifier un document qui date de plus de " + Tools.DayBeforeCanModify + " jours.");
                return;
            }


            if (this.ConfirmWarning("Confirmer suppression Facture Retour N° " + facture.ID + "?"))
            {
                facture.Client.Solde += facture.MontantTotalFacture;
                db.DeleteObject(facture);
                db.SaveChanges();

            }
        }

        private void imprimerBonLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImprimerLivraison();


        }
        private void imprimerLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImprimerFacture();


        }
        private void modifierLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifieFacture();
        }
        private void annulerLaFactureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnnulerFacture();
        }
        private void modifierLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifeLivraison();
        }
        private void annulerLivraisonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnnulerLivraison();

        }
        private void modifierBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifieSortie();
        }
        private void annulerBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnnulerSortie();
        }
        private void cbDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDocument.SelectedIndex != 0)
            {
                txtNoBon.Enabled = true;

            }
            else
            {
                txtNoBon.Enabled = false;
                txtNoBon.Text = "";
            }

            gbEtat.Visible = cbDocument.SelectedIndex == 1 || cbDocument.SelectedIndex == 5;

        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (tcDocument.SelectedTab == tpAttribution)
            {

                AnnulerAttribution();
            }
            if (tcDocument.SelectedTab == tpLivraison) AnnulerLivraison();
            if (tcDocument.SelectedTab == tpSortie) AnnulerSortie();
            if (tcDocument.SelectedTab == tpFacture) AnnulerFacture();
            if (tcDocument.SelectedTab == tpRetour) AnnulerRetour();
            if (tcDocument.SelectedTab == tpFactureRetour) AnnulerFactureRetour();


            Recherche();


        }
        private void btImprimer_Click(object sender, EventArgs e)
        {
            btImprimer.Enabled = false;

            if (tcDocument.SelectedTab == tpAttribution) ImprimerAttribution();
            if (tcDocument.SelectedTab == tpSortie) ImprimerSortie();
            if (tcDocument.SelectedTab == tpFacture) ImprimerFacture();
            if (tcDocument.SelectedTab == tpLivraison) ImprimerLivraison();
            if (tcDocument.SelectedTab == tpRetour) ImprimerRetour();
            if (tcDocument.SelectedTab == tpFactureRetour) ImprimerFactureRetour();


            btImprimer.Enabled = true;
        }
        private void btModifieBon_Click(object sender, EventArgs e)
        {
            if (tcDocument.SelectedTab == tpAttribution) ModifieAttribution();
            if (tcDocument.SelectedTab == tpSortie) ModifieSortie();
            if (tcDocument.SelectedTab == tpFacture) ModifieFacture();
            if (tcDocument.SelectedTab == tpLivraison) ModifeLivraison();
            if (tcDocument.SelectedTab == tpRetour) ModifieRetour();
            if (tcDocument.SelectedTab == tpFactureRetour) ModifieFactureRetour();

            Recherche();
        }

        private void modifierLeBonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModifieAttribution();


        }
        private void annulerLeBonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnnulerAttribution();


        }
        private void btFermer_Click(object sender, EventArgs e)
        {

            ComercialReportController.PrintJoureeVente(dtDate.Value);

        }
        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechAvancee.Checked;
            btRapport.Enabled = !chRechAvancee.Checked;
        }
        private void FrListBonAttribution_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
            UpdateTotal();

        }
        private void imprimerBonDeSortieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImprimerSortie();



        }

        private void FrListBon_Shown(object sender, EventArgs e)
        {
            //Recherche();
        }

        private void tcDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void txtClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (Client != null)
            {
                txtClient.Text = Client.Nom;
            }
        }

        public Client Client { get; set; }
    }
}
