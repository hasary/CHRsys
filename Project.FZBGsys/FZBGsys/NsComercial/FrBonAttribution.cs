﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrBonAttribution : Form
    {
        public BonCommande BonCommande { get; set; } // HERE
        ModelEntities db = new ModelEntities();
        public FrBonAttribution(int? BonID = null)
        {
            InitializeComponent();
            InitialiseControls();
            ListVente = new List<Vente>();
            if (BonID != null)
            {
                BonAttribution = db.BonAttributions.Single(p => p.ID == BonID);
                InputBon();
                chSuivre.Visible = false;
                btParcourrirClient.Enabled = false;
                txtClient.ReadOnly = true;

            }
        }

        public FrBonAttribution(bool isCommande, int CommandeID)
        {
            InitializeComponent();
            InitialiseControls();
            ListVente = new List<Vente>();
            this.BonCommande = db.BonCommandes.Single(p => p.ID == CommandeID);
            Client = BonCommande.Client;
            btParcourrirClient.Enabled = false;
            txtClient.ReadOnly = true;
            dtDate.MinDate = BonCommande.Date.Value.AddDays(-10);
            dtDate.MaxDate = DateTime.Now.AddDays(10);
            // dtDate.Value = BonCommande.Date.Value;
            LoadClient();

            CreateBon();
            BonAttribution.BonCommande = BonCommande;
            foreach (var produit in BonCommande.CommandeProduits)
            {
                BonAttribution.Ventes.Add(
                    new Vente
                {
                    FormatID = produit.FormatID,
                    GoutProduitID = produit.GoutProduitID,
                    Unite = produit.Unite,
                    TotalBouteilles = produit.TotalBouteille,
                    TotalFardeaux = produit.TotalFardeaux,
                    NombreFardeaux = produit.NombreFardeaux,
                    NombrePalettes = produit.NombrePalette,

                });

            }

            if (ListVente == null)
            {
                ListVente = new List<Vente>();
            }

            ListVente.AddRange(BonAttribution.Ventes);

            // this.CommandeNo = CommandeID;
            RefreshGrid();


        }

        private void InputBon()
        {
            dtDate.MinDate = BonAttribution.Date.Value.AddDays(-1);
            dtDate.Value = BonAttribution.Date.Value;
            Client = BonAttribution.Client;
            LoadClient();
            if (BonAttribution.ModePayement != null)
            {
                cbMode.SelectedIndex = BonAttribution.ModePayement.Value;
            }
            ListVente.AddRange(BonAttribution.Ventes);
            RefreshGrid();
            UpdateControls();

        }

        private void InitialiseControls()
        {
            //--------------------------- Gout Produit
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0 && p.UseVente == true).OrderBy(p => p.Nom).ToArray());
            cbGoutProduit.SelectedIndex = 0;
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------------------------------------------------
            /*
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            cbGoutProduit.AutoCompleteCustomSource = source;
            cbGoutProduit.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbGoutProduit.AutoCompleteSource = AutoCompleteSource.CustomSource;
            source.AddRange(db.GoutProduits.Select(p => p.Nom).ToArray());
            */



            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedIndex = 0;
            cbFormat.Items.RemoveAt(3);
            //--------------------------------------------------------------

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtClient.AutoCompleteCustomSource = acsc;
            txtClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtClient.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.Clients.ToList();

            if (results.Count > 0)
                foreach (var client in results)
                {
                    acsc.Add(client.Nom);
                }

            dtDate.Value = Tools.CurrentDateTime.Value.Date;
            dtDate.MaxDate = dtDate.Value;
            dtDate.MinDate = dtDate.Value.AddDays(-1);
            if (dtDate.MinDate.DayOfWeek == DayOfWeek.Friday)
            {
                dtDate.MinDate = dtDate.MinDate.AddDays(-1);
            }

            //--------------------------------------------------------------

        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {

            if (ListVente.Count == 0)
            {
                this.ShowWarning("La liste est vide, ajouter d'abord des éléments");
                return;
            }
            if (cbMode.SelectedIndex == 0)
            {
                //   this.ShowWarning("Selectionnez mode de payement!");
                //     return;
            }

            //      BonAttribution.ModePayement = cbMode.SelectedIndex;




            #region Validation Correspendance Commande
            if (BonCommande != null)
            {
                var formats = BonCommande.CommandeProduits.Select(p => p.FormatID).Distinct().ToList();
                var produits = BonCommande.CommandeProduits.ToList().Select(p => p.GoutProduit.TypeProduitID).Distinct().ToList();

                foreach (var formatID in formats)
                {
                    if (BonCommande.CommandeProduits.Where(p => p.FormatID == formatID).Sum(p => p.TotalBouteille) != BonAttribution.Ventes.Where(p => p.FormatID == formatID).Sum(p => p.TotalBouteilles))
                    {
                        this.ShowWarning("la liste des Produits n'est pas équivalente à la commande enregistré!");
                        return;
                    }
                }

                foreach (var produit in produits)
                {

                    if (BonCommande.CommandeProduits.ToList().Where(p => p.GoutProduit.TypeProduitID == produit).Sum(p => p.TotalBouteille) != BonAttribution.Ventes.ToList().Where(p => p.GoutProduit.TypeProduitID == produit).Sum(p => p.TotalBouteilles))
                    {
                        this.ShowWarning("la liste des Produits n'est pas équivalente à la commande enregistré!");
                        return;
                    }
                }


            }

            #endregion



            var totalMontant = ListVente.Sum(p => p.PrixUnitaire * p.TotalBouteilles);

            if (Client.ClientTypeID != 4 && Client.ClientTypeID != 3)
            {
                if (!this.Client.IsAutoriseCredit.Value)
                {
                    if (!this.ConfirmInformation("ce client n'est pas autorisé facturer en raison de dépassement du seuil, si vous continuer vous aller choisir un autre mode de payement."))
                    {
                        return;
                    }
                }

                if (totalMontant > Client.MaxCreditMontant + Client.Solde)
                {
                    if (!this.ConfirmInformation("Montant total dépasse le crédit autorisé pour ce client, voulez vous continuer? (vous aller etre appelé à forcer le payement)"))
                    {
                        return;
                    }


                }
            }

            if (ChSuperGrossist.Visible == true && ChSuperGrossist.Checked == true)
            {
                if (!this.ConfirmInformation("Ce client va être facturé avec un prix de super grossiste, voulez vous continuer? "))
                {
                    return;
                }
            }



            var currentTime = Tools.GetServerDateTime();

            if (BonAttribution.EntityState == EntityState.Added)
            {
                BonAttribution.InscriptionTime = Tools.GetServerDateTime();
                BonAttribution.InscriptionUID = Tools.CurrentUserID;
            }
            else
            {
                BonAttribution.Date = dtDate.Value;
                BonAttribution.Client = this.Client;
                BonAttribution.ModificationTime = currentTime;

            }


            btEnregistrer.Enabled = false;
            db.SaveChangeTry();
            try
            {

                // NsReports.Stock.StockReportController.PrintBonAttribution(BonAttribution.ID);

                var facture = BonAttribution.Factures.SingleOrDefault(p => p.Etat != (int)EnEtatFacture.Annulée);

                if (facture != null)
                {
                   new FZBGsys.NsComercial.FrFacture(facture.ID, MustSave: true).ShowDialog();

                }
                Dispose();
            }
            catch (Exception z)
            {
                this.ShowError(z.AllMessages("Impossible de creer le rapport"));
            }

            if (chSuivre.Checked)
            {
                try
                {
                    new FrFacture(BonAttribution.ID.ToString(), ChSuperGrossist.Checked).ShowDialog();
                }
                catch (Exception be)
                {
                    this.ShowError(be.AllMessages("Impossible de Poursuivre vers Payement"));
                }

            }

        }

        private void FrBonAttribution_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("\nEtes vous sure de vouloir fermer sans enregistrer ?"))
            {
                Dispose();
            }
        }



        public BonAttribution BonAttribution { get; set; }
        private void txtClient_TextChanged(object sender, EventArgs e)
        {
            /*var txt = txtClient.Text.Trim();
            var clin = db.Clients.Where(p => p.Nom == txt);

            if (clin.Count() == 1)
            {
                this.Client = clin.First();
                LoadClient();
                if (BonAttribution != null)
                {
                    BonAttribution.ClientID = Client.ID;

                }

            }
            else if (clin.Count() > 1)
            {
                Tools.ShowError("Client en double, selectionnez depuis la liste.");
                Client = null;
                txtAdress.Text = "";

            }
            else
            {

                Client = null;
                txtAdress.Text = "";
            }
            */
        }

        private void LoadClient()
        {
            txtClient.ReadOnly = false;
            if (Client != null)
            {
                txtClient.Text = Client.Nom;
                //cbMode.SelectedIndex = Client.Adresse;
                if (Client.ClientParent != null)
                {
                    txtParent.Text = Client.ClientParent.Nom;
                }

                if (true)
                {
                    panSolde.Visible = true;
                    if (Client.IsCompteBloque == null) Client.IsCompteBloque = false;

                    txtSolde.Text = Client.Solde.ToAffInt(NullIsZero: true);

                    var dernierVers = Client.Operations.Where(p => p.Sen == "C" && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client).OrderByDescending(p => p.Date).FirstOrDefault();
                    var dernierFact = Client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré).OrderByDescending(p => p.Date).FirstOrDefault();
                    var dernierVersMontant = Client.Operations.Where(p => p.Sen == "C" && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client && p.Date == dernierVers.Date).Sum(p => p.Montant);
                    var dernierFactMontantTTC = Client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré && p.Date == dernierFact.Date).Sum(p => p.MontantTTC);

                    labVersement.Text = (dernierVers != null) ? dernierVers.Date.Value.ToShortDateString() : "";
                    labChargement.Text = (dernierFact != null) ? dernierFact.Date.Value.ToShortDateString() : "";

                    labVersementDA.Text = (dernierVers != null) ? dernierVersMontant.Value.ToString("# ### ###") : "";
                    labChargementDA.Text = (dernierFact != null) ? dernierFactMontantTTC.Value.ToString("# ### ###") : "";
                    LabEtatCompte.Text = (Client.IsCompteBloque.Value) ? "bloqué" : "ouvert";

                }
                else
                {
                    panSolde.Visible = false;
                    panelComercial.Enabled = true;
                }

                if (Client.IsCompteBloque.Value)
                {
                    this.ShowInformation("Le Compte de ce client est bloqué en raison de dépassement de crédit autorisé et ou le délet de reglement.");
                    panelComercial.Enabled = false;
                }
                else
                {
                    panelComercial.Enabled = true;
                }
            }
            else
            {

                //txtAdress.ReadOnly = true;
            }
        }

        public List<Vente> ListVente { get; set; }
        public Client Client { get; set; }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (this.Client != null && this.Client.isExpire == true)
            {
                Tools.ShowError("Ce compte client est fermé (ancien dossier)");
                this.Client = null;
            }

            else if (this.Client != null)
            {/*
                var existAttribution = Client.BonAttributions.Where(p => p.Etat != (int)EnEtatBonAttribution.Livré && p.Etat != (int)EnEtatBonAttribution.Annulé);
                if (existAttribution.Count() != 0)
                {
                    Tools.ShowError("Vous ne pouvez pas inscrire un bon pour ce client car il pocède deja un bon en instance,\n veillez traiter l'ancein bon avant d'inscrir un nouveau!");
                    this.Client = null;
                }*/

                LoadClient();
                if (Client.ClientTypeID == 1)
                {
                    var exist_nonLiv = db.BonAttributions.Where(p => p.Client.ClientTypeID == 1 && (p.Etat == (int)EnEtatBonAttribution.EnInstance || p.Etat == (int)EnEtatBonAttribution.Chargé)).Count();
                    if (false /*exist_nonLiv != 0*/)
                    {
                        Tools.ShowInformation("Vous devez attendre les livraisons pour attributer à Super Grossi");
                        this.Client = null;
                        txtClient.Text = "";
                        txtSolde.Text = "";
                        txtParent.Text = "";
                        labChargement.Text = "";
                        labChargementDA.Text = "";
                        labVersement.Text = "";
                        labVersementDA.Text = "";
                    }
                }

                ChSuperGrossist.Visible = Client.ClientTypeID == 6;
                
            }

        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;

            }
            if (BonAttribution == null)
            {
                CreateBon(); // 
            }

            var newVente = new Vente();
            //  (int)EnUniteProduit.Fardeaux;
            newVente.BonAttribution = BonAttribution;
            newVente.Format = (Format)cbFormat.SelectedItem;
            newVente.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;

            if (radFardeaux.Checked)
            {
                newVente.Unite = (int)EnUniteProduit.Fardeaux;
                newVente.NombreFardeaux = txtNombre.Text.ParseToInt();
                newVente.TotalFardeaux = newVente.NombreFardeaux;
                newVente.TotalBouteilles = newVente.Format.BouteillesParFardeau * newVente.NombreFardeaux;
            }
            else // palette
            {
                newVente.Unite = (int)EnUniteProduit.Palettes;
                newVente.NombrePalettes = txtNombre.Text.ParseToInt();
                newVente.TotalFardeaux = newVente.Format.FardeauParPalette * newVente.NombrePalettes;
                newVente.TotalBouteilles = newVente.Format.BouteillesParFardeau * newVente.TotalFardeaux;
            }


            if (BonAttribution.Client.ClientTypeID == 4) // gratuit
            {
                newVente.PrixUnitaire = 0;
                newVente.PrixUnitaireHT = 0;
                newVente.isRemise = false;
            }

            else
            {
                var remise = 0;
                var
                   prix = db.PrixProduits.SingleOrDefault(p => p.FormatID == newVente.FormatID &&
                      p.GoutProduitID == newVente.GoutProduitID &&
                      p.TypeClient == BonAttribution.Client.ClientTypeID);

                if (prix == null)
                {
                    this.ShowError("Prix du produit manquant -> contacter l'administrateur");
                    return;
                }

                if (prix.IsRemisable.Value)
                {
                    newVente.isRemise = true;
                    newVente.PrixUnitaire = prix.PrixUnitaire - remise;
                    newVente.PrixUnitaireHT = decimal.Round((decimal)newVente.PrixUnitaire * 100 / 117, 4);
                    //  totalRemise += remise * vente.TotalBouteilles.Value;
                }
                else
                {
                    newVente.isRemise = false;
                    newVente.PrixUnitaire = prix.PrixUnitaire; // ici ca modify prix
                    newVente.PrixUnitaireHT = prix.PrixUnitaireHT;
                }
            }



            ListVente.Add(newVente);
            //   BonAttribution.Montant += newVente.PrixUnitaire * newVente.TotalBouteilles;

            db.AddToVentes(newVente);
            RefreshGrid();
            UpdateControls();
            btParcourrirClient.Enabled = ListVente.Count == 0;
        }

        private void UpdateControls()
        {
            //throw new NotImplementedException();

            txtNombre.Text = "";
            cbGoutProduit.SelectedIndex = 0;
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListVente.Select(p => new
            {
                ID = p.ID,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                //REF = ,
                Quantite = ((p.NombrePalettes != null) ? p.NombrePalettes : p.NombreFardeaux) + " " + p.Unite.ToUniteProduitTxt(),
                Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteilles + " Bouteilles",

            }).ToList();
            // throw new NotImplementedException();

            dgv.HideIDColumn();
            UpdateTotals();

        }

        private void UpdateTotals()
        {
            var pal = ListVente.Sum(p => p.NombrePalettes);
            var fard = ListVente.Sum(p => p.NombreFardeaux);
            var bout = ListVente.Sum(p => p.TotalBouteilles);
            var sfard = ListVente.Sum(p => p.TotalFardeaux);

            if (pal != 0)
            {
                labTotal.Text = pal.ToString() + " palettes";
            }

            if (fard != 0)
            {
                labTotal.Text += "   " + fard.ToString() + " Fardeaux    ";
            }


            labTotal.Text += " =(" + sfard + " Fardeaux / " + bout + " bouteilles).";

        }

        private void CreateBon()
        {
            var date = dtDate.Value.Date;
            var no = db.BonAttributions.Count(p => p.Date == date) + 1;

            BonAttribution = new BonAttribution()
            {
                Client = this.Client,
                Date = date,
                Etat = (int)EnEtatBonAttribution.EnInstance,
                NoFile = no


            };
        }

        private bool isValidAll()
        {
            var message = "";

            if (this.Client == null)
            {
                message += "Selectionnez un Client!";
            }

            if (cbGoutProduit.SelectedIndex == 0)
            {
                message += "\nSelectionnez un produit";
            }

            if (cbFormat.SelectedIndex == 0)
            {
                message += "\nSelectionner un Format";
            }

            if (txtNombre.Text.ParseToInt() == null)
            {
                message += "\nNombre Invalide !";
            }


            if (Tools.StrategieCDPanier/*radPalettes.Checked*/)
            {
                var produitID = ((GoutProduit)cbGoutProduit.SelectedItem).TypeProduitID;

                if (produitID == 1 && ((Format)cbFormat.SelectedItem).ID == 3) //chrea
                {
                    var totalCharg = txtNombre.Text.ParseToInt() * 540 + ((BonAttribution == null) ? 0 : BonAttribution.Ventes.Where(p => p.GoutProduit.TypeProduitID == 1).Sum(p => p.TotalBouteilles));
                    if (totalCharg > Client.PanierChrea)
                    {
                        message += "\nPanier Chréa Insuffisant !!";
                    }
                }

                if (produitID == 2 && ((Format)cbFormat.SelectedItem).ID == 3) //chrea pluoe
                {
                    var totalCharg = txtNombre.Text.ParseToInt() * 540 + ((BonAttribution == null) ? 0 : BonAttribution.Ventes.Where(p => p.GoutProduit.TypeProduitID == 2).Sum(p => p.TotalBouteilles));
                    if (totalCharg > Client.PanierOrangeChrea)
                    {
                        message += "\nPanier Chréa-Pulpe Insuffisant !!";
                    }
                }
            }
            if (!Existe()) message += "\nProduit " + ((GoutProduit)cbGoutProduit.SelectedItem).Nom + " " + ((Format)cbFormat.SelectedItem).Volume+" n'existe pas en stock!";

            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private bool Existe()
        {
            var format = ((Format)cbFormat.SelectedItem).Volume;
            var gout = ((GoutProduit)cbGoutProduit.SelectedItem).Nom;
            var NbrBoutille = txtNombre.Text.ParseToInt();
            DateTime date = DateTime.Now.Date;
            var existstock = db.StockProduitFiniManuals.Where( p => p.DateStock == date);
            if (radPalettes.Checked)
            existstock = existstock.Where(p => p.NombrePalettes >= NbrBoutille || p.isValid == true); 
            existstock = existstock.Where(p => p.NombreFardeaux >= NbrBoutille || p.NombrePalettes >= NbrBoutille || p.isValid == true);
            var valide = existstock.Where(p => p.GoutProduit.Nom == gout & p.Format.Volume == format).ToList();
            if (valide.Count == 0)
            { return false; }
            else
            return true;
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            if (BonAttribution != null)
            {
                BonAttribution.Date = dtDate.Value.Date;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.GetSelectedIndex();

            var selected = ListVente.ElementAt(index.Value);
            BonAttribution.Ventes.Remove(selected);
            ListVente.Remove(selected);
            //    BonAttribution.Montant -= selected.PrixUnitaire * selected.TotalBouteilles;
            db.DeleteObject(selected);
            RefreshGrid();
            UpdateControls();

        }

        private void txtClient_Leave(object sender, EventArgs e)
        {
            //   var clientNom = txtClient.Text.Trim().ToUpper();
            //   this.Client = db.Clients.SingleOrDefault(c => c.Nom == clientNom);
        }

        private void txtClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteilles();
        }

        private void UpdateTotalBouteilles()
        {
            var Nombre = txtNombre.Text.ParseToInt();
            var Format = (Format)cbFormat.SelectedItem;
            if (Nombre != null && cbFormat.SelectedIndex != 0)
            {
                if (radPalettes.Checked)
                {
                    txtTotalBouteilles.Text = (Nombre * Format.FardeauParPalette * Format.BouteillesParFardeau).ToString();
                }
                else
                {
                    txtTotalBouteilles.Text = (Nombre * Format.BouteillesParFardeau).ToString();
                }
            }
            else
            {
                txtTotalBouteilles.Text = "";
            }
        }

        private void txtNombre_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void cbGoutProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbGoutProduit.SelectedIndex > 0)
            {
                if (cbFormat.SelectedIndex > 0)
                {
                    txtNombre.Select();
                }
                else
                {
                    cbFormat.Select();
                }
            }
        }

        private void radFardeaux_CheckedChanged(object sender, EventArgs e)
        {
            txtNombre.Text = "";
        }
    }
}
