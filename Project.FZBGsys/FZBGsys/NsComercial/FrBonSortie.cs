﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace FZBGsys.NsComercial
{
    public partial class FrBonSortie : Form
    {
        ModelEntities db = new ModelEntities();
        public FrBonSortie(int? id = null)
        {
            InitializeComponent();
            ListVente = new List<Vente>();
            ListVenteDetail = new List<VenteDetail>();
            if (id != null)
            {

                InputBonSortie(id.Value);
            }

            txtNoBon.Select();
        }

        private void InputBonSortie(int id)
        {
            BonSortie = db.BonSorties.Single(p => p.ID == id);
            BonAttribution = BonSortie.BonAttribution;
            txtClient.Text = BonAttribution.Client.NomClientAndParent();
            txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();
            ListVenteDetail.AddRange(BonSortie.VenteDetails.ToList());
            UpdateListVentes();
            RefreshGrid();
        }


        private bool VerifNumeros()
        {
            string Numeros = txtNumeros.Text.Trim();
            // var Numeros = txtPlaques.Text.Trim().Replace(" ", "");

            if (Numeros == string.Empty)
            {
                return true;
            }
            Regex regNumeros = new Regex(@"\d+([_,]\d*)*");
            Match m = regNumeros.Match(Numeros);

            return m.Success;


        }





        private void btAfficher_Click(object sender, EventArgs e)
        {
            var id = txtNoBon.Text.ParseToInt();



            if (id != null)
            {
                LoadBonAttribution(id.Value);
            }
            else
            {
                this.ShowWarning("Numéro Bon Invalide");
            }

            

        }

        private void RefreshGrid()
        {
            dgvVente.DataSource = ListVente.Select(p => new
            {
                ID = p.ID,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                REF = p.Unite.ToUniteProduitTxt(),
                Nombre = (p.Unite == (int)EnUniteProduit.Palettes) ? p.NombrePalettes - p.VenteDetails.Sum(s => s.Nombre) : p.NombreFardeaux - p.VenteDetails.Sum(s => s.Nombre),

            }).ToList();
            dgvVente.HideIDColumn();

            //----------------------------------------------------------------

            dgvDetail.DataSource = ListVenteDetail.Select(p => new
            {
                ID = p.ID,
                Produit = p.Vente.GoutProduit.Nom,
                Format = p.Vente.Format.Volume,
                REF = p.Vente.Unite.ToUniteProduitTxt(),
                Production = p.DateProduction.Value.ToShortDateString(),
                Lot = p.Lot,
                Numeros = p.Numeros,
                Nombre = p.Nombre,



            }).ToList();


            dgvDetail.HideIDColumn(); 

        }

        private void LoadBonAttribution(int id)
        {
            this.BonAttribution = db.BonAttributions.SingleOrDefault(p => p.ID == id && p.Factures.Count == 1);
            
            if (BonAttribution == null)
            {
                this.ShowWarning("Numéro Bon introuvable");
                return;
            }

            
            if (BonAttribution.Etat == (int)EnEtatBonAttribution.Annulé || BonAttribution.Factures.First().Etat == (int)EnEtatFacture.Annulée )
            {
                this.ShowWarning("ce Bon est annulé");
                return;
            }


            if (BonAttribution.Factures.Count() != 1)
            {
                this.ShowWarning("ce Bon n'est pas payé, veillez inscrire le payement avant de charger");
                return;
            }


            if (BonAttribution.Etat == (int)EnEtatBonAttribution.Chargé)
            {
                if (BonAttribution.BonSorties.Count() != 0)
                {
                    if (this.ConfirmInformation("Un bon de sortie est déja établie pour ce bon d'attribution,\n voulez-vous l'imprimer?"))
                    {

                        FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonSortie(BonAttribution.BonSorties.First().ID);

                        return;
                    }
                    return;
                }

                // this.ShowWarning("Ce Bon d'attribution est " + BonAttribution.Etat.ToEtatBonAttributionTxt());
                // return;
            }

           

            txtClient.Text = BonAttribution.Client.NomClientAndParent();
            //  txtQuantite.Text = BonAttribution.Client.Adresse;
            txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();

            UpdateListVentes();

            RefreshGrid();

        }

        private void UpdateListVentes()
        {
            ListVente.Clear();
            var listAll = BonAttribution.Ventes.ToList();
            var list = listAll.Where(
                  p => (p.Unite == (int)EnUniteProduit.Palettes && p.NombrePalettes > p.VenteDetails.Sum(s => s.Nombre)) ||
                       (p.Unite == (int)EnUniteProduit.Fardeaux && p.NombreFardeaux > p.VenteDetails.Sum(s => s.Nombre))
                  ).ToList();


            ListVente.AddRange(list);
            btEnregistrer.Enabled = ListVente.Count == 0;
        }

        public List<Vente> ListVente { get; set; }
        public List<VenteDetail> ListVenteDetail { get; set; }
        private void FrBonSortie_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public BonAttribution BonAttribution { get; set; }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            ProcessSelected();

        }

        public int selectedNombreRestant { get; set; }
        private void ProcessSelected()
        {
            if (ListVente.Count == 0)
            {
                selectedNombreRestant = 0;
                selectedVente = null;
                return;
            }
            gbSelection.Enabled = true;

            var selectedIndex = dgvVente.GetSelectedIndex(false);
            if (selectedIndex != null)
            {
                this.selectedVente = ListVente.ElementAt(selectedIndex.Value);
                selectedNombreRestant = (selectedVente.Unite == (int)EnUniteProduit.Palettes) ?
                    selectedVente.NombrePalettes.Value - selectedVente.VenteDetails.Sum(s => s.Nombre).Value :
                    selectedVente.NombreFardeaux.Value - selectedVente.VenteDetails.Sum(s => s.Nombre).Value;
                UpdateControls();

            }
        }

        private void UpdateControls()
        {
            if (selectedVente != null)
            {
                txtSelectedProduit.Text = selectedVente.GoutProduit.Nom + " " + selectedVente.Format.Volume;
                txtQuantite.Text = selectedNombreRestant.ToString() + " " + selectedVente.Unite.ToUniteProduitTxt();

                panPalette.Visible = selectedVente.Unite == (int)EnUniteProduit.Palettes;
                panFardeaux.Visible = selectedVente.Unite == (int)EnUniteProduit.Fardeaux;
            }


            txtNombreFardeaux.Text = "";
            txtNombrePalette.Text = "";

            cbLot.SelectedIndex = 0;
            txtNumeros.Text = "";
            dtDateProduction.Value = DateTime.Now.Date;

            //  if (selectedVente.DateProduction != null) dtDateProduction.Value = selectedVente.DateProduction.Value.Date;
            //  if (selectedVente.Lot != null) cbLot.SelectedItem = selectedVente.Lot;



        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {

            if (dgvVente.Rows.Count != 0)
            {
                this.ShowWarning("Bon de sortie incomplet términer la saisie avant d'enregistrer");
            }
            else
            {

                try
                {
                    foreach (var vente in ListVente)
                    {
                        vente.BonSortie = BonSortie;
                    }
                    var currentTime = Tools.GetServerDateTime();
                    BonAttribution.Etat = (int)EnEtatBonAttribution.Chargé;
                    if (BonSortie.EntityState == EntityState.Added)
                    {
                        BonSortie.InscriptionTime = currentTime;
                        BonSortie.InscriptionUID = Tools.CurrentUserID;
                    }
                    else
                    {
                        BonSortie.ModificationTime = currentTime;
                    }

                    if (BonAttribution.BonCommande != null)
                    {
                        BonAttribution.BonCommande.EtatCommande = (int)EnEtatBonCommande.Solde;
                        
                    }

                    var listevente = BonSortie.Ventes.ToList();
                    FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockautoajoute(currentTime, db, listevente);


                    db.SaveChanges();
                    btEnregistrer.Enabled = false;

                    FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonSortie(BonSortie.ID);


                    Dispose();
                }
                catch (Exception ee)
                {

                    this.ShowError(ee.AllMessages("Impossible d'enregistrer"));
                }
            }
        }
        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {
                if (BonSortie == null)
                {
                    this.BonSortie = new BonSortie()
                    {
                        Date = BonAttribution.Date,
                        ClientNom = BonAttribution.Client.NomClientAndParent()
                    };
                    BonSortie.BonAttribution = BonAttribution;
                }

                selectedVente.BonSortie = BonSortie;
                var venteDetail = new VenteDetail();
                venteDetail.Vente = selectedVente;

                venteDetail.DateProduction = dtDateProduction.Value.Date;
                venteDetail.Lot = cbLot.SelectedItem.ToString();
                venteDetail.BonSortie = BonSortie;
                if (selectedVente.Unite == (int)EnUniteProduit.Palettes)
                {
                    venteDetail.Numeros = txtNumeros.Text.Trim();
                    venteDetail.Nombre = venteDetail.Numeros.ToNombrePlalettes();
                    venteDetail.TotalBouteilles = venteDetail.Nombre * venteDetail.Vente.Format.FardeauParPalette * venteDetail.Vente.Format.BouteillesParFardeau;
                }
                else // farderaux
                {
                    venteDetail.Nombre = txtNombreFardeaux.Text.Trim().ParseToInt();
                    venteDetail.TotalBouteilles = venteDetail.Nombre * venteDetail.Vente.Format.BouteillesParFardeau;
                }

                ListVenteDetail.Add(venteDetail);

                UpdateListVentes();
                // ProcessSelected();

                RefreshGrid();
            }

        }

        private bool isValidAll()  // change this
        {

            string message = "";

            if (selectedVente.Unite == (int)EnUniteProduit.Palettes)
            {
                var Numeros = txtNumeros.Text.Trim();
                var nombre = Numeros.ToNombrePlalettes();
                if (nombre == 0)
                {
                    message += "Numéros palettes invalide!";
                    //  return false;
                }

                else if (nombre > selectedNombreRestant)
                {
                    message += "\nNombre de palettes est supperieur pas la quantite restante";
                    // return false;
                }
                else
                {
                    var date = dtDateProduction.Value.Date;
                    var produit = selectedVente.GoutProduitID;
                    var format = selectedVente.FormatID;
                    //var lot = selectedVente.VenteDetails

                    var existing = ListVenteDetail.Where(p => p.Vente.FormatID == format && p.Vente.GoutProduitID == produit && p.DateProduction == date).ToList();

                    foreach (var exit in existing)
                    {
                        if (exit.Numeros.ContainsNumeros(Numeros))
                        {
                            message += "\nNuméros exist déja dans la liste !";
                            break;
                        }
                    }
                }
            }
            else
            {
                var nombre = txtNombreFardeaux.Text.Trim().ParseToInt();
                if (nombre == null)
                {
                    message += "\nNombre Fardeaux Invalide!";
                }
                else if (nombre > selectedNombreRestant)
                {
                    message += "\nNombre Fardeaux supperieur au quantite resntante !";
                }
            }

            if (cbLot.SelectedIndex == 0)
            {
                message += "\nSelectionnez un Lot";
            }


            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }


            return true;

        }

        public Vente selectedVente { get; set; }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgvDetail.GetSelectedIndex();
            var selectedDetails = ListVenteDetail.ElementAt(selectedIndex.Value);

            db.DeleteObject(selectedDetails);
            ListVenteDetail.Remove(selectedDetails);
            UpdateListVentes();
            RefreshGrid();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("Confirmer Fermer sans Enregistrer?"))
            {
                Dispose();
            }
        }

        private void txtNumeros_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (ch == '-')
            {
                e.KeyChar = '_';
                ch = '_';
            }
            if (txtNumeros.Text.Trim().EndsWith("_") || txtNumeros.Text.Trim().EndsWith(","))
            {
                if (!Char.IsDigit(ch) && ch != 8) e.Handled = true;

            }

            else
            {
                if (!Char.IsDigit(ch) && ch != 8 && ch != Char.Parse("_") && ch != Char.Parse(","))
                {
                    e.Handled = true;
                }
            }
        }

        private void txtNumeros_Leave(object sender, EventArgs e)
        {



        }

        private void txtNumeros_TextChanged(object sender, EventArgs e)
        {
            if (VerifNumeros())
            {
                txtNombrePalette.Text = txtNumeros.Text.Trim().ToNombrePlalettes().ToString();
            }
            else
            {
                txtNombrePalette.Text = "";
            }
        }




        public BonSortie BonSortie { get; set; }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void gbSelection_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void dtDateProduction_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btParcourrirNumeros_Click(object sender, EventArgs e)
        {

            // ------ get numeros from Palettes Dispo and group them into one Numeros string
            var palettesDispoNumeros = "1_200";
            var paletteSelectedNumeors = "";

            paletteSelectedNumeors = FZBGsys.NsStock.FrNumerosPalettes.SelectPalettesDialog(
               dtDateProduction.Value.Date,
               selectedVente.GoutProduit,
               palettesDispoNumeros,
               paletteSelectedNumeors);


            txtNumeros.Text = paletteSelectedNumeors;



        }
    }
}
