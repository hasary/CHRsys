﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrSelectDateClientSatisfaction : Form
    {
        ModelEntities db = new ModelEntities();
        public FrSelectDateClientSatisfaction()
        {
            InitializeComponent();

            InitialiseWeeks();


            InitialiseDate();
            SelectionDone = false;

            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {
            string[] monthText = { "Ja", "" };
            Dictionary<string, string> months = new Dictionary<string, string>();

            DateTime fin = DateTime.Now;
            DateTime debut = DateTime.Parse("2012-05-01");

            for (DateTime i = debut; i < fin; i = i.AddMonths(1))
            {
                // months.Add(i.Month,
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dtAnnee.Visible = raAnnuel.Checked;
            panelPeriode.Visible = raMensuel.Checked;
            if (panelPeriode.Visible)
            {
                InitialiseWeeks();
            }
            else
            {
                From = DateTime.Parse(dtYear.Value.Year.ToString() + "-01-01");
                To = DateTime.Parse(dtYear.Value.Year.ToString() + "-12-31");
            }
            UpdateClients();
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = raMensuel.Checked;
            if (panelPeriode.Visible)
            {
                InitialiseWeeks();
            }
            else
            {
                From = DateTime.Parse(dtYear.Value.Year.ToString() + "-01-01");
                To = DateTime.Parse(dtYear.Value.Year.ToString() + "-12-31");
            }
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            //  dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime From { get; set; }
        public static DateTime To { get; set; }
        public static bool SelectionDone = false;
        private void button1_Click(object sender, EventArgs e)
        {
            int all = 0;
            int ClientID = 0;
            int Mode = 0;
            if (cbClient.SelectedIndex == 0)
            {
                all = 1;
            }
            else
            {
                all = 0;
                ClientID = ((Client)cbClient.SelectedItem).ID;
            }

            if (chFeilleVide.Checked)
            {
                Mode = 0;
            }
            else
            {
                Mode = 1;
            }




            FZBGsys.NsReports.Comercial.ComercialReportController.PrintSatisfactionClient(From, To, all, ClientID, Mode);
            if (raAnnuel.Checked && cbClient.SelectedIndex == 0 && !chFeilleVide.Checked)
            {
                FZBGsys.NsReports.Comercial.ComercialReportController.PrintSatisfactionClientAnalyse(From, To);

            }


        }






        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }


        public Fournisseur Fournisseur { get; set; }

        private void FrSelectDateFournisseur_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void dtYear_ValueChanged(object sender, EventArgs e)
        {
            InitialiseWeeks();
        }

        private void InitialiseWeeks()
       /* {
            this.IsInitializing = true;
            cbWeek.Items.Clear();

            var datebegin = dtYear.Value.Year.ToString() + "-01-01";
            var dateend = dtYear.Value.Year.ToString() + "-12-31";
            DateTime date = DateTime.Parse(datebegin);
            this.weeks = new List<DateTime?>();

            int i = 0;
            while (date.CompareTo(DateTime.Parse(dateend)) < 0)
            {
                i++;
                var enddate = date.AddDays(6);
                var existes = db.ClientSatisfactions.FirstOrDefault(p => p.WeekNo == i);
                cbWeek.Items.Add("S-" + (i).ToString().PadLeft(2, '0') + " :    " + date.ToString("dd  MMM") + "  au  " + date.AddDays(6).ToString("dd  MMM"));
                weeks.Add(date);
               /* if (existes == null)
                {
                    chFeilleVide.Checked = true;
                    chFeilleVide.Enabled = false;
                }
                else
                {
                    chFeilleVide.Checked = false;
                    chFeilleVide.Enabled = true;
                }
              *//*
                date = date.AddDays(7);
            }
            this.IsInitializing = false;
            cbWeek.SelectedIndex = 0;
        }*/
        {
            this.IsInitializing = true;
            cbWeek.Items.Clear();

            var datebegin = dtYear.Value.Year.ToString() + "-01-01";
            var dateend = dtYear.Value.Year.ToString() + "-12-31";
            DateTime date = DateTime.Parse(datebegin);
            this.weeks = new List<DateTime?>();

            int i = 0;
            while (date.CompareTo(DateTime.Parse(dateend)) < 0)
            {
                i++;
                var enddate = date.AddMonths(3).AddDays(-1);
                var existes = db.ClientSatisfactions.FirstOrDefault(p => p.WeekNo == i);
                cbWeek.Items.Add("S-" + (i).ToString().PadLeft(2, '0') + " :    " + date.ToString("dd  MMM") + "  au  " + date.AddMonths(3).AddDays(-1).ToString("dd  MMM"));
                weeks.Add(date);
                /* if (existes == null)
                 {
                     chFeilleVide.Checked = true;
                     chFeilleVide.Enabled = false;
                 }
                 else
                 {
                     chFeilleVide.Checked = false;
                     chFeilleVide.Enabled = true;
                 }
               */
                date = date.AddMonths(3);
            }
            this.IsInitializing = false;
            cbWeek.SelectedIndex = 0;
        }


        public List<DateTime?> weeks { get; set; }

        private void cbWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!this.IsInitializing)
            {
                From = weeks.ElementAt(cbWeek.SelectedIndex).Value;
                To = weeks.ElementAt(cbWeek.SelectedIndex).Value.AddMonths(3).AddDays(-1);
                UpdateClients(); 
            }
        }
        public bool IsInitializing { get; set; }
        private void UpdateClients()
        {
            List<Client> clients = db.BonLivraisons.Where(p => p.DateLivraison >= From && p.DateLivraison <= To && p.BonAttribution.Client.ClientTypeID < 3).Select(p => p.BonAttribution.Client).ToList();
            cbClient.Items.Clear();
           
            clients = clients.Distinct().ToList();
           
            cbClient.Items.Add(new Client { ID = 0, Nom = "[Tout les clients]" });
            cbClient.Items.AddRange(clients.ToArray());
            cbClient.DisplayMember = "Nom";
            cbClient.ValueMember = "ID";
            cbClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbClient.SelectedIndex = 0;

           
        }

        private void dtAnnee_ValueChanged(object sender, EventArgs e)
        {
            From = DateTime.Parse(dtYear.Value.Year.ToString() + "-01-01");
            To = DateTime.Parse(dtYear.Value.Year.ToString() + "-12-31");
            UpdateClients();
        }
    }
}
