
﻿using FZBGsys.NsReports.Comercial;
using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrListVentes : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListVentes()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            cbProduit.Items.Add(new GoutProduit { ID = 0, Nom = "[Tout]" });
            cbProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbProduit.SelectedIndex = 0;
            cbProduit.ValueMember = "ID";
            cbProduit.DisplayMember = "Nom";
            cbProduit.DropDownStyle = ComboBoxStyle.DropDownList;

            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "[Tout]" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.SelectedIndex = 0;
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;

            //----------------------- 
            cbPeriode.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDate.DropDownStyle = ComboBoxStyle.DropDownList;
            cbDate.SelectedIndex = 1;
            cbPeriode.SelectedIndex = 1;

            var Ct = db.Clients.Where(p => p.IsCompteBloque == false && p.isExpire == false).Select(p => p.Nom).Distinct();
            AutoCompleteStringCollection source = new AutoCompleteStringCollection();
            txtClient.AutoCompleteCustomSource = source;
            txtClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtClient.AutoCompleteSource = AutoCompleteSource.CustomSource;

            foreach (var item in Ct)
            {
                source.Add(item);
            }


        }

        private void cbDocument_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNoBon.Text = "";
            txtNoBon.Enabled = cbDocument.SelectedIndex != 0;


        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
        }


        private void Recherche()
        {
            labFilter.Text = "";
            //IEnumerable<Vente> resultVente = db.Ventes;
            IQueryable<Vente> resultVente = db.Ventes;


            if (!chRechAvancee.Checked)
            {

                var date = dtDate.Value.Date;
                //var date = DateTime.Now.Date;
             
                
                if (cbDate.SelectedIndex == (int)EnTypeBon.Bon_Attribution)
                {
                    //resultVente = resultVente.Where(p => p.BonAttributionID != null && p.BonAttribution.Date == date);
                    resultVente = resultVente.Where(p => p.BonAttribution.Date == date);
                    labFilter.Text = "attributions du";
                }
                if (cbDate.SelectedIndex == (int)EnTypeBon.Bon_Sortie)
                {
                    resultVente = resultVente.Where(p => p.BonSortie != null && p.BonSortie.Date == date);
                    labFilter.Text = "sorties du";
                }
                if (cbDate.SelectedIndex == (int)EnTypeBon.Bon_Livraison)
                {
                    resultVente = resultVente.Where(p => p.BonLivraison != null && p.BonLivraison.DateLivraison == date);
                    labFilter.Text = "livraisons du";
                }



                labFilter.Text += " " + dtDate.Value.ToShortDateString();
            }
            else
            {
                var dateDu = dtDateDu.Value.Date;
                var dateAu = dtDateAu.Value.Date;


                labFilter.Text = "";

                if (cbPeriode.SelectedIndex == (int)EnTypeBon.Bon_Attribution)
                {
                    resultVente = resultVente.Where(p => p.BonAttribution.Date >= dateDu && p.BonAttribution.Date <= dateAu);
                    labFilter.Text = "Attributions du " + dateDu.ToShortDateString() + " au " + dateAu.ToShortDateString();
                }
                if (cbPeriode.SelectedIndex == (int)EnTypeBon.Bon_Sortie)
                {
                    resultVente = resultVente.Where(p => p.BonSortie != null && p.BonSortie.Date >= dateDu && p.BonSortie.Date <= dateAu);
                    labFilter.Text = "Sorties du " + dateDu.ToShortDateString()    + " au " + dateAu.ToShortDateString();
                }
                if (cbPeriode.SelectedIndex == (int)EnTypeBon.Bon_Livraison)
                {
                    resultVente = resultVente.Where(p => p.BonLivraison != null && p.BonLivraison.DateLivraison >= dateDu && p.BonLivraison.DateLivraison <= dateAu);
                    labFilter.Text = "Livraisons du " + dateDu.ToShortDateString() + " au " + dateAu.ToShortDateString();
                }

                var ID = txtNoBon.Text.ParseToInt();
                if (ID != null)
                {
                    if (cbDocument.SelectedIndex == (int)EnTypeBon.Bon_Attribution)
                    {
                        resultVente = resultVente.Where(p => p.BonAttribution.ID == ID);
                        labFilter.Text = "Bon d'Attribution N° " + ID;
                    }
                    if (cbDocument.SelectedIndex == (int)EnTypeBon.Bon_Sortie)
                    {
                        resultVente = resultVente.Where(p => p.BonSortieID == ID);
                        labFilter.Text = "Bon de Sortie N° " + ID;
                    }
                    if (cbDocument.SelectedIndex == (int)EnTypeBon.Bon_Livraison)
                    {
                        resultVente = resultVente.Where(p => p.BonLivraisonID == ID);
                        labFilter.Text = "Bon de Livraison N° " + ID;
                    }
                }

                var client = txtClient.Text.Trim();

                if (client != "")
                {
                    resultVente = resultVente.Where(p => p.BonAttribution.Client.Nom.Contains(client));
                    labFilter.Text += " Client \"" + client + "\"";
                }


                if (!chEnInstance.Checked)
                {
                    resultVente = resultVente.Where(p => p.BonAttribution.Etat != (int)EnEtatBonAttribution.EnInstance);
                    //  labFilter.Text += " Ventes en Instance";
                }

                if (!chAnnulé.Checked)
                {
                    resultVente = resultVente.Where(p => p.BonAttribution.Etat != (int)EnEtatBonAttribution.Annulé);
                    // labFilter.Text += " Ventes annulées";
                }

                if (!chSortie.Checked)
                {
                    resultVente = resultVente.Where(p => p.BonAttribution.Etat != (int)EnEtatBonAttribution.Chargé);
                }

                if (!chEnInstance.Checked)
                {
                    resultVente = resultVente.Where(p => p.BonAttribution.Etat != (int)EnEtatBonAttribution.Livré);
                    //   labFilter.Text += " Ventes en Instance";
                }

                if (cbProduit.SelectedIndex != 0)
                {
                    var produit = (GoutProduit)cbProduit.SelectedItem;
                    resultVente = resultVente.Where(p => p.GoutProduitID == produit.ID);
                    labFilter.Text += " " + produit.Nom;
                }
                labFilter.Text += " Etat: ";
                if (chEnInstance.Checked && chLivré.Checked && chSortie.Checked)
                {
                    if (chAnnulé.Checked)
                    {
                        labFilter.Text += "tout (annulés inclues)";
                    }
                    else
                    {
                        labFilter.Text += "tout";
                    }
                }
                else
                {
                    if (chEnInstance.Checked) labFilter.Text += " En instante,";
                     if (chLivré.Checked) labFilter.Text += " Livré,";
                    if (chAnnulé.Checked) labFilter.Text += " Annulé,";
                    if (chSortie.Checked) labFilter.Text += " Sortie,";

                }


                if (cbFormat.SelectedIndex != 0)
                {
                    var format = (Format)cbFormat.SelectedItem;
                    resultVente = resultVente.Where(p => p.FormatID == format.ID);
                    labFilter.Text += " " + format.Volume;
                }



            }

            ListFoundVentes = resultVente.ToList();
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            dgvVente.DataSource = ListFoundVentes.Select(p => new
            {
                ID = p.ID,
                Client = p.BonAttribution.Client.NomClientAndParent(),
                Produit = p.GoutProduit.Nom + " " + p.Format.Volume,
                Quantite = ((p.NombrePalettes == null) ? "" : p.NombrePalettes.ToString().PadLeft(2,'0') + " P  ") + ((p.NombreFardeaux == null) ? "" : p.NombreFardeaux + " F  "),
                Total = p.TotalBouteilles,
                Attribution = "N° " + p.BonAttributionID + " du " + p.BonAttribution.Date.Value.ToShortDateString(),
                Livraison = (p.BonLivraison != null) ? "N° " + p.BonLivraison.ID + " du " + p.BonLivraison.DateLivraison.Value.ToShortDateString() : p.BonAttribution.Etat.ToEtatBonAttributionTxt(),


            }).ToList();
            dgvVente.HideIDColumn();
            UpdateTotals();
        }

        private void UpdateTotals()
        {
            var pal = ListFoundVentes.Sum(p => p.NombrePalettes);
            var fard = ListFoundVentes.Sum(p => p.NombreFardeaux);
            var bout = ListFoundVentes.Sum(p => p.TotalBouteilles);
            var sfard = ListFoundVentes.Sum(p => p.TotalFardeaux);

            if (pal != 0)
            {
                labTotal.Text = pal.ToString() + " palettes";
            }

            if (fard != 0)
            {
                labTotal.Text += "   " + fard.ToString() + " Fardeaux    ";
            }


            labTotal.Text += " =(" + sfard + " Fardeaux / " + bout + " bouteilles).";

        }

        private void FrListVentes_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }


        public List<Vente> ListFoundVentes { get; set; }

        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechAvancee.Checked;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            btImprimer.Enabled = false;
            if (!chRechAvancee.Checked)
            {

                ComercialReportController.PrintVenteJournee(dtDate.Value);

            }
            else
            {
                if (txtNoBon.Text.Trim() != "")
                {
                    var id = txtNoBon.Text.Trim().ParseToInt();
                    if (id != null)
                    {
                        if (cbDocument.SelectedIndex == (int)EnTypeBon.Bon_Attribution)

                            ComercialReportController.PrintBonAttribution(id.Value);

                        if (cbDocument.SelectedIndex == (int)EnTypeBon.Bon_Livraison)
                            ComercialReportController.PrintBonLivraison(id.Value);

                        if (cbDocument.SelectedIndex == (int)EnTypeBon.Bon_Sortie)
                            ComercialReportController.PrintBonSortie(id.Value);


                        if (cbDocument.SelectedIndex == (int)EnTypeBon.Facture)
                        {
                            var facture = db.Factures.Single(p => p.ID == id);

                            ComercialReportController.PrintFacture(

                                id.Value,

                                facture.MontantTotalFacture.ToString(),
                                facture.MontantTotalRemise.ToString());
                        }
                    }
                }
                else
                {


                    ComercialReportController.PrintCustomVentes(ListFoundVentes, labFilter.Text, db); // this will be used in annother form

                    btImprimer.Enabled = true;
                   
                }
            }
        }

      

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
