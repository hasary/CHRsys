﻿namespace FZBGsys.NsComercial
{
    partial class FrBonSortie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDateBon = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btAfficher = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.gbSelection = new System.Windows.Forms.GroupBox();
            this.panFardeaux = new System.Windows.Forms.Panel();
            this.txtNombreFardeaux = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panPalette = new System.Windows.Forms.Panel();
            this.btParcourrirNumeros = new System.Windows.Forms.Button();
            this.txtNombrePalette = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNumeros = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbLot = new System.Windows.Forms.ComboBox();
            this.txtSelectedProduit = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtDateProduction = new System.Windows.Forms.DateTimePicker();
            this.btAjouter = new System.Windows.Forms.Button();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.dgvVente = new System.Windows.Forms.DataGridView();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.btEnlever = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.gbSelection.SuspendLayout();
            this.panFardeaux.SuspendLayout();
            this.panPalette.SuspendLayout();
            this.xpannel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDateBon);
            this.groupBox1.Controls.Add(this.txtClient);
            this.groupBox1.Controls.Add(this.txtNoBon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btAfficher);
            this.groupBox1.Location = new System.Drawing.Point(16, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(311, 130);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bon d\'Attribution";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 68);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 97);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Client:";
            // 
            // txtDateBon
            // 
            this.txtDateBon.Location = new System.Drawing.Point(64, 64);
            this.txtDateBon.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateBon.Name = "txtDateBon";
            this.txtDateBon.ReadOnly = true;
            this.txtDateBon.Size = new System.Drawing.Size(128, 22);
            this.txtDateBon.TabIndex = 2;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(64, 94);
            this.txtClient.Margin = new System.Windows.Forms.Padding(4);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(237, 22);
            this.txtClient.TabIndex = 2;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(64, 34);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(128, 22);
            this.txtNoBon.TabIndex = 2;
            this.txtNoBon.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "N°";
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(200, 32);
            this.btAfficher.Margin = new System.Windows.Forms.Padding(4);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(103, 28);
            this.btAfficher.TabIndex = 0;
            this.btAfficher.Text = "Afficher >>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(89, 75);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Quantite:";
            // 
            // txtQuantite
            // 
            this.txtQuantite.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantite.Location = new System.Drawing.Point(164, 71);
            this.txtQuantite.Margin = new System.Windows.Forms.Padding(4);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.ReadOnly = true;
            this.txtQuantite.Size = new System.Drawing.Size(132, 22);
            this.txtQuantite.TabIndex = 2;
            // 
            // gbSelection
            // 
            this.gbSelection.Controls.Add(this.label3);
            this.gbSelection.Controls.Add(this.panFardeaux);
            this.gbSelection.Controls.Add(this.panPalette);
            this.gbSelection.Controls.Add(this.txtQuantite);
            this.gbSelection.Controls.Add(this.label4);
            this.gbSelection.Controls.Add(this.cbLot);
            this.gbSelection.Controls.Add(this.txtSelectedProduit);
            this.gbSelection.Controls.Add(this.label6);
            this.gbSelection.Controls.Add(this.label7);
            this.gbSelection.Controls.Add(this.dtDateProduction);
            this.gbSelection.Controls.Add(this.btAjouter);
            this.gbSelection.Enabled = false;
            this.gbSelection.Location = new System.Drawing.Point(13, 160);
            this.gbSelection.Margin = new System.Windows.Forms.Padding(4);
            this.gbSelection.Name = "gbSelection";
            this.gbSelection.Padding = new System.Windows.Forms.Padding(4);
            this.gbSelection.Size = new System.Drawing.Size(311, 307);
            this.gbSelection.TabIndex = 16;
            this.gbSelection.TabStop = false;
            this.gbSelection.Text = "Selection:";
            this.gbSelection.Enter += new System.EventHandler(this.gbSelection_Enter);
            // 
            // panFardeaux
            // 
            this.panFardeaux.Controls.Add(this.txtNombreFardeaux);
            this.panFardeaux.Controls.Add(this.label9);
            this.panFardeaux.Location = new System.Drawing.Point(11, 153);
            this.panFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.panFardeaux.Name = "panFardeaux";
            this.panFardeaux.Size = new System.Drawing.Size(287, 53);
            this.panFardeaux.TabIndex = 19;
            // 
            // txtNombreFardeaux
            // 
            this.txtNombreFardeaux.Location = new System.Drawing.Point(128, 20);
            this.txtNombreFardeaux.Margin = new System.Windows.Forms.Padding(4);
            this.txtNombreFardeaux.Name = "txtNombreFardeaux";
            this.txtNombreFardeaux.Size = new System.Drawing.Size(107, 22);
            this.txtNombreFardeaux.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(56, 23);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Nombre:";
            // 
            // panPalette
            // 
            this.panPalette.Controls.Add(this.btParcourrirNumeros);
            this.panPalette.Controls.Add(this.txtNombrePalette);
            this.panPalette.Controls.Add(this.label8);
            this.panPalette.Controls.Add(this.txtNumeros);
            this.panPalette.Controls.Add(this.label5);
            this.panPalette.Location = new System.Drawing.Point(9, 146);
            this.panPalette.Margin = new System.Windows.Forms.Padding(4);
            this.panPalette.Name = "panPalette";
            this.panPalette.Size = new System.Drawing.Size(301, 70);
            this.panPalette.TabIndex = 19;
            // 
            // btParcourrirNumeros
            // 
            this.btParcourrirNumeros.Location = new System.Drawing.Point(245, 1);
            this.btParcourrirNumeros.Margin = new System.Windows.Forms.Padding(4);
            this.btParcourrirNumeros.Name = "btParcourrirNumeros";
            this.btParcourrirNumeros.Size = new System.Drawing.Size(52, 28);
            this.btParcourrirNumeros.TabIndex = 2;
            this.btParcourrirNumeros.Text = "...";
            this.btParcourrirNumeros.UseVisualStyleBackColor = true;
            this.btParcourrirNumeros.Click += new System.EventHandler(this.btParcourrirNumeros_Click);
            // 
            // txtNombrePalette
            // 
            this.txtNombrePalette.Location = new System.Drawing.Point(131, 36);
            this.txtNombrePalette.Margin = new System.Windows.Forms.Padding(4);
            this.txtNombrePalette.Name = "txtNombrePalette";
            this.txtNombrePalette.ReadOnly = true;
            this.txtNombrePalette.Size = new System.Drawing.Size(107, 22);
            this.txtNombrePalette.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 39);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Nombre:";
            // 
            // txtNumeros
            // 
            this.txtNumeros.Location = new System.Drawing.Point(131, 4);
            this.txtNumeros.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumeros.Name = "txtNumeros";
            this.txtNumeros.Size = new System.Drawing.Size(107, 22);
            this.txtNumeros.TabIndex = 0;
            this.txtNumeros.TextChanged += new System.EventHandler(this.txtNumeros_TextChanged);
            this.txtNumeros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeros_KeyPress);
            this.txtNumeros.Leave += new System.EventHandler(this.txtNumeros_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 7);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Numero Palettes:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Produit:";
            // 
            // cbLot
            // 
            this.cbLot.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbLot.FormattingEnabled = true;
            this.cbLot.Items.AddRange(new object[] {
            "",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.cbLot.Location = new System.Drawing.Point(133, 224);
            this.cbLot.Margin = new System.Windows.Forms.Padding(4);
            this.cbLot.Name = "cbLot";
            this.cbLot.Size = new System.Drawing.Size(105, 24);
            this.cbLot.TabIndex = 5;
            // 
            // txtSelectedProduit
            // 
            this.txtSelectedProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSelectedProduit.Location = new System.Drawing.Point(72, 38);
            this.txtSelectedProduit.Margin = new System.Windows.Forms.Padding(4);
            this.txtSelectedProduit.Name = "txtSelectedProduit";
            this.txtSelectedProduit.ReadOnly = true;
            this.txtSelectedProduit.Size = new System.Drawing.Size(225, 22);
            this.txtSelectedProduit.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 112);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Date Production:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(92, 226);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Lot:";
            // 
            // dtDateProduction
            // 
            this.dtDateProduction.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateProduction.Location = new System.Drawing.Point(139, 110);
            this.dtDateProduction.Margin = new System.Windows.Forms.Padding(4);
            this.dtDateProduction.Name = "dtDateProduction";
            this.dtDateProduction.Size = new System.Drawing.Size(159, 22);
            this.dtDateProduction.TabIndex = 3;
            this.dtDateProduction.ValueChanged += new System.EventHandler(this.dtDateProduction_ValueChanged);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(171, 266);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(4);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(132, 31);
            this.btAjouter.TabIndex = 3;
            this.btAjouter.Text = "Ajouter>>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(693, 426);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(268, 43);
            this.xpannel1.TabIndex = 18;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(7, 0);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(135, 0);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // dgvVente
            // 
            this.dgvVente.AllowUserToAddRows = false;
            this.dgvVente.AllowUserToDeleteRows = false;
            this.dgvVente.AllowUserToResizeColumns = false;
            this.dgvVente.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvVente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVente.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvVente.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvVente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVente.Location = new System.Drawing.Point(332, 23);
            this.dgvVente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvVente.MultiSelect = false;
            this.dgvVente.Name = "dgvVente";
            this.dgvVente.ReadOnly = true;
            this.dgvVente.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvVente.RowHeadersVisible = false;
            this.dgvVente.RowTemplate.Height = 16;
            this.dgvVente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVente.Size = new System.Drawing.Size(629, 204);
            this.dgvVente.TabIndex = 17;
            this.dgvVente.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.AllowUserToDeleteRows = false;
            this.dgvDetail.AllowUserToResizeColumns = false;
            this.dgvDetail.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvDetail.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvDetail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvDetail.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvDetail.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Location = new System.Drawing.Point(332, 231);
            this.dgvDetail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvDetail.MultiSelect = false;
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.ReadOnly = true;
            this.dgvDetail.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvDetail.RowHeadersVisible = false;
            this.dgvDetail.RowTemplate.Height = 16;
            this.dgvDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDetail.Size = new System.Drawing.Size(629, 189);
            this.dgvDetail.TabIndex = 19;
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(332, 426);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(4);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(121, 31);
            this.btEnlever.TabIndex = 3;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // FrBonSortie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 483);
            this.Controls.Add(this.dgvVente);
            this.Controls.Add(this.dgvDetail);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.gbSelection);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btEnlever);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrBonSortie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bon de Sortie Produits Fini";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrBonSortie_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbSelection.ResumeLayout(false);
            this.gbSelection.PerformLayout();
            this.panFardeaux.ResumeLayout(false);
            this.panFardeaux.PerformLayout();
            this.panPalette.ResumeLayout(false);
            this.panPalette.PerformLayout();
            this.xpannel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.TextBox txtDateBon;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.GroupBox gbSelection;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.DataGridView dgvVente;
        private System.Windows.Forms.Button btParcourrirNumeros;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNumeros;
        private System.Windows.Forms.TextBox txtSelectedProduit;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dtDateProduction;
        private System.Windows.Forms.ComboBox cbLot;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panPalette;
        private System.Windows.Forms.DataGridView dgvDetail;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Panel panFardeaux;
        private System.Windows.Forms.TextBox txtNombreFardeaux;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNombrePalette;
        private System.Windows.Forms.Label label8;
    }
}