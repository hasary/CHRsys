﻿namespace FZBGsys.NsComercial
{
    partial class FrFactureRoute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.txtDateBon = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btAfficher = new System.Windows.Forms.Button();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.gbLiv = new System.Windows.Forms.GroupBox();
            this.txtDatePC = new System.Windows.Forms.TextBox();
            this.txtNumeroPC = new System.Windows.Forms.TextBox();
            this.txtMatricule = new System.Windows.Forms.TextBox();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.txtNomChauffeur = new System.Windows.Forms.TextBox();
            this.btSelectChauffeur = new System.Windows.Forms.Button();
            this.dtDateLivraison = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.xpannel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.gbLiv.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtAdresse);
            this.groupBox1.Controls.Add(this.txtDateBon);
            this.groupBox1.Controls.Add(this.txtClient);
            this.groupBox1.Controls.Add(this.txtNoBon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btAfficher);
            this.groupBox1.Location = new System.Drawing.Point(19, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(311, 161);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bon d\'Attribution";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 128);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Adr.:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 68);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 97);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Client:";
            // 
            // txtAdresse
            // 
            this.txtAdresse.Location = new System.Drawing.Point(64, 124);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(4);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.ReadOnly = true;
            this.txtAdresse.Size = new System.Drawing.Size(237, 22);
            this.txtAdresse.TabIndex = 2;
            // 
            // txtDateBon
            // 
            this.txtDateBon.Location = new System.Drawing.Point(64, 64);
            this.txtDateBon.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateBon.Name = "txtDateBon";
            this.txtDateBon.ReadOnly = true;
            this.txtDateBon.Size = new System.Drawing.Size(128, 22);
            this.txtDateBon.TabIndex = 2;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(64, 94);
            this.txtClient.Margin = new System.Windows.Forms.Padding(4);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(237, 22);
            this.txtClient.TabIndex = 2;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(64, 34);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(128, 22);
            this.txtNoBon.TabIndex = 2;
            this.txtNoBon.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "N°";
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(200, 32);
            this.btAfficher.Margin = new System.Windows.Forms.Padding(4);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(103, 28);
            this.btAfficher.TabIndex = 0;
            this.btAfficher.Text = "Afficher >>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(621, 377);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(301, 43);
            this.xpannel1.TabIndex = 16;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(36, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(164, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(336, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(576, 354);
            this.dgv.TabIndex = 15;
            // 
            // gbLiv
            // 
            this.gbLiv.Controls.Add(this.txtDatePC);
            this.gbLiv.Controls.Add(this.txtNumeroPC);
            this.gbLiv.Controls.Add(this.txtMatricule);
            this.gbLiv.Controls.Add(this.txtDestination);
            this.gbLiv.Controls.Add(this.txtNomChauffeur);
            this.gbLiv.Controls.Add(this.btSelectChauffeur);
            this.gbLiv.Controls.Add(this.dtDateLivraison);
            this.gbLiv.Controls.Add(this.label4);
            this.gbLiv.Controls.Add(this.label5);
            this.gbLiv.Controls.Add(this.label6);
            this.gbLiv.Controls.Add(this.label7);
            this.gbLiv.Controls.Add(this.label8);
            this.gbLiv.Controls.Add(this.label13);
            this.gbLiv.Controls.Add(this.label9);
            this.gbLiv.Enabled = false;
            this.gbLiv.Location = new System.Drawing.Point(19, 178);
            this.gbLiv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLiv.Name = "gbLiv";
            this.gbLiv.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLiv.Size = new System.Drawing.Size(311, 231);
            this.gbLiv.TabIndex = 35;
            this.gbLiv.TabStop = false;
            this.gbLiv.Text = "Livraison";
            // 
            // txtDatePC
            // 
            this.txtDatePC.Location = new System.Drawing.Point(117, 204);
            this.txtDatePC.Margin = new System.Windows.Forms.Padding(4);
            this.txtDatePC.Name = "txtDatePC";
            this.txtDatePC.ReadOnly = true;
            this.txtDatePC.Size = new System.Drawing.Size(183, 22);
            this.txtDatePC.TabIndex = 82;
            // 
            // txtNumeroPC
            // 
            this.txtNumeroPC.Location = new System.Drawing.Point(117, 177);
            this.txtNumeroPC.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumeroPC.Name = "txtNumeroPC";
            this.txtNumeroPC.ReadOnly = true;
            this.txtNumeroPC.Size = new System.Drawing.Size(183, 22);
            this.txtNumeroPC.TabIndex = 82;
            // 
            // txtMatricule
            // 
            this.txtMatricule.Location = new System.Drawing.Point(117, 149);
            this.txtMatricule.Margin = new System.Windows.Forms.Padding(4);
            this.txtMatricule.Name = "txtMatricule";
            this.txtMatricule.ReadOnly = true;
            this.txtMatricule.Size = new System.Drawing.Size(183, 22);
            this.txtMatricule.TabIndex = 82;
            // 
            // txtDestination
            // 
            this.txtDestination.Location = new System.Drawing.Point(72, 55);
            this.txtDestination.Margin = new System.Windows.Forms.Padding(4);
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.ReadOnly = true;
            this.txtDestination.Size = new System.Drawing.Size(215, 22);
            this.txtDestination.TabIndex = 82;
            this.txtDestination.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDestination_KeyPress);
            // 
            // txtNomChauffeur
            // 
            this.txtNomChauffeur.Location = new System.Drawing.Point(117, 119);
            this.txtNomChauffeur.Margin = new System.Windows.Forms.Padding(4);
            this.txtNomChauffeur.Name = "txtNomChauffeur";
            this.txtNomChauffeur.ReadOnly = true;
            this.txtNomChauffeur.Size = new System.Drawing.Size(183, 22);
            this.txtNomChauffeur.TabIndex = 82;
            // 
            // btSelectChauffeur
            // 
            this.btSelectChauffeur.Location = new System.Drawing.Point(115, 85);
            this.btSelectChauffeur.Margin = new System.Windows.Forms.Padding(4);
            this.btSelectChauffeur.Name = "btSelectChauffeur";
            this.btSelectChauffeur.Size = new System.Drawing.Size(125, 28);
            this.btSelectChauffeur.TabIndex = 81;
            this.btSelectChauffeur.Text = "Selectionner...";
            this.btSelectChauffeur.UseVisualStyleBackColor = true;
            this.btSelectChauffeur.Click += new System.EventHandler(this.btSelectChauffeur_Click);
            // 
            // dtDateLivraison
            // 
            this.dtDateLivraison.CustomFormat = "dd/MM/yyyy";
            this.dtDateLivraison.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDateLivraison.Location = new System.Drawing.Point(72, 25);
            this.dtDateLivraison.Margin = new System.Windows.Forms.Padding(4);
            this.dtDateLivraison.Name = "dtDateLivraison";
            this.dtDateLivraison.Size = new System.Drawing.Size(215, 23);
            this.dtDateLivraison.TabIndex = 80;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Date:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Adresse:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Matricule:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Numéro PC:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(47, 204);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "date PC:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(29, 91);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "Transport";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Nom Chauffeur:";
            // 
            // FrFactureRoute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 430);
            this.Controls.Add(this.gbLiv);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrFactureRoute";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bon de Livraison";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrBonLivraison_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.xpannel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.gbLiv.ResumeLayout(false);
            this.gbLiv.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.TextBox txtDateBon;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.GroupBox gbLiv;
        private System.Windows.Forms.DateTimePicker dtDateLivraison;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtDatePC;
        private System.Windows.Forms.TextBox txtNumeroPC;
        private System.Windows.Forms.TextBox txtMatricule;
        private System.Windows.Forms.TextBox txtDestination;
        private System.Windows.Forms.TextBox txtNomChauffeur;
        private System.Windows.Forms.Button btSelectChauffeur;
    }
}