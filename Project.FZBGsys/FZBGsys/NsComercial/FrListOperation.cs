﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FZBGsys.NsComercial
{
    public partial class FrListOperation : Form
    {
        ModelEntities db = new ModelEntities();
        List<Operation> ListFound;
        public FrListOperation()
        {

            InitializeComponent();
            InitialiseControls();
            //panAdmin.Visible = Tools.CurrentUserID == 1;


        }



        public static void TransfertOperationSage(DateTime From, DateTime To)
        {

            if (true)
            {
                using (var db = new ModelEntities())
                {
                    var operations = db.Operations.Where(
                        p =>
                        p.Date <= To && p.Date >= From).ToList();


                    StreamWriter sr = File.CreateText("Oper_import.txt");
                    sr.WriteLine("Type$Sens$Date$ModePayement$libele$CodeClientFourn$LibeleClientFourn$Montant");
                    foreach (var operation in operations)
                    {
                        string code = "", sujet = "";
                        if (operation.ClientID != null && operation.ClientID > 0)
                        {

                            code = operation.Client.CodeSage;
                            if (code.StartsWith("D"))
                            {
                                sujet = "Divers:" + operation.Client.Nom;
                            }
                            else
                            {
                                sujet = operation.Client.Nom;
                            }

                        }
                        else if (operation.FournisseurID != null && operation.FournisseurID > 0)
                        {
                            code = operation.Fournisseur.CodeSage;
                            sujet = operation.Fournisseur.Nom;
                        }

                        /*
                         Type$Sens$Date$ModePayement$libele$CodeClientFourn$LibeleClientFourn$Montant
                         
                         */
                        sr.WriteLine(
                            operation.TypeOperation.Nom + "$" +
                            operation.Sen + "$" +
                            operation.Date.Value.ToShortDateString() + "$" +
                            operation.ModePayement.ToModePayementTxt() + "$" +
                            operation.Lib + " " + operation.Observation + "$" +
                            code + "$" +
                            sujet + "$" +
                            operation.Montant);

                    }
                    sr.Close();
                    System.Diagnostics.Process.Start("Oper_import.txt");
                }
            }



        }



        private void InitialiseControls()
        {
            cbTypeOperation.Items.Clear();
            this.NoOperationSelected = new TypeOperation { ID = 0, Nom = "[Tout]" };
            cbTypeOperation.Items.Add(NoOperationSelected);
            cbTypeOperation.Items.AddRange(db.TypeOperations.Where(p => p.Categorie == (int)EnCatOperation.Recette).OrderBy(p => p.Nom).ToArray());
            this.NoOperationSelectedEmpty = new TypeOperation { ID = 0, Nom = "" };
            cbTypeOperation.Items.Add(NoOperationSelectedEmpty);
            cbTypeOperation.Items.AddRange(db.TypeOperations.Where(p => p.Categorie == (int)EnCatOperation.Depense).OrderBy(p => p.Nom).ToArray());
            cbTypeOperation.DisplayMember = "Nom";
            cbTypeOperation.ValueMember = "ID";
            cbTypeOperation.SelectedItem = NoOperationSelected;
            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtFourni.AutoCompleteCustomSource = acsc;
            txtFourni.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtFourni.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.Fournisseurs.ToList();

            if (results.Count > 0)
                foreach (var client in results)
                {
                    acsc.Add(client.Nom);
                }


            if (Tools.CurrentUserID != 20 && Tools.CurrentUserID != 1 && Tools.CurrentUserID != 42)

            {
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Associes"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Salaires"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Avances sur Salaire"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Dons"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Remboursement Pret Associer / NMC"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Remboursement Pret Client"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Pret Client"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Pret NMC / Associers ( Entree )"));
                cbTypeOperation.Items.Remove(db.TypeOperations.Single(p => p.Nom == "Pret Associer  / NMC ( Sortie )"));

                btImprimer.Visible = btCloturer.Visible = btaddminsupp.Visible = btAnnuler.Visible = button3.Visible = false;


            }


            UpdateClotureButton();
            Recherche();
        }

        private void FrListOperation_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
        }

        private void Recherche()
        {
            if (dtDate.Value.DayOfWeek == DayOfWeek.Saturday || dtDate.Value.DayOfWeek == DayOfWeek.Friday)
            {
                //  this.ShowInformation("Journée non ouvrable!");
                //  return;
            }

            if (!chRechAvancee.Checked)
            {
                LoadOperationDay();
                ListFound = this.operationsDay;
            }
            else
            {
                IQueryable<Operation> result = db.Operations;
                var dateFrom = dtDateDu.Value.Date;
                var dateTo = dtDateAu.Value.Date;
                result = result.Where(p => p.Date >= dateFrom && p.Date <= dateTo);

                LabFilter.Text = "Mouvements du " + dateFrom.ToShortDateString() + " au " + dateTo.ToShortDateString();
                if (!chRecette.Checked)
                {
                    result = result.Where(p => p.TypeOperation.Categorie != (int)EnCatOperation.Recette);

                }
                else
                {
                    LabFilter.Text += " Recettes";
                }

                if (!chDepense.Checked)
                {
                    result = result.Where(p => p.TypeOperation.Categorie != (int)EnCatOperation.Depense);
                }
                else
                {
                    LabFilter.Text += " Depenses";
                }


                if (cbTypeOperation.SelectedIndex != 0)
                {
                    var typeOperation = (TypeOperation)cbTypeOperation.SelectedItem;
                    var id = (typeOperation).ID;
                    result = result.Where(p => p.TypeOperaitonID == id);
                    LabFilter.Text += " " + typeOperation.Nom;
                }

                if (panFacture.Visible = true && txtNoFacture.Text.Trim().ParseToInt() != null)
                {
                    var factureID = txtNoFacture.Text.Trim().ParseToInt();
                    result = result.Where(p => p.FactureID == factureID);
                    LabFilter.Text += " N° FZ" + factureID;
                }

                if (this.SelectedClient != null)
                {
                    var cli = txtClient.Text.Trim().ToUpper();
                    result = result.Where(p => p.Client.Nom.Contains(cli));
                    LabFilter.Text += " pour " + SelectedClient.Nom;
                }
                if (cbAssocier.Visible && cbAssocier.SelectedIndex > 1)
                {
                    var assoc = cbAssocier.SelectedItem.ToString();
                    result = result.Where(p => p.Lib == assoc);


                }
                if (cbChargeusine.Visible && cbChargeusine.SelectedIndex > 0)
                {
                    var charge = cbChargeusine.SelectedItem.ToString();
                    result = result.Where(p => p.Lib.Contains(charge));


                }
                if (cbChargeusine.Visible && cbchargecategorie.SelectedIndex > 0)
                {
                    var charge = cbchargecategorie.SelectedItem.ToString();
                    result = result.Where(p => p.Lib.Contains(charge));


                }

                if (txtFourni.Text.Trim() != "")
                {
                    var four = txtFourni.Text.Trim().ToUpper();
                    result = result.Where(p => p.Fournisseur.Nom.Contains(four));
                    LabFilter.Text += " " + txtFourni.Text.Trim();
                }

                if (!chEspece.Checked)
                {
                    result = result.Where(p => p.ModePayement != (int)EnModePayement.Especes);
                }

                if (!chVirement.Checked)
                {
                    result = result.Where(p => p.ModePayement != (int)EnModePayement.Virement);
                }

                if (!chTraite.Checked)
                {
                    result = result.Where(p => p.ModePayement != (int)EnModePayement.Traite);
                }

                if (!chCheque.Checked)
                {
                    result = result.Where(p => p.ModePayement != (int)EnModePayement.Chèque);
                }

                ListFound = result.ToList();
            }

            RefreshGrid();

        }

        private void RefreshGrid()
        {
            if (ListFound == null)
            {
                return;
            }
            dgv.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Date = p.Date,
                Sens = p.TypeOperation.Categorie.ToCategorieOperationTxt(),
                Operation = p.TypeOperation.Nom,
                Sujet = ((p.Client != null) ? p.Client.NomClientAndParent() : "") + ((p.FournisseurID != null) ? p.Fournisseur.Nom : ""),
                Libele = p.Lib,
                Montant = p.Montant.ToAffInt(),
                Mode = p.ModePayement.ToModePayementTxt(),
                Observation = p.Observation,
                Inscription = p.ApplicationUtilisateur.Employer.Nom + " le " + p.InscriptionDate.Value.ToString("dd/MM/yyyy à hh:mm "),
                UID = p.InscriptionUID,

            }).ToList();



            dgv.HideIDColumn();
            dgv.HideIDColumn("UID");
            dgv.HideIDColumn("Inscription");


        }



        private void btAnnuler_Click(object sender, EventArgs e)
        {

            var id = dgv.GetSelectedID();

            if (id != null)
            {
                var toDel = ListFound.Single(p => p.ID == id.Value);

                if (toDel.TypeOperaitonID == (int)EnTypeOpration.Avance ||
                    toDel.TypeOperaitonID == (int)EnTypeOpration.Reglement_Créance ||
                    toDel.TypeOperaitonID == (int)EnTypeOpration.Payement_au_Comptant)
                {
                    if (Tools.CurrentUserID != 1)
                    {
                        this.ShowWarning("Vous ne pouvez pas suprimer ce type d'operation.");
                        return;
                    }
                }

                var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == toDel.Date);
                var user = db.ApplicationUtilisateurs.Single(p => p.ID == Tools.CurrentUserID);

                if (hist != null && user.ApplicationGroupe.IsBoss != true)
                {
                    this.ShowWarning("vous ne pouvez pas supprimer cet enregistrement car cette journée est cloturée!");
                    return;
                }

                if (toDel.InscriptionUID != Tools.CurrentUserID && user.ApplicationGroupe.IsBoss != true)
                {
                    this.ShowWarning("vous ne pouvez pas supprimer un enregistrement dont vous n'etes pas l'auteur.");
                    return;
                }

                if (this.ConfirmWarning("Etes-vous sur de vouloir supprimer cette operation?"))
                {
                    var client = toDel.Client;
                    var caisse = db.Caisses.First();
                    switch ((EnTypeOpration)toDel.TypeOperaitonID)
                    {
                        case EnTypeOpration.Reglement_Créance:
                            client.Solde -= toDel.Montant;

                            break;
                        case EnTypeOpration.Avance:
                            client.Solde -= toDel.Montant;
                            break;
                        case EnTypeOpration.Payement_au_Comptant:
                            // client.Solde -= toDel.Montant;
                            toDel.Facture.Etat = (int)EnEtatFacture.Non_payée;
                            break;
                        // case EnTypeOpration.Depense:

                        default:
                            break;
                    }
                    if (toDel.ModePayement == (int)EnModePayement.Especes)
                    {
                        if (toDel.TypeOperation.Categorie == (int)EnCatOperation.Recette)
                        {
                            caisse.Solde -= toDel.Montant;

                        }
                        else if (toDel.TypeOperation.Categorie == (int)EnCatOperation.Depense)
                        {
                            caisse.Solde += toDel.Montant;
                        }
                    }

                    if (toDel.Client != null)
                    {
                        toDel.Client.DateDernierVersement = null;
                    }
                    ListFound.Remove(toDel);
                    db.DeleteObject(toDel);
                    db.SaveChangeTry();
                    RefreshGrid();
                }
            }



        }

        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechAvancee.Checked;
            btCloturer.Enabled = !chRechAvancee.Checked;
            dtDate.Visible = !chRechAvancee.Checked;
            UpdateClotureButton();

            if (!chRechAvancee.Checked)
            {
                Recherche();
            }
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btImprimer_Click(object sender, EventArgs e)
        {
            PrepareListOperationCustom();
            NsReports.Caisse.CaisseReportController.PrintOperationsCustom(LabFilter.Text);
        }

        private void PrepareListOperationCustom()
        {

            if (!chRechAvancee.Checked)
            {
                var date = dtDate.Value.Date;
                var yesterday = date.AddDays(-1);
                if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                {
                    yesterday = yesterday.AddDays(-2);
                }
                var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == yesterday);
                decimal? AnSolde = null;
                if (hist != null)
                {
                    AnSolde = hist.Solde.Value;

                }


                FZBGsys.NsReports.Caisse.CaisseReportController.PrintCaisseJournee(date, AnSolde, yesterday.ToShortDateString()); //an

            }
            else
            {
                Tools.ClearPrintTemp(); //<------------------------ must epmpty printTemp before
                foreach (var item in ListFound.Where(p => p.TypeOperaitonID != (int)EnTypeOpration.Prevision_de_Dépenses))
                {
                    var ptm = new PrintTemp

                    {
                        UID = Tools.CurrentUserID,
                        Date = item.Date,
                        Groupe = item.TypeOperation.Nom,
                        val1 = ((item.Client != null && item.ClientID != 0) ? item.Client.NomClientAndParent() : "") + ((item.FournisseurID != null && item.FournisseurID != 0) ? item.Fournisseur.Nom : ""),
                        Description = item.Lib + ": " + item.Observation,
                        Etat = item.ModePayement.ToModePayementTxt(),
                        MontantC = (item.Sen == "C") ? item.Montant : 0,
                        MontantD = (item.Sen == "D") ? item.Montant : 0,
                    };

                    db.AddToPrintTemps(ptm);
                }
                db.SaveChangeTry();



            }
        }

        private void cbTypeOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            var SelectedTypeOperation = (TypeOperation)cbTypeOperation.SelectedItem;

            panFacture.Visible = SelectedTypeOperation.ID == (int)EnTypeOpration.Payement_au_Comptant;
            cbAssocier.Visible =
                SelectedTypeOperation.ID == (int)EnTypeOpration.Associes ||
                SelectedTypeOperation.ID == (int)EnTypeOpration.Pret_Associer_NMC_ENTREE ||
                SelectedTypeOperation.ID == (int)EnTypeOpration.Pret_ASSOCIER_NMC_SORTIE ||
                SelectedTypeOperation.ID == (int)EnTypeOpration.Rembourssement_Pret_Associer_NMC_SORTIE;

            cbChargeusine.Visible = cbchargecategorie.Visible = SelectedTypeOperation.ID == (int)EnTypeOpration.Charge_Usine;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.SelectedClient = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (SelectedClient != null)
            {
                txtClient.Text = SelectedClient.Nom;
            }
            else
            {
                txtClient.Text = "";
            }

        }

        public TypeOperation NoOperationSelected { get; set; }

        private void btCloturer_Click(object sender, EventArgs e)
        {
            var date = dtDate.Value.Date;

            if (btCloturer.Text == "Réouvrir la journée" && Tools.isBoss == true)
            {
                var todel = db.CaisseHistories.SingleOrDefault(p => p.Date == date);
                if (todel != null)
                {
                    db.DeleteObject(todel);
                    db.SaveChanges();
                    UpdateClotureButton();
                    Recherche();
                    return;
                }
            }

            if (operationsDay.Count != 0)
            {
                if (date.DayOfWeek == DayOfWeek.Saturday)
                {
                    if (this.ConfirmWarning("Transferer la journée du Samedi vers dimanche ?"))
                    {
                        foreach (var item in operationsDay)
                        {
                            item.Date = item.Date.Value.AddDays(+1);
                        }
                        db.SaveChanges();
                        UpdateClotureButton();
                        Recherche();
                    }
                    else
                    {
                        return;
                    }
                }
                if (this.ConfirmWarning("Confirmer cloture de la journéee " + date.ToShortDateString() + " ?\nATTENTION: vous ne pouvez plus faire des modifications sur cette date !! "))
                {
                    var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == date);
                    if (hist != null)
                    {
                        this.ShowInformation("Cette journée est déja cloturée");
                        return;
                    }
                    var yesterday = date.AddDays(-1);
                    if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                    {
                        yesterday = yesterday.AddDays(-2);
                    }
                    var histYesterday = db.CaisseHistories.SingleOrDefault(p => p.Date == yesterday);
                    if (histYesterday == null)
                    {
                        this.ShowInformation("vous ne pouvez pas cloturer cette journée car la journée anterieure est encore ouverte! (" + yesterday.ToLongDateString() + ")");
                        return;
                    }

                    var newHist = new CaisseHistory();
                    newHist.Date = date;
                    newHist.IsReportSended = false;
                    var opEsp = operationsDay.Where(p => p.ModePayement == (int)EnModePayement.Especes).ToList();
                    var c = opEsp.Where(p => p.Sen == "C").Sum(p => p.Montant);
                    var d = opEsp.Where(p => p.Sen == "D").Sum(p => p.Montant);
                    newHist.Solde = histYesterday.Solde + c - d;

                    db.AddToCaisseHistories(newHist);
                    if (db.SaveChangeTry())
                    {
                        this.ShowInformation("journée " + date.ToShortDateString() + " cloturé avec succé.\n Solde Caisse: " + newHist.Solde);
                        UpdateClotureButton();
                        Recherche();
                    };

                }
            }
            else
            {
                if (this.ConfirmWarning("Confirmer cloture de la journéee non ouvrable " + date.ToShortDateString() + " ?\nATTENTION: vous ne pouvez plus faire des modifications sur cette date !! "))
                {

                    var newHist = new CaisseHistory();
                    newHist.Date = date;
                    newHist.IsReportSended = false;
                    var yesterday = date.AddDays(-1);
                    if (yesterday.DayOfWeek == DayOfWeek.Saturday)
                    {
                        yesterday = yesterday.AddDays(-2);
                    }
                    var histYesterday = db.CaisseHistories.SingleOrDefault(p => p.Date == yesterday);
                    newHist.Solde = histYesterday.Solde;

                    db.AddToCaisseHistories(newHist);
                    if (db.SaveChangeTry())
                    {
                        this.ShowInformation("journée " + date.ToShortDateString() + " non-ouvrable.\n Solde Caisse: " + newHist.Solde);
                        UpdateClotureButton();
                        Recherche();
                    };



                }


            }
        }

        private void FrListOperation_Load(object sender, EventArgs e)
        {

        }

        public Client SelectedClient { get; set; }

        public List<Operation> operationsDay { get; set; }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            UpdateClotureButton();
            Recherche();
        }

        private void LoadOperationDay()
        {
            if (!chRechAvancee.Checked)
            {
                var date = dtDate.Value.Date;
                if (Tools.CurrentUserID != 20 && Tools.CurrentUserID != 1)
                    this.operationsDay = db.Operations.Where(p => p.Date == date && (p.TypeOperaitonID == 6 || p.TypeOperaitonID == 14)).ToList();
                else
                    this.operationsDay = db.Operations.Where(p => p.Date == date).ToList();
            }
        }

        private void UpdateClotureButton()
        {
            LoadOperationDay();



            var date = dtDate.Value.Date;
            if (date.DayOfWeek == DayOfWeek.Friday)
            {
                btCloturer.Text = "Journée non-ouvrable";
                btCloturer.Enabled = false;
                return;
            }
            var history = db.CaisseHistories.SingleOrDefault(p => p.Date == date);

            btCloturer.Enabled = true;
            if (operationsDay == null)
            {
                return;
            }

            if (operationsDay.Count == 0)
            {
                btCloturer.Text = "Définir journ. non-ouvrable";
            }
            else if (dtDate.Value.DayOfWeek == DayOfWeek.Saturday)
            {
                btCloturer.Text = "Transfet vers jour suivant";
            }
            else
            {
                btCloturer.Text = "Cloturer la Journée";
            }
            panEdit2.Visible = true;
            if (history != null)
            {
                panEdit2.Visible = false;
                if (!Tools.isBoss == true)
                {
                    btCloturer.Text = "Déja Cloturée";
                    btCloturer.Enabled = false;
                }
                else
                {
                    btCloturer.Text = "Réouvrir la journée";
                    btCloturer.Enabled = true;
                }
            }
        }

        public TypeOperation NoOperationSelectedEmpty { get; set; }

        private void btaddminsupp_Click(object sender, EventArgs e)
        {

            var id = dgv.GetSelectedID();

            if (id != null)
            {
                var toDel = ListFound.Single(p => p.ID == id.Value);

                if (toDel.TypeOperaitonID == (int)EnTypeOpration.Avance ||
                    toDel.TypeOperaitonID == (int)EnTypeOpration.Reglement_Créance ||
                    toDel.TypeOperaitonID == (int)EnTypeOpration.Payement_au_Comptant)
                {
                    if (Tools.CurrentUserID != 1)
                    {
                        this.ShowWarning("Vous ne pouvez pas supprimer ce type d'operation.");
                        return;
                    }
                }

                var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == toDel.Date);
                var user = db.ApplicationUtilisateurs.Single(p => p.ID == Tools.CurrentUserID);

                if (hist != null && user.ApplicationGroupe.IsBoss != true)
                {
                    //  this.ShowWarning("vous ne pouvez pas supprimer cet enregistrement car cette journée est cloturée!");
                    // return;
                }

                if (toDel.InscriptionUID != Tools.CurrentUserID && user.ApplicationGroupe.IsBoss != true)
                {
                    // this.ShowWarning("vous ne pouvez pas supprimer un enregistrement dont vous n'etes pas l'auteur.");
                    // return;
                }

                if (this.ConfirmWarning("Etes-vous sur de vouloir supprimer cette operation?"))
                {
                    var client = toDel.Client;
                    var caisse = db.Caisses.First();
                    switch ((EnTypeOpration)toDel.TypeOperaitonID)
                    {
                        case EnTypeOpration.Reglement_Créance:
                            client.Solde -= toDel.Montant;

                            break;
                        case EnTypeOpration.Avance:
                            client.Solde -= toDel.Montant;
                            break;
                        case EnTypeOpration.Payement_au_Comptant:
                            // client.Solde -= toDel.Montant;
                            toDel.Facture.Etat = (int)EnEtatFacture.Non_payée;
                            break;
                        // case EnTypeOpration.Depense:

                        default:
                            break;
                    }
                    if (toDel.ModePayement == (int)EnModePayement.Especes)
                    {
                        if (toDel.TypeOperation.Categorie == (int)EnCatOperation.Recette)
                        {
                            caisse.Solde -= toDel.Montant;

                        }
                        else if (toDel.TypeOperation.Categorie == (int)EnCatOperation.Depense)
                        {
                            caisse.Solde += toDel.Montant;
                        }
                    }

                    if (toDel.Client != null)
                    {
                        toDel.Client.DateDernierVersement = null;
                    }
                    ListFound.Remove(toDel);
                    db.DeleteObject(toDel);
                    db.SaveChangeTry();
                    RefreshGrid();
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {

                new FrOperationEdit(id.Value).ShowDialog();
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, ListFound);
                RefreshGrid();

            }

        }
    }
}
