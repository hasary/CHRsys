﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrFactureRoute : Form
    {
        ModelEntities db = new ModelEntities();

        public FrFactureRoute(string BonAttributionID)
        {
            InitializeComponent();
            txtNoBon.Text = BonAttributionID;
            txtNoBon.ReadOnly = true;
            LoadBonAttribution(BonAttributionID.ParseToInt().Value);

        }

        public FrFactureRoute(int? id = null)
        {
            InitializeComponent();
            if (id != null)
            {
                InputBonlivraison(id.Value);
            }

            txtNoBon.Select();
        }

        private void InputBonlivraison(int id)
        {
            this.BonLivraison = db.BonLivraisons.Single(p => p.ID == id);
            BonAttribution = BonLivraison.BonAttribution;
            txtNoBon.Text = BonAttribution.ID.ToString();
            txtClient.Text = BonAttribution.Client.NomClientAndParent();
            txtAdresse.Text = BonAttribution.Client.Adresse;
            txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();
            txtDestination.Text = txtAdresse.Text;
            this.Chauffeur = BonLivraison.Chauffeur;

            if (this.Chauffeur != null)
            {
                txtNomChauffeur.Text = this.Chauffeur.Nom;
                txtNumeroPC.Text = this.Chauffeur.NumeroPC;
                txtDatePC.Text = this.Chauffeur.DatePC;
                txtMatricule.Text = this.Chauffeur.DefaultMatricule;
            }

            gbLiv.Enabled = true;
            RefreshGrid();

        }

        private void btAfficher_Click(object sender, EventArgs e)
        {
            var id = txtNoBon.Text.ParseToInt();

            if (id != null)
            {
                LoadBonAttribution(id.Value);
            }
            else
            {
                this.ShowWarning("Numéro Bon Invalide");
            }

        }

        private void LoadBonAttribution(int id)
        {
            this.BonAttribution = db.BonAttributions.SingleOrDefault(p => p.ID == id);
            if (BonAttribution == null)
            {
                this.ShowWarning("Numéro Bon introuvable");
                return;
            }

            //  if (BonAttribution.Etat != (int)EnEtatBonAttribution.Chargé)
            // {
            if (BonAttribution.BonLivraisons.Count != 0)
            {
                if (this.ConfirmInformation("Ce bon est déja livré, voulez-vous imprimer le bon de Livraison?"))
                {
                    FZBGsys.NsRapports.FrViewer.PrintBonLivraison(BonAttribution.BonLivraisons.First().ID);
                    return;
                }
                return;
            }

            //    this.ShowWarning("Ce Bon d'attribution est " + BonAttribution.Etat.ToEtatBonAttributionTxt());
            //    return;
            //  }
            // else 
            if (BonAttribution.BonLivraisons.Count != 0)
            {
                var first = BonAttribution.BonLivraisons.First();
                if (this.ConfirmInformation("Ce Bon est déja livré, voulez-vous imprimer Bon Livraison?"))
                {
                    FZBGsys.NsRapports.FrViewer.PrintBonLivraison(first.ID);
                    return;
                }
            }

            //gbpayement.Enabled = true;
            txtClient.Text = BonAttribution.Client.NomClientAndParent();
            txtAdresse.Text = BonAttribution.Client.Adresse;
            txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();
            txtDestination.Text = txtAdresse.Text;
            gbLiv.Enabled = true;
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            var BonSortie = BonAttribution.BonSorties.FirstOrDefault();
            if (BonSortie != null)
            {
                dgv.DataSource = BonSortie.VenteDetails.Select(p => new
                    {
                        ID = p.ID,
                        Produit = p.Vente.GoutProduit.Nom,
                        Format = p.Vente.Format.Volume,
                        REF = p.Vente.Unite.ToUniteProduitTxt(),
                        Production = p.DateProduction.Value.ToShortDateString(),
                        Lot = p.Lot,
                        Numeros = p.Numeros,
                        Nombre = p.Nombre,

                    }).ToList();
                dgv.HideIDColumn();

            }
            else
            {
                dgv.DataSource = BonAttribution.Ventes.Select(p => new
                {
                    ID = p.ID,
                    Produit = p.GoutProduit.Nom,
                    Format = p.Format.Volume,
                    REF = p.Unite.ToUniteProduitTxt(),
                    Production = "/",
                    Lot = "/",
                    Numeros = "/",
                    Nombre = (p.Unite == (int)EnUniteProduit.Palettes) ? p.NombrePalettes : p.NombreFardeaux,

                }).ToList();
                dgv.HideIDColumn();
            }

        }

        private void UpdateControls()
        {

        }

        private void FrBonLivraison_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public BonAttribution BonAttribution { get; set; }

        private void btSelectChauffeur_Click(object sender, EventArgs e)
        {
            this.Chauffeur = FZBGsys.NsComercial.NsChauffeur.FrList.SelectChaffeurDialog(BonAttribution.Client, db);

            if (this.Chauffeur != null)
            {
                txtNomChauffeur.Text = this.Chauffeur.Nom;
                txtNumeroPC.Text = this.Chauffeur.NumeroPC;
                txtDatePC.Text = this.Chauffeur.DatePC;
                txtMatricule.Text = this.Chauffeur.DefaultMatricule;
            }
        }

        public Chauffeur Chauffeur { get; set; }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {

            if (!isValidAll())
            {
                return;
            }

            if (BonLivraison == null)
            {
                BonLivraison = new BonLivraison();
                BonLivraison.BonAttribution = BonAttribution;
            }
            var ventes = BonAttribution.Ventes.ToList();
            foreach (var vente in ventes)
            {
                vente.BonLivraison = BonLivraison;
            }

            BonLivraison.Chauffeur = Chauffeur;
            BonLivraison.DateLivraison = dtDateLivraison.Value.Date;
            BonLivraison.AdresseLivraison = txtDestination.Text.Trim();
            BonAttribution.Etat = (int)EnEtatBonAttribution.Livré;
            var currentTime = Tools.GetServerDateTime(); 

            if (BonLivraison.EntityState == EntityState.Added)
            {
                BonLivraison.InscriptionTime = currentTime;
                BonLivraison.InscriptionUID = Tools.CurrentUserID;

            }
            else
            {
                BonLivraison.ModificationTime = currentTime;
            }
            db.SaveChanges();
            btEnregistrer.Enabled = false;
            FZBGsys.NsRapports.FrViewer.PrintBonLivraison2(BonLivraison.ID);
            Dispose();

        }


        private bool isValidAll()
        {
            var message = "";
            if (Chauffeur == null)
            {
                message = "Veillez saisir les information du transport.";
            }

            if (txtDestination.Text.Trim() == "")
            {
                message = "\nVeillez saisir l'adresse de destination.";
            }

            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public BonLivraison BonLivraison { get; set; }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void txtDestination_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
