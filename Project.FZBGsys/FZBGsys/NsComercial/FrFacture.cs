
﻿using FZBGsys.NsReports.Comercial;
using FZBGsys.NsTools;
using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrFacture : Form
    {
        ModelEntities db = new ModelEntities();
        List<Vente> ListVente = new List<Vente>();
        public FrFacture(string BonAttributionID, bool SuperGrossist)
        {
            InitializeComponent();
            this.SuperGro = SuperGrossist ;
            txtNoBon.Text = BonAttributionID;
            txtNoBon.ReadOnly = true;
            LoadBonAttribution(BonAttributionID.ParseToInt().Value);
        }

        public FrFacture(int? id = null, bool MustSave = false)
        {
            InitializeComponent();
            if (id != null)
            {
                IsEditFacture = true;
                this.Facture = db.Factures.Single(p => p.ID == id);
                InputFacture();
            }
            else
            {
                IsEditFacture = false;
                txtNoBon.Select();
            }
            this.MustSave = MustSave;
            if (MustSave)
            {
                txtNoBon.ReadOnly = true;
                btAnnuler.Enabled = false;
                this.ControlBox = false;
            }
        }

        private void InputFacture()
        {
            txtNoBon.Text = Facture.BonAttributionID.Value.ToString();
            LoadBonAttribution(Facture.BonAttributionID.Value);
            if (chRemise.Enabled && Facture.MontantRemiseUnite != null)
            {
                chRemise.Checked = true;
                txtRemise.Text = Facture.MontantRemiseUnite.ToString();
                chRemise.Enabled = true;
            }

            cbMode.SelectedIndex = Facture.ModePayement.Value;
            if (BonAttribution.Client.ClientTypeID == 4) // Gratuit
            {
                labMode.Text = "Gratuit";
                cbMode.Visible = false;
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FrFacture_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            panCheque.Visible = cbMode.SelectedIndex == (int)EnModePayement.Chèque;
            panTimbre.Visible = cbMode.SelectedIndex == (int)EnModePayement.Especes;
            UpdateTotals();


        }



        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            txtRemise.Text = "";
            panRemise.Visible = chRemise.Checked;
        }

        private void txtNoBon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (IsEditFacture)
            {
                Facture.Client.Solde += Facture.MontantTTC;

            }



            if (!IsValidAll())
            {
                return;
            }
            /* Facturation */
            if (!this.ConfirmInformation("Confirmer Information paiement du Bon N° " + BonAttribution.ID + " ?"))
            {
                return;
            }

            if (this.Facture == null)
            {
                this.Facture = new Facture() { BonAttribution = this.BonAttribution };
            }
            this.Saved = true;
            Facture.Client = BonAttribution.Client;
            Facture.Date = BonAttribution.Date;
            // Facture.Etat = 0;
            UpdateTotals();
            var current = Tools.GetServerDateTime();
            if (Facture.EntityState == EntityState.Added)
            {
                Facture.InscriptionTime = current;

                Facture.InscriptionUID = Tools.CurrentUserID;
            }
            else
            {
                Facture.ModificationTime = current;
            }
            db.SaveChanges();

            if (BonAttribution.Client.ClientTypeID != 4)
            {
                Facture.ModePayement = cbMode.SelectedIndex;
            }
            else
            {
                Facture.ModePayement = (int)EnModePayement.Gratuit;
            }
            Facture.MontantHT = ht;
            Facture.MontantRemiseUnite = txtRemise.Text.ParseToDec();
            Facture.MontantTimbre = timbre;
            Facture.MontantTotalFacture = totalFact;
            Facture.MontantTotalRemise = totalRemise;
            Facture.MontantTTC = ttc;
            Facture.MontantTVA = ttc - ht;


            var autoriseMontant = Facture.Client.MaxCreditMontant + Facture.Client.Solde;

            if (Facture.Client.Solde == null)
            {
                Facture.Client.Solde = 0;
                Facture.Client.IsSoldable = true;
            }

            UpdateTotals();

            switch ((EnModePayement)Facture.ModePayement)
            {
                case EnModePayement.Especes:
                    #region Espece
                    if (Facture.Client.IsAutoriseCredit.Value && Facture.Client.Solde - Facture.MontantTTC + Facture.Client.MaxCreditMontant <= 0)
                    {
                        this.ShowWarning("Ce client ne peut pas facturer en Espèce.");
                        return;
                    }
                    Facture.Etat = (int)EnEtatFacture.Non_payée;
               //     Facture.Client.Solde -= Facture.MontantTTC;
                    break;
                    #endregion
                case EnModePayement.Avance:
                    #region avance
                    if (Facture.MontantTTC > Facture.Client.Solde)
                    {
                        this.ShowWarning("Solde client insuffisant pour payement avance, veillez choisir un autre mode de payement");
                        return;
                    }
                    Facture.Etat = (int)EnEtatFacture.Payée;
                    Facture.Client.Solde -= Facture.MontantTotalFacture;
                    Facture.Client.IsSoldable = true;
                    break;
                    #endregion

                case EnModePayement.Virement:
                    #region VirementChequeTraite
                    if (Facture.Client.IsAutoriseCredit.Value && Facture.Client.Solde - Facture.MontantTTC + Facture.Client.MaxCreditMontant <= 0)
                    {
                        this.ShowWarning("Ce client ne peut pas facturer par virement, veillez choisir mode de payement à Terme");
                        return;
                    }
                //    Facture.Client.Solde -= Facture.MontantTotalFacture;
                    Facture.Etat = (int)EnEtatFacture.Payée;
                    break;
                case EnModePayement.Chèque:
                    if (Facture.Client.IsAutoriseCredit.Value && Facture.Client.Solde - Facture.MontantTTC + Facture.Client.MaxCreditMontant <= 0)
                    {
                        this.ShowWarning("Ce client ne peut pas facturer par Cheque, veillez choisir mode de payement à Terme");
                        return;
                    }
             //       Facture.Client.Solde -= Facture.MontantTotalFacture;
                    Facture.Etat = (int)EnEtatFacture.Payée;
                    break;
                case EnModePayement.Traite:
                    if (Facture.Client.IsAutoriseCredit.Value && Facture.Client.Solde - Facture.MontantTTC + Facture.Client.MaxCreditMontant <= 0)
                    {
                        this.ShowWarning("Ce client ne peut pas facturer par Traite, veillez choisir mode de payement à Terme");
                        return;
                    }
               //     Facture.Client.Solde -= Facture.MontantTotalFacture;
                    break;

                    #endregion
                case EnModePayement.a_Terme:

                    if (Facture.Client.IsAutoriseCredit == false || Facture.Client.Solde - Facture.MontantTTC + Facture.Client.MaxCreditMontant <= 0)
                    {
                        if (this.ConfirmInformation("Ce client n'est pas autorisé à payer è Terme ou Montant dépasse le plafond autorisé.\n voulez-vous forcer la payement?"))
                        {
                            var connectedID = Tools.ConnectUserID();
                            if (connectedID == null)
                            {
                                return;
                            }
                            var user = db.ApplicationUtilisateurs.Single(p => p.ID == connectedID);


                            if (!user.ApplicationGroupe.IsBoss.Value)
                            {
                                this.ShowWarning("Ce Utilisateur n'est pas autorisé à forcer le payement");
                                return;
                            }
                            FactureForcage forcage = new FactureForcage();
                            forcage.DateTime = Tools.GetServerDateTime();
                            forcage.UtilisateurID = user.ID;
                            forcage.FactureID = Facture.ID;
                            forcage.AncienSolde = Facture.Client.Solde;
                            forcage.Autorisation = (Facture.Client.IsAutoriseCredit == true) ? Facture.Client.MaxCreditMontant : null;
                            db.AddToFactureForcages(forcage);

                        }
                        else
                        {
                            return;
                        }

                    }

                    Facture.Etat = (int)EnEtatFacture.Payée;
               //     Facture.Client.Solde -= Facture.MontantTTC;


                    if (Facture.Client.Solde * Facture.Client.Solde < 0)
                    {
                        Facture.Client.DatePassageDebiteur = Tools.GetServerDateTime();
                    }




                    break;
                default:
                    break;
            }

            try
            {
                BonAttribution.Etat = (int)EnEtatBonAttribution.EnInstance;
                Facture.Etat = (int)EnEtatFacture.Payée;
                if (BonAttribution.BonCommande != null)
                {
                    BonAttribution.BonCommande.EtatCommande = (int)EnEtatBonCommande.Solde;
                }
                try
                {
                    BonAttribution.Client.DateDernierMouvement = Facture.Date;
                    BonAttribution.Client.DateDernierChargement = Facture.Date;

                    db.SaveChanges();
                    if (Facture.MontantTTC == null)
                    {
                        Tools.ShowError("Erreur montant veillez refaire le bon");
                        BonAttribution.Etat = (int)EnEtatBonAttribution.Annulé;
                        Facture.Etat = (int)EnEtatFacture.Annulée;
                        db.SaveChanges();
                    }
                }
                catch (Exception en)
                {

                    this.ShowError(en.AllMessages("Impossible d'enregistrer"));
                    return;
                }
                //    NsAdministration.FrMaintenance.GenerateSolde(db, EnGenerationSoldeMethod.FromFistSoldeEver, null, Facture.ClientID);
                btEnregistrer.Enabled = false;
                var totalTxt = CtoL.convertMontant(Facture.MontantTotalFacture.Value);
                var remiseTxt = (Facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";
     //  NsReports.Stock.StockReportController.PrintFacture(Facture.ID, totalTxt, remiseTxt);
                NsAdministration.FrMaintenance.GenerateSoldeClient(db, DateTime.Now, true, Facture.Client);
               
                ComercialReportController.PrintBonAttribution2(Facture.ID);

                if (chSuivre.Checked)
                {
                    new FrBonLivraison(Facture.BonAttributionID.ToString()).ShowDialog();
                }

                Dispose();
            }


            catch (Exception en)
            {

                this.ShowError(en.AllMessages("Impossible d'enregistrer"));
            }
        }

        private bool IsValidAll()
        {
            string message = "";
            if (cbMode.SelectedIndex == 0 || cbMode.SelectedIndex == -1 && cbMode.Visible)
            {
                message = "Selectionner Mode payement.";
            }
            if (chRemise.Checked && txtRemise.Text.ParseToDec() == null)
            {
                //   message += "\nMontant remise invalide";
            }

            if (panCheque.Visible && (txtCheque.Text.Trim() == "" || dtDateCheque.Value.Date > DateTime.Now))
            {
                message += "\nInformations chèques invalides";
            }





            if (message == "")
            {
                return true;
            }

            this.ShowWarning(message);
            return false;
        }

        private void btAfficher_Click(object sender, EventArgs e)
        {

            var id = txtNoBon.Text.ParseToInt();

            if (id != null)
            {
                LoadBonAttribution(id.Value);
            }
            else
            {
                this.ShowWarning("Numéro Bon Invalide");
            }

        }

        private void LoadBonAttribution(int id)
        {
            this.BonAttribution = db.BonAttributions.SingleOrDefault(p => p.ID == id);
            if (BonAttribution == null)
            {
                this.ShowWarning("Numéro Bon introuvable");
                return;
            }


            if (BonAttribution.Etat == (int)EnEtatBonAttribution.Annulé)
            {
                this.ShowWarning("ce Bon est Annulé");
                return;
            }


            if (!IsEditFacture && BonAttribution.Factures.Count != 0)
            {
                var facture = BonAttribution.Factures.First();
                if (this.ConfirmInformation("Ce Bon est déja payé \nVoulez vous imprimer le Bon?"))
                {
                    var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);
                    var remiseTxt = (facture.MontantTotalRemise == 0) ? "" : "(remise comprise)";

                    ComercialReportController.PrintFacture(facture.ID, totalTxt, remiseTxt);

                    return;
                }
                return;

            }

            gbpayement.Enabled = true;
            txtClient.Text = BonAttribution.Client.NomClientAndParent();

            if (BonAttribution.Client.IsSoldable == true)
            {
                txtSolde.Text = BonAttribution.Client.Solde.ToAffInt();
                txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();
                var client = BonAttribution.Client;
                if (client.IsCompteBloque == true)
                {
                    this.ShowInformation("Ce client n'est pas autorisé à facturer avant de règler sa situation anterieure.");
                    gbpayement.Enabled = false;
                }
                else
                {
                    gbpayement.Enabled = true;


                }

            }

            else
            {
                panSolde.Visible = false;
                gbpayement.Enabled = true;
            }

            if (BonAttribution.Client.ClientTypeID == 4) // Gratuit
            {
                labMode.Text = "Gratuit";
                cbMode.Visible = false;

            }

            ListVente.Clear();
            ListVente.AddRange(BonAttribution.Ventes.ToList());


            chRemise.Enabled = false;// BonAttribution.Client.TypeClient.isRemisable.Value;
            chRemise.Checked = false;
            decimal montant = 0;
            // this.Montant = 0;
            foreach (var vente in ListVente)
            {
                if (BonAttribution.Client.ClientTypeID == 4)
                {
                    break;
                }

                var prix = vente.GoutProduit.PrixProduits.Single(
                  p => p.FormatID == vente.FormatID &&
                         p.TypeClient == BonAttribution.Client.ClientTypeID
                   );
                montant += prix.PrixUnitaire.Value * vente.TotalBouteilles.Value;
                // 

                if (/*prix.IsRemisable == true*/false)
                {
                    chRemise.Enabled = BonAttribution.Client.TypeClient.isRemisable.Value && BonAttribution.Client.isRemisable.Value;
                    chRemise.Checked = BonAttribution.Client.TypeClient.isRemisable.Value && BonAttribution.Client.isRemisable.Value;
                    //break;
                }


            }

            if (chRemise.Enabled && chRemise.Checked)
            {
                var nbrpalette = BonAttribution.Ventes.Sum(p => p.NombrePalettes);


                // BonAttribution.Ventes.Sum(p => p.TotalBouteilles * p.PrixUnitaire);

                if (nbrpalette >= 10 && (montant * 2 <= BonAttribution.Client.Solde))
                {
                    txtRemise.Text = "2";

                }
                else
                {
                    txtRemise.Text = "";
                }

                txtRemise.ReadOnly = true;
                chRemise.Enabled = false;
            }

            UpdateTotals();

            txtRemise_TextChanged(null, null);
            cbMode_SelectedIndexChanged(null, null);
            RefreshGrid();

        }


        private void RefreshGrid()
        {
            dgv.DataSource = ListVente.Select(p => new
            {
                ID = p.ID,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                REF = p.Unite.ToUniteProduitTxt(),
                Palettes = p.NombrePalettes,
                Pochettes = p.NombreFardeaux,
                Bouteilles = p.TotalBouteilles,

            }).ToList();


            dgv.HideIDColumn();
        }


        private BonAttribution BonAttribution { get; set; }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                int[] ids = { 8917, 8926, 8940 };

                foreach (var id in ids)
                {

                    var BonAttribution = db.BonAttributions.Single(p => p.ID == id);
                    var Facture = BonAttribution.Factures.First();
                    remise = 0;
                    totalRemise = 0;
                    timbre = 0;
                    ht = 0;
                    ttc = 0;
                    tva = 0;
                    totalFact = 0;


                    foreach (var vente in BonAttribution.Ventes)
                    {

                        ht += vente.PrixUnitaireHT.Value * vente.TotalBouteilles.Value;
                        ttc += vente.PrixUnitaire.Value * vente.TotalBouteilles.Value;
                    }

                    //ht = ttc * 100 / 117;

                    tva = ttc - ht;


                    totalFact = ttc;


                    Facture.MontantHT = ht;
                    Facture.MontantRemiseUnite = txtRemise.Text.ParseToDec();
                    Facture.MontantTimbre = timbre;
                    Facture.MontantTotalFacture = totalFact;
                    Facture.MontantTotalRemise = totalRemise;
                    Facture.MontantTTC = ttc;
                    Facture.MontantTVA = ttc - ht;


                }
                db.SaveChanges();
            }



            Dispose();
        }

        private void txtRemise_TextChanged(object sender, EventArgs e)
        {
            UpdateTotals();
        }

        decimal remise = 0;
        decimal totalRemise = 0;
        decimal timbre = 0;
        decimal ht = 0;
        decimal ttc = 0;
        decimal tva = 0;
        decimal totalFact = 0;
private  bool SuperGro;



        private void UpdateTotals()
        {



            if (BonAttribution.Client.ClientTypeID == 4) // Gratuit
            {
                labMode.Text = "Gratuit";
                cbMode.Visible = false;
            }

            var selectedModeID = cbMode.SelectedIndex;

            remise = 0;
            totalRemise = 0;
            timbre = 0;
            ht = 0;
            ttc = 0;
            tva = 0;
            totalFact = 0;
            //  Montant = 0;

            if (panRemise.Visible)
            {

                if (txtRemise.Text.ParseToDec() == null)
                {
                    remise = 0;
                }
                else
                {
                    remise = txtRemise.Text.ParseToDec().Value;
                }
            }
            // Montant = 0;



            foreach (var vente in BonAttribution.Ventes)
            {
                PrixProduit prix;
                if (BonAttribution.Client.ClientTypeID == 4) // gratuit
                {
                    vente.PrixUnitaire = 0;
                    vente.PrixUnitaireHT = 0;
                    vente.isRemise = false;
                }

                    

                else 
                {
                    if (BonAttribution.Client.ClientTypeID == 6 && SuperGro == true)
                    {
                        
                        prix = vente.GoutProduit.PrixProduits.SingleOrDefault(p => p.FormatID == vente.FormatID &&
                           p.TypeClient == 1);
                        if (prix == null)
                        {
                            this.ShowError("Prix du produit manquant -> contacter l'administrateur");
                            return;
                        }
                    }
                    else
                    {

                        
                           prix = vente.GoutProduit.PrixProduits.SingleOrDefault(p => p.FormatID == vente.FormatID &&
                              p.TypeClient == BonAttribution.Client.ClientTypeID);
                        if (prix == null)
                        {
                            this.ShowError("Prix du produit manquant -> contacter l'administrateur");
                            return;
                        }
                    }

                    


                    //----------------------------- remise 10 palette super grossiste 2 DA
                    bool remsie10Pal = false;


                    if (BonAttribution.Client.ClientTypeID == 1)
                    {
                        var palsCanettes = BonAttribution.Ventes.Where(p => p.FormatID == 6).Sum(p => p.NombrePalettes);
                        if (palsCanettes > 9 || BonAttribution.ClientID == 3238)
                        {
                            remsie10Pal = true;
                            remise = 2;
                        }
                        else
                        {

                            remsie10Pal = false;
                        }

                    }
                    //-------------------------------------------------------------------------------------



                    if (prix.IsRemisable.Value || (remsie10Pal == true && vente.FormatID == 6))
                    {
                        vente.isRemise = true;
                        vente.PrixUnitaire = prix.PrixUnitaire - remise;
                        vente.PrixUnitaireHT = decimal.Round((decimal)vente.PrixUnitaire * 100 / 117, 4);
                        totalRemise += remise * vente.TotalBouteilles.Value;
                    }
                    else
                    {
                        vente.isRemise = false;
                        vente.PrixUnitaire = prix.PrixUnitaire; // ici ca modify prix
                        vente.PrixUnitaireHT = prix.PrixUnitaireHT;
                    }



                }
                ht += vente.PrixUnitaireHT.Value * vente.TotalBouteilles.Value;
                ttc += vente.PrixUnitaire.Value * vente.TotalBouteilles.Value;
            }

            //ht = ttc * 100 / 117;

            tva = ttc - ht;


            totalFact = ttc;
            if (panTimbre.Visible)
            {
                timbre = ttc / 100;
                if (timbre > 2500)
                {
                    timbre = 2500;
                }

                txtTimbre.Text = timbre.ToString();
                timbre = txtTimbre.Text.ParseToDec().Value;
                totalFact += timbre;
            }

            txtMontant.Text = ttc.ToAffInt();
            txtNetPayer.Text = totalFact.ToAffInt();
            txtTimbre.Text = timbre.ToAffInt();

            //IMORTANT il ya un recalcule dans bon Livraison
        }

        public bool IsEditFacture { get; set; }

        public Facture Facture { get; set; }

        private void txtRemise_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
        }

        private void txtNoBon_Leave(object sender, EventArgs e)
        {
            this.AcceptButton = null;
        }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void chSuivre_CheckedChanged(object sender, EventArgs e)
        {

        }

        //  public decimal? Montant { get; set; }

        public bool MustSave { get; set; }

        private void FrFacture_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        public bool Saved { get; set; }
    }
}
