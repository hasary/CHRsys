﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrBonRetour : Form
    {
        ModelEntities db = new ModelEntities();
        public FrBonRetour(int? BonID = null)
        {
            InitializeComponent();
            InitialiseControls();
            ListVente = new List<Vente>();
            if (BonID != null)
            {
                BonRetour = db.BonRetours.Single(p => p.ID == BonID);
                InputBon();
             //   chSuivre.Visible = false;
                btParcourrirClient.Enabled = false;
                txtClient.ReadOnly = true;
            }
        }



        private void InputBon()
        {
            dtDate.Value = BonRetour.Date.Value;
            Client = BonRetour.Client;
            LoadClient();
            
            ListVente.AddRange(BonRetour.Ventes);
            RefreshGrid();
            UpdateControls();

        }

        private void InitialiseControls()
        {
            //--------------------------- Gout Produit
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbGoutProduit.SelectedIndex = 0;
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------------------------------------------------


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedIndex = 0;
            //--------------------------------------------------------------

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtClient.AutoCompleteCustomSource = acsc;
            txtClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtClient.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.Clients.ToList();

            if (results.Count > 0)
                foreach (var client in results)
                {
                    acsc.Add(client.Nom);
                }

            //--------------------------------------------------------------

        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListVente.Count == 0)
            {
                this.ShowWarning("La liste est vide, ajouter d'abord des éléments");
                return;
            }
          

      //      BonRetour.ModePayement = cbMode.SelectedIndex;

            



          /*  var totalMontant = ListVente.Sum(p => p.PrixUnitaire * p.TotalBouteilles);

            if (totalMontant > Client.MaxCreditMontant + Client.Solde)
            {
                if (!this.ConfirmInformation("Montant total dépasse le crédit autorisé pour ce client, si vous en mode à tèrme ce compte sera bloqué.\nContinuer quand-meme?"))
                {
                    return;
                }
            }
            */
            var currentTime = Tools.GetServerDateTime();

            if (BonRetour.EntityState == EntityState.Added)
            {
                BonRetour.InscriptionTime = Tools.GetServerDateTime();
                BonRetour.InscriptionUID = Tools.CurrentUserID;
            }
            else
            {
                BonRetour.Date = dtDate.Value;
                BonRetour.Client = this.Client;
                BonRetour.ModificationTime = currentTime;
               
            }


            btEnregistrer.Enabled = false;
            db.SaveChangeTry();
            try
            {
                NsAdministration.FrMaintenance.GenerateSoldeClient(db, null, true, BonRetour.Client);


                FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonRetour(BonRetour.ID);

                var facture = BonRetour.FactureRetours.SingleOrDefault(p => p.Etat != (int)EnEtatFacture.Annulée);
               
                if (facture != null)
                {
                    new FZBGsys.NsComercial.FrFacture(facture.ID, MustSave: true).ShowDialog();

                }
                Dispose();
            }
            catch (Exception z)
            {
                this.ShowError(z.AllMessages("Impossible de creer le rapport"));
            }

            if (true)
            {
                try
                {
                    new FrFactureRetour(BonRetour.ID.ToString()).ShowDialog();
                }
                catch (Exception be)
                {
                    this.ShowError(be.AllMessages("Impossible de Poursuivre vers Facturation"));
                }

            }

        }

        private void FrBonRetour_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("\nEtes vous sure de vouloir fermer sans enregistrer ?"))
            {
                Dispose();
            }
        }



        public BonRetour BonRetour { get; set; }
        private void txtClient_TextChanged(object sender, EventArgs e)
        {
            /*var txt = txtClient.Text.Trim();
            var clin = db.Clients.Where(p => p.Nom == txt);

            if (clin.Count() == 1)
            {
                this.Client = clin.First();
                LoadClient();
                if (BonRetour != null)
                {
                    BonRetour.ClientID = Client.ID;

                }

            }
            else if (clin.Count() > 1)
            {
                Tools.ShowError("Client en double, selectionnez depuis la liste.");
                Client = null;
                txtAdress.Text = "";

            }
            else
            {

                Client = null;
                txtAdress.Text = "";
            }
            */
        }

        private void LoadClient()
        {
            txtClient.ReadOnly = false;
            if (Client != null)
            {
                txtClient.Text = Client.Nom;
                //cbMode.SelectedIndex = Client.Adresse;
                if (Client.ClientParent != null)
                {
                    txtParent.Text = Client.ClientParent.Nom;
                }

                if (true)
                {
                    panSolde.Visible = true;
                    if (Client.IsCompteBloque == null) Client.IsCompteBloque = false;

                    txtSolde.Text = Client.Solde.ToAffInt(NullIsZero: true);

                    LabEtatCompte.Text = (Client.IsCompteBloque.Value) ? "bloqué" : "ouvert";

                }
                else
                {
                    panSolde.Visible = false;
                    panelComercial.Enabled = true;
                }

                if (Client.IsCompteBloque.Value)
                {
                    this.ShowInformation("Le Compte de ce client est bloqué en raison de dépassement de crédit autorisé et ou le délet de reglement.");
                    panelComercial.Enabled = false;
                }
                else
                {
                    panelComercial.Enabled = true;
                }
            }
            else
            {

                //txtAdress.ReadOnly = true;
            }
        }

        public List<Vente> ListVente { get; set; }
        public Client Client { get; set; }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);

            LoadClient();
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;

            }
            if (BonRetour == null)
            {
                CreateBon(); // 
            }

            var newVente = new Vente();
            //  (int)EnUniteProduit.Fardeaux;
            newVente.BonRetour = BonRetour;
            newVente.Format = (Format)cbFormat.SelectedItem;
            newVente.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;

            if (radFardeaux.Checked)
            {
                newVente.Unite = (int)EnUniteProduit.Fardeaux;
                newVente.NombreFardeaux = txtNombre.Text.ParseToInt();
                newVente.TotalFardeaux = newVente.NombreFardeaux;
                newVente.TotalBouteilles = newVente.Format.BouteillesParFardeau * newVente.NombreFardeaux;
            }
            else // palette
            {
                newVente.Unite = (int)EnUniteProduit.Palettes;
                newVente.NombrePalettes = txtNombre.Text.ParseToInt();
                newVente.TotalFardeaux = newVente.Format.FardeauParPalette * newVente.NombrePalettes;
                newVente.TotalBouteilles = newVente.Format.BouteillesParFardeau * newVente.TotalFardeaux;
            }
            newVente.PrixUnitaire = newVente.Format.PrixProduits.Single(p => p.GoutProduitID == newVente.GoutProduitID && p.TypeClient == BonRetour.Client.ClientTypeID).PrixUnitaire;

            ListVente.Add(newVente);
            //   BonRetour.Montant += newVente.PrixUnitaire * newVente.TotalBouteilles;

            db.AddToVentes(newVente);
            RefreshGrid();
            UpdateControls();

        }

        private void UpdateControls()
        {
            //throw new NotImplementedException();

            txtNombre.Text = "";
            cbGoutProduit.SelectedIndex = 0;
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListVente.Select(p => new
            {
                ID = p.ID,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                //REF = ,
                Quantite = ((p.NombrePalettes != null) ? p.NombrePalettes : p.NombreFardeaux) + " " + p.Unite.ToUniteProduitTxt(),
                Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteilles + " Bouteilles",

            }).ToList();
            // throw new NotImplementedException();

            dgv.HideIDColumn();
            UpdateTotals();

        }

        private void UpdateTotals()
        {
            var pal = ListVente.Sum(p => p.NombrePalettes);
            var fard = ListVente.Sum(p => p.NombreFardeaux);
            var bout = ListVente.Sum(p => p.TotalBouteilles);
            var sfard = ListVente.Sum(p => p.TotalFardeaux);

            if (pal != 0)
            {
                labTotal.Text = pal.ToString() + " palettes";
            }

            if (fard != 0)
            {
                labTotal.Text += "   " + fard.ToString() + " Fardeaux    ";
            }


            labTotal.Text += " =(" + sfard + " Fardeaux / " + bout + " bouteilles).";

        }

        private void CreateBon()
        {
            var date = dtDate.Value.Date;
            var no = db.BonRetours.Count(p => p.Date == date) + 1;

            BonRetour = new BonRetour()
            {
                Client = this.Client,
                Date = date,
                Etat = (int)EnEtatBonAttribution.EnInstance,
                NoFile = no


            };
        }

        private bool isValidAll()
        {
            var message = "";

            if (this.Client == null)
            {
                message += "Selectionnez un Client!";
            }

            if (cbGoutProduit.SelectedIndex == 0)
            {
                message += "\nSelectionnez un produit";
            }

            if (cbFormat.SelectedIndex == 0)
            {
                message += "\nSelectionner un Format";
            }

            if (txtNombre.Text.ParseToInt() == null)
            {
                message += "\nNombre Invalide !";
            }


            



            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            if (BonRetour != null)
            {
                BonRetour.Date = dtDate.Value.Date;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.GetSelectedIndex();

            var selected = ListVente.ElementAt(index.Value);
            BonRetour.Ventes.Remove(selected);
            ListVente.Remove(selected);
            //    BonRetour.Montant -= selected.PrixUnitaire * selected.TotalBouteilles;
            db.DeleteObject(selected);
            RefreshGrid();
            UpdateControls();

        }

        private void txtClient_Leave(object sender, EventArgs e)
        {
            //   var clientNom = txtClient.Text.Trim().ToUpper();
            //   this.Client = db.Clients.SingleOrDefault(c => c.Nom == clientNom);
        }

        private void txtClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteilles();
        }

        private void UpdateTotalBouteilles()
        {
            var Nombre = txtNombre.Text.ParseToInt();
            var Format = (Format)cbFormat.SelectedItem;
            if (Nombre != null && cbFormat.SelectedIndex != 0)
            {
                if (radPalettes.Checked)
                {
                    txtTotalBouteilles.Text = (Nombre * Format.FardeauParPalette * Format.BouteillesParFardeau).ToString();
                }
                else
                {
                    txtTotalBouteilles.Text = (Nombre * Format.BouteillesParFardeau).ToString();
                }
            }
            else
            {
                txtTotalBouteilles.Text = "";
            }
        }

        private void txtNombre_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void cbGoutProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbGoutProduit.SelectedIndex > 0)
            {
                if (cbFormat.SelectedIndex > 0)
                {
                    txtNombre.Select();
                }
                else
                {
                    cbFormat.Select();
                }
            }
        }

        private void radFardeaux_CheckedChanged(object sender, EventArgs e)
        {
            txtNombre.Text = "";
        }
    }
}
