﻿namespace FZBGsys.NsComercial.NsChauffeur
{
    partial class FrCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnregistre = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNomraison = new FZBGsys.TextBoxx();
            this.txtDatePC = new FZBGsys.TextBoxx();
            this.Activite = new System.Windows.Forms.Label();
            this.txtMatricule = new FZBGsys.TextBoxx();
            this.txtPermi = new FZBGsys.TextBoxx();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTel = new FZBGsys.TextBoxx();
            this.SuspendLayout();
            // 
            // buttonEnregistre
            // 
            this.buttonEnregistre.Location = new System.Drawing.Point(245, 212);
            this.buttonEnregistre.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEnregistre.Name = "buttonEnregistre";
            this.buttonEnregistre.Size = new System.Drawing.Size(74, 27);
            this.buttonEnregistre.TabIndex = 0;
            this.buttonEnregistre.Text = "Enregistrer";
            this.buttonEnregistre.UseVisualStyleBackColor = true;
            this.buttonEnregistre.Click += new System.EventHandler(this.buttonEnregistrerEmployer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Location = new System.Drawing.Point(167, 212);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(74, 27);
            this.buttonAnnuler.TabIndex = 1;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnulerEmployer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 28);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nom / Prenom:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 168);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Livré le:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Immatriculation:";
            // 
            // txtNomraison
            // 
            this.txtNomraison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomraison.Location = new System.Drawing.Point(97, 25);
            this.txtNomraison.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomraison.Name = "txtNomraison";
            this.txtNomraison.Size = new System.Drawing.Size(164, 20);
            this.txtNomraison.TabIndex = 6;
            this.txtNomraison.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // txtDatePC
            // 
            this.txtDatePC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDatePC.Location = new System.Drawing.Point(97, 166);
            this.txtDatePC.Margin = new System.Windows.Forms.Padding(2);
            this.txtDatePC.Name = "txtDatePC";
            this.txtDatePC.Size = new System.Drawing.Size(164, 20);
            this.txtDatePC.TabIndex = 7;
            // 
            // Activite
            // 
            this.Activite.AutoSize = true;
            this.Activite.Location = new System.Drawing.Point(14, 136);
            this.Activite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Activite.Name = "Activite";
            this.Activite.Size = new System.Drawing.Size(81, 13);
            this.Activite.TabIndex = 10;
            this.Activite.Text = "Numero Permis:";
            // 
            // txtMatricule
            // 
            this.txtMatricule.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMatricule.Location = new System.Drawing.Point(97, 49);
            this.txtMatricule.Margin = new System.Windows.Forms.Padding(2);
            this.txtMatricule.Name = "txtMatricule";
            this.txtMatricule.Size = new System.Drawing.Size(164, 20);
            this.txtMatricule.TabIndex = 7;
            // 
            // txtPermi
            // 
            this.txtPermi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPermi.Location = new System.Drawing.Point(97, 134);
            this.txtPermi.Margin = new System.Windows.Forms.Padding(2);
            this.txtPermi.Name = "txtPermi";
            this.txtPermi.Size = new System.Drawing.Size(164, 20);
            this.txtPermi.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 73);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "N° Telephone:";
            // 
            // txtTel
            // 
            this.txtTel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTel.Location = new System.Drawing.Point(97, 73);
            this.txtTel.Margin = new System.Windows.Forms.Padding(2);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(164, 20);
            this.txtTel.TabIndex = 7;
            // 
            // FrCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 267);
            this.Controls.Add(this.Activite);
            this.Controls.Add(this.txtPermi);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.txtMatricule);
            this.Controls.Add(this.txtDatePC);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNomraison);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistre);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrCreate";
            this.Text = "Nouveau Chauffeur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminEmployerNew_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEnregistre;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Activite;
        private TextBoxx txtNomraison;
        private TextBoxx txtDatePC;
        private TextBoxx txtMatricule;
        private TextBoxx txtPermi;
        private System.Windows.Forms.Label label5;
        private TextBoxx txtTel;
    }
}