﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace FZBGsys.NsComercial.NsChauffeur
{
    public partial class FrCreate : Form
    {
        public static Chauffeur ReturnChaffeur { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db;
        public FrCreate(ModelEntities db, bool IsSelectionDialogReturn)
        {
            this.db = db;
            if (db == null) this.db = new ModelEntities();
            InitializeComponent();
            InitializeData();
            this.IsSelectionDialogReturn = IsSelectionDialogReturn;
        }
        public static Chauffeur CreateChaffeurDialog(ModelEntities db, Client client)
        {
            FrCreate e = new FrCreate(db, true);
            e.Client = client;

            e.ShowDialog();

            return ReturnChaffeur;

        }
        private void InitializeData()
        {



        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnChaffeur = new Chauffeur()
                 {
                     Nom = txtNomraison.Text.Trim().ToUpper(),
                     ClientID = (this.Client == null) ? 0 : this.Client.ID,
                     DatePC = txtDatePC.Text.Trim(),
                     NumeroPC = txtPermi.Text.Trim(),
                     DefaultMatricule = txtMatricule.Text.Trim(),
                     NoTelephone = txtTel.Text.Trim(),

                 };

                if (!IsSelectionDialogReturn)
                {
                    db.AddToChauffeurs(ReturnChaffeur);

                    db.SaveChanges();
                }

                Dispose();

            }
        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 100)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 100 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Chauffeurs.FirstOrDefault(em => em.Nom == matricule);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des Chaffeurs";

                }
            }


            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialogReturn)
            {
                db.Dispose();
            }
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnChaffeur = null;
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (Client != null)
            {
//                txtClientNom.Text = Client.Nom;

            }
        }

        public Client Client { get; set; }

        private void txtNomraison_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }
    }
}
