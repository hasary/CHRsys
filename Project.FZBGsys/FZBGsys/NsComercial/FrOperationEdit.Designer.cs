﻿namespace FZBGsys.NsComercial
{
    partial class FrOperationEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSolde = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.btParcourrirClient = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labMontantRecuOperation = new System.Windows.Forms.Label();
            this.txtMontantReçuOperation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTypeOperation = new System.Windows.Forms.ComboBox();
            this.panOperation = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNouveauSolde = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panFactureClient = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMontantFacture = new System.Windows.Forms.TextBox();
            this.btAfficher = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cbDocument = new System.Windows.Forms.ComboBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.cbModePayement = new System.Windows.Forms.ComboBox();
            this.panMode = new System.Windows.Forms.Panel();
            this.panChequePiece = new System.Windows.Forms.Panel();
            this.txtChequePiece = new System.Windows.Forms.TextBox();
            this.labModePayement = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panClient = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panFournisseur = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMontantFactureFournisseur = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFournisseur = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbProduit = new System.Windows.Forms.ComboBox();
            this.txtFactureFourni = new System.Windows.Forms.TextBox();
            this.txtObservation = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbAssocier = new System.Windows.Forms.ComboBox();
            this.panOperation.SuspendLayout();
            this.panFactureClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panMode.SuspendLayout();
            this.panChequePiece.SuspendLayout();
            this.panClient.SuspendLayout();
            this.panFournisseur.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 17);
            this.label1.TabIndex = 28;
            this.label1.Text = "Solde:";
            // 
            // txtSolde
            // 
            this.txtSolde.Location = new System.Drawing.Point(148, 46);
            this.txtSolde.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSolde.Name = "txtSolde";
            this.txtSolde.ReadOnly = true;
            this.txtSolde.Size = new System.Drawing.Size(155, 22);
            this.txtSolde.TabIndex = 27;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(75, 14);
            this.txtClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(228, 22);
            this.txtClient.TabIndex = 26;
            // 
            // btParcourrirClient
            // 
            this.btParcourrirClient.Location = new System.Drawing.Point(309, 14);
            this.btParcourrirClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btParcourrirClient.Name = "btParcourrirClient";
            this.btParcourrirClient.Size = new System.Drawing.Size(43, 25);
            this.btParcourrirClient.TabIndex = 24;
            this.btParcourrirClient.Text = "...";
            this.btParcourrirClient.UseVisualStyleBackColor = true;
            this.btParcourrirClient.Click += new System.EventHandler(this.btParcourrirClient_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(317, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "DA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(300, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "DA";
            // 
            // labMontantRecuOperation
            // 
            this.labMontantRecuOperation.AutoSize = true;
            this.labMontantRecuOperation.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMontantRecuOperation.Location = new System.Drawing.Point(16, 38);
            this.labMontantRecuOperation.Name = "labMontantRecuOperation";
            this.labMontantRecuOperation.Size = new System.Drawing.Size(245, 17);
            this.labMontantRecuOperation.TabIndex = 4;
            this.labMontantRecuOperation.Text = "trente huit milles trois cent quatre vingt dix-neuf ";
            // 
            // txtMontantReçuOperation
            // 
            this.txtMontantReçuOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMontantReçuOperation.Location = new System.Drawing.Point(133, 2);
            this.txtMontantReçuOperation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMontantReçuOperation.Name = "txtMontantReçuOperation";
            this.txtMontantReçuOperation.Size = new System.Drawing.Size(153, 22);
            this.txtMontantReçuOperation.TabIndex = 3;
            this.txtMontantReçuOperation.TextChanged += new System.EventHandler(this.txtMontantReçuOperation_TextChanged);
            this.txtMontantReçuOperation.Enter += new System.EventHandler(this.txtMontantReçuOperation_Enter);
            this.txtMontantReçuOperation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontantReçuOperation_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Montant:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Type:";
            // 
            // cbTypeOperation
            // 
            this.cbTypeOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeOperation.FormattingEnabled = true;
            this.cbTypeOperation.Location = new System.Drawing.Point(91, 62);
            this.cbTypeOperation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeOperation.Name = "cbTypeOperation";
            this.cbTypeOperation.Size = new System.Drawing.Size(225, 24);
            this.cbTypeOperation.TabIndex = 0;
            this.cbTypeOperation.SelectedIndexChanged += new System.EventHandler(this.TypeOperationChanged);
            // 
            // panOperation
            // 
            this.panOperation.Controls.Add(this.txtMontantReçuOperation);
            this.panOperation.Controls.Add(this.label3);
            this.panOperation.Controls.Add(this.labMontantRecuOperation);
            this.panOperation.Controls.Add(this.label5);
            this.panOperation.Location = new System.Drawing.Point(29, 377);
            this.panOperation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panOperation.Name = "panOperation";
            this.panOperation.Size = new System.Drawing.Size(343, 65);
            this.panOperation.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 28;
            this.label6.Text = "Nouveau Solde:";
            // 
            // txtNouveauSolde
            // 
            this.txtNouveauSolde.Location = new System.Drawing.Point(148, 71);
            this.txtNouveauSolde.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNouveauSolde.Name = "txtNouveauSolde";
            this.txtNouveauSolde.ReadOnly = true;
            this.txtNouveauSolde.Size = new System.Drawing.Size(153, 22);
            this.txtNouveauSolde.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(316, 75);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "DA";
            // 
            // panFactureClient
            // 
            this.panFactureClient.Controls.Add(this.label10);
            this.panFactureClient.Controls.Add(this.txtMontantFacture);
            this.panFactureClient.Controls.Add(this.btAfficher);
            this.panFactureClient.Controls.Add(this.label12);
            this.panFactureClient.Controls.Add(this.cbDocument);
            this.panFactureClient.Controls.Add(this.txtNoBon);
            this.panFactureClient.Controls.Add(this.label11);
            this.panFactureClient.Location = new System.Drawing.Point(23, 287);
            this.panFactureClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panFactureClient.Name = "panFactureClient";
            this.panFactureClient.Size = new System.Drawing.Size(348, 80);
            this.panFactureClient.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 49);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 17);
            this.label10.TabIndex = 36;
            this.label10.Text = "Montant Facture:";
            // 
            // txtMontantFacture
            // 
            this.txtMontantFacture.Location = new System.Drawing.Point(149, 48);
            this.txtMontantFacture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMontantFacture.Name = "txtMontantFacture";
            this.txtMontantFacture.ReadOnly = true;
            this.txtMontantFacture.Size = new System.Drawing.Size(151, 22);
            this.txtMontantFacture.TabIndex = 27;
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(307, 7);
            this.btAfficher.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(35, 25);
            this.btAfficher.TabIndex = 35;
            this.btAfficher.Text = ">>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(188, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 17);
            this.label12.TabIndex = 31;
            this.label12.Text = "N°:";
            // 
            // cbDocument
            // 
            this.cbDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocument.FormattingEnabled = true;
            this.cbDocument.Items.AddRange(new object[] {
            "Facture",
            "Attribution"});
            this.cbDocument.Location = new System.Drawing.Point(85, 7);
            this.cbDocument.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbDocument.Name = "cbDocument";
            this.cbDocument.Size = new System.Drawing.Size(95, 24);
            this.cbDocument.TabIndex = 34;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(228, 7);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(73, 22);
            this.txtNoBon.TabIndex = 32;
            this.txtNoBon.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 17);
            this.label11.TabIndex = 33;
            this.label11.Text = "Document:";
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(91, 12);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(225, 22);
            this.dtDate.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date:";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 42);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(53, 17);
            this.dgv.TabIndex = 10;
            this.dgv.Visible = false;
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(238, 512);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 11;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(108, 512);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 12;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // cbModePayement
            // 
            this.cbModePayement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModePayement.FormattingEnabled = true;
            this.cbModePayement.Items.AddRange(new object[] {
            "",
            "Espèces",
            "",
            "Virement",
            "Chèque",
            "Traite"});
            this.cbModePayement.Location = new System.Drawing.Point(139, 7);
            this.cbModePayement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbModePayement.Name = "cbModePayement";
            this.cbModePayement.Size = new System.Drawing.Size(152, 24);
            this.cbModePayement.TabIndex = 29;
            this.cbModePayement.SelectedIndexChanged += new System.EventHandler(this.cbModePayement_SelectedIndexChanged);
            // 
            // panMode
            // 
            this.panMode.Controls.Add(this.panChequePiece);
            this.panMode.Controls.Add(this.label9);
            this.panMode.Controls.Add(this.cbModePayement);
            this.panMode.Location = new System.Drawing.Point(27, 96);
            this.panMode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panMode.Name = "panMode";
            this.panMode.Size = new System.Drawing.Size(307, 79);
            this.panMode.TabIndex = 13;
            // 
            // panChequePiece
            // 
            this.panChequePiece.Controls.Add(this.txtChequePiece);
            this.panChequePiece.Controls.Add(this.labModePayement);
            this.panChequePiece.Location = new System.Drawing.Point(9, 34);
            this.panChequePiece.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panChequePiece.Name = "panChequePiece";
            this.panChequePiece.Size = new System.Drawing.Size(292, 34);
            this.panChequePiece.TabIndex = 33;
            this.panChequePiece.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtChequePiece
            // 
            this.txtChequePiece.Location = new System.Drawing.Point(77, 9);
            this.txtChequePiece.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtChequePiece.Name = "txtChequePiece";
            this.txtChequePiece.Size = new System.Drawing.Size(203, 22);
            this.txtChequePiece.TabIndex = 32;
            this.txtChequePiece.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // labModePayement
            // 
            this.labModePayement.AutoSize = true;
            this.labModePayement.Location = new System.Drawing.Point(11, 9);
            this.labModePayement.Name = "labModePayement";
            this.labModePayement.Size = new System.Drawing.Size(61, 17);
            this.labModePayement.TabIndex = 31;
            this.labModePayement.Text = "Chèque:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Mode payement:";
            // 
            // panClient
            // 
            this.panClient.Controls.Add(this.btParcourrirClient);
            this.panClient.Controls.Add(this.label13);
            this.panClient.Controls.Add(this.label7);
            this.panClient.Controls.Add(this.label1);
            this.panClient.Controls.Add(this.label8);
            this.panClient.Controls.Add(this.txtClient);
            this.panClient.Controls.Add(this.label6);
            this.panClient.Controls.Add(this.txtNouveauSolde);
            this.panClient.Controls.Add(this.txtSolde);
            this.panClient.Location = new System.Drawing.Point(16, 178);
            this.panClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panClient.Name = "panClient";
            this.panClient.Size = new System.Drawing.Size(355, 102);
            this.panClient.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 17);
            this.label13.TabIndex = 28;
            this.label13.Text = "Client:";
            // 
            // panFournisseur
            // 
            this.panFournisseur.Controls.Add(this.label18);
            this.panFournisseur.Controls.Add(this.label17);
            this.panFournisseur.Controls.Add(this.label15);
            this.panFournisseur.Controls.Add(this.txtMontantFactureFournisseur);
            this.panFournisseur.Controls.Add(this.label14);
            this.panFournisseur.Controls.Add(this.txtFournisseur);
            this.panFournisseur.Controls.Add(this.label16);
            this.panFournisseur.Controls.Add(this.cbProduit);
            this.panFournisseur.Controls.Add(this.txtFactureFourni);
            this.panFournisseur.Location = new System.Drawing.Point(12, 178);
            this.panFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panFournisseur.Name = "panFournisseur";
            this.panFournisseur.Size = new System.Drawing.Size(365, 174);
            this.panFournisseur.TabIndex = 30;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(304, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(27, 17);
            this.label18.TabIndex = 37;
            this.label18.Text = "DA";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(115, 17);
            this.label17.TabIndex = 36;
            this.label17.Text = "Montant Facture:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(21, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 17);
            this.label15.TabIndex = 29;
            this.label15.Text = "Produit:";
            // 
            // txtMontantFactureFournisseur
            // 
            this.txtMontantFactureFournisseur.Location = new System.Drawing.Point(141, 92);
            this.txtMontantFactureFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMontantFactureFournisseur.Name = "txtMontantFactureFournisseur";
            this.txtMontantFactureFournisseur.Size = new System.Drawing.Size(151, 22);
            this.txtMontantFactureFournisseur.TabIndex = 27;
            this.txtMontantFactureFournisseur.TextChanged += new System.EventHandler(this.cbMontantFactureFournisseur_TextChanged);
            this.txtMontantFactureFournisseur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontantReçuOperation_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 17);
            this.label14.TabIndex = 28;
            this.label14.Text = "Fournisseur:";
            // 
            // txtFournisseur
            // 
            this.txtFournisseur.Location = new System.Drawing.Point(103, 4);
            this.txtFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFournisseur.Name = "txtFournisseur";
            this.txtFournisseur.Size = new System.Drawing.Size(193, 22);
            this.txtFournisseur.TabIndex = 26;
            this.txtFournisseur.Leave += new System.EventHandler(this.txtFournisseur_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 17);
            this.label16.TabIndex = 31;
            this.label16.Text = "N° Facture:";
            // 
            // cbProduit
            // 
            this.cbProduit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProduit.FormattingEnabled = true;
            this.cbProduit.Location = new System.Drawing.Point(103, 33);
            this.cbProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbProduit.Name = "cbProduit";
            this.cbProduit.Size = new System.Drawing.Size(193, 24);
            this.cbProduit.TabIndex = 0;
            this.cbProduit.SelectedIndexChanged += new System.EventHandler(this.TypeOperationChanged);
            // 
            // txtFactureFourni
            // 
            this.txtFactureFourni.Location = new System.Drawing.Point(141, 63);
            this.txtFactureFourni.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFactureFourni.Name = "txtFactureFourni";
            this.txtFactureFourni.Size = new System.Drawing.Size(73, 22);
            this.txtFactureFourni.TabIndex = 32;
            this.txtFactureFourni.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // txtObservation
            // 
            this.txtObservation.Location = new System.Drawing.Point(128, 450);
            this.txtObservation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtObservation.Multiline = true;
            this.txtObservation.Name = "txtObservation";
            this.txtObservation.Size = new System.Drawing.Size(237, 50);
            this.txtObservation.TabIndex = 31;
            this.txtObservation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObservation_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 454);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(85, 17);
            this.label19.TabIndex = 32;
            this.label19.Text = "Observation";
            // 
            // cbAssocier
            // 
            this.cbAssocier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAssocier.FormattingEnabled = true;
            this.cbAssocier.Items.AddRange(new object[] {
            "Associer:",
            "NMC",
            "Z.Med",
            "Z.Ndine",
            "Z.Samy",
            "Z.ElHadja"});
            this.cbAssocier.Location = new System.Drawing.Point(196, 134);
            this.cbAssocier.Name = "cbAssocier";
            this.cbAssocier.Size = new System.Drawing.Size(121, 24);
            this.cbAssocier.TabIndex = 33;
            // 
            // FrOperationEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 556);
            this.Controls.Add(this.cbAssocier);
            this.Controls.Add(this.panFournisseur);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtObservation);
            this.Controls.Add(this.panClient);
            this.Controls.Add(this.panOperation);
            this.Controls.Add(this.panMode);
            this.Controls.Add(this.panFactureClient);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.cbTypeOperation);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtDate);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrOperationEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mouvement Caisse";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrOperation_FormClosing);
            this.Load += new System.EventHandler(this.FrOperation_Load);
            this.panOperation.ResumeLayout(false);
            this.panOperation.PerformLayout();
            this.panFactureClient.ResumeLayout(false);
            this.panFactureClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panMode.ResumeLayout(false);
            this.panMode.PerformLayout();
            this.panChequePiece.ResumeLayout(false);
            this.panChequePiece.PerformLayout();
            this.panClient.ResumeLayout(false);
            this.panClient.PerformLayout();
            this.panFournisseur.ResumeLayout(false);
            this.panFournisseur.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Button btParcourrirClient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSolde;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labMontantRecuOperation;
        private System.Windows.Forms.TextBox txtMontantReçuOperation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTypeOperation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNouveauSolde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMontantFacture;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.ComboBox cbDocument;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Panel panFactureClient;
        private System.Windows.Forms.Panel panOperation;
        private System.Windows.Forms.Panel panMode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbModePayement;
        private System.Windows.Forms.Panel panClient;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panFournisseur;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFournisseur;
        private System.Windows.Forms.ComboBox cbProduit;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMontantFactureFournisseur;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFactureFourni;
        private System.Windows.Forms.TextBox txtObservation;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panChequePiece;
        private System.Windows.Forms.TextBox txtChequePiece;
        private System.Windows.Forms.Label labModePayement;
        private System.Windows.Forms.ComboBox cbAssocier;
    }
}