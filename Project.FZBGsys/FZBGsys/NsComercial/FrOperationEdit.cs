
﻿using FZBGsys.NsTools;
using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrOperationEdit : Form
    {
        ModelEntities db;
        DateTime CurrentDate;
        public FrOperationEdit(int? ID = null)
        {

            InitializeComponent();

            InitialiseData();
            InitialiseControls();
            if (ID != null)
            {
                InputOperation(ID.Value);
            }

        }

        private void InitialiseControls()
        {
            labMontantRecuOperation.Text = "";

            cbDocument.SelectedIndex = 0;

            cbTypeOperation.Items.Clear();
            this.NoOperationSelected = new TypeOperation { ID = 0, Nom = "" };
            cbTypeOperation.Items.Add(NoOperationSelected);

            cbTypeOperation.Items.AddRange(db.TypeOperations.Where(p => p.Categorie == (int)EnCatOperation.Recette && p.ID != (int)EnTypeOpration.Reglement_Créance && p.ID != (int)EnTypeOpration.Avance).OrderBy(p => p.Nom).ToArray());
            cbTypeOperation.Items.Add(NoOperationSelected);
            cbTypeOperation.Items.AddRange(db.TypeOperations.Where(p => p.Categorie == (int)EnCatOperation.Depense).OrderBy(p => p.Nom).ToArray());

            cbTypeOperation.DisplayMember = "Nom";
            cbTypeOperation.ValueMember = "ID";
            cbTypeOperation.SelectedItem = NoOperationSelected;

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtFournisseur.AutoCompleteCustomSource = acsc;
            txtFournisseur.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtFournisseur.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.Fournisseurs.ToList();

            if (results.Count > 0)
                foreach (var client in results)
                {
                    acsc.Add(client.Nom);
                }

            AutoCompleteStringCollection acscII = new AutoCompleteStringCollection();
            txtObservation.AutoCompleteCustomSource = acscII;
            txtObservation.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtObservation.AutoCompleteSource = AutoCompleteSource.CustomSource;
            acscII.Add("ZAHAF MOHAMED");
            acscII.Add("ZAHAF NOREDINE");
            acscII.Add("ZAHAF SAMIR");
            acscII.Add("ZAHAF ELHADJA");
            acscII.Add("NMC");




        }

        private void InitialiseData()
        {

            ListToSave = new List<Operation>();
            CurrentDate = Tools.GetServerDateTime();
            db = new ModelEntities();



        }

        private void TypeOperationChanged(object sender, EventArgs e)
        {
            cbAssocier.SelectedIndex = 0;
            cbModePayement.Enabled = true;
            txtMontantReçuOperation.Text = "";
            cbModePayement.SelectedIndex = 0;

            this.SelectedTypeOperation = (TypeOperation)cbTypeOperation.SelectedItem;
            panClient.Visible =
                                SelectedTypeOperation.ID == (int)EnTypeOpration.Avance ||
                                SelectedTypeOperation.ID == (int)EnTypeOpration.Payement_au_Comptant ||
                                SelectedTypeOperation.ID == (int)EnTypeOpration.Reglement_Créance ||
                                SelectedTypeOperation.ID == (int)EnTypeOpration.Remboursement_Client_Traite
                                ;
            var tid = (int)EnTypeOpration.Payement_Fournisseur;

            cbAssocier.Visible = SelectedTypeOperation.ID == (int)EnTypeOpration.Associes ||
                                 SelectedTypeOperation.ID == (int)EnTypeOpration.Pret_Associer_NMC_ENTREE ||
                                 SelectedTypeOperation.ID == (int)EnTypeOpration.Pret_ASSOCIER_NMC_SORTIE ||
                                 SelectedTypeOperation.ID == (int)EnTypeOpration.Rembourssement_Pret_Associer_NMC_SORTIE;


            panFournisseur.Visible = (SelectedTypeOperation.ID == tid);
            panMode.Visible = SelectedTypeOperation.IsMultiModePayement == true;
            panFactureClient.Visible = SelectedTypeOperation.ID == (int)EnTypeOpration.Payement_au_Comptant;
            if (SelectedTypeOperation.IsMultiModePayement != true)
            {
                cbModePayement.SelectedIndex = (int)EnModePayement.Especes;
            }

        }

        private void btAfficher_Click(object sender, EventArgs e)
        {
            panFactureClient.Enabled = false;
            LoadFacture();
            panFactureClient.Enabled = true;

        }

        private void LoadFacture()
        {
            var id = txtNoBon.Text.Trim().ParseToInt();

            BonAttribution bonAttribution = null;

            if (cbDocument.SelectedIndex == 0)
            {
                this.SelectedFacture = db.Factures.SingleOrDefault(p => p.ID == id);
                if (SelectedFacture == null)
                {
                    this.ShowWarning("N° de Facture introuvable");
                    return;

                }
                else if (SelectedFacture.ModePayement != (int)EnModePayement.Especes)
                {

                }
                bonAttribution = SelectedFacture.BonAttribution;
            }
            else
            {
                bonAttribution = db.BonAttributions.SingleOrDefault(p => p.ID == id);
                if (bonAttribution == null)
                {
                    this.ShowWarning("N° de Bon introuvable");
                    return;

                }
                else if (bonAttribution.Factures.Count == 0)
                {
                    this.ShowWarning("Ce Bon n'est pas encore Facturé");
                    return;
                }
                else
                {
                    SelectedFacture = bonAttribution.Factures.Single();
                }

            }


            if (bonAttribution.Client != SelectedClient)
            {
                if (SelectedClient == null || this.ConfirmWarning("Cette Bon ne correspond pas le client choisi, Modifier le client?"))
                {
                    SelectedClient = bonAttribution.Client;
                    LoadClient();

                }
                else
                {

                    return;
                }

            }

            cbModePayement.SelectedIndex = SelectedFacture.ModePayement.Value;
            cbModePayement.Enabled = false;

            txtMontantFacture.Text = SelectedFacture.MontantTotalFacture.ToAffInt();
            txtMontantReçuOperation.Text = SelectedFacture.MontantTTC.ToAffInt();
            panFactureClient.Enabled = true;

        }

        private void LoadClient()
        {
            if (SelectedClient.Solde == null)
            {
                SelectedClient.Solde = 0;

            }
            this.TempSolde = SelectedClient.Solde.Value;
            var list = ListToSave.Where(p => p.Client.ID == SelectedClient.ID &&
                                (
                p.TypeOperaitonID == (int)EnTypeOpration.Avance ||
                p.TypeOperaitonID == (int)EnTypeOpration.Payement_au_Comptant ||
                p.TypeOperaitonID == (int)EnTypeOpration.Reglement_Créance ||
                p.TypeOperaitonID == (int)EnTypeOpration.Remboursement_Client_Traite

                )).ToList();
            foreach (var operation in list)
            {
                if (operation.Client.ID == SelectedClient.ID)
                {
                    if (operation.Sen == "D")
                    {
                        TempSolde -= operation.Montant.Value;

                    }
                    else
                    {
                        TempSolde += operation.Montant.Value;
                    }
                }
            }

            txtClient.Text = SelectedClient.Nom;
            txtSolde.Text = TempSolde.ToAffInt();


            if (SelectedClient.IsCompteBloque == true)
            {
                cbTypeOperation.SelectedIndex = (int)EnTypeOpration.Reglement_Créance;
                cbTypeOperation.Enabled = false;
            }

        }

        public Client SelectedClient { get; set; }

        private void txtMontantReçuOperation_TextChanged(object sender, EventArgs e)
        {

            var montant = txtMontantReçuOperation.Text.Trim().ParseToDec();
            if (montant != null)
            {
                if (SelectedClient != null)
                {
                    if (SelectedClient.Solde != null)
                    {
                        txtNouveauSolde.Text = (TempSolde + montant).ToAffInt();
                    }
                    else
                    {
                        txtNouveauSolde.Text = (montant).ToAffInt();
                    }
                }
                labMontantRecuOperation.Text = CtoL.convertMontant(montant.Value);
            }
            else
            {
                txtNouveauSolde.Text = "";
                labMontantRecuOperation.Text = "";
            }
        }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            SelectedClient = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            //  ClearControls();
            if (SelectedClient.isExpire == true)
            {
                if (SelectedClient.Solde >= 0)
                {
                    Tools.ShowError("Ce compte client est fermé (ancien dossier)");
                    SelectedClient = null;
                }
            }
            if (SelectedClient != null)
            {
                LoadClient();
            }
        }

        private void ClearControls()
        {
            txtNoBon.Text = "";
            txtMontantReçuOperation.Text = "";
            txtMontantFacture.Text = "";
            txtNouveauSolde.Text = "";
            txtObservation.Text = "";
            SelectedClient = null;
            SelectedFacture = null;
            SelectedFournisseur = null;
            cbTypeOperation.SelectedIndex = 0;
            //panOperation.Enabled = SelectedClient != null;
        }

        private void txtMontantReçuOperation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',' && e.KeyChar != '-')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }


        }

        public Operation newOperation { get; set; }

        private void EditOperation()
        {

            decimal montant = 0;

            newOperation.Client = null;
            newOperation.Fournisseur = null;

            montant = txtMontantReçuOperation.Text.Trim().ParseToDec().Value;


            //var newOperation = new Operation();
            newOperation.Montant = montant;

            newOperation.TypeOperation = (TypeOperation)cbTypeOperation.SelectedItem;
            newOperation.Client = SelectedClient;
            //   newOperation.ClientID = (SelectedClient == null) ? 0 : SelectedClient.ID;
            newOperation.Sen = (SelectedTypeOperation.Categorie == (int)EnCatOperation.Recette) ? "C" : "D";
            newOperation.InscriptionUID = Tools.CurrentUserID;
            newOperation.InscriptionDate = CurrentDate;
            newOperation.Date = dtDate.Value.Date;
            newOperation.Observation = txtObservation.Text.Trim();
            newOperation.TypeOperation = SelectedTypeOperation;
            newOperation.Lib = GetOperationDescription(newOperation);
            newOperation.Facture = SelectedFacture;
            if (!panFournisseur.Visible) { txtFournisseur.Text = ""; LoadFournisseur(); }
            newOperation.Fournisseur = SelectedFournisseur;
            //  newOperation.FournisseurID = (SelectedFournisseur == null) ? 0 : SelectedFournisseur.ID;
            newOperation.ModePayement = (panMode.Visible) ? cbModePayement.SelectedIndex : 1; // 1 == espèce
            if (newOperation.TypeOperaitonID == (int)EnTypeOpration.Prevision_de_Dépenses)
            {
                newOperation.ModePayement = 0; // prévision
            }
            if (newOperation.Fournisseur == null) newOperation.FournisseurID = 0;
            if (newOperation.Client == null) newOperation.ClientID = 0;


            Operation newOperationAv = null;
            if (newOperation.TypeOperaitonID == (int)EnTypeOpration.Reglement_Créance)
            {
                var montantAvance = this.TempSolde + montant;

                if (montantAvance > 0)
                {
                    newOperation.Montant = -this.TempSolde;

                    newOperationAv = new Operation();
                    newOperationAv.TypeOperation = db.TypeOperations.Single(p => p.ID == (int)EnTypeOpration.Avance);
                    newOperationAv.Lib = GetOperationDescription(newOperationAv);
                    newOperationAv.Date = newOperation.Date;
                    newOperationAv.Client = newOperation.Client;
                    newOperationAv.FournisseurID = 0;
                    newOperationAv.InscriptionDate = newOperation.InscriptionDate;
                    newOperationAv.InscriptionUID = newOperation.InscriptionUID;
                    newOperationAv.Sen = "C";
                    newOperationAv.Montant = montantAvance;
                    newOperationAv.ModePayement = newOperation.ModePayement;
                    if (newOperationAv.Fournisseur == null) newOperation.FournisseurID = 0;
                    if (newOperationAv.Client == null) newOperation.ClientID = 0;

                }
                else
                {
                    newOperation.Montant = montant;
                }
            }
            else
            {
                newOperation.Montant = montant;
            }



            ListToSave.Add(newOperation);
            if (newOperationAv != null)
            {
                ListToSave.Add(newOperationAv);
            }
            var date = dtDate.Value.Date;
            var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == date);
            if (hist != null && hist.EntityState != EntityState.Deleted)
            {
                db.DeleteObject(hist);
            }

            RefreshGrid();
            SelectedClient = null;
            txtClient.Text = "";
            txtSolde.Text = "";
            //    newOperation.Client.SoldeHistory = newOperation.Montant;
            //    newOperation.Client.DateHistory = DateTime.Parse("2012-07-23");
            if (newOperation.TypeOperaitonID == (int)EnTypeOpration.Avance || newOperation.TypeOperaitonID == (int)EnTypeOpration.Reglement_Créance)
            {
                newOperation.Client.DateDernierVersement = newOperation.Date;
            }
            db.SaveChanges();
            ClearControls();


        }

        private string GetOperationDescription(Operation operation)
        {
            var returnDescription = "";
            if (panFactureClient.Visible)
            {
                returnDescription += "FAC N°" + SelectedFacture.ID;
                if (panChequePiece.Visible && txtChequePiece.Text.Trim() != "")
                {
                    returnDescription += " " + txtChequePiece.Text.Trim();
                }
            }
            else if (panFournisseur.Visible)
            {
                if (cbProduit.SelectedIndex != 0)
                {
                    returnDescription += ((TypeRessource)cbProduit.SelectedItem).Nom.ToUpper();
                }

                if (txtFactureFourni.Text.Trim() != "")
                {
                    returnDescription += " FACT N°" + txtFactureFourni.Text.Trim();
                }
            }
            else if (cbAssocier.Visible)
            {
                if (cbAssocier.SelectedIndex != 0)
                {
                    returnDescription += cbAssocier.SelectedItem.ToString();
                }
            }

            return returnDescription;
        }


        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                Date = p.Date,
                Sens = (p.TypeOperation != null) ? p.TypeOperation.Categorie.ToCategorieOperationTxt() : "/",
                Operation = (p.TypeOperation != null) ? p.TypeOperation.Nom : "/",
                Sujet = (p.Client != null) ? p.Client.NomClientAndParent() : ((p.Fournisseur != null) ? p.Fournisseur.Nom : ""),
                Libele = p.Lib,
                Montant = p.Montant.ToAffInt(),
                Mode = p.ModePayement.ToModePayementTxt(),

            }).ToList();
        }
        bool isBoss = false;

        public List<Operation> ListToSave { get; set; }
        private bool IsValidAll()
        {


            var message = "";

            if (panClient.Visible && SelectedClient == null)
            {
                message = "Selectionner un client!";

            }
            else

                if (cbTypeOperation.SelectedIndex < 0 || SelectedTypeOperation == NoOperationSelected)
                {
                    message += "\nSelectionner une Opération dans la liste!";
                }
                else if (panClient.Visible && this.TempSolde < 0 && ((TypeOperation)cbTypeOperation.SelectedItem).ID == (int)EnTypeOpration.Avance)
                {
                    message += "\nClient avec un compte débiteur ne peut pas faire des avances, choisissez réglement créance!";
                }
                else if (panClient.Visible && !(this.TempSolde < 0) && ((TypeOperation)cbTypeOperation.SelectedItem).ID == (int)EnTypeOpration.Reglement_Créance)
                {
                    message += "\nCe client n'a aucune créance à régler!";
                }
                else if (panChequePiece.Visible && txtChequePiece.Text.Trim() == "")
                {
                    message += "\nEntrez Informations " + cbModePayement.Text + " !!";
                }
                else if (cbAssocier.Visible && cbAssocier.SelectedIndex == 0)
                {
                    message += "\nSelectionner un Associer";
                }
                else

                    if (panClient.Visible)
                    {
                        var montant = txtMontantReçuOperation.Text.ParseToDec();

                        if (montant < 0)
                        {
                            var op = SelectedClient.Operations.OrderByDescending(p => p.InscriptionDate).FirstOrDefault();

                            if (op != null)
                            {
                                var facts = SelectedClient.Factures.Where(p => p.InscriptionTime >= op.InscriptionDate);
                                if (facts.Count() > 0)
                                {
                                    message += "\nVous devez d'abord supprimer les factures concernant le client et qui date apres le " + op.InscriptionDate.Value.ToLongTimeString();
                                }
                            }
                        }
                    }

            if (txtMontantReçuOperation.Text.Trim().ParseToDec() == null)
            {
                message += "\nMontant Invalide !!!!!";
            }

            if (panFactureClient.Visible && SelectedFacture == null)
            {
                message += "\nEntrez numéro facture ou attribution!";
            }

            if (panMode.Visible && (cbModePayement.SelectedIndex == 0 || cbModePayement.Text == ""))
            {
                message += "\nSelectionnez un mode de payement!";
            }
            var date = dtDate.Value.Date;
            var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == date);

            if (hist != null)
            {
                message = "vous ne pouvez pas ajouter sur une journée cloturée.";
            }

            if (message == "")
            {
                return true;
            }
            else
            {
                this.ShowWarning(message);
                return false;
            }
        }

        private void InputOperation(int ID)
        {

            newOperation = db.Operations.Single(p => p.ID == ID);
            dtDate.Value = newOperation.Date.Value;
            dtDate.Enabled = false;
            if (newOperation.Client != null && newOperation.ClientID != 0)
            {
                this.ShowInformation("Vous ne pouvez pas modifier ce type d'operation");
                btEnregistrer.Enabled = false;
                return;
            }

            SelectedClient = newOperation.Client;
            LoadClient();
            cbTypeOperation.SelectedItem = newOperation.TypeOperation;

            if (newOperation.Client != null)
            {
                panClient.Visible = true;
                SelectedClient = newOperation.Client;
                LoadClient();
            }

            if (newOperation.Facture != null)
            {
                panFactureClient.Visible = true;
                SelectedFacture = newOperation.Facture;
                LoadFacture();
            }
            txtObservation.Text = newOperation.Observation;

            if (newOperation.Fournisseur != null)
            {
                panFournisseur.Visible = true;
                txtFournisseur.Text = newOperation.Fournisseur.Nom;
                LoadFournisseur();
                SelectedFournisseur = newOperation.Fournisseur;


            }

            cbModePayement.SelectedIndex = newOperation.ModePayement.Value;
            panMode.Visible = newOperation.ModePayement > 1;
            txtMontantReçuOperation.Text = newOperation.Montant.ToAffInt();



        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {


            this.Dispose();

        }

        private void FrOperation_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }
            EditOperation();

            if (newOperation.ClientID != null)
            {
                newOperation.Client.DateDernierMouvement = newOperation.Client.DateDernierMouvement = newOperation.Date;
                newOperation.Client.DateDernierMouvement = newOperation.Client.DateDernierVersement = newOperation.Date;

            }


            switch ((EnTypeOpration)newOperation.TypeOperaitonID)
            {
                case EnTypeOpration.Remboursement_Client_Traite:
                    newOperation.Client.Solde -= newOperation.Montant;
                    break;
                case EnTypeOpration.Reglement_Créance:


                    if (newOperation.Client.Solde >= 0)
                    {
                        newOperation.Client.IsCompteBloque = false;
                    }
                    break;
                case EnTypeOpration.Avance:
                    if (newOperation.Client.Solde != null)
                    {

                        if (newOperation.Client.Solde >= 0)
                        {
                            newOperation.Client.IsCompteBloque = false;
                        }
                    }
                    else
                    {

                        newOperation.Client.IsSoldable = true;
                    }
                    break;
                case EnTypeOpration.Payement_au_Comptant:
                    db.Factures.Single(p => p.ID == newOperation.FactureID).Etat = (int)EnEtatFacture.Payée;

                    break;
                default:
                    break;
            }

            db.SaveChangeTry();

            var clientsAffected = ListToSave.Where(p => p.ClientID != null).Select(p => p.Client).ToList().Distinct().ToList();
            foreach (var client in clientsAffected)
            {
                NsAdministration.FrMaintenance.GenerateSoldeClient(db, null, true, client);
            }
            db.SaveChanges();

            var todel = db.Operations.Where(p => p.ModePayement == null).ToList();
            if (todel.Count != 0)
            {
                foreach (var item in todel)
                {
                    db.DeleteObject(item);
                }
                db.SaveChangeTry();
            }


            Dispose();

        }

        private void txtMontantRecuPayement_KeyUp(object sender, KeyEventArgs e)
        {

        }

        public Facture SelectedFacture { get; set; }

        private void txtMontantReçuOperation_Enter(object sender, EventArgs e)
        {
            //  this.AcceptButton = btAjouter;
        }

        private void txtMontantRecuPayement_Enter(object sender, EventArgs e)
        {
            //this.AcceptButton = btAjouter;
        }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void tcOperation_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public TypeOperation NoOperationSelected { get; set; }

        public TypeOperation SelectedTypeOperation { get; set; }

        private void txtFournisseur_Leave(object sender, EventArgs e)
        {
            LoadFournisseur();
        }

        private void LoadFournisseur()
        {
            var txt = txtFournisseur.Text.Trim();
            var fourn = db.Fournisseurs.Where(p => p.Nom == txt);

            cbProduit.Items.Clear();

            if (fourn.Count() == 1)
            {
                this.SelectedFournisseur = fourn.First();

                cbProduit.Items.Clear();
                var produits = db.TypeRessources.ToList().Where(p => p.Fournisseurs.Contains(SelectedFournisseur));

                cbProduit.Items.Add(new TypeRessource { ID = 0, Nom = "/" });
                cbProduit.Items.AddRange(produits.ToArray());
                if (produits.Count() == 1)
                {
                    cbProduit.SelectedItem = produits.First();
                }
                else
                {
                    cbProduit.SelectedIndex = 0;
                }

                cbProduit.ValueMember = "ID";
                cbProduit.DisplayMember = "Nom";
                cbProduit.DropDownStyle = ComboBoxStyle.DropDownList;


            }
            #region MyRegion
            /* else if (fourn.Count() > 1)
             {
                // Tools.ShowError("Fournisseur en double, selectionnez depuis la liste.");
                 SelectedFournisseur = null;


             }*/
            #endregion
            else
            {

                SelectedFournisseur = null;

            }
        }

        public Fournisseur SelectedFournisseur { get; set; }

        private void cbMontantFactureFournisseur_TextChanged(object sender, EventArgs e)
        {
            var value = txtMontantFactureFournisseur.Text.Trim().ParseToDec();
            if (value != null)
            {
                txtMontantReçuOperation.Text = value.ToAffInt();
            }
            else
            {
                txtMontantReçuOperation.Text = "";
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbModePayement_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbModePayement.SelectedIndex == 3 || cbModePayement.SelectedIndex == 4 || cbModePayement.SelectedIndex == 5)
            {
                panChequePiece.Visible = true;
                labModePayement.Text = cbModePayement.Text.Trim() + ":";
            }
            else
            {
                panChequePiece.Visible = false;
            }
        }

        private void txtObservation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = char.ToUpper(e.KeyChar);

        }

        public decimal TempSolde { get; set; }

        private void FrOperation_Load(object sender, EventArgs e)
        {

        }
    }
}
