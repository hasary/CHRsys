﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrPrixProduitList : Form
    {
        ModelEntities db = new ModelEntities();

        public FrPrixProduitList()
        {
            InitializeComponent();
            IntialiseControls();




            RefreshDataGrid();

        }

        private void IntialiseControls()
        {
            var tt = new TypeClient { ID = 0, Nom = "[Tout]" };
            cbCategorie.Items.Add(tt);
            cbCategorie.Items.AddRange(db.TypeClients.ToArray());
            cbCategorie.SelectedItem = tt;
            cbCategorie.ValueMember = "ID";
            cbCategorie.DisplayMember = "Nom";

            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.ToArray());
            cbFormat.SelectedIndex = 0;
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
        }

        private void RefreshDataGrid()
        {
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.PrixProduits);
            var data = db.PrixProduits.ToList();

            if (cbCategorie.SelectedIndex > 0)
            {
                var cat = (TypeClient)cbCategorie.SelectedItem;
                data = data.Where(p => p.TypeClient == cat.ID).ToList();
            }


            if (cbFormat.SelectedIndex > 0)
            {
                var form = (Format)cbFormat.SelectedItem;
                data = data.Where(p => p.FormatID == form.ID).ToList();
            }


            dgv1.DataSource = data.Select(p => new
            {
                GoutID = p.GoutProduitID,
                FormatID = p.FormatID,
                CatID = p.TypeClient,
                
                Type = p.GoutProduit.TypeProduit.Nom,
                Gout = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                Categorie = p.TypeClient1.Nom,
                PrixTTC = p.PrixUnitaire,
                PrixHTT = p.PrixUnitaireHT,
                Infos = "Modifié par "+p.ApplicationUtilisateur.Employer.Nom + "   "+ p.DateDernierModif.Value.ToString("dd/MM/yyyy à hh:mm")


            }).OrderBy(p=>p.Type).ToList();

            dgv1.HideIDColumn("GoutID");
            dgv1.HideIDColumn("FormatID");
            dgv1.HideIDColumn("CatID");
        }

        private void FrPrixProduitList_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void buttonModifier_Click(object sender, EventArgs e)
        {
            
            //var id = dgv1.GetSelectedID();
            var idgout = int.Parse(dgv1.SelectedRows[0].Cells["GoutID"].Value.ToString());
            var idformat = int.Parse(dgv1.SelectedRows[0].Cells["FormatID"].Value.ToString());
            var idcat = int.Parse(dgv1.SelectedRows[0].Cells["CatID"].Value.ToString());
            var prix = db.PrixProduits.Single(p => p.FormatID == idformat && p.GoutProduitID == idgout && p.TypeClient == idcat);

            new FrPrixProduitEdit(prix).ShowDialog();
           
            RefreshDataGrid();
        }

        private void cbCategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshDataGrid();
        }

        private void buttonNouveau_Click(object sender, EventArgs e)
        {
            new FrPrixProduitEdit().ShowDialog();
            
            RefreshDataGrid();
        }
    }
}
