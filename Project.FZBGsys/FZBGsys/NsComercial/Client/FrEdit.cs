﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys;

using FZBGsys.NsTools;


namespace FZBGsys.NsComercial.NsClient
{
    public partial class FrEdit : Form
    {
        public static Client ReturnClient { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db = new ModelEntities();
        public FrEdit(int IDClient, bool isCreationTemp = false, string TempNom = "", string TempAdresse = "")
        {
            this.isCreationTemp = isCreationTemp;
            this.IDClient = IDClient;
            InitializeComponent();

            cbTypeClient.Items.Clear();
            cbTypeClient.Items.Add(new TypeClient { ID = 0, Nom = "" });
            cbTypeClient.Items.AddRange(db.TypeClients.Where(p => p.ID > 0).ToArray());
            cbTypeClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeClient.SelectedIndex = 0;
            cbTypeClient.ValueMember = "ID";
            cbTypeClient.DisplayMember = "Nom";


            cbCategorieClient.Items.Clear();
            cbCategorieClient.Items.Add(new CategorieClient { ID = 0, Nom = "" });
            cbCategorieClient.Items.AddRange(db.CategorieClients.ToArray());
            cbCategorieClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorieClient.SelectedIndex = 0;
            cbCategorieClient.ValueMember = "ID";
            cbCategorieClient.DisplayMember = "Nom";

            InitializeData(TempNom, TempAdresse);
            IsSelectionDialogReturn = false;

            if (Tools.isBoss != true)
            {
                tcClient.TabPages.Remove(tpAutorisation);
                tcClient.TabPages.Remove(tpCD);
            }
            else
            {
                btActiveNouveau.Visible = ReturnClient.IsCompteBloque.Value;
            }
        }

        public FrEdit(bool ActiveAutorisation)
        {
            //   this.db = db;

            InitializeComponent();
            this.IsSelectionDialogReturn = false;
            InitializeData();
            /*  if (ActiveAutorisation)
              {
                  tcClient.TabPages.Remove(tpAutorisation);
              }
  */
            if (Tools.isBoss != true)
            {
                tcClient.TabPages.Remove(tpAutorisation);
                tcClient.TabPages.Remove(tpCD);
            }

        }


        private void InitializeData(string TempNom = "", string TempAdresse = "")
        {
            chExpire.Enabled = Tools.CurrentUser.ApplicationGroupe.IsBoss == true;
            
            Canceled = null;
            if (!isCreationTemp)
            {
                ReturnClient = db.Clients.Single(c => c.ID == this.IDClient);
            }
            else
            {
                ReturnClient = new Client() { Nom = TempNom, Adresse = TempAdresse, NoFiscale = "", NoArticle = "", NoRegistre = "", Telephone = "" };
            }
            chRemise.Checked = ReturnClient.isRemisable == true;
            txtCodeSage.Text = ReturnClient.CodeSage;
            txtAdresse.Text = ReturnClient.Adresse;
            txtNomraison.Text = ReturnClient.Nom;
            txtTelephone.Text = ReturnClient.Telephone;
            txtRegistre.Text = ReturnClient.NoRegistre;
            txtArticle.Text = ReturnClient.NoArticle;
            txtFiscal.Text = ReturnClient.NoFiscale;
            chAutorise.Checked = ReturnClient.IsAutoriseCredit != false;
            txtMontantAutorise.Text = ReturnClient.MaxCreditMontant.ToAffInt();
            nud.Value = (ReturnClient.MaxCreditDeletJours != null) ? ReturnClient.MaxCreditDeletJours.Value : 0;
            cbTypeClient.SelectedItem = ReturnClient.TypeClient;
            cbwilaya.SelectedItem = ReturnClient.Wilaya;
            //  txtMontantAutorise.Text = "";
            if (ReturnClient.MaxCreditMontant == null)
            {
                labMontant.Text = "";
            }

            txtIntiChrea.Text = ReturnClient.InitChrea.ToString();
            InitOrangeChrea.Text = ReturnClient.InitOrangeChrea.ToString();
            
            txtPanierChrea.Text = ReturnClient.PanierChrea.ToString();
            txtPanierOrangeChrea.Text = ReturnClient.PanierOrangeChrea.ToString();

            CDChrea.Text = ReturnClient.CDChrea.ToString();
            CDOrangeChrea.Text = ReturnClient.CDOrangeChrea.ToString();
                


            chExpire.Checked = ReturnClient.isExpire == true;
            cbParentClient.Items.Add(new ClientParent { ID = 0, Nom = "" });
            cbParentClient.Items.AddRange(db.ClientParents.Where(p => p.ID > 0).ToArray());
            cbParentClient.SelectedIndex = 0;
            cbParentClient.ValueMember = "ID";
            cbParentClient.DisplayMember = "Nom";


            if (ReturnClient.ClientParentID != 0)
            {
                chIsParent.Checked = true;
                cbParentClient.SelectedItem = ReturnClient.ClientParent;
            }
            else
            {
                chIsParent.Checked = false;
            }
        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {

                ReturnClient.Nom = txtNomraison.Text.Trim();
                ReturnClient.Adresse = txtAdresse.Text.Trim();
                ReturnClient.Telephone = txtTelephone.Text.Trim();
                ReturnClient.isExpire = chExpire.Checked;
                ReturnClient.isRemisable = chRemise.Checked;


                ReturnClient.TypeClient = (TypeClient)cbTypeClient.SelectedItem;
                if (ReturnClient.TypeClient.HasRegistre.Value)
                {
                    ReturnClient.NoRegistre = txtRegistre.Text.Trim().ToUpper();
                    ReturnClient.NoArticle = txtArticle.Text.Trim().ToUpper();
                    ReturnClient.NoFiscale = txtFiscal.Text.Trim();
                    ReturnClient.CodeSage = txtCodeSage.Text.Trim();
                }
                else
                {
                    ReturnClient.NoRegistre = null;
                    ReturnClient.NoArticle = null;
                    ReturnClient.NoFiscale = null;
                    ReturnClient.CodeSage = "DV" + ReturnClient.ID;
                }

                ReturnClient.CategorieClient = (CategorieClient)cbCategorieClient.SelectedItem;
                if (chIsParent.Checked)
                {
                    var selected = cbParentClient.SelectedItem;
                    if (selected != null)
                    {
                        var parent = (ClientParent)cbParentClient.SelectedItem;
                        ReturnClient.ClientParentID = parent.ID;


                    }
                    else // new parent
                    {
                        ReturnClient.ClientParent = new ClientParent() { Nom = cbParentClient.Text.ToString() };
                    }
                }
                else
                {
                    ReturnClient.ClientParentID = 0;
                }

                ReturnClient.IsAutoriseCredit = chAutorise.Checked;
                if (chAutorise.Checked)
                {
                    ReturnClient.MaxCreditMontant = txtMontantAutorise.Text.ParseToDec();
                    ReturnClient.MaxCreditDeletJours = (int)nud.Value;
                }
                else
                {
                    ReturnClient.MaxCreditMontant = null;
                    ReturnClient.MaxCreditDeletJours = null;
                }

                ReturnClient.CDChrea = CDChrea.Text.ParseToInt();
                ReturnClient.CDOrangeChrea = CDOrangeChrea.Text.ParseToInt();

                ReturnClient.InitChrea = txtIntiChrea.Text.ParseToInt();
                ReturnClient.InitOrangeChrea = InitOrangeChrea.Text.ParseToInt();

                ReturnClient.PanierChrea = txtPanierChrea.Text.ParseToInt();
                ReturnClient.PanierOrangeChrea = txtPanierOrangeChrea.Text.ParseToInt();

                ReturnClient.Wilaya = cbwilaya.SelectedItem.ToString();

                db.SaveChanges();
                Canceled = false;
                Dispose();

            }
        }

        public static Client createClientTemp(string tempNom, string tempAdre)
        {

            new FrEdit(0, true, tempNom, tempAdre).ShowDialog();
            return ReturnClient;


        }

        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;


            if (panReg.Visible)
            {
                var code = txtCodeSage.Text.Trim();
                if (!code.StartsWith("CL") || code.Replace("CL", "").ParseToInt() == null)
                {
                   // ErrorMessage = "Code Sage Invalide!";
                }
                else
                {
                    var existingCode = db.Clients.Where(p => p.CodeSage == code && p.ID != ReturnClient.ID);
                    if (existingCode.Count() != 0)
                    {
                        ErrorMessage = "Ce code Sage existe déja pour un autre client!!!!";
                    }
                } 
            }

            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 70)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 70 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Clients.FirstOrDefault(em => em.Nom == matricule && em.ID != IDClient);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des client";

                }
            }
            if (cbTypeClient.SelectedIndex < 1)
            {
                ErrorMessage += "\nVeillez selectionner un type Client";
            }

            if (txtAdresse.Text == "")
            {
                ErrorMessage += "\nVeillez Entrer Adresse Client";
            }

            if (panReg.Visible)
            {
                //  informations registre commerce
            }

            if (chIsParent.Checked && cbParentClient.SelectedIndex == 0)
            {
                ErrorMessage += "\nVeillez spécifier ou selectionné le nom du parent";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Canceled == null)
            {
                Canceled = true;
            }
            db.Dispose();
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnClient = null;

            Dispose();
        }

        public int IDClient { get; set; }

        public static bool? Canceled { get; set; }

        public bool isCreationTemp { get; set; }



        private void chIsParent_CheckedChanged(object sender, EventArgs e)
        {
            cbParentClient.Visible = chIsParent.Checked;
        }

        private void cbTypeClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTypeClient.SelectedIndex > 0)
            {
                panReg.Visible = ((TypeClient)cbTypeClient.SelectedItem).HasRegistre.Value;
            }

        }

        private void cbParentClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var montant = txtMontantAutorise.Text.Trim().ParseToDec();
            if (montant != null)
            {
                labMontant.Text = CtoL.convertMontant(montant.Value);
            }
        }

        private void chAutorise_CheckedChanged(object sender, EventArgs e)
        {
            panFactAterme.Enabled = chAutorise.Checked;
        }

        private void cbTypeClient_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var TypeClient = (TypeClient)cbTypeClient.SelectedItem;
            if (TypeClient.ID > 0)
            {
                panReg.Visible = true;// TypeClient.HasRegistre.Value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.ConfirmInformation("Etes Vous sur de vouloir activer ce client ?"))
            {
                ReturnClient.IsCompteBloque = false;
                db.SaveChanges();
                Tools.ShowInformation("Client activée avec succée");
                Dispose();
            }
        }

        
    }
}
