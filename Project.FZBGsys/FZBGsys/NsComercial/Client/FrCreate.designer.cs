﻿namespace FZBGsys.NsComercial.NsClient
{
    partial class FrCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnregistre = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.tcClient = new System.Windows.Forms.TabControl();
            this.tpInformation = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chIsParent = new System.Windows.Forms.CheckBox();
            this.cbwilaya = new System.Windows.Forms.ComboBox();
            this.cbParentClient = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Activite = new System.Windows.Forms.Label();
            this.tpRegistre = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTypeClient = new System.Windows.Forms.ComboBox();
            this.panReg = new System.Windows.Forms.Panel();
            this.txtCodeSage = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tpAutorisation = new System.Windows.Forms.TabPage();
            this.panFactAterme = new System.Windows.Forms.Panel();
            this.labMontant = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nud = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMontantAutorise = new System.Windows.Forms.TextBox();
            this.labxxxxx = new System.Windows.Forms.Label();
            this.chAutorise = new System.Windows.Forms.CheckBox();
            this.cbCategorieClient = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNomraison = new FZBGsys.TextBoxx();
            this.txtAdresse = new FZBGsys.TextBoxx();
            this.txtTelephone = new FZBGsys.TextBoxx();
            this.txtRegistre = new FZBGsys.TextBoxx();
            this.txtFiscal = new FZBGsys.TextBoxx();
            this.txtArticle = new FZBGsys.TextBoxx();
            this.tcClient.SuspendLayout();
            this.tpInformation.SuspendLayout();
            this.tpRegistre.SuspendLayout();
            this.panReg.SuspendLayout();
            this.tpAutorisation.SuspendLayout();
            this.panFactAterme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonEnregistre
            // 
            this.buttonEnregistre.Location = new System.Drawing.Point(280, 244);
            this.buttonEnregistre.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEnregistre.Name = "buttonEnregistre";
            this.buttonEnregistre.Size = new System.Drawing.Size(74, 27);
            this.buttonEnregistre.TabIndex = 0;
            this.buttonEnregistre.Text = "Enregistrer";
            this.buttonEnregistre.UseVisualStyleBackColor = true;
            this.buttonEnregistre.Click += new System.EventHandler(this.buttonEnregistrerEmployer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Location = new System.Drawing.Point(202, 244);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(74, 27);
            this.buttonAnnuler.TabIndex = 1;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnulerEmployer_Click);
            // 
            // tcClient
            // 
            this.tcClient.Controls.Add(this.tpInformation);
            this.tcClient.Controls.Add(this.tpRegistre);
            this.tcClient.Controls.Add(this.tpAutorisation);
            this.tcClient.Location = new System.Drawing.Point(9, 17);
            this.tcClient.Margin = new System.Windows.Forms.Padding(2);
            this.tcClient.Name = "tcClient";
            this.tcClient.SelectedIndex = 0;
            this.tcClient.Size = new System.Drawing.Size(356, 223);
            this.tcClient.TabIndex = 20;
            // 
            // tpInformation
            // 
            this.tpInformation.BackColor = System.Drawing.SystemColors.Control;
            this.tpInformation.Controls.Add(this.label11);
            this.tpInformation.Controls.Add(this.label1);
            this.tpInformation.Controls.Add(this.chIsParent);
            this.tpInformation.Controls.Add(this.cbwilaya);
            this.tpInformation.Controls.Add(this.cbParentClient);
            this.tpInformation.Controls.Add(this.label3);
            this.tpInformation.Controls.Add(this.txtNomraison);
            this.tpInformation.Controls.Add(this.txtAdresse);
            this.tpInformation.Controls.Add(this.txtTelephone);
            this.tpInformation.Controls.Add(this.Activite);
            this.tpInformation.Location = new System.Drawing.Point(4, 22);
            this.tpInformation.Margin = new System.Windows.Forms.Padding(2);
            this.tpInformation.Name = "tpInformation";
            this.tpInformation.Padding = new System.Windows.Forms.Padding(2);
            this.tpInformation.Size = new System.Drawing.Size(348, 197);
            this.tpInformation.TabIndex = 1;
            this.tpInformation.Text = "Informations";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 169);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(39, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Wilaya";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nom / Raison Sociale:";
            // 
            // chIsParent
            // 
            this.chIsParent.AutoSize = true;
            this.chIsParent.Location = new System.Drawing.Point(13, 132);
            this.chIsParent.Margin = new System.Windows.Forms.Padding(2);
            this.chIsParent.Name = "chIsParent";
            this.chIsParent.Size = new System.Drawing.Size(77, 17);
            this.chIsParent.TabIndex = 15;
            this.chIsParent.Text = "Pariné par ";
            this.chIsParent.UseVisualStyleBackColor = true;
            this.chIsParent.CheckedChanged += new System.EventHandler(this.chIsParent_CheckedChanged_1);
            // 
            // cbwilaya
            // 
            this.cbwilaya.FormattingEnabled = true;
            this.cbwilaya.Items.AddRange(new object[] {
            "Adrar",
            "Chlef",
            "Laghouat",
            "Oum-El-Bouaghi",
            "Batna",
            "Bejaïa",
            "Biskra",
            "Bechar",
            "Blida",
            "Bouira",
            "Tamanrasset",
            "Tebessa",
            "Tlemcen",
            "Tiaret",
            "Tizi-Ouzou",
            "Alger",
            "Djelfa",
            "Jijel",
            "Sétif",
            "Saïda",
            "Skikda",
            "Sidi Bel Abbès",
            "Annaba",
            "Guelma",
            "Constantine",
            "Médéa",
            "Mostaganem",
            "M\'sila",
            "Mascara",
            "Ouargla",
            "Oran",
            "El Bayadh",
            "Illizi",
            "Bordj Bou Arréridj",
            "Boumerdès",
            "El Tarf",
            "Tindouf",
            "Tissemsilt",
            "El Oued",
            "Khenchela",
            "Souk Ahras",
            "Tipaza",
            "Mila",
            "Aïn Defla",
            "Naama",
            "Aïn Temouchent",
            "Ghardaïa",
            "Relizane"});
            this.cbwilaya.Location = new System.Drawing.Point(137, 166);
            this.cbwilaya.Margin = new System.Windows.Forms.Padding(2);
            this.cbwilaya.Name = "cbwilaya";
            this.cbwilaya.Size = new System.Drawing.Size(126, 21);
            this.cbwilaya.TabIndex = 14;
            this.cbwilaya.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // cbParentClient
            // 
            this.cbParentClient.FormattingEnabled = true;
            this.cbParentClient.Location = new System.Drawing.Point(137, 129);
            this.cbParentClient.Margin = new System.Windows.Forms.Padding(2);
            this.cbParentClient.Name = "cbParentClient";
            this.cbParentClient.Size = new System.Drawing.Size(180, 21);
            this.cbParentClient.TabIndex = 14;
            this.cbParentClient.Visible = false;
            this.cbParentClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 41);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Adresse:";
            // 
            // Activite
            // 
            this.Activite.AutoSize = true;
            this.Activite.Location = new System.Drawing.Point(13, 98);
            this.Activite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Activite.Name = "Activite";
            this.Activite.Size = new System.Drawing.Size(109, 13);
            this.Activite.TabIndex = 10;
            this.Activite.Text = "Contact / Telephone:";
            // 
            // tpRegistre
            // 
            this.tpRegistre.BackColor = System.Drawing.SystemColors.Control;
            this.tpRegistre.Controls.Add(this.label12);
            this.tpRegistre.Controls.Add(this.label6);
            this.tpRegistre.Controls.Add(this.cbCategorieClient);
            this.tpRegistre.Controls.Add(this.cbTypeClient);
            this.tpRegistre.Controls.Add(this.panReg);
            this.tpRegistre.Location = new System.Drawing.Point(4, 22);
            this.tpRegistre.Margin = new System.Windows.Forms.Padding(2);
            this.tpRegistre.Name = "tpRegistre";
            this.tpRegistre.Size = new System.Drawing.Size(348, 197);
            this.tpRegistre.TabIndex = 2;
            this.tpRegistre.Text = "Dossier Comercial";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 25);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Type Client:";
            // 
            // cbTypeClient
            // 
            this.cbTypeClient.FormattingEnabled = true;
            this.cbTypeClient.Location = new System.Drawing.Point(141, 23);
            this.cbTypeClient.Name = "cbTypeClient";
            this.cbTypeClient.Size = new System.Drawing.Size(180, 21);
            this.cbTypeClient.TabIndex = 19;
            this.cbTypeClient.SelectedIndexChanged += new System.EventHandler(this.cbTypeClient_SelectedIndexChanged_1);
            // 
            // panReg
            // 
            this.panReg.Controls.Add(this.txtCodeSage);
            this.panReg.Controls.Add(this.txtRegistre);
            this.panReg.Controls.Add(this.label10);
            this.panReg.Controls.Add(this.label2);
            this.panReg.Controls.Add(this.label4);
            this.panReg.Controls.Add(this.label5);
            this.panReg.Controls.Add(this.txtFiscal);
            this.panReg.Controls.Add(this.txtArticle);
            this.panReg.Location = new System.Drawing.Point(10, 49);
            this.panReg.Name = "panReg";
            this.panReg.Size = new System.Drawing.Size(320, 117);
            this.panReg.TabIndex = 16;
            this.panReg.Visible = false;
            // 
            // txtCodeSage
            // 
            this.txtCodeSage.Location = new System.Drawing.Point(131, 12);
            this.txtCodeSage.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeSage.Name = "txtCodeSage";
            this.txtCodeSage.Size = new System.Drawing.Size(92, 20);
            this.txtCodeSage.TabIndex = 22;
            this.txtCodeSage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(53, 15);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Code Sage:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "N° Registre Comerce:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 67);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Identifiant Fiscale:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Article d\'imposition:";
            // 
            // tpAutorisation
            // 
            this.tpAutorisation.BackColor = System.Drawing.SystemColors.Control;
            this.tpAutorisation.Controls.Add(this.panFactAterme);
            this.tpAutorisation.Controls.Add(this.chAutorise);
            this.tpAutorisation.Location = new System.Drawing.Point(4, 22);
            this.tpAutorisation.Margin = new System.Windows.Forms.Padding(2);
            this.tpAutorisation.Name = "tpAutorisation";
            this.tpAutorisation.Size = new System.Drawing.Size(348, 197);
            this.tpAutorisation.TabIndex = 3;
            this.tpAutorisation.Text = "Autorisations Crédit";
            // 
            // panFactAterme
            // 
            this.panFactAterme.Controls.Add(this.labMontant);
            this.panFactAterme.Controls.Add(this.label9);
            this.panFactAterme.Controls.Add(this.nud);
            this.panFactAterme.Controls.Add(this.label8);
            this.panFactAterme.Controls.Add(this.label7);
            this.panFactAterme.Controls.Add(this.txtMontantAutorise);
            this.panFactAterme.Controls.Add(this.labxxxxx);
            this.panFactAterme.Enabled = false;
            this.panFactAterme.Location = new System.Drawing.Point(14, 45);
            this.panFactAterme.Margin = new System.Windows.Forms.Padding(2);
            this.panFactAterme.Name = "panFactAterme";
            this.panFactAterme.Size = new System.Drawing.Size(314, 121);
            this.panFactAterme.TabIndex = 1;
            // 
            // labMontant
            // 
            this.labMontant.AutoSize = true;
            this.labMontant.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMontant.Location = new System.Drawing.Point(65, 50);
            this.labMontant.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labMontant.Name = "labMontant";
            this.labMontant.Size = new System.Drawing.Size(230, 13);
            this.labMontant.TabIndex = 6;
            this.labMontant.Text = "trente huit milles trois cent quatre vingt dix-neuf ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(121, 91);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(178, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "jours à partir du passage en débiteur";
            // 
            // nud
            // 
            this.nud.Location = new System.Drawing.Point(70, 89);
            this.nud.Margin = new System.Windows.Forms.Padding(2);
            this.nud.Name = "nud";
            this.nud.Size = new System.Drawing.Size(46, 20);
            this.nud.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Délait du règlement autorisé:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(190, 28);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "DA";
            // 
            // txtMontantAutorise
            // 
            this.txtMontantAutorise.Location = new System.Drawing.Point(70, 27);
            this.txtMontantAutorise.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantAutorise.Name = "txtMontantAutorise";
            this.txtMontantAutorise.Size = new System.Drawing.Size(116, 20);
            this.txtMontantAutorise.TabIndex = 1;
            this.txtMontantAutorise.TextChanged += new System.EventHandler(this.txtMontantAutorise_TextChanged);
            // 
            // labxxxxx
            // 
            this.labxxxxx.AutoSize = true;
            this.labxxxxx.Location = new System.Drawing.Point(5, 7);
            this.labxxxxx.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labxxxxx.Name = "labxxxxx";
            this.labxxxxx.Size = new System.Drawing.Size(180, 13);
            this.labxxxxx.TabIndex = 0;
            this.labxxxxx.Text = "Montant du crédit Maximum autorisé:";
            // 
            // chAutorise
            // 
            this.chAutorise.AutoSize = true;
            this.chAutorise.Location = new System.Drawing.Point(13, 19);
            this.chAutorise.Margin = new System.Windows.Forms.Padding(2);
            this.chAutorise.Name = "chAutorise";
            this.chAutorise.Size = new System.Drawing.Size(211, 17);
            this.chAutorise.TabIndex = 0;
            this.chAutorise.Text = "Ce Client est autorisé à facturer à terme";
            this.chAutorise.UseVisualStyleBackColor = true;
            this.chAutorise.CheckedChanged += new System.EventHandler(this.chAutorise_CheckedChanged);
            // 
            // cbCategorieClient
            // 
            this.cbCategorieClient.FormattingEnabled = true;
            this.cbCategorieClient.Location = new System.Drawing.Point(141, 168);
            this.cbCategorieClient.Name = "cbCategorieClient";
            this.cbCategorieClient.Size = new System.Drawing.Size(180, 21);
            this.cbCategorieClient.TabIndex = 19;
            this.cbCategorieClient.SelectedIndexChanged += new System.EventHandler(this.cbTypeClient_SelectedIndexChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 170);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Categorie Client:";
            // 
            // txtNomraison
            // 
            this.txtNomraison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomraison.Location = new System.Drawing.Point(139, 20);
            this.txtNomraison.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomraison.Name = "txtNomraison";
            this.txtNomraison.Size = new System.Drawing.Size(180, 20);
            this.txtNomraison.TabIndex = 6;
            this.txtNomraison.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // txtAdresse
            // 
            this.txtAdresse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse.Location = new System.Drawing.Point(139, 41);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.Multiline = true;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(180, 55);
            this.txtAdresse.TabIndex = 7;
            this.txtAdresse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNomraison_KeyPress);
            // 
            // txtTelephone
            // 
            this.txtTelephone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelephone.Location = new System.Drawing.Point(139, 98);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(180, 20);
            this.txtTelephone.TabIndex = 8;
            // 
            // txtRegistre
            // 
            this.txtRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegistre.Location = new System.Drawing.Point(131, 42);
            this.txtRegistre.Margin = new System.Windows.Forms.Padding(2);
            this.txtRegistre.Name = "txtRegistre";
            this.txtRegistre.Size = new System.Drawing.Size(180, 20);
            this.txtRegistre.TabIndex = 7;
            // 
            // txtFiscal
            // 
            this.txtFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiscal.Location = new System.Drawing.Point(131, 65);
            this.txtFiscal.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiscal.Name = "txtFiscal";
            this.txtFiscal.Size = new System.Drawing.Size(180, 20);
            this.txtFiscal.TabIndex = 7;
            // 
            // txtArticle
            // 
            this.txtArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtArticle.Location = new System.Drawing.Point(131, 88);
            this.txtArticle.Margin = new System.Windows.Forms.Padding(2);
            this.txtArticle.Name = "txtArticle";
            this.txtArticle.Size = new System.Drawing.Size(180, 20);
            this.txtArticle.TabIndex = 7;
            // 
            // FrCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 283);
            this.Controls.Add(this.tcClient);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistre);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrCreate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nouveau Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminEmployerNew_FormClosing);
            this.tcClient.ResumeLayout(false);
            this.tpInformation.ResumeLayout(false);
            this.tpInformation.PerformLayout();
            this.tpRegistre.ResumeLayout(false);
            this.tpRegistre.PerformLayout();
            this.panReg.ResumeLayout(false);
            this.panReg.PerformLayout();
            this.tpAutorisation.ResumeLayout(false);
            this.tpAutorisation.PerformLayout();
            this.panFactAterme.ResumeLayout(false);
            this.panFactAterme.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonEnregistre;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.TabControl tcClient;
        private System.Windows.Forms.TabPage tpInformation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chIsParent;
        private System.Windows.Forms.ComboBox cbParentClient;
        private System.Windows.Forms.Label label3;
        private TextBoxx txtNomraison;
        private TextBoxx txtAdresse;
        private TextBoxx txtTelephone;
        private System.Windows.Forms.Label Activite;
        private System.Windows.Forms.TabPage tpRegistre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTypeClient;
        private System.Windows.Forms.Panel panReg;
        private TextBoxx txtRegistre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private TextBoxx txtFiscal;
        private TextBoxx txtArticle;
        private System.Windows.Forms.TabPage tpAutorisation;
        private System.Windows.Forms.Panel panFactAterme;
        private System.Windows.Forms.Label labMontant;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nud;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMontantAutorise;
        private System.Windows.Forms.Label labxxxxx;
        private System.Windows.Forms.CheckBox chAutorise;
        private System.Windows.Forms.TextBox txtCodeSage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbwilaya;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbCategorieClient;
    }
}