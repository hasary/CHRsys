﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace FZBGsys.NsComercial.NsClient
{


    public partial class FrList : Form
    {
        ModelEntities db;
        public bool AddAucuneSelection { get; set; }

        public FrList()
        {
            db = new ModelEntities();
            this.InitType(false); // normal list
        }

        public FrList(bool IsSelectionDialog, ModelEntities db)
        {
            this.db = db;
            this.InitType(IsSelectionDialog);

            txtNom.Select();


        }

        private void InitType(bool IsSelectionDialog)
        {
            this.IsSelectionDialog = IsSelectionDialog;


            InitializeComponent();
            panelEdit.Visible = !IsSelectionDialog;
            panelSelect.Visible = IsSelectionDialog;
            if (IsSelectionDialog)
            {
                this.AcceptButton = btSelectOK;
                this.CancelButton = btAnnuler;
            }
            else
            {
                this.AcceptButton = buttonModifier;
            }
            InitializeData();

        }


        private void InitializeData()
        {

            InitialiseGridView(txtNom.Text.Trim(), txtParent.Text.Trim()); // 0 means all sections



        }

        private void InitialiseGridView(string NomFilter, string parentFilter)
        {
            /*if (IsSelectionDialog && NomFilter != String.Empty)
            {
                return;
            }

           */

            var Clients = db.Clients.Where(e => e.ID != 0);
            Clients = Clients.Where(emp => emp.Nom.Contains(NomFilter));
            if (parentFilter != "")
            {
                Clients = Clients.Where(emp => emp.ClientParent.Nom.Contains(parentFilter));
            }

            //  List<MarbreBLIDA.Client> ListClientsList = null;

            if (true)
            {
                dgv1.DataSource = Clients.Where(p => p.Nom != null).OrderByDescending(data => data.Solde / 100000 + data.PanierChrea + data.PanierOrangeChrea).ToList().Select(
                         data => new
                         {
                             Statu = (data.take == true) ? "actif" : "non-actif",
                             Type = data.TypeClient.Nom,
                             Code = data.CodeSage,
                             Numero = data.ID,
                             Nom = data.NomClientAndParent(),
                             //Solde = data.Solde.ToAffInt(false),
                             //  Panier = (data.ClientTypeID != 1) ? "" : ((data.PanierChrea.Value / 540).ToString() + " Ch.  " + (data.PanierOrangeChrea.Value / 540).ToString() + " Cp."),
                             //   CD = (data.ClientTypeID != 1) ? "" : ("1/" + (data.InitChrea.Value).ToString() + " Ch.  " + "1/" + (data.InitOrangeChrea.Value).ToString() + " Cp."),

                             // Parent = (data.ClientParent != null) ? data.ClientParent.Nom : "",
                             //  Solde = data.Solde,
                             //  Adresse = data.Adresse,
                             Tele = data.Telephone,
                             // Registre = (data.NoRegistre == null) ? "/" : data.NoRegistre,
                             Charg = (data.DateDernierChargement != null) ? data.DateDernierChargement.Value.ToShortDateString() : "",
                             //   val1 = (dernierVers != null) ? dernierVersMontant.ToString() : "0",
                             Vers = (data.DateDernierVersement != null) ? data.DateDernierVersement.Value.ToShortDateString() : "",
                         }).ToList();
            }
            else
            {
                dgv1.DataSource = Clients.OrderBy(data => data.Nom).Select(
                        data => new
                        {
                            Statu = (data.take == true) ? "actif" : "non-actif",
                            Type = data.TypeClient.Nom,
                            Code = data.CodeSage,
                            Numero = data.ID,
                            Nom = data.Nom,
                            Parent = (data.ClientParent != null) ? data.ClientParent.Nom : "",
                            //  Solde = data.Solde,
                            // Dernier_Versement = data.Operations.M

                        }).ToList();
            }

            // = ListClientsList;
            dgv1.HideIDColumn("Numero");
            // dgv1.Columns["Solde"].DefaultCellStyle.Format = "# ### ###";
        }
        public static Client SelectClientDialog(ModelEntities db)
        {


            FrList e =
            new FrList(true, db);

            //   e.buttonNouveau.Visible = false;
            e.ShowDialog();

            return ReturnClient;

        }
        private void AdminClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonOKClient_Click(object sender, EventArgs e)
        {

            if (!AddAucuneSelection || dgv1.SelectedRows[0].Index != 0)
            {
                var matricule = dgv1.SelectedRows[0].Cells[0].Value.ToString();
                ReturnClient = db.Clients.Single(em => em.Nom == matricule);
            }
            else
                ReturnClient = null; // en cas de selection aucun
            Dispose();
        }
        private static Client ReturnClient { get; set; }
        public bool IsSelectionDialog { get; set; } // if control is called for selection only (not for editing)
        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            ReturnClient = null;
            Dispose();
        }
        private void buttonNewClient_Click(object sender, EventArgs e)
        {
            if (IsSelectionDialog)
            {
                ReturnClient = FrCreate.CreateClientDialog(db);

                Dispose(); // so return to Utilisateur Liste Selection,

            }
            else
            {
                new FrCreate(null, false).ShowDialog();

                InitializeData();
            }

        }
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            var id = dgv1.GetSelectedID("Numero");
            var selectedClient = db.Clients.Single(em => em.ID == id);
            db.Refresh(RefreshMode.StoreWins, selectedClient);
            new FrEdit(selectedClient.ID).ShowDialog();



            InitializeData();
        }
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            var id = dgv1.GetSelectedID("Numero");
            var selectedClient = db.Clients.Single(em => em.ID == id);
            if (this.ConfirmWarning("Supprimer le Client " + selectedClient.Nom + "?"))
            {
                db.Clients.DeleteObject(selectedClient);
                db.SaveChanges();
                InitializeData();
            }
        }







        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void buttonSelectOK_Click(object sender, EventArgs e)
        {
            if (!IsSelectionDialog) return;
            if (dgv1.SelectedRows.Count == 1)
            {
                var id = dgv1.GetSelectedID("Numero");
                var selectedClient = db.Clients.Single(em => em.ID == id);
                ReturnClient = selectedClient;
            }
            else
            {
                ReturnClient = null;
            }
            Dispose();
        }

        private void FrList_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialog) db.Dispose();
        }

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            InitialiseGridView(txtNom.Text.ToUpper(), txtParent.Text.ToUpper());
        }

        private void txtParent_TextChanged(object sender, EventArgs e)
        {
            //   txtNom.Text = "";
            InitialiseGridView(txtNom.Text.ToUpper(), txtParent.Text.ToUpper());
        }

        private void dgv1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Keys)e.KeyChar == Keys.Enter)
            {
                //buttonOKClient_Click(sender, e);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var id = dgv1.GetSelectedID("Numero");
                var selectedClient = db.Clients.Single(em => em.ID == id);
                selectedClient.take = true;
                db.SaveChangeTry();
                dgv1.SelectedRows[0].Cells["Statu"].Value = "actif";
                var index = dgv1.SelectedRows[0].Index;
                var scrol = dgv1.FirstDisplayedScrollingRowIndex;
                InitialiseGridView("", "");
                dgv1.Rows[index].Selected = true;
                dgv1.FirstDisplayedScrollingRowIndex = scrol;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var id = dgv1.GetSelectedID("Numero");
                var selectedClient = db.Clients.Single(em => em.ID == id);
                selectedClient.take = false;
                db.SaveChangeTry();
                var index = dgv1.SelectedRows[0].Index;
                var scrol = dgv1.FirstDisplayedScrollingRowIndex;
                InitialiseGridView("", "");
                dgv1.Rows[index].Selected = true;
                dgv1.FirstDisplayedScrollingRowIndex = scrol;
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {

            FZBGsys.NsReports.Comercial.ComercialReportController.PrintSituationCommercialeLight();
        }



    }
}
