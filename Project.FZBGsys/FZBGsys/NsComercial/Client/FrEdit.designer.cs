﻿namespace FZBGsys.NsComercial.NsClient
{
    partial class FrEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEnregistre = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Activite = new System.Windows.Forms.Label();
            this.chIsParent = new System.Windows.Forms.CheckBox();
            this.cbParentClient = new System.Windows.Forms.ComboBox();
            this.panReg = new System.Windows.Forms.Panel();
            this.txtCodeSage = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tcClient = new System.Windows.Forms.TabControl();
            this.tpInformation = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.cbwilaya = new System.Windows.Forms.ComboBox();
            this.tpRegistre = new System.Windows.Forms.TabPage();
            this.chExpire = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTypeClient = new System.Windows.Forms.ComboBox();
            this.tpAutorisation = new System.Windows.Forms.TabPage();
            this.chRemise = new System.Windows.Forms.CheckBox();
            this.panFactAterme = new System.Windows.Forms.Panel();
            this.nbFactIgneore = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.labMontant = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nud = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMontantAutorise = new System.Windows.Forms.TextBox();
            this.labxxxxx = new System.Windows.Forms.Label();
            this.chAutorise = new System.Windows.Forms.CheckBox();
            this.tpCD = new System.Windows.Forms.TabPage();
            this.Panier = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtPanierOrangeChrea = new System.Windows.Forms.TextBox();
            this.txtPanierChrea = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.CDChrea = new System.Windows.Forms.TextBox();
            this.CDOrangeChrea = new System.Windows.Forms.TextBox();
            this.txtIntiChrea = new System.Windows.Forms.TextBox();
            this.InitOrangeChrea = new System.Windows.Forms.TextBox();
            this.btActiveNouveau = new System.Windows.Forms.Button();
            this.cbCategorieClient = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNomraison = new FZBGsys.TextBoxx();
            this.txtAdresse = new FZBGsys.TextBoxx();
            this.txtTelephone = new FZBGsys.TextBoxx();
            this.txtRegistre = new FZBGsys.TextBoxx();
            this.txtFiscal = new FZBGsys.TextBoxx();
            this.txtArticle = new FZBGsys.TextBoxx();
            this.panReg.SuspendLayout();
            this.tcClient.SuspendLayout();
            this.tpInformation.SuspendLayout();
            this.tpRegistre.SuspendLayout();
            this.tpAutorisation.SuspendLayout();
            this.panFactAterme.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbFactIgneore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud)).BeginInit();
            this.tpCD.SuspendLayout();
            this.Panier.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonEnregistre
            // 
            this.buttonEnregistre.Location = new System.Drawing.Point(315, 251);
            this.buttonEnregistre.Margin = new System.Windows.Forms.Padding(2);
            this.buttonEnregistre.Name = "buttonEnregistre";
            this.buttonEnregistre.Size = new System.Drawing.Size(74, 27);
            this.buttonEnregistre.TabIndex = 0;
            this.buttonEnregistre.Text = "Enregistrer";
            this.buttonEnregistre.UseVisualStyleBackColor = true;
            this.buttonEnregistre.Click += new System.EventHandler(this.buttonEnregistrerEmployer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Location = new System.Drawing.Point(235, 251);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(74, 27);
            this.buttonAnnuler.TabIndex = 1;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnulerEmployer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nom / Raison Sociale:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 41);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Adresse:";
            // 
            // Activite
            // 
            this.Activite.AutoSize = true;
            this.Activite.Location = new System.Drawing.Point(13, 98);
            this.Activite.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Activite.Name = "Activite";
            this.Activite.Size = new System.Drawing.Size(109, 13);
            this.Activite.TabIndex = 10;
            this.Activite.Text = "Contact / Telephone:";
            // 
            // chIsParent
            // 
            this.chIsParent.AutoSize = true;
            this.chIsParent.Location = new System.Drawing.Point(13, 132);
            this.chIsParent.Margin = new System.Windows.Forms.Padding(2);
            this.chIsParent.Name = "chIsParent";
            this.chIsParent.Size = new System.Drawing.Size(77, 17);
            this.chIsParent.TabIndex = 15;
            this.chIsParent.Text = "Pariné par ";
            this.chIsParent.UseVisualStyleBackColor = true;
            this.chIsParent.CheckedChanged += new System.EventHandler(this.chIsParent_CheckedChanged);
            // 
            // cbParentClient
            // 
            this.cbParentClient.FormattingEnabled = true;
            this.cbParentClient.Location = new System.Drawing.Point(137, 129);
            this.cbParentClient.Margin = new System.Windows.Forms.Padding(2);
            this.cbParentClient.Name = "cbParentClient";
            this.cbParentClient.Size = new System.Drawing.Size(180, 21);
            this.cbParentClient.TabIndex = 14;
            this.cbParentClient.Visible = false;
            this.cbParentClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbParentClient_KeyPress);
            // 
            // panReg
            // 
            this.panReg.Controls.Add(this.txtCodeSage);
            this.panReg.Controls.Add(this.label2);
            this.panReg.Controls.Add(this.label10);
            this.panReg.Controls.Add(this.label4);
            this.panReg.Controls.Add(this.label5);
            this.panReg.Controls.Add(this.txtRegistre);
            this.panReg.Controls.Add(this.txtFiscal);
            this.panReg.Controls.Add(this.txtArticle);
            this.panReg.Location = new System.Drawing.Point(10, 67);
            this.panReg.Name = "panReg";
            this.panReg.Size = new System.Drawing.Size(320, 115);
            this.panReg.TabIndex = 16;
            // 
            // txtCodeSage
            // 
            this.txtCodeSage.Location = new System.Drawing.Point(131, 8);
            this.txtCodeSage.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeSage.Name = "txtCodeSage";
            this.txtCodeSage.ReadOnly = true;
            this.txtCodeSage.Size = new System.Drawing.Size(92, 20);
            this.txtCodeSage.TabIndex = 24;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "N° Registre Comerce:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(53, 11);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Code Sage:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 57);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Identifiant Fiscale:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 80);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Article d\'imposition:";
            // 
            // tcClient
            // 
            this.tcClient.Controls.Add(this.tpInformation);
            this.tcClient.Controls.Add(this.tpRegistre);
            this.tcClient.Controls.Add(this.tpAutorisation);
            this.tcClient.Controls.Add(this.tpCD);
            this.tcClient.Location = new System.Drawing.Point(9, 10);
            this.tcClient.Margin = new System.Windows.Forms.Padding(2);
            this.tcClient.Name = "tcClient";
            this.tcClient.SelectedIndex = 0;
            this.tcClient.Size = new System.Drawing.Size(380, 237);
            this.tcClient.TabIndex = 19;
            // 
            // tpInformation
            // 
            this.tpInformation.BackColor = System.Drawing.SystemColors.Control;
            this.tpInformation.Controls.Add(this.label19);
            this.tpInformation.Controls.Add(this.cbwilaya);
            this.tpInformation.Controls.Add(this.label1);
            this.tpInformation.Controls.Add(this.chIsParent);
            this.tpInformation.Controls.Add(this.cbParentClient);
            this.tpInformation.Controls.Add(this.label3);
            this.tpInformation.Controls.Add(this.txtNomraison);
            this.tpInformation.Controls.Add(this.txtAdresse);
            this.tpInformation.Controls.Add(this.txtTelephone);
            this.tpInformation.Controls.Add(this.Activite);
            this.tpInformation.Location = new System.Drawing.Point(4, 22);
            this.tpInformation.Margin = new System.Windows.Forms.Padding(2);
            this.tpInformation.Name = "tpInformation";
            this.tpInformation.Padding = new System.Windows.Forms.Padding(2);
            this.tpInformation.Size = new System.Drawing.Size(372, 211);
            this.tpInformation.TabIndex = 1;
            this.tpInformation.Text = "Informations";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(39, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "Wilaya";
            // 
            // cbwilaya
            // 
            this.cbwilaya.FormattingEnabled = true;
            this.cbwilaya.Items.AddRange(new object[] {
            "Adrar",
            "Chlef",
            "Laghouat",
            "Oum-El-Bouaghi",
            "Batna",
            "Bejaïa",
            "Biskra",
            "Bechar",
            "Blida",
            "Bouira",
            "Tamanrasset",
            "Tebessa",
            "Tlemcen",
            "Tiaret",
            "Tizi-Ouzou",
            "Alger",
            "Djelfa",
            "Jijel",
            "Sétif",
            "Saïda",
            "Skikda",
            "Sidi Bel Abbès",
            "Annaba",
            "Guelma",
            "Constantine",
            "Médéa",
            "Mostaganem",
            "M\'sila",
            "Mascara",
            "Ouargla",
            "Oran",
            "El Bayadh",
            "Illizi",
            "Bordj Bou Arréridj",
            "Boumerdès",
            "El Tarf",
            "Tindouf",
            "Tissemsilt",
            "El Oued",
            "Khenchela",
            "Souk Ahras",
            "Tipaza",
            "Mila",
            "Aïn Defla",
            "Naama",
            "Aïn Temouchent",
            "Ghardaïa",
            "Relizane"});
            this.cbwilaya.Location = new System.Drawing.Point(139, 159);
            this.cbwilaya.Margin = new System.Windows.Forms.Padding(2);
            this.cbwilaya.Name = "cbwilaya";
            this.cbwilaya.Size = new System.Drawing.Size(126, 21);
            this.cbwilaya.TabIndex = 17;
            // 
            // tpRegistre
            // 
            this.tpRegistre.BackColor = System.Drawing.SystemColors.Control;
            this.tpRegistre.Controls.Add(this.chExpire);
            this.tpRegistre.Controls.Add(this.label20);
            this.tpRegistre.Controls.Add(this.cbCategorieClient);
            this.tpRegistre.Controls.Add(this.label6);
            this.tpRegistre.Controls.Add(this.cbTypeClient);
            this.tpRegistre.Controls.Add(this.panReg);
            this.tpRegistre.Location = new System.Drawing.Point(4, 22);
            this.tpRegistre.Margin = new System.Windows.Forms.Padding(2);
            this.tpRegistre.Name = "tpRegistre";
            this.tpRegistre.Size = new System.Drawing.Size(372, 211);
            this.tpRegistre.TabIndex = 2;
            this.tpRegistre.Text = "Dossier Comercial";
            // 
            // chExpire
            // 
            this.chExpire.AutoSize = true;
            this.chExpire.Location = new System.Drawing.Point(141, 188);
            this.chExpire.Margin = new System.Windows.Forms.Padding(2);
            this.chExpire.Name = "chExpire";
            this.chExpire.Size = new System.Drawing.Size(191, 17);
            this.chExpire.TabIndex = 21;
            this.chExpire.Text = "bloquer ce compte (ancien dossier)";
            this.chExpire.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 12);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Type Client:";
            // 
            // cbTypeClient
            // 
            this.cbTypeClient.Enabled = false;
            this.cbTypeClient.FormattingEnabled = true;
            this.cbTypeClient.Location = new System.Drawing.Point(141, 10);
            this.cbTypeClient.Name = "cbTypeClient";
            this.cbTypeClient.Size = new System.Drawing.Size(180, 21);
            this.cbTypeClient.TabIndex = 19;
            this.cbTypeClient.SelectedIndexChanged += new System.EventHandler(this.cbTypeClient_SelectedIndexChanged_1);
            // 
            // tpAutorisation
            // 
            this.tpAutorisation.BackColor = System.Drawing.SystemColors.Control;
            this.tpAutorisation.Controls.Add(this.chRemise);
            this.tpAutorisation.Controls.Add(this.panFactAterme);
            this.tpAutorisation.Controls.Add(this.chAutorise);
            this.tpAutorisation.Location = new System.Drawing.Point(4, 22);
            this.tpAutorisation.Margin = new System.Windows.Forms.Padding(2);
            this.tpAutorisation.Name = "tpAutorisation";
            this.tpAutorisation.Size = new System.Drawing.Size(372, 211);
            this.tpAutorisation.TabIndex = 3;
            this.tpAutorisation.Text = "Autorisations Crédit";
            // 
            // chRemise
            // 
            this.chRemise.AutoSize = true;
            this.chRemise.Location = new System.Drawing.Point(13, 171);
            this.chRemise.Margin = new System.Windows.Forms.Padding(2);
            this.chRemise.Name = "chRemise";
            this.chRemise.Size = new System.Drawing.Size(214, 17);
            this.chRemise.TabIndex = 2;
            this.chRemise.Text = "Ce Client bénificie du montant de remise";
            this.chRemise.UseVisualStyleBackColor = true;
            // 
            // panFactAterme
            // 
            this.panFactAterme.Controls.Add(this.nbFactIgneore);
            this.panFactAterme.Controls.Add(this.label12);
            this.panFactAterme.Controls.Add(this.labMontant);
            this.panFactAterme.Controls.Add(this.label9);
            this.panFactAterme.Controls.Add(this.nud);
            this.panFactAterme.Controls.Add(this.label8);
            this.panFactAterme.Controls.Add(this.label11);
            this.panFactAterme.Controls.Add(this.label7);
            this.panFactAterme.Controls.Add(this.txtMontantAutorise);
            this.panFactAterme.Controls.Add(this.labxxxxx);
            this.panFactAterme.Enabled = false;
            this.panFactAterme.Location = new System.Drawing.Point(14, 45);
            this.panFactAterme.Margin = new System.Windows.Forms.Padding(2);
            this.panFactAterme.Name = "panFactAterme";
            this.panFactAterme.Size = new System.Drawing.Size(356, 121);
            this.panFactAterme.TabIndex = 1;
            // 
            // nbFactIgneore
            // 
            this.nbFactIgneore.Location = new System.Drawing.Point(228, 23);
            this.nbFactIgneore.Margin = new System.Windows.Forms.Padding(2);
            this.nbFactIgneore.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nbFactIgneore.Name = "nbFactIgneore";
            this.nbFactIgneore.Size = new System.Drawing.Size(46, 20);
            this.nbFactIgneore.TabIndex = 8;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(278, 23);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 7;
            this.label12.Text = "fact.";
            // 
            // labMontant
            // 
            this.labMontant.AutoSize = true;
            this.labMontant.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMontant.Location = new System.Drawing.Point(3, 45);
            this.labMontant.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labMontant.Name = "labMontant";
            this.labMontant.Size = new System.Drawing.Size(209, 15);
            this.labMontant.TabIndex = 6;
            this.labMontant.Text = "trente huit milles trois cent quatre vingt dix-neuf ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(121, 91);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(178, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "jours à partir du passage en débiteur";
            // 
            // nud
            // 
            this.nud.Location = new System.Drawing.Point(62, 89);
            this.nud.Margin = new System.Windows.Forms.Padding(2);
            this.nud.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nud.Name = "nud";
            this.nud.Size = new System.Drawing.Size(46, 20);
            this.nud.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 72);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Délait du règlement autorisé:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(183, 25);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Ignorer";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(128, 23);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "DA";
            // 
            // txtMontantAutorise
            // 
            this.txtMontantAutorise.Location = new System.Drawing.Point(8, 22);
            this.txtMontantAutorise.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantAutorise.Name = "txtMontantAutorise";
            this.txtMontantAutorise.Size = new System.Drawing.Size(116, 20);
            this.txtMontantAutorise.TabIndex = 1;
            this.txtMontantAutorise.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // labxxxxx
            // 
            this.labxxxxx.AutoSize = true;
            this.labxxxxx.Location = new System.Drawing.Point(5, 7);
            this.labxxxxx.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labxxxxx.Name = "labxxxxx";
            this.labxxxxx.Size = new System.Drawing.Size(180, 13);
            this.labxxxxx.TabIndex = 0;
            this.labxxxxx.Text = "Montant du crédit Maximum autorisé:";
            // 
            // chAutorise
            // 
            this.chAutorise.AutoSize = true;
            this.chAutorise.Location = new System.Drawing.Point(13, 19);
            this.chAutorise.Margin = new System.Windows.Forms.Padding(2);
            this.chAutorise.Name = "chAutorise";
            this.chAutorise.Size = new System.Drawing.Size(211, 17);
            this.chAutorise.TabIndex = 0;
            this.chAutorise.Text = "Ce Client est autorisé à facturer à terme";
            this.chAutorise.UseVisualStyleBackColor = true;
            this.chAutorise.CheckedChanged += new System.EventHandler(this.chAutorise_CheckedChanged);
            // 
            // tpCD
            // 
            this.tpCD.BackColor = System.Drawing.SystemColors.Control;
            this.tpCD.Controls.Add(this.Panier);
            this.tpCD.Controls.Add(this.groupBox1);
            this.tpCD.Location = new System.Drawing.Point(4, 22);
            this.tpCD.Name = "tpCD";
            this.tpCD.Padding = new System.Windows.Forms.Padding(3);
            this.tpCD.Size = new System.Drawing.Size(372, 211);
            this.tpCD.TabIndex = 4;
            this.tpCD.Text = "Priorité";
            // 
            // Panier
            // 
            this.Panier.Controls.Add(this.label18);
            this.Panier.Controls.Add(this.label17);
            this.Panier.Controls.Add(this.txtPanierOrangeChrea);
            this.Panier.Controls.Add(this.txtPanierChrea);
            this.Panier.Location = new System.Drawing.Point(13, 99);
            this.Panier.Name = "Panier";
            this.Panier.Size = new System.Drawing.Size(338, 70);
            this.Panier.TabIndex = 3;
            this.Panier.TabStop = false;
            this.Panier.Text = "Panier";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(32, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Chréa:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(182, 25);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Chréa-Pulpe:";
            // 
            // txtPanierOrangeChrea
            // 
            this.txtPanierOrangeChrea.Location = new System.Drawing.Point(262, 22);
            this.txtPanierOrangeChrea.Name = "txtPanierOrangeChrea";
            this.txtPanierOrangeChrea.Size = new System.Drawing.Size(67, 20);
            this.txtPanierOrangeChrea.TabIndex = 0;
            // 
            // txtPanierChrea
            // 
            this.txtPanierChrea.Location = new System.Drawing.Point(88, 23);
            this.txtPanierChrea.Name = "txtPanierChrea";
            this.txtPanierChrea.Size = new System.Drawing.Size(67, 20);
            this.txtPanierChrea.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.CDChrea);
            this.groupBox1.Controls.Add(this.CDOrangeChrea);
            this.groupBox1.Controls.Add(this.txtIntiChrea);
            this.groupBox1.Controls.Add(this.InitOrangeChrea);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(340, 81);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Couldowns en Bouteilles";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(177, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Valuer Actuelle:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(177, 20);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Valuer Actuelle:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Chréa:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Chréa-Pulpe:";
            // 
            // CDChrea
            // 
            this.CDChrea.Location = new System.Drawing.Point(266, 16);
            this.CDChrea.Name = "CDChrea";
            this.CDChrea.Size = new System.Drawing.Size(67, 20);
            this.CDChrea.TabIndex = 1;
            // 
            // CDOrangeChrea
            // 
            this.CDOrangeChrea.Location = new System.Drawing.Point(266, 46);
            this.CDOrangeChrea.Name = "CDOrangeChrea";
            this.CDOrangeChrea.Size = new System.Drawing.Size(65, 20);
            this.CDOrangeChrea.TabIndex = 0;
            // 
            // txtIntiChrea
            // 
            this.txtIntiChrea.Location = new System.Drawing.Point(89, 19);
            this.txtIntiChrea.Name = "txtIntiChrea";
            this.txtIntiChrea.Size = new System.Drawing.Size(67, 20);
            this.txtIntiChrea.TabIndex = 1;
            // 
            // InitOrangeChrea
            // 
            this.InitOrangeChrea.Location = new System.Drawing.Point(89, 49);
            this.InitOrangeChrea.Name = "InitOrangeChrea";
            this.InitOrangeChrea.Size = new System.Drawing.Size(67, 20);
            this.InitOrangeChrea.TabIndex = 0;
            // 
            // btActiveNouveau
            // 
            this.btActiveNouveau.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btActiveNouveau.Location = new System.Drawing.Point(12, 252);
            this.btActiveNouveau.Margin = new System.Windows.Forms.Padding(2);
            this.btActiveNouveau.Name = "btActiveNouveau";
            this.btActiveNouveau.Size = new System.Drawing.Size(167, 26);
            this.btActiveNouveau.TabIndex = 20;
            this.btActiveNouveau.Text = "Activer ce Nouveau Compte";
            this.btActiveNouveau.UseVisualStyleBackColor = true;
            this.btActiveNouveau.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbCategorieClient
            // 
            this.cbCategorieClient.FormattingEnabled = true;
            this.cbCategorieClient.Location = new System.Drawing.Point(141, 36);
            this.cbCategorieClient.Name = "cbCategorieClient";
            this.cbCategorieClient.Size = new System.Drawing.Size(180, 21);
            this.cbCategorieClient.TabIndex = 19;
            this.cbCategorieClient.SelectedIndexChanged += new System.EventHandler(this.cbTypeClient_SelectedIndexChanged_1);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(42, 38);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 13);
            this.label20.TabIndex = 20;
            this.label20.Text = "Categorie Client:";
            // 
            // txtNomraison
            // 
            this.txtNomraison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomraison.Location = new System.Drawing.Point(139, 20);
            this.txtNomraison.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomraison.Name = "txtNomraison";
            this.txtNomraison.Size = new System.Drawing.Size(180, 20);
            this.txtNomraison.TabIndex = 6;
            // 
            // txtAdresse
            // 
            this.txtAdresse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse.Location = new System.Drawing.Point(139, 41);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.Multiline = true;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(180, 55);
            this.txtAdresse.TabIndex = 7;
            // 
            // txtTelephone
            // 
            this.txtTelephone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTelephone.Location = new System.Drawing.Point(139, 98);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.Size = new System.Drawing.Size(180, 20);
            this.txtTelephone.TabIndex = 8;
            // 
            // txtRegistre
            // 
            this.txtRegistre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRegistre.Location = new System.Drawing.Point(131, 35);
            this.txtRegistre.Margin = new System.Windows.Forms.Padding(2);
            this.txtRegistre.Name = "txtRegistre";
            this.txtRegistre.Size = new System.Drawing.Size(180, 20);
            this.txtRegistre.TabIndex = 7;
            // 
            // txtFiscal
            // 
            this.txtFiscal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFiscal.Location = new System.Drawing.Point(131, 58);
            this.txtFiscal.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiscal.Name = "txtFiscal";
            this.txtFiscal.Size = new System.Drawing.Size(180, 20);
            this.txtFiscal.TabIndex = 7;
            // 
            // txtArticle
            // 
            this.txtArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtArticle.Location = new System.Drawing.Point(131, 80);
            this.txtArticle.Margin = new System.Windows.Forms.Padding(2);
            this.txtArticle.Name = "txtArticle";
            this.txtArticle.Size = new System.Drawing.Size(180, 20);
            this.txtArticle.TabIndex = 7;
            // 
            // FrEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 292);
            this.Controls.Add(this.btActiveNouveau);
            this.Controls.Add(this.tcClient);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistre);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrEdit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Modifier Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminEmployerNew_FormClosing);
            this.panReg.ResumeLayout(false);
            this.panReg.PerformLayout();
            this.tcClient.ResumeLayout(false);
            this.tpInformation.ResumeLayout(false);
            this.tpInformation.PerformLayout();
            this.tpRegistre.ResumeLayout(false);
            this.tpRegistre.PerformLayout();
            this.tpAutorisation.ResumeLayout(false);
            this.tpAutorisation.PerformLayout();
            this.panFactAterme.ResumeLayout(false);
            this.panFactAterme.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbFactIgneore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud)).EndInit();
            this.tpCD.ResumeLayout(false);
            this.Panier.ResumeLayout(false);
            this.Panier.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonEnregistre;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label Activite;
        private System.Windows.Forms.CheckBox chIsParent;
        private System.Windows.Forms.ComboBox cbParentClient;
        private TextBoxx txtNomraison;
        private TextBoxx txtAdresse;
        private TextBoxx txtTelephone;
        private System.Windows.Forms.Panel panReg;
        private TextBoxx txtRegistre;
        private TextBoxx txtFiscal;
        private TextBoxx txtArticle;
        private System.Windows.Forms.TabControl tcClient;
        private System.Windows.Forms.TabPage tpInformation;
        private System.Windows.Forms.TabPage tpRegistre;
        private System.Windows.Forms.TabPage tpAutorisation;
        private System.Windows.Forms.Panel panFactAterme;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMontantAutorise;
        private System.Windows.Forms.Label labxxxxx;
        private System.Windows.Forms.CheckBox chAutorise;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nud;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labMontant;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTypeClient;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCodeSage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chExpire;
        private System.Windows.Forms.CheckBox chRemise;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nbFactIgneore;
        private System.Windows.Forms.TabPage tpCD;
        private System.Windows.Forms.GroupBox Panier;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtPanierOrangeChrea;
        private System.Windows.Forms.TextBox txtPanierChrea;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox CDChrea;
        private System.Windows.Forms.TextBox CDOrangeChrea;
        private System.Windows.Forms.TextBox txtIntiChrea;
        private System.Windows.Forms.TextBox InitOrangeChrea;
        private System.Windows.Forms.Button btActiveNouveau;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbwilaya;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbCategorieClient;
    }
}