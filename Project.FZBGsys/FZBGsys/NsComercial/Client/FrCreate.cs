﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys;

using FZBGsys.NsTools;


namespace FZBGsys.NsComercial.NsClient
{
    public partial class FrCreate : Form
    {
        public static Client ReturnClient { get; set; }
        public bool IsSelectionDialogReturn { get; set; }
        private ModelEntities db;
        public FrCreate(ModelEntities db = null, bool IsSelectionDialogReturn = false)
        {
            this.db = db;
            if (db == null) this.db = new ModelEntities();
            InitializeComponent();
            this.IsSelectionDialogReturn = IsSelectionDialogReturn;
            InitializeData();
            //this.IsSelectionDialogReturn = IsSelectionDialogReturn;
            if (Tools.isBoss != true)
            {
                tcClient.TabPages.Remove(tpAutorisation);
            }
        }

        public FrCreate(bool ActiveAutorisation)
        {
            //   this.db = db;

            InitializeComponent();
            this.IsSelectionDialogReturn = false;
            InitializeData();
            /*   if (ActiveAutorisation)
               {
                   tcClient.TabPages.Remove(tpAutorisation);
               }*/

            if (Tools.isBoss != true)
            {
                tcClient.TabPages.Remove(tpAutorisation);
            }
        }

        public static Client CreateClientDialog(ModelEntities db)
        {
            FrCreate e =
            new FrCreate(db, true);
            //          e.IsSelectionDialogReturn = true;
            e.ShowDialog();

            return ReturnClient;

        }
        private void InitializeData()
        {
            cbTypeClient.Items.Clear();
            cbTypeClient.Items.Add(new TypeClient { ID = 0, Nom = "" });
            cbTypeClient.Items.AddRange(db.TypeClients.Where(p => p.ID > 0).ToArray());
            cbTypeClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeClient.SelectedIndex = 0;
            cbTypeClient.ValueMember = "ID";
            cbTypeClient.DisplayMember = "Nom";

            cbCategorieClient.Items.Clear();
            cbCategorieClient.Items.Add(new CategorieClient { ID = 0, Nom = "" });
            cbCategorieClient.Items.AddRange(db.CategorieClients.ToArray());
            cbCategorieClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorieClient.SelectedIndex = 0;
            cbCategorieClient.ValueMember = "ID";
            cbCategorieClient.DisplayMember = "Nom";

            /* chAdd.Visible = true;
             if (IsSelectionDialogReturn)
             {
                 chAdd.Enabled = true;
                 chAdd.Checked = false;

             }
             else {
                 chAdd.Enabled = false;
                 chAdd.Checked = true;
            
             }*/

            cbParentClient.Items.Add(new ClientParent { ID = 0, Nom = "" });
            cbParentClient.Items.AddRange(db.ClientParents.Where(p => p.ID > 0).ToArray());
            cbParentClient.SelectedIndex = 0;
            cbParentClient.ValueMember = "ID";
            cbParentClient.DisplayMember = "Nom";
            //     cbParentClient.DropDownStyle = ComboBoxStyle.DropDownList;


        }
        private void buttonEnregistrerEmployer_Click(object sender, EventArgs e)
        {
            if (isValidAll())
            {
                DateTime now = Tools.GetServerDateTime();
                ReturnClient = new Client()
                 {

                     //   EquipeID = ((MarbreBLIDA.Equipe)cbEquipe.SelectedItem).ID,
                     Nom = txtNomraison.Text.Trim().ToUpper(),
                     Adresse = txtAdresse.Text.Trim().ToUpper(),
                     Telephone = txtTelephone.Text.Trim().ToUpper(),
                     IsSoldable = true,
                     Solde = 0,
                     XSolde = null,
                     SoldeHistory = 0,
                     DateHistory = now,
                     DateInscription = now,
                     InscriptionUID = Tools.CurrentUserID,
                     IsCompteBloque = true,
                     CategorieClient = (CategorieClient)cbCategorieClient.SelectedItem,
                     Wilaya = cbwilaya.SelectedItem.ToString(),

                 };

                ReturnClient.TypeClient = (TypeClient)cbTypeClient.SelectedItem;
                if (ReturnClient.TypeClient.HasRegistre.Value)
                {
                    ReturnClient.NoRegistre = txtRegistre.Text.Trim().ToUpper();
                    ReturnClient.NoArticle = txtArticle.Text.Trim().ToUpper();
                    ReturnClient.NoFiscale = txtFiscal.Text.Trim();
                    ReturnClient.CodeSage = txtCodeSage.Text.Trim();


                }
                else
                {
                    ReturnClient.NoRegistre = null;
                    ReturnClient.NoArticle = null;
                    ReturnClient.NoFiscale = null;
                    ReturnClient.CodeSage = "DV" + ReturnClient.ID;


                }


                if (chIsParent.Checked)
                {
                    var selected = cbParentClient.SelectedItem;
                    if (selected != null)
                    {
                        var parent = (ClientParent)cbParentClient.SelectedItem;
                        ReturnClient.ClientParentID = parent.ID;


                    }
                    else // new parent
                    {
                        ReturnClient.ClientParent = new ClientParent() { Nom = cbParentClient.Text };
                    }
                }
                else
                {
                    ReturnClient.ClientParentID = 0; // 0 means no parent
                }

                ReturnClient.IsAutoriseCredit = chAutorise.Checked;
                if (chAutorise.Checked)
                {
                    ReturnClient.MaxCreditMontant = txtMontantAutorise.Text.ParseToDec();
                    ReturnClient.MaxCreditDeletJours = (int)nud.Value;
                }
                else
                {
                    ReturnClient.MaxCreditMontant = null;
                    ReturnClient.MaxCreditDeletJours = null;
                }


                if (!IsSelectionDialogReturn)
                {
                    db.AddToClients(ReturnClient);
                    db.SaveChanges();

                }
                /*  else if (chAdd.Checked)
                  {
                      db.AddToClients(ReturnClient);

                      db.SaveChanges();

                  }
  */
                Dispose();

            }
        }
        private bool isValidAll()
        {
            string ErrorMessage = String.Empty;
            if (panReg.Visible)
            {
                var code = txtCodeSage.Text.Trim();
                if (!code.StartsWith("CL") || code.Replace("CL", "").ParseToInt() == null)
                {
                    ErrorMessage = "Code Sage Invalide!";
                }
                else
                {
                    var existingCode = db.Clients.Where(p => p.CodeSage == code);
                    var existingCode2 = db.Soldes23072012.Where(p => p.code == code);
                    if (existingCode.Count() != 0)
                    {
                        ErrorMessage = "Ce code Sage existe déja pour un autre client!!!!";
                    }
                    else

                        if (existingCode2.Count() != 0)
                        {
                            ErrorMessage = "Ce code Sage existe déja pour un autre client!!!!";
                        }
                }
            }

            if (txtNomraison.Text.Trim().Length < 1 || txtNomraison.Text.Length > 100)
            {

                ErrorMessage += "\nCe Nom est Invalide (doit faire entre 2 et 100 caracteres).";
            }
            else
            {
                var matricule = txtNomraison.Text.Trim();
                var Uti = db.Clients.FirstOrDefault(em => em.Nom == matricule);

                if (Uti != null)
                {
                    ErrorMessage += "\nCe Nom existe déja dans la liste des clients";

                }
            }
            if (txtAdresse.Text == "")
            {
                ErrorMessage += "\nVeillez Entrer Adresse Client";
            }

            if (cbTypeClient.SelectedIndex < 1)
            {
                ErrorMessage += "\nVeillez selectionner un type Client";
            }

            if (panReg.Visible)
            {
                //  informations registre commerce
            }

            if (chIsParent.Checked && cbParentClient.SelectedIndex == 0)
            {
                ErrorMessage += "\nVeillez spécifier ou selectionné le nom du parent";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                // MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminEmployerNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialogReturn)
            {
                db.Dispose();
            }
        }
        private void buttonAnnulerEmployer_Click(object sender, EventArgs e)
        {
            ReturnClient = null;
            Dispose();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void chIsParent_CheckedChanged(object sender, EventArgs e)
        {
            cbParentClient.Enabled = chIsParent.Checked;
        }

        private void txtNomraison_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void cbTypeClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTypeClient.SelectedIndex > 0)
            {
                panReg.Visible = ((TypeClient)cbTypeClient.SelectedItem).HasRegistre.Value;
            }
        }

        private void cbTypeClient_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var TypeClient = (TypeClient)cbTypeClient.SelectedItem;
            if (TypeClient.ID > 0)
            {
                panReg.Visible = TypeClient.HasRegistre.Value;
            }
        }

        private void txtMontantAutorise_TextChanged(object sender, EventArgs e)
        {
            var montant = txtMontantAutorise.Text.Trim().ParseToDec();
            if (montant != null)
            {
                labMontant.Text = CtoL.convertMontant(montant.Value);
            }
        }

        private void chAutorise_CheckedChanged(object sender, EventArgs e)
        {
            panFactAterme.Enabled = chAutorise.Checked;
        }

        private void chIsParent_CheckedChanged_1(object sender, EventArgs e)
        {
            cbParentClient.Visible = chIsParent.Checked;
        }


    }
}
