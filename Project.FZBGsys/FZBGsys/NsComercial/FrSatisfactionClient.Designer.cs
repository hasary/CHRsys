﻿namespace FZBGsys.NsComercial
{
    partial class FrSatisfactionClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.cbClient = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.raDisponibilite0 = new System.Windows.Forms.RadioButton();
            this.raDisponibilite1 = new System.Windows.Forms.RadioButton();
            this.raDisponibilite2 = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.raEmbalage0 = new System.Windows.Forms.RadioButton();
            this.raEmbalage1 = new System.Windows.Forms.RadioButton();
            this.raEmbalage2 = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.raVariete0 = new System.Windows.Forms.RadioButton();
            this.raVariete1 = new System.Windows.Forms.RadioButton();
            this.raVariete2 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.raGout0 = new System.Windows.Forms.RadioButton();
            this.raGout1 = new System.Windows.Forms.RadioButton();
            this.raGout2 = new System.Windows.Forms.RadioButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.raRelationEntreprise0 = new System.Windows.Forms.RadioButton();
            this.raRelationEntreprise1 = new System.Windows.Forms.RadioButton();
            this.raRelationEntreprise2 = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.raRespectCommande0 = new System.Windows.Forms.RadioButton();
            this.raRespectCommande1 = new System.Windows.Forms.RadioButton();
            this.raRespectCommande2 = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.raRespectDelais0 = new System.Windows.Forms.RadioButton();
            this.raRespectDelais1 = new System.Windows.Forms.RadioButton();
            this.raRespectDelais2 = new System.Windows.Forms.RadioButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.raProximite0 = new System.Windows.Forms.RadioButton();
            this.raProximite1 = new System.Windows.Forms.RadioButton();
            this.raProximite2 = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.raRappotQP0 = new System.Windows.Forms.RadioButton();
            this.raRappotQP1 = new System.Windows.Forms.RadioButton();
            this.raRappotQP2 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbWeek = new System.Windows.Forms.ComboBox();
            this.dtYear = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.labNumTel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtObsevation = new System.Windows.Forms.TextBox();
            this.btAutoSat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(411, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(727, 481);
            this.dgv.TabIndex = 7;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Location = new System.Drawing.Point(788, 497);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(349, 44);
            this.xpannel1.TabIndex = 19;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(209, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(132, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Fermer";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(413, 502);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(271, 501);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 20;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // cbClient
            // 
            this.cbClient.FormattingEnabled = true;
            this.cbClient.Location = new System.Drawing.Point(33, 117);
            this.cbClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbClient.Name = "cbClient";
            this.cbClient.Size = new System.Drawing.Size(356, 24);
            this.cbClient.TabIndex = 15;
            this.cbClient.SelectedIndexChanged += new System.EventHandler(this.cbClient_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Client";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(12, 185);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(392, 241);
            this.tabControl1.TabIndex = 38;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(384, 212);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Qualite";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.raDisponibilite0);
            this.panel4.Controls.Add(this.raDisponibilite1);
            this.panel4.Controls.Add(this.raDisponibilite2);
            this.panel4.Location = new System.Drawing.Point(8, 154);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(365, 33);
            this.panel4.TabIndex = 0;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 6);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Disponibilité";
            // 
            // raDisponibilite0
            // 
            this.raDisponibilite0.AutoSize = true;
            this.raDisponibilite0.Location = new System.Drawing.Point(301, 6);
            this.raDisponibilite0.Margin = new System.Windows.Forms.Padding(4);
            this.raDisponibilite0.Name = "raDisponibilite0";
            this.raDisponibilite0.Size = new System.Drawing.Size(33, 21);
            this.raDisponibilite0.TabIndex = 3;
            this.raDisponibilite0.TabStop = true;
            this.raDisponibilite0.Text = ".";
            this.raDisponibilite0.UseVisualStyleBackColor = true;
            // 
            // raDisponibilite1
            // 
            this.raDisponibilite1.AutoSize = true;
            this.raDisponibilite1.Location = new System.Drawing.Point(235, 6);
            this.raDisponibilite1.Margin = new System.Windows.Forms.Padding(4);
            this.raDisponibilite1.Name = "raDisponibilite1";
            this.raDisponibilite1.Size = new System.Drawing.Size(33, 21);
            this.raDisponibilite1.TabIndex = 2;
            this.raDisponibilite1.TabStop = true;
            this.raDisponibilite1.Text = ".";
            this.raDisponibilite1.UseVisualStyleBackColor = true;
            // 
            // raDisponibilite2
            // 
            this.raDisponibilite2.AutoSize = true;
            this.raDisponibilite2.Location = new System.Drawing.Point(172, 6);
            this.raDisponibilite2.Margin = new System.Windows.Forms.Padding(4);
            this.raDisponibilite2.Name = "raDisponibilite2";
            this.raDisponibilite2.Size = new System.Drawing.Size(33, 21);
            this.raDisponibilite2.TabIndex = 1;
            this.raDisponibilite2.TabStop = true;
            this.raDisponibilite2.Text = ".";
            this.raDisponibilite2.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.raEmbalage0);
            this.panel3.Controls.Add(this.raEmbalage1);
            this.panel3.Controls.Add(this.raEmbalage2);
            this.panel3.Location = new System.Drawing.Point(8, 113);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(365, 33);
            this.panel3.TabIndex = 0;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "Emballage";
            // 
            // raEmbalage0
            // 
            this.raEmbalage0.AutoSize = true;
            this.raEmbalage0.Location = new System.Drawing.Point(301, 6);
            this.raEmbalage0.Margin = new System.Windows.Forms.Padding(4);
            this.raEmbalage0.Name = "raEmbalage0";
            this.raEmbalage0.Size = new System.Drawing.Size(33, 21);
            this.raEmbalage0.TabIndex = 3;
            this.raEmbalage0.TabStop = true;
            this.raEmbalage0.Text = ".";
            this.raEmbalage0.UseVisualStyleBackColor = true;
            // 
            // raEmbalage1
            // 
            this.raEmbalage1.AutoSize = true;
            this.raEmbalage1.Location = new System.Drawing.Point(235, 6);
            this.raEmbalage1.Margin = new System.Windows.Forms.Padding(4);
            this.raEmbalage1.Name = "raEmbalage1";
            this.raEmbalage1.Size = new System.Drawing.Size(33, 21);
            this.raEmbalage1.TabIndex = 2;
            this.raEmbalage1.TabStop = true;
            this.raEmbalage1.Text = ".";
            this.raEmbalage1.UseVisualStyleBackColor = true;
            // 
            // raEmbalage2
            // 
            this.raEmbalage2.AutoSize = true;
            this.raEmbalage2.Location = new System.Drawing.Point(172, 6);
            this.raEmbalage2.Margin = new System.Windows.Forms.Padding(4);
            this.raEmbalage2.Name = "raEmbalage2";
            this.raEmbalage2.Size = new System.Drawing.Size(33, 21);
            this.raEmbalage2.TabIndex = 1;
            this.raEmbalage2.TabStop = true;
            this.raEmbalage2.Text = ".";
            this.raEmbalage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.raVariete0);
            this.panel2.Controls.Add(this.raVariete1);
            this.panel2.Controls.Add(this.raVariete2);
            this.panel2.Location = new System.Drawing.Point(7, 73);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(365, 33);
            this.panel2.TabIndex = 0;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Variété";
            // 
            // raVariete0
            // 
            this.raVariete0.AutoSize = true;
            this.raVariete0.Location = new System.Drawing.Point(301, 6);
            this.raVariete0.Margin = new System.Windows.Forms.Padding(4);
            this.raVariete0.Name = "raVariete0";
            this.raVariete0.Size = new System.Drawing.Size(33, 21);
            this.raVariete0.TabIndex = 3;
            this.raVariete0.TabStop = true;
            this.raVariete0.Text = ".";
            this.raVariete0.UseVisualStyleBackColor = true;
            // 
            // raVariete1
            // 
            this.raVariete1.AutoSize = true;
            this.raVariete1.Location = new System.Drawing.Point(235, 6);
            this.raVariete1.Margin = new System.Windows.Forms.Padding(4);
            this.raVariete1.Name = "raVariete1";
            this.raVariete1.Size = new System.Drawing.Size(33, 21);
            this.raVariete1.TabIndex = 2;
            this.raVariete1.TabStop = true;
            this.raVariete1.Text = ".";
            this.raVariete1.UseVisualStyleBackColor = true;
            // 
            // raVariete2
            // 
            this.raVariete2.AutoSize = true;
            this.raVariete2.Location = new System.Drawing.Point(172, 6);
            this.raVariete2.Margin = new System.Windows.Forms.Padding(4);
            this.raVariete2.Name = "raVariete2";
            this.raVariete2.Size = new System.Drawing.Size(33, 21);
            this.raVariete2.TabIndex = 1;
            this.raVariete2.TabStop = true;
            this.raVariete2.Text = ".";
            this.raVariete2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.raGout0);
            this.panel1.Controls.Add(this.raGout1);
            this.panel1.Controls.Add(this.raGout2);
            this.panel1.Location = new System.Drawing.Point(8, 7);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(365, 62);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Gôut";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(273, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "non satisfait";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(213, 6);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "satisfait";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(121, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Très satisfait";
            // 
            // raGout0
            // 
            this.raGout0.AutoSize = true;
            this.raGout0.Location = new System.Drawing.Point(301, 34);
            this.raGout0.Margin = new System.Windows.Forms.Padding(4);
            this.raGout0.Name = "raGout0";
            this.raGout0.Size = new System.Drawing.Size(33, 21);
            this.raGout0.TabIndex = 3;
            this.raGout0.TabStop = true;
            this.raGout0.Text = ".";
            this.raGout0.UseVisualStyleBackColor = true;
            // 
            // raGout1
            // 
            this.raGout1.AutoSize = true;
            this.raGout1.Location = new System.Drawing.Point(235, 34);
            this.raGout1.Margin = new System.Windows.Forms.Padding(4);
            this.raGout1.Name = "raGout1";
            this.raGout1.Size = new System.Drawing.Size(33, 21);
            this.raGout1.TabIndex = 2;
            this.raGout1.TabStop = true;
            this.raGout1.Text = ".";
            this.raGout1.UseVisualStyleBackColor = true;
            // 
            // raGout2
            // 
            this.raGout2.AutoSize = true;
            this.raGout2.Location = new System.Drawing.Point(172, 34);
            this.raGout2.Margin = new System.Windows.Forms.Padding(4);
            this.raGout2.Name = "raGout2";
            this.raGout2.Size = new System.Drawing.Size(33, 21);
            this.raGout2.TabIndex = 1;
            this.raGout2.TabStop = true;
            this.raGout2.Text = ".";
            this.raGout2.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Controls.Add(this.panel8);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(384, 212);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Préstation";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label1);
            this.panel6.Controls.Add(this.raRelationEntreprise0);
            this.panel6.Controls.Add(this.raRelationEntreprise1);
            this.panel6.Controls.Add(this.raRelationEntreprise2);
            this.panel6.Location = new System.Drawing.Point(9, 110);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(365, 33);
            this.panel6.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Relation / entreprise";
            // 
            // raRelationEntreprise0
            // 
            this.raRelationEntreprise0.AutoSize = true;
            this.raRelationEntreprise0.Location = new System.Drawing.Point(301, 6);
            this.raRelationEntreprise0.Margin = new System.Windows.Forms.Padding(4);
            this.raRelationEntreprise0.Name = "raRelationEntreprise0";
            this.raRelationEntreprise0.Size = new System.Drawing.Size(33, 21);
            this.raRelationEntreprise0.TabIndex = 3;
            this.raRelationEntreprise0.TabStop = true;
            this.raRelationEntreprise0.Text = ".";
            this.raRelationEntreprise0.UseVisualStyleBackColor = true;
            // 
            // raRelationEntreprise1
            // 
            this.raRelationEntreprise1.AutoSize = true;
            this.raRelationEntreprise1.Location = new System.Drawing.Point(235, 6);
            this.raRelationEntreprise1.Margin = new System.Windows.Forms.Padding(4);
            this.raRelationEntreprise1.Name = "raRelationEntreprise1";
            this.raRelationEntreprise1.Size = new System.Drawing.Size(33, 21);
            this.raRelationEntreprise1.TabIndex = 2;
            this.raRelationEntreprise1.TabStop = true;
            this.raRelationEntreprise1.Text = ".";
            this.raRelationEntreprise1.UseVisualStyleBackColor = true;
            // 
            // raRelationEntreprise2
            // 
            this.raRelationEntreprise2.AutoSize = true;
            this.raRelationEntreprise2.Location = new System.Drawing.Point(172, 6);
            this.raRelationEntreprise2.Margin = new System.Windows.Forms.Padding(4);
            this.raRelationEntreprise2.Name = "raRelationEntreprise2";
            this.raRelationEntreprise2.Size = new System.Drawing.Size(33, 21);
            this.raRelationEntreprise2.TabIndex = 1;
            this.raRelationEntreprise2.TabStop = true;
            this.raRelationEntreprise2.Text = ".";
            this.raRelationEntreprise2.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.raRespectCommande0);
            this.panel7.Controls.Add(this.raRespectCommande1);
            this.panel7.Controls.Add(this.raRespectCommande2);
            this.panel7.Location = new System.Drawing.Point(8, 69);
            this.panel7.Margin = new System.Windows.Forms.Padding(4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(365, 33);
            this.panel7.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 6);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(127, 17);
            this.label11.TabIndex = 5;
            this.label11.Text = "Respct Commande";
            // 
            // raRespectCommande0
            // 
            this.raRespectCommande0.AutoSize = true;
            this.raRespectCommande0.Location = new System.Drawing.Point(301, 6);
            this.raRespectCommande0.Margin = new System.Windows.Forms.Padding(4);
            this.raRespectCommande0.Name = "raRespectCommande0";
            this.raRespectCommande0.Size = new System.Drawing.Size(33, 21);
            this.raRespectCommande0.TabIndex = 3;
            this.raRespectCommande0.TabStop = true;
            this.raRespectCommande0.Text = ".";
            this.raRespectCommande0.UseVisualStyleBackColor = true;
            // 
            // raRespectCommande1
            // 
            this.raRespectCommande1.AutoSize = true;
            this.raRespectCommande1.Location = new System.Drawing.Point(235, 6);
            this.raRespectCommande1.Margin = new System.Windows.Forms.Padding(4);
            this.raRespectCommande1.Name = "raRespectCommande1";
            this.raRespectCommande1.Size = new System.Drawing.Size(33, 21);
            this.raRespectCommande1.TabIndex = 2;
            this.raRespectCommande1.TabStop = true;
            this.raRespectCommande1.Text = ".";
            this.raRespectCommande1.UseVisualStyleBackColor = true;
            // 
            // raRespectCommande2
            // 
            this.raRespectCommande2.AutoSize = true;
            this.raRespectCommande2.Location = new System.Drawing.Point(172, 6);
            this.raRespectCommande2.Margin = new System.Windows.Forms.Padding(4);
            this.raRespectCommande2.Name = "raRespectCommande2";
            this.raRespectCommande2.Size = new System.Drawing.Size(33, 21);
            this.raRespectCommande2.TabIndex = 1;
            this.raRespectCommande2.TabStop = true;
            this.raRespectCommande2.Text = ".";
            this.raRespectCommande2.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.label13);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.label15);
            this.panel8.Controls.Add(this.raRespectDelais0);
            this.panel8.Controls.Add(this.raRespectDelais1);
            this.panel8.Controls.Add(this.raRespectDelais2);
            this.panel8.Location = new System.Drawing.Point(9, 4);
            this.panel8.Margin = new System.Windows.Forms.Padding(4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(365, 62);
            this.panel8.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 34);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(128, 17);
            this.label12.TabIndex = 5;
            this.label12.Text = "Respect des délais";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(273, 6);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 17);
            this.label13.TabIndex = 4;
            this.label13.Text = "non satisfait";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(213, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 17);
            this.label14.TabIndex = 4;
            this.label14.Text = "satisfait";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(121, 6);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 17);
            this.label15.TabIndex = 4;
            this.label15.Text = "Très satisfait";
            // 
            // raRespectDelais0
            // 
            this.raRespectDelais0.AutoSize = true;
            this.raRespectDelais0.Location = new System.Drawing.Point(301, 34);
            this.raRespectDelais0.Margin = new System.Windows.Forms.Padding(4);
            this.raRespectDelais0.Name = "raRespectDelais0";
            this.raRespectDelais0.Size = new System.Drawing.Size(33, 21);
            this.raRespectDelais0.TabIndex = 3;
            this.raRespectDelais0.TabStop = true;
            this.raRespectDelais0.Text = ".";
            this.raRespectDelais0.UseVisualStyleBackColor = true;
            // 
            // raRespectDelais1
            // 
            this.raRespectDelais1.AutoSize = true;
            this.raRespectDelais1.Location = new System.Drawing.Point(235, 34);
            this.raRespectDelais1.Margin = new System.Windows.Forms.Padding(4);
            this.raRespectDelais1.Name = "raRespectDelais1";
            this.raRespectDelais1.Size = new System.Drawing.Size(33, 21);
            this.raRespectDelais1.TabIndex = 2;
            this.raRespectDelais1.TabStop = true;
            this.raRespectDelais1.Text = ".";
            this.raRespectDelais1.UseVisualStyleBackColor = true;
            // 
            // raRespectDelais2
            // 
            this.raRespectDelais2.AutoSize = true;
            this.raRespectDelais2.Location = new System.Drawing.Point(172, 34);
            this.raRespectDelais2.Margin = new System.Windows.Forms.Padding(4);
            this.raRespectDelais2.Name = "raRespectDelais2";
            this.raRespectDelais2.Size = new System.Drawing.Size(33, 21);
            this.raRespectDelais2.TabIndex = 1;
            this.raRespectDelais2.TabStop = true;
            this.raRespectDelais2.Text = ".";
            this.raRespectDelais2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(384, 212);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Environnement";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.raProximite0);
            this.panel5.Controls.Add(this.raProximite1);
            this.panel5.Controls.Add(this.raProximite2);
            this.panel5.Location = new System.Drawing.Point(8, 70);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(365, 33);
            this.panel5.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 6);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 17);
            this.label10.TabIndex = 5;
            this.label10.Text = "Proximité du produit";
            // 
            // raProximite0
            // 
            this.raProximite0.AutoSize = true;
            this.raProximite0.Location = new System.Drawing.Point(303, 6);
            this.raProximite0.Margin = new System.Windows.Forms.Padding(4);
            this.raProximite0.Name = "raProximite0";
            this.raProximite0.Size = new System.Drawing.Size(33, 21);
            this.raProximite0.TabIndex = 3;
            this.raProximite0.TabStop = true;
            this.raProximite0.Text = ".";
            this.raProximite0.UseVisualStyleBackColor = true;
            // 
            // raProximite1
            // 
            this.raProximite1.AutoSize = true;
            this.raProximite1.Location = new System.Drawing.Point(236, 6);
            this.raProximite1.Margin = new System.Windows.Forms.Padding(4);
            this.raProximite1.Name = "raProximite1";
            this.raProximite1.Size = new System.Drawing.Size(33, 21);
            this.raProximite1.TabIndex = 2;
            this.raProximite1.TabStop = true;
            this.raProximite1.Text = ".";
            this.raProximite1.UseVisualStyleBackColor = true;
            // 
            // raProximite2
            // 
            this.raProximite2.AutoSize = true;
            this.raProximite2.Location = new System.Drawing.Point(173, 6);
            this.raProximite2.Margin = new System.Windows.Forms.Padding(4);
            this.raProximite2.Name = "raProximite2";
            this.raProximite2.Size = new System.Drawing.Size(33, 21);
            this.raProximite2.TabIndex = 1;
            this.raProximite2.TabStop = true;
            this.raProximite2.Text = ".";
            this.raProximite2.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label16);
            this.panel9.Controls.Add(this.label17);
            this.panel9.Controls.Add(this.label18);
            this.panel9.Controls.Add(this.label19);
            this.panel9.Controls.Add(this.raRappotQP0);
            this.panel9.Controls.Add(this.raRappotQP1);
            this.panel9.Controls.Add(this.raRappotQP2);
            this.panel9.Location = new System.Drawing.Point(9, 5);
            this.panel9.Margin = new System.Windows.Forms.Padding(4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(365, 62);
            this.panel9.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 34);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(139, 17);
            this.label16.TabIndex = 5;
            this.label16.Text = "Rapport qualité / prix";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(273, 6);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 17);
            this.label17.TabIndex = 4;
            this.label17.Text = "non satisfait";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(213, 6);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 17);
            this.label18.TabIndex = 4;
            this.label18.Text = "satisfait";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(121, 6);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 17);
            this.label19.TabIndex = 4;
            this.label19.Text = "Très satisfait";
            // 
            // raRappotQP0
            // 
            this.raRappotQP0.AutoSize = true;
            this.raRappotQP0.Location = new System.Drawing.Point(301, 34);
            this.raRappotQP0.Margin = new System.Windows.Forms.Padding(4);
            this.raRappotQP0.Name = "raRappotQP0";
            this.raRappotQP0.Size = new System.Drawing.Size(33, 21);
            this.raRappotQP0.TabIndex = 3;
            this.raRappotQP0.TabStop = true;
            this.raRappotQP0.Text = ".";
            this.raRappotQP0.UseVisualStyleBackColor = true;
            // 
            // raRappotQP1
            // 
            this.raRappotQP1.AutoSize = true;
            this.raRappotQP1.Location = new System.Drawing.Point(235, 34);
            this.raRappotQP1.Margin = new System.Windows.Forms.Padding(4);
            this.raRappotQP1.Name = "raRappotQP1";
            this.raRappotQP1.Size = new System.Drawing.Size(33, 21);
            this.raRappotQP1.TabIndex = 2;
            this.raRappotQP1.TabStop = true;
            this.raRappotQP1.Text = ".";
            this.raRappotQP1.UseVisualStyleBackColor = true;
            // 
            // raRappotQP2
            // 
            this.raRappotQP2.AutoSize = true;
            this.raRappotQP2.Location = new System.Drawing.Point(172, 34);
            this.raRappotQP2.Margin = new System.Windows.Forms.Padding(4);
            this.raRappotQP2.Name = "raRappotQP2";
            this.raRappotQP2.Size = new System.Drawing.Size(33, 21);
            this.raRappotQP2.TabIndex = 1;
            this.raRappotQP2.TabStop = true;
            this.raRappotQP2.Text = ".";
            this.raRappotQP2.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbWeek);
            this.groupBox1.Controls.Add(this.dtYear);
            this.groupBox1.Location = new System.Drawing.Point(17, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(387, 59);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Période";
            // 
            // cbWeek
            // 
            this.cbWeek.FormattingEnabled = true;
            this.cbWeek.Location = new System.Drawing.Point(121, 22);
            this.cbWeek.Margin = new System.Windows.Forms.Padding(4);
            this.cbWeek.Name = "cbWeek";
            this.cbWeek.Size = new System.Drawing.Size(251, 24);
            this.cbWeek.TabIndex = 1;
            this.cbWeek.SelectedIndexChanged += new System.EventHandler(this.cbWeek_SelectedIndexChanged);
            // 
            // dtYear
            // 
            this.dtYear.CustomFormat = "yyyy";
            this.dtYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtYear.Location = new System.Drawing.Point(16, 23);
            this.dtYear.Margin = new System.Windows.Forms.Padding(4);
            this.dtYear.Name = "dtYear";
            this.dtYear.ShowUpDown = true;
            this.dtYear.Size = new System.Drawing.Size(92, 22);
            this.dtYear.TabIndex = 0;
            this.dtYear.ValueChanged += new System.EventHandler(this.dtYear_ValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(33, 151);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 17);
            this.label20.TabIndex = 40;
            this.label20.Text = "Tel:";
            // 
            // labNumTel
            // 
            this.labNumTel.AutoSize = true;
            this.labNumTel.Location = new System.Drawing.Point(75, 151);
            this.labNumTel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labNumTel.Name = "labNumTel";
            this.labNumTel.Size = new System.Drawing.Size(16, 17);
            this.labNumTel.TabIndex = 41;
            this.labNumTel.Text = "0";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtObsevation);
            this.groupBox2.Location = new System.Drawing.Point(19, 436);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(384, 55);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Observation";
            // 
            // txtObsevation
            // 
            this.txtObsevation.Location = new System.Drawing.Point(5, 23);
            this.txtObsevation.Margin = new System.Windows.Forms.Padding(4);
            this.txtObsevation.Multiline = true;
            this.txtObsevation.Name = "txtObsevation";
            this.txtObsevation.Size = new System.Drawing.Size(373, 25);
            this.txtObsevation.TabIndex = 0;
            // 
            // btAutoSat
            // 
            this.btAutoSat.ForeColor = System.Drawing.Color.Red;
            this.btAutoSat.Location = new System.Drawing.Point(582, 502);
            this.btAutoSat.Name = "btAutoSat";
            this.btAutoSat.Size = new System.Drawing.Size(133, 32);
            this.btAutoSat.TabIndex = 43;
            this.btAutoSat.Text = "AutoSatisfation";
            this.btAutoSat.UseVisualStyleBackColor = true;
            this.btAutoSat.Click += new System.EventHandler(this.btAutoSat_Click);
            // 
            // FrSatisfactionClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 546);
            this.Controls.Add(this.btAutoSat);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labNumTel);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cbClient);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrSatisfactionClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Satisfaction Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrEvaluationFournisseur_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.ComboBox cbClient;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RadioButton raGout0;
        private System.Windows.Forms.RadioButton raGout1;
        private System.Windows.Forms.RadioButton raGout2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton raDisponibilite0;
        private System.Windows.Forms.RadioButton raDisponibilite1;
        private System.Windows.Forms.RadioButton raDisponibilite2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton raEmbalage0;
        private System.Windows.Forms.RadioButton raEmbalage1;
        private System.Windows.Forms.RadioButton raEmbalage2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton raVariete0;
        private System.Windows.Forms.RadioButton raVariete1;
        private System.Windows.Forms.RadioButton raVariete2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbWeek;
        private System.Windows.Forms.DateTimePicker dtYear;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton raRelationEntreprise0;
        private System.Windows.Forms.RadioButton raRelationEntreprise1;
        private System.Windows.Forms.RadioButton raRelationEntreprise2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RadioButton raRespectCommande0;
        private System.Windows.Forms.RadioButton raRespectCommande1;
        private System.Windows.Forms.RadioButton raRespectCommande2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton raRespectDelais0;
        private System.Windows.Forms.RadioButton raRespectDelais1;
        private System.Windows.Forms.RadioButton raRespectDelais2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RadioButton raProximite0;
        private System.Windows.Forms.RadioButton raProximite1;
        private System.Windows.Forms.RadioButton raProximite2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RadioButton raRappotQP0;
        private System.Windows.Forms.RadioButton raRappotQP1;
        private System.Windows.Forms.RadioButton raRappotQP2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labNumTel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtObsevation;
        private System.Windows.Forms.Button btAutoSat;
    }
}