﻿namespace FZBGsys.NsComercial
{
    partial class FrSelectMonth
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerJourn = new System.Windows.Forms.DateTimePicker();
            this.radioButtonPeriode = new System.Windows.Forms.RadioButton();
            this.panelPeriode = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePickerDu = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButtonMon = new System.Windows.Forms.RadioButton();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerMonth = new System.Windows.Forms.DateTimePicker();
            this.radioButtonDate = new System.Windows.Forms.RadioButton();
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.panelPeriode.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimePickerJourn
            // 
            this.dateTimePickerJourn.CustomFormat = "dd MMMM yyyy";
            this.dateTimePickerJourn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerJourn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerJourn.Location = new System.Drawing.Point(176, 52);
            this.dateTimePickerJourn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dateTimePickerJourn.Name = "dateTimePickerJourn";
            this.dateTimePickerJourn.Size = new System.Drawing.Size(200, 23);
            this.dateTimePickerJourn.TabIndex = 27;
            this.dateTimePickerJourn.Visible = false;
            // 
            // radioButtonPeriode
            // 
            this.radioButtonPeriode.AutoSize = true;
            this.radioButtonPeriode.Location = new System.Drawing.Point(29, 134);
            this.radioButtonPeriode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonPeriode.Name = "radioButtonPeriode";
            this.radioButtonPeriode.Size = new System.Drawing.Size(101, 21);
            this.radioButtonPeriode.TabIndex = 30;
            this.radioButtonPeriode.Text = "Périodique:";
            this.radioButtonPeriode.UseVisualStyleBackColor = true;
            this.radioButtonPeriode.CheckedChanged += new System.EventHandler(this.radioButtonPeriode_CheckedChanged);
            // 
            // panelPeriode
            // 
            this.panelPeriode.Controls.Add(this.label3);
            this.panelPeriode.Controls.Add(this.dateTimePickerDu);
            this.panelPeriode.Controls.Add(this.dateTimePickerAu);
            this.panelPeriode.Controls.Add(this.label5);
            this.panelPeriode.Location = new System.Drawing.Point(77, 159);
            this.panelPeriode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelPeriode.Name = "panelPeriode";
            this.panelPeriode.Size = new System.Drawing.Size(316, 65);
            this.panelPeriode.TabIndex = 28;
            this.panelPeriode.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Au:";
            // 
            // dateTimePickerDu
            // 
            this.dateTimePickerDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDu.Location = new System.Drawing.Point(99, 6);
            this.dateTimePickerDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerDu.Name = "dateTimePickerDu";
            this.dateTimePickerDu.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerDu.TabIndex = 0;
            // 
            // dateTimePickerAu
            // 
            this.dateTimePickerAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerAu.Location = new System.Drawing.Point(99, 37);
            this.dateTimePickerAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerAu.Name = "dateTimePickerAu";
            this.dateTimePickerAu.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerAu.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Du:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(257, 300);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(173, 30);
            this.button1.TabIndex = 31;
            this.button1.Text = "Générer le Rapport";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButtonMon
            // 
            this.radioButtonMon.AutoSize = true;
            this.radioButtonMon.Checked = true;
            this.radioButtonMon.Location = new System.Drawing.Point(29, 94);
            this.radioButtonMon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonMon.Name = "radioButtonMon";
            this.radioButtonMon.Size = new System.Drawing.Size(97, 21);
            this.radioButtonMon.TabIndex = 33;
            this.radioButtonMon.TabStop = true;
            this.radioButtonMon.Text = "Mensuelle:";
            this.radioButtonMon.UseVisualStyleBackColor = true;
            this.radioButtonMon.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(20, 302);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.MarqueeAnimationSpeed = 20;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(219, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 34;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimePickerMonth);
            this.groupBox1.Controls.Add(this.radioButtonPeriode);
            this.groupBox1.Controls.Add(this.panelPeriode);
            this.groupBox1.Controls.Add(this.radioButtonMon);
            this.groupBox1.Controls.Add(this.dateTimePickerJourn);
            this.groupBox1.Controls.Add(this.radioButtonDate);
            this.groupBox1.Location = new System.Drawing.Point(20, 25);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(411, 263);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selection de dates / période";
            // 
            // dateTimePickerMonth
            // 
            this.dateTimePickerMonth.CustomFormat = "MMMM yyyy";
            this.dateTimePickerMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMonth.Location = new System.Drawing.Point(176, 94);
            this.dateTimePickerMonth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerMonth.Name = "dateTimePickerMonth";
            this.dateTimePickerMonth.ShowUpDown = true;
            this.dateTimePickerMonth.Size = new System.Drawing.Size(161, 22);
            this.dateTimePickerMonth.TabIndex = 34;
            // 
            // radioButtonDate
            // 
            this.radioButtonDate.AutoSize = true;
            this.radioButtonDate.Location = new System.Drawing.Point(29, 52);
            this.radioButtonDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioButtonDate.Name = "radioButtonDate";
            this.radioButtonDate.Size = new System.Drawing.Size(104, 21);
            this.radioButtonDate.TabIndex = 29;
            this.radioButtonDate.Text = "Journalière:";
            this.radioButtonDate.UseVisualStyleBackColor = true;
            this.radioButtonDate.Visible = false;
            this.radioButtonDate.CheckedChanged += new System.EventHandler(this.radioButtonDate_CheckedChanged);
            // 
            // bgw
            // 
            this.bgw.WorkerReportsProgress = true;
            this.bgw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.bgw.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgw_ProgressChanged);
            this.bgw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_RunWorkerCompleted);
            // 
            // FrSelectMonth
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 341);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrSelectMonth";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rapport d\'Activité";
            this.panelPeriode.ResumeLayout(false);
            this.panelPeriode.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerJourn;
        private System.Windows.Forms.RadioButton radioButtonPeriode;
        private System.Windows.Forms.Panel panelPeriode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePickerDu;
        private System.Windows.Forms.DateTimePicker dateTimePickerAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButtonMon;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerMonth;
        private System.Windows.Forms.RadioButton radioButtonDate;
        private System.ComponentModel.BackgroundWorker bgw;
    }
}