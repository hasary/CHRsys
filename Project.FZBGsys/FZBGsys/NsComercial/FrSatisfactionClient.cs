﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrSatisfactionClient : Form
    {
        ModelEntities db = new ModelEntities();
        public FrSatisfactionClient(int? IDClientSatisfaction = null)
        {
            InitializeComponent();


            InitialiseWeeks();
            ListClientSatisfactions = new List<ClientSatisfaction>();

            if (IDClientSatisfaction != null)
            {
                this.ClientSatisfatction = db.ClientSatisfactions.Single(p => p.ID == IDClientSatisfaction);
                InputClientSatisfaction();
            }

            btAutoSat.Visible = Tools.CurrentUserID == 1; 

        }

        private void InputClientSatisfaction()
        {
            dtYear.Value = ClientSatisfatction.From.Value;
            var client = db.Clients.Single(p => p.ID == ClientSatisfatction.ClientID);
            cbClient.SelectedItem = client;
            cbWeek.SelectedIndex = ClientSatisfatction.WeekNo.Value - 1;

            raGout0.Checked = ClientSatisfatction.CritereGout.Value == 0;
            raGout1.Checked = ClientSatisfatction.CritereGout.Value == 1;
            raGout2.Checked = ClientSatisfatction.CritereGout.Value == 2;

            raVariete0.Checked = ClientSatisfatction.CritereVariete.Value == 0;
            raVariete1.Checked = ClientSatisfatction.CritereVariete.Value == 1;
            raVariete2.Checked = ClientSatisfatction.CritereVariete.Value == 2;

            raEmbalage0.Checked = ClientSatisfatction.CritereEmballage.Value == 0;
            raEmbalage1.Checked = ClientSatisfatction.CritereEmballage.Value == 1;
            raEmbalage2.Checked = ClientSatisfatction.CritereEmballage.Value == 2;


            raDisponibilite0.Checked = ClientSatisfatction.CritereDisponibilite.Value == 0;
            raDisponibilite1.Checked = ClientSatisfatction.CritereDisponibilite.Value == 1;
            raDisponibilite2.Checked = ClientSatisfatction.CritereDisponibilite.Value == 2;

            raRespectDelais0.Checked = ClientSatisfatction.CritereRespectDelai.Value == 0;
            raRespectDelais1.Checked = ClientSatisfatction.CritereRespectDelai.Value == 1;
            raRespectDelais2.Checked = ClientSatisfatction.CritereRespectDelai.Value == 2;


            raRespectCommande0.Checked = ClientSatisfatction.CritereRespectCommande.Value == 0;
            raRespectCommande1.Checked = ClientSatisfatction.CritereRespectCommande.Value == 1;
            raRespectCommande2.Checked = ClientSatisfatction.CritereRespectCommande.Value == 2;


            raRelationEntreprise0.Checked = ClientSatisfatction.CritereRelationAvecEntre.Value == 0;
            raRelationEntreprise1.Checked = ClientSatisfatction.CritereRelationAvecEntre.Value == 1;
            raRelationEntreprise2.Checked = ClientSatisfatction.CritereRelationAvecEntre.Value == 2;


            raRappotQP0.Checked = ClientSatisfatction.CriterePrix.Value == 0;
            raRappotQP1.Checked = ClientSatisfatction.CriterePrix.Value == 1;
            raRappotQP2.Checked = ClientSatisfatction.CriterePrix.Value == 2;

            raProximite0.Checked = ClientSatisfatction.CritereProximite.Value == 0;
            raProximite1.Checked = ClientSatisfatction.CritereProximite.Value == 1;
            raProximite2.Checked = ClientSatisfatction.CritereProximite.Value == 2;



        }



        private void label7_Click(object sender, EventArgs e)
        {

        }

        public DateTime From { get; set; }
        public DateTime To { get; set; }


        private void button1_Click(object sender, EventArgs e)
        {
            if (cbClient.SelectedItem != null)
            {
                //  new FrListArrivage(((Fournisseur)cbClient.SelectedItem).ID, From, To).ShowDialog();
            }
        }

        private void dateTimePickerMonth_ValueChanged(object sender, EventArgs e)
        {
            //    InitialiseDates();
        }



        private void tbQualite_Scroll(object sender, EventArgs e)
        {
            //   labEvalQuantite.Text = tbQualite.Value.ToString();
            //   updateTotal();
        }

        private void updateTotal()
        {
            // labTotal.Text =
            //     (labEvalDelai.Text.ParseToInt() +
            //   labEvalQuantite.Text.ParseToInt() +
            //   labEvalPrix.Text.ParseToInt() +
            //    labEvalModalite.Text.ParseToInt()).ToString();
        }

        private void tbDelai_Scroll(object sender, EventArgs e)
        {
            //   labEvalDelai.Text = tbDelai.Value.ToString();
            updateTotal();
        }

        private void tbPrix_Scroll(object sender, EventArgs e)
        {
            // labEvalPrix.Text = tbPrix.Value.ToString();
            updateTotal();
        }

        private void tbModalite_Scroll(object sender, EventArgs e)
        {
            // labEvalModalite.Text = tbModalite.Value.ToString();
            updateTotal();
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isvalidAll())
            {
                return;
            }

            this.ClientSatisfatction = new ClientSatisfaction();
            ClientSatisfatction.Client = (Client)cbClient.SelectedItem;
            ClientSatisfatction.From = From;
            ClientSatisfatction.To = To;
            ClientSatisfatction.WeekNo = cbWeek.SelectedIndex;


            ClientSatisfatction.CritereGout = (raGout0.Checked) ? 0 : ((raGout1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereVariete = (raVariete0.Checked) ? 0 : ((raVariete1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereEmballage = (raEmbalage0.Checked) ? 0 : ((raEmbalage1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereDisponibilite = (raDisponibilite0.Checked) ? 0 : ((raDisponibilite1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereRespectDelai = (raRespectDelais0.Checked) ? 0 : ((raRespectDelais1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereRespectCommande = (raRespectCommande0.Checked) ? 0 : ((raRespectCommande1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereRelationAvecEntre = (raRelationEntreprise0.Checked) ? 0 : ((raRelationEntreprise0.Checked) ? 1 : 2);
            ClientSatisfatction.CriterePrix = (raRappotQP0.Checked) ? 0 : ((raRappotQP1.Checked) ? 1 : 2);
            ClientSatisfatction.CritereProximite = (raProximite0.Checked) ? 0 : ((raProximite1.Checked) ? 1 : 2);
            ClientSatisfatction.Observation = txtObsevation.Text.Trim();

            ListClientSatisfactions.Add(ClientSatisfatction);
            db.AddToClientSatisfactions(ClientSatisfatction);
            db.SaveChanges();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, ClientSatisfatction);

            ClearControls();
            UpdateContorls();
            RefreshGrid();

        }
        private bool isvalidAll()
        {
            string ErrorMessage = "";

            if (cbClient.SelectedIndex < 1)
            {
                ErrorMessage += "Selectionnez un client!";
            }

            if (
                !raGout0.Checked && !raGout1.Checked && !raGout2.Checked ||
                !raVariete0.Checked && !raVariete1.Checked && !raVariete2.Checked ||
                !raEmbalage0.Checked && !raEmbalage1.Checked && !raEmbalage2.Checked ||
                !raDisponibilite0.Checked && !raDisponibilite1.Checked && !raDisponibilite2.Checked ||
                !raRespectDelais0.Checked && !raRespectDelais1.Checked && !raRespectDelais2.Checked ||
                !raRespectCommande0.Checked && !raRespectCommande1.Checked && !raRespectCommande2.Checked ||
                !raRelationEntreprise0.Checked && !raRelationEntreprise1.Checked && !raRelationEntreprise2.Checked ||
                !raRappotQP0.Checked && !raRappotQP1.Checked && !raRappotQP2.Checked ||
                !raProximite0.Checked && !raProximite1.Checked && !raProximite2.Checked


                )
            {
                ErrorMessage += "\nInformations Satisfaction manquantes, vérifiez!";

            }

            if (ErrorMessage != "")
            {
                Tools.ShowError(ErrorMessage);
                return false;
            }

            return true;
        }


        private void ClearControls()
        {
            cbClient.SelectedIndex = 0;
            ClientSatisfatction = null;

            raGout0.Checked = false;
            raVariete0.Checked = false;
            raEmbalage0.Checked = false;
            raDisponibilite0.Checked = false;
            raRespectDelais0.Checked = false;
            raRespectCommande0.Checked = false;
            raRelationEntreprise0.Checked = false;
            raRappotQP0.Checked = false;
            raProximite0.Checked = false;

            raGout1.Checked = false;
            raVariete1.Checked = false;
            raEmbalage1.Checked = false;
            raDisponibilite1.Checked = false;
            raRespectDelais1.Checked = false;
            raRespectCommande1.Checked = false;
            raRelationEntreprise1.Checked = false;
            raRappotQP1.Checked = false;
            raProximite1.Checked = false;

            raGout2.Checked = false;
            raVariete2.Checked = false;
            raEmbalage2.Checked = false;
            raDisponibilite2.Checked = false;
            raRespectDelais2.Checked = false;
            raRespectCommande2.Checked = false;
            raRelationEntreprise2.Checked = false;
            raRappotQP2.Checked = false;
            raProximite2.Checked = false;

            //    tbDelai.Value = tbModalite.Value = tbPrix.Value = tbQualite.Value = 0;
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListClientSatisfactions.Select(p => new
            {
                ID = p.ID,
                //Periode = p.From.Value.ToString("dd MMM") + " au " + p.To.Value.ToString("dd MMM"),
                Client = p.Client.Nom,
                Gout = p.CritereGout.ToSatisfactionTxt(),
                Variete = p.CritereVariete.ToSatisfactionTxt(),
                Emballage = p.CritereEmballage.ToSatisfactionTxt(),
                Disponibilite = p.CritereDisponibilite.ToSatisfactionTxt(),
                RespectDelais = p.CritereRespectDelai.ToSatisfactionTxt(),
                RespectCommande = p.CritereRespectCommande.ToSatisfactionTxt(),
                RelationEntreprise = p.CritereRelationAvecEntre.ToSatisfactionTxt(),
                Prix = p.CriterePrix.ToSatisfactionTxt(),
                Proximite = p.CritereProximite.ToSatisfactionTxt()

            }).ToList();
            dgv.Columns["ID"].Visible = false;

        }

        public List<ClientSatisfaction> ListClientSatisfactions { get; set; }
        public ClientSatisfaction ClientSatisfatction { get; set; }

        private void FrEvaluationFournisseur_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selected = dgv.GetSelectedID();
            this.ClientSatisfatction = db.ClientSatisfactions.Single(p => p.ID == selected);

            ListClientSatisfactions.Remove(this.ClientSatisfatction);
            db.DeleteObject(ClientSatisfatction);
            db.SaveChanges();
            //  db.Refresh(System.Data.Objects.RefreshMode.ClientWins, FournisseurEval);
            UpdateContorls();
            InputClientSatisfaction();
            RefreshGrid();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            db.SaveChanges();
            Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
            return;
           



        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dtYear_ValueChanged(object sender, EventArgs e)
        {
            InitialiseWeeks();
        }

        /*private void InitialiseWeeks()
        {

            var datebegin = dtYear.Value.Year.ToString() + "-01-01";
            var dateend = dtYear.Value.Year.ToString() + "-12-31";
            DateTime date = DateTime.Parse(datebegin);
            this.weeks = new List<DateTime>();
            int i = 0;
            while (date.CompareTo(DateTime.Parse(dateend)) < 0)
            {
                i++;
                var enddate = date.AddDays(6);
                var ventes = db.BonLivraisons.FirstOrDefault(p => p.DateLivraison >= date && p.DateLivraison <= enddate);
                if (ventes != null)
                {
                    cbWeek.Items.Add("S-" + (i).ToString().PadLeft(2, '0') + " :    " + date.ToString("dd  MMM") + "  au  " + date.AddDays(6).ToString("dd  MMM"));
                }
                weeks.Add(date);
                date = date.AddDays(7);
            }*/
        private void InitialiseWeeks()
        {

            var datebegin = dtYear.Value.Year.ToString() + "-01-01";
            var dateend = dtYear.Value.Year.ToString() + "-12-31";
            DateTime date = DateTime.Parse(datebegin);
            this.weeks = new List<DateTime>();
            int i = 0;
            while (date.CompareTo(DateTime.Parse(dateend)) < 0)
            {
                i++;
                var enddate = date.AddMonths(3).AddDays(-1);
                var ventes = db.BonLivraisons.FirstOrDefault(p => p.DateLivraison >= date && p.DateLivraison <= enddate);
                if (ventes != null)
                {
                    cbWeek.Items.Add("S-" + (i).ToString().PadLeft(2, '0') + " :    " + date.ToString("dd  MMM") + "  au  " + date.AddMonths(3).AddDays(-1).ToString("dd  MMM"));
                }
                weeks.Add(date);
                date = date.AddMonths(3);
            }
            /*
                        string query = "EXEC [dbo].[fnListofWeeks] @DateFrom= '" + datebegin + "' , @DateTo= '" + dateend + "' ";
                        this.result = new DataTable();
                        using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(@"Server=" + Tools.SQL_Server + ";DataBase=" + Tools.SQL_DataBase + ";User ID=" + Tools.SQL_UserName + ";password=" + Tools.SQL_Password + ""))
                        {
                            conn.Open();
                            using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(query, conn))
                            {

                                using (System.Data.SqlClient.SqlDataReader dr = cmd.ExecuteReader())
                                {
                                    result.Load(dr);
                                }
                            }
                        }

                        for (int i = 0; i < result.Rows.Count; i++)
                        {
                            var debut = DateTime.Parse(result.Rows[i][0].ToString());
                            var fin = DateTime.Parse(result.Rows[i][1].ToString()).AddDays(1);
                
                        }

                        */




        }

        public int WeekNumber(DateTime date)
        {
            return 1 + date.DayOfYear / 7;
        }

        private void cbWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbClient.Items.Clear();
            if (weeks != null && weeks.Count != 0)
            {
                this.From = weeks.ElementAt(cbWeek.SelectedIndex);
                this.To = weeks.ElementAt(cbWeek.SelectedIndex).AddDays(6);
                this.ListClientSatisfactions.Clear();
                ListClientSatisfactions.AddRange(db.ClientSatisfactions.Where(p => p.From == this.From).ToList());
                RefreshGrid();
                UpdateContorls();


            }
            else
            {

            }
        }

        private void UpdateContorls()
        {
            List<Client> clients = db.BonLivraisons.Where(p => p.DateLivraison >= From && p.DateLivraison <= To && p.BonAttribution.Client.ClientTypeID == 1).Select(p => p.BonAttribution.Client).ToList();
            cbClient.Items.Clear();
            var tmp = ListClientSatisfactions.Select(p => p.Client).ToList();
            clients = clients.Distinct().ToList();
            foreach (var client in tmp)
            {
                if (clients.Contains(client)) clients.Remove(client);
            }


            cbClient.Items.Add(new Client { ID = 0, Nom = "" });
            cbClient.Items.AddRange(clients.ToArray());
            cbClient.DisplayMember = "Nom";
            cbClient.ValueMember = "ID";
            cbClient.DropDownStyle = ComboBoxStyle.DropDownList;
            cbClient.SelectedIndex = 0;
        }



        //public DataTable result { get; set; }

        private void cbClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbClient.SelectedIndex > 0)
            {
                labNumTel.Text = ((Client)cbClient.SelectedItem).Telephone;
            }
            else
            {
                labNumTel.Text = "";
            }
        }

        public List<DateTime> weeks { get; set; }

        private void btAutoSat_Click(object sender, EventArgs e)
        /*{
            var dateBegin = DateTime.Parse("2015-01-01"); // put date begin heare
          
            var dateEnd = DateTime.Now;
            Random r = new Random();

            DateTime date = dateBegin;
            //this.weeks = new List<DateTime>();
            int i = 0;
            while (date.CompareTo(dateEnd) < 0)
            {
                i++;
                var enddate = date.AddDays(6);
                var ventes = db.BonLivraisons.Where(p => p.DateLivraison >= date && p.DateLivraison <= enddate && p.BonAttribution.Client.ClientTypeID == 1).ToList();
                if (ventes.Count != 0)
                {
                    var clientIDs = ventes.Select(p => p.BonAttribution.ClientID).ToList().Distinct().ToList();

                    foreach (var ClientID in clientIDs)
                    {
                        db.ClientSatisfactions.AddObject(new ClientSatisfaction()
                        {
                            ClientID             = ClientID.Value,
                            CritereDisponibilite = r.Next(10000) % 2,
                            CritereGout = r.Next(10000) % 2 + 1,
                            CriterePrix = r.Next(10000) % 2 + 1,
                            CritereEmballage = r.Next(10000) % 3,
                            CritereProximite = r.Next(10000) % 3,
                            CritereRelationAvecEntre = r.Next(10000) % 3,
                            CritereRespectCommande   = r.Next(10000) % 2 + 1,
                            CritereRespectDelai = r.Next(10000) % 3,
                            CritereVariete      = r.Next(10000) % 2 + 1,
                            Observation = "",
                            From = date,
                            To = enddate,
                            WeekNo = i,

                        });
                    }
                    //cbWeek.Items.Add("S-" + (i).ToString().PadLeft(2, '0') + " :    " + date.ToString("dd  MMM") + "  au  " + date.AddDays(6).ToString("dd  MMM"));
                }
                //weeks.Add(date);
                date = date.AddDays(7);
            }

            db.SaveChanges();
            Tools.ShowError("done");
            //   return;
        }*/
        {
            var dateBegin = DateTime.Parse("2015-01-01"); // put date begin heare

            var dateEnd = DateTime.Now;
            Random r = new Random();

            DateTime date = dateBegin;
            //this.weeks = new List<DateTime>();
            int i = 0;
            while (date.CompareTo(dateEnd) < 0)
            {
                i++;
                var enddate = date.AddMonths(3).AddDays(-1);
                var ventes = db.BonLivraisons.Where(p => p.DateLivraison >= date && p.DateLivraison <= enddate && p.BonAttribution.Client.ClientTypeID == 1).ToList();
                if (ventes.Count != 0)
                {
                    var clientIDs = ventes.Select(p => p.BonAttribution.ClientID).ToList().Distinct().ToList();

                    foreach (var ClientID in clientIDs)
                    {
                        db.ClientSatisfactions.AddObject(new ClientSatisfaction()
                        {
                            ClientID = ClientID.Value,
                            CritereDisponibilite = r.Next(10000) % 2,
                            CritereGout = r.Next(10000) % 2 + 1,
                            CriterePrix = r.Next(10000) % 2 + 1,
                            CritereEmballage = r.Next(10000) % 3,
                            CritereProximite = r.Next(10000) % 3,
                            CritereRelationAvecEntre = r.Next(10000) % 3,
                            CritereRespectCommande = r.Next(10000) % 2 + 1,
                            CritereRespectDelai = r.Next(10000) % 3,
                            CritereVariete = r.Next(10000) % 2 + 1,
                            Observation = "",
                            From = date,
                            To = enddate,
                            WeekNo = i,

                        });
                    }
                    //cbWeek.Items.Add("S-" + (i).ToString().PadLeft(2, '0') + " :    " + date.ToString("dd  MMM") + "  au  " + date.AddDays(6).ToString("dd  MMM"));
                }
                //weeks.Add(date);
                date = date.AddMonths(3);
            }

            db.SaveChanges();
            Tools.ShowError("done");
            //   return;
        }
    }
}
