﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrSelectDateClient : Form
    {
        ModelEntities db = new ModelEntities();
        public FrSelectDateClient()
        {
            InitializeComponent();
            InitialiseDate();
            SelectionDone = false;

            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {
            string[] monthText = { "Ja", "" };
            Dictionary<string, string> months = new Dictionary<string, string>();

            DateTime fin = DateTime.Now;
            DateTime debut = DateTime.Parse("2012-05-01");

            for (DateTime i = debut; i < fin; i = i.AddMonths(1))
            {
                // months.Add(i.Month,
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerMonth.Visible = radioButtonMon.Checked;
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = radioButtonPeriode.Checked;
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime From { get; set; }
        public static DateTime To { get; set; }
        public static bool SelectionDone = false;
        private void button1_Click(object sender, EventArgs e)
        {

            if (Client == null)
            {
                this.ShowWarning("Selectionnez un client!");
                return;
            }
            SelectionDone = true;
            button1.Enabled = false;
            DateTime du = DateTime.Now;
            DateTime au = DateTime.Now;

            if (radioButtonDate.Checked)
            {
                du = au = dateTimePickerJourn.Value;

            }

            if (radioButtonPeriode.Checked)
            {
                du = dateTimePickerDu.Value;
                au = dateTimePickerAu.Value;
            }
            if (radioButtonMon.Checked)
            {
                var dt = dateTimePickerMonth.Value;
                au = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                du = new DateTime(dt.Year, dt.Month, 1);
            }

            //   Actions.Rapports.FrViewer.PrintRecapShowroom(du, au);
            From = du;
            if (From < DateTime.Parse("2012-07-24"))
            {
                From = DateTime.Parse("2012-07-24");
            }
            To = au;
            //    progressBar1.Visible = false;

            decimal? soldeBugin = 0;
            var solde23 = db.Soldes23072012.SingleOrDefault(p => p.code == Client.CodeSage);
            if (solde23 != null)
            {
                soldeBugin = solde23.credit.Replace('.', ',').ParseToDec() - solde23.debit.Replace('.', ',').ParseToDec();
            }
            else
            {
                soldeBugin = Client.SoldeHistory;
            }
            var dateHi = Client.DateHistory;
            if (dateHi != null)
            {
                soldeBugin -= Client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré && p.Date > dateHi && p.Date < From).Sum(p => p.MontantTTC);
                soldeBugin += Client.FactureRetours.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonRetour.Etat != (int)EnEtatBonAttribution.Annulé && p.Date > dateHi && p.Date < From).Sum(p => p.MontantTTC);
                soldeBugin -= Client.Operations.Where(p => p.Sen == "D" && p.Date > dateHi && p.Date < From && p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client).Sum(p => p.Montant);
                soldeBugin += Client.Operations.Where(p => p.Sen == "C" && p.Date > dateHi && p.Date < From && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client).Sum(p => p.Montant);
            }

            var bonNonLivre = Client.BonAttributions.Where(p => p.Etat != (int)EnEtatBonAttribution.Annulé && p.Etat != (int)EnEtatBonAttribution.Livré && p.Date > dateHi && p.Date <= To).ToList();
            if (bonNonLivre.Count() != 0)
            {
                Tools.ShowInformation("Ce client possede des bon en instance non livré");
                return;
            }
            else

                FZBGsys.NsReports.Comercial.ComercialReportController.PrintReleverClient(Client, soldeBugin, From, To);

            // Dispose();
            button1.Enabled = true;

        }




        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {

            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (Client != null)
            {
                txtClient.Text = Client.Nom;
            }

        }

        public Client Client { get; set; }

        private void FrSelectDateClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }
    }
}
