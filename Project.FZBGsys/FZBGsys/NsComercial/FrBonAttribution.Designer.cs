﻿namespace FZBGsys.NsComercial
{
    partial class FrBonAttribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btParcourrirClient = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panSolde = new System.Windows.Forms.Panel();
            this.labVersementDA = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labChargementDA = new System.Windows.Forms.Label();
            this.labChargement = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labVersement = new System.Windows.Forms.Label();
            this.txtSolde = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtParent = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.LabEtatCompte = new System.Windows.Forms.Label();
            this.cbMode = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbGoutProduit = new System.Windows.Forms.ComboBox();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotalBouteilles = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.radFardeaux = new System.Windows.Forms.RadioButton();
            this.radPalettes = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            this.chSuivre = new System.Windows.Forms.CheckBox();
            this.panelComercial = new System.Windows.Forms.Panel();
            this.ChSuperGrossist = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panSolde.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panelComercial.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.Location = new System.Drawing.Point(225, 16);
            this.dgv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(496, 292);
            this.dgv.TabIndex = 9;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(526, 348);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(202, 49);
            this.xpannel1.TabIndex = 12;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(4, 10);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(92, 27);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(100, 10);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(95, 27);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Suivant >>";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(116, 200);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(100, 25);
            this.btAjouter.TabIndex = 20;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(225, 365);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(83, 25);
            this.btEnlever.TabIndex = 21;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btParcourrirClient
            // 
            this.btParcourrirClient.Location = new System.Drawing.Point(22, 17);
            this.btParcourrirClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btParcourrirClient.Name = "btParcourrirClient";
            this.btParcourrirClient.Size = new System.Drawing.Size(33, 20);
            this.btParcourrirClient.TabIndex = 22;
            this.btParcourrirClient.Text = "...";
            this.btParcourrirClient.UseVisualStyleBackColor = true;
            this.btParcourrirClient.Click += new System.EventHandler(this.btParcourrirClient_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panSolde);
            this.groupBox1.Controls.Add(this.txtParent);
            this.groupBox1.Controls.Add(this.txtClient);
            this.groupBox1.Controls.Add(this.btParcourrirClient);
            this.groupBox1.Location = new System.Drawing.Point(6, 39);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(212, 122);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Client";
            // 
            // panSolde
            // 
            this.panSolde.Controls.Add(this.labVersementDA);
            this.panSolde.Controls.Add(this.label10);
            this.panSolde.Controls.Add(this.labChargementDA);
            this.panSolde.Controls.Add(this.labChargement);
            this.panSolde.Controls.Add(this.label9);
            this.panSolde.Controls.Add(this.labVersement);
            this.panSolde.Controls.Add(this.txtSolde);
            this.panSolde.Controls.Add(this.label7);
            this.panSolde.Location = new System.Drawing.Point(6, 62);
            this.panSolde.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panSolde.Name = "panSolde";
            this.panSolde.Size = new System.Drawing.Size(203, 57);
            this.panSolde.TabIndex = 31;
            // 
            // labVersementDA
            // 
            this.labVersementDA.AutoSize = true;
            this.labVersementDA.Location = new System.Drawing.Point(118, 23);
            this.labVersementDA.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labVersementDA.Name = "labVersementDA";
            this.labVersementDA.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labVersementDA.Size = new System.Drawing.Size(13, 13);
            this.labVersementDA.TabIndex = 33;
            this.labVersementDA.Text = "--";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 24);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 32;
            this.label10.Text = "vers.";
            // 
            // labChargementDA
            // 
            this.labChargementDA.AutoSize = true;
            this.labChargementDA.Location = new System.Drawing.Point(118, 39);
            this.labChargementDA.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labChargementDA.Name = "labChargementDA";
            this.labChargementDA.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labChargementDA.Size = new System.Drawing.Size(13, 13);
            this.labChargementDA.TabIndex = 32;
            this.labChargementDA.Text = "--";
            // 
            // labChargement
            // 
            this.labChargement.AutoSize = true;
            this.labChargement.Location = new System.Drawing.Point(53, 40);
            this.labChargement.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labChargement.Name = "labChargement";
            this.labChargement.Size = new System.Drawing.Size(10, 13);
            this.labChargement.TabIndex = 26;
            this.labChargement.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 40);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 32;
            this.label9.Text = "charg.";
            // 
            // labVersement
            // 
            this.labVersement.AutoSize = true;
            this.labVersement.Location = new System.Drawing.Point(53, 24);
            this.labVersement.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labVersement.Name = "labVersement";
            this.labVersement.Size = new System.Drawing.Size(10, 13);
            this.labVersement.TabIndex = 26;
            this.labVersement.Text = "-";
            // 
            // txtSolde
            // 
            this.txtSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSolde.Location = new System.Drawing.Point(53, 3);
            this.txtSolde.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtSolde.Name = "txtSolde";
            this.txtSolde.ReadOnly = true;
            this.txtSolde.Size = new System.Drawing.Size(147, 19);
            this.txtSolde.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 5);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Solde:";
            // 
            // txtParent
            // 
            this.txtParent.Location = new System.Drawing.Point(59, 40);
            this.txtParent.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtParent.Name = "txtParent";
            this.txtParent.ReadOnly = true;
            this.txtParent.Size = new System.Drawing.Size(149, 20);
            this.txtParent.TabIndex = 23;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(59, 17);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(149, 20);
            this.txtClient.TabIndex = 23;
            this.txtClient.TextChanged += new System.EventHandler(this.txtClient_TextChanged);
            this.txtClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClient_KeyPress);
            this.txtClient.Leave += new System.EventHandler(this.txtClient_Leave);
            // 
            // LabEtatCompte
            // 
            this.LabEtatCompte.AutoSize = true;
            this.LabEtatCompte.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabEtatCompte.Location = new System.Drawing.Point(187, 49);
            this.LabEtatCompte.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabEtatCompte.Name = "LabEtatCompte";
            this.LabEtatCompte.Size = new System.Drawing.Size(11, 13);
            this.LabEtatCompte.TabIndex = 27;
            this.LabEtatCompte.Text = "-";
            this.LabEtatCompte.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.LabEtatCompte.Visible = false;
            // 
            // cbMode
            // 
            this.cbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMode.FormattingEnabled = true;
            this.cbMode.Items.AddRange(new object[] {
            "",
            "Espèces",
            "Avance",
            "Virement",
            "Chèque",
            "Traite",
            "à Terme"});
            this.cbMode.Location = new System.Drawing.Point(610, 327);
            this.cbMode.Name = "cbMode";
            this.cbMode.Size = new System.Drawing.Size(111, 21);
            this.cbMode.TabIndex = 25;
            this.cbMode.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(519, 330);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Mode Payement:";
            this.label8.Visible = false;
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(63, 18);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(151, 20);
            this.dtDate.TabIndex = 24;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Date:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LabEtatCompte);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbGoutProduit);
            this.groupBox2.Controls.Add(this.cbFormat);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(8, 2);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(204, 73);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Produit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Gôut:";
            // 
            // cbGoutProduit
            // 
            this.cbGoutProduit.FormattingEnabled = true;
            this.cbGoutProduit.Location = new System.Drawing.Point(62, 17);
            this.cbGoutProduit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbGoutProduit.Name = "cbGoutProduit";
            this.cbGoutProduit.Size = new System.Drawing.Size(136, 21);
            this.cbGoutProduit.TabIndex = 1;
            this.cbGoutProduit.SelectedIndexChanged += new System.EventHandler(this.cbGoutProduit_SelectedIndexChanged);
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(62, 43);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(90, 21);
            this.cbFormat.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 46);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Format:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtTotalBouteilles);
            this.groupBox3.Controls.Add(this.txtNombre);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.radFardeaux);
            this.groupBox3.Controls.Add(this.radPalettes);
            this.groupBox3.Location = new System.Drawing.Point(8, 80);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(206, 115);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Quantite";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1, 84);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Total Bouteilles:";
            // 
            // txtTotalBouteilles
            // 
            this.txtTotalBouteilles.Location = new System.Drawing.Point(104, 84);
            this.txtTotalBouteilles.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtTotalBouteilles.Name = "txtTotalBouteilles";
            this.txtTotalBouteilles.ReadOnly = true;
            this.txtTotalBouteilles.Size = new System.Drawing.Size(76, 20);
            this.txtTotalBouteilles.TabIndex = 1;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(104, 44);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(76, 20);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtNombre.Enter += new System.EventHandler(this.txtNombre_Enter);
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(102, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Nombre:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radFardeaux
            // 
            this.radFardeaux.AutoSize = true;
            this.radFardeaux.Location = new System.Drawing.Point(19, 46);
            this.radFardeaux.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radFardeaux.Name = "radFardeaux";
            this.radFardeaux.Size = new System.Drawing.Size(69, 17);
            this.radFardeaux.TabIndex = 0;
            this.radFardeaux.Text = "Fardeaux";
            this.radFardeaux.UseVisualStyleBackColor = true;
            this.radFardeaux.CheckedChanged += new System.EventHandler(this.radFardeaux_CheckedChanged);
            // 
            // radPalettes
            // 
            this.radPalettes.AutoSize = true;
            this.radPalettes.Checked = true;
            this.radPalettes.Location = new System.Drawing.Point(19, 24);
            this.radPalettes.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radPalettes.Name = "radPalettes";
            this.radPalettes.Size = new System.Drawing.Size(63, 17);
            this.radPalettes.TabIndex = 0;
            this.radPalettes.TabStop = true;
            this.radPalettes.Text = "Palettes";
            this.radPalettes.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(223, 327);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Total attribution:";
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(335, 333);
            this.labTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(0, 13);
            this.labTotal.TabIndex = 29;
            // 
            // chSuivre
            // 
            this.chSuivre.AutoSize = true;
            this.chSuivre.Checked = true;
            this.chSuivre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chSuivre.Location = new System.Drawing.Point(430, 370);
            this.chSuivre.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chSuivre.Name = "chSuivre";
            this.chSuivre.Size = new System.Drawing.Size(95, 17);
            this.chSuivre.TabIndex = 30;
            this.chSuivre.Text = "Suivre Facture";
            this.chSuivre.UseVisualStyleBackColor = true;
            this.chSuivre.Visible = false;
            // 
            // panelComercial
            // 
            this.panelComercial.Controls.Add(this.groupBox2);
            this.panelComercial.Controls.Add(this.btAjouter);
            this.panelComercial.Controls.Add(this.groupBox3);
            this.panelComercial.Location = new System.Drawing.Point(0, 165);
            this.panelComercial.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelComercial.Name = "panelComercial";
            this.panelComercial.Size = new System.Drawing.Size(220, 232);
            this.panelComercial.TabIndex = 31;
            // 
            // ChSuperGrossist
            // 
            this.ChSuperGrossist.AutoSize = true;
            this.ChSuperGrossist.Location = new System.Drawing.Point(321, 370);
            this.ChSuperGrossist.Margin = new System.Windows.Forms.Padding(2);
            this.ChSuperGrossist.Name = "ChSuperGrossist";
            this.ChSuperGrossist.Size = new System.Drawing.Size(100, 17);
            this.ChSuperGrossist.TabIndex = 30;
            this.ChSuperGrossist.Text = "Super Grossiste";
            this.ChSuperGrossist.UseVisualStyleBackColor = true;
            this.ChSuperGrossist.Visible = false;
            // 
            // FrBonAttribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(737, 400);
            this.Controls.Add(this.panelComercial);
            this.Controls.Add(this.cbMode);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ChSuperGrossist);
            this.Controls.Add(this.chSuivre);
            this.Controls.Add(this.labTotal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrBonAttribution";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bon d\'attribution";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrBonAttribution_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panSolde.ResumeLayout(false);
            this.panSolde.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panelComercial.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btParcourrirClient;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbGoutProduit;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.RadioButton radFardeaux;
        private System.Windows.Forms.RadioButton radPalettes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTotalBouteilles;
        private System.Windows.Forms.TextBox txtParent;
        private System.Windows.Forms.ComboBox cbMode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.CheckBox chSuivre;
        private System.Windows.Forms.Label LabEtatCompte;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSolde;
        private System.Windows.Forms.Panel panSolde;
        private System.Windows.Forms.Panel panelComercial;
        private System.Windows.Forms.Label labChargement;
        private System.Windows.Forms.Label labVersement;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labVersementDA;
        private System.Windows.Forms.Label labChargementDA;
        private System.Windows.Forms.CheckBox ChSuperGrossist;
    }
}