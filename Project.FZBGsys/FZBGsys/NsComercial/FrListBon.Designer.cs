﻿namespace FZBGsys.NsComercial
{
    partial class FrListBon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvAttribution = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.btRapport = new System.Windows.Forms.Button();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.btRecherche = new System.Windows.Forms.Button();
            this.btImprimer = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.btParcourrirClient = new System.Windows.Forms.Button();
            this.cbDocument = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gbEtat = new System.Windows.Forms.GroupBox();
            this.chCharge = new System.Windows.Forms.CheckBox();
            this.chAnnulé = new System.Windows.Forms.CheckBox();
            this.chLivré = new System.Windows.Forms.CheckBox();
            this.chEnInstance = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tcDocument = new System.Windows.Forms.TabControl();
            this.tpAttribution = new System.Windows.Forms.TabPage();
            this.tpFacture = new System.Windows.Forms.TabPage();
            this.dgvFacture = new System.Windows.Forms.DataGridView();
            this.tpSortie = new System.Windows.Forms.TabPage();
            this.dgvSortie = new System.Windows.Forms.DataGridView();
            this.tpLivraison = new System.Windows.Forms.TabPage();
            this.dgvLivraison = new System.Windows.Forms.DataGridView();
            this.tpRetour = new System.Windows.Forms.TabPage();
            this.dgvRetour = new System.Windows.Forms.DataGridView();
            this.tpFactureRetour = new System.Windows.Forms.TabPage();
            this.dgvFactureRetour = new System.Windows.Forms.DataGridView();
            this.btModifie = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.panEdit = new System.Windows.Forms.Panel();
            this.labTotalBon = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.afficherLeContenueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLeBonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLeBonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmBonSortie = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerBonDeSortieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmBonLivraison = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerBonLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLivraisonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmFacture = new System.Windows.Forms.ToolStripMenuItem();
            this.imprimerLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modifierLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annulerLaFactureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttribution)).BeginInit();
            this.panDate.SuspendLayout();
            this.panRechAvance.SuspendLayout();
            this.gbEtat.SuspendLayout();
            this.tcDocument.SuspendLayout();
            this.tpAttribution.SuspendLayout();
            this.tpFacture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFacture)).BeginInit();
            this.tpSortie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSortie)).BeginInit();
            this.tpLivraison.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLivraison)).BeginInit();
            this.tpRetour.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRetour)).BeginInit();
            this.tpFactureRetour.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactureRetour)).BeginInit();
            this.panEdit.SuspendLayout();
            this.cMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvAttribution
            // 
            this.dgvAttribution.AllowUserToAddRows = false;
            this.dgvAttribution.AllowUserToDeleteRows = false;
            this.dgvAttribution.AllowUserToResizeColumns = false;
            this.dgvAttribution.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvAttribution.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAttribution.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvAttribution.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAttribution.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvAttribution.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAttribution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAttribution.Location = new System.Drawing.Point(2, 2);
            this.dgvAttribution.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvAttribution.MultiSelect = false;
            this.dgvAttribution.Name = "dgvAttribution";
            this.dgvAttribution.ReadOnly = true;
            this.dgvAttribution.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvAttribution.RowHeadersVisible = false;
            this.dgvAttribution.RowTemplate.Height = 16;
            this.dgvAttribution.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAttribution.Size = new System.Drawing.Size(695, 366);
            this.dgvAttribution.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Date Création:";
            // 
            // btRapport
            // 
            this.btRapport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btRapport.Location = new System.Drawing.Point(814, 417);
            this.btRapport.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRapport.Name = "btRapport";
            this.btRapport.Size = new System.Drawing.Size(123, 28);
            this.btRapport.TabIndex = 57;
            this.btRapport.Text = "Rapport Journée";
            this.btRapport.UseVisualStyleBackColor = true;
            this.btRapport.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(12, 55);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(125, 17);
            this.chRechAvancee.TabIndex = 61;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(12, 417);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 63;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // btImprimer
            // 
            this.btImprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btImprimer.Location = new System.Drawing.Point(238, 420);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(82, 28);
            this.btImprimer.TabIndex = 58;
            this.btImprimer.Text = "Imprimer";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // dtDate
            // 
            this.dtDate.Enabled = false;
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(94, 22);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(96, 20);
            this.dtDate.TabIndex = 64;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 112);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date Création:";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(40, 131);
            this.panDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(160, 57);
            this.panDate.TabIndex = 24;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(47, 31);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(105, 20);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(47, 8);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(105, 20);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.btParcourrirClient);
            this.panRechAvance.Controls.Add(this.cbDocument);
            this.panRechAvance.Controls.Add(this.label7);
            this.panRechAvance.Controls.Add(this.gbEtat);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.txtNoBon);
            this.panRechAvance.Controls.Add(this.label3);
            this.panRechAvance.Controls.Add(this.panDate);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(12, 77);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(218, 274);
            this.panRechAvance.TabIndex = 62;
            // 
            // btParcourrirClient
            // 
            this.btParcourrirClient.Location = new System.Drawing.Point(183, 78);
            this.btParcourrirClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btParcourrirClient.Name = "btParcourrirClient";
            this.btParcourrirClient.Size = new System.Drawing.Size(26, 19);
            this.btParcourrirClient.TabIndex = 31;
            this.btParcourrirClient.Text = "...";
            this.btParcourrirClient.UseVisualStyleBackColor = true;
            this.btParcourrirClient.Click += new System.EventHandler(this.btParcourrirClient_Click);
            // 
            // cbDocument
            // 
            this.cbDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocument.FormattingEnabled = true;
            this.cbDocument.Items.AddRange(new object[] {
            "[Tout]",
            "Attribution",
            "Sortie",
            "Livraison",
            "Facture",
            "BonRetour",
            "FactureRetour"});
            this.cbDocument.Location = new System.Drawing.Point(62, 16);
            this.cbDocument.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbDocument.Name = "cbDocument";
            this.cbDocument.Size = new System.Drawing.Size(116, 21);
            this.cbDocument.TabIndex = 30;
            this.cbDocument.SelectedIndexChanged += new System.EventHandler(this.cbDocument_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 19);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Document:";
            // 
            // gbEtat
            // 
            this.gbEtat.Controls.Add(this.chCharge);
            this.gbEtat.Controls.Add(this.chAnnulé);
            this.gbEtat.Controls.Add(this.chLivré);
            this.gbEtat.Controls.Add(this.chEnInstance);
            this.gbEtat.Location = new System.Drawing.Point(40, 203);
            this.gbEtat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbEtat.Name = "gbEtat";
            this.gbEtat.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbEtat.Size = new System.Drawing.Size(163, 66);
            this.gbEtat.TabIndex = 28;
            this.gbEtat.TabStop = false;
            this.gbEtat.Text = "Etat";
            this.gbEtat.Visible = false;
            // 
            // chCharge
            // 
            this.chCharge.AutoSize = true;
            this.chCharge.Checked = true;
            this.chCharge.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chCharge.Location = new System.Drawing.Point(12, 37);
            this.chCharge.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chCharge.Name = "chCharge";
            this.chCharge.Size = new System.Drawing.Size(60, 17);
            this.chCharge.TabIndex = 1;
            this.chCharge.Text = "Chargé";
            this.chCharge.UseVisualStyleBackColor = true;
            // 
            // chAnnulé
            // 
            this.chAnnulé.AutoSize = true;
            this.chAnnulé.Location = new System.Drawing.Point(97, 37);
            this.chAnnulé.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chAnnulé.Name = "chAnnulé";
            this.chAnnulé.Size = new System.Drawing.Size(59, 17);
            this.chAnnulé.TabIndex = 0;
            this.chAnnulé.Text = "Annulé";
            this.chAnnulé.UseVisualStyleBackColor = true;
            // 
            // chLivré
            // 
            this.chLivré.AutoSize = true;
            this.chLivré.Checked = true;
            this.chLivré.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chLivré.Location = new System.Drawing.Point(97, 15);
            this.chLivré.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chLivré.Name = "chLivré";
            this.chLivré.Size = new System.Drawing.Size(49, 17);
            this.chLivré.TabIndex = 0;
            this.chLivré.Text = "Livré";
            this.chLivré.UseVisualStyleBackColor = true;
            // 
            // chEnInstance
            // 
            this.chEnInstance.AutoSize = true;
            this.chEnInstance.Checked = true;
            this.chEnInstance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEnInstance.Location = new System.Drawing.Point(12, 15);
            this.chEnInstance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chEnInstance.Name = "chEnInstance";
            this.chEnInstance.Size = new System.Drawing.Size(83, 17);
            this.chEnInstance.TabIndex = 0;
            this.chEnInstance.Text = "En Instance";
            this.chEnInstance.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 81);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Client:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(52, 79);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(126, 20);
            this.txtClient.TabIndex = 26;
            this.txtClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClient_KeyPress);
            // 
            // txtNoBon
            // 
            this.txtNoBon.Enabled = false;
            this.txtNoBon.Location = new System.Drawing.Point(90, 46);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(55, 20);
            this.txtNoBon.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 48);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "N°:";
            // 
            // tcDocument
            // 
            this.tcDocument.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcDocument.Controls.Add(this.tpAttribution);
            this.tcDocument.Controls.Add(this.tpFacture);
            this.tcDocument.Controls.Add(this.tpSortie);
            this.tcDocument.Controls.Add(this.tpLivraison);
            this.tcDocument.Controls.Add(this.tpRetour);
            this.tcDocument.Controls.Add(this.tpFactureRetour);
            this.tcDocument.Location = new System.Drawing.Point(235, 16);
            this.tcDocument.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tcDocument.Name = "tcDocument";
            this.tcDocument.SelectedIndex = 0;
            this.tcDocument.Size = new System.Drawing.Size(707, 396);
            this.tcDocument.TabIndex = 65;
            this.tcDocument.SelectedIndexChanged += new System.EventHandler(this.tcDocument_SelectedIndexChanged);
            // 
            // tpAttribution
            // 
            this.tpAttribution.Controls.Add(this.dgvAttribution);
            this.tpAttribution.Location = new System.Drawing.Point(4, 22);
            this.tpAttribution.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpAttribution.Name = "tpAttribution";
            this.tpAttribution.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpAttribution.Size = new System.Drawing.Size(699, 370);
            this.tpAttribution.TabIndex = 0;
            this.tpAttribution.Text = "Bon d\'Attribution";
            this.tpAttribution.UseVisualStyleBackColor = true;
            // 
            // tpFacture
            // 
            this.tpFacture.Controls.Add(this.dgvFacture);
            this.tpFacture.Location = new System.Drawing.Point(4, 22);
            this.tpFacture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpFacture.Name = "tpFacture";
            this.tpFacture.Size = new System.Drawing.Size(699, 370);
            this.tpFacture.TabIndex = 3;
            this.tpFacture.Text = "Payement";
            this.tpFacture.UseVisualStyleBackColor = true;
            // 
            // dgvFacture
            // 
            this.dgvFacture.AllowUserToAddRows = false;
            this.dgvFacture.AllowUserToDeleteRows = false;
            this.dgvFacture.AllowUserToResizeColumns = false;
            this.dgvFacture.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvFacture.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFacture.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFacture.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvFacture.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvFacture.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFacture.Location = new System.Drawing.Point(0, 0);
            this.dgvFacture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvFacture.MultiSelect = false;
            this.dgvFacture.Name = "dgvFacture";
            this.dgvFacture.ReadOnly = true;
            this.dgvFacture.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvFacture.RowHeadersVisible = false;
            this.dgvFacture.RowTemplate.Height = 16;
            this.dgvFacture.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFacture.Size = new System.Drawing.Size(699, 370);
            this.dgvFacture.TabIndex = 11;
            // 
            // tpSortie
            // 
            this.tpSortie.Controls.Add(this.dgvSortie);
            this.tpSortie.Location = new System.Drawing.Point(4, 22);
            this.tpSortie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpSortie.Name = "tpSortie";
            this.tpSortie.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpSortie.Size = new System.Drawing.Size(699, 370);
            this.tpSortie.TabIndex = 1;
            this.tpSortie.Text = "Bon de Sortie";
            this.tpSortie.UseVisualStyleBackColor = true;
            // 
            // dgvSortie
            // 
            this.dgvSortie.AllowUserToAddRows = false;
            this.dgvSortie.AllowUserToDeleteRows = false;
            this.dgvSortie.AllowUserToResizeColumns = false;
            this.dgvSortie.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSortie.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvSortie.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSortie.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvSortie.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvSortie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSortie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSortie.Location = new System.Drawing.Point(2, 2);
            this.dgvSortie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvSortie.MultiSelect = false;
            this.dgvSortie.Name = "dgvSortie";
            this.dgvSortie.ReadOnly = true;
            this.dgvSortie.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSortie.RowHeadersVisible = false;
            this.dgvSortie.RowTemplate.Height = 16;
            this.dgvSortie.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSortie.Size = new System.Drawing.Size(695, 366);
            this.dgvSortie.TabIndex = 11;
            // 
            // tpLivraison
            // 
            this.tpLivraison.Controls.Add(this.dgvLivraison);
            this.tpLivraison.Location = new System.Drawing.Point(4, 22);
            this.tpLivraison.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpLivraison.Name = "tpLivraison";
            this.tpLivraison.Size = new System.Drawing.Size(699, 370);
            this.tpLivraison.TabIndex = 2;
            this.tpLivraison.Text = "Bon de Livraison";
            this.tpLivraison.UseVisualStyleBackColor = true;
            // 
            // dgvLivraison
            // 
            this.dgvLivraison.AllowUserToAddRows = false;
            this.dgvLivraison.AllowUserToDeleteRows = false;
            this.dgvLivraison.AllowUserToResizeColumns = false;
            this.dgvLivraison.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvLivraison.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvLivraison.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvLivraison.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvLivraison.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvLivraison.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLivraison.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLivraison.Location = new System.Drawing.Point(0, 0);
            this.dgvLivraison.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvLivraison.MultiSelect = false;
            this.dgvLivraison.Name = "dgvLivraison";
            this.dgvLivraison.ReadOnly = true;
            this.dgvLivraison.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvLivraison.RowHeadersVisible = false;
            this.dgvLivraison.RowTemplate.Height = 16;
            this.dgvLivraison.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLivraison.Size = new System.Drawing.Size(699, 370);
            this.dgvLivraison.TabIndex = 11;
            // 
            // tpRetour
            // 
            this.tpRetour.Controls.Add(this.dgvRetour);
            this.tpRetour.Location = new System.Drawing.Point(4, 22);
            this.tpRetour.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpRetour.Name = "tpRetour";
            this.tpRetour.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpRetour.Size = new System.Drawing.Size(699, 370);
            this.tpRetour.TabIndex = 4;
            this.tpRetour.Text = "Bon de Retour";
            this.tpRetour.UseVisualStyleBackColor = true;
            // 
            // dgvRetour
            // 
            this.dgvRetour.AllowUserToAddRows = false;
            this.dgvRetour.AllowUserToDeleteRows = false;
            this.dgvRetour.AllowUserToResizeColumns = false;
            this.dgvRetour.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvRetour.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvRetour.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvRetour.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvRetour.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvRetour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRetour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRetour.Location = new System.Drawing.Point(2, 2);
            this.dgvRetour.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvRetour.MultiSelect = false;
            this.dgvRetour.Name = "dgvRetour";
            this.dgvRetour.ReadOnly = true;
            this.dgvRetour.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvRetour.RowHeadersVisible = false;
            this.dgvRetour.RowTemplate.Height = 16;
            this.dgvRetour.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRetour.Size = new System.Drawing.Size(695, 366);
            this.dgvRetour.TabIndex = 12;
            // 
            // tpFactureRetour
            // 
            this.tpFactureRetour.Controls.Add(this.dgvFactureRetour);
            this.tpFactureRetour.Location = new System.Drawing.Point(4, 22);
            this.tpFactureRetour.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpFactureRetour.Name = "tpFactureRetour";
            this.tpFactureRetour.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpFactureRetour.Size = new System.Drawing.Size(699, 370);
            this.tpFactureRetour.TabIndex = 5;
            this.tpFactureRetour.Text = "Facture de Retour";
            this.tpFactureRetour.UseVisualStyleBackColor = true;
            // 
            // dgvFactureRetour
            // 
            this.dgvFactureRetour.AllowUserToAddRows = false;
            this.dgvFactureRetour.AllowUserToDeleteRows = false;
            this.dgvFactureRetour.AllowUserToResizeColumns = false;
            this.dgvFactureRetour.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvFactureRetour.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvFactureRetour.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFactureRetour.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvFactureRetour.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvFactureRetour.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFactureRetour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFactureRetour.Location = new System.Drawing.Point(2, 2);
            this.dgvFactureRetour.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvFactureRetour.MultiSelect = false;
            this.dgvFactureRetour.Name = "dgvFactureRetour";
            this.dgvFactureRetour.ReadOnly = true;
            this.dgvFactureRetour.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvFactureRetour.RowHeadersVisible = false;
            this.dgvFactureRetour.RowTemplate.Height = 16;
            this.dgvFactureRetour.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFactureRetour.Size = new System.Drawing.Size(695, 366);
            this.dgvFactureRetour.TabIndex = 12;
            // 
            // btModifie
            // 
            this.btModifie.Location = new System.Drawing.Point(9, 6);
            this.btModifie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btModifie.Name = "btModifie";
            this.btModifie.Size = new System.Drawing.Size(77, 28);
            this.btModifie.TabIndex = 58;
            this.btModifie.Text = "Modifier ";
            this.btModifie.UseVisualStyleBackColor = true;
            this.btModifie.Click += new System.EventHandler(this.btModifieBon_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(91, 5);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(74, 28);
            this.btAnnuler.TabIndex = 58;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // panEdit
            // 
            this.panEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panEdit.Controls.Add(this.labTotalBon);
            this.panEdit.Controls.Add(this.label8);
            this.panEdit.Controls.Add(this.btAnnuler);
            this.panEdit.Controls.Add(this.btModifie);
            this.panEdit.Location = new System.Drawing.Point(326, 414);
            this.panEdit.Name = "panEdit";
            this.panEdit.Size = new System.Drawing.Size(345, 36);
            this.panEdit.TabIndex = 66;
            // 
            // labTotalBon
            // 
            this.labTotalBon.AutoSize = true;
            this.labTotalBon.Location = new System.Drawing.Point(236, 11);
            this.labTotalBon.Name = "labTotalBon";
            this.labTotalBon.Size = new System.Drawing.Size(0, 13);
            this.labTotalBon.TabIndex = 60;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(189, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 59;
            this.label8.Text = "Total:";
            // 
            // afficherLeContenueToolStripMenuItem
            // 
            this.afficherLeContenueToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.afficherLeContenueToolStripMenuItem.Name = "afficherLeContenueToolStripMenuItem";
            this.afficherLeContenueToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.afficherLeContenueToolStripMenuItem.Text = "Afficher le Contenu";
            // 
            // modifierLeBonToolStripMenuItem
            // 
            this.modifierLeBonToolStripMenuItem.Name = "modifierLeBonToolStripMenuItem";
            this.modifierLeBonToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.modifierLeBonToolStripMenuItem.Text = "Modifier le Bon";
            this.modifierLeBonToolStripMenuItem.Click += new System.EventHandler(this.modifierLeBonToolStripMenuItem_Click);
            // 
            // annulerLeBonToolStripMenuItem
            // 
            this.annulerLeBonToolStripMenuItem.Name = "annulerLeBonToolStripMenuItem";
            this.annulerLeBonToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.annulerLeBonToolStripMenuItem.Text = "Annuler le Bon";
            this.annulerLeBonToolStripMenuItem.Click += new System.EventHandler(this.annulerLeBonToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(180, 6);
            // 
            // tsmBonSortie
            // 
            this.tsmBonSortie.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerBonDeSortieToolStripMenuItem,
            this.modifierBonDeSortieToolStripMenuItem,
            this.annulerBonDeSortieToolStripMenuItem});
            this.tsmBonSortie.Name = "tsmBonSortie";
            this.tsmBonSortie.Size = new System.Drawing.Size(183, 22);
            this.tsmBonSortie.Text = "Bon de Sortie";
            // 
            // imprimerBonDeSortieToolStripMenuItem
            // 
            this.imprimerBonDeSortieToolStripMenuItem.Name = "imprimerBonDeSortieToolStripMenuItem";
            this.imprimerBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.imprimerBonDeSortieToolStripMenuItem.Text = "Imprimer Bon de Sortie";
            this.imprimerBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.imprimerBonDeSortieToolStripMenuItem_Click);
            // 
            // modifierBonDeSortieToolStripMenuItem
            // 
            this.modifierBonDeSortieToolStripMenuItem.Name = "modifierBonDeSortieToolStripMenuItem";
            this.modifierBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.modifierBonDeSortieToolStripMenuItem.Text = "Modifier Bon de Sortie";
            this.modifierBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.modifierBonDeSortieToolStripMenuItem_Click);
            // 
            // annulerBonDeSortieToolStripMenuItem
            // 
            this.annulerBonDeSortieToolStripMenuItem.Name = "annulerBonDeSortieToolStripMenuItem";
            this.annulerBonDeSortieToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.annulerBonDeSortieToolStripMenuItem.Text = "Annuler Bon de Sortie";
            this.annulerBonDeSortieToolStripMenuItem.Click += new System.EventHandler(this.annulerBonDeSortieToolStripMenuItem_Click);
            // 
            // tsmBonLivraison
            // 
            this.tsmBonLivraison.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerBonLivraisonToolStripMenuItem,
            this.modifierLivraisonToolStripMenuItem,
            this.annulerLivraisonToolStripMenuItem});
            this.tsmBonLivraison.Name = "tsmBonLivraison";
            this.tsmBonLivraison.Size = new System.Drawing.Size(183, 22);
            this.tsmBonLivraison.Text = "Bon de Livraison";
            // 
            // imprimerBonLivraisonToolStripMenuItem
            // 
            this.imprimerBonLivraisonToolStripMenuItem.Name = "imprimerBonLivraisonToolStripMenuItem";
            this.imprimerBonLivraisonToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.imprimerBonLivraisonToolStripMenuItem.Text = "Imprimer Bon Livraison";
            this.imprimerBonLivraisonToolStripMenuItem.Click += new System.EventHandler(this.imprimerBonLivraisonToolStripMenuItem_Click);
            // 
            // modifierLivraisonToolStripMenuItem
            // 
            this.modifierLivraisonToolStripMenuItem.Name = "modifierLivraisonToolStripMenuItem";
            this.modifierLivraisonToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.modifierLivraisonToolStripMenuItem.Text = "Modifier Livraison";
            this.modifierLivraisonToolStripMenuItem.Click += new System.EventHandler(this.modifierLivraisonToolStripMenuItem_Click);
            // 
            // annulerLivraisonToolStripMenuItem
            // 
            this.annulerLivraisonToolStripMenuItem.Name = "annulerLivraisonToolStripMenuItem";
            this.annulerLivraisonToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.annulerLivraisonToolStripMenuItem.Text = "Annuler Livraison";
            this.annulerLivraisonToolStripMenuItem.Click += new System.EventHandler(this.annulerLivraisonToolStripMenuItem_Click);
            // 
            // tsmFacture
            // 
            this.tsmFacture.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.imprimerLaFactureToolStripMenuItem,
            this.modifierLaFactureToolStripMenuItem,
            this.annulerLaFactureToolStripMenuItem});
            this.tsmFacture.Name = "tsmFacture";
            this.tsmFacture.Size = new System.Drawing.Size(183, 22);
            this.tsmFacture.Text = "Facture";
            // 
            // imprimerLaFactureToolStripMenuItem
            // 
            this.imprimerLaFactureToolStripMenuItem.Name = "imprimerLaFactureToolStripMenuItem";
            this.imprimerLaFactureToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.imprimerLaFactureToolStripMenuItem.Text = "Imprimer la Facture";
            this.imprimerLaFactureToolStripMenuItem.Click += new System.EventHandler(this.imprimerLaFactureToolStripMenuItem_Click);
            // 
            // modifierLaFactureToolStripMenuItem
            // 
            this.modifierLaFactureToolStripMenuItem.Name = "modifierLaFactureToolStripMenuItem";
            this.modifierLaFactureToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.modifierLaFactureToolStripMenuItem.Text = "Modifier la Facture";
            this.modifierLaFactureToolStripMenuItem.Click += new System.EventHandler(this.modifierLaFactureToolStripMenuItem_Click);
            // 
            // annulerLaFactureToolStripMenuItem
            // 
            this.annulerLaFactureToolStripMenuItem.Name = "annulerLaFactureToolStripMenuItem";
            this.annulerLaFactureToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.annulerLaFactureToolStripMenuItem.Text = "Annuler la Facture";
            this.annulerLaFactureToolStripMenuItem.Click += new System.EventHandler(this.annulerLaFactureToolStripMenuItem_Click);
            // 
            // cMenu
            // 
            this.cMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afficherLeContenueToolStripMenuItem,
            this.modifierLeBonToolStripMenuItem,
            this.annulerLeBonToolStripMenuItem,
            this.toolStripSeparator1,
            this.tsmBonSortie,
            this.tsmBonLivraison,
            this.tsmFacture});
            this.cMenu.Name = "cMenu";
            this.cMenu.Size = new System.Drawing.Size(184, 142);
            // 
            // FrListBon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 463);
            this.Controls.Add(this.tcDocument);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.panEdit);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btRapport);
            this.Controls.Add(this.btImprimer);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrListBon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liste des documents";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListBonAttribution_FormClosing);
            this.Shown += new System.EventHandler(this.FrListBon_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAttribution)).EndInit();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.gbEtat.ResumeLayout(false);
            this.gbEtat.PerformLayout();
            this.tcDocument.ResumeLayout(false);
            this.tpAttribution.ResumeLayout(false);
            this.tpFacture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFacture)).EndInit();
            this.tpSortie.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSortie)).EndInit();
            this.tpLivraison.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLivraison)).EndInit();
            this.tpRetour.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRetour)).EndInit();
            this.tpFactureRetour.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFactureRetour)).EndInit();
            this.panEdit.ResumeLayout(false);
            this.panEdit.PerformLayout();
            this.cMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAttribution;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btRapport;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.GroupBox gbEtat;
        private System.Windows.Forms.CheckBox chAnnulé;
        private System.Windows.Forms.CheckBox chLivré;
        private System.Windows.Forms.CheckBox chEnInstance;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chCharge;
        private System.Windows.Forms.ComboBox cbDocument;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tcDocument;
        private System.Windows.Forms.TabPage tpAttribution;
        private System.Windows.Forms.TabPage tpSortie;
        private System.Windows.Forms.TabPage tpLivraison;
        private System.Windows.Forms.TabPage tpFacture;
        private System.Windows.Forms.DataGridView dgvSortie;
        private System.Windows.Forms.DataGridView dgvLivraison;
        private System.Windows.Forms.DataGridView dgvFacture;
        private System.Windows.Forms.Button btModifie;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Panel panEdit;
        private System.Windows.Forms.ContextMenuStrip cMenu;
        private System.Windows.Forms.ToolStripMenuItem afficherLeContenueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLeBonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLeBonToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmBonSortie;
        private System.Windows.Forms.ToolStripMenuItem imprimerBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerBonDeSortieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmBonLivraison;
        private System.Windows.Forms.ToolStripMenuItem imprimerBonLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLivraisonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmFacture;
        private System.Windows.Forms.ToolStripMenuItem imprimerLaFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modifierLaFactureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annulerLaFactureToolStripMenuItem;
        private System.Windows.Forms.Label labTotalBon;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tpRetour;
        private System.Windows.Forms.DataGridView dgvRetour;
        private System.Windows.Forms.TabPage tpFactureRetour;
        private System.Windows.Forms.DataGridView dgvFactureRetour;
        private System.Windows.Forms.Button btParcourrirClient;
    }
}