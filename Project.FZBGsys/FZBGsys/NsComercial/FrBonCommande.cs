﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrBonCommande : Form
    {
        ModelEntities db = new ModelEntities();
        public FrBonCommande(int? BonID = null)
        {
            InitializeComponent();
            InitialiseControls();
            ListCommandeProduit = new List<CommandeProduit>();
            if (BonID != null)
            {
                BonCommande = db.BonCommandes.Single(p => p.ID == BonID);
                InputBon();
              //  chSuivre.Visible = false;
                btParcourrirClient.Enabled = false;
                txtClient.ReadOnly = true;

            }
        }



        private void InputBon()
        {
            dtDate.MinDate = BonCommande.Date.Value.AddDays(-1);
            dtDate.Value = BonCommande.Date.Value;
            Client = BonCommande.Client;
            LoadClient();
         /*   if (BonCommande.ModePayement != null)
            {
                cbMode.SelectedIndex = BonCommande.ModePayement.Value;
            }*/
            ListCommandeProduit.AddRange(BonCommande.CommandeProduits);
            RefreshGrid();
            UpdateControls();

        }

        private void InitialiseControls()
        {
           
            //--------------------------- Gout Produit
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbGoutProduit.SelectedIndex = 0;
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------------------------------------------------


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedIndex = 0;
            //--------------------------------------------------------------

            AutoCompleteStringCollection acsc = new AutoCompleteStringCollection();
            txtClient.AutoCompleteCustomSource = acsc;
            txtClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            txtClient.AutoCompleteSource = AutoCompleteSource.CustomSource;
            var results = db.Clients.ToList();

            if (results.Count > 0)
                foreach (var client in results)
                {
                    acsc.Add(client.Nom);
                }

         /*   dtDate.Value = Tools.CurrentDateTime.Value.Date.AddDays(1);
            if (dtDate.Value.DayOfWeek == DayOfWeek.Friday)
            {
                dtDate.Value = dtDate.Value.AddDays(1);    
            }*/
            //dtDate.MinDate = dtDate.Value;
            /*if (dtDate.MinDate.DayOfWeek == DayOfWeek.Friday)
            {
                dtDate.MinDate = dtDate.MinDate.AddDays(-1);
            }
            */
            //--------------------------------------------------------------

        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
         

            if (ListCommandeProduit.Count == 0)
            {
                this.ShowWarning("La liste est vide, ajouter d'abord des éléments");
                return;
            }
         



          //  var totalMontant = ListCommandeProduit.Sum(p => p.PrixUnitaire * p.TotalBouteilles);

         
            var currentTime = Tools.GetServerDateTime();

            if (BonCommande.EntityState == EntityState.Added)
            {
                BonCommande.InscriptionDate = Tools.GetServerDateTime();
                BonCommande.InscriptionUID = Tools.CurrentUserID;
            }
            else
            {
                BonCommande.Date = dtDate.Value;
                BonCommande.Client = this.Client;
             //  BonCommande.InscriptionUID = currentTime;

            }


            btEnregistrer.Enabled = false;
            db.SaveChangeTry();
            Dispose();
         

        }

        private void FrBonCommande_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("\nEtes vous sure de vouloir fermer sans enregistrer ?"))
            {
                Dispose();
            }
        }



        public BonCommande BonCommande { get; set; }
        private void txtClient_TextChanged(object sender, EventArgs e)
        {
            /*var txt = txtClient.Text.Trim();
            var clin = db.Clients.Where(p => p.Nom == txt);

            if (clin.Count() == 1)
            {
                this.Client = clin.First();
                LoadClient();
                if (BonCommande != null)
                {
                    BonCommande.ClientID = Client.ID;

                }

            }
            else if (clin.Count() > 1)
            {
                Tools.ShowError("Client en double, selectionnez depuis la liste.");
                Client = null;
                txtAdress.Text = "";

            }
            else
            {

                Client = null;
                txtAdress.Text = "";
            }
            */
        }

        private void LoadClient()
        {
            txtClient.ReadOnly = false;
            
            if (Client != null)
            {
                txtClient.Text = Client.Nom;
                labSde.Text = Client.Solde.ToString();
                //cbMode.SelectedIndex = Client.Adresse;
                if (Client.ClientParent != null)
                {
                    txtParent.Text = Client.ClientParent.Nom;
                }

              
                else
                {
                    //panSolde.Visible = false;
                    panelComercial.Enabled = true;
                }

                if (Client.IsCompteBloque.Value)
                {
                    this.ShowInformation("Le Compte de ce client est bloqué en raison de dépassement de crédit autorisé et ou le délet de reglement.");
                    panelComercial.Enabled = false;
                }
                else
                {
                    panelComercial.Enabled = true;
                }
            }
            else
            {

                //txtAdress.ReadOnly = true;
            }
        }

        public List<CommandeProduit> ListCommandeProduit { get; set; }
        public Client Client { get; set; }

        private void btParcourrirClient_Click(object sender, EventArgs e)
        {
            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (this.Client != null && this.Client.isExpire == true)
            {
                Tools.ShowError("Ce compte client est fermé (ancien dossier)");
                this.Client = null;
            }

            else if (this.Client != null)
            {/*
                var existCommande = Client.BonCommandes.Where(p => p.Etat != (int)EnEtatBonCommande.Livré && p.Etat != (int)EnEtatBonCommande.Annulé);
                if (existCommande.Count() != 0)
                {
                    Tools.ShowError("Vous ne pouvez pas inscrire un bon pour ce client car il pocède deja un bon en instance,\n veillez traiter l'ancein bon avant d'inscrir un nouveau!");
                    this.Client = null;
                }*/

                LoadClient();
            }

        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;

            }
            if (BonCommande == null)
            {
                CreateBon(); // 
            }

            var newCommandeProduit = new CommandeProduit();
            //  (int)EnUniteProduit.Fardeaux;
            newCommandeProduit.BonCommande = BonCommande;
            newCommandeProduit.Format = (Format)cbFormat.SelectedItem;
            newCommandeProduit.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;

            if (radFardeaux.Checked)
            {
                newCommandeProduit.Unite = (int)EnUniteProduit.Fardeaux;
                newCommandeProduit.NombreFardeaux = txtNombre.Text.ParseToInt();
                newCommandeProduit.TotalFardeaux = newCommandeProduit.NombreFardeaux;
                newCommandeProduit.TotalBouteille = newCommandeProduit.Format.BouteillesParFardeau * newCommandeProduit.NombreFardeaux;
            }
            else // palette
            {
                newCommandeProduit.Unite = (int)EnUniteProduit.Palettes;
                newCommandeProduit.NombrePalette = txtNombre.Text.ParseToInt();
                newCommandeProduit.TotalFardeaux = newCommandeProduit.Format.FardeauParPalette * newCommandeProduit.NombrePalette;
                newCommandeProduit.TotalBouteille = newCommandeProduit.Format.BouteillesParFardeau * newCommandeProduit.TotalFardeaux;
            }

            ListCommandeProduit.Add(newCommandeProduit);
            //   BonCommande.Montant += newCommandeProduit.PrixUnitaire * newCommandeProduit.TotalBouteilles;

            db.AddToCommandeProduits(newCommandeProduit);
            RefreshGrid();
            UpdateControls();

        }

        private void UpdateControls()
        {
            //throw new NotImplementedException();

            txtNombre.Text = "";
            cbGoutProduit.SelectedIndex = 0;
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListCommandeProduit.Select(p => new
            {
                ID = p.ID,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                //REF = ,
                Quantite = ((p.NombrePalette != null) ? p.NombrePalette : p.NombreFardeaux) + " " + p.Unite.ToUniteProduitTxt(),
                Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteille + " Bouteilles",

            }).ToList();
            // throw new NotImplementedException();

            dgv.HideIDColumn();
            UpdateTotals();

        }

        private void UpdateTotals()
        {
            var pal = ListCommandeProduit.Sum(p => p.NombrePalette);
            var fard = ListCommandeProduit.Sum(p => p.NombreFardeaux);
            var bout = ListCommandeProduit.Sum(p => p.TotalBouteille);
            var sfard = ListCommandeProduit.Sum(p => p.TotalFardeaux);

            if (pal != 0)
            {
                labTotal.Text = pal.ToString() + " palettes";
            }

            if (fard != 0)
            {
                labTotal.Text += "   " + fard.ToString() + " Fardeaux    ";
            }


            labTotal.Text += " =(" + sfard + " Fardeaux / " + bout + " bouteilles).";

        }

        private void CreateBon()
        {
            var date = dtDate.Value.Date;
            var no = db.BonCommandes.Count(p => p.Date == date) + 1;

            BonCommande = new BonCommande()
            {
                Client = this.Client,
                Date = date,
                EtatCommande = (int)EnEtatBonCommande.EnInstance,
              //  NoFile = no


            };
        }

        private bool isValidAll()
        {
            var message = "";

            if (this.Client == null)
            {
                message += "Selectionnez un Client!";
            }

            if (cbGoutProduit.SelectedIndex == 0)
            {
                message += "\nSelectionnez un produit";
            }

            if (cbFormat.SelectedIndex == 0)
            {
                message += "\nSelectionner un Format";
            }

            if (txtNombre.Text.ParseToInt() == null)
            {
                message += "\nNombre Invalide !";
            }






            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            if (BonCommande != null)
            {
                BonCommande.Date = dtDate.Value.Date;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.GetSelectedIndex();

            var selected = ListCommandeProduit.ElementAt(index.Value);
            BonCommande.CommandeProduits.Remove(selected);
            ListCommandeProduit.Remove(selected);
            //    BonCommande.Montant -= selected.PrixUnitaire * selected.TotalBouteilles;
            db.DeleteObject(selected);
            RefreshGrid();
            UpdateControls();

        }

        private void txtClient_Leave(object sender, EventArgs e)
        {
            //   var clientNom = txtClient.Text.Trim().ToUpper();
            //   this.Client = db.Clients.SingleOrDefault(c => c.Nom == clientNom);
        }

        private void txtClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteilles();
        }

        private void UpdateTotalBouteilles()
        {
            var Nombre = txtNombre.Text.ParseToInt();
            var Format = (Format)cbFormat.SelectedItem;
            if (Nombre != null && cbFormat.SelectedIndex != 0)
            {
                if (radPalettes.Checked)
                {
                    txtTotalBouteilles.Text = (Nombre * Format.FardeauParPalette * Format.BouteillesParFardeau).ToString();
                }
                else
                {
                    txtTotalBouteilles.Text = (Nombre * Format.BouteillesParFardeau).ToString();
                }
            }
            else
            {
                txtTotalBouteilles.Text = "";
            }
        }

        private void txtNombre_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAjouter;
        }

        private void cbGoutProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbGoutProduit.SelectedIndex > 0)
            {
                if (cbFormat.SelectedIndex > 0)
                {
                    txtNombre.Select();
                }
                else
                {
                    cbFormat.Select();
                }
            }
        }

        private void radFardeaux_CheckedChanged(object sender, EventArgs e)
        {
            txtNombre.Text = "";
        }

        private void btCommande_Click(object sender, EventArgs e)
        {

        }
    }
}
