﻿namespace FZBGsys.NsComercial
{
    partial class FrSelectDateClientSatisfaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.raMensuel = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbClient = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtAnnee = new System.Windows.Forms.DateTimePicker();
            this.raAnnuel = new System.Windows.Forms.RadioButton();
            this.panelPeriode = new System.Windows.Forms.Panel();
            this.chFeilleVide = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbWeek = new System.Windows.Forms.ComboBox();
            this.dtYear = new System.Windows.Forms.DateTimePicker();
            this.groupBox1.SuspendLayout();
            this.panelPeriode.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // raMensuel
            // 
            this.raMensuel.AutoSize = true;
            this.raMensuel.Checked = true;
            this.raMensuel.Location = new System.Drawing.Point(27, 38);
            this.raMensuel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.raMensuel.Name = "raMensuel";
            this.raMensuel.Size = new System.Drawing.Size(233, 21);
            this.raMensuel.TabIndex = 30;
            this.raMensuel.TabStop = true;
            this.raMensuel.Text = "Questionnaires Hébdomadaires:";
            this.raMensuel.UseVisualStyleBackColor = true;
            this.raMensuel.CheckedChanged += new System.EventHandler(this.radioButtonPeriode_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(275, 298);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(159, 30);
            this.button1.TabIndex = 31;
            this.button1.Text = "Générer le Rapport";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbClient);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtAnnee);
            this.groupBox1.Controls.Add(this.raMensuel);
            this.groupBox1.Controls.Add(this.raAnnuel);
            this.groupBox1.Controls.Add(this.panelPeriode);
            this.groupBox1.Location = new System.Drawing.Point(20, 22);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(413, 263);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selection Client et  période";
            // 
            // cbClient
            // 
            this.cbClient.FormattingEnabled = true;
            this.cbClient.Location = new System.Drawing.Point(101, 223);
            this.cbClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbClient.Name = "cbClient";
            this.cbClient.Size = new System.Drawing.Size(289, 24);
            this.cbClient.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 226);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 37;
            this.label1.Text = "Client:";
            // 
            // dtAnnee
            // 
            this.dtAnnee.CustomFormat = "    année   yyyy";
            this.dtAnnee.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAnnee.Location = new System.Drawing.Point(187, 182);
            this.dtAnnee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtAnnee.Name = "dtAnnee";
            this.dtAnnee.ShowUpDown = true;
            this.dtAnnee.Size = new System.Drawing.Size(149, 22);
            this.dtAnnee.TabIndex = 34;
            this.dtAnnee.Visible = false;
            this.dtAnnee.ValueChanged += new System.EventHandler(this.dtAnnee_ValueChanged);
            // 
            // raAnnuel
            // 
            this.raAnnuel.AutoSize = true;
            this.raAnnuel.Location = new System.Drawing.Point(27, 183);
            this.raAnnuel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.raAnnuel.Name = "raAnnuel";
            this.raAnnuel.Size = new System.Drawing.Size(150, 21);
            this.raAnnuel.TabIndex = 33;
            this.raAnnuel.TabStop = true;
            this.raAnnuel.Text = "Rapports Annuels :";
            this.raAnnuel.UseVisualStyleBackColor = true;
            this.raAnnuel.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // panelPeriode
            // 
            this.panelPeriode.Controls.Add(this.chFeilleVide);
            this.panelPeriode.Controls.Add(this.groupBox2);
            this.panelPeriode.Location = new System.Drawing.Point(5, 62);
            this.panelPeriode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelPeriode.Name = "panelPeriode";
            this.panelPeriode.Size = new System.Drawing.Size(401, 106);
            this.panelPeriode.TabIndex = 28;
            // 
            // chFeilleVide
            // 
            this.chFeilleVide.AutoSize = true;
            this.chFeilleVide.Location = new System.Drawing.Point(127, 71);
            this.chFeilleVide.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chFeilleVide.Name = "chFeilleVide";
            this.chFeilleVide.Size = new System.Drawing.Size(241, 21);
            this.chFeilleVide.TabIndex = 39;
            this.chFeilleVide.Text = "Questionnaires vides uniquement";
            this.chFeilleVide.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbWeek);
            this.groupBox2.Controls.Add(this.dtYear);
            this.groupBox2.Location = new System.Drawing.Point(5, 6);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(387, 59);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Période";
            // 
            // cbWeek
            // 
            this.cbWeek.FormattingEnabled = true;
            this.cbWeek.Location = new System.Drawing.Point(121, 22);
            this.cbWeek.Margin = new System.Windows.Forms.Padding(4);
            this.cbWeek.Name = "cbWeek";
            this.cbWeek.Size = new System.Drawing.Size(251, 24);
            this.cbWeek.TabIndex = 1;
            this.cbWeek.SelectedIndexChanged += new System.EventHandler(this.cbWeek_SelectedIndexChanged);
            // 
            // dtYear
            // 
            this.dtYear.CustomFormat = "yyyy";
            this.dtYear.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtYear.Location = new System.Drawing.Point(16, 23);
            this.dtYear.Margin = new System.Windows.Forms.Padding(4);
            this.dtYear.Name = "dtYear";
            this.dtYear.ShowUpDown = true;
            this.dtYear.Size = new System.Drawing.Size(92, 22);
            this.dtYear.TabIndex = 0;
            this.dtYear.ValueChanged += new System.EventHandler(this.dtYear_ValueChanged);
            // 
            // FrSelectDateClientSatisfaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 348);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrSelectDateClientSatisfaction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rapport Personalisé";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrSelectDateFournisseur_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelPeriode.ResumeLayout(false);
            this.panelPeriode.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton raMensuel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtAnnee;
        private System.Windows.Forms.RadioButton raAnnuel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbClient;
        private System.Windows.Forms.Panel panelPeriode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbWeek;
        private System.Windows.Forms.DateTimePicker dtYear;
        private System.Windows.Forms.CheckBox chFeilleVide;
    }
}