﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrBonLivraison : Form
    {
        ModelEntities db = new ModelEntities();

        public FrBonLivraison(string BonAttributionID)
        {
            InitializeComponent();
            txtNoBon.Text = BonAttributionID;
            txtNoBon.ReadOnly = true;
            LoadBonAttribution(BonAttributionID.ParseToInt().Value);

         
        }

        public FrBonLivraison(int? id = null)
        {
            InitializeComponent();
            if (id != null)
            {
                InputBonlivraison(id.Value);
                txtNumeroPC.ReadOnly = this.Chauffeur.NumeroPC.Length > 1;
                txtDatePC.ReadOnly = this.Chauffeur.DatePC.Length > 1;
                txtMatricule.ReadOnly = this.Chauffeur.DefaultMatricule.Length > 1;
                txtNoBon.ReadOnly = true;
            }

            txtNoBon.Select();
        }

        private void InputBonlivraison(int id)
        {
            this.Modify = true;
            this.BonLivraison = db.BonLivraisons.Single(p => p.ID == id);
            BonAttribution = BonLivraison.BonAttribution;
            dtDateLivraison.Value = BonLivraison.DateLivraison.Value;
            txtNoBon.Text = BonAttribution.ID.ToString();
            txtClient.Text = BonAttribution.Client.NomClientAndParent();
            txtAdresse.Text = BonAttribution.Client.Adresse;
            txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();
            txtDestination.Text = txtAdresse.Text;
            //  dtDateLivraison.Value = BonAttribution.Date.Value;
            this.Chauffeur = BonLivraison.Chauffeur;

            if (this.Chauffeur != null)
            {
                txtNomChauffeur.Text = this.Chauffeur.Nom;
                txtNumeroPC.Text = this.Chauffeur.NumeroPC;
                txtDatePC.Text = this.Chauffeur.DatePC;
                txtMatricule.Text = this.Chauffeur.DefaultMatricule;
            }

            gbLiv.Enabled = true;
            RefreshGrid();

        }

        private void btAfficher_Click(object sender, EventArgs e)
        {
            var id = txtNoBon.Text.ParseToInt();

            if (id != null)
            {
                LoadBonAttribution(id.Value);
            }
            else
            {
                this.ShowWarning("Numéro Bon Invalide");
            }

        }

        private void LoadBonAttribution(int id)
        {


            this.BonAttribution = db.BonAttributions.SingleOrDefault(p => p.ID == id);
            if (BonAttribution == null)
            {
                this.ShowWarning("Numéro Bon introuvable");
                return;
            }

            if (BonAttribution.Etat != (int)EnEtatBonAttribution.Chargé)
            {
                this.ShowWarning("Ce Bon est " + BonAttribution.Etat.ToEtatBonAttributionTxt() + " ");
                return;
            }

            //  if (BonAttribution.Etat != (int)EnEtatBonAttribution.Chargé)
            // {
            if (BonAttribution.BonLivraisons.Count != 0)
            {
                if (this.ConfirmInformation("Ce bon est déja livré, voulez-vous imprimer le bon de Livraison?"))
                {

                    FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonLivraison3(BonAttribution.BonLivraisons.First().ID);

                    return;
                }
                return;
            }

            //    this.ShowWarning("Ce Bon d'attribution est " + BonAttribution.Etat.ToEtatBonAttributionTxt());
            //    return;
            //  }
            // else 
            if (BonAttribution.BonLivraisons.Count != 0)
            {
                var first = BonAttribution.BonLivraisons.First();
                if (this.ConfirmInformation("Ce Bon est déja livré, voulez-vous imprimer Bon Livraison?"))
                {

                    FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonLivraison(first.ID);

                    return;
                }
            }

            //gbpayement.Enabled = true;
            txtClient.Text = BonAttribution.Client.NomClientAndParent();
            txtAdresse.Text = BonAttribution.Client.Adresse;
            txtDateBon.Text = BonAttribution.Date.Value.ToShortDateString();
            txtDestination.Text = txtAdresse.Text;

            gbLiv.Enabled = true;
            dtDateLivraison.Value = Tools.GetServerDateTime();
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            var BonSortie = BonAttribution.BonSorties.FirstOrDefault();
            if (BonSortie != null)
            {
                dgv.DataSource = BonSortie.VenteDetails.Select(p => new
                    {
                        ID = p.ID,
                        Produit = p.Vente.GoutProduit.Nom,
                        Format = p.Vente.Format.Volume,
                        REF = p.Vente.Unite.ToUniteProduitTxt(),
                        Production = p.DateProduction.Value.ToShortDateString(),
                        Lot = p.Lot,
                        Numeros = p.Numeros,
                        Nombre = p.Nombre,

                    }).ToList();
                dgv.HideIDColumn();

            }
            else
            {
                dgv.DataSource = BonAttribution.Ventes.Select(p => new
                {
                    ID = p.ID,
                    Produit = p.GoutProduit.Nom,
                    Format = p.Format.Volume,
                    REF = p.Unite.ToUniteProduitTxt(),
                    Production = "/",
                    Lot = "/",
                    Numeros = "/",
                    Nombre = (p.Unite == (int)EnUniteProduit.Palettes) ? p.NombrePalettes : p.NombreFardeaux,

                }).ToList();
                dgv.HideIDColumn();
            }

        }

        private void UpdateControls()
        {

        }

        private void FrBonLivraison_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public BonAttribution BonAttribution { get; set; }

        private void btSelectChauffeur_Click(object sender, EventArgs e)
        {
            this.Chauffeur = FZBGsys.NsComercial.NsChauffeur.FrList.SelectChaffeurDialog(BonAttribution.Client, db);

            if (this.Chauffeur != null)
            {
                txtNomChauffeur.Text = this.Chauffeur.Nom;
                txtNumeroPC.Text = this.Chauffeur.NumeroPC;
                txtDatePC.Text = this.Chauffeur.DatePC;
                txtMatricule.Text = this.Chauffeur.DefaultMatricule;

                txtNumeroPC.ReadOnly =  this.Chauffeur.NumeroPC.Length >1;
                txtDatePC.ReadOnly = this.Chauffeur.DatePC.Length > 1;
                txtMatricule.ReadOnly = this.Chauffeur.DefaultMatricule.Length > 1;
                
            }
        }

        public Chauffeur Chauffeur { get; set; }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {

            if (!isValidAll())
            {
                return;
            }
            if (!this.ConfirmInformation("Etes vous sure de vouloir livrer l'atrribution N° " + BonAttribution.ID + "?0\nClient: " + BonAttribution.Client.Nom + "\nChauffeur: " + Chauffeur.Nom))
            {
                return;
            }

            if (BonLivraison == null)
            {
                BonLivraison = new BonLivraison();
                BonLivraison.BonAttribution = BonAttribution;
            }
            var ventes = BonAttribution.Ventes.ToList();
            foreach (var vente in ventes)
            {
                vente.BonLivraison = BonLivraison;


            }
            var Facture = BonAttribution.Factures.First();

            if (Facture.MontantTTC != BonAttribution.Ventes.Sum(p => p.TotalBouteilles * p.PrixUnitaire))
            {//Recalcule Montants
                Facture.MontantTTC = BonAttribution.Ventes.Sum(p => p.TotalBouteilles * p.PrixUnitaire);
                Facture.MontantHT = BonAttribution.Ventes.Sum(p => p.TotalBouteilles * p.PrixUnitaireHT);
                Facture.MontantTVA = Facture.MontantTTC - Facture.MontantHT;
                Facture.MontantTotalFacture = Facture.MontantTTC;
                if (Facture.ModePayement == (int)EnModePayement.Especes)
                {
                    var timbre = Facture.MontantTTC / 100;
                    if (timbre > 2500)
                    {
                        timbre = 2500;
                    }
                    Facture.MontantTotalFacture += timbre;
                }
                
            }

            Chauffeur.NumeroPC = txtNumeroPC.Text;
            Chauffeur.DatePC = txtDatePC.Text;
            Chauffeur.DefaultMatricule = txtMatricule.Text;


            BonLivraison.Chauffeur = Chauffeur;
            

            BonLivraison.AdresseLivraison = txtDestination.Text.Trim();
            BonAttribution.Etat = (int)EnEtatBonAttribution.Livré;
            var currentTime = Tools.GetServerDateTime();

            if (BonLivraison.EntityState == EntityState.Added)
            {
                BonLivraison.InscriptionTime = currentTime;
                BonLivraison.InscriptionUID = Tools.CurrentUserID;
                BonLivraison.DateLivraison = dtDateLivraison.Value;
            }
            else
            {
                BonLivraison.ModificationTime = currentTime;
            }
            BonLivraison.BonAttribution.Client.DateDernierChargement = dtDateLivraison.Value;
            BonLivraison.BonAttribution.Client.DateDernierMouvement = dtDateLivraison.Value;
            db.SaveChanges();
            btEnregistrer.Enabled = false;
            NsAdministration.FrMaintenance.GenerateSoldeClient(db, null, true, BonLivraison.BonAttribution.Client);

            //----------------------- Update Panier Client

            int NombreChrea = BonAttribution.Ventes.Where(p => p.GoutProduit.TypeProduitID == 1 && p.FormatID == 3).Sum(p => p.TotalBouteilles).Value;
            int NombreOrangeChrea = BonAttribution.Ventes.Where(p => p.GoutProduit.TypeProduitID == 2 && p.FormatID == 3).Sum(p => p.TotalBouteilles).Value;
            if (NombreChrea + NombreOrangeChrea != 0)
            {
                BonAttribution.Client.PanierChrea -= NombreChrea;
                BonAttribution.Client.PanierOrangeChrea -= NombreOrangeChrea;

               
                var clients = db.Clients.Where(p => p.ClientTypeID == 1 && p.ID != BonAttribution.ClientID);
                foreach (var client in clients)
                {

                    client.PanierChrea += NombreChrea / client.InitChrea;
                    client.PanierOrangeChrea += NombreOrangeChrea / client.InitOrangeChrea;

                    if (client.PanierChrea > 32400)
                    {
                        client.PanierChrea = 32400;
                    }

                    if (client.PanierOrangeChrea > 32400)
                    {
                        client.PanierOrangeChrea = 32400;
                    }
                }


            }


            db.SaveChanges();

            FZBGsys.NsReports.Comercial.ComercialReportController.PrintBonLivraison3(BonLivraison.ID);

            Dispose();

        }


        private bool isValidAll()
        {
            var message = "";
            if (Chauffeur == null)
            {
                message = "\nVeillez saisir les information du transport.";
            }

            if (txtDestination.Text.Trim() == "")
            {
                message = "\nVeillez saisir l'adresse de destination.";
            }

            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public BonLivraison BonLivraison { get; set; }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void txtDestination_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        public bool Modify { get; set; }
    }
}
