﻿namespace FZBGsys.NsComercial
{
    partial class FrListVentes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvVente = new System.Windows.Forms.DataGridView();
            this.btRecherche = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.cbProduit = new System.Windows.Forms.ComboBox();
            this.cbPeriode = new System.Windows.Forms.ComboBox();
            this.cbDocument = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gbEtat = new System.Windows.Forms.GroupBox();
            this.chSortie = new System.Windows.Forms.CheckBox();
            this.chAnnulé = new System.Windows.Forms.CheckBox();
            this.chLivré = new System.Windows.Forms.CheckBox();
            this.chEnInstance = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.btImprimer = new System.Windows.Forms.Button();
            this.cbDate = new System.Windows.Forms.ComboBox();
            this.labFilter = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVente)).BeginInit();
            this.panRechAvance.SuspendLayout();
            this.gbEtat.SuspendLayout();
            this.panDate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvVente
            // 
            this.dgvVente.AllowUserToAddRows = false;
            this.dgvVente.AllowUserToDeleteRows = false;
            this.dgvVente.AllowUserToResizeColumns = false;
            this.dgvVente.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvVente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVente.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvVente.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvVente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVente.Location = new System.Drawing.Point(264, 37);
            this.dgvVente.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvVente.MultiSelect = false;
            this.dgvVente.Name = "dgvVente";
            this.dgvVente.ReadOnly = true;
            this.dgvVente.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvVente.RowHeadersVisible = false;
            this.dgvVente.RowTemplate.Height = 16;
            this.dgvVente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVente.Size = new System.Drawing.Size(661, 332);
            this.dgvVente.TabIndex = 11;
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(13, 373);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 65;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // btFermer
            // 
            this.btFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btFermer.Location = new System.Drawing.Point(817, 373);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(108, 28);
            this.btFermer.TabIndex = 64;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(144, 21);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(96, 20);
            this.dtDate.TabIndex = 69;
            this.dtDate.Value = new System.DateTime(2015, 7, 6, 0, 0, 0, 0);
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.cbFormat);
            this.panRechAvance.Controls.Add(this.cbProduit);
            this.panRechAvance.Controls.Add(this.cbPeriode);
            this.panRechAvance.Controls.Add(this.cbDocument);
            this.panRechAvance.Controls.Add(this.label7);
            this.panRechAvance.Controls.Add(this.gbEtat);
            this.panRechAvance.Controls.Add(this.label1);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.txtNoBon);
            this.panRechAvance.Controls.Add(this.label3);
            this.panRechAvance.Controls.Add(this.panDate);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(13, 76);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(245, 293);
            this.panRechAvance.TabIndex = 68;
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(174, 90);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(57, 21);
            this.cbFormat.TabIndex = 73;
            // 
            // cbProduit
            // 
            this.cbProduit.FormattingEnabled = true;
            this.cbProduit.Location = new System.Drawing.Point(62, 90);
            this.cbProduit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbProduit.Name = "cbProduit";
            this.cbProduit.Size = new System.Drawing.Size(104, 21);
            this.cbProduit.TabIndex = 72;
            // 
            // cbPeriode
            // 
            this.cbPeriode.Enabled = false;
            this.cbPeriode.FormattingEnabled = true;
            this.cbPeriode.Items.AddRange(new object[] {
            "date attribution",
            "date sortie",
            "date livraison"});
            this.cbPeriode.Location = new System.Drawing.Point(15, 125);
            this.cbPeriode.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbPeriode.Name = "cbPeriode";
            this.cbPeriode.Size = new System.Drawing.Size(120, 21);
            this.cbPeriode.TabIndex = 71;
            // 
            // cbDocument
            // 
            this.cbDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocument.FormattingEnabled = true;
            this.cbDocument.Items.AddRange(new object[] {
            "",
            "Attribution",
            "Sortie",
            "Livraison",
            "Facture"});
            this.cbDocument.Location = new System.Drawing.Point(62, 33);
            this.cbDocument.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbDocument.Name = "cbDocument";
            this.cbDocument.Size = new System.Drawing.Size(83, 21);
            this.cbDocument.TabIndex = 30;
            this.cbDocument.SelectedIndexChanged += new System.EventHandler(this.cbDocument_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1, 36);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Document:";
            // 
            // gbEtat
            // 
            this.gbEtat.Controls.Add(this.chSortie);
            this.gbEtat.Controls.Add(this.chAnnulé);
            this.gbEtat.Controls.Add(this.chLivré);
            this.gbEtat.Controls.Add(this.chEnInstance);
            this.gbEtat.Location = new System.Drawing.Point(10, 215);
            this.gbEtat.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbEtat.Name = "gbEtat";
            this.gbEtat.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbEtat.Size = new System.Drawing.Size(217, 66);
            this.gbEtat.TabIndex = 28;
            this.gbEtat.TabStop = false;
            this.gbEtat.Text = "Etat";
            // 
            // chSortie
            // 
            this.chSortie.AutoSize = true;
            this.chSortie.Checked = true;
            this.chSortie.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chSortie.Location = new System.Drawing.Point(38, 39);
            this.chSortie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chSortie.Name = "chSortie";
            this.chSortie.Size = new System.Drawing.Size(60, 17);
            this.chSortie.TabIndex = 1;
            this.chSortie.Text = "Chargé";
            this.chSortie.UseVisualStyleBackColor = true;
            // 
            // chAnnulé
            // 
            this.chAnnulé.AutoSize = true;
            this.chAnnulé.Location = new System.Drawing.Point(122, 39);
            this.chAnnulé.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chAnnulé.Name = "chAnnulé";
            this.chAnnulé.Size = new System.Drawing.Size(59, 17);
            this.chAnnulé.TabIndex = 0;
            this.chAnnulé.Text = "Annulé";
            this.chAnnulé.UseVisualStyleBackColor = true;
            // 
            // chLivré
            // 
            this.chLivré.AutoSize = true;
            this.chLivré.Checked = true;
            this.chLivré.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chLivré.Location = new System.Drawing.Point(122, 16);
            this.chLivré.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chLivré.Name = "chLivré";
            this.chLivré.Size = new System.Drawing.Size(49, 17);
            this.chLivré.TabIndex = 0;
            this.chLivré.Text = "Livré";
            this.chLivré.UseVisualStyleBackColor = true;
            // 
            // chEnInstance
            // 
            this.chEnInstance.AutoSize = true;
            this.chEnInstance.Checked = true;
            this.chEnInstance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEnInstance.Location = new System.Drawing.Point(38, 16);
            this.chEnInstance.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chEnInstance.Name = "chEnInstance";
            this.chEnInstance.Size = new System.Drawing.Size(83, 17);
            this.chEnInstance.TabIndex = 0;
            this.chEnInstance.Text = "En Instance";
            this.chEnInstance.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 96);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Produit:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Client:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(62, 66);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtClient.Name = "txtClient";
            this.txtClient.Size = new System.Drawing.Size(169, 20);
            this.txtClient.TabIndex = 26;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Enabled = false;
            this.txtNoBon.Location = new System.Drawing.Point(179, 33);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(52, 20);
            this.txtNoBon.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(151, 36);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "N°:";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(40, 148);
            this.panDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(160, 57);
            this.panDate.TabIndex = 24;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(47, 31);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(105, 20);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(47, 8);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(105, 20);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(13, 55);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(125, 17);
            this.chRechAvancee.TabIndex = 67;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // btImprimer
            // 
            this.btImprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btImprimer.Location = new System.Drawing.Point(264, 373);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(108, 28);
            this.btImprimer.TabIndex = 64;
            this.btImprimer.Text = "Imprimer";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbDate
            // 
            this.cbDate.Enabled = false;
            this.cbDate.FormattingEnabled = true;
            this.cbDate.Items.AddRange(new object[] {
            "",
            "date attribution",
            "date sortie",
            "date livraison"});
            this.cbDate.Location = new System.Drawing.Point(19, 20);
            this.cbDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(119, 21);
            this.cbDate.TabIndex = 70;
            // 
            // labFilter
            // 
            this.labFilter.AutoSize = true;
            this.labFilter.Location = new System.Drawing.Point(262, 18);
            this.labFilter.Name = "labFilter";
            this.labFilter.Size = new System.Drawing.Size(0, 13);
            this.labFilter.TabIndex = 71;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Total:";
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(52, 8);
            this.labTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(31, 13);
            this.labTotal.TabIndex = 73;
            this.labTotal.Text = "0000";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.labTotal);
            this.panel1.Location = new System.Drawing.Point(377, 374);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(417, 34);
            this.panel1.TabIndex = 74;
            // 
            // FrListVentes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 412);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labFilter);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.dgvVente);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrListVentes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liste des Ventes";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrListVentes_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVente)).EndInit();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.gbEtat.ResumeLayout(false);
            this.gbEtat.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvVente;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.ComboBox cbDocument;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbEtat;
        private System.Windows.Forms.CheckBox chSortie;
        private System.Windows.Forms.CheckBox chAnnulé;
        private System.Windows.Forms.CheckBox chLivré;
        private System.Windows.Forms.CheckBox chEnInstance;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.ComboBox cbPeriode;
        private System.Windows.Forms.ComboBox cbDate;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.ComboBox cbProduit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.Panel panel1;
    }
}