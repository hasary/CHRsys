﻿namespace FZBGsys.NsComercial
{
    partial class FrListOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.cbChargeusine = new System.Windows.Forms.ComboBox();
            this.cbAssocier = new System.Windows.Forms.ComboBox();
            this.cbchargecategorie = new System.Windows.Forms.ComboBox();
            this.chVirement = new System.Windows.Forms.CheckBox();
            this.chCheque = new System.Windows.Forms.CheckBox();
            this.chTraite = new System.Windows.Forms.CheckBox();
            this.chEspece = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chDepense = new System.Windows.Forms.CheckBox();
            this.chRecette = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btRecherche = new System.Windows.Forms.Button();
            this.panFacture = new System.Windows.Forms.Panel();
            this.txtNoFacture = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbTypeOperation = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFourni = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btImprimer = new System.Windows.Forms.Button();
            this.panEdit = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.btCloturer = new System.Windows.Forms.Button();
            this.LabFilter = new System.Windows.Forms.Label();
            this.panEdit2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btaddminsupp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panRechAvance.SuspendLayout();
            this.panFacture.SuspendLayout();
            this.panDate.SuspendLayout();
            this.panEdit.SuspendLayout();
            this.panEdit2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(234, 35);
            this.dgv.Margin = new System.Windows.Forms.Padding(2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(649, 392);
            this.dgv.TabIndex = 11;
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(61, 19);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(168, 20);
            this.dtDate.TabIndex = 68;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.cbChargeusine);
            this.panRechAvance.Controls.Add(this.cbAssocier);
            this.panRechAvance.Controls.Add(this.cbchargecategorie);
            this.panRechAvance.Controls.Add(this.chVirement);
            this.panRechAvance.Controls.Add(this.chCheque);
            this.panRechAvance.Controls.Add(this.chTraite);
            this.panRechAvance.Controls.Add(this.chEspece);
            this.panRechAvance.Controls.Add(this.label9);
            this.panRechAvance.Controls.Add(this.chDepense);
            this.panRechAvance.Controls.Add(this.chRecette);
            this.panRechAvance.Controls.Add(this.label8);
            this.panRechAvance.Controls.Add(this.button1);
            this.panRechAvance.Controls.Add(this.btRecherche);
            this.panRechAvance.Controls.Add(this.panFacture);
            this.panRechAvance.Controls.Add(this.cbTypeOperation);
            this.panRechAvance.Controls.Add(this.label7);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.txtFourni);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.panDate);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(9, 67);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(218, 401);
            this.panRechAvance.TabIndex = 67;
            // 
            // cbChargeusine
            // 
            this.cbChargeusine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChargeusine.FormattingEnabled = true;
            this.cbChargeusine.Items.AddRange(new object[] {
            "Type:",
            "Achat consommable",
            "Service"});
            this.cbChargeusine.Location = new System.Drawing.Point(76, 63);
            this.cbChargeusine.Margin = new System.Windows.Forms.Padding(2);
            this.cbChargeusine.Name = "cbChargeusine";
            this.cbChargeusine.Size = new System.Drawing.Size(125, 21);
            this.cbChargeusine.TabIndex = 79;
            // 
            // cbAssocier
            // 
            this.cbAssocier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAssocier.FormattingEnabled = true;
            this.cbAssocier.Items.AddRange(new object[] {
            "Associer:",
            "NMC",
            "Z.Med",
            "Z.Ndine",
            "Z.Samy",
            "Z.ElHadja",
            "Z.Med&Z.Ndine"});
            this.cbAssocier.Location = new System.Drawing.Point(110, 62);
            this.cbAssocier.Margin = new System.Windows.Forms.Padding(2);
            this.cbAssocier.Name = "cbAssocier";
            this.cbAssocier.Size = new System.Drawing.Size(92, 21);
            this.cbAssocier.TabIndex = 79;
            // 
            // cbchargecategorie
            // 
            this.cbchargecategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbchargecategorie.FormattingEnabled = true;
            this.cbchargecategorie.Items.AddRange(new object[] {
            "catégorie:",
            "PET",
            "Canette",
            "Flote",
            "Administration",
            "Commercial",
            "Magasin",
            "Laboratoire",
            "Logistique",
            "Frais de mission",
            "Autre"});
            this.cbchargecategorie.Location = new System.Drawing.Point(75, 94);
            this.cbchargecategorie.Margin = new System.Windows.Forms.Padding(2);
            this.cbchargecategorie.Name = "cbchargecategorie";
            this.cbchargecategorie.Size = new System.Drawing.Size(127, 21);
            this.cbchargecategorie.TabIndex = 80;
            // 
            // chVirement
            // 
            this.chVirement.AutoSize = true;
            this.chVirement.Checked = true;
            this.chVirement.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chVirement.Location = new System.Drawing.Point(130, 334);
            this.chVirement.Margin = new System.Windows.Forms.Padding(2);
            this.chVirement.Name = "chVirement";
            this.chVirement.Size = new System.Drawing.Size(67, 17);
            this.chVirement.TabIndex = 78;
            this.chVirement.Text = "Virement";
            this.chVirement.UseVisualStyleBackColor = true;
            // 
            // chCheque
            // 
            this.chCheque.AutoSize = true;
            this.chCheque.Checked = true;
            this.chCheque.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chCheque.Location = new System.Drawing.Point(55, 334);
            this.chCheque.Margin = new System.Windows.Forms.Padding(2);
            this.chCheque.Name = "chCheque";
            this.chCheque.Size = new System.Drawing.Size(67, 17);
            this.chCheque.TabIndex = 78;
            this.chCheque.Text = "chèques";
            this.chCheque.UseVisualStyleBackColor = true;
            // 
            // chTraite
            // 
            this.chTraite.AutoSize = true;
            this.chTraite.Checked = true;
            this.chTraite.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chTraite.Location = new System.Drawing.Point(130, 308);
            this.chTraite.Margin = new System.Windows.Forms.Padding(2);
            this.chTraite.Name = "chTraite";
            this.chTraite.Size = new System.Drawing.Size(53, 17);
            this.chTraite.TabIndex = 78;
            this.chTraite.Text = "Traite";
            this.chTraite.UseVisualStyleBackColor = true;
            // 
            // chEspece
            // 
            this.chEspece.AutoSize = true;
            this.chEspece.Checked = true;
            this.chEspece.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEspece.Location = new System.Drawing.Point(56, 308);
            this.chEspece.Margin = new System.Windows.Forms.Padding(2);
            this.chEspece.Name = "chEspece";
            this.chEspece.Size = new System.Drawing.Size(67, 17);
            this.chEspece.TabIndex = 78;
            this.chEspece.Text = "Espèces";
            this.chEspece.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 308);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 77;
            this.label9.Text = "Mode:";
            // 
            // chDepense
            // 
            this.chDepense.AutoSize = true;
            this.chDepense.Checked = true;
            this.chDepense.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDepense.Location = new System.Drawing.Point(132, 9);
            this.chDepense.Margin = new System.Windows.Forms.Padding(2);
            this.chDepense.Name = "chDepense";
            this.chDepense.Size = new System.Drawing.Size(74, 17);
            this.chDepense.TabIndex = 76;
            this.chDepense.Text = "Dépenses";
            this.chDepense.UseVisualStyleBackColor = true;
            // 
            // chRecette
            // 
            this.chRecette.AutoSize = true;
            this.chRecette.Checked = true;
            this.chRecette.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chRecette.Location = new System.Drawing.Point(59, 9);
            this.chRecette.Margin = new System.Windows.Forms.Padding(2);
            this.chRecette.Name = "chRecette";
            this.chRecette.Size = new System.Drawing.Size(69, 17);
            this.chRecette.TabIndex = 76;
            this.chRecette.Text = "Recettes";
            this.chRecette.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 164);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 75;
            this.label8.Text = "Fournisseur:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 140);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 19);
            this.button1.TabIndex = 74;
            this.button1.Text = "....";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(8, 370);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 71;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // panFacture
            // 
            this.panFacture.Controls.Add(this.txtNoFacture);
            this.panFacture.Controls.Add(this.label3);
            this.panFacture.Location = new System.Drawing.Point(55, 93);
            this.panFacture.Margin = new System.Windows.Forms.Padding(2);
            this.panFacture.Name = "panFacture";
            this.panFacture.Size = new System.Drawing.Size(146, 29);
            this.panFacture.TabIndex = 73;
            // 
            // txtNoFacture
            // 
            this.txtNoFacture.Enabled = false;
            this.txtNoFacture.Location = new System.Drawing.Point(75, 7);
            this.txtNoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoFacture.Name = "txtNoFacture";
            this.txtNoFacture.Size = new System.Drawing.Size(55, 20);
            this.txtNoFacture.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "N° Facture:";
            // 
            // cbTypeOperation
            // 
            this.cbTypeOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeOperation.FormattingEnabled = true;
            this.cbTypeOperation.Items.AddRange(new object[] {
            "[Tout]",
            "Reglement Créance",
            "Avance",
            "Payement Espèces"});
            this.cbTypeOperation.Location = new System.Drawing.Point(15, 38);
            this.cbTypeOperation.Margin = new System.Windows.Forms.Padding(2);
            this.cbTypeOperation.Name = "cbTypeOperation";
            this.cbTypeOperation.Size = new System.Drawing.Size(186, 21);
            this.cbTypeOperation.TabIndex = 30;
            this.cbTypeOperation.SelectedIndexChanged += new System.EventHandler(this.cbTypeOperation_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 29;
            this.label7.Text = "Type:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 124);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Client:";
            // 
            // txtFourni
            // 
            this.txtFourni.Location = new System.Drawing.Point(35, 186);
            this.txtFourni.Margin = new System.Windows.Forms.Padding(2);
            this.txtFourni.Name = "txtFourni";
            this.txtFourni.Size = new System.Drawing.Size(167, 20);
            this.txtFourni.TabIndex = 26;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(52, 141);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(152, 20);
            this.txtClient.TabIndex = 26;
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(41, 238);
            this.panDate.Margin = new System.Windows.Forms.Padding(2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(160, 57);
            this.panDate.TabIndex = 24;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(47, 31);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(105, 20);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 32);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(47, 8);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(105, 20);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2012, 7, 23, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 219);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date :";
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(14, 46);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(125, 17);
            this.chRechAvancee.TabIndex = 66;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Date :";
            // 
            // btImprimer
            // 
            this.btImprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btImprimer.Location = new System.Drawing.Point(689, 433);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(107, 30);
            this.btImprimer.TabIndex = 70;
            this.btImprimer.Text = "Imprimer la liste";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // panEdit
            // 
            this.panEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panEdit.Controls.Add(this.btAnnuler);
            this.panEdit.Location = new System.Drawing.Point(232, 431);
            this.panEdit.Name = "panEdit";
            this.panEdit.Size = new System.Drawing.Size(149, 36);
            this.panEdit.TabIndex = 72;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(2, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(140, 29);
            this.btAnnuler.TabIndex = 58;
            this.btAnnuler.Text = "Supprimer la selection";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btFermer
            // 
            this.btFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btFermer.Location = new System.Drawing.Point(801, 433);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(82, 30);
            this.btFermer.TabIndex = 69;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // btCloturer
            // 
            this.btCloturer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btCloturer.ForeColor = System.Drawing.Color.Crimson;
            this.btCloturer.Location = new System.Drawing.Point(531, 434);
            this.btCloturer.Margin = new System.Windows.Forms.Padding(2);
            this.btCloturer.Name = "btCloturer";
            this.btCloturer.Size = new System.Drawing.Size(154, 28);
            this.btCloturer.TabIndex = 73;
            this.btCloturer.Text = "Cloturer la Journée";
            this.btCloturer.UseVisualStyleBackColor = true;
            this.btCloturer.Click += new System.EventHandler(this.btCloturer_Click);
            // 
            // LabFilter
            // 
            this.LabFilter.AutoSize = true;
            this.LabFilter.Location = new System.Drawing.Point(237, 11);
            this.LabFilter.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabFilter.Name = "LabFilter";
            this.LabFilter.Size = new System.Drawing.Size(0, 13);
            this.LabFilter.TabIndex = 74;
            // 
            // panEdit2
            // 
            this.panEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panEdit2.Controls.Add(this.button3);
            this.panEdit2.Controls.Add(this.btaddminsupp);
            this.panEdit2.Location = new System.Drawing.Point(386, 435);
            this.panEdit2.Margin = new System.Windows.Forms.Padding(2);
            this.panEdit2.Name = "panEdit2";
            this.panEdit2.Size = new System.Drawing.Size(139, 37);
            this.panEdit2.TabIndex = 75;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.ForeColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(8, 2);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(62, 28);
            this.button3.TabIndex = 70;
            this.button3.Text = "Modifier";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btaddminsupp
            // 
            this.btaddminsupp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btaddminsupp.ForeColor = System.Drawing.Color.Red;
            this.btaddminsupp.Location = new System.Drawing.Point(74, 2);
            this.btaddminsupp.Margin = new System.Windows.Forms.Padding(2);
            this.btaddminsupp.Name = "btaddminsupp";
            this.btaddminsupp.Size = new System.Drawing.Size(62, 28);
            this.btaddminsupp.TabIndex = 70;
            this.btaddminsupp.Text = "Supprim";
            this.btaddminsupp.UseVisualStyleBackColor = true;
            this.btaddminsupp.Click += new System.EventHandler(this.btaddminsupp_Click);
            // 
            // FrListOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 482);
            this.Controls.Add(this.panEdit2);
            this.Controls.Add(this.LabFilter);
            this.Controls.Add(this.btCloturer);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.panEdit);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrListOperation";
            this.Text = "Liste des operations caisse";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrListOperation_FormClosed);
            this.Load += new System.EventHandler(this.FrListOperation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.panFacture.ResumeLayout(false);
            this.panFacture.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panEdit.ResumeLayout(false);
            this.panEdit2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.ComboBox cbTypeOperation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoFacture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panFacture;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.Panel panEdit;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chDepense;
        private System.Windows.Forms.CheckBox chRecette;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFourni;
        private System.Windows.Forms.Button btCloturer;
        private System.Windows.Forms.Label LabFilter;
        private System.Windows.Forms.CheckBox chVirement;
        private System.Windows.Forms.CheckBox chCheque;
        private System.Windows.Forms.CheckBox chTraite;
        private System.Windows.Forms.CheckBox chEspece;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbAssocier;
        private System.Windows.Forms.Panel panEdit2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btaddminsupp;
        private System.Windows.Forms.ComboBox cbChargeusine;
        private System.Windows.Forms.ComboBox cbchargecategorie;
    }
}