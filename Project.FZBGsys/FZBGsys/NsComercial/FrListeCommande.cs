﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrListeCommande : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListeCommande()
        {
            InitializeComponent();
            btAttribution.Visible = Tools.CurrentUser.GroupeID == 9;
        }

        private void chRechercheAvance_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = chRechercheAvance.Checked;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btBrowseClient_Click(object sender, EventArgs e)
        {
            this.Client = FZBGsys.NsComercial.NsClient.FrList.SelectClientDialog(db);
            if (this.Client != null && this.Client.isExpire == true)
            {
                Tools.ShowError("Ce compte client est fermé (ancien dossier)");
                this.Client = null;
            }

            else if (this.Client != null)
            {
                txtClient.Text = this.Client.Nom;

            }
        }

        public Client Client { get; set; }

        private void FrListeCommande_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
        }

        private void Recherche()
        {
            if (!chRechercheAvance.Checked)
            {
                var from = dtDate.Value.Date;
                var to = dtDate.Value.Date.AddDays(1);
                ListCommandeFound = db.BonCommandes.Where(p => p.InscriptionDate > from && p.InscriptionDate < to).ToList();
            }
            else
            {
                IQueryable<BonCommande> result = db.BonCommandes.Where(p => p.EtatCommande != (int)EnEtatBonCommande.Annulé);
                var from = dtFrom.Value.Date;
                var to = dtTo.Value.Date;

                if (cbDateType.SelectedIndex == 0)
                {
                    result = result.Where(p => p.InscriptionDate > from && p.InscriptionDate < to);
                }
                else if (cbDateType.SelectedIndex == 1)
                {
                    result = result.Where(p => p.Date > from && p.InscriptionDate < to);
                }

                if (this.Client != null)
                {
                    result = result.Where(p => p.ClientID == Client.ID);
                }

                if (!chEtatEnInstance.Checked)
                {
                    result = result.Where(p => p.EtatCommande != (int)EnEtatBonCommande.EnInstance);

                }

                if (!chEtatSolde.Checked)
                {
                    result = result.Where(p => p.EtatCommande != (int)EnEtatBonCommande.Solde);

                }


                if (!chproduitChrea.Checked)
                {
                    result = result.Where(p => !p.CommandeProduits.Select(q => q.GoutProduit.TypeProduitID).Contains(1));
                }

                if (!chproduitOrangeChrea.Checked)
                {
                    result = result.Where(p => !p.CommandeProduits.Select(q => q.GoutProduit.TypeProduitID).Contains(2));
                }

                ListCommandeFound = result.ToList();
            }

            RefreshGrid();
        }

        private void RefreshGrid()
        {
            dgvCommande.DataSource = ListCommandeFound.Select(
                p =>
                    new
                    {
                        No = p.ID,
                        Client = p.Client.NomClientAndParent(),
                        Elevement = p.Date.Value.ToString("dddd dd/MM/yy"),
                        Produit = ((p.CommandeProduits.Select(q => q.GoutProduit.TypeProduitID).Contains(1)) ? "Chréa " : "") + "  " +
                                    ((p.CommandeProduits.Select(q => q.GoutProduit.TypeProduitID).Contains(2)) ? "Orange-Chréa " : ""),
                        Quantite = p.CommandeProduits.Sum(q => q.NombrePalette) + " P",
                        Etat = p.EtatCommande.ToEtatBonCommandeTxt(),

                        Enregisrement = p.ApplicationUtilisateur.Employer.Nom + " " + p.InscriptionDate.Value.ToString("dd/MM/yyyy à hh:mm")
                    }
                ).ToList();
        }

        public List<BonCommande> ListCommandeFound { get; set; }

        private void btModifie_Click(object sender, EventArgs e)
        {
            var id = dgvCommande.GetSelectedID("No");
            var com = db.BonCommandes.Single(p => p.ID == id);
            if (com.EtatCommande != (int)EnEtatBonCommande.Annulé)
            {
                Tools.ShowInformation("cette commande est "+com.EtatCommande.ToEtatBonCommandeTxt());
                return;
            }
            if (com == null)
            {
                return;
            }
            if (com.ApplicationUtilisateur.ID != Tools.CurrentUserID)
            {
                Tools.ShowInformation("vous ne pouvez pas modifier une commande dont vous n'etes pas l'auteur");
                return;
            }
            new FrBonCommande(id.Value).ShowDialog();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, com);
            RefreshGrid();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            var id = dgvCommande.GetSelectedID("No");
            var com = db.BonCommandes.Single(p => p.ID == id);
            if (com == null)
            {
                return;
            }
            if (com.ApplicationUtilisateur.ID != Tools.CurrentUserID)
            {
                Tools.ShowInformation("vous ne pouvez pas annuler une commande dont vous n'etes pas l'auteur");
                return;
            }

            if (this.ConfirmWarning("Etes-vous sure de vouloir annuler la commande " + com.Client.Nom + " ?"))
            {
                com.EtatCommande = (int)EnEtatBonCommande.Annulé;
                db.SaveChanges();
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, com);
                RefreshGrid();
            }
        }

        private void btImprimer_Click(object sender, EventArgs e)
        {
            Tools.ClearPrintTemp();
            foreach (var p in ListCommandeFound)
            {
               /* db.AddToPrintTemps(new PrintTemp
                {

                    Nom = p.Client.NomClientAndParent(),
                    Date = p.Date,
                    Description = ((p.CommandeProduits.Select(q => q.GoutProduit.TypeProduitID).Contains(1)) ? "Chréa " : "") + "  " +
                                ((p.CommandeProduits.Select(q => q.GoutProduit.TypeProduitID).Contains(2)) ? "Orange-Chréa " : ""),
                    Piece = p.CommandeProduits.Sum(q => q.NombrePalette),
                    Etat = p.EtatCommande.ToEtatBonCommandeTxt(),
                    UID = Tools.CurrentUserID,
                    val2 = p.ApplicationUtilisateur.Employer.Nom + " " + p.InscriptionDate.Value.ToString("dd/MM/yyyy à hh:mm")
                }
                

                );
                */

                db.AddToPrintTemps(new PrintTemp{UID = Tools.CurrentUserID, Piece = p.ID});

            }
            db.SaveChanges();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btAttribution_Click(object sender, EventArgs e)
        {
            var id = dgvCommande.GetSelectedID("No");

            if (id == null)
            {
                return;
            }
           var BonCommande = db.BonCommandes.SingleOrDefault(p => p.ID == id.Value);
            if (BonCommande != null)
            {
                if (BonCommande.EtatCommande != (int)EnEtatBonCommande.EnInstance)
                {
                    Tools.ShowError("Cette commande est " + BonCommande.EtatCommande.ToEtatBonCommandeTxt());
                    return;
                }
            }

            new FrBonAttribution(true, id.Value).ShowDialog();

            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, BonCommande);
            RefreshGrid();
        }
    }
}
