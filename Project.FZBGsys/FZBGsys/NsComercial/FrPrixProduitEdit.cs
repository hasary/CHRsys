﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrPrixProduitEdit : Form
    {
        ModelEntities db = new ModelEntities();
        PrixProduit Prix;
        public FrPrixProduitEdit()
        {
            InitializeComponent();

            InitialiseControls();
        }


        public FrPrixProduitEdit(PrixProduit PrixAncienContext)
        {
            InitializeComponent();

            InitialiseControls();
            this.Prix = PrixAncienContext;

            this.Prix = db.PrixProduits.FirstOrDefault(p => p.FormatID == PrixAncienContext.FormatID && p.GoutProduitID == PrixAncienContext.GoutProduitID && p.TypeClient == PrixAncienContext.TypeClient);
            InputPrix();

        }

        private void InputPrix()
        {
            cbCategorieClient.SelectedItem = Prix.TypeClient1;
            cbFormat.SelectedItem = Prix.Format;
            cbGout.SelectedItem = Prix.GoutProduit;
            var REF = db.ReferenceProduits.SingleOrDefault(p => p.FormatID == Prix.FormatID && p.GoutProduitID == Prix.GoutProduitID);

            txtReference.Text = REF == null ? "G" + Prix.GoutProduitID.ToString() + "F" + Prix.FormatID.ToString() : REF.Reference;

            cbFormat.Enabled = false;
            cbGout.Enabled = false;
            cbCategorieClient.Enabled = false;


            txtPrix.Text = Prix.PrixUnitaire.ToString();
        }

        private void InitialiseControls()
        {
            var tt = new TypeClient { ID = 0, Nom = "[selectionner]" };
            cbCategorieClient.Items.Add(tt);
            cbCategorieClient.Items.AddRange(db.TypeClients.ToArray());
            cbCategorieClient.SelectedItem = tt;
            cbCategorieClient.ValueMember = "ID";
            cbCategorieClient.DisplayMember = "Nom";


            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.ToArray());
            cbFormat.SelectedIndex = 0;
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";



            cbGout.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGout.Items.AddRange(db.GoutProduits.ToArray());
            cbGout.SelectedIndex = 0;
            cbGout.DisplayMember = "Nom";
            cbGout.ValueMember = "ID";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrPrixProduitEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var CurrentDate = Tools.GetServerDateTime();

            #region en cas de modfication Prix
            if (this.Prix != null) // modifier existant
            {
                this.Prix.TypeClient1 = (TypeClient)cbCategorieClient.SelectedItem;
                this.Prix.Format = (Format)cbFormat.SelectedItem;
                this.Prix.GoutProduit = (GoutProduit)cbGout.SelectedItem;
                var REF = db.ReferenceProduits.SingleOrDefault(p => p.FormatID == this.Prix.Format.ID && p.GoutProduitID == this.Prix.GoutProduit.ID);
                if (REF != null)
                {
                    REF.Reference = txtReference.Text.Trim();
                }
                else
                {
                    db.AddToReferenceProduits(new ReferenceProduit
                    {
                        GoutProduit = this.Prix.GoutProduit,
                        Format = this.Prix.Format,
                        Reference = txtReference.Text.Trim()
                    });

                }


                if (this.Prix.PrixUnitaire != Decimal.Parse(txtPrix.Text))
                {
                    this.Prix.DateDernierModif = CurrentDate;
                    this.Prix.UtilisateurModif = Tools.CurrentUserID;

                }
                this.Prix.PrixUnitaire = Decimal.Parse(txtPrix.Text);
                this.Prix.PrixUnitaireHT = this.Prix.PrixUnitaire * 100 / 117;



                DialogResult result = MessageBox.Show("Voulez vous modifier tout les prix de type produit " + this.Prix.GoutProduit.TypeProduit.Nom + " ?", "Confirmer", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    var typeID = this.Prix.GoutProduit.TypeProduitID;
                    var typeClientID = this.Prix.TypeClient;
                    var format = this.Prix.FormatID;

                    var prodAModifier = db.PrixProduits.Where(p =>
                        p.GoutProduit.TypeProduitID == typeID &&
                        p.TypeClient == typeClientID && p.FormatID == format
                        ).ToList();


                    foreach (var item in prodAModifier)
                    {
                        if (item.PrixUnitaire != this.Prix.PrixUnitaire)
                        {
                            item.DateDernierModif = CurrentDate;
                            item.UtilisateurModif = Tools.CurrentUserID;

                        }
                        item.PrixUnitaire = this.Prix.PrixUnitaire;
                        item.PrixUnitaireHT = this.Prix.PrixUnitaireHT;
                        
                    }
                    db.SaveChanges();
                    //      db.Refresh(System.Data.Objects.RefreshMode.StoreWins, prodAModifier);
                }
                else
                {

                    db.SaveChanges();
                    //    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, this.Prix);
                }



                Dispose();
                return;
            }
            #endregion


            // nouveau prix


            var Prix = new PrixProduit();
            Prix.TypeClient1 = (TypeClient)cbCategorieClient.SelectedItem;
            Prix.Format = (Format)cbFormat.SelectedItem;
            Prix.GoutProduit = (GoutProduit)cbGout.SelectedItem;


            //var existe = db.PrixProduits.FirstOrDefault(p => p.Format.Volume == Prix.Format.Volume && p.GoutProduit.Nom == Prix.GoutProduit.Nom && p.TypeClient1.ID == Prix.TypeClient1.ID);
            /*if (existe != null)
            {
                MessageBox.Show("Prix Existant", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }*/


            if (txtPrix.Text.ParseToDec() == null)
            {
                MessageBox.Show("Prix invalide", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            decimal temp;

            if (!decimal.TryParse(txtPrix.Text, out temp))
            {
                MessageBox.Show("Prix invalide", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;

            }


            if (txtPrix.Text == "")
            {
                MessageBox.Show("Prix invalide", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Prix.PrixUnitaire = Decimal.Parse(txtPrix.Text);
            Prix.PrixUnitaireHT = Prix.PrixUnitaire * 100 / 117;
            Prix.UtilisateurModif = Tools.CurrentUserID;
            Prix.DateDernierModif = CurrentDate;
            Prix.IsRemisable = false;


            db.PrixProduits.AddObject(Prix);
            if(db.ReferenceProduits.Where(p=> p.Reference == txtReference.Text.Trim()) == null)
            db.AddToReferenceProduits(new ReferenceProduit
            {
                GoutProduit = Prix.GoutProduit,
                Format = Prix.Format,
                Reference = txtReference.Text.Trim()
            });


           // db.SaveChanges();

            DialogResult result2 = MessageBox.Show("Voulez vous creer tout les prix manquant de type produit " + Prix.GoutProduit.TypeProduit.Nom + " ?", "Confirmer", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result2 == System.Windows.Forms.DialogResult.Yes)
            {
                var typeID = Prix.GoutProduit.TypeProduitID;
                var typeClientID = Prix.TypeClient;
                var format = Prix.FormatID;

               /* var prodAModifier = db.PrixProduits.Where(p =>
                    p.GoutProduit.TypeProduitID == typeID &&
                    p.TypeClient == typeClientID && p.FormatID == format
                    ).ToList();*/
                foreach (var formatt in db.Formats.Where(p=> p.ID == format))
                {
                    foreach (var gout in db.GoutProduits.Where(p => p.TypeProduitID == typeID))
                    {
                        var exe = db.PrixProduits.SingleOrDefault(
                            p => p.FormatID == formatt.ID && p.GoutProduitID == gout.ID && p.TypeClient == typeClientID);
                        if (exe != null)
                        {
                            /*db.AddToPrixProduits(new PrixProduit
                            {
                                GoutProduit = gout,
                                Format = formatt,
                                TypeClient = typeClientID,
                                PrixUnitaire = Prix.PrixUnitaire,
                                PrixUnitaireHT = Prix.PrixUnitaireHT,
                                DateDernierModif = CurrentDate,
                                UtilisateurModif = Tools.CurrentUserID,
                                IsRemisable = false

                            });*/
                            var refpd =db.ReferenceProduits.Where(p => p.GoutProduitID == gout.ID && p.FormatID == formatt.ID).ToList();
                            if (refpd.Count == 0)
                            db.AddToReferenceProduits(new ReferenceProduit
                            {
                                GoutProduit = gout,
                                Format = formatt,
                                Reference = "G" + gout.ID.ToString() + "F" + formatt.ID.ToString(),
                            });

                        }
                    }
                }

                
                db.SaveChanges();
                //      db.Refresh(System.Data.Objects.RefreshMode.StoreWins, prodAModifier);
            }


            // db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Prix);
            Dispose();

        }

        private void txtPrix_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        private void cbGout_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateRefrenceProduit();
        }

        private void GenerateRefrenceProduit()
        {
            if (cbFormat.SelectedIndex <1 || cbGout.SelectedIndex < 1)
            {
                txtPrix.Text = "";
                return;
            }

            txtReference.Text = "G" +
                ((GoutProduit)cbGout.SelectedItem).ID.ToString() + "F" +
                ((Format)(cbFormat.SelectedItem)).ID.ToString();
        }

        private void cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateRefrenceProduit();
        }
    }
}
