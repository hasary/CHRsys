﻿namespace FZBGsys.NsComercial
{
    partial class FrListeCommande
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvCommande = new System.Windows.Forms.DataGridView();
            this.btModifie = new System.Windows.Forms.Button();
            this.btRecherche = new System.Windows.Forms.Button();
            this.btImprimer = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.btBrowseClient = new System.Windows.Forms.Button();
            this.chproduitOrangeChrea = new System.Windows.Forms.CheckBox();
            this.chproduitChrea = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chEtatSolde = new System.Windows.Forms.CheckBox();
            this.chEtatEnInstance = new System.Windows.Forms.CheckBox();
            this.cbDateType = new System.Windows.Forms.ComboBox();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chRechercheAvance = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btAttribution = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommande)).BeginInit();
            this.panRechAvance.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCommande
            // 
            this.dgvCommande.AllowUserToAddRows = false;
            this.dgvCommande.AllowUserToDeleteRows = false;
            this.dgvCommande.AllowUserToResizeColumns = false;
            this.dgvCommande.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCommande.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCommande.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCommande.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCommande.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCommande.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvCommande.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommande.Location = new System.Drawing.Point(294, 17);
            this.dgvCommande.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvCommande.MultiSelect = false;
            this.dgvCommande.Name = "dgvCommande";
            this.dgvCommande.ReadOnly = true;
            this.dgvCommande.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvCommande.RowHeadersVisible = false;
            this.dgvCommande.RowTemplate.Height = 16;
            this.dgvCommande.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCommande.Size = new System.Drawing.Size(803, 437);
            this.dgvCommande.TabIndex = 11;
            // 
            // btModifie
            // 
            this.btModifie.Location = new System.Drawing.Point(4, 5);
            this.btModifie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btModifie.Name = "btModifie";
            this.btModifie.Size = new System.Drawing.Size(103, 34);
            this.btModifie.TabIndex = 66;
            this.btModifie.Text = "Modifier ";
            this.btModifie.UseVisualStyleBackColor = true;
            this.btModifie.Click += new System.EventHandler(this.btModifie_Click);
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(12, 467);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(147, 34);
            this.btRecherche.TabIndex = 67;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // btImprimer
            // 
            this.btImprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btImprimer.Location = new System.Drawing.Point(363, 5);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(140, 34);
            this.btImprimer.TabIndex = 64;
            this.btImprimer.Text = "Imprimer la lsite";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Visible = false;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(113, 5);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(106, 34);
            this.btAnnuler.TabIndex = 65;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(55, 45);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(224, 22);
            this.dtDate.TabIndex = 0;
            // 
            // panRechAvance
            // 
            this.panRechAvance.Controls.Add(this.btBrowseClient);
            this.panRechAvance.Controls.Add(this.chproduitOrangeChrea);
            this.panRechAvance.Controls.Add(this.chproduitChrea);
            this.panRechAvance.Controls.Add(this.label7);
            this.panRechAvance.Controls.Add(this.txtClient);
            this.panRechAvance.Controls.Add(this.label6);
            this.panRechAvance.Controls.Add(this.label5);
            this.panRechAvance.Controls.Add(this.chEtatSolde);
            this.panRechAvance.Controls.Add(this.chEtatEnInstance);
            this.panRechAvance.Controls.Add(this.cbDateType);
            this.panRechAvance.Controls.Add(this.dtTo);
            this.panRechAvance.Controls.Add(this.dtFrom);
            this.panRechAvance.Controls.Add(this.label4);
            this.panRechAvance.Controls.Add(this.label3);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(17, 119);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(271, 334);
            this.panRechAvance.TabIndex = 68;
            // 
            // btBrowseClient
            // 
            this.btBrowseClient.Location = new System.Drawing.Point(217, 154);
            this.btBrowseClient.Name = "btBrowseClient";
            this.btBrowseClient.Size = new System.Drawing.Size(45, 23);
            this.btBrowseClient.TabIndex = 11;
            this.btBrowseClient.Text = "...";
            this.btBrowseClient.UseVisualStyleBackColor = true;
            this.btBrowseClient.Click += new System.EventHandler(this.btBrowseClient_Click);
            // 
            // chproduitOrangeChrea
            // 
            this.chproduitOrangeChrea.AutoSize = true;
            this.chproduitOrangeChrea.Checked = true;
            this.chproduitOrangeChrea.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chproduitOrangeChrea.Location = new System.Drawing.Point(87, 228);
            this.chproduitOrangeChrea.Name = "chproduitOrangeChrea";
            this.chproduitOrangeChrea.Size = new System.Drawing.Size(109, 21);
            this.chproduitOrangeChrea.TabIndex = 10;
            this.chproduitOrangeChrea.Text = "Chréa-Pulpe";
            this.chproduitOrangeChrea.UseVisualStyleBackColor = true;
            // 
            // chproduitChrea
            // 
            this.chproduitChrea.AutoSize = true;
            this.chproduitChrea.Checked = true;
            this.chproduitChrea.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chproduitChrea.Location = new System.Drawing.Point(87, 201);
            this.chproduitChrea.Name = "chproduitChrea";
            this.chproduitChrea.Size = new System.Drawing.Size(68, 21);
            this.chproduitChrea.TabIndex = 10;
            this.chproduitChrea.Text = "Chréa";
            this.chproduitChrea.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "Produit:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(19, 155);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(192, 22);
            this.txtClient.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Client";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Etat:";
            // 
            // chEtatSolde
            // 
            this.chEtatSolde.AutoSize = true;
            this.chEtatSolde.Checked = true;
            this.chEtatSolde.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEtatSolde.Location = new System.Drawing.Point(87, 301);
            this.chEtatSolde.Name = "chEtatSolde";
            this.chEtatSolde.Size = new System.Drawing.Size(74, 21);
            this.chEtatSolde.TabIndex = 5;
            this.chEtatSolde.Text = "Soldée";
            this.chEtatSolde.UseVisualStyleBackColor = true;
            // 
            // chEtatEnInstance
            // 
            this.chEtatEnInstance.AutoSize = true;
            this.chEtatEnInstance.Checked = true;
            this.chEtatEnInstance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEtatEnInstance.Location = new System.Drawing.Point(87, 274);
            this.chEtatEnInstance.Name = "chEtatEnInstance";
            this.chEtatEnInstance.Size = new System.Drawing.Size(104, 21);
            this.chEtatEnInstance.TabIndex = 5;
            this.chEtatEnInstance.Text = "En Instance";
            this.chEtatEnInstance.UseVisualStyleBackColor = true;
            // 
            // cbDateType
            // 
            this.cbDateType.FormattingEnabled = true;
            this.cbDateType.Items.AddRange(new object[] {
            "Enregistrement",
            "Enlevement"});
            this.cbDateType.Location = new System.Drawing.Point(79, 17);
            this.cbDateType.Name = "cbDateType";
            this.cbDateType.Size = new System.Drawing.Size(133, 24);
            this.cbDateType.TabIndex = 4;
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(79, 80);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(133, 22);
            this.dtTo.TabIndex = 3;
            this.dtTo.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(79, 50);
            this.dtFrom.MinDate = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(133, 22);
            this.dtFrom.TabIndex = 3;
            this.dtFrom.Value = new System.DateTime(2013, 1, 1, 0, 0, 0, 0);
            this.dtFrom.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "au";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "du";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "date ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 17);
            this.label1.TabIndex = 69;
            this.label1.Text = "Date Enregistrerement";
            // 
            // chRechercheAvance
            // 
            this.chRechercheAvance.AutoSize = true;
            this.chRechercheAvance.Location = new System.Drawing.Point(17, 92);
            this.chRechercheAvance.Name = "chRechercheAvance";
            this.chRechercheAvance.Size = new System.Drawing.Size(158, 21);
            this.chRechercheAvance.TabIndex = 70;
            this.chRechercheAvance.Text = "Recherche Avancée";
            this.chRechercheAvance.UseVisualStyleBackColor = true;
            this.chRechercheAvance.CheckedChanged += new System.EventHandler(this.chRechercheAvance_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.button1.Location = new System.Drawing.Point(714, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 34);
            this.button1.TabIndex = 71;
            this.button1.Text = "Fermer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btAttribution);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btAnnuler);
            this.panel1.Controls.Add(this.btImprimer);
            this.panel1.Controls.Add(this.btModifie);
            this.panel1.Location = new System.Drawing.Point(286, 467);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(811, 43);
            this.panel1.TabIndex = 72;
            // 
            // btAttribution
            // 
            this.btAttribution.Location = new System.Drawing.Point(225, 6);
            this.btAttribution.Name = "btAttribution";
            this.btAttribution.Size = new System.Drawing.Size(118, 33);
            this.btAttribution.TabIndex = 72;
            this.btAttribution.Text = "Attribution";
            this.btAttribution.UseVisualStyleBackColor = true;
            this.btAttribution.Click += new System.EventHandler(this.btAttribution_Click);
            // 
            // FrListeCommande
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 512);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chRechercheAvance);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.dgvCommande);
            this.Name = "FrListeCommande";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Liste des Commandes";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListeCommande_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommande)).EndInit();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCommande;
        private System.Windows.Forms.Button btModifie;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chRechercheAvance;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chEtatSolde;
        private System.Windows.Forms.CheckBox chEtatEnInstance;
        private System.Windows.Forms.ComboBox cbDateType;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.CheckBox chproduitOrangeChrea;
        private System.Windows.Forms.CheckBox chproduitChrea;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btBrowseClient;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btAttribution;
    }
}