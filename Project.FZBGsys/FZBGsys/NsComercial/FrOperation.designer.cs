﻿namespace FZBGsys.NsComercial
{
    partial class FrOperation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSolde = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.btParcourrirClient = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labMontantRecuOperation = new System.Windows.Forms.Label();
            this.txtMontantReçuOperation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTypeOperation = new System.Windows.Forms.ComboBox();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.panOperation = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNouveauSolde = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panFactureClient = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMontantFacture = new System.Windows.Forms.TextBox();
            this.btAfficher = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.cbDocument = new System.Windows.Forms.ComboBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.cbModePayement = new System.Windows.Forms.ComboBox();
            this.panMode = new System.Windows.Forms.Panel();
            this.panChequePiece = new System.Windows.Forms.Panel();
            this.txtChequePiece = new System.Windows.Forms.TextBox();
            this.labModePayement = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panClient = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panFournisseur = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMontantFactureFournisseur = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtFournisseur = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbProduit = new System.Windows.Forms.ComboBox();
            this.txtFactureFourni = new System.Windows.Forms.TextBox();
            this.txtObservation = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbAssocier = new System.Windows.Forms.ComboBox();
            this.cbchargeusine = new System.Windows.Forms.ComboBox();
            this.cbchargecategorie = new System.Windows.Forms.ComboBox();
            this.panOperation.SuspendLayout();
            this.panFactureClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.panMode.SuspendLayout();
            this.panChequePiece.SuspendLayout();
            this.panClient.SuspendLayout();
            this.panFournisseur.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "Solde:";
            // 
            // txtSolde
            // 
            this.txtSolde.Location = new System.Drawing.Point(111, 37);
            this.txtSolde.Margin = new System.Windows.Forms.Padding(2);
            this.txtSolde.Name = "txtSolde";
            this.txtSolde.ReadOnly = true;
            this.txtSolde.Size = new System.Drawing.Size(117, 20);
            this.txtSolde.TabIndex = 27;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(56, 11);
            this.txtClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(172, 20);
            this.txtClient.TabIndex = 26;
            // 
            // btParcourrirClient
            // 
            this.btParcourrirClient.Location = new System.Drawing.Point(232, 11);
            this.btParcourrirClient.Margin = new System.Windows.Forms.Padding(2);
            this.btParcourrirClient.Name = "btParcourrirClient";
            this.btParcourrirClient.Size = new System.Drawing.Size(32, 20);
            this.btParcourrirClient.TabIndex = 24;
            this.btParcourrirClient.Text = "...";
            this.btParcourrirClient.UseVisualStyleBackColor = true;
            this.btParcourrirClient.Click += new System.EventHandler(this.btParcourrirClient_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(238, 39);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "DA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(225, 5);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "DA";
            // 
            // labMontantRecuOperation
            // 
            this.labMontantRecuOperation.AutoSize = true;
            this.labMontantRecuOperation.Font = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labMontantRecuOperation.Location = new System.Drawing.Point(12, 31);
            this.labMontantRecuOperation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labMontantRecuOperation.Name = "labMontantRecuOperation";
            this.labMontantRecuOperation.Size = new System.Drawing.Size(209, 15);
            this.labMontantRecuOperation.TabIndex = 4;
            this.labMontantRecuOperation.Text = "trente huit milles trois cent quatre vingt dix-neuf ";
            // 
            // txtMontantReçuOperation
            // 
            this.txtMontantReçuOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMontantReçuOperation.Location = new System.Drawing.Point(100, 2);
            this.txtMontantReçuOperation.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantReçuOperation.Name = "txtMontantReçuOperation";
            this.txtMontantReçuOperation.Size = new System.Drawing.Size(116, 19);
            this.txtMontantReçuOperation.TabIndex = 3;
            this.txtMontantReçuOperation.TextChanged += new System.EventHandler(this.txtMontantReçuOperation_TextChanged);
            this.txtMontantReçuOperation.Enter += new System.EventHandler(this.txtMontantReçuOperation_Enter);
            this.txtMontantReçuOperation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontantReçuOperation_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 4);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Montant:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 52);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Type:";
            // 
            // cbTypeOperation
            // 
            this.cbTypeOperation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeOperation.FormattingEnabled = true;
            this.cbTypeOperation.Location = new System.Drawing.Point(68, 50);
            this.cbTypeOperation.Margin = new System.Windows.Forms.Padding(2);
            this.cbTypeOperation.Name = "cbTypeOperation";
            this.cbTypeOperation.Size = new System.Drawing.Size(170, 21);
            this.cbTypeOperation.TabIndex = 0;
            this.cbTypeOperation.SelectedIndexChanged += new System.EventHandler(this.TypeOperationChanged);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(283, 411);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(92, 27);
            this.btEnlever.TabIndex = 6;
            this.btEnlever.Text = "<<Enlevez";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAjouter.Location = new System.Drawing.Point(183, 411);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(95, 27);
            this.btAjouter.TabIndex = 5;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // panOperation
            // 
            this.panOperation.Controls.Add(this.txtMontantReçuOperation);
            this.panOperation.Controls.Add(this.label3);
            this.panOperation.Controls.Add(this.labMontantRecuOperation);
            this.panOperation.Controls.Add(this.label5);
            this.panOperation.Location = new System.Drawing.Point(22, 306);
            this.panOperation.Margin = new System.Windows.Forms.Padding(2);
            this.panOperation.Name = "panOperation";
            this.panOperation.Size = new System.Drawing.Size(257, 53);
            this.panOperation.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 59);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Nouveau Solde:";
            // 
            // txtNouveauSolde
            // 
            this.txtNouveauSolde.Location = new System.Drawing.Point(111, 58);
            this.txtNouveauSolde.Margin = new System.Windows.Forms.Padding(2);
            this.txtNouveauSolde.Name = "txtNouveauSolde";
            this.txtNouveauSolde.ReadOnly = true;
            this.txtNouveauSolde.Size = new System.Drawing.Size(116, 20);
            this.txtNouveauSolde.TabIndex = 27;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(237, 61);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "DA";
            // 
            // panFactureClient
            // 
            this.panFactureClient.Controls.Add(this.label10);
            this.panFactureClient.Controls.Add(this.txtMontantFacture);
            this.panFactureClient.Controls.Add(this.btAfficher);
            this.panFactureClient.Controls.Add(this.label12);
            this.panFactureClient.Controls.Add(this.cbDocument);
            this.panFactureClient.Controls.Add(this.txtNoBon);
            this.panFactureClient.Controls.Add(this.label11);
            this.panFactureClient.Location = new System.Drawing.Point(17, 233);
            this.panFactureClient.Margin = new System.Windows.Forms.Padding(2);
            this.panFactureClient.Name = "panFactureClient";
            this.panFactureClient.Size = new System.Drawing.Size(261, 65);
            this.panFactureClient.TabIndex = 13;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 40);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 36;
            this.label10.Text = "Montant Facture:";
            // 
            // txtMontantFacture
            // 
            this.txtMontantFacture.Location = new System.Drawing.Point(112, 39);
            this.txtMontantFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantFacture.Name = "txtMontantFacture";
            this.txtMontantFacture.ReadOnly = true;
            this.txtMontantFacture.Size = new System.Drawing.Size(114, 20);
            this.txtMontantFacture.TabIndex = 27;
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(230, 6);
            this.btAfficher.Margin = new System.Windows.Forms.Padding(2);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(26, 20);
            this.btAfficher.TabIndex = 35;
            this.btAfficher.Text = ">>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(141, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "N°:";
            // 
            // cbDocument
            // 
            this.cbDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDocument.FormattingEnabled = true;
            this.cbDocument.Items.AddRange(new object[] {
            "Facture",
            "Attribution"});
            this.cbDocument.Location = new System.Drawing.Point(64, 6);
            this.cbDocument.Margin = new System.Windows.Forms.Padding(2);
            this.cbDocument.Name = "cbDocument";
            this.cbDocument.Size = new System.Drawing.Size(72, 21);
            this.cbDocument.TabIndex = 34;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(171, 6);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(56, 20);
            this.txtNoBon.TabIndex = 32;
            this.txtNoBon.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(2, 9);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 13);
            this.label11.TabIndex = 33;
            this.label11.Text = "Document:";
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(68, 10);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(170, 20);
            this.dtDate.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date:";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(284, 14);
            this.dgv.Margin = new System.Windows.Forms.Padding(2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(589, 393);
            this.dgv.TabIndex = 10;
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(777, 410);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(95, 27);
            this.btEnregistrer.TabIndex = 11;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(680, 410);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(92, 27);
            this.btAnnuler.TabIndex = 12;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // cbModePayement
            // 
            this.cbModePayement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbModePayement.FormattingEnabled = true;
            this.cbModePayement.Items.AddRange(new object[] {
            "",
            "Espèces",
            "",
            "Virement",
            "Chèque",
            "Traite"});
            this.cbModePayement.Location = new System.Drawing.Point(104, 6);
            this.cbModePayement.Margin = new System.Windows.Forms.Padding(2);
            this.cbModePayement.Name = "cbModePayement";
            this.cbModePayement.Size = new System.Drawing.Size(115, 21);
            this.cbModePayement.TabIndex = 29;
            this.cbModePayement.SelectedIndexChanged += new System.EventHandler(this.cbModePayement_SelectedIndexChanged);
            // 
            // panMode
            // 
            this.panMode.Controls.Add(this.panChequePiece);
            this.panMode.Controls.Add(this.label9);
            this.panMode.Controls.Add(this.cbModePayement);
            this.panMode.Location = new System.Drawing.Point(20, 78);
            this.panMode.Margin = new System.Windows.Forms.Padding(2);
            this.panMode.Name = "panMode";
            this.panMode.Size = new System.Drawing.Size(230, 64);
            this.panMode.TabIndex = 13;
            // 
            // panChequePiece
            // 
            this.panChequePiece.Controls.Add(this.txtChequePiece);
            this.panChequePiece.Controls.Add(this.labModePayement);
            this.panChequePiece.Location = new System.Drawing.Point(7, 28);
            this.panChequePiece.Margin = new System.Windows.Forms.Padding(2);
            this.panChequePiece.Name = "panChequePiece";
            this.panChequePiece.Size = new System.Drawing.Size(219, 28);
            this.panChequePiece.TabIndex = 33;
            this.panChequePiece.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // txtChequePiece
            // 
            this.txtChequePiece.Location = new System.Drawing.Point(58, 7);
            this.txtChequePiece.Margin = new System.Windows.Forms.Padding(2);
            this.txtChequePiece.Name = "txtChequePiece";
            this.txtChequePiece.Size = new System.Drawing.Size(153, 20);
            this.txtChequePiece.TabIndex = 32;
            this.txtChequePiece.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // labModePayement
            // 
            this.labModePayement.AutoSize = true;
            this.labModePayement.Location = new System.Drawing.Point(8, 7);
            this.labModePayement.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labModePayement.Name = "labModePayement";
            this.labModePayement.Size = new System.Drawing.Size(47, 13);
            this.labModePayement.TabIndex = 31;
            this.labModePayement.Text = "Chèque:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 8);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Mode payement:";
            // 
            // panClient
            // 
            this.panClient.Controls.Add(this.btParcourrirClient);
            this.panClient.Controls.Add(this.label13);
            this.panClient.Controls.Add(this.label7);
            this.panClient.Controls.Add(this.label1);
            this.panClient.Controls.Add(this.label8);
            this.panClient.Controls.Add(this.txtClient);
            this.panClient.Controls.Add(this.label6);
            this.panClient.Controls.Add(this.txtNouveauSolde);
            this.panClient.Controls.Add(this.txtSolde);
            this.panClient.Location = new System.Drawing.Point(12, 145);
            this.panClient.Margin = new System.Windows.Forms.Padding(2);
            this.panClient.Name = "panClient";
            this.panClient.Size = new System.Drawing.Size(266, 83);
            this.panClient.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 13);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Client:";
            // 
            // panFournisseur
            // 
            this.panFournisseur.Controls.Add(this.label18);
            this.panFournisseur.Controls.Add(this.label17);
            this.panFournisseur.Controls.Add(this.label15);
            this.panFournisseur.Controls.Add(this.txtMontantFactureFournisseur);
            this.panFournisseur.Controls.Add(this.label14);
            this.panFournisseur.Controls.Add(this.txtFournisseur);
            this.panFournisseur.Controls.Add(this.label16);
            this.panFournisseur.Controls.Add(this.cbProduit);
            this.panFournisseur.Controls.Add(this.txtFactureFourni);
            this.panFournisseur.Location = new System.Drawing.Point(14, 145);
            this.panFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.panFournisseur.Name = "panFournisseur";
            this.panFournisseur.Size = new System.Drawing.Size(250, 141);
            this.panFournisseur.TabIndex = 30;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(228, 77);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(22, 13);
            this.label18.TabIndex = 37;
            this.label18.Text = "DA";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 77);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 36;
            this.label17.Text = "Montant Facture:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 29);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Produit:";
            // 
            // txtMontantFactureFournisseur
            // 
            this.txtMontantFactureFournisseur.Location = new System.Drawing.Point(106, 75);
            this.txtMontantFactureFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantFactureFournisseur.Name = "txtMontantFactureFournisseur";
            this.txtMontantFactureFournisseur.Size = new System.Drawing.Size(114, 20);
            this.txtMontantFactureFournisseur.TabIndex = 27;
            this.txtMontantFactureFournisseur.TextChanged += new System.EventHandler(this.cbMontantFactureFournisseur_TextChanged);
            this.txtMontantFactureFournisseur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMontantReçuOperation_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Fournisseur:";
            // 
            // txtFournisseur
            // 
            this.txtFournisseur.Location = new System.Drawing.Point(77, 3);
            this.txtFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtFournisseur.Name = "txtFournisseur";
            this.txtFournisseur.Size = new System.Drawing.Size(146, 20);
            this.txtFournisseur.TabIndex = 26;
            this.txtFournisseur.Leave += new System.EventHandler(this.txtFournisseur_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 55);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "N° Facture:";
            // 
            // cbProduit
            // 
            this.cbProduit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProduit.FormattingEnabled = true;
            this.cbProduit.Location = new System.Drawing.Point(77, 27);
            this.cbProduit.Margin = new System.Windows.Forms.Padding(2);
            this.cbProduit.Name = "cbProduit";
            this.cbProduit.Size = new System.Drawing.Size(146, 21);
            this.cbProduit.TabIndex = 0;
            this.cbProduit.SelectedIndexChanged += new System.EventHandler(this.TypeOperationChanged);
            // 
            // txtFactureFourni
            // 
            this.txtFactureFourni.Location = new System.Drawing.Point(106, 51);
            this.txtFactureFourni.Margin = new System.Windows.Forms.Padding(2);
            this.txtFactureFourni.Name = "txtFactureFourni";
            this.txtFactureFourni.Size = new System.Drawing.Size(56, 20);
            this.txtFactureFourni.TabIndex = 32;
            this.txtFactureFourni.Enter += new System.EventHandler(this.txtNoBon_Enter);
            // 
            // txtObservation
            // 
            this.txtObservation.Location = new System.Drawing.Point(96, 366);
            this.txtObservation.Margin = new System.Windows.Forms.Padding(2);
            this.txtObservation.Multiline = true;
            this.txtObservation.Name = "txtObservation";
            this.txtObservation.Size = new System.Drawing.Size(179, 41);
            this.txtObservation.TabIndex = 31;
            this.txtObservation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObservation_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 369);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Observation";
            // 
            // cbAssocier
            // 
            this.cbAssocier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAssocier.FormattingEnabled = true;
            this.cbAssocier.Items.AddRange(new object[] {
            "Associer:",
            "NMC",
            "Z.Med",
            "Z.Ndine",
            "Z.Samy",
            "Z.ElHadja",
            "Z.Med&Z.Ndine"});
            this.cbAssocier.Location = new System.Drawing.Point(147, 109);
            this.cbAssocier.Margin = new System.Windows.Forms.Padding(2);
            this.cbAssocier.Name = "cbAssocier";
            this.cbAssocier.Size = new System.Drawing.Size(92, 21);
            this.cbAssocier.TabIndex = 33;
            // 
            // cbchargeusine
            // 
            this.cbchargeusine.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbchargeusine.FormattingEnabled = true;
            this.cbchargeusine.Items.AddRange(new object[] {
            "Type:",
            "Achat consommable",
            "Service"});
            this.cbchargeusine.Location = new System.Drawing.Point(112, 109);
            this.cbchargeusine.Margin = new System.Windows.Forms.Padding(2);
            this.cbchargeusine.Name = "cbchargeusine";
            this.cbchargeusine.Size = new System.Drawing.Size(127, 21);
            this.cbchargeusine.TabIndex = 34;
            // 
            // cbchargecategorie
            // 
            this.cbchargecategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbchargecategorie.FormattingEnabled = true;
            this.cbchargecategorie.Items.AddRange(new object[] {
            "catégorie:",
            "PET",
            "Canette",
            "Flote",
            "Administration",
            "Commercial",
            "Magasin",
            "Laboratoire",
            "Logistique",
            "Frais de mission",
            "Autre"});
            this.cbchargecategorie.Location = new System.Drawing.Point(112, 139);
            this.cbchargecategorie.Margin = new System.Windows.Forms.Padding(2);
            this.cbchargecategorie.Name = "cbchargecategorie";
            this.cbchargecategorie.Size = new System.Drawing.Size(127, 21);
            this.cbchargecategorie.TabIndex = 34;
            // 
            // FrOperation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 452);
            this.Controls.Add(this.cbAssocier);
            this.Controls.Add(this.cbchargeusine);
            this.Controls.Add(this.cbchargecategorie);
            this.Controls.Add(this.panFournisseur);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtObservation);
            this.Controls.Add(this.panClient);
            this.Controls.Add(this.panOperation);
            this.Controls.Add(this.panMode);
            this.Controls.Add(this.panFactureClient);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.cbTypeOperation);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrOperation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mouvement Caisse";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrOperation_FormClosing);
            this.Load += new System.EventHandler(this.FrOperation_Load);
            this.panOperation.ResumeLayout(false);
            this.panOperation.PerformLayout();
            this.panFactureClient.ResumeLayout(false);
            this.panFactureClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.panMode.ResumeLayout(false);
            this.panMode.PerformLayout();
            this.panChequePiece.ResumeLayout(false);
            this.panChequePiece.PerformLayout();
            this.panClient.ResumeLayout(false);
            this.panClient.PerformLayout();
            this.panFournisseur.ResumeLayout(false);
            this.panFournisseur.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.Button btParcourrirClient;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSolde;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labMontantRecuOperation;
        private System.Windows.Forms.TextBox txtMontantReçuOperation;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTypeOperation;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNouveauSolde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMontantFacture;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.ComboBox cbDocument;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Panel panFactureClient;
        private System.Windows.Forms.Panel panOperation;
        private System.Windows.Forms.Panel panMode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbModePayement;
        private System.Windows.Forms.Panel panClient;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panFournisseur;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtFournisseur;
        private System.Windows.Forms.ComboBox cbProduit;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtMontantFactureFournisseur;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFactureFourni;
        private System.Windows.Forms.TextBox txtObservation;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panChequePiece;
        private System.Windows.Forms.TextBox txtChequePiece;
        private System.Windows.Forms.Label labModePayement;
        private System.Windows.Forms.ComboBox cbAssocier;
        private System.Windows.Forms.ComboBox cbchargeusine;
        private System.Windows.Forms.ComboBox cbchargecategorie;
    }
}