
﻿using FZBGsys.NsTools;
using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsComercial
{
    public partial class FrFactureRetour : Form
    {
        ModelEntities db = new ModelEntities();
        List<Vente> ListVente = new List<Vente>();
        public FrFactureRetour(string BonRetourID)
        {
            InitializeComponent();
            txtNoBon.Text = BonRetourID;
            txtNoBon.ReadOnly = true;
            LoadBonRetour(BonRetourID.ParseToInt().Value);
        }

        public FrFactureRetour(int? id = null, bool MustSave = false)
        {
            InitializeComponent();
            if (id != null)
            {
                IsEditFacture = true;
                this.FactureRetour = db.FactureRetours.Single(p => p.ID == id);
                InputFacture();
            }
            else
            {
                IsEditFacture = false;
                txtNoBon.Select();
            }
            this.MustSave = MustSave;
            if (MustSave)
            {
                txtNoBon.ReadOnly = true;
                btAnnuler.Enabled = false;
                this.ControlBox = false;
            }
        }

        private void InputFacture()
        {
            txtNoBon.Text = FactureRetour.BonRetourID.Value.ToString();
            LoadBonRetour(FactureRetour.BonRetourID.Value);
            if (chRemise.Enabled && FactureRetour.MontantRemiseUnite != null)
            {
                chRemise.Checked = true;
                txtRemise.Text = FactureRetour.MontantRemiseUnite.ToString();
                chRemise.Enabled = true;
            }

            cbMode.SelectedIndex = FactureRetour.ModePayement.Value;

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FrFacture_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            panCheque.Visible = cbMode.SelectedIndex == (int)EnModePayement.Chèque;
            panTimbre.Visible = cbMode.SelectedIndex == (int)EnModePayement.Especes;
            UpdateTotals();


        }



        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            txtRemise.Text = "";
            panRemise.Visible = chRemise.Checked;
        }

        private void txtNoBon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (IsEditFacture)
            {
                FactureRetour.Client.Solde -= FactureRetour.MontantTTC;

            }


            if (!IsValidAll())
            {
                return;
            }

            if (!this.ConfirmInformation("Confirmer Facturation du Bon de Retour N° " + BonRetour.ID + " ?"))
            {
                return;
            }

            if (this.FactureRetour == null)
            {
                this.FactureRetour = new FactureRetour() { BonRetour = this.BonRetour };
            }
            this.Saved = true;
            FactureRetour.Client = BonRetour.Client;
            FactureRetour.Date = BonRetour.Date;
            
            FactureRetour.ModePayement = (int)EnModePayement.Avance;
            FactureRetour.MontantHT = ht;
            FactureRetour.MontantRemiseUnite = txtRemise.Text.ParseToDec();
            FactureRetour.MontantTimbre = timbre;
            FactureRetour.MontantTotalFacture = totalFact;
            FactureRetour.MontantTotalRemise = totalRemise;
            FactureRetour.MontantTTC = ttc;
            FactureRetour.MontantTVA = tva;

            
            var current = Tools.GetServerDateTime();
            if (FactureRetour.EntityState == EntityState.Added)
            {
                FactureRetour.InscriptionTime = current;

                FactureRetour.InscriptionUID = Tools.CurrentUserID;
            }
            else
            {
                FactureRetour.ModificationTime = current;
            }

            /* var autoriseMontant = FactureRetour.Client.MaxCreditMontant + FactureRetour.Client.Solde;

             if (FactureRetour.Client.Solde == null)
             {
                 FactureRetour.Client.Solde = 0;
                 FactureRetour.Client.IsSoldable = true;
             }
             */


            FactureRetour.Client.Solde += FactureRetour.MontantTotalFacture;
            FactureRetour.Client.IsSoldable = true;
            FactureRetour.Etat = (int)EnEtatFacture.Payée;

            /*
                            case EnModePayement.Virement:
                                FactureRetour.Client.Solde -= FactureRetour.MontantTotalFacture;
                                FactureRetour.Etat = (int)EnEtatFacture.Payée;
                                break;

                            case EnModePayement.Chèque:
                                FactureRetour.Client.Solde -= FactureRetour.MontantTotalFacture;
                                FactureRetour.Etat = (int)EnEtatFacture.Payée;
                                break;

                            case EnModePayement.Traite:
                                FactureRetour.Client.Solde -= FactureRetour.MontantTotalFacture;
                                break;

                            case EnModePayement.a_Terme:
                                if (FactureRetour.Client.IsAutoriseCredit == false)
                                {
                                    this.ShowWarning("Ce client n'est pas autorisé à facturé à tèrme, Veillez choisir un autre mode de payement");
                                    return;
                                }

                                if (FactureRetour.Client.Solde - FactureRetour.MontantTTC + FactureRetour.Client.MaxCreditMontant <= 0)
                                {
                                    if (this.ConfirmInformation("Montant dépasse la plafond autorisé.\n voulez-vous forcer la facturation?"))
                                    {
                                        var connectedID = Tools.ConnectUserID();
                                        if (connectedID == null)
                                        {
                                            return;
                                        }
                                        var user = db.ApplicationUtilisateurs.Single(p => p.ID == connectedID);


                                        if (!user.ApplicationGroupe.IsBoss.Value)
                                        {
                                            this.ShowWarning("Ce Utilisateur n'est pas autorisé à forcer la facturation");
                                            return;
                                        }

                                    }
                                    // Facture.Client.IsCompteBloque = true;



                                }
                                FactureRetour.Etat = (int)EnEtatFacture.Payée;
                                var ancienSolde = FactureRetour.Client.Solde;
                                FactureRetour.Client.Solde -= FactureRetour.MontantTotalFacture;
                                //   Facture.Client.IsSoldable = true;

                                if (FactureRetour.Client.Solde * FactureRetour.Client.Solde < 0)
                                {
                                    FactureRetour.Client.DatePassageDebiteur = Tools.GetServerDateTime();
                                }




                                break;*/


            try
            {

                db.SaveChanges();
                NsAdministration.FrMaintenance.GenerateSoldeClient(db, null, true, FactureRetour.Client);
                btEnregistrer.Enabled = false;
                var totalTxt = CtoL.convertMontant(FactureRetour.MontantTotalFacture.Value);
                var remiseTxt = (FactureRetour.MontantTotalRemise == 0) ? "" : "(remise comprise)";

                FZBGsys.NsReports.Comercial.ComercialReportController.PrintFactureRetour(FactureRetour.ID, totalTxt, remiseTxt);

                /* if (chSuivre.Checked)
                 {
                     new FrBonLivraison(FactureRetour.BonRetourID.ToString()).ShowDialog();
                 }
                 */
                Dispose();
            }
            catch (Exception en)
            {

                this.ShowError(en.AllMessages("Impossible d'enregistrer"));
            }
        }

        private bool IsValidAll()
        {
            string message = "";
            if (cbMode.SelectedIndex == 0 || cbMode.SelectedIndex == -1)
            {
               // message = "Selectionner Mode payement.";
            }
            if (chRemise.Checked && txtRemise.Text.ParseToDec() == null)
            {
                message += "\nMontant remise invalide";
            }

            if (panCheque.Visible && (txtCheque.Text.Trim() == "" || dtDateCheque.Value.Date > DateTime.Now))
            {
                message += "\nInformations chèques invalides";
            }

            if (message == "")
            {
                return true;
            }

            this.ShowWarning(message);
            return false;
        }

        private void btAfficher_Click(object sender, EventArgs e)
        {

            var id = txtNoBon.Text.ParseToInt();

            if (id != null)
            {
                LoadBonRetour(id.Value);
            }
            else
            {
                this.ShowWarning("Numéro Bon Invalide");
            }

        }

        private void LoadBonRetour(int id)
        {
            this.BonRetour = db.BonRetours.SingleOrDefault(p => p.ID == id);
            if (BonRetour == null)
            {
                this.ShowWarning("Numéro Bon introuvable");
                return;
            }


            if (!IsEditFacture && BonRetour.FactureRetours.Count != 0)
            {
                var factureRetour = BonRetour.FactureRetours.First();
                if (this.ConfirmInformation("Ce Bon de Retour est déja Facturé (facture de retour N° " + factureRetour.ID + ")\nVoulez vous imprimer la facture de retour?"))
                {
                    var totalTxt = CtoL.convertMontant(factureRetour.MontantTotalFacture.Value);
                    var remiseTxt = (factureRetour.MontantTotalRemise == 0) ? "" : "(remise comprise)";

                    FZBGsys.NsReports.Comercial.ComercialReportController.PrintFactureRetour(factureRetour.ID, totalTxt, remiseTxt);

                    return;
                }
                return;

            }
            /* else if (BonRetour.Etat != (int)EnEtatBonRetour.Livré)
             {
                 this.ShowWarning("Ce Bon d'attribution est " + BonRetour.Etat.ToEtatBonRetourTxt());
                 return;
             }
             */
            gbpayement.Enabled = true;
            txtClient.Text = BonRetour.Client.NomClientAndParent();

            if (BonRetour.Client.IsSoldable == true)
            {
                //txtSolde.Text = BonRetour.Client.Solde.ToAffInt();
                txtDateBon.Text = BonRetour.Date.Value.ToShortDateString();
                var client = BonRetour.Client;
             /*   if (client.IsCompteBloque == true)
                {
                    this.ShowInformation("Ce client n'est pas autorisé à facturer avant de règler sa situation anterieure.");
                    gbpayement.Enabled = false;
                }
                else
                {
                    gbpayement.Enabled = true;


                }*/

            }
            else
            {
                //panSolde.Visible = false;
                gbpayement.Enabled = true;
            }

            ListVente.Clear();
            ListVente.AddRange(BonRetour.Ventes.ToList());


            chRemise.Enabled = false;// BonRetour.Client.TypeClient.isRemisable.Value;
            chRemise.Checked = false;

            this.Montant = 0;
            foreach (var vente in ListVente)
            {
                var prix = vente.GoutProduit.PrixProduits.Single(
                    p => p.FormatID == vente.FormatID &&
                         p.TypeClient == BonRetour.Client.ClientTypeID
                    );

                Montant += prix.PrixUnitaire * vente.TotalBouteilles.Value;

                if (prix.IsRemisable == true)
                {
                    chRemise.Enabled = BonRetour.Client.TypeClient.isRemisable.Value;
                    chRemise.Checked = BonRetour.Client.TypeClient.isRemisable.Value;
                    //break;
                }
            }




            UpdateTotals();
            db.SaveChanges();
            txtNetPayer.Text = this.Montant.ToAffInt();
            txtMontant.Text = this.Montant.ToAffInt();


            RefreshGrid();

        }


        private void RefreshGrid()
        {
            dgv.DataSource = ListVente.Select(p => new
            {
                ID = p.ID,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                REF = p.Unite.ToUniteProduitTxt(),
                Palettes = p.NombrePalettes,
                Pochettes = p.NombreFardeaux,
                Bouteilles = p.TotalBouteilles,

            }).ToList();


            dgv.HideIDColumn();
        }


        private BonRetour BonRetour { get; set; }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtRemise_TextChanged(object sender, EventArgs e)
        {
            UpdateTotals();
        }

        decimal remise = 0;
        decimal totalRemise = 0;
        decimal timbre = 0;
        decimal ht = 0;
        decimal ttc = 0;
        decimal tva = 0;
        decimal totalFact = 0;


        private void UpdateTotals()
        {
            var selectedModeID = cbMode.SelectedIndex;

            remise = 0;
            totalRemise = 0;
            timbre = 0;
            ht = 0;
            ttc = 0;
            tva = 0;
            totalFact = 0;

            if (panRemise.Visible)
            {

                if (txtRemise.Text.ParseToDec() == null)
                {
                    remise = 0;
                }
                else
                {
                    remise = txtRemise.Text.ParseToDec().Value;
                }
            }

            foreach (var vente in BonRetour.Ventes)
            {
                var prix = vente.GoutProduit.PrixProduits.Single(p => p.FormatID == vente.FormatID &&
                    p.TypeClient == BonRetour.Client.ClientTypeID);

                if (prix.IsRemisable.Value)
                {
                    vente.isRemise = true;
                    vente.PrixUnitaire = prix.PrixUnitaire - remise;
                    totalRemise += remise * vente.TotalBouteilles.Value;
                }
                else
                {
                    vente.isRemise = false;
                    vente.PrixUnitaire = prix.PrixUnitaire;
                }
            }

            ht = (Montant.Value) * 100 / 117;
            var remiseHT = totalRemise * 100 / 117;
            ht = ht - remiseHT;
            tva = ht * 17 / 100;
            ttc = tva + ht;

            totalFact = ttc;
            if (panTimbre.Visible)
            {
                timbre = ht / 100;
                if (timbre > 2500)
                {
                    timbre = 2500;
                }

                txtTimbre.Text = timbre.ToString();
                timbre = txtTimbre.Text.ParseToDec().Value;
                totalFact += timbre;
            }

            txtMontant.Text = ttc.ToAffInt();
            txtNetPayer.Text = totalFact.ToAffInt();
            txtTimbre.Text = timbre.ToAffInt();


        }

        public bool IsEditFacture { get; set; }

        public FactureRetour FactureRetour { get; set; }

        private void txtRemise_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if (e.KeyChar == '.')
            {
                e.KeyChar = ',';
            }
        }

        private void txtNoBon_Leave(object sender, EventArgs e)
        {
            this.AcceptButton = null;
        }

        private void txtNoBon_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btAfficher;
        }

        private void chSuivre_CheckedChanged(object sender, EventArgs e)
        {

        }

        public decimal? Montant { get; set; }

        public bool MustSave { get; set; }

        private void FrFacture_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        public bool Saved { get; set; }

        private void gbpayement_Enter(object sender, EventArgs e)
        {

        }
    }
}
