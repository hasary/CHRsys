﻿namespace FZBGsys.NsComercial
{
    partial class FrFactureRetour
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDateBon = new System.Windows.Forms.TextBox();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btAfficher = new System.Windows.Forms.Button();
            this.gbpayement = new System.Windows.Forms.GroupBox();
            this.panCheque = new System.Windows.Forms.Panel();
            this.dtDateCheque = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCheque = new System.Windows.Forms.TextBox();
            this.panTimbre = new System.Windows.Forms.Panel();
            this.txtTimbre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.chRemise = new System.Windows.Forms.CheckBox();
            this.cbMode = new System.Windows.Forms.ComboBox();
            this.panRemise = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRemise = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNetPayer = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMontant = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbpayement.SuspendLayout();
            this.panCheque.SuspendLayout();
            this.panTimbre.SuspendLayout();
            this.panRemise.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(333, 14);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(660, 354);
            this.dgv.TabIndex = 10;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(692, 377);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(301, 43);
            this.xpannel1.TabIndex = 13;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(36, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(164, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDateBon);
            this.groupBox1.Controls.Add(this.txtClient);
            this.groupBox1.Controls.Add(this.txtNoBon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btAfficher);
            this.groupBox1.Location = new System.Drawing.Point(13, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(311, 130);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Bon de Retour";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 64);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Client:";
            // 
            // txtDateBon
            // 
            this.txtDateBon.Location = new System.Drawing.Point(64, 60);
            this.txtDateBon.Margin = new System.Windows.Forms.Padding(4);
            this.txtDateBon.Name = "txtDateBon";
            this.txtDateBon.ReadOnly = true;
            this.txtDateBon.Size = new System.Drawing.Size(128, 22);
            this.txtDateBon.TabIndex = 2;
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(64, 90);
            this.txtClient.Margin = new System.Windows.Forms.Padding(4);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(237, 22);
            this.txtClient.TabIndex = 2;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(64, 30);
            this.txtNoBon.Margin = new System.Windows.Forms.Padding(4);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(128, 22);
            this.txtNoBon.TabIndex = 2;
            this.txtNoBon.Enter += new System.EventHandler(this.txtNoBon_Enter);
            this.txtNoBon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoBon_KeyPress);
            this.txtNoBon.Leave += new System.EventHandler(this.txtNoBon_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "N°";
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(200, 28);
            this.btAfficher.Margin = new System.Windows.Forms.Padding(4);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(103, 28);
            this.btAfficher.TabIndex = 0;
            this.btAfficher.Text = "Afficher >>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // gbpayement
            // 
            this.gbpayement.Controls.Add(this.panCheque);
            this.gbpayement.Controls.Add(this.panTimbre);
            this.gbpayement.Controls.Add(this.label11);
            this.gbpayement.Controls.Add(this.label12);
            this.gbpayement.Controls.Add(this.chRemise);
            this.gbpayement.Controls.Add(this.cbMode);
            this.gbpayement.Controls.Add(this.panRemise);
            this.gbpayement.Controls.Add(this.label8);
            this.gbpayement.Controls.Add(this.txtNetPayer);
            this.gbpayement.Controls.Add(this.label9);
            this.gbpayement.Controls.Add(this.txtMontant);
            this.gbpayement.Controls.Add(this.label4);
            this.gbpayement.Enabled = false;
            this.gbpayement.Location = new System.Drawing.Point(13, 152);
            this.gbpayement.Margin = new System.Windows.Forms.Padding(4);
            this.gbpayement.Name = "gbpayement";
            this.gbpayement.Padding = new System.Windows.Forms.Padding(4);
            this.gbpayement.Size = new System.Drawing.Size(308, 239);
            this.gbpayement.TabIndex = 15;
            this.gbpayement.TabStop = false;
            this.gbpayement.Text = "Rembourcement";
            this.gbpayement.Enter += new System.EventHandler(this.gbpayement_Enter);
            // 
            // panCheque
            // 
            this.panCheque.Controls.Add(this.dtDateCheque);
            this.panCheque.Controls.Add(this.label14);
            this.panCheque.Controls.Add(this.label13);
            this.panCheque.Controls.Add(this.txtCheque);
            this.panCheque.Location = new System.Drawing.Point(15, 129);
            this.panCheque.Margin = new System.Windows.Forms.Padding(4);
            this.panCheque.Name = "panCheque";
            this.panCheque.Size = new System.Drawing.Size(275, 68);
            this.panCheque.TabIndex = 20;
            this.panCheque.Visible = false;
            // 
            // dtDateCheque
            // 
            this.dtDateCheque.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateCheque.Location = new System.Drawing.Point(107, 34);
            this.dtDateCheque.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateCheque.Name = "dtDateCheque";
            this.dtDateCheque.Size = new System.Drawing.Size(164, 22);
            this.dtDateCheque.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(45, 39);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 17);
            this.label14.TabIndex = 3;
            this.label14.Text = "Date:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "N° Chèque:";
            // 
            // txtCheque
            // 
            this.txtCheque.Location = new System.Drawing.Point(104, 4);
            this.txtCheque.Margin = new System.Windows.Forms.Padding(4);
            this.txtCheque.Name = "txtCheque";
            this.txtCheque.Size = new System.Drawing.Size(167, 22);
            this.txtCheque.TabIndex = 2;
            // 
            // panTimbre
            // 
            this.panTimbre.Controls.Add(this.txtTimbre);
            this.panTimbre.Controls.Add(this.label7);
            this.panTimbre.Controls.Add(this.label10);
            this.panTimbre.Location = new System.Drawing.Point(12, 134);
            this.panTimbre.Margin = new System.Windows.Forms.Padding(4);
            this.panTimbre.Name = "panTimbre";
            this.panTimbre.Size = new System.Drawing.Size(277, 31);
            this.panTimbre.TabIndex = 16;
            this.panTimbre.Visible = false;
            // 
            // txtTimbre
            // 
            this.txtTimbre.Location = new System.Drawing.Point(107, 2);
            this.txtTimbre.Margin = new System.Windows.Forms.Padding(4);
            this.txtTimbre.Name = "txtTimbre";
            this.txtTimbre.ReadOnly = true;
            this.txtTimbre.Size = new System.Drawing.Size(127, 22);
            this.txtTimbre.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 6);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Timbre:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(243, 6);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 17);
            this.label10.TabIndex = 19;
            this.label10.Text = "DA";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(260, 204);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 17);
            this.label11.TabIndex = 19;
            this.label11.Text = "DA";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(260, 30);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 17);
            this.label12.TabIndex = 19;
            this.label12.Text = "DA";
            // 
            // chRemise
            // 
            this.chRemise.AutoSize = true;
            this.chRemise.Checked = true;
            this.chRemise.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chRemise.Location = new System.Drawing.Point(20, 66);
            this.chRemise.Margin = new System.Windows.Forms.Padding(4);
            this.chRemise.Name = "chRemise";
            this.chRemise.Size = new System.Drawing.Size(81, 21);
            this.chRemise.TabIndex = 4;
            this.chRemise.Text = "Remise:";
            this.chRemise.UseVisualStyleBackColor = true;
            this.chRemise.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cbMode
            // 
            this.cbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMode.FormattingEnabled = true;
            this.cbMode.Items.AddRange(new object[] {
            "",
            "Espèces",
            "Avance"});
            this.cbMode.Location = new System.Drawing.Point(171, 101);
            this.cbMode.Margin = new System.Windows.Forms.Padding(4);
            this.cbMode.Name = "cbMode";
            this.cbMode.Size = new System.Drawing.Size(117, 24);
            this.cbMode.TabIndex = 18;
            this.cbMode.Visible = false;
            this.cbMode.SelectedIndexChanged += new System.EventHandler(this.cbMode_SelectedIndexChanged);
            // 
            // panRemise
            // 
            this.panRemise.Controls.Add(this.label6);
            this.panRemise.Controls.Add(this.txtRemise);
            this.panRemise.Location = new System.Drawing.Point(111, 60);
            this.panRemise.Margin = new System.Windows.Forms.Padding(4);
            this.panRemise.Name = "panRemise";
            this.panRemise.Size = new System.Drawing.Size(179, 37);
            this.panRemise.TabIndex = 16;
            this.panRemise.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(100, 7);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "DA / Unité";
            // 
            // txtRemise
            // 
            this.txtRemise.Location = new System.Drawing.Point(11, 4);
            this.txtRemise.Margin = new System.Windows.Forms.Padding(4);
            this.txtRemise.Name = "txtRemise";
            this.txtRemise.Size = new System.Drawing.Size(80, 22);
            this.txtRemise.TabIndex = 2;
            this.txtRemise.TextChanged += new System.EventHandler(this.txtRemise_TextChanged);
            this.txtRemise.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRemise_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 104);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(155, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Mode Rembourcement:";
            this.label8.Visible = false;
            // 
            // txtNetPayer
            // 
            this.txtNetPayer.Location = new System.Drawing.Point(152, 201);
            this.txtNetPayer.Margin = new System.Windows.Forms.Padding(4);
            this.txtNetPayer.Name = "txtNetPayer";
            this.txtNetPayer.ReadOnly = true;
            this.txtNetPayer.Size = new System.Drawing.Size(100, 22);
            this.txtNetPayer.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 204);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Net Rembourssé:";
            // 
            // txtMontant
            // 
            this.txtMontant.Location = new System.Drawing.Point(121, 29);
            this.txtMontant.Margin = new System.Windows.Forms.Padding(4);
            this.txtMontant.Name = "txtMontant";
            this.txtMontant.ReadOnly = true;
            this.txtMontant.Size = new System.Drawing.Size(131, 22);
            this.txtMontant.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 33);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Montant:";
            // 
            // FrFactureRetour
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 433);
            this.Controls.Add(this.gbpayement);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrFactureRetour";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Facture de Retour";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrFacture_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrFacture_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbpayement.ResumeLayout(false);
            this.gbpayement.PerformLayout();
            this.panCheque.ResumeLayout(false);
            this.panCheque.PerformLayout();
            this.panTimbre.ResumeLayout(false);
            this.panTimbre.PerformLayout();
            this.panRemise.ResumeLayout(false);
            this.panRemise.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.GroupBox gbpayement;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbMode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panRemise;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRemise;
        private System.Windows.Forms.CheckBox chRemise;
        private System.Windows.Forms.TextBox txtNetPayer;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTimbre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMontant;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panCheque;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCheque;
        private System.Windows.Forms.Panel panTimbre;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDateBon;
        private System.Windows.Forms.DateTimePicker dtDateCheque;
    }
}