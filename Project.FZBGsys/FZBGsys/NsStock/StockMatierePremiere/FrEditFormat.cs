﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrEditFormat : Form
    {
        private int? IDFormat;
       // private Format M;


        public FrEditFormat()
        {
            InitializeComponent();
        }

        public FrEditFormat(int IDFormat)
        {
            InitializeComponent();
            using (var db = new ModelEntities())
            {
                this.IDFormat = IDFormat;
               var  M = db.Formats.Single(p => p.ID == IDFormat);
                txtvolme.Text = M.Volume;
                txtBF.Text = M.BouteillesParFardeau.ToString();
                txtFP.Text = M.FardeauParPalette.ToString();
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                if (IDFormat == null)
                {
                   var M = new Format();
                    M.Volume = txtvolme.Text;
                    M.BouteillesParFardeau = int.Parse(txtBF.Text);
                    M.FardeauParPalette = int.Parse(txtFP.Text);
                    db.Formats.AddObject(M);
                  
                }
                else
                {
                    var M = db.Formats.Single(p => p.ID == IDFormat);
                    M.Volume = txtvolme.Text;
                    M.BouteillesParFardeau = int.Parse(txtBF.Text);
                    M.FardeauParPalette = int.Parse(txtFP.Text);
                               


                }

                db.SaveChanges();

            }
            Dispose();
        }

        private void btannule_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }


    }
}
