﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrNEArrome : Form
    {
        ModelEntities db = new ModelEntities();
        public TypeArrome Model { get; set; }
        public FrNEArrome(int? id = null)
        {
            InitializeComponent();
            initialiseControle();
            if (id != null)
            {
                LoadModel(id);
            }
        }

        private void LoadModel(int? id)
        {
            Model = db.TypeArromes.Single(p => p.ID == id);
            txtNom.Text = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Model.Nom.ToLower());
           

           
        }

        private void initialiseControle()
        {

           
            //---------------------------------------------------------------------------
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrNEProduit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private bool IsValid() {
            if (txtNom.Text.Length <2)
            {
                Tools.ShowError("Nom Invalide");
                return false;
            }

            
                var nom = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtNom.Text.ToLower());

                var exist = db.TypeArromes.Where(p => p.Nom == nom);
                if (Model != null)
                {
                    exist = exist.Where(p => p.Nom != Model.Nom);
                }

                if (exist.Count() != 0)
                {
                    Tools.ShowError("Nom Existe déja");
                    return false; 
                }

                
            return true;
        
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!IsValid())
            {
                return;
            }

            if (Model == null)
            {
                Model = new TypeArrome();
                
                db.AddToTypeArromes(Model);
               
            }

            Model.Nom = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtNom.Text.ToLower());
            
            db.SaveChanges();
            Dispose();
        }

        private void txtConge_KeyPress(object sender, KeyPressEventArgs e)
        {


            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }

        }
    }
}
