﻿namespace FZBGsys.NsStock.StockMatierePremiere
{
    partial class FrEditFormat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtvolme = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFP = new System.Windows.Forms.TextBox();
            this.txtBF = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btannule = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtvolme
            // 
            this.txtvolme.Location = new System.Drawing.Point(176, 33);
            this.txtvolme.Name = "txtvolme";
            this.txtvolme.Size = new System.Drawing.Size(100, 22);
            this.txtvolme.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "nom";
            // 
            // txtFP
            // 
            this.txtFP.Location = new System.Drawing.Point(176, 88);
            this.txtFP.Name = "txtFP";
            this.txtFP.Size = new System.Drawing.Size(100, 22);
            this.txtFP.TabIndex = 0;
            // 
            // txtBF
            // 
            this.txtBF.Location = new System.Drawing.Point(176, 152);
            this.txtBF.Name = "txtBF";
            this.txtBF.Size = new System.Drawing.Size(100, 22);
            this.txtBF.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "fardeau par palette";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "boutille par fardeau";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(208, 236);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "enregistrer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btannule
            // 
            this.btannule.Location = new System.Drawing.Point(59, 236);
            this.btannule.Name = "btannule";
            this.btannule.Size = new System.Drawing.Size(75, 23);
            this.btannule.TabIndex = 3;
            this.btannule.Text = "annulé";
            this.btannule.UseVisualStyleBackColor = true;
            this.btannule.Click += new System.EventHandler(this.btannule_Click);
            // 
            // FrEditFormat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 295);
            this.Controls.Add(this.btannule);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBF);
            this.Controls.Add(this.txtFP);
            this.Controls.Add(this.txtvolme);
            this.Name = "FrEditFormat";
            this.Text = "FrEditFormat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtvolme;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFP;
        private System.Windows.Forms.TextBox txtBF;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btannule;
    }
}