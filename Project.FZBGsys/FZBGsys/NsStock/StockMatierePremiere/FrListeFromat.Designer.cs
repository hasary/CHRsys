﻿namespace FZBGsys.NsStock.StockMatierePremiere
{
    partial class FrListeFromat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btnov = new System.Windows.Forms.Button();
            this.btmod = new System.Windows.Forms.Button();
            this.btsup = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(448, 277);
            this.dgv.TabIndex = 7;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick_1);
            // 
            // btnov
            // 
            this.btnov.Location = new System.Drawing.Point(52, 313);
            this.btnov.Name = "btnov";
            this.btnov.Size = new System.Drawing.Size(75, 23);
            this.btnov.TabIndex = 8;
            this.btnov.Text = "nouveau";
            this.btnov.UseVisualStyleBackColor = true;
            this.btnov.Click += new System.EventHandler(this.btnov_Click);
            // 
            // btmod
            // 
            this.btmod.Location = new System.Drawing.Point(186, 313);
            this.btmod.Name = "btmod";
            this.btmod.Size = new System.Drawing.Size(75, 23);
            this.btmod.TabIndex = 8;
            this.btmod.Text = "modifier";
            this.btmod.UseVisualStyleBackColor = true;
            this.btmod.Click += new System.EventHandler(this.btmod_Click);
            // 
            // btsup
            // 
            this.btsup.Location = new System.Drawing.Point(309, 313);
            this.btsup.Name = "btsup";
            this.btsup.Size = new System.Drawing.Size(75, 23);
            this.btsup.TabIndex = 8;
            this.btsup.Text = "supprimer";
            this.btsup.UseVisualStyleBackColor = true;
            this.btsup.Click += new System.EventHandler(this.btsup_Click);
            // 
            // FrListeFromat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 361);
            this.Controls.Add(this.btsup);
            this.Controls.Add(this.btmod);
            this.Controls.Add(this.btnov);
            this.Controls.Add(this.dgv);
            this.Name = "FrListeFromat";
            this.Text = "FrListeFromat";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btnov;
        private System.Windows.Forms.Button btmod;
        private System.Windows.Forms.Button btsup;

    }
}