﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrStockMatiere : Form
    {
        ModelEntities db;//= new ModelEntities();
        public FrStockMatiere(ModelEntities db = null)
        {

            if (db == null)
            {
                this.db = new ModelEntities();
            }
            else
            {
                this.db = db;
            }

            InitializeComponent();

            InitializeControls();            
            panEdit.Visible = Tools.CurrentUser.GroupeID == 3 || Tools.isBoss == true;

        }

        public FrStockMatiere(EnSection Destination, ModelEntities db, bool? AutoManual)
        {
            InitializeComponent();
            this.db = db;
            this.IsOnlyAutoTransfertTypeRessource = AutoManual == true;
            this.IsOnlyManualTransfertTypeRessource = AutoManual == false;
            InitializeControls();
            //    this.DestinationSection = Destination;

        }


        private void InitializeControls()
        {

            //---------------------- Categorie (type Ressource) -------------------------------------
            cbTypeRessource.Items.Add(new TypeRessource { ID = 0, Nom = "[Tout]" });
            var typeressources = db.TypeRessources.Where(p => p.ID > 0);
            if (IsOnlyAutoTransfertTypeRessource)
            {
                typeressources = typeressources.Where(p => p.IsAutoTransfert == true);
            }
            if (IsOnlyManualTransfertTypeRessource)
            {
                typeressources = typeressources.Where(p => p.IsAutoTransfert == false);
            }

            cbTypeRessource.Items.AddRange(typeressources.ToArray());
            cbTypeRessource.DisplayMember = "Nom";
            cbTypeRessource.ValueMember = "ID";
            cbTypeRessource.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeRessource.SelectedIndex = 0;

                        
            //----------------------------------------------------------------------------------------

            cbTypeArome.Items.Add(new TypeArrome { ID = 0, Nom = "[Tout]" });
            var TypeArome = db.TypeArromes.Where(p => p.ID > 0);
            cbTypeArome.Items.AddRange(TypeArome.ToArray());
            cbTypeArome.DisplayMember = "Nom";
            cbTypeArome.ValueMember = "ID";
            cbTypeArome.SelectedIndex = 0;
        }

        private void FrListArrivage_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!IsSelectionDialog)
            {
                db.Dispose();
            }
        }
        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();

        }

        private void Recherche()
        {
            string filterCat = "", filterFour = "", filterDate = "";
            ListFound = null;
            IQueryable<StockMatiere> result = db.StockMatieres;


            if (cbTypeRessource.SelectedIndex != 0)
            {
                var selected = (TypeRessource)cbTypeRessource.SelectedItem;
                result = result.Where(p => p.Ressource.TypeRessourceID == selected.ID);
                filterCat = selected.Nom;

            }


            if (cbFournisseur.SelectedIndex != 0)
            {
                var selected = (Fournisseur)cbFournisseur.SelectedItem;
                filterFour = "fournisseur " + selected.Nom;
                result = result.Where(p => p.Ressource.FournisseurID == selected.ID);

            }

            if (cbTypeArome.SelectedIndex != 0)
            {
                var selected = (TypeArrome)cbTypeArome.SelectedItem;
                result = result.Where(p => p.Ressource.TypeArromeID == selected.ID);
            }

            this.ListFound = result.ToList();

            if (chConsome.Checked && !chEnstock.Checked)
            {
                ListFound = ListFound.Where(p => p.DateSortie <= dtAu.Value.Date && p.DateSortie >= dtDu.Value.Date).ToList();
                filterDate = (dtDu.Value.Date == dtAu.Value.Date) ?
                    "sortie le " + dtDu.Value.ToShortDateString() : "sortie entre " + dtDu.Value.Date.ToShortDateString() + " et " + dtAu.Value.ToShortDateString();
            }
            else if (!chConsome.Checked && chEnstock.Checked)
            {
                ListFound = ListFound.Where(p => p.EnStock == true).ToList();
                filterDate = "Matière en Stock uniquement";

            }
            else if (chConsome.Checked && chEnstock.Checked)
            {
                ListFound = ListFound.Where(p => p.EnStock == true || (p.DateSortie <= dtAu.Value.Date && p.DateSortie >= dtDu.Value.Date)).ToList();
                filterDate = "Matière en Stock ou ";
                filterDate += (dtDu.Value.Date == dtAu.Value.Date) ?
                    "sortie le " + dtDu.Value.ToShortDateString() : "sortie entre " + dtDu.Value.Date.ToShortDateString() + " et " + dtAu.Value.ToShortDateString();
            }
            else
            {
                return;
            }


            /*  if (exisitingList != null)
              {

                  foreach (var item in exisitingList)
                  {
                      var toUpdate = ListFound.SingleOrDefault(p => p.RessourceID == item.RessourceID);
                      if (toUpdate != null)
                      {
                          toUpdate.Quantite -= item.QunatiteUM;
                          if (toUpdate.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece)
                              toUpdate.Piece -= item.QuantitePiece;
                          if (toUpdate.Quantite == 0)
                          {
                              ListFound.Remove(toUpdate);
                          }
                      }
                  }
              }*/
            if (IsOnlyAutoTransfertTypeRessource)
            {
                ListFound = ListFound.Where(p => p.Ressource.TypeRessource.IsAutoTransfert == true).ToList();
            }

            if (IsOnlyManualTransfertTypeRessource)
            {
                ListFound = ListFound.Where(p => p.Ressource.TypeRessource.IsAutoTransfert == false).ToList();
            }





            RrefreshGrid();
            labFilterTxt.Text = filterDate + " " + filterCat + " " + filterFour;
        }

        private void RrefreshGrid()
        {
            dgv.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Descption = p.Ressource.ToDescription(),
                Fournisseur = p.Ressource.Fournisseur.Nom,

                Nombre = (p.Piece != null) ? p.Piece.Value + " " + p.Ressource.TypeRessource.UniteDePiece : "/",
                Quantite = p.Quantite.ToAffInt() + " " + p.Ressource.TypeRessource.UniteDeMesure,
                Etat = (p.EnStock.Value) ? "En Stock" : "Sortie: " + p.DateSortie.Value.ToShortDateString(),
            }
              ).ToList();

            dgv.Columns["ID"].Visible = false;
        }
        public static int? SelectIDStockMTDialog(List<TransfertMatiere> listExist, EnSection Destination, ModelEntities db, bool? AutoManual)
        {
            var dialog = new FrStockMatiere(Destination, db, AutoManual);
            dialog.panSelect.Visible = true;
            dialog.btPrint.Visible = false;
            dialog.chConsome.Checked = false;
            dialog.chEnstock.Checked = true;
            dialog.chConsome.Enabled = false;
            dialog.chEnstock.Enabled = false;
            dialog.IsSelectionDialog = true;

            dialog.exisitingList = listExist;
            dialog.Recherche();

            dialog.ShowDialog();
            return SelectedStockMTID;
        }

        public List<StockMatiere> ListFound { get; set; }
        public bool IsSelectionDialog { get; set; }
        public bool IsOnlyAutoTransfertTypeRessource { get; set; }
        private void btDelete_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();

            var toDel = db.Arrivages.SingleOrDefault(p => p.ID == id);
            if (toDel != null)
            {
                if (this.ConfirmWarning("Confirmer suppression élement?"))
                {
                    try
                    {
                        db.DeleteObject(toDel);
                        db.SaveChanges();
                    }
                    catch (Exception exp)
                    {

                        this.ShowError(exp.AllMessages("Impossible de supprimer un element!"));
                    }
                }
            }
            else
            {
                this.ShowError("element introuvable!");
            }

            var index = dgv.SelectedRows[0].Index;
            ListFound.RemoveAt(index);


        }
        private void btPrint_Click(object sender, EventArgs e)
        {
            if (ListFound == null || ListFound.Count == 0)
            {
                this.ShowInformation("Il n'y a rien à imprimer!");
                return;
            }


            FZBGsys.NsReports.Stock.StockReportController.PrintCustomStockMp(ListFound, labFilterTxt.Text, db);

        }


        private void chDate_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chConsome.Checked;
        }
        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private void chConsome_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chConsome.Checked;
        }


        #region gestion stock
        internal static void AddToStockMT(Arrivage ar, ModelEntities db, List<StockMatiere> existingStock, DateTime? dt = null)
        {
            var existings = db.StockMatieres.Where(p => p.RessourceID == ar.RessourceID);

            if (ar.Ressource.EntityState == EntityState.Added || existings.Count() == 0) //  create new Stock
            {
                //-------------------------------------------------------  existing List ----------------
                var existingsList = existingStock.Where(rs =>
                rs.Ressource == ar.Ressource);

                if (existingsList.Count() == 0) // not found in list or db so create new Stock
                {
                    var newStock = new StockMatiere()
                       {
                           Ressource = ar.Ressource,
                           Quantite = ar.Quantite,
                           Piece = (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? ar.Piece : null,
                           EnStock = true,

                       };

                    db.AddToStockMatieres(newStock);
                    existingStock.Add(newStock);
                }
                else if (existingsList.Count() >= 1) // found in list 
                {
                    var stock = existingsList.First();
                    stock.Quantite += ar.Quantite;

                    if (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece += ar.Piece;
                    return;
                }
                else // 
                {
                    Tools.ShowError("plusieurs stock meme categorie");
                    return;
                }
                //-----------------------------------------------------------------------------------------------------


            }
            else if (existings.Count() >= 1) // found in db
            {
                //  var exisiting = existings.First();
                var stock = existings.FirstOrDefault(p => p.EnStock == true);
                if (stock != null)
                {
                    stock.Quantite += ar.Quantite;
                    if (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece += ar.Piece;
                }
                else // foud but none EnStock == true
                {
                    var newStock = new StockMatiere()
                    {
                        Ressource = ar.Ressource,
                        Quantite = ar.Quantite,
                        Piece = (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? ar.Piece : null,
                        EnStock = true,

                    };
                }

            }
            else // 
            {
                Tools.ShowError("plusieurs stock meme categorie");
                return;
            }


        }

        internal static void AddRangeToStockMT(List<Arrivage> ListToSave, ModelEntities db)
        {
            var list = new List<StockMatiere>();
            foreach (var item in ListToSave)
            {
                AddToStockMT(item, db, list);
            }
        }
        internal static void AddToStockMT(StockMatiere ar, ModelEntities db, DateTime? dt = null)
        {
            var existings = db.StockMatieres.Where(rs =>
                rs.RessourceID == ar.RessourceID &&
                rs.EnStock == true);


            if (existings.Count() == 0) //  create new Stock
            {
                db.AddToStockMatieres(new StockMatiere()
                {
                    Ressource = ar.Ressource,
                    Quantite = ar.Quantite,
                    Piece = (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? ar.Piece : null,
                    EnStock = true,
                    DateFabrication = ar.DateFabrication,
                });

            }
            else if (existings.Count() >= 1)
            {
                var stock = existings.OrderBy(p => p.ID).First();
                stock.Quantite += ar.Quantite;
                if (ar.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece += ar.Piece;

            }
            else // 
            {
                // Tools.ShowError("plusieurs stock meme categorie");
                // return;
            }

        }
        internal static void AddRangeToStockMT(List<StockMatiere> ListToSave, ModelEntities db)
        {
            foreach (var item in ListToSave)
            {
                AddToStockMT(item, db);
            }
        }
        internal static void RemoveFromStockMT(StockMatiere mat, ModelEntities db, DateTime? DateSortie = null)
        {
            if (DateSortie == null)
            {
                DateSortie = Tools.CurrentDateTime;
            }

            var existings = db.StockMatieres.ToList().Where(p => mat.Ressource == p.Ressource && p.EnStock == true);
            var count = existings.Count();
            if (count == 0)      //------ not found
            {
                Tools.ShowError("Produit introuvable en Stock!!");
            }
            else if (count >= 1) //------ found 
            {
                var stock = existings.OrderBy(p => p.ID).First();
                if (stock.Quantite < mat.Quantite)
                {
                    Tools.ShowError("Quantite Produit En Stock insuffisante !");
                }

                else if (stock.Quantite == mat.Quantite)
                {
                    stock.DateSortie = DateSortie;
                    stock.EnStock = false;
                }
                else
                {
                    stock.Quantite -= mat.Quantite;

                    if ((stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece))
                    {
                        stock.Piece -= mat.Piece;
                    }


                    db.AddToStockMatieres(new StockMatiere()
                    {
                        EnStock = false,
                        DateSortie = DateSortie,
                        Piece = (stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? mat.Piece : null,
                        Quantite = mat.Quantite,
                        Ressource = mat.Ressource,

                    });


                }
            }
            else // impossible
            {
                Tools.ShowError("Plusieurs stock matiere en stock meme categorie");
            }


        }
        internal static bool RemoveFromStockMT(TransfertMatiere transfert, ModelEntities db)
        {
            /* if (DateSortie == null)
             {
                 DateSortie = Tools.CurrentDateTime;
             }
             */
            var existings = db.StockMatieres.ToList().Where(p => transfert.Ressource == p.Ressource && p.EnStock == true);
            var count = existings.Count();
            if (count == 0)      //------ not found
            {
                Tools.ShowError("Produit introuvable en Stock!!");
                return false;
            }
            else if (count >= 1) //------ found 
            {
                var stock = existings.OrderBy(p => p.ID).First();
                if (stock.Quantite < transfert.QunatiteUM)
                {
                    Tools.ShowError("Quantite Produit En Stock insuffisante !");
                    return false;
                }

                else if (stock.Quantite == transfert.QunatiteUM)
                {
                    stock.DateSortie = transfert.Date;
                    stock.EnStock = false;
                    transfert.StockMatiere = stock;
                }
                else
                {
                    stock.Quantite -= transfert.QunatiteUM;

                    if ((stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece))
                    {
                        stock.Piece -= transfert.QuantitePiece;
                    }


                    transfert.StockMatiere = new StockMatiere()
                    {
                        EnStock = false,
                        DateSortie = transfert.Date,
                        Piece = (stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) ? transfert.QuantitePiece : null,
                        Quantite = transfert.QunatiteUM,
                        Ressource = transfert.Ressource,

                    };


                }
                return true;
            }
            else // impossible
            {
                Tools.ShowError("Plusieurs stock matiere en stock meme categorie");
                return false;
            }


        }
        internal static void RemoveRangeFromStockMT(List<StockMatiere> list, ModelEntities db)
        {
            foreach (var item in list)
            {
                RemoveFromStockMT(item, db);
            }

        }
        internal static void RemoveRangeFromStockMT(List<TransfertMatiere> list, ModelEntities db)
        {
            foreach (var item in list)
            {
                RemoveFromStockMT(item, db);
            }

        }


        internal static void RemoveFromStockMT(Arrivage mat, ModelEntities db) // annulage
        {
            var existings = db.StockMatieres.ToList().Where(p => mat.Ressource == p.Ressource && p.EnStock == true);
            var count = existings.Count();
            if (count == 0)      //------ not found
            {
                Tools.ShowError("Produit introuvable en Stock!!");
            }
            else if (count >= 1) //------ found 
            {
                var stock = existings.OrderBy(p => p.ID).First();
                if (stock.Quantite < mat.Quantite)
                {
                    Tools.ShowError("Quantite Produit En Stock insuffisante !");
                }

                else if (stock.Quantite == mat.Quantite)
                {
                    //  db.DeleteObject(stock.Ressource);
                    db.DeleteObject(stock);
                }
                else // stock >  demane
                {
                    stock.Quantite -= mat.Quantite;
                    if (stock.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) stock.Piece -= mat.Piece;

                }
            }
            else // impossible
            {
                Tools.ShowError("Plusieurs stock matiere en stock meme categorie");
            }


        }
        internal static void RemoveRangeFromStockMT(List<Arrivage> list, ModelEntities db)
        {
            foreach (var item in list)
            {
                RemoveFromStockMT(item, db);
            }

        }

        #endregion

        //      internal EnSection DestinationSection { get; set; }

        public static int? SelectedStockMTID { get; set; }

        private void btSelect_Click(object sender, EventArgs e)
        {
            SelectedStockMTID = dgv.GetSelectedID();
            Dispose();
        }

        public List<TransfertMatiere> exisitingList { get; set; }

        public bool IsOnlyManualTransfertTypeRessource { get; set; }

        internal static int? SelectIDStockMTAllDialog(ModelEntities db)
        {
            var dialog = new FrStockMatiere(db);
            dialog.panSelect.Visible = true;
            dialog.btPrint.Visible = false;
            dialog.chConsome.Checked = false;
            dialog.chEnstock.Checked = true;
            dialog.chConsome.Enabled = true;
            dialog.chEnstock.Enabled = true;
            dialog.IsSelectionDialog = true;

            //   dialog.exisitingList = listExist;
            dialog.Recherche();

            dialog.ShowDialog();
            return SelectedStockMTID;
        }

        private void btCorriger_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {

                new FrEditMatiere(id.Value).ShowDialog();
                Recherche();
            }

        }

        private void btEffacer_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {

                var toDel = db.StockMatieres.Single(p => p.ID == id.Value);
                if (toDel.EnStock != true)
                {
                    this.ShowWarning("Ne peut pas supprimer un element consomé");
                    return;
                }
                if (this.ConfirmWarning("Etes-vous vraiment sur de vouloir supprimer ce element du stock?"))
                {
                    db.DeleteObject(toDel);
                    db.SaveChanges();
                    Recherche();
                }
            }
        }

        private void cbCategorie_SelectedIndexChanged(object sender, EventArgs e)
        {
            //----------------------- Fournisseur --------------------------------------------------
            cbFournisseur.Items.Clear();
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[Tout]" });
            if (cbTypeRessource.SelectedIndex <= 0)
            {
                cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0).ToArray());
            }
            else
            {
                var selected = (TypeRessource)cbTypeRessource.SelectedItem;
                cbFournisseur.Items.AddRange(selected.Fournisseurs.Where(p => p.ID > 0).ToArray());
            }
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;

            cbTypeArome.Visible = label4.Visible = cbTypeRessource.SelectedIndex == 5;
        }
    }
}
