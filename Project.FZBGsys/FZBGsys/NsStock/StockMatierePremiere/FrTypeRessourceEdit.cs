﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrTypeRessourceEdit : Form
    {
        ModelEntities db = new ModelEntities();
        public TypeRessource TypeRessource { get; set; }

        public FrTypeRessourceEdit(int? EditID = null)
        {
            InitializeComponent();
            intialiseControl();

            if (EditID != null)
            {
                LoadTypeRessource(EditID.Value);
            }

        }

        private void LoadTypeRessource(int id)
        {
            TypeRessource = db.TypeRessources.Single(p => p.ID == id);
            txtNom.Text = TypeRessource.Nom;
            txtUM.Text = TypeRessource.UniteDeMesure;
            txtUP.Text = TypeRessource.UniteDePiece;
            txtUparPack.Text = TypeRessource.QuantiteParPiece.ToString();
            cbClasse.SelectedItem = TypeRessource.ClasseRessource;
            raAuto.Checked = TypeRessource.IsAutoTransfert == true;
            raUnite.Checked = TypeRessource.DefaultUnite == 0;
            raPak.Checked = TypeRessource.DefaultUnite == 1;
            chqttFix.Checked = TypeRessource.QuantiteParPieceFix == true;

        }

        private void intialiseControl()
        {
            cbClasse.Items.Add(new ClasseRessource { ID = 0, Nom = "[Tout]" });
            cbClasse.Items.AddRange(db.ClasseRessources.Where(p => p.ID > 0).ToArray());
            cbClasse.DisplayMember = "Nom";
            cbClasse.ValueMember = "ID";
            cbClasse.DropDownStyle = ComboBoxStyle.DropDownList;
            cbClasse.SelectedIndex = 0;
        }

        private void FrTypeRessourceEdit_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (TypeRessource ==  null)
            {
                TypeRessource = new TypeRessource();
                db.AddToTypeRessources(TypeRessource);
            }

            TypeRessource.ClasseRessource = (ClasseRessource)cbClasse.SelectedItem;
            TypeRessource.DefaultUnite = raUnite.Checked ?0 : 1;
            TypeRessource.IsAutoTransfert = raAuto.Checked;
            TypeRessource.Nom = txtNom.Text;
            TypeRessource.QuantiteParPiece = txtUparPack.Text.ParseToInt();
            TypeRessource.QuantiteParPieceFix = chqttFix.Checked;
            TypeRessource.UniteDeMesure = txtUM.Text;
            TypeRessource.UniteDePiece = txtUP.Text;

            db.SaveChanges();
            Dispose();

        }
    }
}
