﻿namespace FZBGsys.NsStock
{
    partial class FrListArrivage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btPrint = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtNoFacture = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.btRecherche = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCategorie = new System.Windows.Forms.ComboBox();
            this.chDate = new System.Windows.Forms.CheckBox();
            this.labFilterTxt = new System.Windows.Forms.Label();
            this.dgvPET = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPET)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btPrint
            // 
            this.btPrint.Location = new System.Drawing.Point(12, 6);
            this.btPrint.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(152, 34);
            this.btPrint.TabIndex = 8;
            this.btPrint.Text = "Imprimer La liste";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(169, 6);
            this.btFermer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(112, 34);
            this.btFermer.TabIndex = 9;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(3, 10);
            this.btDelete.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(173, 34);
            this.btDelete.TabIndex = 8;
            this.btDelete.Text = "Supprimer Selection";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.txtNoFacture);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.panDate);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.btRecherche);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbCategorie);
            this.groupBox1.Controls.Add(this.chDate);
            this.groupBox1.Location = new System.Drawing.Point(8, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(272, 446);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recherche";
            // 
            // txtNoFacture
            // 
            this.txtNoFacture.Location = new System.Drawing.Point(140, 320);
            this.txtNoFacture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNoFacture.Name = "txtNoFacture";
            this.txtNoFacture.Size = new System.Drawing.Size(123, 22);
            this.txtNoFacture.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 322);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "N° Facture ou BL:";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(28, 62);
            this.panDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(213, 70);
            this.panDate.TabIndex = 21;
            // 
            // dtAu
            // 
            this.dtAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtAu.Location = new System.Drawing.Point(63, 38);
            this.dtAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtAu.Name = "dtAu";
            this.dtAu.Size = new System.Drawing.Size(139, 22);
            this.dtAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDu
            // 
            this.dtDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDu.Location = new System.Drawing.Point(63, 10);
            this.dtDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDu.Name = "dtDu";
            this.dtDu.Size = new System.Drawing.Size(139, 22);
            this.dtDu.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(28, 182);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(235, 24);
            this.cbFournisseur.TabIndex = 17;
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(4, 405);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(147, 34);
            this.btRecherche.TabIndex = 8;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Fournisseur:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Categorie: ";
            // 
            // cbCategorie
            // 
            this.cbCategorie.FormattingEnabled = true;
            this.cbCategorie.Location = new System.Drawing.Point(28, 265);
            this.cbCategorie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCategorie.Name = "cbCategorie";
            this.cbCategorie.Size = new System.Drawing.Size(235, 24);
            this.cbCategorie.TabIndex = 14;
            // 
            // chDate
            // 
            this.chDate.AutoSize = true;
            this.chDate.Checked = true;
            this.chDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chDate.Enabled = false;
            this.chDate.Location = new System.Drawing.Point(16, 34);
            this.chDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chDate.Name = "chDate";
            this.chDate.Size = new System.Drawing.Size(120, 21);
            this.chDate.TabIndex = 0;
            this.chDate.Text = "Date arrivage:";
            this.chDate.UseVisualStyleBackColor = true;
            this.chDate.CheckedChanged += new System.EventHandler(this.chDate_CheckedChanged);
            // 
            // labFilterTxt
            // 
            this.labFilterTxt.AutoSize = true;
            this.labFilterTxt.Location = new System.Drawing.Point(288, 22);
            this.labFilterTxt.Name = "labFilterTxt";
            this.labFilterTxt.Size = new System.Drawing.Size(0, 17);
            this.labFilterTxt.TabIndex = 11;
            // 
            // dgvPET
            // 
            this.dgvPET.AllowUserToAddRows = false;
            this.dgvPET.AllowUserToDeleteRows = false;
            this.dgvPET.AllowUserToResizeColumns = false;
            this.dgvPET.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPET.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPET.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPET.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPET.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPET.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPET.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPET.Location = new System.Drawing.Point(293, 46);
            this.dgvPET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvPET.Name = "dgvPET";
            this.dgvPET.ReadOnly = true;
            this.dgvPET.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPET.RowHeadersVisible = false;
            this.dgvPET.RowTemplate.Height = 16;
            this.dgvPET.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPET.Size = new System.Drawing.Size(781, 359);
            this.dgvPET.TabIndex = 12;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(384, 14);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 28);
            this.button1.TabIndex = 13;
            this.button1.Text = "IsInventSept";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(416, 14);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(32, 28);
            this.button2.TabIndex = 14;
            this.button2.Text = "undo";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.btDelete);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(287, 411);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(491, 47);
            this.panel1.TabIndex = 15;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(183, 10);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(111, 34);
            this.button3.TabIndex = 15;
            this.button3.Text = "Modifier";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btFermer);
            this.panel2.Controls.Add(this.btPrint);
            this.panel2.Location = new System.Drawing.Point(789, 415);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(293, 50);
            this.panel2.TabIndex = 16;
            // 
            // FrListArrivage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 489);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvPET);
            this.Controls.Add(this.labFilterTxt);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrListArrivage";
            this.Text = "Historique Arrivage Matiere Premiere";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListArrivage_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPET)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

       // private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chDate;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCategorie;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Label labFilterTxt;
        private System.Windows.Forms.DataGridView dgvPET;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtNoFacture;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
    }
}