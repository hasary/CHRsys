﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrEditMatiere : Form
    {
        public FrEditMatiere(int id)
        {
            InitializeComponent();

            this.ID = id;
            LoadMatiere();
        }

        private void LoadMatiere()
        {
            using (var db = new ModelEntities())
            {
                var subject = db.StockMatieres.Single(p => p.ID == ID);
                if (subject.EnStock != true)
                {
                    this.ShowWarning("Ne peut pas Modifier un element consomé");
                    Dispose();
                }
                txtMatiere.Text = subject.Ressource.ToDescription();
                txtFournisseur.Text = (subject.Ressource.Fournisseur != null) ? subject.Ressource.Fournisseur.Nom : "";
                txtQtt.Text = subject.Quantite.ToAffInt();
                if (subject.Ressource.TypeRessource.UniteDePiece != null)
                {
                    txtpce.Text = subject.Piece.ToString();
                    panPce.Visible = true;
                }
                else
                {
                    panPce.Visible = false;

                }

                labUM.Text = subject.Ressource.TypeRessource.UniteDeMesure;
                labUP.Text = subject.Ressource.TypeRessource.UniteDePiece;
            }
        }

        public int ID { get; set; }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var subject = db.StockMatieres.Single(p => p.ID == ID);

                var qtt = txtQtt.Text.ParseToDec();
                var pce = txtpce.Text.ParseToInt();


                if (qtt == null)
                {
                    this.ShowWarning("Quantite invalide");
                    return;
                }
                if (pce == null)
                {
                    this.ShowWarning("Nombre invalide");
                    return;
                }
                subject.Quantite = qtt;
                subject.Piece = pce;

                db.SaveChanges();


            }

            Dispose();
        }
    }
}
