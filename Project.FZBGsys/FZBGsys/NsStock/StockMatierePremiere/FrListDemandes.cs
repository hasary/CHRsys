﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrListDemande : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListDemande()
        {
            InitializeComponent();
            InitializeControls();
        }

        public FrListDemande(int? fournisseurID = null, DateTime? from = null, DateTime? to = null)
        {
            InitializeComponent();
            InitializeControls();

            if (fournisseurID != null)
            {
                dtAu.Value = to.Value;
                dtDu.Value = from.Value;
                var fourni = db.Fournisseurs.Single(p => p.ID == fournisseurID);
                cbFournisseur.SelectedItem = fourni;
                Recherche();
            }


        }

        private void InitializeControls()
        {
            //----------------------- Fournisseur --------------------------------------------------
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[Tout]" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Categorie (type Ressource) -------------------------------------
            cbCategorie.Items.Add(new TypeRessource { ID = 0, Nom = "[Tout]" });
            cbCategorie.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0).ToArray());
            cbCategorie.DisplayMember = "Nom";
            cbCategorie.ValueMember = "ID";
            cbCategorie.DropDownStyle = ComboBoxStyle.DropDownList;
            cbCategorie.SelectedIndex = 0;


            //----------------------------------------------------------------------------------------
        }



        private void FrListDemande_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
           Recherche();

        }

        private void Recherche()
        {
            string filterCat = "", filterFour = "", filterDate = "";
            ListFound = null;
            IQueryable<BonDemande> result = db.BonDemandes;

            if (chDate.Checked)
            {
                result = result.Where(p => p.Date <= dtAu.Value.Date && p.Date >= dtDu.Value.Date);
                filterDate = (dtDu.Value.Date == dtAu.Value.Date) ?
                    "date du " + dtDu.Value.ToShortDateString() : "période du " + dtDu.Value.Date.ToShortDateString() + " au " + dtAu.Value.ToShortDateString();
            }

            if (cbCategorie.SelectedIndex != 0)
            {

                var selected = (TypeRessource)cbCategorie.SelectedItem;
                result = result.Where(p => p.DemandeMatieres.Select(q=>q.Ressource.TypeRessourceID).Contains(selected.ID));
                filterCat = selected.Nom;

            }

            if (txtNoFacture.Text != "")
            {
               // result = result.Where(p => p.NoFact.Contains(txtNoFacture.Text));
            }

            if (cbFournisseur.SelectedIndex != 0)
            {
                //var idCat = int.Parse(cbCategorie.SelectedValue.ToString());
                var selected = (Fournisseur)cbFournisseur.SelectedItem;
                filterFour = "fournisseur " + selected.Nom;
                result = result.Where(p => p.DemandeMatieres.Select(q => q.FournisseurID).Contains(selected.ID));

            }

            this.ListFound = result.ToList();

            RefreshGrid();
            labFilterTxt.Text = filterDate + " " + filterCat + " " + filterFour;
        }

        private void RefreshGrid()
        {
            dgvPET.DataSource = ListFound.OrderByDescending(p => p.Date).ToList().Select(p => new
            {
                ID = p.ID,
                //FactureBL = (p.NoFact != null) ? p.NoFact.ToString() : "",
                Date = p.Date.Value.ToShortDateString(),
                Fournisseur = p.DemandeMatieres.ToList().First().Fournisseur.CodeSage + " - " + p.DemandeMatieres.ToList().First().Ressource.Fournisseur.Nom,
                Descption = p.DemandeMatieres.ToList().First().Ressource.ToDescription(),
                
            }
              ).ToList();

           // dgvPET.Columns["ID"].Visible = false;
        }

        public List<BonDemande> ListFound { get; set; }

        private void btDelete_Click(object sender, EventArgs e)
        {
            var id = dgvPET.GetSelectedID();

            var toDel = db.BonDemandes.SingleOrDefault(p => p.ID == id);
            if (toDel != null)
            {
                if (this.ConfirmWarning("Confirmer suppression élement?"))
                {
                    try
                    {
                      //  FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(toDel, db);
                        var demandes = toDel.DemandeMatieres.ToList();
                        foreach (var item in demandes)
                        {
                            db.DeleteObject(item);
                        }

                        db.DeleteObject(toDel);

                        db.SaveChanges();

                    }
                    catch (Exception exp)
                    {

                        this.ShowError(exp.AllMessages("Impossible de supprimer un element!"));
                    }
                }
            }
            else
            {
                this.ShowError("element introuvable!");
            }



            var index = dgvPET.SelectedRows[0].Index;
            ListFound.RemoveAt(index);
            RefreshGrid();

        }

        private void btPrint_Click(object sender, EventArgs e)
        {
            if (ListFound == null || ListFound.Count == 0)
            {
                this.ShowInformation("Il n'y a rien à imprimer!");
                return;
            }
            Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before

            foreach (var Demande in ListFound)
            {
           /*     db.AddToPrintTemps(new PrintTemp()
                {
                    Date = Demande.DateDemande,
                    Description = ((Demande.NoFact != "") ? "N° " + Demande.NoFact + " " : "") + Demande.Ressource.ToDescription(),
                    Nom = Demande.Ressource.Fournisseur.CodeSage + " - " + Demande.Ressource.Fournisseur.Nom,
                    Piece = Demande.Piece,
                    Quantite = Demande.Quantite,
                    UID = Tools.CurrentUserID,
                    val1 = Demande.Ressource.TypeRessource.UniteDeMesure,
                    val2 = Demande.Ressource.TypeRessource.UniteDePiece

                });*/
            }

            db.SaveChanges();


        }

        private void chDate_CheckedChanged(object sender, EventArgs e)
        {
            panDate.Visible = chDate.Checked;
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        /*
                private void button1_Click(object sender, EventArgs e)
                {
                    var traite = new List<int>();
                    //using (var db = new ModelEntities())


                    for (int i = 0; i < dgvPET.SelectedRows.Count; i++)
                    {
                        var selectedID = dgvPET.SelectedRows[i].Cells["ID"].Value.ToString().ParseToInt();
                        var selected = db.Demandes.SingleOrDefault(p => p.ID == selectedID);
                        var exist = db.StockMatiereHistories.SingleOrDefault(p => p.DemandeID == selected.ID);
                        if (exist==null)
                        {
                            db.AddToStockMatiereHistories(new StockMatiereHistory()
                            {
                                DateFabrication = selected.DateFabrication,
                                DateHistory = DateTime.Parse("2012-09-01"),
                                EnStock = true,
                                Piece = selected.Piece,
                                Quantite = selected.Quantite,
                                RessourceID = selected.RessourceID,
                                DemandeID = selected.ID
                            });

                    

                        }
                    }

                    db.SaveChanges();

                }
        */
        private void button2_Click(object sender, EventArgs e)
        {
            var selectedID = dgvPET.SelectedRows[0].Cells["ID"].Value.ToString().ParseToInt();
         ///   var selected = db.Demandes.SingleOrDefault(p => p.ID == selectedID);
         //   var exist = db.StockMatiereHistories.SingleOrDefault(p => p.DemandeID == selected.ID);
          /*  if (exist != null)
            {
                db.DeleteObject(exist);
                db.SaveChanges();
            }*/

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var id = dgvPET.GetSelectedID();

            FZBGsys.NsReports.Stock.StockReportController.PrintDemandeMatiere(id.Value);

        }
    }
}
