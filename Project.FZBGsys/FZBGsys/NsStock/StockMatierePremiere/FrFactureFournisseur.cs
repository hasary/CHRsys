﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrFactureFournisseur : Form
    {
        ModelEntities db = new ModelEntities();
        decimal ht = 0;
        decimal ttc = 0;
        decimal tva = 0;
        decimal totalFact = 0;
        List <Arrivage> Liste= new List <Arrivage>();
        
        public FrFactureFournisseur(string ArrivageID)
        {
            InitializeComponent();
            txtNoBon.Text = ArrivageID;
            txtNoBon.ReadOnly=true;
            //LoadBonArrivage(ArrivageID.ParseToInt().Value);
        }

        public FrFactureFournisseur(int? id = null, bool MustSave = false)
        {
            InitializeComponent();
            if (id != null)
            {
                IsEditFacture = true;
                this.FactureFournisseur = db.FactureFournisseurs.Single(p => p.ID == id);
                InputFacture();
            }
            else
            {
                IsEditFacture = false;
                txtNoBon.Select();
            }
            this.MustSave = MustSave;
            if (MustSave)
            {
                txtNoBon.ReadOnly = true;
                btAnnuler.Enabled = false;
                this.ControlBox = false;
            }
        }

        private void InputFacture()
        {
            txtNoBon.Text = FactureFournisseur.ArrivageID.Value.ToString();
            //LoadBonArrivage(FactureFournisseur.ArrivageID.Value);
            

            cbMode.SelectedIndex = FactureFournisseur.ModePayement.Value;
           
        }

        private void cbMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            panCheque.Visible = cbMode.SelectedIndex == (int)EnModePayement.Chèque;
            panTimbre.Visible = cbMode.SelectedIndex == (int)EnModePayement.Especes;
            UpdateTotals();


        }

        private void txtNoBon_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
        }


        private void UpdateTotals()
        {
            var selectedModeID = cbMode.SelectedIndex;

            ht = 0;
            ttc = 0;
            tva = 0;
            totalFact = 0;
            //  Montant = 0;


            foreach (var total in Liste)
            {


                ttc += (total.PrixUnitaire == null) ? 0 : total.PrixUnitaire.Value * total.Quantite.Value;

            }
               

                

                //ht += vente.PrixUnitaireHT.Value * vente.TotalBouteilles.Value;
            
            

           //ht = ttc * 100 / 117;

            //tva = ttc - ht;



            totalFact = ttc;
           
            txtMontant.Text = ttc.ToAffInt();
            txtNetPayer.Text = totalFact.ToAffInt();
            RefreshGrid();
            //txtTimbre.Text = timbre.ToAffInt();
                        
        }

        private void LoadBonArrivage(string fac)
        {
            var Arrivage = db.Arrivages.Where(p => p.NoFact == fac);
            Liste = Arrivage.ToList();
            if (Arrivage == null)
            {
                this.ShowWarning("Numéro Bon introuvable");
                return;
            }


            /*if (!IsEditFacture && listArrivage. FactureFournisseurs.Count != 0)
            {
                var facture = Arrivage.FactureFournisseurs.First();
                /*if (this.ConfirmInformation("Ce Bon est déja payé \nVoulez vous imprimer le Bon?"))
                {
                    //var totalTxt = CtoL.convertMontant(facture.MontantTotalFacture.Value);                   

                    return;
                }
                return;

            }*/

            gbpayement.Enabled = true;
            txtClient.Text = Arrivage.First().Ressource.Fournisseur.Nom;

            

                txtSolde.Text = Arrivage.First().Ressource.Fournisseur.Solde.ToAffInt();

                txtDateBon.Text = Arrivage.First().DateArrivage.Value.ToShortDateString();
                var Fournisseur = Arrivage.First().Ressource.Fournisseur;               
                gbpayement.Enabled = true;                                    
                                        
            decimal? montant = 0;
            // this.Montant = 0;
            foreach (var listeArrivage in Arrivage)
            {

                montant += txtPrixU.Text.ParseToDec() * listeArrivage.Quantite;
                // 

            }                  
                        
            UpdateTotals();                        
            cbMode_SelectedIndexChanged(null, null);
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            dgv.DataSource = Liste.Select(p => new
            {
                ID = p.ID,
                NoFacture = p.NoFact,
                Fournisseur = p.Ressource.Fournisseur.Nom,
                Quantite = p.Quantite,
                DateArrivage = p.DateArrivage,

                PrixUnitaire = p.PrixUnitaire,

            }).ToList();


            dgv.HideIDColumn();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (IsEditFacture)
            {

              FactureFournisseur.Fournisseur.Solde += FactureFournisseur.MontantTTC;


            }



            if (!IsValidAll())
            {
                return;
            }
            /* Facturation */
            if (!this.ConfirmInformation("Confirmer Information paiement du Bon N° " + Arrivage.ID + " ?"))
            {
                return;
            }

            if (this.FactureFournisseur == null)
            {
                this.FactureFournisseur = new FactureFournisseur() { Arrivage = this.Arrivage };
            }
            this.Saved = true;
            FactureFournisseur.Fournisseur = Arrivage.Ressource.Fournisseur;
            FactureFournisseur.Date = Arrivage.DateArrivage;
            UpdateTotals();
            var current = Tools.GetServerDateTime();
            if (FactureFournisseur.EntityState == EntityState.Added)
            {
                FactureFournisseur.InscriptionTime = current;

                FactureFournisseur.InscriptionUID = Tools.CurrentUserID;
            }
            else
            {
                FactureFournisseur.ModificationTime = current;
            }
            db.SaveChanges();

            FactureFournisseur.ModePayement = cbMode.SelectedIndex;
            FactureFournisseur.MontantHT = ht;            

            //FactureFournisseur.MontantTotalFacture = totalFact;

            FactureFournisseur.MontantTTC = ttc;
            FactureFournisseur.MontantTVA = ttc - ht;

          /*  if (FactureFournisseur.Fournisseur.Solde == null)
            {
                FactureFournisseur.Fournisseur.Solde = 0;               
            }*/

            UpdateTotals();


        }

        private bool IsValidAll()
        {
            string message = "";
            if (cbMode.SelectedIndex == 0 || cbMode.SelectedIndex == -1 && cbMode.Visible)
            {
                message = "Selectionner Mode payement.";
            }
           

            if (panCheque.Visible && (txtCheque.Text.Trim() == "" || dtDateCheque.Value.Date > DateTime.Now))
            {
                message += "\nInformations chèques invalides";
            }





            if (message == "")
            {
                return true;
            }

            this.ShowWarning(message);
            return false;
        }

        public bool IsEditFacture { get; set; }
                
        public bool MustSave { get; set; }
                
        public FactureFournisseur FactureFournisseur { get; set; }

        public Arrivage Arrivage { get; set; }

        public bool Saved { get; set; }

        private void btAfficher_Click(object sender, EventArgs e)
        {
            var Nfac = txtNoBon.Text;

            if (Nfac != null)
            {
                LoadBonArrivage(Nfac);
            }
            else
            {
                this.ShowWarning("Numéro Bon Invalide");
            }
        }

        private void txtPrixU_Leave(object sender, EventArgs e)
        {
            using (ModelEntities bdt = new ModelEntities())
            {
                var id = dgv.SelectedRows[0].Cells["ID"].Value.ToString().ParseToInt();
                if (id == null)
                {
                    Tools.ShowError("Aucun élément n'est séléctionné");

                }
                else
                {
                    var prix = bdt.Arrivages.Single(p => p.ID == id); //Arrivage.Singl(p => p.ID == id);

                    prix.PrixUnitaire = txtPrixU.Text.ParseToDec();
                    bdt.SaveChanges();
                    txtPrixU.Text = prix.PrixUnitaire.ToString();

                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, Liste);
                }
            }
            
            UpdateTotals();
        }
    }
}
