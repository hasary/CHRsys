﻿namespace FZBGsys.NsStock.StockMatierePremiere
{
    partial class FrEvaluationFournisseur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerMonth = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbModalite = new System.Windows.Forms.TrackBar();
            this.tbPrix = new System.Windows.Forms.TrackBar();
            this.tbDelai = new System.Windows.Forms.TrackBar();
            this.tbQualite = new System.Windows.Forms.TrackBar();
            this.labTotal = new System.Windows.Forms.Label();
            this.labEvalModalite = new System.Windows.Forms.Label();
            this.labEvalPrix = new System.Windows.Forms.Label();
            this.labEvalDelai = new System.Windows.Forms.Label();
            this.labEvalQuantite = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbArrivages = new System.Windows.Forms.ComboBox();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.btAutoEval = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbModalite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPrix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDelai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbQualite)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgv.Location = new System.Drawing.Point(380, 14);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(752, 412);
            this.dgv.TabIndex = 7;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Location = new System.Drawing.Point(788, 434);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(345, 44);
            this.xpannel1.TabIndex = 19;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(209, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(132, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Fermer";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(375, 436);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(236, 436);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 20;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(20, 108);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(332, 24);
            this.cbFournisseur.TabIndex = 15;
            this.cbFournisseur.SelectedIndexChanged += new System.EventHandler(this.cbFournisseur_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Fournisseur:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 22;
            this.label1.Text = "Période:";
            // 
            // dateTimePickerMonth
            // 
            this.dateTimePickerMonth.CustomFormat = "MMMM yyyy";
            this.dateTimePickerMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMonth.Location = new System.Drawing.Point(152, 445);
            this.dateTimePickerMonth.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePickerMonth.Name = "dateTimePickerMonth";
            this.dateTimePickerMonth.ShowUpDown = true;
            this.dateTimePickerMonth.Size = new System.Drawing.Size(25, 22);
            this.dateTimePickerMonth.TabIndex = 35;
            this.dateTimePickerMonth.Visible = false;
            this.dateTimePickerMonth.ValueChanged += new System.EventHandler(this.dateTimePickerMonth_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 436);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 30);
            this.button1.TabIndex = 37;
            this.button1.Text = "Ouvrir...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbModalite);
            this.groupBox1.Controls.Add(this.tbPrix);
            this.groupBox1.Controls.Add(this.tbDelai);
            this.groupBox1.Controls.Add(this.tbQualite);
            this.groupBox1.Controls.Add(this.labTotal);
            this.groupBox1.Controls.Add(this.labEvalModalite);
            this.groupBox1.Controls.Add(this.labEvalPrix);
            this.groupBox1.Controls.Add(this.labEvalDelai);
            this.groupBox1.Controls.Add(this.labEvalQuantite);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(16, 169);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(353, 224);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Evaluations";
            // 
            // tbModalite
            // 
            this.tbModalite.Location = new System.Drawing.Point(136, 146);
            this.tbModalite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbModalite.Maximum = 5;
            this.tbModalite.Name = "tbModalite";
            this.tbModalite.Size = new System.Drawing.Size(156, 56);
            this.tbModalite.TabIndex = 1;
            this.tbModalite.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tbModalite.ValueChanged += new System.EventHandler(this.tbModalite_Scroll);
            // 
            // tbPrix
            // 
            this.tbPrix.Location = new System.Drawing.Point(136, 110);
            this.tbPrix.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPrix.Maximum = 5;
            this.tbPrix.Name = "tbPrix";
            this.tbPrix.Size = new System.Drawing.Size(156, 56);
            this.tbPrix.TabIndex = 1;
            this.tbPrix.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tbPrix.ValueChanged += new System.EventHandler(this.tbPrix_Scroll);
            // 
            // tbDelai
            // 
            this.tbDelai.Location = new System.Drawing.Point(136, 63);
            this.tbDelai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbDelai.Maximum = 5;
            this.tbDelai.Name = "tbDelai";
            this.tbDelai.Size = new System.Drawing.Size(156, 56);
            this.tbDelai.TabIndex = 1;
            this.tbDelai.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tbDelai.ValueChanged += new System.EventHandler(this.tbDelai_Scroll);
            // 
            // tbQualite
            // 
            this.tbQualite.Location = new System.Drawing.Point(136, 22);
            this.tbQualite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbQualite.Maximum = 15;
            this.tbQualite.Name = "tbQualite";
            this.tbQualite.Size = new System.Drawing.Size(156, 56);
            this.tbQualite.TabIndex = 1;
            this.tbQualite.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.tbQualite.ValueChanged += new System.EventHandler(this.tbQualite_Scroll);
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.labTotal.Location = new System.Drawing.Point(309, 199);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(26, 17);
            this.labTotal.TabIndex = 0;
            this.labTotal.Text = "00";
            // 
            // labEvalModalite
            // 
            this.labEvalModalite.AutoSize = true;
            this.labEvalModalite.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEvalModalite.Location = new System.Drawing.Point(309, 158);
            this.labEvalModalite.Name = "labEvalModalite";
            this.labEvalModalite.Size = new System.Drawing.Size(26, 17);
            this.labEvalModalite.TabIndex = 0;
            this.labEvalModalite.Text = "00";
            // 
            // labEvalPrix
            // 
            this.labEvalPrix.AutoSize = true;
            this.labEvalPrix.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEvalPrix.Location = new System.Drawing.Point(309, 121);
            this.labEvalPrix.Name = "labEvalPrix";
            this.labEvalPrix.Size = new System.Drawing.Size(26, 17);
            this.labEvalPrix.TabIndex = 0;
            this.labEvalPrix.Text = "00";
            // 
            // labEvalDelai
            // 
            this.labEvalDelai.AutoSize = true;
            this.labEvalDelai.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEvalDelai.Location = new System.Drawing.Point(309, 81);
            this.labEvalDelai.Name = "labEvalDelai";
            this.labEvalDelai.Size = new System.Drawing.Size(26, 17);
            this.labEvalDelai.TabIndex = 0;
            this.labEvalDelai.Text = "00";
            // 
            // labEvalQuantite
            // 
            this.labEvalQuantite.AutoSize = true;
            this.labEvalQuantite.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labEvalQuantite.Location = new System.Drawing.Point(309, 34);
            this.labEvalQuantite.Name = "labEvalQuantite";
            this.labEvalQuantite.Size = new System.Drawing.Size(26, 17);
            this.labEvalQuantite.TabIndex = 0;
            this.labEvalQuantite.Text = "00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(245, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Total:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 178);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "paiement";
            this.label9.Click += new System.EventHandler(this.label7_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Modalités de";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Prix";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Délai de livraison";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Qualité";
            // 
            // cbArrivages
            // 
            this.cbArrivages.FormattingEnabled = true;
            this.cbArrivages.Location = new System.Drawing.Point(492, 440);
            this.cbArrivages.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbArrivages.Name = "cbArrivages";
            this.cbArrivages.Size = new System.Drawing.Size(45, 24);
            this.cbArrivages.TabIndex = 15;
            this.cbArrivages.Visible = false;
            this.cbArrivages.SelectedIndexChanged += new System.EventHandler(this.cbArrivages_SelectedIndexChanged);
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(108, 15);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(200, 22);
            this.dtFrom.TabIndex = 39;
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(108, 45);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(200, 22);
            this.dtTo.TabIndex = 39;
            this.dtTo.ValueChanged += new System.EventHandler(this.dtTo_ValueChanged);
            // 
            // btAutoEval
            // 
            this.btAutoEval.ForeColor = System.Drawing.Color.Red;
            this.btAutoEval.Location = new System.Drawing.Point(573, 437);
            this.btAutoEval.Name = "btAutoEval";
            this.btAutoEval.Size = new System.Drawing.Size(103, 29);
            this.btAutoEval.TabIndex = 40;
            this.btAutoEval.Text = "AutoEval";
            this.btAutoEval.UseVisualStyleBackColor = true;
            this.btAutoEval.Click += new System.EventHandler(this.btAutoEval_Click);
            // 
            // FrEvaluationFournisseur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 487);
            this.Controls.Add(this.btAutoEval);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.dtFrom);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dateTimePickerMonth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbArrivages);
            this.Controls.Add(this.cbFournisseur);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrEvaluationFournisseur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Evaluation Fournisseur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrEvaluationFournisseur_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbModalite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbPrix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDelai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbQualite)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerMonth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TrackBar tbModalite;
        private System.Windows.Forms.TrackBar tbPrix;
        private System.Windows.Forms.TrackBar tbDelai;
        private System.Windows.Forms.TrackBar tbQualite;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.Label labEvalModalite;
        private System.Windows.Forms.Label labEvalPrix;
        private System.Windows.Forms.Label labEvalDelai;
        private System.Windows.Forms.Label labEvalQuantite;
        private System.Windows.Forms.ComboBox cbArrivages;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Button btAutoEval;
    }
}