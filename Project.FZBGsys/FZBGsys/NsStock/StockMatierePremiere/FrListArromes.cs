﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrListArrome : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListArrome()
        {
            InitializeComponent();
            InitialiseControls();
             RefreshGrid();
        }

        private void RefreshGrid()
        {
            IQueryable<TypeArrome> data = db.TypeArromes.Where(p => p.ID > 0);


            dgv.DataSource = data.OrderBy(p => p.Nom).Select(p => new
            {
                ID = p.ID,
                Nom = p.Nom,
            }).ToList();


            dgv.HideIDColumn();
        }

        private void InitialiseControls()
        {

        }

        private void FrListProduit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            new FrNEArrome().ShowDialog();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.TypeArromes);
            RefreshGrid();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                new FrNEArrome(id).ShowDialog();
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.TypeArromes);
                RefreshGrid();
            }
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                var toDel = db.TypeArromes.Single(p => p.ID == id);

                try
                {
                    db.DeleteObject(toDel);
                    db.SaveChanges();
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.TypeArromes);
                    RefreshGrid();
                }
                catch (Exception ll)
                {

                    Tools.ShowError("ne peut pas suprimer un element utilisé");
                }



            }
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }
    }
}
