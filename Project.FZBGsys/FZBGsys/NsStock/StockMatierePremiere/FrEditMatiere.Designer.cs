﻿namespace FZBGsys.NsStock
{
    partial class FrEditMatiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMatiere = new System.Windows.Forms.TextBox();
            this.txtFournisseur = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtQtt = new System.Windows.Forms.TextBox();
            this.txtpce = new System.Windows.Forms.TextBox();
            this.lab210 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labUM = new System.Windows.Forms.Label();
            this.labUP = new System.Windows.Forms.Label();
            this.btOK = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.panPce = new System.Windows.Forms.Panel();
            this.panPce.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtMatiere
            // 
            this.txtMatiere.Location = new System.Drawing.Point(92, 31);
            this.txtMatiere.Name = "txtMatiere";
            this.txtMatiere.ReadOnly = true;
            this.txtMatiere.Size = new System.Drawing.Size(297, 22);
            this.txtMatiere.TabIndex = 0;
            // 
            // txtFournisseur
            // 
            this.txtFournisseur.Location = new System.Drawing.Point(92, 59);
            this.txtFournisseur.Name = "txtFournisseur";
            this.txtFournisseur.ReadOnly = true;
            this.txtFournisseur.Size = new System.Drawing.Size(297, 22);
            this.txtFournisseur.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Matiere:";
            // 
            // txtQtt
            // 
            this.txtQtt.Location = new System.Drawing.Point(170, 140);
            this.txtQtt.Name = "txtQtt";
            this.txtQtt.Size = new System.Drawing.Size(100, 22);
            this.txtQtt.TabIndex = 3;
            // 
            // txtpce
            // 
            this.txtpce.Location = new System.Drawing.Point(77, 3);
            this.txtpce.Name = "txtpce";
            this.txtpce.Size = new System.Drawing.Size(100, 22);
            this.txtpce.TabIndex = 3;
            // 
            // lab210
            // 
            this.lab210.AutoSize = true;
            this.lab210.Location = new System.Drawing.Point(93, 144);
            this.lab210.Name = "lab210";
            this.lab210.Size = new System.Drawing.Size(62, 17);
            this.lab210.TabIndex = 4;
            this.lab210.Text = "Quantite";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nombre";
            // 
            // labUM
            // 
            this.labUM.AutoSize = true;
            this.labUM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUM.Location = new System.Drawing.Point(286, 140);
            this.labUM.Name = "labUM";
            this.labUM.Size = new System.Drawing.Size(13, 17);
            this.labUM.TabIndex = 4;
            this.labUM.Text = ".";
            // 
            // labUP
            // 
            this.labUP.AutoSize = true;
            this.labUP.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUP.Location = new System.Drawing.Point(193, 4);
            this.labUP.Name = "labUP";
            this.labUP.Size = new System.Drawing.Size(13, 17);
            this.labUP.TabIndex = 4;
            this.labUP.Text = ".";
            // 
            // btOK
            // 
            this.btOK.Location = new System.Drawing.Point(320, 206);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(97, 27);
            this.btOK.TabIndex = 5;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(217, 206);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(97, 27);
            this.btAnnuler.TabIndex = 5;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // panPce
            // 
            this.panPce.Controls.Add(this.txtpce);
            this.panPce.Controls.Add(this.label3);
            this.panPce.Controls.Add(this.labUP);
            this.panPce.Location = new System.Drawing.Point(92, 107);
            this.panPce.Name = "panPce";
            this.panPce.Size = new System.Drawing.Size(222, 32);
            this.panPce.TabIndex = 6;
            // 
            // FrEditMatiere
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(429, 259);
            this.Controls.Add(this.panPce);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.labUM);
            this.Controls.Add(this.lab210);
            this.Controls.Add(this.txtQtt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFournisseur);
            this.Controls.Add(this.txtMatiere);
            this.Name = "FrEditMatiere";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Corriger Stock Matiere";
            this.panPce.ResumeLayout(false);
            this.panPce.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMatiere;
        private System.Windows.Forms.TextBox txtFournisseur;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtQtt;
        private System.Windows.Forms.TextBox txtpce;
        private System.Windows.Forms.Label lab210;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labUM;
        private System.Windows.Forms.Label labUP;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Panel panPce;
    }
}