﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrArrivageEdit : Form
    {
        public FrArrivageEdit(int idArrivage)
        {
            InitializeComponent();
            this.IDArrivage = idArrivage;

            using (var db = new ModelEntities())
            {
                var Arrivage = db.Arrivages.Single(p => p.ID == IDArrivage);
                txtBLF.Text = Arrivage.NoFact.ToString();
                txtDescr.Text = Arrivage.Ressource.ToDescription();
                dtDate.Value = Arrivage.DateArrivage.Value;
                if (Arrivage.Ressource.ToDescription().Trim() == "CO2")
                {
                    txtqnt.Visible = label4.Visible = true;
                    txtqnt.Text = Arrivage.Quantite.ToString();
                }
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                var Arrivage = db.Arrivages.Single(p => p.ID == IDArrivage);
                Arrivage.NoFact = txtBLF.Text;
                Arrivage.DateArrivage = dtDate.Value;
                Arrivage.Quantite = txtqnt.Text.ParseToDec() != null ? txtqnt.Text.ParseToDec() : 0 ;
                db.SaveChanges();
            }

            Dispose();
            
        }

        public int IDArrivage { get; set; }
    }
}
