﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrSelectDateFournisseur : Form
    {
        ModelEntities db = new ModelEntities();
        public FrSelectDateFournisseur()
        {
            InitializeComponent();

            //---------------------- Fournisseur
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "[Tout les Fournisseurs]" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            InitialiseDate();
            SelectionDone = false;

            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {
            string[] monthText = { "Ja", "" };
            Dictionary<string, string> months = new Dictionary<string, string>();

            DateTime fin = DateTime.Now;
            DateTime debut = DateTime.Parse("2012-05-01");

            for (DateTime i = debut; i < fin; i = i.AddMonths(1))
            {
                // months.Add(i.Month,
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dtAnnee.Visible = raAnnuel.Checked;
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = raMensuel.Checked;
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            //  dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime From { get; set; }
        public static DateTime To { get; set; }
        public static bool SelectionDone = false;
        private void button1_Click(object sender, EventArgs e)
        {


            /*
                        GenerateKho(7, 2012);
                        GenerateKho(8, 2012);
                        GenerateKho(9, 2012);
                        GenerateKho(10, 2012);
                        GenerateKho(11, 2012);
                        GenerateKho(12, 2012);
                        GenerateKho(1, 2013);
                        GenerateKho(2, 2013);
                        GenerateKho(3, 2013);
                        GenerateKho(4, 2013);
                        GenerateKho(5, 2013);
           

                        Tools.ShowInformation("Done");
                        return;
                         */
            var anneDu = dtAnnee.Value.Year.ToString() + "-01-01";
            var anneAU = dtAnnee.Value.Year.ToString() + "-12-31";

            var moisDu = dtMoisDu.Value.Year + "-" + dtMoisDu.Value.Month + "-01";
            var moisAu = dtMoisAu.Value.Year + "-" + dtMoisAu.Value.Month + "-" + DateTime.DaysInMonth(dtMoisAu.Value.Year, dtMoisAu.Value.Month).ToString();

            var from = DateTime.Parse((raAnnuel.Checked) ? anneDu : moisDu);
            var to = DateTime.Parse((raAnnuel.Checked) ? anneAU : moisAu);

     NsReports.Stock.StockReportController.PrintEvalFournisseur(from, to, (cbFournisseur.SelectedIndex == 0), ((Fournisseur)cbFournisseur.SelectedItem).ID, raAnnuel.Checked);



            if (cbFournisseur.SelectedIndex == 0)
            {

                NsReports.Stock.StockReportController.PrintEvalFournisseurAnalyse(from, to);

            }
        }

        private void GenerateKho(int month, int year)
        {
            var arrivages = db.Arrivages.ToList().Where(p => p.DateArrivage.Value.Date.Month == month && p.DateArrivage.Value.Date.Year == year).ToList();
            var fournisseurs = arrivages.Select(p => p.Ressource.FournisseurID).Distinct();
            var dateFrom = new DateTime(year, month, 01);
            var dateTo = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            Random r = new Random(DateTime.Now.Millisecond);

            foreach (var fournisseurID in fournisseurs)
            {
                int A = 15;
                int B = 5;
                int C = 5;
                int D = 5;

                for (int i = 0; i < 5; i++)
                {
                    var ra = r.Next(1, 1000) % 18;
                    if (ra == 1 || ra == 9 || ra == 10 || ra == 11) A -= 1;
                    if (ra == 2 || ra == 3) B -= 1;
                    if (ra == 4 || ra == 6 || ra == 7) C -= 1;
                    if (ra == 8) D -= 1;


                }
                /*
                min = 26 - (B + C + D);

                A = r.Next(min, 15); // 15
                min = 26 - (A + C + D);
                r = new Random(DateTime.Now.Millisecond);
                B = r.Next(min, 5); //5
                min =  26 - (B + A + D);

                r = new Random(DateTime.Now.Millisecond);
                C = r.Next(min, 5); // 5
                min = 26 - (B + C + A);

                r = new Random(DateTime.Now.Millisecond);
                D = r.Next(min, 5);//5
                */
                int total = A + B + C + D;


                db.AddToFournisseurEvals(new FournisseurEval()
                {
                    FournisseurID = fournisseurID,
                    EvalDelait = B,
                    EvalQualite = A,
                    EvalPrix = C,
                    EvalModalite = D,
                    DateFrom = dateFrom,
                    DateTo = dateTo


                });

            }

            db.SaveChanges();
        }




        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }


        public Fournisseur Fournisseur { get; set; }

        private void FrSelectDateFournisseur_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }
    }
}
