﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{

    public partial class FrEvaluationFournisseur : Form
    {
        public class ArrivageDescription
        {
            public int ID { get; set; }
            public string Description { get; set; }
        }
        ModelEntities db = new ModelEntities();
        public FrEvaluationFournisseur(int? IDFournisseursEval = null)
        {

            InitializeComponent();

            InitialiseDates();
            InitialiseListFourni();
            ListFournisseurEval = new List<FournisseurEval>();
            btAutoEval.Visible = Tools.CurrentUserID == 1;
            if (IDFournisseursEval != null)
            {
                this.FournisseurEval = db.FournisseurEvals.Single(p => p.ID == IDFournisseursEval);
                InputFournisseurEval();
            }

        }

        private void InputFournisseurEval()
        {
            cbFournisseur.SelectedItem = FournisseurEval.Fournisseur;
            ArrivageDescription tmp = new ArrivageDescription
            {
                ID = FournisseurEval.ArrivageID.Value,
                Description =
                    FournisseurEval.Arrivage.DateArrivage.Value.ToString("dd/MM/yyyy")
                    + "  -  " + FournisseurEval.Arrivage.Ressource.ToDescription()
            };

            cbArrivages.SelectedItem = tmp;

            tbDelai.Value = FournisseurEval.EvalDelait.Value;
            tbQualite.Value = FournisseurEval.EvalQualite.Value;
            tbPrix.Value = FournisseurEval.EvalPrix.Value;
            tbModalite.Value = FournisseurEval.EvalModalite.Value;



            dateTimePickerMonth.Value = FournisseurEval.DateFrom.Value;
        }

        private void InitialiseListFourni()
        {
            cbFournisseur.Items.Clear();
            if (From == null || To == null)
            {
                return;
            }
            Arrivages = db.Arrivages.Where(p => p.DateArrivage >= From && p.DateArrivage <= To).ToList();
            var fournisseurs = Arrivages.Select(p => p.Ressource.Fournisseur).Distinct().ToList();
            // fournisseurs = fournisseurs.Where(p => !ListFournisseurEval.Select(s => s.Fournisseur).Contains(p)).ToList();
            //---------------------- Fournisseur
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(fournisseurs.Where(p => p.ID > 0).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        public DateTime From { get; set; }
        public DateTime To { get; set; }


        private void button1_Click(object sender, EventArgs e)
        {
            if (cbFournisseur.SelectedItem != null)
            {
                new FrListArrivage(((Fournisseur)cbFournisseur.SelectedItem).ID, From, To).ShowDialog();
            }
        }

        private void dateTimePickerMonth_ValueChanged(object sender, EventArgs e)
        {
            InitialiseDates();
            InitialiseListFourni();
        }

        private void InitialiseDates()
        {/*
            this.From = dateTimePickerMonth.Value.AddDays(1 - dateTimePickerMonth.Value.Day).Date;
            this.To = dateTimePickerMonth.Value.AddDays(
                DateTime.DaysInMonth(dateTimePickerMonth.Value.Year, dateTimePickerMonth.Value.Month)
                - dateTimePickerMonth.Value.Day).Date;
            
          * */

            this.From = dtFrom.Value;
            this.To = dtTo.Value;
            ListFournisseurEval = db.FournisseurEvals.Where(p => p.DateFrom == From && p.DateTo == To).ToList();
            RefreshGrid();
        }

        private void tbQualite_Scroll(object sender, EventArgs e)
        {
            labEvalQuantite.Text = tbQualite.Value.ToString();
            updateTotal();
        }

        private void updateTotal()
        {
            labTotal.Text =
                (labEvalDelai.Text.ParseToInt() +
                labEvalQuantite.Text.ParseToInt() +
                labEvalPrix.Text.ParseToInt() +
                labEvalModalite.Text.ParseToInt()).ToString();
        }

        private void tbDelai_Scroll(object sender, EventArgs e)
        {
            labEvalDelai.Text = tbDelai.Value.ToString();
            updateTotal();
        }

        private void tbPrix_Scroll(object sender, EventArgs e)
        {
            labEvalPrix.Text = tbPrix.Value.ToString();
            updateTotal();
        }

        private void tbModalite_Scroll(object sender, EventArgs e)
        {
            labEvalModalite.Text = tbModalite.Value.ToString();
            updateTotal();
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (FournisseurEval == null)
            {
                FournisseurEval = new FournisseurEval();
            }

            if (cbFournisseur.SelectedIndex < 1)
            {
                Tools.ShowInformation("Selectionnez d'abord un fournisseur et arrivage");
                return;
            }

            FournisseurEval.DateFrom = this.From.Date;
            FournisseurEval.DateTo = this.To.Date;
            FournisseurEval.EvalDelait = labEvalDelai.Text.ParseToInt();
            FournisseurEval.EvalModalite = labEvalModalite.Text.ParseToInt();
            FournisseurEval.EvalPrix = labEvalPrix.Text.ParseToInt();
            FournisseurEval.EvalQualite = labEvalQuantite.Text.ParseToInt();
            FournisseurEval.FournisseurID = ((Fournisseur)cbFournisseur.SelectedItem).ID;
            // FournisseurEval.ArrivageID = ((ArrivageDescription)cbArrivages.SelectedItem).ID;


            ListFournisseurEval.Add(FournisseurEval);
            db.AddToFournisseurEvals(FournisseurEval);
            db.SaveChanges();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, FournisseurEval);
            InitialiseDates();
            InitialiseListeArrivages();
            ClearControls();
            RefreshGrid();
        }

        private void ClearControls()
        {
            //   cbFournisseur.SelectedIndex = 0;
            FournisseurEval = null;
            tbDelai.Value = tbModalite.Value = tbPrix.Value = tbQualite.Value = 0;
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListFournisseurEval.Select(p => new
            {
                ID = p.ID,
                Periode = p.DateFrom.Value.ToString("MMMM yyyy") + " au " + p.DateTo.Value.ToString("MMMM yyyy"),
                CodeFourn = p.Fournisseur.CodeSage,
                Fournisseur = p.Fournisseur.Nom,
                // Arriavge = p.Arrivage.DateArrivage.Value.ToShortDateString() + "  -  " + p.Arrivage.Ressource.ToDescription(),
                Evaluation = p.EvalQualite.ToString() + " + " + p.EvalDelait.ToString() + " + " + p.EvalPrix.ToString() + " + " + p.EvalModalite.ToString(),
                Total = (p.EvalDelait + p.EvalModalite + p.EvalPrix + p.EvalQualite).ToString() + "/30"

            }).ToList();
            dgv.Columns["ID"].Visible = false;

        }

        public List<FournisseurEval> ListFournisseurEval { get; set; }
        public FournisseurEval FournisseurEval { get; set; }

        private void FrEvaluationFournisseur_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selected = dgv.GetSelectedID();
            this.FournisseurEval = db.FournisseurEvals.Single(p => p.ID == selected);
            InputFournisseurEval();
            ListFournisseurEval.Remove(this.FournisseurEval);
            db.DeleteObject(FournisseurEval);
            db.SaveChanges();
            //  db.Refresh(System.Data.Objects.RefreshMode.ClientWins, FournisseurEval);
            InitialiseDates();
            InitialiseListFourni();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            db.SaveChanges();
            Dispose();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void cbFournisseur_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitialiseDates();
            InitialiseListeArrivages();
        }

        private void InitialiseListeArrivages()
        {/*
            var Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
            var arrivages = Arrivages.Where(p => p.Ressource.FournisseurID == Fournisseur.ID).ToList().Select(p => new ArrivageDescription { ID = p.ID, Description = p.DateArrivage.Value.ToString("dd/MM/yyyy") + "  -  " + p.Ressource.ToDescription() });
            int cc = arrivages.Count();
            arrivages = arrivages.ToList().Where(p => !this.ListFournisseurEval.Select(q => q.ArrivageID).Contains(p.ID));
            cbArrivages.Items.Clear();
            cbArrivages.Items.Add(new ArrivageDescription { ID = 0, Description = "" });
            cbArrivages.Items.AddRange(
                arrivages.ToArray());
            cbArrivages.DisplayMember = "Description";
            cbArrivages.ValueMember = "ID";
            cbArrivages.DropDownStyle = ComboBoxStyle.DropDownList;
            cbArrivages.SelectedIndex = 0;
          * */
        }

        public List<Arrivage> Arrivages { get; set; }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            InitialiseDates();
            InitialiseListFourni();
        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {
            InitialiseDates();
            InitialiseListFourni();
        }

        private void cbArrivages_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btAutoEval_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            var begin = DateTime.Parse("2013-07-01");
            var end = DateTime.Parse("2014-05-31"); ;

            var dateFrom = begin;

            while (dateFrom < end)
            {
                var dateTo = dateFrom.AddDays(DateTime.DaysInMonth(dateFrom.Year, dateFrom.Month) - 1);
                var fournisseurs = db.Arrivages.Where(p => p.DateArrivage >= dateFrom && p.DateArrivage <= dateTo).Select(p => p.Ressource.Fournisseur).ToList().Distinct().ToList();

                foreach (var four in fournisseurs)
                {
                    var FournisseurEval = new FournisseurEval();

                    FournisseurEval.DateFrom = dateFrom;
                    FournisseurEval.DateTo = dateTo;

                    FournisseurEval.EvalDelait = 2 + r.Next(1000) % 4;
                    FournisseurEval.EvalModalite = 2 + r.Next(1000) % 4;
                    FournisseurEval.EvalPrix = 2 + r.Next(1000) % 4;
                    FournisseurEval.EvalQualite = 10 + r.Next(1000) % 6;

                    FournisseurEval.FournisseurID = four.ID;

                    db.AddToFournisseurEvals(FournisseurEval);
                }

                db.SaveChanges();
                dateFrom = dateTo.AddDays(1);
            }

            Tools.ShowError("done");
        }
    }
}
