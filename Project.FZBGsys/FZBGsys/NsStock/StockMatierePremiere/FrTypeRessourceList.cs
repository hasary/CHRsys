﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrTypeRessourceList : Form
    {
        ModelEntities db = new ModelEntities();

        public FrTypeRessourceList()
        {
            InitializeComponent();
            IntiliseControls();
            RefreshGrid();
        }

        private void IntiliseControls()
        {
            cbClasse.Items.Add(new ClasseRessource { ID = 0, Nom = "[Tout]" });
            cbClasse.Items.AddRange(db.ClasseRessources.Where(p => p.ID > 0).ToArray());
            cbClasse.DisplayMember = "Nom";
            cbClasse.ValueMember = "ID";
            cbClasse.DropDownStyle = ComboBoxStyle.DropDownList;
            cbClasse.SelectedIndex = 0;
        }

        private void RefreshGrid()
        {
            IQueryable<TypeRessource> data = db.TypeRessources;
            if (cbClasse.SelectedIndex > 0)
            {
                var selectedClasse = (ClasseRessource)cbClasse.SelectedItem;
                data = data.Where(p => p.ClasseRessource == selectedClasse);
            }

            dgv.DataSource = data.Select(p => new
            {ID = p.ID,
                Categorie = p.ClasseRessource.Nom,
                Description = p.Nom,
                Mesurer = p.UniteDeMesure,
                Unite = p.UniteDePiece,

            }

                ).ToList();

            dgv.HideIDColumn();
        }

        private void FrTypeRessourceList_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            new FrTypeRessourceEdit().ShowDialog();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                new FrTypeRessourceEdit(id).ShowDialog();
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.TypeRessources);
                RefreshGrid();
            }



        }

        private void btDel_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                var todel = db.TypeRessources.Single(p => p.ID == id);
                try
                {
                    db.DeleteObject(todel);
                    db.SaveChanges();
                }
                catch (Exception efd)
                {

                    Tools.ShowError("Vous ne pouvez pas supprimer un élément déja utilisé");
                }
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.TypeRessources);
                RefreshGrid();
            }
        }

        private void cbClasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }
    }
}
