﻿namespace FZBGsys.NsStock
{
    partial class FrBesoinMatiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.labUniteDePiece = new System.Windows.Forms.Label();
            this.btAjouter = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.txtPiece = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btEnlever = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labUniteDeMesure = new System.Windows.Forms.Label();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.gbMatiere = new System.Windows.Forms.GroupBox();
            this.labUniteDeMesure3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.labUniteDePiece2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPiceEnStock = new System.Windows.Forms.TextBox();
            this.labUniteDeMesure2 = new System.Windows.Forms.Label();
            this.txtQttEnStock = new System.Windows.Forms.TextBox();
            this.txtMatiere = new System.Windows.Forms.TextBox();
            this.dtDateArrivage = new System.Windows.Forms.DateTimePicker();
            this.chDateArrivage = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btOuvrirStock = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtObservation = new System.Windows.Forms.TextBox();
            this.panObs = new System.Windows.Forms.Panel();
            this.btAuto = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.gbMatiere.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panObs.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(319, 25);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(744, 469);
            this.dgv.TabIndex = 7;
            // 
            // labUniteDePiece
            // 
            this.labUniteDePiece.AutoSize = true;
            this.labUniteDePiece.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDePiece.Location = new System.Drawing.Point(197, 32);
            this.labUniteDePiece.Name = "labUniteDePiece";
            this.labUniteDePiece.Size = new System.Drawing.Size(56, 17);
            this.labUniteDePiece.TabIndex = 24;
            this.labUniteDePiece.Text = "boxBid";
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(180, 508);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 28;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(165, 25);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(132, 22);
            this.dtDate.TabIndex = 18;
            // 
            // txtPiece
            // 
            this.txtPiece.Location = new System.Drawing.Point(101, 32);
            this.txtPiece.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPiece.Name = "txtPiece";
            this.txtPiece.Size = new System.Drawing.Size(89, 22);
            this.txtPiece.TabIndex = 26;
            this.txtPiece.TextChanged += new System.EventHandler(this.txtPiece_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 17);
            this.label10.TabIndex = 25;
            this.label10.Text = "Nombre:";
            // 
            // btEnlever
            // 
            this.btEnlever.ForeColor = System.Drawing.Color.Red;
            this.btEnlever.Location = new System.Drawing.Point(319, 508);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 20;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Visible = false;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(111, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 19;
            this.label1.Text = "Date:";
            // 
            // labUniteDeMesure
            // 
            this.labUniteDeMesure.AutoSize = true;
            this.labUniteDeMesure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure.Location = new System.Drawing.Point(197, 70);
            this.labUniteDeMesure.Name = "labUniteDeMesure";
            this.labUniteDeMesure.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure.TabIndex = 23;
            this.labUniteDeMesure.Text = "UM";
            // 
            // txtQuantite
            // 
            this.txtQuantite.Location = new System.Drawing.Point(101, 66);
            this.txtQuantite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.Size = new System.Drawing.Size(89, 22);
            this.txtQuantite.TabIndex = 27;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "Quantite:";
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(727, 498);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(336, 43);
            this.xpannel1.TabIndex = 21;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(68, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(132, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(205, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // gbMatiere
            // 
            this.gbMatiere.Controls.Add(this.labUniteDeMesure3);
            this.gbMatiere.Controls.Add(this.textBox1);
            this.gbMatiere.Controls.Add(this.label5);
            this.gbMatiere.Controls.Add(this.cbFournisseur);
            this.gbMatiere.Controls.Add(this.labUniteDePiece2);
            this.gbMatiere.Controls.Add(this.label8);
            this.gbMatiere.Controls.Add(this.label7);
            this.gbMatiere.Controls.Add(this.txtPiceEnStock);
            this.gbMatiere.Controls.Add(this.labUniteDeMesure2);
            this.gbMatiere.Controls.Add(this.txtQttEnStock);
            this.gbMatiere.Controls.Add(this.txtMatiere);
            this.gbMatiere.Enabled = false;
            this.gbMatiere.Location = new System.Drawing.Point(20, 108);
            this.gbMatiere.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbMatiere.Name = "gbMatiere";
            this.gbMatiere.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbMatiere.Size = new System.Drawing.Size(293, 231);
            this.gbMatiere.TabIndex = 29;
            this.gbMatiere.TabStop = false;
            this.gbMatiere.Text = "Matiere Premiere";
            // 
            // labUniteDeMesure3
            // 
            this.labUniteDeMesure3.AutoSize = true;
            this.labUniteDeMesure3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure3.Location = new System.Drawing.Point(229, 204);
            this.labUniteDeMesure3.Name = "labUniteDeMesure3";
            this.labUniteDeMesure3.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure3.TabIndex = 31;
            this.labUniteDeMesure3.Text = "UM";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(133, 199);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(89, 22);
            this.textBox1.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 17);
            this.label5.TabIndex = 29;
            this.label5.Text = "Seuil inf.";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.Enabled = false;
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(21, 84);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(255, 24);
            this.cbFournisseur.TabIndex = 28;
            // 
            // labUniteDePiece2
            // 
            this.labUniteDePiece2.AutoSize = true;
            this.labUniteDePiece2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDePiece2.Location = new System.Drawing.Point(229, 129);
            this.labUniteDePiece2.Name = "labUniteDePiece2";
            this.labUniteDePiece2.Size = new System.Drawing.Size(56, 17);
            this.labUniteDePiece2.TabIndex = 25;
            this.labUniteDePiece2.Text = "boxBid";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 17);
            this.label8.TabIndex = 5;
            this.label8.Text = "Quantite En Stock";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Fournisseur";
            // 
            // txtPiceEnStock
            // 
            this.txtPiceEnStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPiceEnStock.Location = new System.Drawing.Point(133, 127);
            this.txtPiceEnStock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPiceEnStock.Name = "txtPiceEnStock";
            this.txtPiceEnStock.ReadOnly = true;
            this.txtPiceEnStock.Size = new System.Drawing.Size(89, 22);
            this.txtPiceEnStock.TabIndex = 2;
            // 
            // labUniteDeMesure2
            // 
            this.labUniteDeMesure2.AutoSize = true;
            this.labUniteDeMesure2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure2.Location = new System.Drawing.Point(229, 158);
            this.labUniteDeMesure2.Name = "labUniteDeMesure2";
            this.labUniteDeMesure2.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure2.TabIndex = 23;
            this.labUniteDeMesure2.Text = "UM";
            // 
            // txtQttEnStock
            // 
            this.txtQttEnStock.Location = new System.Drawing.Point(133, 155);
            this.txtQttEnStock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQttEnStock.Name = "txtQttEnStock";
            this.txtQttEnStock.ReadOnly = true;
            this.txtQttEnStock.Size = new System.Drawing.Size(89, 22);
            this.txtQttEnStock.TabIndex = 27;
            // 
            // txtMatiere
            // 
            this.txtMatiere.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatiere.Location = new System.Drawing.Point(19, 30);
            this.txtMatiere.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMatiere.Name = "txtMatiere";
            this.txtMatiere.ReadOnly = true;
            this.txtMatiere.Size = new System.Drawing.Size(257, 22);
            this.txtMatiere.TabIndex = 2;
            // 
            // dtDateArrivage
            // 
            this.dtDateArrivage.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateArrivage.Location = new System.Drawing.Point(560, 513);
            this.dtDateArrivage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateArrivage.Name = "dtDateArrivage";
            this.dtDateArrivage.Size = new System.Drawing.Size(113, 22);
            this.dtDateArrivage.TabIndex = 30;
            this.dtDateArrivage.Visible = false;
            // 
            // chDateArrivage
            // 
            this.chDateArrivage.AutoSize = true;
            this.chDateArrivage.Location = new System.Drawing.Point(560, 513);
            this.chDateArrivage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chDateArrivage.Name = "chDateArrivage";
            this.chDateArrivage.Size = new System.Drawing.Size(117, 21);
            this.chDateArrivage.TabIndex = 29;
            this.chDateArrivage.Text = "Date Arrivage";
            this.chDateArrivage.UseVisualStyleBackColor = true;
            this.chDateArrivage.Visible = false;
            this.chDateArrivage.CheckedChanged += new System.EventHandler(this.chDateArrivage_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Selection Matiere:";
            // 
            // btOuvrirStock
            // 
            this.btOuvrirStock.Location = new System.Drawing.Point(165, 66);
            this.btOuvrirStock.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btOuvrirStock.Name = "btOuvrirStock";
            this.btOuvrirStock.Size = new System.Drawing.Size(132, 28);
            this.btOuvrirStock.TabIndex = 1;
            this.btOuvrirStock.Text = "Ouvrir le Stock...";
            this.btOuvrirStock.UseVisualStyleBackColor = true;
            this.btOuvrirStock.Click += new System.EventHandler(this.btOuvrirStock_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtPiece);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtQuantite);
            this.groupBox2.Controls.Add(this.labUniteDeMesure);
            this.groupBox2.Controls.Add(this.labUniteDePiece);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(20, 345);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(293, 101);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Quantite demandée";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 31;
            this.label3.Text = "Observation";
            // 
            // txtObservation
            // 
            this.txtObservation.Location = new System.Drawing.Point(15, 25);
            this.txtObservation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtObservation.Name = "txtObservation";
            this.txtObservation.Size = new System.Drawing.Size(267, 22);
            this.txtObservation.TabIndex = 32;
            this.txtObservation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObservation_KeyPress);
            // 
            // panObs
            // 
            this.panObs.Controls.Add(this.txtObservation);
            this.panObs.Controls.Add(this.label3);
            this.panObs.Location = new System.Drawing.Point(13, 452);
            this.panObs.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panObs.Name = "panObs";
            this.panObs.Size = new System.Drawing.Size(292, 50);
            this.panObs.TabIndex = 33;
            // 
            // btAuto
            // 
            this.btAuto.ForeColor = System.Drawing.Color.Red;
            this.btAuto.Location = new System.Drawing.Point(437, 508);
            this.btAuto.Name = "btAuto";
            this.btAuto.Size = new System.Drawing.Size(88, 33);
            this.btAuto.TabIndex = 34;
            this.btAuto.Text = "Autobesoin";
            this.btAuto.UseVisualStyleBackColor = true;
            this.btAuto.Click += new System.EventHandler(this.btAuto_Click);
            // 
            // FrBesoinMatiere
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1075, 553);
            this.Controls.Add(this.btAuto);
            this.Controls.Add(this.panObs);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dtDateArrivage);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.chDateArrivage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbMatiere);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.btOuvrirStock);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrBesoinMatiere";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Demande de Besoin Matiere Première";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrTransfert_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.gbMatiere.ResumeLayout(false);
            this.gbMatiere.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panObs.ResumeLayout(false);
            this.panObs.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label labUniteDePiece;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.TextBox txtPiece;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labUniteDeMesure;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.GroupBox gbMatiere;
        private System.Windows.Forms.Label labUniteDePiece2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPiceEnStock;
        private System.Windows.Forms.Label labUniteDeMesure2;
        private System.Windows.Forms.TextBox txtQttEnStock;
        private System.Windows.Forms.TextBox txtMatiere;
        private System.Windows.Forms.Button btOuvrirStock;
        private System.Windows.Forms.DateTimePicker dtDateArrivage;
        private System.Windows.Forms.CheckBox chDateArrivage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtObservation;
        private System.Windows.Forms.Panel panObs;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label labUniteDeMesure3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btAuto;
    }
}