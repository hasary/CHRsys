﻿namespace FZBGsys.NsStock.StockMatierePremiere
{
    partial class FrFactureFournisseur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panCheque = new System.Windows.Forms.Panel();
            this.dtDateCheque = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCheque = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbMode = new System.Windows.Forms.ComboBox();
            this.labMode = new System.Windows.Forms.Label();
            this.txtMontant = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbpayement = new System.Windows.Forms.GroupBox();
            this.panTimbre = new System.Windows.Forms.Panel();
            this.txtTimbre = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNetPayer = new System.Windows.Forms.TextBox();
            this.txtPrixU = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtClient = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panSolde = new System.Windows.Forms.Panel();
            this.txtSolde = new System.Windows.Forms.TextBox();
            this.txtDateBon = new System.Windows.Forms.TextBox();
            this.txtNoBon = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btAfficher = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.panCheque.SuspendLayout();
            this.gbpayement.SuspendLayout();
            this.panTimbre.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panSolde.SuspendLayout();
            this.xpannel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // panCheque
            // 
            this.panCheque.Controls.Add(this.dtDateCheque);
            this.panCheque.Controls.Add(this.label14);
            this.panCheque.Controls.Add(this.label13);
            this.panCheque.Controls.Add(this.txtCheque);
            this.panCheque.Location = new System.Drawing.Point(11, 105);
            this.panCheque.Name = "panCheque";
            this.panCheque.Size = new System.Drawing.Size(206, 55);
            this.panCheque.TabIndex = 20;
            this.panCheque.Visible = false;
            // 
            // dtDateCheque
            // 
            this.dtDateCheque.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateCheque.Location = new System.Drawing.Point(80, 28);
            this.dtDateCheque.Margin = new System.Windows.Forms.Padding(2);
            this.dtDateCheque.Name = "dtDateCheque";
            this.dtDateCheque.Size = new System.Drawing.Size(124, 20);
            this.dtDateCheque.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(34, 32);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Date:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "N° Chèque:";
            // 
            // txtCheque
            // 
            this.txtCheque.Location = new System.Drawing.Point(78, 3);
            this.txtCheque.Name = "txtCheque";
            this.txtCheque.Size = new System.Drawing.Size(126, 20);
            this.txtCheque.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(195, 166);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "DA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Net à Payer:";
            // 
            // cbMode
            // 
            this.cbMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMode.FormattingEnabled = true;
            this.cbMode.Items.AddRange(new object[] {
            "",
            "Espèces",
            "Avance",
            "Virement",
            "Chèque",
            "Traite",
            "à Terme"});
            this.cbMode.Location = new System.Drawing.Point(90, 82);
            this.cbMode.Name = "cbMode";
            this.cbMode.Size = new System.Drawing.Size(127, 21);
            this.cbMode.TabIndex = 18;
            // 
            // labMode
            // 
            this.labMode.AutoSize = true;
            this.labMode.Location = new System.Drawing.Point(40, 84);
            this.labMode.Name = "labMode";
            this.labMode.Size = new System.Drawing.Size(37, 13);
            this.labMode.TabIndex = 17;
            this.labMode.Text = "Mode:";
            // 
            // txtMontant
            // 
            this.txtMontant.Location = new System.Drawing.Point(91, 26);
            this.txtMontant.Name = "txtMontant";
            this.txtMontant.ReadOnly = true;
            this.txtMontant.Size = new System.Drawing.Size(99, 20);
            this.txtMontant.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Montant:";
            // 
            // gbpayement
            // 
            this.gbpayement.Controls.Add(this.panCheque);
            this.gbpayement.Controls.Add(this.panTimbre);
            this.gbpayement.Controls.Add(this.label11);
            this.gbpayement.Controls.Add(this.label8);
            this.gbpayement.Controls.Add(this.label12);
            this.gbpayement.Controls.Add(this.cbMode);
            this.gbpayement.Controls.Add(this.labMode);
            this.gbpayement.Controls.Add(this.txtNetPayer);
            this.gbpayement.Controls.Add(this.label9);
            this.gbpayement.Controls.Add(this.txtPrixU);
            this.gbpayement.Controls.Add(this.label6);
            this.gbpayement.Controls.Add(this.txtMontant);
            this.gbpayement.Controls.Add(this.label4);
            this.gbpayement.Enabled = false;
            this.gbpayement.Location = new System.Drawing.Point(5, 148);
            this.gbpayement.Name = "gbpayement";
            this.gbpayement.Size = new System.Drawing.Size(245, 194);
            this.gbpayement.TabIndex = 19;
            this.gbpayement.TabStop = false;
            this.gbpayement.Text = "Payement";
            // 
            // panTimbre
            // 
            this.panTimbre.Controls.Add(this.txtTimbre);
            this.panTimbre.Controls.Add(this.label7);
            this.panTimbre.Controls.Add(this.label10);
            this.panTimbre.Location = new System.Drawing.Point(9, 109);
            this.panTimbre.Name = "panTimbre";
            this.panTimbre.Size = new System.Drawing.Size(208, 25);
            this.panTimbre.TabIndex = 16;
            this.panTimbre.Visible = false;
            // 
            // txtTimbre
            // 
            this.txtTimbre.Location = new System.Drawing.Point(80, 2);
            this.txtTimbre.Name = "txtTimbre";
            this.txtTimbre.ReadOnly = true;
            this.txtTimbre.Size = new System.Drawing.Size(96, 20);
            this.txtTimbre.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Timbre:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(182, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "DA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(195, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "DA";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(195, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "DA";
            // 
            // txtNetPayer
            // 
            this.txtNetPayer.Location = new System.Drawing.Point(91, 163);
            this.txtNetPayer.Name = "txtNetPayer";
            this.txtNetPayer.ReadOnly = true;
            this.txtNetPayer.Size = new System.Drawing.Size(99, 20);
            this.txtNetPayer.TabIndex = 2;
            // 
            // txtPrixU
            // 
            this.txtPrixU.Location = new System.Drawing.Point(91, 52);
            this.txtPrixU.Name = "txtPrixU";
            this.txtPrixU.Size = new System.Drawing.Size(99, 20);
            this.txtPrixU.TabIndex = 2;
            this.txtPrixU.Leave += new System.EventHandler(this.txtPrixU_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Prix Unitaire:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(194, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "DA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Solde Actuel:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Fournisseur:";
            // 
            // txtClient
            // 
            this.txtClient.Location = new System.Drawing.Point(70, 73);
            this.txtClient.Name = "txtClient";
            this.txtClient.ReadOnly = true;
            this.txtClient.Size = new System.Drawing.Size(171, 20);
            this.txtClient.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panSolde);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDateBon);
            this.groupBox1.Controls.Add(this.txtClient);
            this.groupBox1.Controls.Add(this.txtNoBon);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btAfficher);
            this.groupBox1.Location = new System.Drawing.Point(3, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(247, 131);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Arrivage";
            // 
            // panSolde
            // 
            this.panSolde.Controls.Add(this.label5);
            this.panSolde.Controls.Add(this.label3);
            this.panSolde.Controls.Add(this.txtSolde);
            this.panSolde.Location = new System.Drawing.Point(2, 94);
            this.panSolde.Margin = new System.Windows.Forms.Padding(2);
            this.panSolde.Name = "panSolde";
            this.panSolde.Size = new System.Drawing.Size(223, 32);
            this.panSolde.TabIndex = 38;
            // 
            // txtSolde
            // 
            this.txtSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSolde.Location = new System.Drawing.Point(91, 9);
            this.txtSolde.Name = "txtSolde";
            this.txtSolde.ReadOnly = true;
            this.txtSolde.Size = new System.Drawing.Size(98, 19);
            this.txtSolde.TabIndex = 2;
            // 
            // txtDateBon
            // 
            this.txtDateBon.Location = new System.Drawing.Point(48, 49);
            this.txtDateBon.Name = "txtDateBon";
            this.txtDateBon.ReadOnly = true;
            this.txtDateBon.Size = new System.Drawing.Size(97, 20);
            this.txtDateBon.TabIndex = 2;
            // 
            // txtNoBon
            // 
            this.txtNoBon.Location = new System.Drawing.Point(48, 24);
            this.txtNoBon.Name = "txtNoBon";
            this.txtNoBon.Size = new System.Drawing.Size(97, 20);
            this.txtNoBon.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "N°";
            // 
            // btAfficher
            // 
            this.btAfficher.Location = new System.Drawing.Point(150, 23);
            this.btAfficher.Name = "btAfficher";
            this.btAfficher.Size = new System.Drawing.Size(77, 23);
            this.btAfficher.TabIndex = 0;
            this.btAfficher.Text = "Afficher >>";
            this.btAfficher.UseVisualStyleBackColor = true;
            this.btAfficher.Click += new System.EventHandler(this.btAfficher_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(27, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(92, 27);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(123, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(95, 27);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(526, 305);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(226, 35);
            this.xpannel1.TabIndex = 17;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(257, 10);
            this.dgv.Margin = new System.Windows.Forms.Padding(2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(495, 288);
            this.dgv.TabIndex = 16;
            // 
            // FrFactureFournisseur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 352);
            this.Controls.Add(this.gbpayement);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Name = "FrFactureFournisseur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Payement Fournisseur";
            this.panCheque.ResumeLayout(false);
            this.panCheque.PerformLayout();
            this.gbpayement.ResumeLayout(false);
            this.gbpayement.PerformLayout();
            this.panTimbre.ResumeLayout(false);
            this.panTimbre.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panSolde.ResumeLayout(false);
            this.panSolde.PerformLayout();
            this.xpannel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panCheque;
        private System.Windows.Forms.DateTimePicker dtDateCheque;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCheque;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbMode;
        private System.Windows.Forms.Label labMode;
        private System.Windows.Forms.TextBox txtMontant;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbpayement;
        private System.Windows.Forms.Panel panTimbre;
        private System.Windows.Forms.TextBox txtTimbre;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNetPayer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtClient;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panSolde;
        private System.Windows.Forms.TextBox txtSolde;
        private System.Windows.Forms.TextBox txtDateBon;
        private System.Windows.Forms.TextBox txtNoBon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btAfficher;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPrixU;
        private System.Windows.Forms.Label label6;
    }
}