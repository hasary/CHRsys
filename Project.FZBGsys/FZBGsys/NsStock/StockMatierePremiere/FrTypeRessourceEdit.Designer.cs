﻿namespace FZBGsys.NsStock.StockMatierePremiere
{
    partial class FrTypeRessourceEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbClasse = new System.Windows.Forms.ComboBox();
            this.txtUM = new System.Windows.Forms.TextBox();
            this.txtUP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.raUnite = new System.Windows.Forms.RadioButton();
            this.raPak = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.raManuel = new System.Windows.Forms.RadioButton();
            this.raAuto = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUparPack = new System.Windows.Forms.TextBox();
            this.chqttFix = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(103, 26);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(268, 22);
            this.txtNom.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Type";
            // 
            // cbClasse
            // 
            this.cbClasse.FormattingEnabled = true;
            this.cbClasse.Location = new System.Drawing.Point(103, 74);
            this.cbClasse.Name = "cbClasse";
            this.cbClasse.Size = new System.Drawing.Size(130, 24);
            this.cbClasse.TabIndex = 3;
            // 
            // txtUM
            // 
            this.txtUM.Location = new System.Drawing.Point(145, 128);
            this.txtUM.Name = "txtUM";
            this.txtUM.Size = new System.Drawing.Size(88, 22);
            this.txtUM.TabIndex = 0;
            // 
            // txtUP
            // 
            this.txtUP.Location = new System.Drawing.Point(145, 167);
            this.txtUP.Name = "txtUP";
            this.txtUP.Size = new System.Drawing.Size(88, 22);
            this.txtUP.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Unite de mesure";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Unite pack";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(285, 344);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 30);
            this.button1.TabIndex = 5;
            this.button1.Text = "Enregistrer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(175, 344);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 30);
            this.button2.TabIndex = 5;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Unite Courante";
            // 
            // raUnite
            // 
            this.raUnite.AutoSize = true;
            this.raUnite.Checked = true;
            this.raUnite.Location = new System.Drawing.Point(145, 214);
            this.raUnite.Name = "raUnite";
            this.raUnite.Size = new System.Drawing.Size(62, 21);
            this.raUnite.TabIndex = 7;
            this.raUnite.TabStop = true;
            this.raUnite.Text = "Unité";
            this.raUnite.UseVisualStyleBackColor = true;
            // 
            // raPak
            // 
            this.raPak.AutoSize = true;
            this.raPak.Location = new System.Drawing.Point(240, 214);
            this.raPak.Name = "raPak";
            this.raPak.Size = new System.Drawing.Size(168, 21);
            this.raPak.TabIndex = 7;
            this.raPak.Text = "Pack (palettes/carton)";
            this.raPak.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(50, 258);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Transfert";
            // 
            // raManuel
            // 
            this.raManuel.AutoSize = true;
            this.raManuel.Checked = true;
            this.raManuel.Location = new System.Drawing.Point(10, 8);
            this.raManuel.Name = "raManuel";
            this.raManuel.Size = new System.Drawing.Size(75, 21);
            this.raManuel.TabIndex = 7;
            this.raManuel.TabStop = true;
            this.raManuel.Text = "Manuel";
            this.raManuel.UseVisualStyleBackColor = true;
            // 
            // raAuto
            // 
            this.raAuto.AutoSize = true;
            this.raAuto.Location = new System.Drawing.Point(104, 8);
            this.raAuto.Name = "raAuto";
            this.raAuto.Size = new System.Drawing.Size(108, 21);
            this.raAuto.TabIndex = 7;
            this.raAuto.Text = "Automatique";
            this.raAuto.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.raAuto);
            this.panel1.Controls.Add(this.raManuel);
            this.panel1.Location = new System.Drawing.Point(135, 246);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 39);
            this.panel1.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(282, 103);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Unite par Pack";
            // 
            // txtUparPack
            // 
            this.txtUparPack.Location = new System.Drawing.Point(283, 128);
            this.txtUparPack.Name = "txtUparPack";
            this.txtUparPack.Size = new System.Drawing.Size(88, 22);
            this.txtUparPack.TabIndex = 0;
            // 
            // chqttFix
            // 
            this.chqttFix.AutoSize = true;
            this.chqttFix.Location = new System.Drawing.Point(283, 163);
            this.chqttFix.Name = "chqttFix";
            this.chqttFix.Size = new System.Drawing.Size(63, 21);
            this.chqttFix.TabIndex = 11;
            this.chqttFix.Text = "Fixe?";
            this.chqttFix.UseVisualStyleBackColor = true;
            // 
            // FrTypeRessourceEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 401);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chqttFix);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtUM);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtUP);
            this.Controls.Add(this.raUnite);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbClasse);
            this.Controls.Add(this.txtUparPack);
            this.Controls.Add(this.raPak);
            this.Name = "FrTypeRessourceEdit";
            this.Text = "Matiere Premiere";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrTypeRessourceEdit_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbClasse;
        private System.Windows.Forms.TextBox txtUM;
        private System.Windows.Forms.TextBox txtUP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton raUnite;
        private System.Windows.Forms.RadioButton raPak;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton raManuel;
        private System.Windows.Forms.RadioButton raAuto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtUparPack;
        private System.Windows.Forms.CheckBox chqttFix;
    }
}