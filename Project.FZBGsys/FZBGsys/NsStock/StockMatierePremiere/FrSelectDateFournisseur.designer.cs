﻿namespace FZBGsys.NsStock.StockMatierePremiere
{
    partial class FrSelectDateFournisseur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.raMensuel = new System.Windows.Forms.RadioButton();
            this.panelPeriode = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtMoisAu = new System.Windows.Forms.DateTimePicker();
            this.dtMoisDu = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtAnnee = new System.Windows.Forms.DateTimePicker();
            this.raAnnuel = new System.Windows.Forms.RadioButton();
            this.panelPeriode.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // raMensuel
            // 
            this.raMensuel.AutoSize = true;
            this.raMensuel.Checked = true;
            this.raMensuel.Location = new System.Drawing.Point(21, 103);
            this.raMensuel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.raMensuel.Name = "raMensuel";
            this.raMensuel.Size = new System.Drawing.Size(155, 21);
            this.raMensuel.TabIndex = 30;
            this.raMensuel.TabStop = true;
            this.raMensuel.Text = "Rapports Mensuels:";
            this.raMensuel.UseVisualStyleBackColor = true;
            this.raMensuel.CheckedChanged += new System.EventHandler(this.radioButtonPeriode_CheckedChanged);
            // 
            // panelPeriode
            // 
            this.panelPeriode.Controls.Add(this.label3);
            this.panelPeriode.Controls.Add(this.label5);
            this.panelPeriode.Controls.Add(this.dtMoisAu);
            this.panelPeriode.Controls.Add(this.dtMoisDu);
            this.panelPeriode.Location = new System.Drawing.Point(69, 126);
            this.panelPeriode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelPeriode.Name = "panelPeriode";
            this.panelPeriode.Size = new System.Drawing.Size(316, 76);
            this.panelPeriode.TabIndex = 28;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Au:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Du:";
            // 
            // dtMoisAu
            // 
            this.dtMoisAu.CustomFormat = "MMMM yyyy";
            this.dtMoisAu.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtMoisAu.Location = new System.Drawing.Point(111, 41);
            this.dtMoisAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtMoisAu.Name = "dtMoisAu";
            this.dtMoisAu.ShowUpDown = true;
            this.dtMoisAu.Size = new System.Drawing.Size(149, 22);
            this.dtMoisAu.TabIndex = 34;
            // 
            // dtMoisDu
            // 
            this.dtMoisDu.CustomFormat = "MMMM yyyy";
            this.dtMoisDu.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtMoisDu.Location = new System.Drawing.Point(111, 5);
            this.dtMoisDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtMoisDu.Name = "dtMoisDu";
            this.dtMoisDu.ShowUpDown = true;
            this.dtMoisDu.Size = new System.Drawing.Size(149, 22);
            this.dtMoisDu.TabIndex = 34;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(339, 302);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(159, 30);
            this.button1.TabIndex = 31;
            this.button1.Text = "Générer le Rapport";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dtAnnee);
            this.groupBox1.Controls.Add(this.raMensuel);
            this.groupBox1.Controls.Add(this.raAnnuel);
            this.groupBox1.Controls.Add(this.panelPeriode);
            this.groupBox1.Location = new System.Drawing.Point(20, 22);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(477, 274);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selection Fournisseur et  période";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(143, 41);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(303, 24);
            this.cbFournisseur.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 37;
            this.label1.Text = "Fournisseur:";
            // 
            // dtAnnee
            // 
            this.dtAnnee.CustomFormat = "    année   yyyy";
            this.dtAnnee.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtAnnee.Location = new System.Drawing.Point(180, 219);
            this.dtAnnee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtAnnee.Name = "dtAnnee";
            this.dtAnnee.ShowUpDown = true;
            this.dtAnnee.Size = new System.Drawing.Size(149, 22);
            this.dtAnnee.TabIndex = 34;
            this.dtAnnee.Visible = false;
            // 
            // raAnnuel
            // 
            this.raAnnuel.AutoSize = true;
            this.raAnnuel.Location = new System.Drawing.Point(21, 220);
            this.raAnnuel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.raAnnuel.Name = "raAnnuel";
            this.raAnnuel.Size = new System.Drawing.Size(150, 21);
            this.raAnnuel.TabIndex = 33;
            this.raAnnuel.TabStop = true;
            this.raAnnuel.Text = "Rapports Annuels :";
            this.raAnnuel.UseVisualStyleBackColor = true;
            this.raAnnuel.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // FrSelectDateFournisseur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 346);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrSelectDateFournisseur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rapport Personalisé";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrSelectDateFournisseur_FormClosed);
            this.panelPeriode.ResumeLayout(false);
            this.panelPeriode.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton raMensuel;
        private System.Windows.Forms.Panel panelPeriode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtAnnee;
        private System.Windows.Forms.RadioButton raAnnuel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.DateTimePicker dtMoisAu;
        private System.Windows.Forms.DateTimePicker dtMoisDu;
    }
}