﻿namespace FZBGsys.NsStock
{
    partial class FrStockMatiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btPrint = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtAu = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtDu = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.chEnstock = new System.Windows.Forms.CheckBox();
            this.chConsome = new System.Windows.Forms.CheckBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.btRecherche = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTypeRessource = new System.Windows.Forms.ComboBox();
            this.labFilterTxt = new System.Windows.Forms.Label();
            this.panSelect = new System.Windows.Forms.Panel();
            this.btSelect = new System.Windows.Forms.Button();
            this.btCorriger = new System.Windows.Forms.Button();
            this.btEffacer = new System.Windows.Forms.Button();
            this.panEdit = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTypeArome = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panDate.SuspendLayout();
            this.panSelect.SuspendLayout();
            this.panEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(218, 37);
            this.dgv.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(596, 292);
            this.dgv.TabIndex = 7;
            // 
            // btPrint
            // 
            this.btPrint.Location = new System.Drawing.Point(612, 342);
            this.btPrint.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(114, 28);
            this.btPrint.TabIndex = 8;
            this.btPrint.Text = "Imprimer La liste";
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(730, 342);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(84, 28);
            this.btFermer.TabIndex = 9;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panDate);
            this.groupBox1.Controls.Add(this.chEnstock);
            this.groupBox1.Controls.Add(this.chConsome);
            this.groupBox1.Controls.Add(this.cbTypeArome);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btRecherche);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbTypeRessource);
            this.groupBox1.Location = new System.Drawing.Point(6, 8);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(204, 362);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recherche";
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtAu);
            this.panDate.Controls.Add(this.label1);
            this.panDate.Controls.Add(this.dtDu);
            this.panDate.Controls.Add(this.label3);
            this.panDate.Location = new System.Drawing.Point(23, 228);
            this.panDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(160, 57);
            this.panDate.TabIndex = 23;
            this.panDate.Visible = false;
            // 
            // dtAu
            // 
            this.dtAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtAu.Location = new System.Drawing.Point(47, 31);
            this.dtAu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtAu.Name = "dtAu";
            this.dtAu.Size = new System.Drawing.Size(105, 20);
            this.dtAu.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Et:";
            // 
            // dtDu
            // 
            this.dtDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDu.Location = new System.Drawing.Point(47, 8);
            this.dtDu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtDu.Name = "dtDu";
            this.dtDu.Size = new System.Drawing.Size(105, 20);
            this.dtDu.TabIndex = 1;
            this.dtDu.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Entre:";
            // 
            // chEnstock
            // 
            this.chEnstock.AutoSize = true;
            this.chEnstock.Checked = true;
            this.chEnstock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chEnstock.Location = new System.Drawing.Point(114, 206);
            this.chEnstock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chEnstock.Name = "chEnstock";
            this.chEnstock.Size = new System.Drawing.Size(70, 17);
            this.chEnstock.TabIndex = 22;
            this.chEnstock.Text = "En Stock";
            this.chEnstock.UseVisualStyleBackColor = true;
            // 
            // chConsome
            // 
            this.chConsome.AutoSize = true;
            this.chConsome.Location = new System.Drawing.Point(23, 206);
            this.chConsome.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chConsome.Name = "chConsome";
            this.chConsome.Size = new System.Drawing.Size(70, 17);
            this.chConsome.TabIndex = 22;
            this.chConsome.Text = "Consomé";
            this.chConsome.UseVisualStyleBackColor = true;
            this.chConsome.CheckedChanged += new System.EventHandler(this.chConsome_CheckedChanged);
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(23, 96);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(162, 21);
            this.cbFournisseur.TabIndex = 17;
            // 
            // btRecherche
            // 
            this.btRecherche.Location = new System.Drawing.Point(3, 329);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 8;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 79);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Fournisseur:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Categorie: ";
            // 
            // cbTypeRessource
            // 
            this.cbTypeRessource.FormattingEnabled = true;
            this.cbTypeRessource.Location = new System.Drawing.Point(23, 45);
            this.cbTypeRessource.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbTypeRessource.Name = "cbTypeRessource";
            this.cbTypeRessource.Size = new System.Drawing.Size(162, 21);
            this.cbTypeRessource.TabIndex = 14;
            this.cbTypeRessource.SelectedIndexChanged += new System.EventHandler(this.cbCategorie_SelectedIndexChanged);
            // 
            // labFilterTxt
            // 
            this.labFilterTxt.AutoSize = true;
            this.labFilterTxt.Location = new System.Drawing.Point(216, 8);
            this.labFilterTxt.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labFilterTxt.Name = "labFilterTxt";
            this.labFilterTxt.Size = new System.Drawing.Size(52, 13);
            this.labFilterTxt.TabIndex = 11;
            this.labFilterTxt.Text = "               ";
            // 
            // panSelect
            // 
            this.panSelect.Controls.Add(this.btSelect);
            this.panSelect.Location = new System.Drawing.Point(220, 337);
            this.panSelect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panSelect.Name = "panSelect";
            this.panSelect.Size = new System.Drawing.Size(145, 35);
            this.panSelect.TabIndex = 12;
            this.panSelect.Visible = false;
            // 
            // btSelect
            // 
            this.btSelect.Location = new System.Drawing.Point(2, 5);
            this.btSelect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btSelect.Name = "btSelect";
            this.btSelect.Size = new System.Drawing.Size(101, 26);
            this.btSelect.TabIndex = 0;
            this.btSelect.Text = "Selectionner";
            this.btSelect.UseVisualStyleBackColor = true;
            this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
            // 
            // btCorriger
            // 
            this.btCorriger.Location = new System.Drawing.Point(9, 2);
            this.btCorriger.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btCorriger.Name = "btCorriger";
            this.btCorriger.Size = new System.Drawing.Size(76, 24);
            this.btCorriger.TabIndex = 13;
            this.btCorriger.Text = "Corriger";
            this.btCorriger.UseVisualStyleBackColor = true;
            this.btCorriger.Click += new System.EventHandler(this.btCorriger_Click);
            // 
            // btEffacer
            // 
            this.btEffacer.Location = new System.Drawing.Point(89, 2);
            this.btEffacer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btEffacer.Name = "btEffacer";
            this.btEffacer.Size = new System.Drawing.Size(76, 24);
            this.btEffacer.TabIndex = 13;
            this.btEffacer.Text = "Effacer";
            this.btEffacer.UseVisualStyleBackColor = true;
            this.btEffacer.Click += new System.EventHandler(this.btEffacer_Click);
            // 
            // panEdit
            // 
            this.panEdit.Controls.Add(this.btEffacer);
            this.panEdit.Controls.Add(this.btCorriger);
            this.panEdit.Location = new System.Drawing.Point(392, 342);
            this.panEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panEdit.Name = "panEdit";
            this.panEdit.Size = new System.Drawing.Size(185, 35);
            this.panEdit.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 131);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Type Arome:";
            this.label4.Visible = false;
            // 
            // cbTypeArome
            // 
            this.cbTypeArome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeArome.FormattingEnabled = true;
            this.cbTypeArome.Location = new System.Drawing.Point(21, 148);
            this.cbTypeArome.Margin = new System.Windows.Forms.Padding(2);
            this.cbTypeArome.Name = "cbTypeArome";
            this.cbTypeArome.Size = new System.Drawing.Size(162, 21);
            this.cbTypeArome.TabIndex = 17;
            this.cbTypeArome.Visible = false;
            // 
            // FrStockMatiere
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 382);
            this.Controls.Add(this.panEdit);
            this.Controls.Add(this.panSelect);
            this.Controls.Add(this.labFilterTxt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.btPrint);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrStockMatiere";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Stock Matière Première";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrListArrivage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panSelect.ResumeLayout(false);
            this.panEdit.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTypeRessource;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.Label labFilterTxt;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtAu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtDu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chConsome;
        private System.Windows.Forms.CheckBox chEnstock;
        private System.Windows.Forms.Panel panSelect;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.Button btCorriger;
        private System.Windows.Forms.Button btEffacer;
        private System.Windows.Forms.Panel panEdit;
        private System.Windows.Forms.ComboBox cbTypeArome;
        private System.Windows.Forms.Label label4;
    }
}