﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockMatierePremiere
{
    public partial class FrListeFromat : Form
    {
        public FrListeFromat()
        {
            InitializeComponent();
            refrecheGrid();

        }

      


        private void refrecheGrid()
        {
            var db = new ModelEntities();

           
            dgv.DataSource = db.Formats.Select(p => new
            {
                Id = p.ID,
                Nom = p.Volume,
                Boutille_fardeau = p.BouteillesParFardeau,
                fardeau_pallette = p.FardeauParPalette
            }).ToList();

            db.Dispose();
        }

       
        private void dgv_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void btnov_Click(object sender, EventArgs e)
        {
           

            FrEditFormat form = new FrEditFormat();

            form.ShowDialog();

            
        }

        private void btsup_Click(object sender, EventArgs e)
        {
            var id = int.Parse(dgv.SelectedRows[0].Cells[0].Value.ToString());
            using (var db = new ModelEntities())
            {
                var T = db.Formats.Single(p=> p.ID == id);
                db.DeleteObject(T);
                db.SaveChanges();
                
            }
            refrecheGrid();
        }

        private void btmod_Click(object sender, EventArgs e)
        {
            var id = int.Parse(dgv.SelectedRows[0].Cells[0].Value.ToString());
            new FrEditFormat(id).ShowDialog();
            refrecheGrid();
        }
    }
}
