﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys;

using FZBGsys.NsReports.Stock;

namespace FZBGsys.NsStock
{
    public partial class FrBesoinMatiere : Form
    {
        ModelEntities db = new ModelEntities();
        public FrBesoinMatiere()
        {
            InitializeComponent();
            InitialiseControls();
            //  this.Destination = Destination;
            ListToSave = new List<DemandeMatiere>();
            btAuto.Visible = Tools.CurrentUserID == 1;
        }

        private void InitialiseControls()
        {
            //---------------------- Fournisseur
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            // cbFournisseur.Items.AddRange(db.Fournisseurs.Where(p => p.ID > 0 ).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;

            panObs.Visible = false;// Destination == EnSection.Autre;

            UnLoadMatiere();
        }


        private void btOuvrirStock_Click(object sender, EventArgs e)
        {
            var id = FZBGsys.NsStock.FrStockMatiere.SelectIDStockMTAllDialog(db);
            if (id != null)
            {
                SelectedStockMT = db.StockMatieres.SingleOrDefault(p => p.ID == id.Value);
                if (SelectedStockMT == null)
                {
                    this.ShowError("Impossible de trouver matiere selectionnée!");
                    UnLoadMatiere();
                    return;
                }
                /*else if (ListToSave.Select(p=>p.StockMatiereID).ToList().Contains(SelectedStockMT.ID))
                {
                    var qttEnstock = ListToSave.Where(p => p.StockMatiereID == SelectedStockMT.ID).Sum(p=>p.QunatiteUM);

                    if (qttEnstock   >= SelectedStockMT.Quantite   )
                    {
                        this.ShowError("Quantie en stock insuffisante (déja saisie)");
                        UnLoadMatiere();
                        return;
                    }
                }*/
                LoadMatiere();
            }



        }

        private void LoadMatiere()
        {
            UnLoadMatiere();
            gbMatiere.Enabled = true;
            var ressource = SelectedStockMT.Ressource;

            txtMatiere.Text = ressource.ToDescription();
            cbFournisseur.Items.Clear();
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.TypeRessources.Contains(ressource.TypeRessource)).ToArray());
            cbFournisseur.SelectedItem = ressource.Fournisseur;
            txtQttEnStock.Text = SelectedStockMT.Quantite.ToAffInt();
            txtPiceEnStock.Text = SelectedStockMT.Piece.ToString();
            labUniteDeMesure.Text = labUniteDeMesure2.Text = ressource.TypeRessource.UniteDeMesure;
            labUniteDePiece.Text = labUniteDePiece2.Text = ressource.TypeRessource.UniteDePiece;
            //  txtPiece.Visible = ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece;
            txtPiceEnStock.Enabled = ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece;
        }

        private void UnLoadMatiere()
        {
            txtMatiere.Text = "";
            cbFournisseur.Items.Clear();
            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });


            txtQttEnStock.Text = "";
            txtPiceEnStock.Text = "";

            labUniteDeMesure.Text = labUniteDeMesure2.Text = "";
            labUniteDePiece.Text = labUniteDePiece2.Text = "";
            gbMatiere.Enabled = false;
        }

        //    internal EnSection Destination { get; set; }

        private void FrTransfert_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        public StockMatiere SelectedStockMT { get; set; }

        private void chDateArrivage_CheckedChanged(object sender, EventArgs e)
        {
            dtDateArrivage.Visible = chDateArrivage.Checked;
        }

        private void chDateFabrication_CheckedChanged(object sender, EventArgs e)
        {
            //    dtDateFabrication.Visible = chDateFabrication.Checked;
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (BonDemande == null)
            {
                this.BonDemande = new BonDemande();
                BonDemande.Date = DateTime.Now;
            }


            if (!isValidAll())
            {
                return;
            }


            var newDemande = new DemandeMatiere();
            newDemande.BonDemande = BonDemande;
            if (panObs.Visible)
            {
                newDemande.Observation = txtObservation.Text.Trim();
            }

            //  newDemande.DestinationSectionID = (int)Destination;

            newDemande.Ressource = SelectedStockMT.Ressource;
            newDemande.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
            newDemande.QunatiteUM = txtQuantite.Text.ParseToDec();
            if (SelectedStockMT.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newDemande.QuantitePiece = txtPiece.Text.ParseToInt();

            //newTransfert.StockMatiere = SelectedStockMT;

            //  FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newDemande, db);
            ListToSave.Add(newDemande);
            db.AddToDemandeMatieres(newDemande);

            RefreshGrid();
            UpdateControls();

        }

        private void UpdateControls()
        {
            UnLoadMatiere();
            txtQuantite.Text = "";
            txtPiece.Text = "";
        }

        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Date = p.BonDemande.Date.Value.ToShortDateString(),
                Matiere = p.Ressource.ToDescription(),
                Fournisseur = p.Fournisseur.Nom,
                //   Fabrication = (p.DateFabrication != null) ? p.DateFabrication.Value.ToShortDateString() : "",
                Quantie = (p.QuantitePiece != null) ? p.QuantitePiece + " " + p.Ressource.TypeRessource.UniteDePiece : "/",
                Total = p.QunatiteUM + " " + p.Ressource.TypeRessource.UniteDeMesure,


            }).ToList();

            dgv.HideIDColumn();
        }
        public List<DemandeMatiere> ListToSave { get; set; }
        private bool isValidAll()
        {
            var message = "";

            if (SelectedStockMT == null)
            {
                message = "Selectionner matiere première avec boutton \"Ouvrir le stock\"";

            }
            else
            {
                if (txtQuantite.Text.ParseToDec() == null && txtPiece.Text.ParseToInt() == null)
                {
                    message += "\nQuantite invalide!";
                }
                /*  else if (txtQuantite.Text.ParseToDec() > txtQttEnStock.Text.ParseToDec())
                  {
                      message += "\nQuantite Insufissante !!";
                  }*/

                if (SelectedStockMT.Ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece)
                {

                    if (txtPiece.Text.ParseToInt() == null)
                    {
                        //   message += "\nNombre invalide!";
                    }
                    else if (txtPiece.Text.ParseToInt() > txtPiceEnStock.Text.ParseToInt())
                    {
                        // message += "\nQuantite Insufissante !!";
                    }
                }
            }

            if (message == "")
            {
                return true;
            }


            this.ShowWarning(message);
            return false;
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0) Dispose();
            else
                if (this.ConfirmWarning("Annuler tout et fermer ?\n(ATTENTION: rien ne sera enregistré!)"))
                {
                    Dispose();
                }
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                this.ShowInformation("La liste est vide, remplire d'abord avec \"Ajouter\" ");
                return;
            }

            BonDemande.Date = dtDate.Value.Date;
            BonDemande.InscriptionDate = Tools.GetServerDateTime();
            BonDemande.InscriptionUID = Tools.CurrentUserID;
            //      FZBGsys.NsStock.FrStockMatiere.RemoveRangeFromStockMT(ListToSave, db);

            try
            {
                db.SaveChanges();

                StockReportController.PrintDemandeMatiere(BonDemande.ID);


                Dispose();
            }
            catch (Exception exp)
            {

                this.ShowError(exp.AllMessages("Impossible d'enregistrer:"));
            }

        }

        private void PrintReport()
        {

            Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before

            foreach (var demande in ListToSave)
            {
                db.AddToPrintTemps(new PrintTemp()
                {
                    Date = demande.BonDemande.Date,
                    Description = demande.Ressource.ToDescription(),
                    Nom = demande.Ressource.Fournisseur.Nom,
                    Piece = demande.QuantitePiece,
                    Quantite = demande.QunatiteUM,
                    UID = Tools.CurrentUserID,
                    val1 = demande.Ressource.TypeRessource.UniteDeMesure,
                    val2 = demande.Ressource.TypeRessource.UniteDePiece,
                    val3 = BonDemande.ID.ToString(),
                });
            }

            db.SaveChanges();


            //FZBGsys.NsReports.FrViewer.PrintDemandeMatiere();

        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.SelectedRows[0].Index;
            var toDel = ListToSave.ElementAt(index);
            ListToSave.Remove(toDel);

            //   var toDelMatiere = toDel.StockMatiere;
            //  FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
            //    db.DeleteObject(toDelMatiere);

            db.DeleteObject(toDel);
            RefreshGrid();

        }

        private void txtPiece_TextChanged(object sender, EventArgs e)
        {
            if (txtPiece.Text.ParseToInt() != null && SelectedStockMT.Ressource.TypeRessource.QuantiteParPieceFix == true)
            {
                txtQuantite.Text = (txtPiece.Text.ParseToInt() * SelectedStockMT.Ressource.TypeRessource.QuantiteParPiece).ToString();
            }
        }

        private void txtObservation_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLower(e.KeyChar))
            {
                e.KeyChar = char.ToUpper(e.KeyChar);
            }
        }

        public BonDemande BonDemande { get; set; }

        private void btAuto_Click(object sender, EventArgs e)
        {
            var begin = DateTime.Parse("2013-06-01");
            var end   = DateTime.Parse("2014-05-31");
            Random r  = new Random();
            var arrivages = db.Arrivages.Where(p => p.DateArrivage >= begin && p.DateArrivage <= end);

            foreach (var arr in arrivages)
            {
                var Bon = new BonDemande();
                Bon.Date = arr.DateArrivage.Value.AddDays(3 + r.Next(1000) % 5);
                Bon.InscriptionDate = arr.DateArrivage.Value.AddDays(3 + r.Next(1000) % 5).AddHours(8 + r.Next(1000) % 6);
                Bon.InscriptionUID = 35;

                var newDemande = new DemandeMatiere();
                newDemande.BonDemande = Bon;
                newDemande.Ressource = arr.Ressource;
                newDemande.Fournisseur = arr.Ressource.Fournisseur;
                newDemande.QunatiteUM = arr.Quantite;
                newDemande.QuantitePiece = arr.Piece;
                db.BonDemandes.AddObject(Bon);

            }

            db.SaveChanges();
            Tools.ShowInformation("done.");

        }
    }
}
