﻿namespace FZBGsys.NsStock
{
    partial class FrArrivage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panGoutProduit = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.cbGoutProduit = new System.Windows.Forms.ComboBox();
            this.chDateFab = new System.Windows.Forms.CheckBox();
            this.labFixQtperNombre = new System.Windows.Forms.Label();
            this.panTypeEtiquette = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.cbTypeEtiquette = new System.Windows.Forms.ComboBox();
            this.txtPiece = new System.Windows.Forms.TextBox();
            this.panTypeColorant = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.cbTypeColorant = new System.Windows.Forms.ComboBox();
            this.PanCole = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.cbTypeCole = new System.Windows.Forms.ComboBox();
            this.labUniteDePiece = new System.Windows.Forms.Label();
            this.panTypeArome = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.cbArrome = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panInterCalaireFormat = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.cbFormatIntercalaire = new System.Windows.Forms.ComboBox();
            this.dtFab = new System.Windows.Forms.DateTimePicker();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.panBouchon = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cbCouleurBouchon = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbTypeBouchon = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panGamme = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cbGamme = new System.Windows.Forms.ComboBox();
            this.labUniteDeMesure = new System.Windows.Forms.Label();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.panFormat = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbCategorie = new System.Windows.Forms.ComboBox();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btAjouter = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.panGoutProduit.SuspendLayout();
            this.panTypeEtiquette.SuspendLayout();
            this.panTypeColorant.SuspendLayout();
            this.PanCole.SuspendLayout();
            this.panTypeArome.SuspendLayout();
            this.panInterCalaireFormat.SuspendLayout();
            this.panBouchon.SuspendLayout();
            this.panGamme.SuspendLayout();
            this.panFormat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panGoutProduit);
            this.groupBox1.Controls.Add(this.chDateFab);
            this.groupBox1.Controls.Add(this.labFixQtperNombre);
            this.groupBox1.Controls.Add(this.panTypeEtiquette);
            this.groupBox1.Controls.Add(this.txtPiece);
            this.groupBox1.Controls.Add(this.panTypeColorant);
            this.groupBox1.Controls.Add(this.PanCole);
            this.groupBox1.Controls.Add(this.labUniteDePiece);
            this.groupBox1.Controls.Add(this.panTypeArome);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.panInterCalaireFormat);
            this.groupBox1.Controls.Add(this.dtFab);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.panBouchon);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.panGamme);
            this.groupBox1.Controls.Add(this.labUniteDeMesure);
            this.groupBox1.Controls.Add(this.txtQuantite);
            this.groupBox1.Controls.Add(this.panFormat);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbCategorie);
            this.groupBox1.Location = new System.Drawing.Point(12, 50);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(317, 421);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Matière Premiere";
            // 
            // panGoutProduit
            // 
            this.panGoutProduit.Controls.Add(this.label15);
            this.panGoutProduit.Controls.Add(this.cbGoutProduit);
            this.panGoutProduit.Location = new System.Drawing.Point(47, 261);
            this.panGoutProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panGoutProduit.Name = "panGoutProduit";
            this.panGoutProduit.Size = new System.Drawing.Size(251, 37);
            this.panGoutProduit.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 17);
            this.label15.TabIndex = 6;
            this.label15.Text = "Produit:";
            // 
            // cbGoutProduit
            // 
            this.cbGoutProduit.FormattingEnabled = true;
            this.cbGoutProduit.Location = new System.Drawing.Point(67, 7);
            this.cbGoutProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbGoutProduit.Name = "cbGoutProduit";
            this.cbGoutProduit.Size = new System.Drawing.Size(177, 24);
            this.cbGoutProduit.TabIndex = 7;
            // 
            // chDateFab
            // 
            this.chDateFab.AutoSize = true;
            this.chDateFab.Location = new System.Drawing.Point(19, 138);
            this.chDateFab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chDateFab.Name = "chDateFab";
            this.chDateFab.Size = new System.Drawing.Size(134, 21);
            this.chDateFab.TabIndex = 17;
            this.chDateFab.Text = "Date Fabrication";
            this.chDateFab.UseVisualStyleBackColor = true;
            this.chDateFab.CheckedChanged += new System.EventHandler(this.chDateFab_CheckedChanged);
            // 
            // labFixQtperNombre
            // 
            this.labFixQtperNombre.AutoSize = true;
            this.labFixQtperNombre.Location = new System.Drawing.Point(193, 350);
            this.labFixQtperNombre.Name = "labFixQtperNombre";
            this.labFixQtperNombre.Size = new System.Drawing.Size(0, 17);
            this.labFixQtperNombre.TabIndex = 16;
            // 
            // panTypeEtiquette
            // 
            this.panTypeEtiquette.Controls.Add(this.label14);
            this.panTypeEtiquette.Controls.Add(this.cbTypeEtiquette);
            this.panTypeEtiquette.Location = new System.Drawing.Point(15, 223);
            this.panTypeEtiquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panTypeEtiquette.Name = "panTypeEtiquette";
            this.panTypeEtiquette.Size = new System.Drawing.Size(289, 37);
            this.panTypeEtiquette.TabIndex = 11;
            this.panTypeEtiquette.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 6);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 17);
            this.label14.TabIndex = 6;
            this.label14.Text = "Type:";
            // 
            // cbTypeEtiquette
            // 
            this.cbTypeEtiquette.FormattingEnabled = true;
            this.cbTypeEtiquette.Location = new System.Drawing.Point(99, 2);
            this.cbTypeEtiquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeEtiquette.Name = "cbTypeEtiquette";
            this.cbTypeEtiquette.Size = new System.Drawing.Size(177, 24);
            this.cbTypeEtiquette.TabIndex = 7;
            // 
            // txtPiece
            // 
            this.txtPiece.Location = new System.Drawing.Point(113, 334);
            this.txtPiece.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPiece.Name = "txtPiece";
            this.txtPiece.Size = new System.Drawing.Size(100, 22);
            this.txtPiece.TabIndex = 15;
            this.txtPiece.TextChanged += new System.EventHandler(this.txtPiece_TextChanged);
            this.txtPiece.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPiece_KeyPress);
            // 
            // panTypeColorant
            // 
            this.panTypeColorant.Controls.Add(this.label13);
            this.panTypeColorant.Controls.Add(this.cbTypeColorant);
            this.panTypeColorant.Location = new System.Drawing.Point(11, 263);
            this.panTypeColorant.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panTypeColorant.Name = "panTypeColorant";
            this.panTypeColorant.Size = new System.Drawing.Size(289, 30);
            this.panTypeColorant.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 17);
            this.label13.TabIndex = 6;
            this.label13.Text = "Type Colorant:";
            // 
            // cbTypeColorant
            // 
            this.cbTypeColorant.FormattingEnabled = true;
            this.cbTypeColorant.Location = new System.Drawing.Point(123, 2);
            this.cbTypeColorant.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeColorant.Name = "cbTypeColorant";
            this.cbTypeColorant.Size = new System.Drawing.Size(125, 24);
            this.cbTypeColorant.TabIndex = 7;
            // 
            // PanCole
            // 
            this.PanCole.Controls.Add(this.label11);
            this.PanCole.Controls.Add(this.cbTypeCole);
            this.PanCole.Location = new System.Drawing.Point(17, 220);
            this.PanCole.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanCole.Name = "PanCole";
            this.PanCole.Size = new System.Drawing.Size(289, 37);
            this.PanCole.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 6;
            this.label11.Text = "Type:";
            // 
            // cbTypeCole
            // 
            this.cbTypeCole.FormattingEnabled = true;
            this.cbTypeCole.Location = new System.Drawing.Point(99, 2);
            this.cbTypeCole.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeCole.Name = "cbTypeCole";
            this.cbTypeCole.Size = new System.Drawing.Size(177, 24);
            this.cbTypeCole.TabIndex = 7;
            // 
            // labUniteDePiece
            // 
            this.labUniteDePiece.AutoSize = true;
            this.labUniteDePiece.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDePiece.Location = new System.Drawing.Point(219, 336);
            this.labUniteDePiece.Name = "labUniteDePiece";
            this.labUniteDePiece.Size = new System.Drawing.Size(56, 17);
            this.labUniteDePiece.TabIndex = 14;
            this.labUniteDePiece.Text = "boxBid";
            // 
            // panTypeArome
            // 
            this.panTypeArome.Controls.Add(this.label3);
            this.panTypeArome.Controls.Add(this.cbArrome);
            this.panTypeArome.Location = new System.Drawing.Point(17, 263);
            this.panTypeArome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panTypeArome.Name = "panTypeArome";
            this.panTypeArome.Size = new System.Drawing.Size(289, 37);
            this.panTypeArome.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Type Arrome";
            // 
            // cbArrome
            // 
            this.cbArrome.FormattingEnabled = true;
            this.cbArrome.Location = new System.Drawing.Point(99, 2);
            this.cbArrome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbArrome.Name = "cbArrome";
            this.cbArrome.Size = new System.Drawing.Size(177, 24);
            this.cbArrome.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(37, 337);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "Nombre:";
            // 
            // panInterCalaireFormat
            // 
            this.panInterCalaireFormat.Controls.Add(this.label12);
            this.panInterCalaireFormat.Controls.Add(this.cbFormatIntercalaire);
            this.panInterCalaireFormat.Location = new System.Drawing.Point(15, 177);
            this.panInterCalaireFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panInterCalaireFormat.Name = "panInterCalaireFormat";
            this.panInterCalaireFormat.Size = new System.Drawing.Size(289, 37);
            this.panInterCalaireFormat.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Format:";
            // 
            // cbFormatIntercalaire
            // 
            this.cbFormatIntercalaire.FormattingEnabled = true;
            this.cbFormatIntercalaire.Location = new System.Drawing.Point(99, 2);
            this.cbFormatIntercalaire.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormatIntercalaire.Name = "cbFormatIntercalaire";
            this.cbFormatIntercalaire.Size = new System.Drawing.Size(177, 24);
            this.cbFormatIntercalaire.TabIndex = 7;
            // 
            // dtFab
            // 
            this.dtFab.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFab.Location = new System.Drawing.Point(157, 138);
            this.dtFab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtFab.Name = "dtFab";
            this.dtFab.Size = new System.Drawing.Size(137, 22);
            this.dtFab.TabIndex = 0;
            this.dtFab.Visible = false;
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(25, 95);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(271, 24);
            this.cbFournisseur.TabIndex = 13;
            // 
            // panBouchon
            // 
            this.panBouchon.Controls.Add(this.label5);
            this.panBouchon.Controls.Add(this.cbCouleurBouchon);
            this.panBouchon.Controls.Add(this.label7);
            this.panBouchon.Controls.Add(this.cbTypeBouchon);
            this.panBouchon.Location = new System.Drawing.Point(19, 220);
            this.panBouchon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panBouchon.Name = "panBouchon";
            this.panBouchon.Size = new System.Drawing.Size(291, 90);
            this.panBouchon.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Couleur:";
            // 
            // cbCouleurBouchon
            // 
            this.cbCouleurBouchon.FormattingEnabled = true;
            this.cbCouleurBouchon.Location = new System.Drawing.Point(99, 53);
            this.cbCouleurBouchon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCouleurBouchon.Name = "cbCouleurBouchon";
            this.cbCouleurBouchon.Size = new System.Drawing.Size(177, 24);
            this.cbCouleurBouchon.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Type:";
            // 
            // cbTypeBouchon
            // 
            this.cbTypeBouchon.FormattingEnabled = true;
            this.cbTypeBouchon.Location = new System.Drawing.Point(99, 7);
            this.cbTypeBouchon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeBouchon.Name = "cbTypeBouchon";
            this.cbTypeBouchon.Size = new System.Drawing.Size(177, 24);
            this.cbTypeBouchon.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 12;
            this.label6.Text = "Fournisseur:";
            // 
            // panGamme
            // 
            this.panGamme.Controls.Add(this.label8);
            this.panGamme.Controls.Add(this.cbGamme);
            this.panGamme.Location = new System.Drawing.Point(19, 177);
            this.panGamme.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panGamme.Name = "panGamme";
            this.panGamme.Size = new System.Drawing.Size(289, 37);
            this.panGamme.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "Gamme:";
            // 
            // cbGamme
            // 
            this.cbGamme.FormattingEnabled = true;
            this.cbGamme.Location = new System.Drawing.Point(99, 2);
            this.cbGamme.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbGamme.Name = "cbGamme";
            this.cbGamme.Size = new System.Drawing.Size(177, 24);
            this.cbGamme.TabIndex = 7;
            // 
            // labUniteDeMesure
            // 
            this.labUniteDeMesure.AutoSize = true;
            this.labUniteDeMesure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure.Location = new System.Drawing.Point(219, 368);
            this.labUniteDeMesure.Name = "labUniteDeMesure";
            this.labUniteDeMesure.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure.TabIndex = 10;
            this.labUniteDeMesure.Text = "UM";
            // 
            // txtQuantite
            // 
            this.txtQuantite.Location = new System.Drawing.Point(113, 369);
            this.txtQuantite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.Size = new System.Drawing.Size(100, 22);
            this.txtQuantite.TabIndex = 16;
            this.txtQuantite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantite_KeyPress);
            // 
            // panFormat
            // 
            this.panFormat.Controls.Add(this.label9);
            this.panFormat.Controls.Add(this.cbFormat);
            this.panFormat.Location = new System.Drawing.Point(19, 177);
            this.panFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panFormat.Name = "panFormat";
            this.panFormat.Size = new System.Drawing.Size(289, 37);
            this.panFormat.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(99, 6);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(177, 24);
            this.cbFormat.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 374);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Quantite:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Categorie: ";
            // 
            // cbCategorie
            // 
            this.cbCategorie.FormattingEnabled = true;
            this.cbCategorie.Location = new System.Drawing.Point(112, 39);
            this.cbCategorie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCategorie.Name = "cbCategorie";
            this.cbCategorie.Size = new System.Drawing.Size(184, 24);
            this.cbCategorie.TabIndex = 2;
            this.cbCategorie.SelectedIndexChanged += new System.EventHandler(this.SelectCategorie);
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(131, 12);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(177, 22);
            this.dtDate.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date Arrivage:";
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(205, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(68, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(132, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(336, 12);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(755, 460);
            this.dgv.TabIndex = 6;
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(197, 480);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 17;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(336, 481);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 4;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(745, 478);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(345, 43);
            this.xpannel1.TabIndex = 7;
            // 
            // FrArrivage
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 526);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrArrivage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Saisie Arrivage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrArrivage_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panGoutProduit.ResumeLayout(false);
            this.panGoutProduit.PerformLayout();
            this.panTypeEtiquette.ResumeLayout(false);
            this.panTypeEtiquette.PerformLayout();
            this.panTypeColorant.ResumeLayout(false);
            this.panTypeColorant.PerformLayout();
            this.PanCole.ResumeLayout(false);
            this.PanCole.PerformLayout();
            this.panTypeArome.ResumeLayout(false);
            this.panTypeArome.PerformLayout();
            this.panInterCalaireFormat.ResumeLayout(false);
            this.panInterCalaireFormat.PerformLayout();
            this.panBouchon.ResumeLayout(false);
            this.panBouchon.PerformLayout();
            this.panGamme.ResumeLayout(false);
            this.panGamme.PerformLayout();
            this.panFormat.ResumeLayout(false);
            this.panFormat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labUniteDeMesure;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbArrome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbCategorie;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panTypeArome;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.TextBox txtPiece;
        private System.Windows.Forms.Label labUniteDePiece;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panFormat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbTypeBouchon;
        private System.Windows.Forms.Panel panGamme;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbGamme;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.Panel panBouchon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbCouleurBouchon;
        private System.Windows.Forms.Panel PanCole;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbTypeCole;
        private System.Windows.Forms.Panel panInterCalaireFormat;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cbFormatIntercalaire;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Label labFixQtperNombre;
        private System.Windows.Forms.DateTimePicker dtFab;
        private System.Windows.Forms.CheckBox chDateFab;
        private System.Windows.Forms.Panel panTypeColorant;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbTypeColorant;
        private System.Windows.Forms.Panel panTypeEtiquette;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbTypeEtiquette;
        private System.Windows.Forms.Panel panGoutProduit;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cbGoutProduit;
    }
}