﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockProduitFini
{
    public partial class FrDistribution : Form
    {
        ModelEntities db = new ModelEntities();
        public FrDistribution()
        {
            
            Listdistribution= new List<StockDistribution>();
            InitializeComponent();
            InitialiseControls();
        }

        private void InitialiseControls()
        {
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID >0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.SelectedIndex = 0;


            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.SelectedIndex = 0;
        }

              
        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (Listdistribution.Count == 0 || Listdistribution == null) return;

            var temp = Tools.CurrentDateTime;

            //var listsession = .SessionProductions.ToList();
            bool modifier = false;
            //if (distributon.EntityState == EntityState.Modified) modifier = true;
            var currentTime = dtDate.Value;
            FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockautoajoute(currentTime, db, Listdistribution);
            try
            {
                db.SaveChanges();
                Dispose();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.AllMessages("erreur"));
            }

        }

        public List<StockDistribution> Listdistribution { get; set; }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }
            var distrubution = new StockDistribution();
            distrubution.Client = textClient.Text;
            distrubution.Format = (Format)cbFormat.SelectedItem;
            distrubution.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;
            distrubution.Date = dtDate.Value;
            if (radPalettes.Checked)
            {
                distrubution.NombrePalettes = txtNombre.Text.ParseToInt();
                distrubution.TotalFardeaux = distrubution.Format.FardeauParPalette * distrubution.NombrePalettes;
                distrubution.TotalBouteilles = distrubution.Format.BouteillesParFardeau * distrubution.TotalFardeaux;
            }
            else
            {
                distrubution.NombreFardeaux = txtNombre.Text.ParseToInt();
                distrubution.TotalFardeaux = distrubution.NombreFardeaux;
                distrubution.TotalBouteilles = distrubution.Format.BouteillesParFardeau * distrubution.NombreFardeaux;
            }
            distrubution.Observation = txtObservation.Text;

            Listdistribution.Add(distrubution);
            db.AddToStockDistributions(distrubution);
            RefreshGrid();


        }

        private void RefreshGrid()
        {
            dgv.DataSource = Listdistribution.Select(p => new
            {
                ID = p.ID,
                Référence = p.NumeroBon,
                Bénéficiaire = p.Client,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                Quantite = (p.NombrePalettes == 0) ? p.NombreFardeaux : p.NombrePalettes,
                Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteilles + " Bouteilles",
            }).ToList();
            dgv.HideIDColumn();
               

        }

        private bool isValidAll()
        {

            if (cbFormat.SelectedIndex < 1)
            { this.ShowWarning("\nSelectionner un Format!"); return false; }
            if (cbGoutProduit.SelectedIndex < 1)
            { this.ShowWarning("\nSelectionner un gout du produit!"); return false; }
            if (txtNombre.Text == "")
            { this.ShowWarning("\nVeulliez entrer nombre de palettes ou fradeaux"); return false; }

            return true;
        }

        
    }
}
