﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockProduitFini
{
    public partial class FrNEProduit : Form
    {
        ModelEntities db = new ModelEntities();
        public GoutProduit Model { get; set; }
        public FrNEProduit(int? id = null)
        {
            InitializeComponent();
            initialiseControle();
            if (id != null)
            {
                LoadModel(id);
            }
        }

        private void LoadModel(int? id)
        {
            Model = db.GoutProduits.Single(p => p.ID == id);
            txtNom.Text = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Model.Nom.ToLower());
            txtConge.Text = Model.ProdTeoriqueLittreParConge.ToString();
            chvente.Checked = Model.UseVente == true;
            chEtiquette.Checked = Model.UseEtiquette == true;
            cbType.SelectedItem = Model.TypeProduit;

            for (int i = 0; i < chl.Items.Count; i++)
            {
                chl.SetItemChecked(i, Model.TypeArromes.Contains((TypeArrome)chl.Items[i]));
                // ((CheckBox)chl.Items[i]).Font.Bold = Fournisseur.TypeRessources.Contains((TypeRessource)chl.Items[i];
            }
        }

        private void initialiseControle()
        {
            cbType.Items.Add(new TypeProduit { ID = 0, Nom = "[Tout]" });
            cbType.Items.AddRange(db.TypeProduits.Where(p => p.ID > 0).ToArray());
            cbType.DisplayMember = "Nom";
            cbType.ValueMember = "ID";
            cbType.DropDownStyle = ComboBoxStyle.DropDownList;
            cbType.SelectedIndex = 0;
            //---------------------------------------------------------------------------
            ((ListBox)this.chl).DataSource = db.TypeArromes.Where(p => p.ID > 0).OrderBy(p => p.Nom).ToList();
            ((ListBox)this.chl).DisplayMember = "Nom";
            ((ListBox)this.chl).ValueMember = "ID";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrNEProduit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Model == null)
            {
                Model = new GoutProduit();
                db.AddToGoutProduits(Model);
            }

            Model.Nom = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(txtNom.Text.ToLower());
            Model.ProdTeoriqueLittreParConge = txtConge.Text.ToString().ParseToInt();
            Model.UseVente = chvente.Checked;
            Model.UseEtiquette = chEtiquette.Checked;
            Model.TypeProduit = (TypeProduit)cbType.SelectedItem;

            Model.TypeArromes.Clear();

            foreach (var selected in chl.CheckedItems)
            {
                Model.TypeArromes.Add((TypeArrome)selected);
            }


            db.SaveChanges();
            Dispose();
        }

        private void txtConge_KeyPress(object sender, KeyPressEventArgs e)
        {


            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }

        }
    }
}
