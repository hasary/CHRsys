﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockProduitFini
{
    public partial class FrStockProduitFiniManual : Form
    {
        ModelEntities db = new ModelEntities();
        public StockProduitFiniManual SelectedStock { get; set; }
        public FrStockProduitFiniManual(bool isViewer = false)
        {
            
            this.ViewerMode = isViewer;
            InitializeComponent();
            dtDate.Value = Tools.CurrentDateTime.Value.Date;
            InitialiseContorls();
            gpSelect.Enabled = !ViewerMode && (Tools.CurrentUser.GroupeID == 1 || Tools.CurrentUser.GroupeID == 2);
            btValid.Enabled = (Tools.CurrentUser.GroupeID == 4 || Tools.CurrentUser.GroupeID == 1);
            LoadStock();

        }

        private void InitialiseContorls()
        {
            //--------------------------- Gout Produit
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbGoutProduit.SelectedIndex = 0;
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------------------------------------------------


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedIndex = 0;
            cbFormat.Items.RemoveAt(3);
            //--------------------------------------------------------------
        }

        /*public static void chargeStock(bool a)
        {
           LoadStock(a);
        }*/

        private void LoadStock()
        {
            
            var date = dtDate.Value.Date;
            var dateHier = dtDate.Value.Date.AddDays(-1);
            var tmp = db.StockProduitFiniManuals.Where(p => p.DateStock == date);
            var stockHier = db.StockProduitFiniManuals.Where(p => p.DateStock == dateHier).ToList();
                /*  if (cbFormat.SelectedIndex > 0)
                  {
                      tmp = tmp.Where(p => p.FormatID == ((Format)cbFormat.SelectedItem).ID);
                  }

                  if (cbGoutProduit.SelectedIndex > 0)
                  {
                      tmp = tmp.Where(p => p.GoutProduitID == ((GoutProduit)cbGoutProduit.SelectedItem).ID);
                  }*/
                var stockdb = tmp.ToList();
                int[] formats = db.Formats.Select(p => p.ID).ToArray(); //{ 3, 5, 8, 1, 6 };
                int[] gouts = db.GoutProduits.Select(p => p.ID).ToArray(); // { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 14, 15 };
                // var existsfromat = stockdb.Select(a=>a.FormatID).ToList();
                //  var existsGouts = stockdb.Select(a => a.GoutProduitID).ToList();
                //var exist = stockHier.Where(p => p.DateStock == date);


                if (stockdb.Count() == 0)
                {
                    while (stockHier.Count() == 0)
                    {
                        dateHier = dateHier.AddDays(-1);
                        stockHier = db.StockProduitFiniManuals.Where(p => p.DateStock == dateHier).ToList();
                    }


                    foreach (var stock in stockHier)
                    {
                        db.AddToStockProduitFiniManuals(new StockProduitFiniManual

                            {
                                DateStock = dtDate.Value,
                                FormatID = stock.FormatID,
                                GoutProduitID = stock.GoutProduitID,
                                isValid = (stock.TotalBouteilles > 0) ? true : false,
                                NombreFardeaux = stock.NombreFardeaux,
                                NombrePalettes = stock.NombrePalettes,
                                TotalBouteilles = stock.TotalBouteilles,
                                take = stock.take,
                                DateTimeValid = stock.DateTimeValid,


                            });
                    }

                    db.SaveChanges();
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.StockProduitFiniManuals);

                

            }
          /*  foreach (var format in formats)
            {
                foreach (var gout in gouts)
                {
                    var exists = stockdb.Where(p => p.FormatID == format && p.GoutProduitID == gout);
                    if (exists.Count() == 0)
                    {
                        db.AddToStockProduitFiniManuals(new StockProduitFiniManual
                                        {
                                            DateStock = dtDate.Value,
                                            FormatID = format,
                                            GoutProduitID = gout,
                                            isValid = false,
                                            NombreFardeaux = 0,
                                            NombrePalettes = 0,
                                            TotalBouteilles = 0,
                                            take = true,
                                            DateTimeValid = (format == 6 || format == 8 || format == 5) ? dtDate.Value.Date.AddMonths(9) : dtDate.Value.Date.AddMonths(6),

                                            
                                        });
                    }
                }
            }*/


                tmp = db.StockProduitFiniManuals.Where(p => p.DateStock == date);
                if (cbFormat.SelectedIndex > 0)
                {
                    tmp = tmp.Where(p => p.FormatID == ((Format)cbFormat.SelectedItem).ID);
                }

                if (cbGoutProduit.SelectedIndex > 0)
                {
                    tmp = tmp.Where(p => p.GoutProduitID == ((GoutProduit)cbGoutProduit.SelectedItem).ID);
                }
                this.stockdb = tmp.ToList();


                //btValid.Enabled = stockdb.Select(p => p.isValid).Contains(false);
                //button1.Enabled = !btValid.Enabled;
                RefreshGrid();
           
        }

            internal static void autochargestock()
            {
                using (var db = new ModelEntities())
            {
                var date = Tools.CurrentDateTime.Value.Date;//dtDate.Value.Date;
            var dateHier = date.Date.AddDays(-1);//date.Value.Date.AddDays(-1);
            
                var tmp = db.StockProduitFiniManuals.Where(p => p.DateStock == date);
                var stockHier = db.StockProduitFiniManuals.Where(p => p.DateStock == dateHier).ToList();
                var stockdb = tmp.ToList();
                
                if (stockdb.Count() == 0)
                {
                    while (stockHier.Count() == 0)
                    {
                        dateHier = dateHier.AddDays(-1);
                        stockHier = db.StockProduitFiniManuals.Where(p => p.DateStock == dateHier).ToList();
                    }


                    foreach (var stock in stockHier)
                    {
                        db.AddToStockProduitFiniManuals(new StockProduitFiniManual

                            {
                                DateStock = Tools.CurrentDateTime.Value.Date,
                                FormatID = stock.FormatID,
                                GoutProduitID = stock.GoutProduitID,
                                isValid = (stock.TotalBouteilles > 0) ? true : false,
                                NombreFardeaux = stock.NombreFardeaux,
                                NombrePalettes = stock.NombrePalettes,
                                TotalBouteilles = stock.TotalBouteilles,
                                take = stock.take, 
                                DateTimeValid = stock.DateTimeValid,


                            });
                    }

                    db.SaveChanges();
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.StockProduitFiniManuals);
                }
            }
            }

        private void RefreshGrid()
        {
            if (ViewerMode)
            {
                var stockValid = stockdb.Where(p => p.isValid == true).ToList();
                var stockAttrib = db.BonAttributions.Where(p => p.Date == dtDate.Value && p.Etat == (int)EnEtatBonAttribution.EnInstance).ToList();
                var stockSortie = db.BonAttributions.Where(p => p.Date == dtDate.Value && (p.Etat == (int)EnEtatBonAttribution.Chargé || p.Etat == (int)EnEtatBonAttribution.Livré)).ToList();
                var ventesAttrib = new List<Vente>();
                var ventesSortie = new List<Vente>();

                foreach (var item in stockAttrib)
                {
                    ventesAttrib.AddRange(item.Ventes);
                }
                foreach (var item in stockSortie)
                {
                    ventesSortie.AddRange(item.Ventes);
                }
                // var stockValid = stockdb.Where(p => p.isValid == true).ToList();
                dgv.DataSource = stockValid.OrderBy(p=>p.GoutProduit.Nom).Select(p => new
                {
                    ID = p.ID,

                    Produit = p.GoutProduit.Nom,
                    Format = p.Format.Volume,
                    Initial = p.NombrePalettes.ToString() + " P  " + p.NombreFardeaux.ToString() + " F",
                   
                    Sortie = ventesSortie.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombrePalettes).ToString() + " P " +
                              ventesSortie.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombreFardeaux).ToString() + " F ",
                   
                              Attribue = ventesAttrib.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombrePalettes).ToString() + " P " +
                              ventesAttrib.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombreFardeaux).ToString() + " F ",
                    Reste = (p.NombrePalettes - ventesSortie.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombrePalettes) -
                   
                    ventesAttrib.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombrePalettes)).ToString() + " P" +
                     (p.NombreFardeaux - ventesSortie.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombreFardeaux) -
                     ventesAttrib.Where(m => m.FormatID == p.FormatID && m.GoutProduitID == p.GoutProduitID).Sum(m => m.NombreFardeaux)).ToString() + " F",
                    Activer = (p.take == true) ? "Oui" : "Non",
                    DatePeremption = p.DateTimeValid,
                }).ToList();
            }
            else
            {
                var stockdbToAffiche = stockdb;
               /* if (Tools.CurrentUser.GroupeID != 1)
                {
                    stockdbToAffiche = stockdbToAffiche.Where(p => p.isValid == true).ToList();
                }*/
                dgv.DataSource = stockdbToAffiche.OrderBy(p=>p.GoutProduit.Nom).Select(p => new
                {
                    ID = p.ID,
                    Produit = p.GoutProduit.Nom,
                    Format = p.Format.Volume,
                    Palettes = (p.NombrePalettes>0)?p.NombrePalettes:0,
                    Fardeaux = (p.NombreFardeaux>0)?p.NombreFardeaux:0,
                    TotalBouteilles = (p.TotalBouteilles>0)?p.TotalBouteilles:0,
                    Activer = (p.take == true) ? "Oui" : "Non",
                    EnProduction = (p.isValid == true)? "Oui" : "Non",
                    DatePeremption = p.DateTimeValid,

                }).ToList();
                dgv.HideIDColumn();


            }


            //btValid.Enabled = stockdb.Select(p => p.isValid).Contains(false) && Tools.CurrentUser.GroupeID == 1;
        }

        private void FrStockProduitFiniManual_FormClosed(object sender, FormClosedEventArgs e)
        {
            db.Dispose();
        }

        public bool ViewerMode { get; set; }

        public List<StockProduitFiniManual> stockdb { get; set; }

        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            InputStock();
        }

        private void InputStock()
        {
            var id = dgv.GetSelectedID(null, false);
            if (id != null)
            {
                SelectedStock = db.StockProduitFiniManuals.Single(p => p.ID == id);
                txtFormat.Text = SelectedStock.Format.Volume;
                txtProduit.Text = SelectedStock.GoutProduit.Nom;
                txtNombreFardeaux.Text = SelectedStock.NombreFardeaux.ToString();
                txtNombrePalette.Text = SelectedStock.NombrePalettes.ToString();
                dtperemt.Value = (SelectedStock.DateTimeValid != null)? SelectedStock.DateTimeValid.Value : dtDate.Value  ;


                //  gpSelect.Enabled = SelectedStock.isValid != true;

            }
        }

        private void txtNombrePalette_Leave(object sender, EventArgs e)
        {
            UpdateStock();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //  UpdateStock();
        }

        private void UpdateStock()
        {
            SelectedStock.NombrePalettes = txtNombrePalette.Text.ParseToInt();
            SelectedStock.NombreFardeaux = txtNombreFardeaux.Text.ParseToInt();
            SelectedStock.DateTimeValid = dtperemt.Value;
            if (SelectedStock.NombreFardeaux == null)
            {
                SelectedStock.NombreFardeaux = 0;
            }

            if (SelectedStock.NombrePalettes == null)
            {
                SelectedStock.NombrePalettes = 0;
            }

            SelectedStock.TotalBouteilles = SelectedStock.NombrePalettes * SelectedStock.Format.BouteillesParFardeau * SelectedStock.Format.FardeauParPalette + SelectedStock.NombreFardeaux * SelectedStock.Format.BouteillesParFardeau;

            db.SaveChanges();
            int index = dgv.GetSelectedIndex().Value;

            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, SelectedStock);
            LoadStock();
            dgv.Rows[index].Selected = true;
        }

        private void txtNombrePalette_TextChanged(object sender, EventArgs e)
        {
            UpdateStock();
        }

        private void txtNombreFardeaux_TextChanged(object sender, EventArgs e)
        {
            UpdateStock();
        }

        private void txtNombreFardeaux_Leave(object sender, EventArgs e)
        {
            UpdateStock();
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btValid_Click(object sender, EventArgs e)
        {
            var ros = dgv.SelectedRows;
            
                var id = ros[0].Cells["ID"].Value.ToString().ParseToInt();
                var sel = db.StockProduitFiniManuals.Single(p => p.ID == id);
                sel.isValid = !sel.isValid;
                var index = dgv.CurrentRow.Index;
                //seIndex.Add(dgv.Rows.IndexOf(ros[i]));
            


            var scrol = dgv.FirstDisplayedScrollingRowIndex;

            db.SaveChanges();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.StockProduitFiniManuals);
            LoadStock();



            dgv.ClearSelection();
            dgv.Rows[index].Selected = true;
            //dgv.Rows.IndexOf[ros].Selected = true;
            //dgv.FirstDisplayedScrollingRowIndex = scrol;
            
            
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LoadStock();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            FZBGsys.NsReports.Stock.StockReportController.PrintStockProduitFiniManual(dtDate.Value.Date);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var ros = dgv.SelectedRows;
            List<int> seIndex = new List<int>();
            for (int i = 0; i < ros.Count; i++)
            {
                var id = ros[i].Cells["ID"].Value.ToString().ParseToInt();
                var sel = db.StockProduitFiniManuals.Single(p => p.ID == id);
                sel.take = !sel.take;
                seIndex.Add(dgv.Rows.IndexOf(ros[i]));
            }


              var scrol = dgv.FirstDisplayedScrollingRowIndex;

            db.SaveChanges();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.StockProduitFiniManuals);
            LoadStock();

            

            dgv.ClearSelection(); 
            foreach (var item in seIndex)

            {
                dgv.Rows[item].Selected = true;
            }

            dgv.FirstDisplayedScrollingRowIndex = scrol;
        }

        private void cbGoutProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadStock();
        }

        private void cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadStock();
        }

        private void dtperemt_Leave(object sender, EventArgs e)
        {
            UpdateStock();
        }

        internal static void stockauto(DateTime Date, ModelEntities db, List<SessionProduction> SessionProduction, bool ModifierSession, bool AddProduction)// stocké par la production
        {
            SessionProduction Session;
            autochargestock();
            Date = Tools.CurrentDateTime.Value.Date;
            if (!AddProduction && !ModifierSession) // ajouté une session dans une ancienne production
            {
                Session = SessionProduction.Last();
                var stockman = db.StockProduitFiniManuals.SingleOrDefault(p => p.FormatID == Session.Production.BouteileFormatID && p.GoutProduitID == Session.GoutProduitID && p.DateStock == Date);
                if (stockman != null)
                {
                    if (Session.NombrePalettesTotal != null) stockman.NombrePalettes += Session.NombrePalettesTotal;
                    if (Session.NombreFardeauxTotal != null) stockman.NombreFardeaux += Session.NombreFardeauxTotal;
                    stockman.TotalBouteilles += Session.NombreBouteillesTotal;
                    if (stockman.isValid == false) stockman.isValid = true;
                }
                else // si le produit n existe pas dans stock
                {
                    
                    db.AddToStockProduitFiniManuals(new StockProduitFiniManual

                    {
                        DateStock = Tools.CurrentDateTime.Value.Date,
                        FormatID = Session.Production.Format.ID,
                        GoutProduitID = Session.GoutProduit.ID,
                        isValid = true,
                        NombreFardeaux = Session.NombreFardeauxTotal,
                        NombrePalettes = Session.NombrePalettesTotal,
                        TotalBouteilles = Session.NombreBouteillesTotal,
                        take = true,
                        DateTimeValid = Date.AddYears(1),


                    });
                
                }
            }
                       
            else if (!ModifierSession) // stocker pour une nouvelle production 
            {
               
                foreach (var itm in SessionProduction)
                {                   
                    
                    var stockman = db.StockProduitFiniManuals.SingleOrDefault(p => p.FormatID == itm.Production.BouteileFormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == Date);
                    if (stockman != null)
                    {
                        if (itm.NombrePalettesTotal != null) stockman.NombrePalettes += itm.NombrePalettesTotal;
                        if (itm.NombreFardeauxTotal != null) stockman.NombreFardeaux += itm.NombreFardeauxTotal;
                        stockman.TotalBouteilles += itm.NombreBouteillesTotal;
                        if (stockman.isValid == false) stockman.isValid = true;
                    }
                    else
                    {                       
                        db.AddToStockProduitFiniManuals(new StockProduitFiniManual

                        {
                            DateStock = Tools.CurrentDateTime.Value.Date,
                            FormatID = itm.Production.Format.ID,
                            GoutProduitID = itm.GoutProduit.ID,
                            isValid = true,
                            NombreFardeaux = itm.NombreFardeauxTotal,
                            NombrePalettes = itm.NombrePalettesTotal,
                            TotalBouteilles = itm.NombreBouteillesTotal,
                            take = true,
                            DateTimeValid = (itm.Production.Format.ID == 6 || itm.Production.Format.ID == 8 || itm.Production.Format.ID == 5) ? Date.AddMonths(9) : Date.AddMonths(6),


                        });                   
                                            }
                }
            }
            else
            {
                foreach (var itm in SessionProduction) //Modifier une session
                {
                    var id = itm.ID;
                    using (var Md = new ModelEntities())
                    {
                        var totalpalette = Md.SessionProductions.Single(p => p.ID == id);
                        var date = Tools.CurrentDateTime.Value.Date;

                        var Remplire = db.StockProduitFiniManuals.Single(p => p.FormatID == itm.Production.BouteileFormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == date);

                        Remplire.NombrePalettes += ((itm.NombrePalettesTotal == null) ? 0 : itm.NombrePalettesTotal) - ((totalpalette.NombrePalettesTotal == null) ? 0 : totalpalette.NombrePalettesTotal);
                        Remplire.NombreFardeaux += ((itm.NombreFardeauxTotal == null) ? 0 : itm.NombreFardeauxTotal) - ((totalpalette.NombreFardeauxTotal == null) ? 0 : totalpalette.NombreFardeauxTotal);
                        Remplire.TotalBouteilles += itm.NombreBouteillesTotal - totalpalette.NombreBouteillesTotal;

                        if (Remplire.isValid == false) Remplire.isValid = true; 
                    }

                }

            }
            
        }



        internal static void stockautoajoute(DateTime currentTime, ModelEntities db, List<Vente> listevente) // destocker pour la vente
        {
            autochargestock();

            
            foreach (var itm in listevente)
            {
                var destocker = db.StockProduitFiniManuals.Single(p => p.FormatID == itm.FormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == currentTime.Date);
                if(itm.NombrePalettes != null) destocker.NombrePalettes -= itm.NombrePalettes;
                if (itm.NombreFardeaux != null)
                {
                    var test = destocker.NombreFardeaux - itm.NombreFardeaux;
                    if (test < 0)
                    {
                        destocker.NombrePalettes--;
                        destocker.NombreFardeaux = (destocker.Format.FardeauParPalette - itm.NombreFardeaux) + destocker.NombreFardeaux;
                    }
                    else destocker.NombreFardeaux -= itm.NombreFardeaux;
                }

                destocker.TotalBouteilles -= itm.TotalBouteilles;

                if (destocker.TotalBouteilles < 1 && destocker.NombreFardeaux < 1)
                    destocker.isValid = false;

            }
            
            
        }

        internal static void stockautoajoute(DateTime currentTime, ModelEntities db, List<StockDistribution> listdist, bool ModifierGratuit) // destocker pour gratuit
        {
            autochargestock();

            if (!ModifierGratuit)
            {

                foreach (var itm in listdist)
                {
                    var decharge = db.StockProduitFiniManuals.Single(p => p.FormatID == itm.FormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == currentTime.Date);
                    if (itm.NombrePalettes != null) decharge.NombrePalettes -= itm.NombrePalettes;
                    if (itm.NombreFardeaux != null)
                    {
                        var test = decharge.NombreFardeaux - itm.NombreFardeaux;
                        if (test < 0)
                        {
                            decharge.NombrePalettes--;
                            decharge.NombreFardeaux = (decharge.Format.FardeauParPalette - itm.NombreFardeaux) + decharge.NombreFardeaux;
                        }
                        else decharge.NombreFardeaux -= itm.NombreFardeaux;
                    }
                    decharge.TotalBouteilles -= itm.TotalBouteilles;

                    if (decharge.TotalBouteilles < 1 && decharge.NombreFardeaux < 1)
                        decharge.isValid = false;


                }
            }
            else
            {
                foreach (var itm in listdist) //Modifier une session
                {
                    var id = itm.ID;
                    if (id != 0)
                    {
                        using (var Md = new ModelEntities())
                        {
                            var totalpalette = Md.StockDistributions.Single(p => p.ID == id);
                            var date = Tools.CurrentDateTime.Value.Date;

                            var Remplire = db.StockProduitFiniManuals.Single(p => p.FormatID == itm.FormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == date);

                            Remplire.NombrePalettes += ((itm.NombrePalettes == null) ? 0 : itm.NombrePalettes) - ((totalpalette.NombrePalettes == null) ? 0 : totalpalette.NombrePalettes);
                            Remplire.NombreFardeaux += ((itm.TotalFardeaux == null) ? 0 : itm.TotalFardeaux) - ((totalpalette.TotalFardeaux == null) ? 0 : totalpalette.TotalFardeaux);
                            Remplire.TotalBouteilles += itm.TotalBouteilles - totalpalette.TotalBouteilles;

                            if (Remplire.isValid == false) Remplire.isValid = true;
                        }
                    }
                    else
                    {
                        var decharge = db.StockProduitFiniManuals.Single(p => p.FormatID == itm.FormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == currentTime.Date);
                        if (itm.NombrePalettes != null) decharge.NombrePalettes -= itm.NombrePalettes;
                        if (itm.NombreFardeaux != null)
                        {
                            var test = decharge.NombreFardeaux - itm.NombreFardeaux;
                            if (test < 0)
                            {
                                decharge.NombrePalettes--;
                                decharge.NombreFardeaux = (decharge.Format.FardeauParPalette - itm.NombreFardeaux) + decharge.NombreFardeaux;
                            }
                            else decharge.NombreFardeaux -= itm.NombreFardeaux;
                        }
                        decharge.TotalBouteilles -= itm.TotalBouteilles;

                        if (decharge.TotalBouteilles < 1 && decharge.NombreFardeaux < 1)
                            decharge.isValid = false;


                    }

                }



            }
        }


        internal static void stockautoajoute(ModelEntities db, List<Vente> listesortie) // supprimer bon sortie
        {
            autochargestock();
            var Date = Tools.CurrentDateTime.Value.Date;

            foreach (var itm in listesortie)
            {
                var stockman = db.StockProduitFiniManuals.Single(p => p.FormatID == itm.FormatID && p.GoutProduitID == itm.GoutProduitID && p.DateStock == Date);
                if (itm.NombrePalettes != null) stockman.NombrePalettes += itm.NombrePalettes;
                if (itm.NombreFardeaux != null) stockman.NombreFardeaux += itm.NombreFardeaux;
                stockman.TotalBouteilles += itm.TotalBouteilles;
                if (stockman.isValid == false) stockman.isValid = true;
            }
        }

        internal static void stockautoajoute(ModelEntities db, StockDistribution At)
        {
            autochargestock();
            var Date = Tools.CurrentDateTime.Value.Date;

                var stockman = db.StockProduitFiniManuals.Single(p => p.FormatID == At.FormatID && p.GoutProduitID == At.GoutProduitID && p.DateStock == Date);
                if (At.NombrePalettes != null) stockman.NombrePalettes += At.NombrePalettes;
                if (At.NombreFardeaux != null) stockman.NombreFardeaux += At.NombreFardeaux;
                stockman.TotalBouteilles += At.TotalBouteilles;
                if (stockman.isValid == false) stockman.isValid = true;
            
        }
    }
}
