﻿namespace FZBGsys.NsStock.StockProduitFini
{
    partial class FrNEProduit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chEtiquette = new System.Windows.Forms.CheckBox();
            this.chvente = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.txtConge = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpInfos = new System.Windows.Forms.TabPage();
            this.tpArome = new System.Windows.Forms.TabPage();
            this.chl = new System.Windows.Forms.CheckedListBox();
            this.tabControl1.SuspendLayout();
            this.tpInfos.SuspendLayout();
            this.tpArome.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(77, 64);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(264, 22);
            this.txtNom.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom:";
            // 
            // cbType
            // 
            this.cbType.FormattingEnabled = true;
            this.cbType.Location = new System.Drawing.Point(77, 21);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(143, 24);
            this.cbType.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 14;
            this.label2.Text = "Type:";
            // 
            // chEtiquette
            // 
            this.chEtiquette.AutoSize = true;
            this.chEtiquette.Location = new System.Drawing.Point(77, 193);
            this.chEtiquette.Name = "chEtiquette";
            this.chEtiquette.Size = new System.Drawing.Size(169, 21);
            this.chEtiquette.TabIndex = 16;
            this.chEtiquette.Text = "Utilise Etiquette (PET)";
            this.chEtiquette.UseVisualStyleBackColor = true;
            // 
            // chvente
            // 
            this.chvente.AutoSize = true;
            this.chvente.Location = new System.Drawing.Point(77, 151);
            this.chvente.Name = "chvente";
            this.chvente.Size = new System.Drawing.Size(165, 21);
            this.chvente.TabIndex = 16;
            this.chvente.Text = "Disponible en Vente?";
            this.chvente.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(179, 358);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 30);
            this.button2.TabIndex = 17;
            this.button2.Text = "Annuler";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(289, 358);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 30);
            this.button1.TabIndex = 18;
            this.button1.Text = "Enregistrer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtConge
            // 
            this.txtConge.Location = new System.Drawing.Point(77, 107);
            this.txtConge.Name = "txtConge";
            this.txtConge.Size = new System.Drawing.Size(93, 22);
            this.txtConge.TabIndex = 0;
            this.txtConge.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtConge_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Prod:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(184, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "littres par conge";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpInfos);
            this.tabControl1.Controls.Add(this.tpArome);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(381, 330);
            this.tabControl1.TabIndex = 19;
            // 
            // tpInfos
            // 
            this.tpInfos.BackColor = System.Drawing.SystemColors.Control;
            this.tpInfos.Controls.Add(this.chEtiquette);
            this.tpInfos.Controls.Add(this.cbType);
            this.tpInfos.Controls.Add(this.txtNom);
            this.tpInfos.Controls.Add(this.txtConge);
            this.tpInfos.Controls.Add(this.chvente);
            this.tpInfos.Controls.Add(this.label1);
            this.tpInfos.Controls.Add(this.label3);
            this.tpInfos.Controls.Add(this.label4);
            this.tpInfos.Controls.Add(this.label2);
            this.tpInfos.Location = new System.Drawing.Point(4, 25);
            this.tpInfos.Name = "tpInfos";
            this.tpInfos.Padding = new System.Windows.Forms.Padding(3);
            this.tpInfos.Size = new System.Drawing.Size(373, 301);
            this.tpInfos.TabIndex = 0;
            this.tpInfos.Text = "Informations";
            // 
            // tpArome
            // 
            this.tpArome.Controls.Add(this.chl);
            this.tpArome.Location = new System.Drawing.Point(4, 25);
            this.tpArome.Name = "tpArome";
            this.tpArome.Padding = new System.Windows.Forms.Padding(3);
            this.tpArome.Size = new System.Drawing.Size(373, 301);
            this.tpArome.TabIndex = 1;
            this.tpArome.Text = "Arromes Composants";
            this.tpArome.UseVisualStyleBackColor = true;
            // 
            // chl
            // 
            this.chl.CheckOnClick = true;
            this.chl.FormattingEnabled = true;
            this.chl.Items.AddRange(new object[] {
            "one ",
            "two"});
            this.chl.Location = new System.Drawing.Point(6, 6);
            this.chl.MultiColumn = true;
            this.chl.Name = "chl";
            this.chl.Size = new System.Drawing.Size(399, 293);
            this.chl.Sorted = true;
            this.chl.TabIndex = 4;
            // 
            // FrNEProduit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 414);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "FrNEProduit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Inscription Produit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrNEProduit_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tpInfos.ResumeLayout(false);
            this.tpInfos.PerformLayout();
            this.tpArome.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chEtiquette;
        private System.Windows.Forms.CheckBox chvente;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtConge;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpInfos;
        private System.Windows.Forms.TabPage tpArome;
        private System.Windows.Forms.CheckedListBox chl;
    }
}