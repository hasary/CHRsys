﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockProduitFini
{
    public partial class FrListProduit : Form
    {
        ModelEntities db = new ModelEntities();
        public FrListProduit()
        {
            InitializeComponent();
            InitialiseControls();
           // RefreshGrid();
        }

        private void RefreshGrid()
        {
            IQueryable<GoutProduit> data = db.GoutProduits;
            if (cbType.SelectedIndex > 0)
            {
                var t = ((TypeProduit)cbType.SelectedItem).ID;

                data = data.Where(p => p.TypeProduitID == t );

            }

            dgv.DataSource = data.OrderBy(p=>p.Nom).Select(p => new
            {
                ID = p.ID,
                Type = p.TypeProduit.Nom,
                Nom = p.Nom,
                Etiquette = (p.UseEtiquette == true) ? "Oui":"",
                EnVente = (p.UseVente == true) ? "Oui" : "",

            }).ToList();


            dgv.HideIDColumn();
        }

        private void InitialiseControls()
        {
            cbType.Items.Add(new TypeProduit { ID = 0, Nom = "[Tout]" });
            cbType.Items.AddRange(db.TypeProduits.Where(p => p.ID > 0).ToArray());
            cbType.DisplayMember = "Nom";
            cbType.ValueMember = "ID";
            cbType.DropDownStyle = ComboBoxStyle.DropDownList;
            cbType.SelectedIndex = 0;
        }

        private void FrListProduit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            new FrNEProduit().ShowDialog();
            db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.GoutProduits);
            RefreshGrid();
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                new FrNEProduit(id).ShowDialog();
                db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.GoutProduits);
                RefreshGrid();
            }
        }

        private void btDel_Click(object sender, EventArgs e)
        {
            var id = dgv.GetSelectedID();
            if (id != null)
            {
                var toDel = db.GoutProduits.Single(p => p.ID == id);

                try
                {
                    db.DeleteObject(toDel);
                    db.SaveChanges();
                    db.Refresh(System.Data.Objects.RefreshMode.StoreWins, db.GoutProduits);
                    RefreshGrid();
                }
                catch (Exception ll)
                {

                    Tools.ShowError("ne peut pas suprimer un element utilisé");
                }



            }
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshGrid();
        }
    }
}
