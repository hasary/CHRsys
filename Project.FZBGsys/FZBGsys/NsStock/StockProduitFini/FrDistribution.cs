﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockProduitFini
{
    public partial class FrDistribution : Form
    {
        ModelEntities db = new ModelEntities();
        bool modifier = false;
        int? id; 
        
        public FrDistribution(int? ID = null)
        {
            //stockDistribution = new StockDistribution();
            Listdistribution= new List<StockDistribution>();
            InitializeComponent();
            InitialiseControls();
            if (ID != null)
            {
                this.id = ID;
                StockDistribution = db.StockDistributions.Single(p => p.ID == ID);
                
                //var Dist = StockDistribution.ToList();
                
                Modifier(ID);
            }
        }

        private void Modifier(int? ID)
        {

            Listdistribution.Add(StockDistribution);
            textClient.Text = StockDistribution.Client;
            textClient.Enabled = false;
            modifier = true;

            RefreshGrid();
        }

       
        private void InitialiseControls()
        {
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID >0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.SelectedIndex = 0;


            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.SelectedIndex = 0;
        }

              
        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (Listdistribution.Count == 0 || Listdistribution == null) return;

            var temp = Tools.CurrentDateTime.Value.Date;

            //var listsession = .SessionProductions.ToList();
            
            //var S = StockDistribution.EntityState;
           // if (StockDistribution.EntityState == EntityState.Modified || StockDistribution.EntityState == EntityState.Deleted) modifier = true;
            var currentTime = dtDate.Value;
            FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockautoajoute(temp, db, Listdistribution, modifier);
            try
            {
                db.SaveChanges();
                Dispose();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.AllMessages("erreur"));
            }

        }

        public List<StockDistribution> Listdistribution { get; set; }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }
            var dist = new StockDistribution();
            dist.Client = textClient.Text;
            dist.Format = (Format)cbFormat.SelectedItem;
            dist.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;
            dist.Date = dtDate.Value;
            if (radPalettes.Checked)
            {
                dist.NombrePalettes = txtNombre.Text.ParseToInt();
                dist.TotalFardeaux = dist.Format.FardeauParPalette * dist.NombrePalettes;
                dist.TotalBouteilles = dist.Format.BouteillesParFardeau * dist.TotalFardeaux;
            }
            else
            {
                dist.NombreFardeaux = txtNombre.Text.ParseToInt();
                dist.TotalFardeaux = dist.NombreFardeaux;
                dist.TotalBouteilles = dist.Format.BouteillesParFardeau * dist.NombreFardeaux;
            }
            dist.Observation = txtObservation.Text;
            dist.InscriptionUID = Tools.CurrentUserID;
            dist.InscriptionTime = Tools.CurrentDateTime;

            Listdistribution.Add(dist);
            db.AddToStockDistributions(dist);
            RefreshGrid();


        }

        private void RefreshGrid()
        {
            dgv.DataSource = Listdistribution.Select(p => new
            {
                ID = p.ID,
                //Référence = p.NumeroBon,
                Bénéficiaire = p.Client,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                Quantite = (p.NombrePalettes == null) ? p.NombreFardeaux : p.NombrePalettes,
                Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteilles + " Bouteilles",
            }).ToList();
            dgv.HideIDColumn();
               

        }

        private bool isValidAll()
        {

            if (cbFormat.SelectedIndex < 1)
            { this.ShowWarning("\nSelectionner un Format!"); return false; }
            if (cbGoutProduit.SelectedIndex < 1)
            { this.ShowWarning("\nSelectionner un gout du produit!"); return false; }
            if (txtNombre.Text == "")
            { this.ShowWarning("\nVeulliez entrer nombre de palettes ou fradeaux"); return false; }

            return true;
        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteilles();
        }

        private void UpdateTotalBouteilles()
        {
            var Nombre = txtNombre.Text.ParseToInt();
            var Format = (Format)cbFormat.SelectedItem;
            if (Nombre != null && cbFormat.SelectedIndex != 0)
            {
                if (radPalettes.Checked)
                {
                    txtTotalBouteilles.Text = (Nombre * Format.FardeauParPalette * Format.BouteillesParFardeau).ToString();
                }
                else
                {
                    txtTotalBouteilles.Text = (Nombre * Format.BouteillesParFardeau).ToString();
                }
            }
            else
            {
                txtTotalBouteilles.Text = "";
            }
        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var index = dgv.GetSelectedIndex();
            if (index == null) return;
            
            var At = Listdistribution.ElementAt(index.Value);
            Listdistribution.RemoveAt(index.Value);
            FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockautoajoute(db, At);
            db.DeleteObject(At);
            
            RefreshGrid();

        }



        public StockDistribution StockDistribution { get; set; }
    }
}
