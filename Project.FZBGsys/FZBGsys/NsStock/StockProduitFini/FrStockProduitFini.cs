﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock
{
    public partial class FrStockProduitFini : Form
    {
        ModelEntities db = new ModelEntities();
        public FrStockProduitFini()
        {
            InitializeComponent();
            InitialiseControls();
        }

        private void InitialiseControls()
        {
            //--------------------------- type produit
            cbTypeProduit.Items.Add(new GoutProduit { ID = 0, Nom = "[Tout]" });
            cbTypeProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbTypeProduit.SelectedIndex = 0;
            cbTypeProduit.ValueMember = "ID";
            cbTypeProduit.DisplayMember = "Nom";
            cbTypeProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "[Tout]" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;
            cbFormat.SelectedIndex = 0;
            //---------------------- Format (Bouteilles)
            cbLot.Items.Add("[Tout]");
            cbLot.Items.Add("A");
            cbLot.Items.Add("B");
            cbLot.Items.Add("C");
            cbLot.Items.Add("D");
            cbLot.Items.Add("E");
            cbLot.SelectedIndex = 0;
            cbLot.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void chDateProduction_CheckedChanged(object sender, EventArgs e)
        {
            panDateProduction.Visible = chDateEntree.Checked;
        }

        private void chProduitConsome_CheckedChanged(object sender, EventArgs e)
        {
            panConsome.Visible = chProduitConsome.Checked;
        }

        private void FrStockPETProduitFini_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.IsSelectDialog)
            {
                db.Dispose();
            }
        }
        //RechercheProduction
        private void RechercheMagazin()
        {
            IQueryable<StockPalette> resultPalette = db.StockPalettes;
            IQueryable<StockFardeau> resultFardeau = db.StockFardeaux;

            if (chDateEntree.Checked)
            {
                var dateEntDu = dtDateEntreeDu.Value.Date;
                var dateEntAu = dtDateEntreeAu.Value.Date;

                resultPalette = resultPalette.Where(p =>
                    p.DateEntree >= dateEntDu &&
                    p.DateEntree <= dateEntAu
                    );

                resultFardeau = resultFardeau.Where(p =>
                   p.DateEntree >= dateEntDu &&
                    p.DateEntree <= dateEntAu);

            }

            if (!chProduitEnStock.Checked)
            {
                resultPalette = resultPalette.Where(p => p.EnStock == false);
                resultFardeau = resultFardeau.Where(p => p.EnStock == false);

                if (chProduitConsome.Checked)
                {
                    var dateSorDu = dtDateEntreeDu.Value.Date;
                    var dateSorAu = dtDateEntreeAu.Value.Date;

                    resultPalette = resultPalette.Where(p =>
                        p.DateSortie >= dateSorDu &&
                        p.DateSortie <= dateSorAu
                        );

                    resultFardeau = resultFardeau.Where(p =>
                       p.DateSortie >= dateSorDu &&
                        p.DateSortie <= dateSorAu);
                }
                else
                {
                    this.ShowWarning("Selectionner Etat de Stock!");
                    return;
                }
            }
            else
            {
                if (chProduitConsome.Checked)
                {
                    var dateSorDu = dtDateEntreeDu.Value.Date;
                    var dateSorAu = dtDateEntreeAu.Value.Date;

                    resultPalette = resultPalette.Where(p => p.EnStock == true || (
                        p.DateSortie >= dateSorDu &&
                        p.DateSortie <= dateSorAu)
                        );

                    resultFardeau = resultFardeau.Where(p => p.EnStock == true || (
                        p.DateSortie >= dateSorDu &&
                        p.DateSortie <= dateSorAu)
                        );
                }
                else
                {
                    resultPalette = resultPalette.Where(p => p.EnStock == true);
                    resultFardeau = resultFardeau.Where(p => p.EnStock == true);
                }



            }



            if (cbTypeProduit.SelectedIndex > 0)
            {
                var typeProduitID = ((GoutProduit)cbTypeProduit.SelectedItem).ID;
                resultPalette = resultPalette.Where(p => p.GoutProduitID == typeProduitID);
                resultFardeau = resultFardeau.Where(p => p.GoutProduitID == typeProduitID);

            }

            if (cbFormat.SelectedIndex > 0)
            {
                var formatID = ((Format)cbFormat.SelectedItem).ID;
                resultPalette = resultPalette.Where(p => p.FormatID == formatID);
                resultFardeau = resultFardeau.Where(p => p.FormatID == formatID);
            }

            if (cbLot.SelectedIndex > 0)
            {
                var lot = cbLot.SelectedItem.ToString();
                resultPalette = resultPalette.Where(p => p.Lot == lot);
                resultFardeau = resultFardeau.Where(p => p.Lot == lot);
            }

            this.ListPalettesFoundMagazin = resultPalette.ToList();
            this.ListFardeauxFoundMagazin = resultFardeau.ToList();

        }

        private void RechercheProduction()
        {
            IQueryable<StockPETPalette> resultPalette = db.StockPETPalettes;
            IQueryable<StockPETFardeau> resultFardeau = db.StockPETFardeaux;


            if (chDateEntree.Checked)
            {
                var dateEntDu = dtDateEntreeDu.Value.Date;
                var dateEntAu = dtDateEntreeAu.Value.Date;

                resultPalette = resultPalette.Where(p =>
                    p.DateProduction >= dateEntDu &&
                    p.DateProduction <= dateEntAu
                    );

                resultFardeau = resultFardeau.Where(p =>
                   p.DateProduction >= dateEntDu &&
                   p.DateProduction <= dateEntAu);

            }


            if (!chProduitEnStock.Checked)
            {
                resultPalette = resultPalette.Where(p => p.EnStock == false);
                resultFardeau = resultFardeau.Where(p => p.EnStock == false);

                if (chProduitConsome.Checked)
                {
                    var dateSorDu = dtDateEntreeDu.Value.Date;
                    var dateSorAu = dtDateEntreeAu.Value.Date;

                    resultPalette = resultPalette.Where(p =>
                        p.DateSortie >= dateSorDu &&
                        p.DateSortie <= dateSorAu
                        );

                    resultFardeau = resultFardeau.Where(p =>
                       p.DateSotie >= dateSorDu &&
                        p.DateSotie <= dateSorAu);
                }
                else
                {
                    this.ShowWarning("Selectionner Etat de Stock!");
                    return;
                }
            }
            else
            {
                if (chProduitConsome.Checked)
                {
                    var dateSorDu = dtDateEntreeDu.Value.Date;
                    var dateSorAu = dtDateEntreeAu.Value.Date;

                    resultPalette = resultPalette.Where(p => p.EnStock == true || (
                        p.DateSortie >= dateSorDu &&
                        p.DateSortie <= dateSorAu)
                        );

                    resultFardeau = resultFardeau.Where(p => p.EnStock == true || (
                        p.DateSotie >= dateSorDu &&
                        p.DateSotie <= dateSorAu)
                        );
                }
                else
                {
                    resultPalette = resultPalette.Where(p => p.EnStock == true);
                    resultFardeau = resultFardeau.Where(p => p.EnStock == true);
                }



            }



            if (cbTypeProduit.SelectedIndex > 0)
            {
                var typeProduitID = ((GoutProduit)cbTypeProduit.SelectedItem).ID;
                resultPalette = resultPalette.Where(p => p.SessionProduction.GoutProduitID == typeProduitID);
                resultFardeau = resultFardeau.Where(p => p.SessionProduction.GoutProduitID == typeProduitID);

            }

            if (cbFormat.SelectedIndex > 0)
            {
                var formatID = ((Format)cbFormat.SelectedItem).ID;
                resultPalette = resultPalette.Where(p => p.SessionProduction.Production.BouteileFormatID == formatID);
                resultFardeau = resultFardeau.Where(p => p.SessionProduction.Production.BouteileFormatID == formatID);
            }

            if (cbLot.SelectedIndex > 0)
            {
                var lot = cbLot.SelectedItem.ToString();
                resultPalette = resultPalette.Where(p => p.Lot == lot);
                resultFardeau = resultFardeau.Where(p => p.Lot == lot);
            }

            this.ListPalettesFoundProduction = resultPalette.ToList();
            this.ListFardeauxFoundProduction = resultFardeau.ToList();
        }





        private void btRecherche_Click(object sender, EventArgs e)
        {
            if (chMagazin.Checked)
            {
                RechercheMagazin();
            }

            if (chMagazin.Checked)
            {
                RechercheProduction();
            }

            RefreshGrid();
        }

        private void RefreshGrid()
        {
            var dsMagazin = ListPalettesFoundMagazin.Select(p => new
              {
                  Produit = p.GoutProduit.Nom,
                  F = p.Format.Volume,
                  Unite = p.Nombre + " Palettes",
                  Numeros = p.Numeros,
                  Lot = p.Lot,
                  Total = p.TotalBouteilles,
                  Entree = p.DateEntree.Value.ToShortDateString(),
                  Etat = (p.EnStock == true) ? "En Stock" : "Sortie le " + p.DateSortie.Value.ToShortDateString(),

              }).ToList();


            dsMagazin.AddRange(ListFardeauxFoundMagazin.Select(p => new
           {
               Produit = p.GoutProduit.Nom,
               F = p.Format.Volume,
               Unite = p.Nombre + " Fardeaux",
               Numeros = "/",
               Lot = p.Lot,
               Total = p.TotalBouteilles,
               Entree = p.DateEntree.Value.ToShortDateString(),
               Etat = (p.EnStock == true) ? "En Stock" : "Sortie le " + p.DateSortie.Value.ToShortDateString(),

           }).ToList());

            dgvMagazin.DataSource = dsMagazin;

            //----------------------------------------------------------

            var dsProduction = ListPalettesFoundProduction.Select(p => new
            {
                Produit = p.SessionProduction.GoutProduit.Nom,
                F = p.SessionProduction.Production.Format.Volume,
                Unite = p.Nombre + " Palettes",
                Numeros = p.Numeros,
                Lot = p.Lot,
                Total = p.TotalBouteilles,
                Entree = p.SessionProduction.Production.HeureFinDeProd.Value.ToShortDateString(),
                Etat = (p.EnStock == true) ? "En Stock" : "Sortie le " + p.DateSortie.Value.ToShortDateString(),

            }).ToList();


            dsProduction.AddRange(ListFardeauxFoundProduction.Select(p => new
            {
                Produit = p.SessionProduction.GoutProduit.Nom,
                F = p.SessionProduction.Production.Format.Volume,
                Unite = p.Nombre + " Fardeaux",
                Numeros = "/",
                Lot = p.Lot,
                Total = p.TotalBouteilles,
                Entree = p.SessionProduction.Production.HeureFinDeProd.Value.ToShortDateString(),
                Etat = (p.EnStock == true) ? "En Stock" : "Sortie le " + p.DateSotie.Value.ToShortDateString(),

            }).ToList());



            dgvProduction.DataSource = dsProduction;



        }

        private void UpdateTotal()
        {

            if (tabControl1.SelectedTab == tpProduction)
            {
                labTotalBouteilles.Text = (ListFardeauxFoundProduction.Sum(p => p.TotalBouteilles) +
                                     ListPalettesFoundProduction.Sum(p => p.TotalBouteilles)).ToString() + " Bouteilles";

                string totalFP = "";

                if (ListPalettesFoundProduction.Count != 0)
                {
                    totalFP += ListPalettesFoundProduction.Sum(p => p.Nombre) + " P  ";
                }
                if (ListFardeauxFoundProduction.Count != 0)
                {
                    totalFP += ListFardeauxFoundProduction.Sum(p => p.Nombre) + " F  ";
                }
                labTotalPF.Text = totalFP;

            }


            if (tabControl1.SelectedTab == tpMagazin)
            {
                if (ListFardeauxFoundMagazin == null || ListFardeauxFoundProduction == null)
                {
                    return;
                }
                labTotalBouteilles.Text = (ListFardeauxFoundMagazin.Sum(p => p.TotalBouteilles) +
                                     ListPalettesFoundMagazin.Sum(p => p.TotalBouteilles)).ToString() + " Bouteilles";

                string totalFP = "";

                if (ListPalettesFoundMagazin.Count != 0)
                {
                    totalFP += ListPalettesFoundMagazin.Sum(p => p.Nombre) + " P  ";
                }
                if (ListFardeauxFoundMagazin.Count != 0)
                {
                    totalFP += ListFardeauxFoundMagazin.Sum(p => p.Nombre) + " F  ";
                }

                labTotalPF.Text = totalFP;

            }



        }

        private void UpdateControls()
        {
            tabControl1.TabPages.Clear();
            if (ListPalettesFoundMagazin.Count != 0 || ListFardeauxFoundMagazin.Count != 0)
            {
                tabControl1.TabPages.Add(tpMagazin);

            }
            if (ListPalettesFoundProduction.Count != 0 || ListFardeauxFoundProduction.Count != 0)
            {
                tabControl1.TabPages.Add(tpProduction);

            }
            tabControl1.Refresh();
            UpdateTotal();

        }


        public bool IsSelectDialog { get; set; }

        private void btRecherche_Click_1(object sender, EventArgs e)
        {
            if (chProduction.Checked)
            {
                RechercheProduction();
            }

            if (chMagazin.Checked)
            {
                RechercheMagazin();
            }

            RefreshGrid();
            UpdateControls();

        }

        public List<StockPETPalette> ListPalettesFoundProduction { get; set; }

        public List<StockPETFardeau> ListFardeauxFoundProduction { get; set; }

        public List<StockPalette> ListPalettesFoundMagazin { get; set; }

        public List<StockFardeau> ListFardeauxFoundMagazin { get; set; }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btSelectionner_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTotal();
        }

        private void btImprimer_Click(object sender, EventArgs e)
        {

        }
    }

}
