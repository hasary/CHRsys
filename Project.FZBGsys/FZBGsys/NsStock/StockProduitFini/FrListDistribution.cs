﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsStock.StockProduitFini
{
    public partial class FrListDistribution : Form
    {
        ModelEntities db = new ModelEntities();
        List<StockDistribution> ListFound;
        public FrListDistribution()
        {
            InitializeComponent();
            InitialiseControls();
        }

        private void InitialiseControls()
        {
            var gout = db.GoutProduits.ToArray();
            var format = db.Formats.ToArray();
            cbformat.Items.AddRange(format);
            cbgout.Items.AddRange(gout);
            cbformat.DisplayMember = "Volume";
            cbformat.ValueMember = "ID";
            cbgout.DisplayMember = "Nom";
            cbgout.ValueMember = "ID";
        }

        private void chRechAvancee_CheckedChanged(object sender, EventArgs e)
        {
            panRechAvance.Enabled = !panRechAvance.Enabled;
        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
        }

        private void Recherche()
        {
            DateTime Now = dtDate.Value;
            if (!chRechAvancee.Checked)
            {
                
                ListFound = db.StockDistributions.Where(p => p.Date == Now.Date).ToList();
            }
            else
            {
                IQueryable<StockDistribution> Result = db.StockDistributions;
                var Datedebut = dtdebut.Value.Date;
                var Datefin = dtfin.Value.Date;
                Result = Result.Where(p => p.Date >= Datedebut && p.Date <= Datefin);
                if(txtclient.Text !="")
                {
                    var client = txtclient.Text.Trim();
                    Result = Result.Where(p => p.Client.Contains(client));
                }

                if(cbgout.SelectedIndex >= 0)
                {
                    var Gout = ((GoutProduit) cbgout.SelectedItem).Nom;
                    Result = Result.Where(p => p.GoutProduit.Nom == Gout);
                }

                if (cbformat.SelectedIndex >= 0)
                {
                    var Format = ((Format)cbformat.SelectedItem).Volume;
                    Result = Result.Where(p => p.Format.Volume == Format);
                }

                ListFound = Result.ToList();

            }
            RefreshGrid();
        }

        private void RefreshGrid()
        {
            if (ListFound == null) return;
            dgvVente.DataSource = ListFound.Select(p => new
            {
                ID = p.ID,
                Client = p.Client,
                Produit = p.GoutProduit.Nom,
                Format = p.Format.Volume,
                Quantité = (p.NombrePalettes != null && p.NombreFardeaux != null) ? p.NombrePalettes + "P ET " + p.NombreFardeaux + "F" :
                (p.NombrePalettes != null) ? p.NombrePalettes + "P" : p.NombreFardeaux + "F",
                Total = p.TotalFardeaux + " Fardeaux " + p.TotalBouteilles + " Bouteilles",
                Date = p.Date,
                Observation = p.Observation
            }).ToList();
            dgvVente.HideIDColumn();

        }

        private void btmodifier_Click(object sender, EventArgs e)
        {
            var id = int.Parse(dgvVente.SelectedRows[0].Cells[0].Value.ToString());
            if (id != null)
            {
                new FrDistribution(id).ShowDialog();
                Recherche();
            }
        }


      
    }
}
