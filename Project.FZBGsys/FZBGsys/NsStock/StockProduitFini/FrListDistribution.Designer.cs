﻿namespace FZBGsys.NsStock.StockProduitFini
{
    partial class FrListDistribution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btRecherche = new System.Windows.Forms.Button();
            this.chRechAvancee = new System.Windows.Forms.CheckBox();
            this.btImprimer = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.dgvVente = new System.Windows.Forms.DataGridView();
            this.panRechAvance = new System.Windows.Forms.Panel();
            this.cbformat = new System.Windows.Forms.ComboBox();
            this.cbgout = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtclient = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtfin = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.dtdebut = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btmodifier = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVente)).BeginInit();
            this.panRechAvance.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(46, 335);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(110, 28);
            this.btRecherche.TabIndex = 81;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // chRechAvancee
            // 
            this.chRechAvancee.AutoSize = true;
            this.chRechAvancee.Location = new System.Drawing.Point(19, 60);
            this.chRechAvancee.Margin = new System.Windows.Forms.Padding(2);
            this.chRechAvancee.Name = "chRechAvancee";
            this.chRechAvancee.Size = new System.Drawing.Size(125, 17);
            this.chRechAvancee.TabIndex = 82;
            this.chRechAvancee.Text = "Recherche Avancée";
            this.chRechAvancee.UseVisualStyleBackColor = true;
            this.chRechAvancee.CheckedChanged += new System.EventHandler(this.chRechAvancee_CheckedChanged);
            // 
            // btImprimer
            // 
            this.btImprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btImprimer.Location = new System.Drawing.Point(240, 335);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(108, 28);
            this.btImprimer.TabIndex = 79;
            this.btImprimer.Text = "Imprimer";
            this.btImprimer.UseVisualStyleBackColor = true;
            // 
            // btFermer
            // 
            this.btFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btFermer.Location = new System.Drawing.Point(822, 335);
            this.btFermer.Margin = new System.Windows.Forms.Padding(2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(108, 28);
            this.btFermer.TabIndex = 80;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            // 
            // dgvVente
            // 
            this.dgvVente.AllowUserToAddRows = false;
            this.dgvVente.AllowUserToDeleteRows = false;
            this.dgvVente.AllowUserToResizeColumns = false;
            this.dgvVente.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvVente.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvVente.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvVente.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvVente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVente.Location = new System.Drawing.Point(240, 21);
            this.dgvVente.Margin = new System.Windows.Forms.Padding(2);
            this.dgvVente.MultiSelect = false;
            this.dgvVente.Name = "dgvVente";
            this.dgvVente.ReadOnly = true;
            this.dgvVente.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvVente.RowHeadersVisible = false;
            this.dgvVente.RowTemplate.Height = 16;
            this.dgvVente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVente.Size = new System.Drawing.Size(701, 300);
            this.dgvVente.TabIndex = 74;
            // 
            // panRechAvance
            // 
            this.panRechAvance.BackColor = System.Drawing.SystemColors.Control;
            this.panRechAvance.Controls.Add(this.cbformat);
            this.panRechAvance.Controls.Add(this.cbgout);
            this.panRechAvance.Controls.Add(this.label2);
            this.panRechAvance.Controls.Add(this.label3);
            this.panRechAvance.Controls.Add(this.txtclient);
            this.panRechAvance.Controls.Add(this.panel1);
            this.panRechAvance.Enabled = false;
            this.panRechAvance.Location = new System.Drawing.Point(9, 94);
            this.panRechAvance.Margin = new System.Windows.Forms.Padding(2);
            this.panRechAvance.Name = "panRechAvance";
            this.panRechAvance.Size = new System.Drawing.Size(223, 227);
            this.panRechAvance.TabIndex = 88;
            // 
            // cbformat
            // 
            this.cbformat.FormattingEnabled = true;
            this.cbformat.Location = new System.Drawing.Point(151, 42);
            this.cbformat.Margin = new System.Windows.Forms.Padding(2);
            this.cbformat.Name = "cbformat";
            this.cbformat.Size = new System.Drawing.Size(62, 21);
            this.cbformat.TabIndex = 73;
            // 
            // cbgout
            // 
            this.cbgout.FormattingEnabled = true;
            this.cbgout.Location = new System.Drawing.Point(57, 42);
            this.cbgout.Margin = new System.Windows.Forms.Padding(2);
            this.cbgout.Name = "cbgout";
            this.cbgout.Size = new System.Drawing.Size(90, 21);
            this.cbgout.TabIndex = 72;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Produit:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 20);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Client:";
            // 
            // txtclient
            // 
            this.txtclient.Location = new System.Drawing.Point(57, 18);
            this.txtclient.Margin = new System.Windows.Forms.Padding(2);
            this.txtclient.Name = "txtclient";
            this.txtclient.Size = new System.Drawing.Size(156, 20);
            this.txtclient.TabIndex = 26;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.dtfin);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.dtdebut);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(39, 110);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(160, 57);
            this.panel1.TabIndex = 24;
            // 
            // dtfin
            // 
            this.dtfin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtfin.Location = new System.Drawing.Point(47, 31);
            this.dtfin.Margin = new System.Windows.Forms.Padding(2);
            this.dtfin.Name = "dtfin";
            this.dtfin.Size = new System.Drawing.Size(105, 20);
            this.dtfin.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 37);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Au:";
            // 
            // dtdebut
            // 
            this.dtdebut.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtdebut.Location = new System.Drawing.Point(47, 8);
            this.dtdebut.Margin = new System.Windows.Forms.Padding(2);
            this.dtdebut.Name = "dtdebut";
            this.dtdebut.Size = new System.Drawing.Size(105, 20);
            this.dtdebut.TabIndex = 1;
            this.dtdebut.Value = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, -38);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Du:";
            // 
            // dtDate
            // 
            this.dtDate.Location = new System.Drawing.Point(62, 21);
            this.dtDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(168, 20);
            this.dtDate.TabIndex = 90;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 89;
            this.label1.Text = "Date :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "De:";
            // 
            // btmodifier
            // 
            this.btmodifier.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btmodifier.Location = new System.Drawing.Point(362, 335);
            this.btmodifier.Margin = new System.Windows.Forms.Padding(2);
            this.btmodifier.Name = "btmodifier";
            this.btmodifier.Size = new System.Drawing.Size(108, 28);
            this.btmodifier.TabIndex = 79;
            this.btmodifier.Text = "Modifier";
            this.btmodifier.UseVisualStyleBackColor = true;
            this.btmodifier.Click += new System.EventHandler(this.btmodifier_Click);
            // 
            // FrListDistribution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 412);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panRechAvance);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.chRechAvancee);
            this.Controls.Add(this.btmodifier);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.dgvVente);
            this.Name = "FrListDistribution";
            this.Text = "Liste Distribution Gratuite";
            ((System.ComponentModel.ISupportInitialize)(this.dgvVente)).EndInit();
            this.panRechAvance.ResumeLayout(false);
            this.panRechAvance.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.CheckBox chRechAvancee;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.DataGridView dgvVente;
        private System.Windows.Forms.Panel panRechAvance;
        private System.Windows.Forms.ComboBox cbformat;
        private System.Windows.Forms.ComboBox cbgout;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtclient;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtfin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtdebut;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btmodifier;
    }
}