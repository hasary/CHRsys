﻿namespace FZBGsys.NsStock
{
    partial class FrListTransfertProduitFini
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvPalettes = new System.Windows.Forms.DataGridView();
            this.panDate = new System.Windows.Forms.Panel();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTypeProduit = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbLot = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtDateProductionAu = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtDateProductionDu = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.btRecherche = new System.Windows.Forms.Button();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.btFermer = new System.Windows.Forms.Button();
            this.panRecherche = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btSupprimer = new System.Windows.Forms.Button();
            this.labTransfert = new System.Windows.Forms.Label();
            this.tcDataView = new System.Windows.Forms.TabControl();
            this.tpPalette = new System.Windows.Forms.TabPage();
            this.tpFardeaux = new System.Windows.Forms.TabPage();
            this.dgvFardeaux = new System.Windows.Forms.DataGridView();
            this.btImprimer = new System.Windows.Forms.Button();
            this.chRechercheAvance = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPalettes)).BeginInit();
            this.panDate.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panRecherche.SuspendLayout();
            this.tcDataView.SuspendLayout();
            this.tpPalette.SuspendLayout();
            this.tpFardeaux.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFardeaux)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvPalettes
            // 
            this.dgvPalettes.AllowUserToAddRows = false;
            this.dgvPalettes.AllowUserToDeleteRows = false;
            this.dgvPalettes.AllowUserToResizeColumns = false;
            this.dgvPalettes.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPalettes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPalettes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvPalettes.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPalettes.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvPalettes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPalettes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPalettes.Location = new System.Drawing.Point(4, 4);
            this.dgvPalettes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvPalettes.MultiSelect = false;
            this.dgvPalettes.Name = "dgvPalettes";
            this.dgvPalettes.ReadOnly = true;
            this.dgvPalettes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPalettes.RowHeadersVisible = false;
            this.dgvPalettes.RowTemplate.Height = 16;
            this.dgvPalettes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPalettes.Size = new System.Drawing.Size(777, 406);
            this.dgvPalettes.TabIndex = 9;
            // 
            // panDate
            // 
            this.panDate.Controls.Add(this.dtDateAu);
            this.panDate.Controls.Add(this.label5);
            this.panDate.Controls.Add(this.dtDateDu);
            this.panDate.Controls.Add(this.label4);
            this.panDate.Location = new System.Drawing.Point(27, 23);
            this.panDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panDate.Name = "panDate";
            this.panDate.Size = new System.Drawing.Size(213, 70);
            this.panDate.TabIndex = 23;
            this.panDate.Visible = false;
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(63, 38);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(139, 22);
            this.dtDateAu.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 17);
            this.label5.TabIndex = 20;
            this.label5.Text = "Au:";
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(63, 10);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(139, 22);
            this.dtDateDu.TabIndex = 1;
            this.dtDateDu.Value = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Du:";
            // 
            // cbTypeProduit
            // 
            this.cbTypeProduit.FormattingEnabled = true;
            this.cbTypeProduit.Location = new System.Drawing.Point(83, 130);
            this.cbTypeProduit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbTypeProduit.Name = "cbTypeProduit";
            this.cbTypeProduit.Size = new System.Drawing.Size(157, 24);
            this.cbTypeProduit.TabIndex = 45;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 133);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 44;
            this.label1.Text = "Produit:";
            // 
            // cbLot
            // 
            this.cbLot.FormattingEnabled = true;
            this.cbLot.Location = new System.Drawing.Point(109, 208);
            this.cbLot.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbLot.Name = "cbLot";
            this.cbLot.Size = new System.Drawing.Size(93, 24);
            this.cbLot.TabIndex = 47;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(63, 212);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 17);
            this.label13.TabIndex = 46;
            this.label13.Text = "Lot:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 170);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 46;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(109, 166);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(93, 24);
            this.cbFormat.TabIndex = 47;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtDateProductionAu);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.dtDateProductionDu);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(27, 277);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 70);
            this.panel1.TabIndex = 49;
            this.panel1.Visible = false;
            // 
            // dtDateProductionAu
            // 
            this.dtDateProductionAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateProductionAu.Location = new System.Drawing.Point(63, 38);
            this.dtDateProductionAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateProductionAu.Name = "dtDateProductionAu";
            this.dtDateProductionAu.Size = new System.Drawing.Size(139, 22);
            this.dtDateProductionAu.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Au:";
            // 
            // dtDateProductionDu
            // 
            this.dtDateProductionDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateProductionDu.Location = new System.Drawing.Point(63, 10);
            this.dtDateProductionDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateProductionDu.Name = "dtDateProductionDu";
            this.dtDateProductionDu.Size = new System.Drawing.Size(139, 22);
            this.dtDateProductionDu.TabIndex = 1;
            this.dtDateProductionDu.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Du:";
            // 
            // btRecherche
            // 
            this.btRecherche.Location = new System.Drawing.Point(15, 498);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(147, 34);
            this.btRecherche.TabIndex = 50;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // dtDate
            // 
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(127, 28);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(139, 22);
            this.dtDate.TabIndex = 51;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 17);
            this.label7.TabIndex = 52;
            this.label7.Text = "Date Transfert:";
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(941, 498);
            this.btFermer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(139, 34);
            this.btFermer.TabIndex = 50;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // panRecherche
            // 
            this.panRecherche.Controls.Add(this.label9);
            this.panRecherche.Controls.Add(this.label8);
            this.panRecherche.Controls.Add(this.panDate);
            this.panRecherche.Controls.Add(this.label1);
            this.panRecherche.Controls.Add(this.cbTypeProduit);
            this.panRecherche.Controls.Add(this.label13);
            this.panRecherche.Controls.Add(this.cbLot);
            this.panRecherche.Controls.Add(this.panel1);
            this.panRecherche.Controls.Add(this.label2);
            this.panRecherche.Controls.Add(this.cbFormat);
            this.panRecherche.Enabled = false;
            this.panRecherche.Location = new System.Drawing.Point(15, 112);
            this.panRecherche.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panRecherche.Name = "panRecherche";
            this.panRecherche.Size = new System.Drawing.Size(256, 370);
            this.panRecherche.TabIndex = 53;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 258);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 17);
            this.label9.TabIndex = 51;
            this.label9.Text = "Date Production";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 50;
            this.label8.Text = "Date Transfert";
            // 
            // btSupprimer
            // 
            this.btSupprimer.Location = new System.Drawing.Point(287, 497);
            this.btSupprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSupprimer.Name = "btSupprimer";
            this.btSupprimer.Size = new System.Drawing.Size(169, 34);
            this.btSupprimer.TabIndex = 50;
            this.btSupprimer.Text = "Supprimer Selection";
            this.btSupprimer.UseVisualStyleBackColor = true;
            this.btSupprimer.Click += new System.EventHandler(this.btSupprimer_Click);
            // 
            // labTransfert
            // 
            this.labTransfert.AutoSize = true;
            this.labTransfert.Location = new System.Drawing.Point(292, 28);
            this.labTransfert.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labTransfert.Name = "labTransfert";
            this.labTransfert.Size = new System.Drawing.Size(0, 17);
            this.labTransfert.TabIndex = 54;
            // 
            // tcDataView
            // 
            this.tcDataView.Controls.Add(this.tpPalette);
            this.tcDataView.Controls.Add(this.tpFardeaux);
            this.tcDataView.Location = new System.Drawing.Point(287, 48);
            this.tcDataView.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tcDataView.Name = "tcDataView";
            this.tcDataView.SelectedIndex = 0;
            this.tcDataView.Size = new System.Drawing.Size(793, 443);
            this.tcDataView.TabIndex = 55;
            // 
            // tpPalette
            // 
            this.tpPalette.Controls.Add(this.dgvPalettes);
            this.tpPalette.Location = new System.Drawing.Point(4, 25);
            this.tpPalette.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpPalette.Name = "tpPalette";
            this.tpPalette.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpPalette.Size = new System.Drawing.Size(785, 414);
            this.tpPalette.TabIndex = 0;
            this.tpPalette.Text = "Transfert Palettes";
            this.tpPalette.UseVisualStyleBackColor = true;
            // 
            // tpFardeaux
            // 
            this.tpFardeaux.Controls.Add(this.dgvFardeaux);
            this.tpFardeaux.Location = new System.Drawing.Point(4, 25);
            this.tpFardeaux.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpFardeaux.Name = "tpFardeaux";
            this.tpFardeaux.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpFardeaux.Size = new System.Drawing.Size(785, 414);
            this.tpFardeaux.TabIndex = 1;
            this.tpFardeaux.Text = "Transfert Fardeaux";
            this.tpFardeaux.UseVisualStyleBackColor = true;
            // 
            // dgvFardeaux
            // 
            this.dgvFardeaux.AllowUserToAddRows = false;
            this.dgvFardeaux.AllowUserToDeleteRows = false;
            this.dgvFardeaux.AllowUserToResizeColumns = false;
            this.dgvFardeaux.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvFardeaux.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvFardeaux.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvFardeaux.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvFardeaux.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvFardeaux.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFardeaux.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFardeaux.Location = new System.Drawing.Point(4, 4);
            this.dgvFardeaux.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvFardeaux.MultiSelect = false;
            this.dgvFardeaux.Name = "dgvFardeaux";
            this.dgvFardeaux.ReadOnly = true;
            this.dgvFardeaux.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvFardeaux.RowHeadersVisible = false;
            this.dgvFardeaux.RowTemplate.Height = 16;
            this.dgvFardeaux.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFardeaux.Size = new System.Drawing.Size(665, 347);
            this.dgvFardeaux.TabIndex = 10;
            // 
            // btImprimer
            // 
            this.btImprimer.Location = new System.Drawing.Point(789, 498);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(145, 33);
            this.btImprimer.TabIndex = 56;
            this.btImprimer.Text = "Imprimer La Liste";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // chRechercheAvance
            // 
            this.chRechercheAvance.AutoSize = true;
            this.chRechercheAvance.Location = new System.Drawing.Point(15, 70);
            this.chRechercheAvance.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chRechercheAvance.Name = "chRechercheAvance";
            this.chRechercheAvance.Size = new System.Drawing.Size(157, 21);
            this.chRechercheAvance.TabIndex = 57;
            this.chRechercheAvance.Text = "Recherche avancée";
            this.chRechercheAvance.UseVisualStyleBackColor = true;
            this.chRechercheAvance.CheckedChanged += new System.EventHandler(this.cbRechercheAvance_CheckedChanged);
            // 
            // FrListTransfertProduitFini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1096, 546);
            this.Controls.Add(this.chRechercheAvance);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.tcDataView);
            this.Controls.Add(this.labTransfert);
            this.Controls.Add(this.panRecherche);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.btSupprimer);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.btRecherche);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrListTransfertProduitFini";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Liste Transfert Produit Fini";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrListTransfertProduitFini_FormClosed);
            this.Load += new System.EventHandler(this.FrListTransfertProduitFini_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPalettes)).EndInit();
            this.panDate.ResumeLayout(false);
            this.panDate.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panRecherche.ResumeLayout(false);
            this.panRecherche.PerformLayout();
            this.tcDataView.ResumeLayout(false);
            this.tpPalette.ResumeLayout(false);
            this.tpFardeaux.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvFardeaux)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPalettes;
        private System.Windows.Forms.Panel panDate;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbTypeProduit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbLot;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtDateProductionAu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtDateProductionDu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Panel panRecherche;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btSupprimer;
        private System.Windows.Forms.Label labTransfert;
        private System.Windows.Forms.TabControl tcDataView;
        private System.Windows.Forms.TabPage tpPalette;
        private System.Windows.Forms.TabPage tpFardeaux;
        private System.Windows.Forms.DataGridView dgvFardeaux;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.CheckBox chRechercheAvance;
    }
}