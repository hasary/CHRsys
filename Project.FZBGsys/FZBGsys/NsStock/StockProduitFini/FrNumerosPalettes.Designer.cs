﻿namespace FZBGsys.NsStock
{
    partial class FrNumerosPalettes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGoutProduit = new System.Windows.Forms.TextBox();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btOk = new System.Windows.Forms.Button();
            this.txtNumeros = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbSelected = new System.Windows.Forms.ListBox();
            this.lbDispo = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date:";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(48, 19);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(76, 20);
            this.txtDate.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 21);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Produits";
            // 
            // txtGoutProduit
            // 
            this.txtGoutProduit.Location = new System.Drawing.Point(193, 19);
            this.txtGoutProduit.Margin = new System.Windows.Forms.Padding(2);
            this.txtGoutProduit.Name = "txtGoutProduit";
            this.txtGoutProduit.ReadOnly = true;
            this.txtGoutProduit.Size = new System.Drawing.Size(126, 20);
            this.txtGoutProduit.TabIndex = 1;
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(131, 257);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(70, 24);
            this.btEnlever.TabIndex = 15;
            this.btEnlever.Text = "<<Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(131, 125);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(70, 25);
            this.btAjouter.TabIndex = 14;
            this.btAjouter.Text = "Ajouter>>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(170, 376);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(74, 26);
            this.btAnnuler.TabIndex = 13;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(248, 376);
            this.btOk.Margin = new System.Windows.Forms.Padding(2);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(72, 26);
            this.btOk.TabIndex = 12;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // txtNumeros
            // 
            this.txtNumeros.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumeros.Location = new System.Drawing.Point(19, 380);
            this.txtNumeros.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumeros.Name = "txtNumeros";
            this.txtNumeros.ReadOnly = true;
            this.txtNumeros.Size = new System.Drawing.Size(147, 20);
            this.txtNumeros.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 62);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Palettes Disponnibles";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(205, 62);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Palettes Séléctionnées";
            // 
            // lbSelected
            // 
            this.lbSelected.FormattingEnabled = true;
            this.lbSelected.Location = new System.Drawing.Point(208, 79);
            this.lbSelected.Margin = new System.Windows.Forms.Padding(2);
            this.lbSelected.Name = "lbSelected";
            this.lbSelected.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbSelected.Size = new System.Drawing.Size(109, 290);
            this.lbSelected.TabIndex = 7;
            // 
            // lbDispo
            // 
            this.lbDispo.FormattingEnabled = true;
            this.lbDispo.Location = new System.Drawing.Point(19, 79);
            this.lbDispo.Margin = new System.Windows.Forms.Padding(2);
            this.lbDispo.Name = "lbDispo";
            this.lbDispo.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.lbDispo.Size = new System.Drawing.Size(105, 290);
            this.lbDispo.TabIndex = 8;
            // 
            // FrNumerosPalettes
            // 
            this.AcceptButton = this.btOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btAnnuler;
            this.ClientSize = new System.Drawing.Size(328, 406);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.btAnnuler);
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.txtNumeros);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbSelected);
            this.Controls.Add(this.lbDispo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtGoutProduit);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrNumerosPalettes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Selection Numéros Palettes";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGoutProduit;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.TextBox txtNumeros;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox lbSelected;
        private System.Windows.Forms.ListBox lbDispo;
    }
}