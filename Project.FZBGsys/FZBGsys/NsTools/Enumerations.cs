﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FZBGsys
{

    public enum EnGenerationSoldeMethod
    {
        FromSoldeHistory,
        FromFistSoldeEver
    }
    public enum EnTypeBon
    {
        Bon_Attribution = 1,
        Bon_Sortie = 2,
        Bon_Livraison = 3,
        Facture = 4,
        Bon_Retour = 5,
        Facture_Retour = 6,

    }
    public enum EnEtatFacture
    {
        Non_payée = 0,
        Payée = 1,
        Annulée,

    }
    public enum EnCatOperation
    {
        Recette = 1,
        Depense = -1,

    }
    public enum EnTypeOpration
    {

        Reglement_Créance = 1,
        Avance = 2,
        Payement_au_Comptant = 3,
        Pret_Client = 4,
        Pret_Associer_NMC_ENTREE = 5,
        Payement_Fournisseur = 6,
        Rembourssement_Pret_Associer_NMC_SORTIE = 7,
        Rembourssement_Pret_Client = 8,
        Verssement_Bancaire = 9,
        Salaires = 10,
        Avances_sur_Salaire = 11,
        Honoraire = 12,
        Impos_Assurance_Domaine = 13,
        Charge_Usine = 14,
        Associes = 15,
        Credit_Anterieur_ALC = 16,
        Pret_ASSOCIER_NMC_SORTIE = 17,
        Payement_Divers = 18,
        Prevision_de_Dépenses = 19,
        Pyement_Importation = 20,
        Remboursement_Client_Traite = 21,
        Investissement_Amenagement = 23

    }
    public enum EnTypeArret
    {
        Rinçage = 0,
        RinçageFinProd = 4,
        CIP = 5,
        Maintenance = 1,
        Autre = 2,

    }
    public enum EnModePayement
    {
        Especes = 1,
        Avance = 2,
        Virement = 3,
        Chèque = 4,
        Traite = 5,
        a_Terme = 6,
        Gratuit = 7,
    }
    public enum EnUniteProduit
    {
        Palettes = 1,
        Fardeaux = 2

    }
    public enum EnEtatBonAttribution
    {
        EnInstance = 1,
        Chargé = 2,
        Livré = 3,
        Annulé = 0,

    }

    public enum EnEtatBonCommande
    {
        EnInstance = 1,
        Solde = 2,
        Annulé = 0,

    }
    public enum EnUniteParDefault
    {
        Quantite = 0,
        Piece = 1

    }
    public enum EnFormat
    {
        Format1L = 1,
        Format1_5L = 2,
        Format2L = 3,
    }
    public enum EnTypeBouchon
    {
        Plastoque = 1,
        Courane = 2,
    }
    public enum EnTypeProduit
    {
        Chréa = 1,
        Orange_Chréa = 2,


    }
    public enum EnTypeEtiquette
    {
        Nilon = 1,
        Papier = 2,


    }
    public enum EnClassRessource
    {
        Bouteillage = 1,
        Siroperie = 2,
        Embalage = 3

    }
    public enum EnSection
    {/*
      1	Administration
2	Direction
3	Unité PET
5	Magazin
6	Unité Canette
7	Comercial
9	Unité Verre
100	Externe (Vendu)

      
      */

        Administration = 1,
        Direction = 2,
        Unité_PET = 3,
        Unité_Verre = 9,
        Magazin = 5,
        Unité_Canette = 6,
        Autre = 4,
        Comercial = 7,
        Extern = 100,



    }
    public enum EnTypeRessource
    {
        Preforme = 1,
        Sucre = 2,
        Film_Thermofusible = 3,
        Film_Etirable = 4,
        Arrome = 5,
        Etiquette = 6,
        Bouchon = 7,
        Cole = 8,
        Bouteille_Verre = 9,
        Cannete = 10,
        Aspartam = 11,
        Acide_Citrique = 12,
        Consontre_Orange = 13,
        CO2 = 14,
        InterCalaire = 15,
        Colorant = 16,
        Benzoate = 17,
        Barquette = 18,
        Produit_Chimique = 19,
        Solvant = 20,
        Encre = 21,

        Prod_Chimique_Promain = 23,
        Prod_Chimique_Proneige_AL = 24,
        Prod_Chimique_Proneige_AC = 25,
        Prod_Chimique_OXIPRO_AS = 26,
        Prod_Chimique_Javel = 27,
        Prod_Chimique_Proflow = 28,
        Prod_Chimique_Acide_Chlorhydrique = 29,
        Couvercle = 30,
        Cittrate


    }

    public enum EnParametre
    {
        Impression = 1,
        RapportPeriodique = 2,
        RapportJournalier = 3,
        RapportMensual = 4,
    }
}
