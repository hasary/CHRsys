using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

namespace FZBGsys.NsTools
{
    public class MergeEx
    {
        #region Fields
        private string sourcefolder;
        private string destinationfile;
        private ArrayList fileList = new ArrayList();
        #endregion

        #region Public Methods
        ///
        /// Add a new file, together with a given docname to the fileList and namelist collection
        ///
        public void AddFile(string pathnname)
        {
            fileList.Add(pathnname);
        }

        ///
        /// Generate the merged PDF
        ///
        public void Execute()
        {
            MergeDocs();
        }
        #endregion

        #region Private Methods
        ///
        /// Merges the Docs and renders the destinationFile
        ///

        public static List<bool> TakeFirstPageOnly;
        private void MergeDocs()
        {

            //Step 1: Create a Docuement-Object
            Document document = new Document();
            try
            {
                //Step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationfile, FileMode.Create));

                //Step 3: Open the document
                document.Open();

                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;

                int n = 0;
                int rotation = 0;
                int nbr = 0;
                //Loops for each file that has been listed
                foreach (string filename in fileList)
                {
                    Chapter chapter1 = new Chapter(new Paragraph(" "), nbr);
                    nbr++;
                    chapter1.BookmarkTitle = nbr + "- " + filename.Replace(".pdf", "");
                    //  Section section1 = chapter1.AddSection(20f, "", 2);
                    //  Section section2 = chapter1.AddSection(20f, "", 2);

                    //The current file path
                    string filePath = sourcefolder + filename;

                    // we create a reader for the document
                    PdfReader reader = new PdfReader(filePath);

                    //Gets the number of pages to process
                    // n = 1;

                    if (TakeFirstPageOnly != null && !TakeFirstPageOnly[nbr - 1])
                    {
                        n = reader.NumberOfPages;
                    }
                    else
                    {
                        n = 1;
                    }

                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(reader.GetPageSizeWithRotation(1));
                        document.NewPage();

                        //Insert to Destination on the first page
                        if (i == 1)
                        {
                            Chunk fileRef = new Chunk(" ");
                            fileRef.SetLocalDestination(filename);
                            document.Add(chapter1);
                            document.Add(fileRef);
                        }

                        page = writer.GetImportedPage(reader, i);
                        rotation = reader.GetPageRotation(i);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(i).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }
                    }
                }
            }
            catch (Exception e) { throw e; }
            finally { document.Close(); }
        }
        #endregion

        #region Properties
        ///
        /// Gets or Sets the SourceFolder
        ///
        public string SourceFolder
        {
            get { return sourcefolder; }
            set { sourcefolder = value; }
        }

        ///
        /// Gets or Sets the DestinationFile
        ///
        public string DestinationFile
        {
            get { return destinationfile; }
            set { destinationfile = value; }
        }
        #endregion
    }
}