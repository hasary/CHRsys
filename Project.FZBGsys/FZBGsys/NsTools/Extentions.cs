﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys
{

    #region Extentions
    public static class Extensions
    {
        public static bool AllowModifyDate(this DateTime date)
        {
            if (Tools.CurrentUser.GroupeID == 1)
            {
                return true;
            }
            return date.CompareTo(Tools.CurrentDateTime) < Tools.DayBeforeCanModify;
        }



        public static bool SaveChangeTry(this ModelEntities db)
        {

            try
            {
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {

                Tools.ShowError(e.AllMessages("Impossible d'enregistrer"));
                return false;
            }

        }

        public static string ToAffInt(this decimal? deci, bool NullIsZero = false)
        {
            if (deci == null) return (NullIsZero) ? "0" : "";
            return ((decimal)deci).ToString("N2").Replace(",0000", "").Replace(",000", "").Replace(",00", "");
        }

        public static string ToAffInt(this decimal deci)
        {
            //  if (deci == null) return "";
            return deci.ToString("N2").Replace(",0000", "").Replace(",000", "").Replace(",00", "");
        }

        public static string ToModePayementTxt(this int? modeInt)
        {
            if (modeInt == null)
            {
                return "";
            }

            switch ((EnModePayement)modeInt)
            {
                case EnModePayement.Especes:
                    return "Especes";
                case EnModePayement.Avance:
                    return "Avance";
                case EnModePayement.Virement:
                    return "Virement";
                case EnModePayement.Chèque:
                    return "Chèque";
                case EnModePayement.Traite:
                    return "Traite";
                case EnModePayement.a_Terme:
                    return "à terme";
                default:
                    return "";
            }

        }

        public static string ToSatisfactionTxt(this int? val)
        {
            switch (val)
            {
                case 0: return "non satisfait";
                case 1: return "satisfait";
                case 2: return "très satisfait";
                default:
                    return "";
            }
        }
        public static string ToCategorieOperationTxt(this int? cat)
        {

            switch ((EnCatOperation)cat)
            {
                case EnCatOperation.Recette:
                    return "Recette";
                case EnCatOperation.Depense:
                    return "Depense";
                default:
                    return "Error";
            }


        }

        public static string ToEtatBonAttributionTxt(this int? noBon)
        {
            switch ((EnEtatBonAttribution)noBon)
            {
                case EnEtatBonAttribution.EnInstance:
                    return "En Instance";
                case EnEtatBonAttribution.Livré:
                    return "Livré";
                case EnEtatBonAttribution.Annulé:
                    return "Annulé";
                case EnEtatBonAttribution.Chargé:
                    return "Chargé";
                default:
                    return "";
            }

        }

        public static string ToEtatBonCommandeTxt(this int? noBon)
        {
            switch ((EnEtatBonCommande)noBon)
            {
                case EnEtatBonCommande.EnInstance:
                    return "En Instance";
                case EnEtatBonCommande.Solde:
                    return "Soldée";
                case EnEtatBonCommande.Annulé:
                    return "Annulée";
                
                default:
                    return "";
            }

        }


        public static string ToUniteProduitTxt(this int? unite)
        {
            switch ((EnUniteProduit)unite)
            {
                case EnUniteProduit.Palettes:
                    return "Palettes";
                case EnUniteProduit.Fardeaux:
                    return "Fardeaux";

                default: return "";

            }


        }

        public static string MinutesToHoursMinutesTxt(this int? minutes)
        {
            string result = "";
            if (minutes == null)
            {
                return "";
            }
            if (minutes == 0)
            {
                return "0 min";
            }
            var hour = (minutes / 60);
            var restMinutes = (minutes % 60);

            if (hour != 0)
            {
                result += hour.ToString() + " h ";
            }

            if (restMinutes != 0)
            {
                result += restMinutes.ToString() + " min";
            }

            return result;

        }
        public static int? GetTotalMinutesFromHeureTxt(this string heuretxt)
        {

            int? result = 0;

            if (!heuretxt.Contains("h"))
            {
                result = heuretxt.Replace("min", "").Replace(" ", "").ParseToInt();
            }
            else if (!heuretxt.Contains("min"))
            {
                result = 60 * heuretxt.Replace("h", "").Replace(" ", "").ParseToInt();
            }
            else
            {

                try
                {
                    string[] split = heuretxt.Replace(" ", "").Replace("h", "#").Replace("min", "").Split('#');

                    result = split[0].ParseToInt().Value * 60 + split[1].ParseToInt().Value;

                }
                catch (Exception)
                {

                    result = null;
                }
            }
            return result;
        }

        public static Ressource CloneRessource(this Ressource ers)
        {

            var rs = new Ressource();

            rs.TypeRessourceID = ers.TypeRessourceID;
            rs.FournisseurID = ers.FournisseurID;
            rs.TypePreformeID = ers.TypePreformeID;
            rs.TypeArromeID = ers.TypeArromeID;
            rs.FormatID = ers.FormatID;
            rs.EpaisseurFilmThermo = ers.EpaisseurFilmThermo;
            rs.TypeBouchonID = ers.TypeBouchonID;
            rs.TypeCole = ers.TypeCole;
            rs.IntercalaireFormat = ers.IntercalaireFormat;
            rs.CouleurBouchon = ers.CouleurBouchon;
            rs.TypeColorant = ers.TypeColorant;
            rs.TypeEtiquetteID = ers.TypeEtiquetteID;
            rs.EpaisseurFilmThermo = ers.EpaisseurFilmThermo;
            rs.GoutProduitID = ers.GoutProduitID;
            rs.TypeProduitChimiqueID = ers.TypeProduitChimiqueID;
            return rs;
        }
        /*    public static bool EqualsRessource(this Ressource rs, Ressource ers)
            {
                return
                    rs.TypeRessourceID == ers.TypeRessourceID &&
                    rs.FournisseurID == ers.FournisseurID &&
                    rs.TypePreformeID == ers.TypePreformeID &&
                    rs.TypeArromeID == ers.TypeArromeID &&
                    rs.FormatID == ers.FormatID &&
                    rs.TypeBouchonID == ers.TypeBouchonID &&
                    rs.TypeCole == ers.TypeCole &&
                    rs.IntercalaireFormat == ers.IntercalaireFormat &&
                    rs.CouleurBouchon == ers.CouleurBouchon &&
                    rs.TypeColorantID == ers.TypeColorantID &&
                    rs.TypeEtiquetteID == ers.TypeEtiquetteID &&
                    rs.EpaisseurFilmThermo == ers.EpaisseurFilmThermo &&
                    rs.GoutProduitID == ers.GoutProduitID &&
                    rs.TypeProduitChimiqueID == ers.TypeProduitChimiqueID;
            }
         */
        public static string AllMessages(this Exception e, string firstMessage)
        {
            var message = firstMessage;
            var exeption = e;
            while (exeption != null)
            {
                message += "\n-> " + exeption.Message;
                exeption = exeption.InnerException;
            }

            return message;
        }



        #region DataGridView
        public static int? GetSelectedID(this DataGridView dgv, string columName = null, bool showError = true)
        {
            if (dgv.RowCount == 0 || dgv.SelectedRows.Count == 0)
            {
               if(showError) Tools.ShowError("Aucun élément n'est séléctionné");
                return null;
            }
            var name = (columName == null) ? "ID" : columName;
            return int.Parse(dgv.SelectedRows[0].Cells[name].Value.ToString());

        }

        public static string GetSelectedCulumns(this DataGridView dgv, string columName = null)
        {
            if (dgv.RowCount == 0 || dgv.SelectedRows.Count == 0)
            {
                Tools.ShowError("Aucun élément n'est séléctionné");
                return null;
            }
            var name = (columName == null) ? "ID" : columName;
            return dgv.SelectedRows[0].Cells[name].Value.ToString();

        }

        public static int? GetSelectedIndex(this DataGridView dgv, bool showError = true)
        {
            if (dgv.RowCount == 0 || dgv.SelectedRows.Count == 0)
            {
                if (showError)
                {
                    Tools.ShowError("Aucun élément n'est séléctionné");
                }
                return null;
            }
            return dgv.SelectedRows[0].Index;

        }
        public static void HideIDColumn(this DataGridView dgv, string columName = null)
        {
            var name = (columName == null) ? "ID" : columName;
            dgv.Columns[name].Visible = false;
        }
        #endregion

        public static int? ParseToInt(this string number, bool acceptZero = true)
        {

            int outInt = 0;

            if (int.TryParse(number, out outInt))
            {
                if (outInt == 0)
                {
                    if (acceptZero)
                    {
                        return 0;
                    }
                    else
                    {
                        return null;
                    }
                }
                return outInt;
            }
            else
            {
                return null;
            }

        }
        public static decimal? ParseToDec(this string number, bool acceptZero = true)
        {

            decimal outDec = 0;

            if (decimal.TryParse(number, out outDec))
            {
                if (outDec == 0)
                {
                    if (acceptZero)
                    {
                        return 0;
                    }
                    else
                    {
                        return null;
                    }
                }
                return outDec;
            }
            else
            {
                return null;
            }

        }

        #region Dialog messages
        public static void ShowInformation(this Form form, string message)
        {
            MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void ShowError(this Form form, string message)
        {
            MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static void ShowWarning(this Form form, string message)
        {
            MessageBox.Show(message, "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public static bool ConfirmWarning(this Form form, string p)
        {

            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            return (dl == DialogResult.Yes);

        }

        public static string NomClientAndParent(this Client client)
        {
            if (client == null)
            {
                return "null";
            }
            if (client.ClientParentID != 0)
            {
                return client.Nom + " [" + client.ClientParent.Nom + "]";
            }

            return client.Nom;
        }

        public static bool ConfirmInformation(this Form form, string p)
        {

            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            return (dl == DialogResult.Yes);

        }
        #endregion

        #region Text
        public static string ToDescription(this Ressource ressource)
        {
            string desciption = "";

            if (ressource.TypeProduitChimique != null)
            {
                return ressource.TypeProduitChimique.Nom; // exception produit chimique
            }

            desciption = ressource.TypeRessource.Nom;

            if (ressource.TypePreforme != null)
            {
                desciption += " " + ressource.TypePreforme.GRS;// +" format " + ressource.PreformeGamme.Format.Volume;
            }

            if (ressource.TypeEtiquette != null)
            {
                desciption += " " + ressource.TypeEtiquette.Nom;
            }
            if (ressource.TypeArrome != null)
            {
                desciption += " " + ressource.TypeArrome.Nom;
            }

            if (ressource.TypeColorant != null)
            {
                desciption += " " + ressource.TypeColorant.Code;
            }

            if (ressource.GoutProduit != null)
            {
                desciption += " " + ressource.GoutProduit.Nom;
            }

            if (ressource.FormatBarquette != null)
            {
                desciption += " " + ressource.FormatBarquette;
            }

            if (ressource.EpaisseurFilmThermo != null)
            {
                desciption += " " + ressource.EpaisseurFilmThermo;
            }

            if (ressource.Format != null)
            {
                desciption += " " + ressource.Format.Volume;
            }

            if (ressource.IntercalaireFormat != null)
            {
                desciption += " " + ressource.IntercalaireFormat;
            }

            if (ressource.TypeBouchon != null)
            {
                desciption += " " + ressource.TypeBouchon.Nom + " " + ressource.CouleurBouchon;
            }

            if (ressource.TypeCole != null)
            {
                desciption += " " + ressource.TypeCole;
            }

            return desciption;
        }
        public static string ToSectionTxt(this EnSection section)
        {


            switch (section)
            {
                case EnSection.Administration: return "Administration";

                case EnSection.Direction:
                    return "Administration";
                case EnSection.Unité_PET:
                    return "Unité PET";
                case EnSection.Unité_Canette:
                    return "Unité Canette";
                case EnSection.Magazin:
                    return "Magazin";
                case EnSection.Autre:
                    return "Autres Destinations";
                case EnSection.Comercial:
                    return "Comercial";
                case EnSection.Extern:
                    return "Externe";
                default:
                    return "";
            }
        }
        public static string TypeProduitTxt(this Production Production)
        {
            string result = "";
            var sessions = Production.SessionProductions.Where(p=>p.GoutProduit != null);

            foreach (var item in sessions)
            {
                 result += " & " + item.GoutProduit.Nom;
            }
            if (result != "")
            {
                result = result.Substring(2);
            }
            return result;
        }

        #endregion


        public static StockPETPalette SetNumeros(this StockPETPalette stock, string Numeros = null)
        {

            if (Numeros != null)
            {
                stock.Numeros = Numeros;
            }
            stock.Nombre = stock.Numeros.ToNombrePlalettes();
            stock.TotalBouteilles = stock.Nombre * stock.SessionProduction.Production.Format.BouteillesParFardeau * stock.SessionProduction.Production.Format.FardeauParPalette;

            return stock;
        }


        public static StockPalette SetNumeros(this StockPalette stock, string Numeros = null)
        {

            if (Numeros != null)
            {
                stock.Numeros = Numeros;
            }
            stock.Nombre = stock.Numeros.ToNombrePlalettes();
            stock.TotalBouteilles = stock.Nombre * stock.Format.BouteillesParFardeau * stock.Format.FardeauParPalette;

            return stock;
        }



        #region Numeros Plaques
        public static string ToNumerosPalettes(this List<int> listNumeorsInt)
        {

            return FZBGsys.NsStock.FrNumerosPalettes.ListInt2Numeros(listNumeorsInt);
        }
        public static List<int> ToListInt(this string Numeors)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.Numeros2ListInt(Numeors);
        }
        public static string ToGroupedNumeros(this List<string> listNumeors)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.ListNumeros2GroupedNumeros(listNumeors);
        }
        public static bool ContainsNumeros(this string Numeros, string pattern)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.CommonNumeros(Numeros, pattern) != null;
        }
        public static int ToNombrePlalettes(this string Numeors)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.Numeros2Nombre(Numeors);
        }
        public static string ToOrderedNumeros(this string Numeros)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.Numeros2OrderedNumeros(Numeros);

        }


        public static string AddNumeros(this string Numeros, string pattern)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.AddNumeros(Numeros, pattern);
        }

        public static string RemoveNumeros(this string Numeros, string pattern)
        {
            return FZBGsys.NsStock.FrNumerosPalettes.MinusNumeros(Numeros, pattern);
        }
        #endregion


    }
    #endregion
}
