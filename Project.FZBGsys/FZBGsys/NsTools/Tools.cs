﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;

namespace FZBGsys
{
    #region tools
    public static class Tools
    {
        public static int CurrentUserID;

        //public static string SQL_Server = @"192.168.2.110\SQLFZBG";
        public static string SQL_Server = @"localhost\SQLHOME";
        public static string SQL_DataBase = @"FZBGdb";
        public static string SQL_UserName = @"sa";
        public static string SQL_Password = @"SNKbegin=1973";
        public static int MonthBeforeClientNonActif = 3;

        public static bool StrategieCDPanier = false;
        public static int[] CoefPrioriteAutirise = { 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, 16, 18, 20, 24, 25, 27, 30, 36, 40, 45, 48, 50, 54, 60, 72, 75, 80, 90, 100 };

        //------------------------------------------------------- contants ----------------
        public static bool AutoCreateTransfertFromProductionMP = true;//                  - 
        public static bool AutoCreateTransfertFromProductionPF = false;//                 -
        public static int DayBeforeCanModify = 2;//                                       -                                         
        //---------------------------------------------------------------------------------
        public static void UdatedbAutorisations(MenuStrip menu)
        {
            var db = new ModelEntities();

            #region update text or delete autorisations if items not exists

            Dictionary<string, string> menuItemsNames = new Dictionary<string, string>();

            foreach (var item in menu.Items)
            {
                var tsItem = (ToolStripMenuItem)item;
                menuItemsNames.Add(tsItem.Name, tsItem.Text);
                foreach (ToolStripItem subitem in tsItem.DropDownItems)
                {
                    menuItemsNames.Add(subitem.Name, subitem.Text);
                }
            }


            foreach (var item in db.ApplicationActions)
            {
                var men = menuItemsNames.SingleOrDefault(p => p.Key == item.Nom);
                if (men.Value == null)
                {
                    var autorisation = db.ApplicationAutorisations.Where(p => p.ApplicationActionID == item.ID);
                    foreach (var act in autorisation)
                    {
                        db.DeleteObject(act);
                    }
                    db.DeleteObject(item);
                }
                else if (item.Text != men.Value)
                {
                    item.Text = men.Value;
                }


            }

            db.SaveChanges();

            #endregion

            int mainPos = 0;
            foreach (var item in menu.Items)
            {

                var tsItem = (ToolStripMenuItem)item;
                mainPos++;

                var applicationAction = db.ApplicationActions.SingleOrDefault(p => p.Nom == tsItem.Name);
                if (applicationAction == null)
                {
                    applicationAction = new ApplicationAction
                   {
                       Text = tsItem.Text,
                       //   IsPublic = false,
                       Nom = tsItem.Name,
                       ParentActionID = 0,
                       Position = mainPos
                   };

                    db.AddToApplicationActions(applicationAction);
                    db.AddToApplicationAutorisations(new ApplicationAutorisation
                    {
                        ApplicationAction = applicationAction,
                        GroupeID = 15,
                        Autorisation = true,

                    });
                    db.SaveChanges();
                }


                int pos = 0;
                foreach (ToolStripItem subitem in tsItem.DropDownItems)
                {

                    if (!(subitem is ToolStripMenuItem))
                        continue;
                    pos++;


                    var applicationSubAction = db.ApplicationActions.SingleOrDefault(p => p.Nom == subitem.Name);
                    if (applicationSubAction == null)
                    {
                        applicationSubAction = new ApplicationAction
                       {
                           Text = subitem.Text,
                           //  IsPublic = false,
                           Nom = subitem.Name,
                           ParentActionID = applicationAction.ID,
                           Position = pos
                       };

                        db.AddToApplicationActions(applicationSubAction);
                        db.AddToApplicationAutorisations(new ApplicationAutorisation
                        {
                            ApplicationAction = applicationSubAction,
                            GroupeID = 15,
                            Autorisation = true,

                        });
                        db.SaveChanges();
                    }


                }

            }
            db.Dispose();
        }
        public static void BackupDataBaseManual(string fileName = null, bool silent = false)
        {

            SqlConnection conn = null;

            try
            {
                conn = new
                       SqlConnection(@"Server=" + SQL_Server + ";DataBase=" + SQL_DataBase + ";User ID=" + "sa" + ";password=" + SQL_Password);
                conn.Open();
                SqlCommand cmd = new SqlCommand("USE [master]", conn);
                cmd.ExecuteNonQuery();

                if (fileName == null)
                {
                    SaveFileDialog dialog = new SaveFileDialog();
                    dialog.Filter = "Fichier Sauveguarde *.bak|*.bak";
                    dialog.FileName = SQL_DataBase + "_" + DateTime.Now.ToString("yyyyMMddhhmm") + ".bak";
                    var res = dialog.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        fileName = dialog.FileName;

                    }
                    else
                    {
                        return;
                    }
                }
                var commande = "BACKUP DATABASE [" + SQL_DataBase + "] TO  DISK = '" + fileName + "' WITH NOFORMAT, INIT,  NAME = '" + SQL_DataBase + "-Complète Base de données Sauvegarde', SKIP, NOREWIND, NOUNLOAD,  STATS = 10";
                SqlCommand cmd2 = new SqlCommand(commande, conn);


                cmd2.ExecuteNonQuery();
                if (!silent) Tools.ShowInformation("Suaveguarde réussie");
            }
            catch (Exception ex)
            {
                Tools.ShowError(ex.AllMessages("Impossible de lancer la sauveguarde"));
            }


            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }



        }

        internal static void RestoreDataBaseManual(bool create = false)
        {

            string fileName = "";
            SqlConnection conn = null;

            try
            {
                conn = new
                       SqlConnection(@"Server=" + SQL_Server + ";DataBase=" + "master" + ";User ID=" + "sa" + ";password=" + SQL_Password);
                conn.Open();
                SqlCommand cmd = new SqlCommand("USE [master]", conn);
                cmd.ExecuteNonQuery();
                if (create && File.Exists("empty.mdb"))
                {
                    FileInfo fn = new FileInfo("empty.mdb");
                    fileName = fn.FullName;
                }
                else
                {
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Filter = "Fichier Sauveguarde *.mdb|*.mdb";
                    dialog.FileName = SQL_DataBase + "_" + DateTime.Now.ToString("yyyyMMddhhmm") + ".mdb";
                    var res = dialog.ShowDialog();
                    if (res == DialogResult.OK)
                    {
                        fileName = dialog.FileName;

                    }
                    else
                    {
                        return;
                    }

                }
                // var com = "DELETE DATABASE TRBsys";
                // SqlCommand cmd0 = new SqlCommand(com, conn);

                var commande = "RESTORE DATABASE [" + Tools.SQL_DataBase + "]  FROM  DISK = '" + fileName + "' WITH  FILE = 1,  NOUNLOAD, REPLACE,  STATS = 10";
                SqlCommand cmd2 = new SqlCommand(commande, conn);
                if (create || Tools.ConfirmWarning("Confirmer la restauration des données??\nATTENTION: tout les données actuels seront EFFACES et remplacés par ceux du fichier selectionné!!!"))
                {
                    var single = "ALTER DATABASE [" + Tools.SQL_DataBase + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
                    SqlCommand cmds = new SqlCommand(single, conn);
                    if (!create) cmds.ExecuteNonQuery();

                    cmd2.ExecuteNonQuery();


                    single = "ALTER DATABASE [" + Tools.SQL_DataBase + "]  SET MULTI_USER";
                    cmds = new SqlCommand(single, conn);
                    if (!create) cmds.ExecuteNonQuery();
                    Tools.ShowInformation("Restauration réussie.");
                    if (!create) Application.Restart();
                }




            }
            catch (Exception ex)
            {
                Tools.ShowError(ex.AllMessages("Impossible de restaurer"));
            }


            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }


        }
        internal static bool ConfirmWarning(string p)
        {
            DialogResult dl = MessageBox.Show(p, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            return (dl == DialogResult.Yes);
        }

        internal static void ShowInformation(string message)
        {
            MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        internal static void ShowError(string message)
        {
            MessageBox.Show(message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public static bool? isBoss { get; set; }
        internal static void ClearPrintTemp()
        {
            SqlConnection conn = null;
            try
            {
                conn = new
                    SqlConnection(@"Server=" + Tools.SQL_Server + ";DataBase=" + Tools.SQL_DataBase + ";User ID=" + Tools.SQL_UserName + ";password=" + Tools.SQL_Password + "");
                conn.Open();
                SqlCommand cmd = new SqlCommand(
                    "DELETE FROM PrintTemp WHERE UID = " + Tools.CurrentUserID, conn);
                //  cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }
        }

        internal static void GenerateStockHistory(DateTime dateHistory)
        {
            SqlConnection conn = null;
            try
            {
                conn = new
                    SqlConnection(@"Server=" + Tools.SQL_Server + ";DataBase=" + Tools.SQL_DataBase + ";User ID=" + Tools.SQL_UserName + ";password=" + Tools.SQL_Password + "");
                conn.Open();
                SqlCommand cmd = new SqlCommand(
                    "EXEC [dbo].[GenerateStockHistory] @DateHistory = '" + dateHistory.ToString("yyyy-MM-dd") + "'", conn);
                //  cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }
        }


        public static void BackupDataBase()
        {


            SqlConnection conn = null;

            try
            {
                conn = new
                       SqlConnection(@"Server=" + Tools.SQL_Server + ";DataBase=" + Tools.SQL_DataBase + ";User ID=" + Tools.SQL_UserName + ";password=" + Tools.SQL_Password + "");
                conn.Open();
                SqlCommand cmd = new SqlCommand("USE [master]", conn);
                cmd.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("EXEC	[dbo].[backupFZBG]  ", conn);
                //  cmd.CommandType = CommandType.StoredProcedure;

                cmd2.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }


            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }

            }



        }

        public static DateTime GetServerDateTime()
        {
            const string query = "Select GetDate()";
            DataTable result = new DataTable();
            using (SqlConnection conn = new SqlConnection(@"Server=" + Tools.SQL_Server + ";DataBase=" + Tools.SQL_DataBase + ";User ID=" + Tools.SQL_UserName + ";password=" + Tools.SQL_Password + ""))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        result.Load(dr);
                    }
                }
            }

            var date = result.Rows[0][0].ToString();
            CurrentDateTime = DateTime.Parse(date);
            return DateTime.Parse(date);
        }
        public static int? GetMinutesBetween2Times(DateTime fin, DateTime debut)
        {
            if (fin.CompareTo(debut) > 0)
            {
                var result = (int)fin.TimeOfDay.TotalMinutes - (int)debut.TimeOfDay.TotalMinutes;

                if (result <= 0)
                {
                    return null;
                }
                return result;
            }
            else
            {

                if (fin.Hour > 12 || debut.Hour < 12)
                {
                    return null;
                }

                var xfin = fin.AddHours(12);
                var xdebut = debut.AddHours(-12);


                var result = (int)xfin.TimeOfDay.TotalMinutes - (int)xdebut.TimeOfDay.TotalMinutes;

                if (result <= 0)
                {
                    return null;
                }
                return result;
            }
        }
        public static DateTime? CurrentDateTime { get; set; }
        internal static DateTime GetFillDateFromDateTime(DateTime? Date, TimeSpan? HoursMinutes)
        {


            return new DateTime(Date.Value.Year, Date.Value.Month, Date.Value.Day, HoursMinutes.Value.Hours, HoursMinutes.Value.Minutes, 0);
        }
        internal static int? ConnectUserID()
        {
            var dialog = new FZBGsys.NsApplication.FrLoginForm(true);
            dialog.ShowDialog();
            int returnedID;
            if (FZBGsys.NsApplication.FrLoginForm.ConnectedUser != null)
            {
                returnedID = FZBGsys.NsApplication.FrLoginForm.ConnectedUser.ID;
                FZBGsys.NsApplication.FrLoginForm.ConnectedUser = null;
                return returnedID;
            }
            else
            {
                return null;
            }

        }
        public static ApplicationUtilisateur CurrentUser { get; set; }



        internal static void InitializeConstants()
        {
            Tools.CurrentDateTime = Tools.GetServerDateTime();
        }

        public static ParametreAutorisation ParametreAutorisation { get; set; }
    }


    public class TextBoxx : TextBox
    {
        public TextBoxx()
        {

            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

        }

    }
    #endregion



}
