﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Net.Mail;
using System.Net;

using FZBGsys.NsReports;


namespace FZBGsys
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Tools.InitializeConstants();

            string[] args = Environment.GetCommandLineArgs();

            #region send Mail if args
            if (args.Length != 0 & args.Contains("mail"))
            {
                using (ModelEntities db = new ModelEntities())
                {
                    var usr = db.ApplicationUtilisateurs.SingleOrDefault(u => u.ID == 1);

                }


                Tools.CurrentUserID = 1;
                //  CurrentUser = 1;
                SendRecapsEmail(args);

                Application.Exit();

                return;
            }

            if (args.Length != 0 & args.Contains("backup"))
            {
                Tools.BackupDataBase();
                Application.Exit();

                return;

            }

            #endregion



            Application.Run(new FrMain());
        }



        private static void SendRecapsEmail(string[] args)
        {
            #region mail

            //  MessageBox.Show("Email");
            //  return;
            var typeperiode = "";

            #region Prepare Dates
            var dateFrom = DateTime.Now.Date;
            var dateTo = DateTime.Now.Date;
            var filename = dateTo.ToString("dd_MM_yyyy");

            if (args.Contains("day"))
            {
                typeperiode = "day";
                dateFrom = DateTime.Now.Date;
                dateTo = DateTime.Now.Date;
                filename = dateTo.ToString("dd_MM_yyyy");
            }

            /*  if (args.Contains("day-")) // contains error
              {                
                  typeperiode = args.Single(p=>p.Contains("day-"));
                  var ret = typeperiode.Replace("day-", "").ParseToInt().Value;
                  dateFrom = dateFrom.AddDays(ret).Date;
                  dateTo = dateTo.AddDays(ret).Date;
                  filename = dateTo.ToString("dd_MM_yyyy");
              }
              */
            if (args.Contains("day-1"))
            {
                typeperiode = "day";
                dateFrom = dateFrom.AddDays(-1).Date;
                dateTo = dateTo.AddDays(-1).Date;
                filename = dateTo.ToString("dd_MM_yyyy");
            }

            if (args.Contains("day-2"))
            {
                typeperiode = "day-2";
                dateFrom = dateFrom.AddDays(-2).Date;
                dateTo = dateTo.AddDays(-2).Date;
                filename = dateTo.ToString("dd_MM_yyyy");
            }



            if (args.Contains("week"))
            {
                typeperiode = "week";
                var now = DateTime.Now;
                while (now.DayOfWeek != DayOfWeek.Wednesday)
                {
                    now = now.AddDays(-1);
                }

                dateFrom = now.AddDays(-5).Date;
                dateTo = now.Date;
                filename = dateFrom.ToString("dd_MM_yyyy") + "_au_" + dateTo.ToString("dd_MM_yyyy");
            }


            if (args.Contains("ten"))
            {
                typeperiode = "ten";
                var now = DateTime.Now;

                dateFrom = now.AddDays(-10).Date;
                dateTo = now.Date;
                filename = dateFrom.ToString("dd_MM_yyyy") + "_au_" + dateTo.ToString("dd_MM_yyyy");
            }


            if (args.Contains("month-1"))
            {
                typeperiode = "month";
                var now = DateTime.Now;
                var month = now.AddMonths(-1);
                var days = DateTime.DaysInMonth(month.Year, month.Month);

                dateFrom = new DateTime(month.Year, month.Month, 1);
                dateTo = new DateTime(month.Year, month.Month, days);
                filename = dateFrom.ToString("MMMM_yyyy");
            }


            if (args.Contains("month"))
            {
                typeperiode = "month";
                var now = DateTime.Now;
                var month = now;//.AddMonths(1);
                var days = DateTime.DaysInMonth(month.Year, month.Month);

                dateFrom = new DateTime(month.Year, month.Month, 1);
                dateTo = new DateTime(month.Year, month.Month, days);
                filename = dateFrom.ToString("MMMM_yyyy");
            }


            #endregion


            // FZBGsys.NsReports.FrViewer
            var listFiles = new List<string>();
            ReportController.ExportPDF = true;
            // true;
            //ReportController.sufix = dateFrom.ToString("dd_MM_yyyy") + " " + dateTo.ToString("dd_MM_yyyy");

            using (var db = new ModelEntities())
            {
                var yesterday = dateFrom.AddDays(-1);

                var date = Tools.GetServerDateTime();
                date = new DateTime(date.Year, date.Month-1, DateTime.DaysInMonth(date.Year, date.Month-1));
                var stockhistory = db.StockMatiereHistories.FirstOrDefault(p => p.DateHistory == date.Date);
                if (stockhistory == null)
                {
                    Tools.GenerateStockHistory(date);
                }


                foreach (var item in args)
                {

                    #region details vente

                    if (item == "vente")
                    {
                        //var existing = db.Ventes.Where(p => p.BonAttribution.Date == dateFrom).Count();
                        #region annulerAttributionNonChargé
                        var attribs = db.BonAttributions.Where(p => p.Date == dateFrom);
                        foreach (var bon in attribs)
                        {
                            if (bon.Factures.Count() == 0)
                            {
                                //bon.Etat = (int)EnEtatBonAttribution.Annulé;
                            }
                        }

                        #endregion



                        ReportController.ExportFileName = "détails_ventes_" + filename;
                        FZBGsys.NsReports.Comercial.ComercialReportController.PrintVenteJournee(dateFrom);
                        if (ReportController.ExportFileName != null)
                        {
                            listFiles.Add(ReportController.ExportFileName);
                        }
                        else
                        {
                            //  MessageBox.Show("ErrorExporting " + ReportController.ExportFileName);


                        }

                    }
                    #endregion

                    #region vente

                    if (item == "vente")
                    {
                        //var existing = db.Ventes.Where(p => p.BonAttribution.Date == dateFrom).Count();

                        ReportController.ExportFileName = "activite_comerciale_" + filename;
                        FZBGsys.NsReports.Comercial.ComercialReportController.PrintActiviteClientJournee(dateFrom);
                        if (ReportController.ExportFileName != null)
                        {
                            listFiles.Add(ReportController.ExportFileName);
                        }
                        else
                        {
                            //  MessageBox.Show("ErrorExporting " + ReportController.ExportFileName);


                        }

                    }
                    #endregion
                    //-------------------------------------------------------------------------
                    #region production
                    if (item == "production")
                    {
                        //    var demain = dateFrom.AddDays(1);
                        var productions = db.Productions.Where(p => p.HeureDemarrage.Value > yesterday && p.HeureDemarrage < dateFrom);

                        if (productions.Count() != 0)
                        {
                            foreach (var production in productions)
                            {
                                string fileName = null;
                                if (production.SectionID == (int)EnSection.Unité_PET)
                                {
                                    fileName = FZBGsys.NsProduction.FrProductionPET.GenerateProductionPDFRepport(null, production);  // "production_" + filename + "_" + production.Equipe.Nom.Replace(" ", "_");
                                }
                                else if (production.SectionID == (int)EnSection.Unité_Canette)
                                {
                                    fileName = FZBGsys.NsProduction.FrProductionCAN.GenerateProductionPDFRepport(null, production);  // "production_" + filename + "_" + production.Equipe.Nom.Replace(" ", "_");
                                }
                                else if (production.SectionID == (int)EnSection.Unité_Verre)
                                {
                                    fileName = FZBGsys.NsProduction.FrProductionVER.GenerateProductionPDFRepport(null, production);  // "production_" + filename + "_" + production.Equipe.Nom.Replace(" ", "_");

                                }

                                if (fileName != null)
                                {
                                    listFiles.Add(fileName);
                                }
                                else
                                {

                                    //   MessageBox.Show("ErrorExporting " + ReportController.ExportFileName);


                                }
                            }


                        }


                    }
                    #endregion
                    //
                    #region arrivageMT

                    if (item == "arrivage")
                    {
               ReportController.ExportFileName = "arrivage_matiere_" + filename;


                        Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before
                        var ListFound = db.Arrivages.Where(p => p.DateArrivage == dateFrom);
                        foreach (var arrivage in ListFound)
                        {
                            db.AddToPrintTemps(new PrintTemp()
                            {
                                Date = arrivage.DateArrivage,
                                Description = arrivage.Ressource.ToDescription(),
                                Nom = arrivage.Ressource.Fournisseur.Nom,
                                Piece = arrivage.Piece,
                                Quantite = arrivage.Quantite,
                                UID = Tools.CurrentUserID,
                                val1 = arrivage.Ressource.TypeRessource.UniteDeMesure,
                                val2 = arrivage.Ressource.TypeRessource.UniteDePiece

                            });
                        }

                        db.SaveChanges();


                        FZBGsys.NsReports.Stock.StockReportController.PrintArrivages(dateFrom.ToShortDateString());

                        if (ReportController.ExportFileName != null)
                        {
                            listFiles.Add(ReportController.ExportFileName);
                        }
                        else
                        {
                            //  MessageBox.Show("ErrorExporting " + ReportController.ExportFileName);


                        }

                    }
                    #endregion
                    //-----------------
                    #region Caisse

                    if (item == "caisse")
                    {

                        ReportController.ExportFileName = "Mouvements_Caisse_" + filename;


                        var AnSoldeDate = yesterday;
                        if (yesterday.Date.DayOfWeek == DayOfWeek.Saturday)
                        {
                            AnSoldeDate = yesterday.AddDays(-2);
                        }
                        var hist = db.CaisseHistories.SingleOrDefault(p => p.Date == AnSoldeDate);
                        decimal? AnSolde = null;
                        if (hist != null)
                        {
                            AnSolde = hist.Solde.Value;

                        }
                        else
                        {
                            break;
                        }

                        FZBGsys.NsReports.Caisse.CaisseReportController.PrintCaisseJournee(dateFrom, AnSolde, yesterday.ToShortDateString()); //an
                        if (ReportController.ExportFileName != null)
                        {
                            listFiles.Add(ReportController.ExportFileName);
                        }
                        else
                        {
                            //  MessageBox.Show("ErrorExporting " + ReportController.ExportFileName);


                        }

                    }
                    #endregion

                    #region Situation Comercial

                    if (item == "soldes")
                    {

                        FZBGsys.NsAdministration.FrMaintenance.RegulBonAttribution(db, dateTo); //annul all non en instance

                        ReportController.ExportFileName = "situation_comerciale_" + filename;
                        FZBGsys.NsReports.Comercial.ComercialReportController.PrepareSituationComerciale(dateFrom);
                        if (ReportController.ExportFileName != null)
                        {
                            listFiles.Add(ReportController.ExportFileName);

                        }
                        else
                        {


                        }

                    }
                    #endregion

                }
            }


            ReportController.ExportPDF = false;


            sendmail(listFiles, args);

            #endregion
        }

        private static void sendmail(List<string> fileNames, string[] args)
        {


            if (fileNames.Count == 0)
            {
                return;
            }
            string subjectNC = "";

            if (args.Contains("day"))
            {
                subjectNC = "Situations journalières automatiques " + DateTime.Now.ToShortDateString();
            }

            if (args.Contains("day-1"))
            {
                subjectNC = "Situations journalières automatiques " + DateTime.Now.AddDays(-1).ToShortDateString();
            }

            if (args.Contains("day-2"))
            {
                subjectNC = "Situations journalières automatiques " + DateTime.Now.AddDays(-2).ToShortDateString();
            }

            if (args.Contains("week"))
            {
                subjectNC = "Situations Hébdomadaires automatiques ";
            }

            if (args.Contains("month"))
            {
                subjectNC = "Situations Mensuelles automatiques ";
            }

            if (args.Contains("month-1"))
            {
                subjectNC = "Situations Mensuelles automatiques ";
            }

            if (args.Contains("ten") && args.Contains("arrivage"))
            {
                subjectNC = "Production et suivi des Blocs (10 jours) ";
            }

            if (args.Contains("suiviArrivageD"))
            {
                subjectNC = "Résumé du suivi des Blocs --D-- ";
            }

            if (args.Contains("inventaireSW") || args.Contains("inventaireMGZ") || args.Contains("inventaireBloc"))
            {
                subjectNC = "Etats d'inventaires au " + DateTime.Now.ToShortDateString();
            }
            var fromAddress = new MailAddress("fzboissonsgazeuses@gmail.com", "F.Z. Boissons Gazeuses (système)");


            const string fromPassword = "fzbg190584";
            string subject = subjectNC;
            const string body = "Envoyé automatiquement par le serveur FZBG";

            var smtp = new SmtpClient
            {

                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)

            };


            using (var message = new MailMessage()
            {
                Subject = subject,
                Body = body,
                From = fromAddress

            })
            {

                //message.Bcc.Add(new MailAddress("sidi.elkebir1247@gmail.com"));
                message.Bcc.Add(new MailAddress("aminarull@hotmail.com"));
                //    message.To.Add(new MailAddress("zaahafmohamed@hotmail.com"));
                //message.To.Add(new MailAddress("m.zaahaf@newmarbrecontinental.com"));
                //message.To.Add(new MailAddress("n.zaahaf@newmarbrecontinental.com"));
                //  message.To.Add(new MailAddress("noureddinezahaf@yahoo.fr"));
                //message.To.Add(new MailAddress("slimane_boudi@yahoo.fr"));
                //  message.To.Add(new MailAddress("djamelbenfissa@yahoo.fr"));
                // message.To.Add(new MailAddress("djamelbennefissa@yahoo.fr"));

                //  message.Bcc.Add(new MailAddress("sab.rebai@hotmail.fr"));



                foreach (var att in fileNames)
                {
                    message.Attachments.Add(new Attachment(att));
                }
                try
                {
                    smtp.Send(message);
                }
                catch (Exception ex)
                {

                    //   throw;
                }
            }
        }




    }
}
