﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys
{
    public partial class FrUpdate : Form
    {
        public FrUpdate(System.Deployment.Application.ApplicationDeployment updater)
        {
            this.updater = updater;
            InitializeComponent();
            backgroundWorker1.RunWorkerAsync();
         //--   updater.UpdateCompleted  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            backgroundWorker1.CancelAsync();
            updater.UpdateAsyncCancel();
            IsCanceled = true;
            Dispose();
        }

        public System.Deployment.Application.ApplicationDeployment updater { get; set; }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            updater.Update();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsCanceled = false;
            Dispose();
        }

        public static bool IsCanceled { get; set; }
    }
}
