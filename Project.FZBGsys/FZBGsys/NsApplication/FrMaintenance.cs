﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsAdministration
{
    public enum EnMaintenanceTask
    {
        Update_Comptes = 0,
        Update_Stock_Produit_Fini = 1,
        Regul_Factures = 2,
        Import_Production = 3,
        Regul_Prix_Vente_Null = 4,
        Regul_Destination_Transfer = 5,
        Genreate_Soldes = 6,
        Redifine_Solde_History = 7,
        Regul_BonAttribution = 8,
    }
    public partial class FrMaintenance : Form
    {
        private static string MailFromAdresse = "";
        private static string MailFromPassword = "";
        private static string MailFromSMTP = "";

        ModelEntities db = new ModelEntities();
        public FrMaintenance()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var selectedTaskName = lB.SelectedItem.ToString();
            var selectedTask = (EnMaintenanceTask)lB.SelectedIndex;
            if (this.ConfirmWarning("Confirm Execution " + selectedTaskName + " ?"))
            {


                using (var db = new ModelEntities())
                {
                    RunTask(selectedTask, db);
                }


            }

        }

        public static void RunTask(EnMaintenanceTask Task, ModelEntities db)
        {
            //----------------------------- here add tasks
            #region  Update Stock PF
            if (Task == EnMaintenanceTask.Update_Stock_Produit_Fini)
            {
                var ventesDetails = db.VenteDetails.Where(p => p.isTraite == null).ToList();

                foreach (var detail in ventesDetails)
                {
                    if (detail.Vente.Unite == (int)EnUniteProduit.Palettes)
                    {
                        var resteZero = ProcessPaletteInStockPET(detail, db);
                        if (!resteZero)
                        {
                            resteZero = ProcessPaletteInStock(detail, db);
                            if (!resteZero)
                            {
                                db.AddToAutoAssingLogs(new AutoAssingLog()
                                {
                                    Log = "Vente non exist in stock",
                                    VenteDetailID = detail.ID,
                                });
                            }
                            else
                            {
                                detail.isTraite = true;
                            }
                        }
                        else
                        {
                            detail.isTraite = true;
                        }
                    }
                    else if (detail.Vente.Unite == (int)EnUniteProduit.Fardeaux)
                    {
                        var resteZero = ProcessFardeauxInStockPET(detail, db);
                        if (!resteZero)
                        {
                            resteZero = ProcessFardeauxInStock(detail, db);
                            if (!resteZero)
                            {
                                db.AddToAutoAssingLogs(new AutoAssingLog()
                                {
                                    Log = "Vente non exist in stock",
                                    VenteDetailID = detail.ID,
                                });
                            }
                            else
                            {
                                detail.isTraite = true;
                            }
                        }
                        else
                        {
                            detail.isTraite = true;

                        }

                    }
                }



            }
            #endregion
            //----------------------------- Update Comptes
            #region Update Comptes status

            if (Task == EnMaintenanceTask.Update_Comptes)
            {
                var dateNow = DateTime.Now; // you are in server !!
                var Clients = db.Clients.Where(p => p.Solde < 0 && p.IsCompteBloque != true);
                foreach (var client in Clients)
                {
                    if ((decimal)Tools.GetMinutesBetween2Times(dateNow, client.DatePassageDebiteur.Value) / (decimal)(60 * 24) > (decimal)client.MaxCreditDeletJours)
                    {
                        client.IsCompteBloque = true;
                    }
                }
            }
            #endregion
            //----------------------------- Regul factures
            #region Regul Factures
            if (Task == EnMaintenanceTask.Regul_Factures)
            {
                var factures = db.Factures.ToList();
                foreach (var facture in factures)
                {

                    decimal remise = 1.5m;
                    decimal totalRemise = 0;
                    decimal timbre = 0;
                    decimal ht = 0;
                    decimal ttc = 0;
                    decimal tva = 0;
                    decimal totalFact = 0;
                    //  Montant = 0;
                    var BonAttribution = facture.BonAttribution;

                    // Montant = 0;
                    foreach (var vente in BonAttribution.Ventes)
                    {
                        var prix = vente.GoutProduit.PrixProduits.Single(p => p.FormatID == vente.FormatID &&
                            p.TypeClient == BonAttribution.Client.ClientTypeID);

                        if (vente.isRemise == true && BonAttribution.Client.ClientTypeID == 1 && BonAttribution.ClientID != 3163)
                        {
                            vente.isRemise = true;
                            vente.PrixUnitaire = prix.PrixUnitaire - remise;
                            vente.PrixUnitaireHT = vente.PrixUnitaire * 100 / 117;
                            totalRemise += remise * vente.TotalBouteilles.Value;
                        }
                        else
                        {
                            vente.isRemise = false;
                            vente.PrixUnitaire = prix.PrixUnitaire; // ici ca modify prix
                        }
                        ttc += vente.PrixUnitaire.Value * vente.TotalBouteilles.Value;
                    }

                    ht = ttc * 100 / 117;
                    //  var remiseHT = totalRemise * 100 / 117;
                    // ht = ht - remiseHT;
                    tva = ht * 17 / 100;
                    //  ttc = tva + ht;

                    totalFact = ttc;
                    if (facture.ModePayement == (int)EnModePayement.Especes)
                    {
                        timbre = ttc / 100;
                        if (timbre > 2500)
                        {
                            timbre = 2500;
                        }

                        // txtTimbre.Text = timbre.ToString();
                        // timbre = txtTimbre.Text.ParseToDec().Value;
                        totalFact += timbre;
                    }


                    facture.MontantHT = ht;
                    if (totalRemise != 0) facture.MontantRemiseUnite = 1.5m; else facture.MontantRemiseUnite = 0;
                    facture.MontantTimbre = timbre;
                    facture.MontantTotalFacture = totalFact;
                    facture.MontantTotalRemise = totalRemise;
                    facture.MontantTTC = ttc;
                    facture.MontantTVA = tva;

                }

                //   db.SaveChanges();
            }

            #endregion
            //----------------------------- 
            #region Import Prodcution
            if (Task == EnMaintenanceTask.Import_Production)
            {
                var imported = db.travs.ToList();
                var toDel = db.Productions.Where(p => p.Addons == "imported");
                var toDel2 = db.SessionProductions.Where(p => p.Addons == "imported");
                foreach (var item in toDel)
                {
                    db.DeleteObject(item);
                }

                foreach (var item in toDel2)
                {
                    db.DeleteObject(item);
                }
                db.SaveChanges();
                foreach (var prod in imported)
                {
                    var existProduction = db.Productions.ToList().Where(p => p.HeureDemarrage.Value.Date == prod.Date);
                    Production CurrentProd = null;
                    if (existProduction.Count() == 0)
                    //create production
                    {
                        var newProduction = new Production();
                        newProduction.Addons = "imported";
                        newProduction.BouteileFormatID = prod.FormatID;
                        newProduction.BouteilleNonConform = 0;
                        newProduction.BouteilleSoufflees = prod.préforme_.ParseToInt();
                        newProduction.Cadence = 8000;
                        //newProduction.CartonBouchon = prod.bouchon.ParseToInt();
                        newProduction.EquipeID = prod.EquipeID;
                        newProduction.HeureDemarrage = new DateTime(prod.Date.Value.Year, prod.Date.Value.Month, prod.Date.Value.Day, 8, 0, 0);
                        newProduction.HeureFinDeProd = new DateTime(prod.Date.Value.Year, prod.Date.Value.Month, prod.Date.Value.Day, 16, 0, 0);
                        newProduction.InscriptionUID = 1;
                        newProduction.NombreBouchon = prod.bouchon.ParseToInt();
                        newProduction.NombreEtiquette = (prod.GoutProduitID == 1) ? prod.etiquette_orangina_.ParseToInt() : prod.etiquette_chréa.ParseToInt();
                        newProduction.PoidsFilmThermo = prod.filme_themo.ParseToDec();
                        newProduction.SectionID = 3;
                        //newProduction.TotalBouteilles = prod.TotalBouteilles;
                        db.AddToProductions(newProduction);
                        db.SaveChanges();
                        CurrentProd = newProduction;
                    }
                    else
                    {
                        CurrentProd = existProduction.First();
                    }

                    SessionProduction NewSession = new SessionProduction();
                    NewSession.GoutProduitID = prod.GoutProduitID;
                    NewSession.HeureDemarrage = new DateTime(prod.Date.Value.Year, prod.Date.Value.Month, prod.Date.Value.Day, 8, 0, 0);
                    NewSession.HeureFinProduction = new DateTime(prod.Date.Value.Year, prod.Date.Value.Month, prod.Date.Value.Day, 16, 0, 0);
                    NewSession.NombreBouteillesTotal = prod.TotalBouteilles;
                    CurrentProd.TotalBouteilles += prod.TotalBouteilles;
                    NewSession.NombreBouteillesLotA = prod.TotalBouteilles;
                    NewSession.NombreDeConge = 1;
                    NewSession.NombreBouteillesTotal = prod.TotalBouteilles;
                    NewSession.NombrePalettesTotal = prod.TotalBouteilles / 540;
                    NewSession.Production = CurrentProd;
                    NewSession.TempsNetProduction = 8 * 60;
                    NewSession.TempsTotalTravail = 8 * 60;
                    NewSession.Addons = "imported";

                }


            }
            #endregion
            //---------------
            #region regul prix vente
            if (Task == EnMaintenanceTask.Regul_Prix_Vente_Null)
            {
                var vs = db.Ventes.Where(p => p.PrixUnitaire == null).ToList();
                foreach (var vente in vs)
                {
                    var BonAttribution = vente.BonAttribution;
                    var prix = vente.GoutProduit.PrixProduits.SingleOrDefault(p => p.FormatID == vente.FormatID &&
                        p.TypeClient == BonAttribution.Client.ClientTypeID);

                    if (prix == null)
                    {
                        Tools.ShowError("Prix du produit manquant contacter l'administrateur");
                        return;
                    }

                    if (prix.IsRemisable.Value)
                    {
                        vente.isRemise = true;
                        vente.PrixUnitaire = prix.PrixUnitaire - 1.5m;
                        vente.PrixUnitaireHT = vente.PrixUnitaire * 100 / 117;
                        // totalRemise += 1.5m * vente.TotalBouteilles.Value;
                    }
                    else
                    {
                        vente.isRemise = false;
                        vente.PrixUnitaire = prix.PrixUnitaire; // ici ca modify prix
                    }
                    //ttc += vente.PrixUnitaire.Value * vente.TotalBouteilles.Value;
                }
            }

            #endregion
            //--------------------------------------
            #region gnration solde
            if (Task == EnMaintenanceTask.Genreate_Soldes)
            {
                var Clients = db.Clients.ToList();
                foreach (var client in Clients)
                {
                    GenerateSoldeClient(db, null, false, client);

                }


            }

            if (Task == EnMaintenanceTask.Regul_BonAttribution)
            {
                RegulBonAttribution(db, DateTime.Now); //annul bon

            }
            #endregion
            //--------------------------------------



            #region Regulation Destination Tranfert
            if (Task == EnMaintenanceTask.Regul_Destination_Transfer)
            {

                var Soufflage = db.ProductionSoufflages.ToList();
                foreach (var item in Soufflage)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var Siro = db.ProductionSiroperies.ToList();
                foreach (var item in Siro)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var film = db.ProductionFilmThermoes.ToList();
                foreach (var item in film)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var bar = db.ProductionBarquettes.ToList();
                foreach (var item in bar)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var bouch = db.ProductionBouchons.ToList();
                foreach (var item in bouch)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var bout = db.ProductionBouteilles.ToList();
                foreach (var item in bout)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var can = db.ProductionCannettes.ToList();
                foreach (var item in bouch)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var couv = db.ProductionCouvercles.ToList();
                foreach (var item in bouch)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

                var eti = db.ProductionEtiquettes.ToList();
                foreach (var item in eti)
                {
                    if (item.TransfertMatiere != null)
                    {
                        item.TransfertMatiere.DestinationSectionID = item.Production.SectionID;
                    }
                }

            }
            #endregion

            db.SaveChanges();
            Tools.ShowInformation("Done.");
        }

        public static void RegulBonAttribution(ModelEntities db, DateTime to)
        {


            var clients = db.Clients.ToList();
            foreach (var client in clients)
            {
                var attributions = client.BonAttributions.Where(p => p.Etat == (int)EnEtatBonAttribution.EnInstance && p.Date <= to);
                foreach (var attribution in attributions)
                {
                    attribution.Etat = (int)EnEtatBonAttribution.Annulé;
                    var fact = attribution.Factures.FirstOrDefault();
                    if (fact != null)
                    {
                        fact.Etat = (int)EnEtatFacture.Annulée;
                    }
                }


                //  GenerateSoldeClient(db, to, true, client);
            }
            db.SaveChanges();

        }
        public static void GenerateSoldeClient(ModelEntities db, DateTime? to, bool IncludeAttributionNonLivre, Client client)
        {
            client.XSolde = null;

            if (client.ID == 3228)
            {

            }

            if (to == null)
            {
                to = Tools.CurrentDateTime.Value.Date;
            }

            DateTime from = (true/*method == EnGenerationSoldeMethod.FromFistSoldeEver*/) ? DateTime.Parse("2015-01-01") : client.DateHistory.Value;

            var factures = client.Factures.Where(p => p.Date > from && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée);
            int dd = factures.Count();
            #region FilterFact

            if (IncludeAttributionNonLivre)
            {
                factures = factures.Where(p => p.BonAttribution.Etat != (int)EnEtatBonAttribution.Annulé);
            }
            else
            {
                factures = factures.Where(p => p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré);
            }
            //   dd = factures.Count();

            foreach (var fact in factures)
            {
                if (fact.Etat == null)
                {
                    fact.Etat = (int)EnEtatFacture.Payée;
                }
            }
            #endregion
            //  var cc = factures.Count();

            var retours = client.FactureRetours.Where(p => p.Date > from && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée && p.BonRetour.Etat != (int)EnEtatBonAttribution.Annulé);
            //  int count = retours.Count();
            var operationD = client.Operations.Where(p => p.Date > from && p.Sen == "D" && p.Date <= to &&
                p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client);
            //   count = operationD.Count();
            var operationC = client.Operations.Where(p => p.Date > from && p.Sen == "C" && p.Date <= to &&
                p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client);

            //  count = operationC.Count();

            client.DateHistory = DateTime.Parse("2015-01-01");
            decimal? soldeHistory = null;

            var clientHistory = db.Soldes01012015.SingleOrDefault(p => p.Code == client.CodeSage);

            if (clientHistory == null)
            {
                soldeHistory = 0;
            }
            else
            {
                if (clientHistory.debit != null && clientHistory.debit.ParseToDec() != 0)
                {
                    soldeHistory = -clientHistory.debit.ParseToDec();

                }
                else if (clientHistory.credit != null && clientHistory.credit.ParseToDec() != 0)
                {
                    soldeHistory = clientHistory.credit.ParseToDec();

                }

            }

            if (soldeHistory == null)
            {
                soldeHistory = 0;
            }
            client.SoldeHistory = soldeHistory;
            client.XSolde = client.SoldeHistory +
                    operationC.Sum(p => p.Montant) -
                    operationD.Sum(p => p.Montant) -
                    factures.Sum(p => p.MontantTTC) +
                    retours.Sum(p => p.MontantTTC);



            if (to.Value.Date.CompareTo(Tools.CurrentDateTime.Value.Date) == 0)
            {
                client.Solde = client.XSolde;
                client.DateMajSoldeHistory = Tools.CurrentDateTime.Value.Date;
                client.DateDernierChargement = factures.Count() != 0 ? factures.OrderByDescending(p => p.Date).First().Date : null;
                client.DateDernierMouvement = operationC.Count() != 0 ? operationC.OrderByDescending(p => p.Date).First().Date : null;
            }



        }/*
        public static void GenerateSoldeClient(ModelEntities db, DateTime? to, bool IncludeAttributionNonLivre, Client client)
        {
            client.XSolde = null;

            if (client.ID == 3228)
            {

            }

            if (to == null)
            {
                to = Tools.CurrentDateTime.Value.Date;
            }

            DateTime from = (true/*method == EnGenerationSoldeMethod.FromFistSoldeEver*//*) ? DateTime.Parse("2012-07-23") : client.DateHistory.Value;

            var factures = client.Factures.Where(p => p.Date > from && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée);
            int dd = factures.Count();
            #region FilterFact

            if (IncludeAttributionNonLivre)
            {
                factures = factures.Where(p => p.BonAttribution.Etat != (int)EnEtatBonAttribution.Annulé);
            }
            else
            {
                factures = factures.Where(p => p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré);
            }
            //   dd = factures.Count();

            foreach (var fact in factures)
            {
                if (fact.Etat == null)
                {
                    fact.Etat = (int)EnEtatFacture.Payée;
                }
            }
            #endregion
            //  var cc = factures.Count();

            var retours = client.FactureRetours.Where(p => p.Date > from && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée && p.BonRetour.Etat != (int)EnEtatBonAttribution.Annulé);
            //  int count = retours.Count();
            var operationD = client.Operations.Where(p => p.Date > from && p.Sen == "D" && p.Date <= to &&
                p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client);
            //   count = operationD.Count();
            var operationC = client.Operations.Where(p => p.Date > from && p.Sen == "C" && p.Date <= to &&
                p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client);

            //  count = operationC.Count();

            client.DateHistory = DateTime.Parse("2012-07-23");
            decimal? soldeHistory = null;

            var clientHistory = db.Soldes23072012.SingleOrDefault(p => p.code == client.CodeSage);

            if (clientHistory == null)
            {
                soldeHistory = 0;
            }
            else
            {
                if (clientHistory.debit != null && clientHistory.debit.ParseToDec() != 0)
                {
                    soldeHistory = -clientHistory.debit.Replace(".", ",").ParseToDec();

                }
                else if (clientHistory.credit != null && clientHistory.credit.ParseToDec() != 0)
                {
                    soldeHistory = clientHistory.credit.Replace(".", ",").ParseToDec();

                }

            }

            if (soldeHistory == null)
            {
                soldeHistory = 0;
            }
            client.SoldeHistory = soldeHistory;
            client.XSolde = client.SoldeHistory +
                    operationC.Sum(p => p.Montant) -
                    operationD.Sum(p => p.Montant) -
                    factures.Sum(p => p.MontantTTC) +
                    retours.Sum(p => p.MontantTTC);



            if (to.Value.Date.CompareTo(Tools.CurrentDateTime.Value.Date) == 0)
            {
                client.Solde = client.XSolde;
                client.DateMajSoldeHistory = Tools.CurrentDateTime.Value.Date;
                client.DateDernierChargement = factures.Count() != 0 ? factures.OrderByDescending(p => p.Date).First().Date : null;
                client.DateDernierMouvement = operationC.Count() != 0 ? operationC.OrderByDescending(p => p.Date).First().Date : null;
            }



        }*/

        private static void MailReports(List<string> ListReport)
        {


        }
        private static string ReportVenteDay()
        {

            return "";
        }

        private static bool ProcessFardeauxInStockPET(VenteDetail detail, ModelEntities db)
        {
            Vente vente = detail.Vente;
            var StockPETDispo = db.StockPETFardeaux.Where(p =>
              p.DateProduction == detail.DateProduction &&
              p.SessionProduction.GoutProduitID == vente.GoutProduitID &&
              p.SessionProduction.Production.BouteileFormatID == vente.FormatID &&
              p.Lot == detail.Lot &&
              p.EnStock == true
              ).OrderByDescending(p => p.Nombre).ToList();
            var resteNombre = detail.Nombre;
            foreach (var fardDispo in StockPETDispo)
            {
                if (fardDispo.Nombre == resteNombre)
                {
                    fardDispo.EnStock = false;
                    fardDispo.DateSotie = vente.BonSortie.Date;
                    return true;
                }
                else if (fardDispo.Nombre < resteNombre)
                {
                    fardDispo.EnStock = false;
                    fardDispo.DateSotie = vente.BonSortie.Date;
                    resteNombre -= fardDispo.Nombre;
                }
                else // fardDispo.Nombre > resteNombre 
                {
                    db.StockPETFardeaux.AddObject(new StockPETFardeau()
                    {
                        DateSotie = vente.BonSortie.Date,
                        DateProduction = detail.DateProduction,
                        EnStock = false,
                        Lot = detail.Lot,
                        Nombre = resteNombre,
                        SessionProduction = fardDispo.SessionProduction,
                        TotalBouteilles = resteNombre * fardDispo.SessionProduction.Production.Format.BouteillesParFardeau,
                        TransfertFardeaux = fardDispo.TransfertFardeaux,

                    });

                    return true;

                }
            }

            return false;
        }
        private static bool ProcessFardeauxInStock(VenteDetail detail, ModelEntities db)
        {
            Vente vente = detail.Vente;
            var StockDispo = db.StockFardeaux.Where(p =>
              p.TransfertFardeau.StockPETFardeau.DateProduction == vente.BonSortie.Date &&
              p.GoutProduitID == vente.GoutProduitID &&
              p.FormatID == vente.FormatID &&
              p.Lot == detail.Lot &&
              p.EnStock == true
              ).OrderByDescending(p => p.Nombre).ToList();
            var resteNombre = detail.Nombre;
            foreach (var fardDispo in StockDispo)
            {
                if (fardDispo.Nombre == resteNombre)
                {
                    fardDispo.EnStock = false;
                    fardDispo.DateSortie = vente.BonSortie.Date;
                    return true;
                }
                else if (fardDispo.Nombre < resteNombre)
                {
                    fardDispo.EnStock = false;
                    fardDispo.DateSortie = vente.BonSortie.Date;
                    resteNombre -= fardDispo.Nombre;
                }
                else // fardDispo.Nombre > resteNombre 
                {
                    db.StockFardeaux.AddObject(new StockFardeau()
                    {
                        DateSortie = vente.BonSortie.Date,

                        EnStock = false,
                        Lot = detail.Lot,
                        Nombre = resteNombre,
                        TotalBouteilles = resteNombre * fardDispo.Format.BouteillesParFardeau,
                        DateEntree = fardDispo.DateEntree,
                        Format = fardDispo.Format,
                        GoutProduit = fardDispo.GoutProduit,
                        TransfertFardeau = fardDispo.TransfertFardeau,
                    });

                    return true;

                }
            }

            return false;
        }
        private static bool ProcessPaletteInStockPET(VenteDetail detail, ModelEntities db)
        {
            var vente = detail.Vente;
            var StockPalettesDispo = db.StockPETPalettes.Where(p =>
                p.DateProduction == detail.DateProduction &&
                p.SessionProduction.GoutProduitID == vente.GoutProduitID &&
                p.SessionProduction.Production.BouteileFormatID == vente.FormatID &&
                p.Lot == detail.Lot &&
                 p.EnStock == true
                ).ToList();
            int resteNombre = vente.NombrePalettes.Value;
            foreach (var paleteDispo in StockPalettesDispo)
            {
                string CommonNumerosPalette = FZBGsys.NsStock.FrNumerosPalettes.CommonNumeros(detail.Numeros, paleteDispo.Numeros);
                if (CommonNumerosPalette == null) continue;
                var CommonNombrePalette = CommonNumerosPalette.ToNombrePlalettes();
                if (CommonNumerosPalette.ToOrderedNumeros() == paleteDispo.Numeros.ToOrderedNumeros())
                {
                    paleteDispo.EnStock = false;
                    paleteDispo.DateSortie = vente.BonSortie.Date.Value;
                    resteNombre -= paleteDispo.Nombre.Value;
                }
                else
                {
                    db.StockPETPalettes.AddObject(new StockPETPalette()
                    {
                        DateProduction = detail.DateProduction,
                        DateSortie = detail.BonSortie.Date.Value,
                        EnStock = false,
                        SessionProduction = paleteDispo.SessionProduction,
                        Lot = paleteDispo.Lot,
                    }.SetNumeros(CommonNumerosPalette));
                    paleteDispo.SetNumeros(paleteDispo.Numeros.RemoveNumeros(CommonNumerosPalette));
                    resteNombre -= CommonNombrePalette;
                }
            }

            return resteNombre == 0;
        }
        private static bool ProcessPaletteInStock(VenteDetail detail, ModelEntities db)
        {
            Vente vente = detail.Vente;
            var StockPalettesDispo = db.StockPalettes.Where(p =>
                p.TransfertPalette.StockPETPalette.DateProduction == detail.DateProduction &&
                p.GoutProduitID == vente.GoutProduitID &&
                p.FormatID == vente.FormatID &&
                p.Lot == detail.Lot &&
                 p.EnStock == true
                ).ToList();
            int resteNombre = vente.NombrePalettes.Value;

            foreach (var paleteDispo in StockPalettesDispo)
            {
                string CommonNumerosPalette = FZBGsys.NsStock.FrNumerosPalettes.CommonNumeros(detail.Numeros, paleteDispo.Numeros);
                if (CommonNumerosPalette == null) continue;
                var CommonNombrePalette = CommonNumerosPalette.ToNombrePlalettes();
                if (CommonNumerosPalette.ToOrderedNumeros() == paleteDispo.Numeros.ToOrderedNumeros())
                {
                    paleteDispo.EnStock = false;
                    paleteDispo.DateSortie = vente.BonSortie.Date.Value;
                    resteNombre -= paleteDispo.Nombre.Value;
                }
                else
                {
                    db.StockPalettes.AddObject(new StockPalette()
                    {
                        DateSortie = vente.BonSortie.Date.Value,
                        EnStock = false,
                        Nombre = CommonNombrePalette,
                        Numeros = CommonNumerosPalette,
                        Lot = paleteDispo.Lot,
                        DateEntree = paleteDispo.DateEntree,
                        FormatID = paleteDispo.FormatID,
                        GoutProduitID = paleteDispo.GoutProduitID,
                        TotalBouteilles = CommonNombrePalette * paleteDispo.Format.BouteillesParFardeau * paleteDispo.Format.FardeauParPalette,
                        TransfertPalette = paleteDispo.TransfertPalette,
                        TransfertProduitFiniID = paleteDispo.TransfertProduitFiniID,

                    }.SetNumeros(CommonNumerosPalette));
                    paleteDispo.SetNumeros(paleteDispo.Numeros.RemoveNumeros(CommonNumerosPalette));
                    resteNombre -= CommonNombrePalette;
                }
            }

            return resteNombre == 0;
        }

    }
}
