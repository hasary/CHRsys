﻿namespace FZBGsys.NsAdministration
{
    partial class FrMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lB = new System.Windows.Forms.ListBox();
            this.txtOut = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(587, 347);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 0;
            this.button1.Text = "Fermer";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(16, 347);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(188, 28);
            this.button2.TabIndex = 0;
            this.button2.Text = "Executer";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(216, 347);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(363, 27);
            this.progressBar1.TabIndex = 1;
            // 
            // lB
            // 
            this.lB.FormattingEnabled = true;
            this.lB.ItemHeight = 16;
            this.lB.Items.AddRange(new object[] {
            "Update Stock PF",
            "Update Comptes",
            "Regul Factures",
            "Import Production",
            "Regul Prix Ventes",
            "Regul Destination Transfer",
            "Genreate Soldes (op: livré)",
            "Redifine Solde History",
            "Regul Bon Attribution",
            "Regul Rembourssement Vente"});
            this.lB.Location = new System.Drawing.Point(16, 15);
            this.lB.Margin = new System.Windows.Forms.Padding(4);
            this.lB.Name = "lB";
            this.lB.Size = new System.Drawing.Size(192, 324);
            this.lB.TabIndex = 2;
            // 
            // txtOut
            // 
            this.txtOut.Location = new System.Drawing.Point(216, 17);
            this.txtOut.Margin = new System.Windows.Forms.Padding(4);
            this.txtOut.Multiline = true;
            this.txtOut.Name = "txtOut";
            this.txtOut.ReadOnly = true;
            this.txtOut.Size = new System.Drawing.Size(469, 322);
            this.txtOut.TabIndex = 3;
            // 
            // FrMaintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 390);
            this.Controls.Add(this.txtOut);
            this.Controls.Add(this.lB);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrMaintenance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Maintenance FZBG";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ListBox lB;
        private System.Windows.Forms.TextBox txtOut;
    }
}