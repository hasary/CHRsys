﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsApplication
{
    public partial class FrEditPass : Form
    {
        ModelEntities db = new ModelEntities();
        private ApplicationUtilisateur Model;
        public FrEditPass(ApplicationUtilisateur Model)
        {
            this.Model = Model;
            InitializeComponent();
            InitialiseData();
        }
        private void InitialiseData()
        {


            cbGroupe.Items.Clear();
            // cbGroupe.Items.Add(new ApplicationGroupe() { ID = 0, Nom = "[Choisir un Groupe]" });
            // cbGroupe.Items.AddRange(db.ApplicationGroupes.Where(u => u.ID > 0).ToArray());
            var list = db.ApplicationGroupes.ToList();
            cbGroupe.DataSource = list;
            cbGroupe.DropDownStyle = ComboBoxStyle.DropDownList;
            cbGroupe.ValueMember = "ID";
            cbGroupe.DisplayMember = "Nom";
            
            cbGroupe.SelectedItem = list.SingleOrDefault(l => l.ID == Model.ApplicationGroupe.ID);  // Important



            txtNom.Text = this.Model.Employer.Nom;
            txtUserName.Text = this.Model.UserName;
            chActive.Checked = this.Model.Enabled;
          //  txtPass.Text = Model.Password;
          //  txtPassRe.Text = Model.Password;
         //   txtWinUser.Text = Model.WindowsUserName;


        }
        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (IsValidateInputs())
            {
                var user = db.ApplicationUtilisateurs.Single(u => u.UserName == Model.UserName);
             //   user.GroupeID = ((ApplicationGroupe)cbGroupe.SelectedItem).ID;
                user.Password = FrLoginForm.Encrypt(txtPass.Text);
              //  user.Enabled = chActive.Checked;
               // user.WindowsUserName = txtWinUser.Text.Trim();
                db.SaveChanges();
                Tools.ShowInformation("Votre mot de passe à été changé.");
   
                Dispose(); 
            }
        }
        private void ButtonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private bool IsValidateInputs()
        {
            string ErrorMessage = "";

            #region validations

            // int matricule;
            if (FrLoginForm.Encrypt(txtAncienPass.Text) != Model.Password)
            {
                ErrorMessage = "\nAncien Mot de passe incorrect veuillez taper votre mot de passe actuel ";
            }
            else
            {
                if (txtPass.Text.Length < 4 || txtPass.Text.Length > 20)
                    ErrorMessage += "\nLe Nom mot de passe doit faire entre 4 et 20 caractères ";

                if (txtPass.Text != txtPassRe.Text)
                    ErrorMessage += "\nLes deux mots de passes ne correspondent pas ";

            }
         
            
           
            #endregion


            if (ErrorMessage != "")
            {

                Tools.ShowError(ErrorMessage);

                //     MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        private void AdminUserEdit_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void buttonThis_Click(object sender, EventArgs e)
        {
       //     txtWinUser.Text = Environment.UserName;
        }


    }
}
