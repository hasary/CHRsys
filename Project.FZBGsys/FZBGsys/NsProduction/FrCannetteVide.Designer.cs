﻿namespace FZBGsys.NsProduction
{
    partial class FrCannetteVide
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbProduit = new System.Windows.Forms.ComboBox();
            this.chDateFabriq = new System.Windows.Forms.CheckBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNoBox = new System.Windows.Forms.TextBox();
            this.dtDateFabriq = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtHeure = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNonConforme = new System.Windows.Forms.TextBox();
            this.txtTotalCanette = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(263, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(746, 380);
            this.dgv.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Date:";
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(76, 22);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(166, 22);
            this.dtDate.TabIndex = 9;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(737, 400);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(272, 43);
            this.xpannel1.TabIndex = 11;
            this.xpannel1.Paint += new System.Windows.Forms.PaintEventHandler(this.xpannel1_Paint);
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(4, 4);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(122, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(132, 4);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(263, 396);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(124, 396);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 19;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(76, 59);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(166, 24);
            this.cbFormat.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbProduit);
            this.groupBox1.Controls.Add(this.chDateFabriq);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtNoBox);
            this.groupBox1.Controls.Add(this.dtDateFabriq);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtQuantite);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(16, 134);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 257);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cannette";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 17);
            this.label3.TabIndex = 32;
            this.label3.Text = "Prod.:";
            // 
            // cbProduit
            // 
            this.cbProduit.FormattingEnabled = true;
            this.cbProduit.Location = new System.Drawing.Point(60, 30);
            this.cbProduit.Name = "cbProduit";
            this.cbProduit.Size = new System.Drawing.Size(166, 24);
            this.cbProduit.TabIndex = 32;
            // 
            // chDateFabriq
            // 
            this.chDateFabriq.AutoSize = true;
            this.chDateFabriq.Location = new System.Drawing.Point(6, 72);
            this.chDateFabriq.Name = "chDateFabriq";
            this.chDateFabriq.Size = new System.Drawing.Size(96, 21);
            this.chDateFabriq.TabIndex = 26;
            this.chDateFabriq.Text = "Date Fab.:";
            this.chDateFabriq.UseVisualStyleBackColor = true;
            this.chDateFabriq.CheckedChanged += new System.EventHandler(this.cbDateFabriq_CheckedChanged);
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(21, 127);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(209, 24);
            this.cbFournisseur.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Fournisseur:";
            // 
            // txtNoBox
            // 
            this.txtNoBox.Location = new System.Drawing.Point(108, 171);
            this.txtNoBox.Name = "txtNoBox";
            this.txtNoBox.Size = new System.Drawing.Size(80, 22);
            this.txtNoBox.TabIndex = 4;
            // 
            // dtDateFabriq
            // 
            this.dtDateFabriq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateFabriq.Location = new System.Drawing.Point(108, 72);
            this.dtDateFabriq.Name = "dtDateFabriq";
            this.dtDateFabriq.Size = new System.Drawing.Size(118, 22);
            this.dtDateFabriq.TabIndex = 3;
            this.dtDateFabriq.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(194, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 17);
            this.label8.TabIndex = 25;
            this.label8.Text = "Pcs.";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "N° Palette:";
            // 
            // txtQuantite
            // 
            this.txtQuantite.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantite.Location = new System.Drawing.Point(108, 206);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.Size = new System.Drawing.Size(80, 22);
            this.txtQuantite.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Quantite:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(380, 399);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(136, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Total consomées:";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(86, 103);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "Heure:";
            // 
            // dtHeure
            // 
            this.dtHeure.CustomFormat = "HH:mm";
            this.dtHeure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtHeure.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHeure.Location = new System.Drawing.Point(161, 100);
            this.dtHeure.MaxDate = new System.DateTime(2040, 5, 2, 0, 0, 0, 0);
            this.dtHeure.MinDate = new System.DateTime(2010, 5, 1, 0, 0, 0, 0);
            this.dtHeure.Name = "dtHeure";
            this.dtHeure.ShowUpDown = true;
            this.dtHeure.Size = new System.Drawing.Size(81, 22);
            this.dtHeure.TabIndex = 29;
            this.dtHeure.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(380, 434);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 17);
            this.label10.TabIndex = 30;
            this.label10.Text = "Non-Conforme:";
            // 
            // txtNonConforme
            // 
            this.txtNonConforme.Location = new System.Drawing.Point(563, 431);
            this.txtNonConforme.Name = "txtNonConforme";
            this.txtNonConforme.Size = new System.Drawing.Size(81, 22);
            this.txtNonConforme.TabIndex = 31;
            // 
            // txtTotalCanette
            // 
            this.txtTotalCanette.Location = new System.Drawing.Point(563, 396);
            this.txtTotalCanette.Name = "txtTotalCanette";
            this.txtTotalCanette.ReadOnly = true;
            this.txtTotalCanette.Size = new System.Drawing.Size(147, 22);
            this.txtTotalCanette.TabIndex = 31;
            // 
            // FrCannetteVide
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 487);
            this.Controls.Add(this.txtNonConforme);
            this.Controls.Add(this.txtTotalCanette);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dtHeure);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.dgv);
            this.Name = "FrCannetteVide";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Consomation Cannette Vide";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrSouflage_FormClosing);
            this.Load += new System.EventHandler(this.FrSouflage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNoBox;
        private System.Windows.Forms.DateTimePicker dtDateFabriq;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtHeure;
        private System.Windows.Forms.TextBox txtNonConforme;
        private System.Windows.Forms.TextBox txtTotalCanette;
        private System.Windows.Forms.CheckBox chDateFabriq;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbProduit;
    }
}