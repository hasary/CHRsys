﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrCouvercle : Form
    {
        ModelEntities db;//= new ModelEntities();
        Production Production = null;

        public FrCouvercle()
        {
            db = new ModelEntities();
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionCouvercle>();
            //     dtDate.Value = DateTime.Now;
        }

        public FrCouvercle(Production Production, ModelEntities db = null)
        {
            if (db != null)
            {
                this.db = db;
            }
            else
            {
                this.db = new ModelEntities();
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionCouvercle>();
            //  Production = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            this.Production = Production;
            if (Production != null)
            {
                LoadProduction();
            }
        }

        private void LoadProduction()
        {
            //  txtNonConform.Text = Production.BouteilleNonConform.ToString();
            cbFormat.SelectedItem = Production.Format;
            dtDate.Value = Production.HeureDemarrage.Value;
            var list = Production.ProductionCouvercles.ToList();
            ListToSave.AddRange(list);
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        public FrCouvercle(DateTime date)
        {
            ListToSave = new List<ProductionCouvercle>();
            dtDate.Value = date;
        }

        private void IntitialiseControls()
        {
            //---------------------- Fournisseur
            var TypeRessourceCouvercle = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Couvercle);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourceCouvercle)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;





            dtHeure.Value = new DateTime(2012, 5, 1, 8, 0, 0);
            //dtHeure.MaxDate = new DateTime(2012, 7, 1, 8, 0, 0);
            //dtHeure.MinDate = new DateTime(2012, 7, 1, 8, 0, 0);

            // -------------------------------------



        }

        private void FrCouvercle_Load(object sender, EventArgs e)
        {

        }

        private void FrCouvercle_FormClosing(object sender, FormClosingEventArgs e)
        {
            // db.Dispose();
        }
        public List<ProductionCouvercle> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {

            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
                //       CreateProduction();
            }

            var newCouvercle = new ProductionCouvercle();

            // newCouvercle.Date = dtDate.Value.Date;
            newCouvercle.DateHeure = Tools.GetFillDateFromDateTime(dtDate.Value, dtHeure.Value.TimeOfDay);
            newCouvercle.DateFabrication = dtDateFabriq.Value.Date;
            newCouvercle.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
            newCouvercle.NombreCartons = txtNbrCarton.Text.ParseToInt();
            newCouvercle.NombreCouvercleParCarton = txtNbrCouvercle.Text.ParseToInt();
            newCouvercle.QuantitePiece = newCouvercle.NombreCartons * newCouvercle.NombreCouvercleParCarton;


            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(p => p.FournisseurID == newCouvercle.FournisseurID && p.TypeRessourceID == (int)EnTypeRessource.Couvercle).OrderBy(p => p.ID);
                if (ressources.Count() == 0)
                {
                    this.ShowWarning("type de couvecle n'est pas en Stock");
                    return;
                }
                var ressource = ressources.First();

                var newTransfert = new TransfertMatiere();

                newTransfert.Observation = "consomation " + newCouvercle.DateHeure.Value.ToShortDateString() + " (auto)";


                newTransfert.DestinationSectionID = (int)EnSection.Unité_Canette;
                newTransfert.Date = newCouvercle.DateHeure.Value.Date;

                newTransfert.DateFabrication = newCouvercle.DateFabrication;
                newTransfert.Ressource = ressource;
                newTransfert.Fournisseur = newCouvercle.Fournisseur;
                newTransfert.QunatiteUM = newCouvercle.QuantitePiece;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = newCouvercle.NombreCartons; // 

                //newTransfert.StockMatiere = SelectedStockMT;
                newCouvercle.TransfertMatiere = newTransfert;
                var sucess = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!sucess)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newCouvercle);
                    return;
                }

            }
            #endregion


            ListToSave.Add(newCouvercle);

            Production.ProductionCouvercles.Add(newCouvercle);


            RefreshGrid();
            UpdateControls();
            UpdateTotal();

        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
                // dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            txtNbrCarton.Text = "";
            //   txtNbrCouvercle.Text = "";
        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }


            if (cbFournisseur.SelectedIndex == 0)
            {
                //Message += "Selectionner un Fournisseur";
            }


            int outInt = 0;


            if (ListToSave.Select(p => p.DateHeure).ToList().Contains(dtHeure.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }

            if (!int.TryParse(txtNbrCarton.Text.Trim(), out outInt))
            {
                Message += "\nNombre carton invalide!";

            }
            else if (true)
            {

            }

            if (!int.TryParse(txtNbrCouvercle.Text.Trim(), out outInt))
            {
                Message += "\nNombre Couvercles Invalide!";

            }

            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            labTotal.Text = ListToSave.Sum(p => p.NombreCartons).ToString() + " cartons  " + ListToSave.Sum(p => p.QuantitePiece) + " bouchons";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Heure = p.DateHeure.Value.ToString(@"HH\:mm"),
                //   Couleur = p.CouleurCouvercle,
                Cartons = p.NombreCartons,
                Couvecrles = p.NombreCouvercleParCarton,
                Total = p.QuantitePiece,
                Date_Fabr = p.DateFabrication,
                Fournisseur = p.Fournisseur.Nom,

            }).ToList();

            dgv.HideIDColumn();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("la liste est vide voulez-vous quand-meme enregistrer?"))
                {
                    return;
                }
            }

            //   int nonConform = 0;
            /*  if (!int.TryParse(txtNonConform.Text.Trim(), out nonConform))
              {
                  this.ShowWarning("veillez saisir quantite boutilles non conformes!");
                  return;
              }*/


            if (Production == null)
            {
                //    CreateProduction();
            }
            /*foreach (var bouchons in ListToSave)
            {
                if (bouchons.ID == 0)
                {
                    Production.ProductionCouvercles.Add(bouchons);
                }
            }*/

            UpdateProduction(); //  update total bouteilles in production object
            Dispose();
            /*   try
               {
                   db.SaveChanges();
                   Dispose();
               }
               catch (Exception exp)
               {
                   this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

               }
               */
        }

        private void UpdateProduction()
        {
            Production.CartonCouvercle = ListToSave.Sum(p => p.NombreCartons);
            Production.NombreCouvercle = ListToSave.Sum(p => p.QuantitePiece);
        }

        /* private void CreateProduction()
         {
             Production = new Production()
             {
                 Heur  = dtDate.Value.Date,
                 Format = (Format)cbFormat.SelectedItem,
                 SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
             };

             //   db.AddToProductions(Production);
         }*/

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {


        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            InputCouvercle(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly 

            ListToSave.RemoveAt(selectedIndex.Value);
            var toDel = Production.ProductionCouvercles.ElementAt(selectedIndex.Value);
            Production.ProductionCouvercles.Remove(toDel);

            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }



            db.DeleteObject(toDel);
            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();

        }

        private void InputCouvercle(ProductionCouvercle s)
        {
            dtDate.Value = s.DateHeure.Value;
            dtHeure.Value = s.DateHeure.Value;
            cbFournisseur.SelectedItem = s.Fournisseur;
            //    cbCouleurCouvercle.SelectedItem = s.CouleurCouvercle;
            dtDateFabriq.Value = s.DateFabrication.Value;
            txtNbrCarton.Text = s.NombreCartons.ToString();
            txtNbrCouvercle.Text = s.NombreCouvercleParCarton.ToString();
            //   txtLot.Text = s.NoLot.ToString();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool EditForm { get; set; }

        private void txtNbrBox_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalCarton();
        }

        private void UpdateTotalCarton()
        {
            int? outCart = txtNbrCarton.Text.ParseToInt();
            int? outBouch = txtNbrCouvercle.Text.ParseToInt();

            if (outCart != null && outBouch != null)
            {
                txtQttTotal.Text = (outBouch * outCart).ToString();
            }
            else
            {
                txtQttTotal.Text = "";
            }

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            var key = e.KeyChar;
            if (key != 8 && !char.IsLetter(key))
            {
                e.Handled = true;
            }

            if (char.IsLower(key))
            {
                e.KeyChar = char.ToUpper(key);
            }
        }
    }
}
