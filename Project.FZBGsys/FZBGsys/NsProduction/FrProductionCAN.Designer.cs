﻿namespace FZBGsys.NsProduction
{
    partial class FrProductionCAN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gbProduit = new System.Windows.Forms.GroupBox();
            this.txtPerformance = new System.Windows.Forms.TextBox();
            this.txtEfficience = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtProductionBouteille = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtHeureNetProduction = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTotalArret = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtHeureTotalTravail = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btSiroperie = new System.Windows.Forms.Button();
            this.btFilmThermo = new System.Windows.Forms.Button();
            this.btBarquette = new System.Windows.Forms.Button();
            this.btCouvercle = new System.Windows.Forms.Button();
            this.btCannetteVide = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpSession = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtObservationSession = new System.Windows.Forms.TextBox();
            this.btAjouterSession = new System.Windows.Forms.Button();
            this.btModifierSession = new System.Windows.Forms.Button();
            this.btSupprimerSession = new System.Windows.Forms.Button();
            this.dgvSession = new System.Windows.Forms.DataGridView();
            this.txtFinProd = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDemarage = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tpSuivi = new System.Windows.Forms.TabPage();
            this.txtErrSiroperie = new System.Windows.Forms.TextBox();
            this.txtErrEtiquette = new System.Windows.Forms.TextBox();
            this.txtSiropherie = new System.Windows.Forms.TextBox();
            this.txtFilmThermo = new System.Windows.Forms.TextBox();
            this.txtBarquette = new System.Windows.Forms.TextBox();
            this.txtCouvercle = new System.Windows.Forms.TextBox();
            this.txtCanette = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtObservationSuiviMatiere = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.btImprimer = new System.Windows.Forms.Button();
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.pb = new System.Windows.Forms.ProgressBar();
            this.label17 = new System.Windows.Forms.Label();
            this.panProgress = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCadence = new System.Windows.Forms.TextBox();
            this.cbEquipe = new System.Windows.Forms.ComboBox();
            this.cbOperant = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.gbProduit.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpSession.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSession)).BeginInit();
            this.tpSuivi.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbFormat
            // 
            this.cbFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(104, 127);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(140, 24);
            this.cbFormat.TabIndex = 23;
            this.cbFormat.SelectedIndexChanged += new System.EventHandler(this.cbFormat_SelectedIndexChanged);
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(104, 21);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(140, 22);
            this.dtDate.TabIndex = 22;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 17);
            this.label1.TabIndex = 24;
            this.label1.Text = "Démarrage:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(26, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "Format:";
            // 
            // gbProduit
            // 
            this.gbProduit.Controls.Add(this.txtPerformance);
            this.gbProduit.Controls.Add(this.txtEfficience);
            this.gbProduit.Controls.Add(this.label8);
            this.gbProduit.Controls.Add(this.txtProductionBouteille);
            this.gbProduit.Controls.Add(this.label14);
            this.gbProduit.Controls.Add(this.label13);
            this.gbProduit.Controls.Add(this.txtHeureNetProduction);
            this.gbProduit.Controls.Add(this.label12);
            this.gbProduit.Controls.Add(this.txtTotalArret);
            this.gbProduit.Controls.Add(this.label11);
            this.gbProduit.Controls.Add(this.txtHeureTotalTravail);
            this.gbProduit.Controls.Add(this.label10);
            this.gbProduit.Location = new System.Drawing.Point(16, 188);
            this.gbProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbProduit.Name = "gbProduit";
            this.gbProduit.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbProduit.Size = new System.Drawing.Size(232, 311);
            this.gbProduit.TabIndex = 26;
            this.gbProduit.TabStop = false;
            this.gbProduit.Text = "Rapport de Production";
            // 
            // txtPerformance
            // 
            this.txtPerformance.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPerformance.Location = new System.Drawing.Point(123, 279);
            this.txtPerformance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPerformance.Name = "txtPerformance";
            this.txtPerformance.ReadOnly = true;
            this.txtPerformance.Size = new System.Drawing.Size(91, 22);
            this.txtPerformance.TabIndex = 2;
            // 
            // txtEfficience
            // 
            this.txtEfficience.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEfficience.Location = new System.Drawing.Point(123, 246);
            this.txtEfficience.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtEfficience.Name = "txtEfficience";
            this.txtEfficience.ReadOnly = true;
            this.txtEfficience.Size = new System.Drawing.Size(91, 22);
            this.txtEfficience.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 279);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Performance";
            // 
            // txtProductionBouteille
            // 
            this.txtProductionBouteille.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductionBouteille.Location = new System.Drawing.Point(123, 203);
            this.txtProductionBouteille.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtProductionBouteille.Name = "txtProductionBouteille";
            this.txtProductionBouteille.ReadOnly = true;
            this.txtProductionBouteille.Size = new System.Drawing.Size(91, 22);
            this.txtProductionBouteille.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(37, 249);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "Efficience";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(17, 183);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(162, 17);
            this.label13.TabIndex = 0;
            this.label13.Text = "Production En Bouteilles";
            // 
            // txtHeureNetProduction
            // 
            this.txtHeureNetProduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeureNetProduction.Location = new System.Drawing.Point(123, 148);
            this.txtHeureNetProduction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeureNetProduction.Name = "txtHeureNetProduction";
            this.txtHeureNetProduction.ReadOnly = true;
            this.txtHeureNetProduction.Size = new System.Drawing.Size(91, 22);
            this.txtHeureNetProduction.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(189, 17);
            this.label12.TabIndex = 0;
            this.label12.Text = "Heures nettes de Production";
            // 
            // txtTotalArret
            // 
            this.txtTotalArret.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalArret.Location = new System.Drawing.Point(123, 89);
            this.txtTotalArret.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTotalArret.Name = "txtTotalArret";
            this.txtTotalArret.ReadOnly = true;
            this.txtTotalArret.Size = new System.Drawing.Size(91, 22);
            this.txtTotalArret.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Total Arrets";
            // 
            // txtHeureTotalTravail
            // 
            this.txtHeureTotalTravail.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeureTotalTravail.Location = new System.Drawing.Point(123, 50);
            this.txtHeureTotalTravail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeureTotalTravail.Name = "txtHeureTotalTravail";
            this.txtHeureTotalTravail.ReadOnly = true;
            this.txtHeureTotalTravail.Size = new System.Drawing.Size(91, 22);
            this.txtHeureTotalTravail.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(172, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Heures Totales de Travail";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 224);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Siropherie";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(47, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Film Thermofusible";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Enabled = false;
            this.label5.Location = new System.Drawing.Point(47, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Barquettes";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Couvercle";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Cannette vide";
            // 
            // btSiroperie
            // 
            this.btSiroperie.Location = new System.Drawing.Point(421, 217);
            this.btSiroperie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSiroperie.Name = "btSiroperie";
            this.btSiroperie.Size = new System.Drawing.Size(151, 30);
            this.btSiroperie.TabIndex = 0;
            this.btSiroperie.Text = "Ouvrir la liste...";
            this.btSiroperie.UseVisualStyleBackColor = true;
            this.btSiroperie.Click += new System.EventHandler(this.btSiroperie_Click);
            // 
            // btFilmThermo
            // 
            this.btFilmThermo.Location = new System.Drawing.Point(421, 162);
            this.btFilmThermo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFilmThermo.Name = "btFilmThermo";
            this.btFilmThermo.Size = new System.Drawing.Size(151, 30);
            this.btFilmThermo.TabIndex = 0;
            this.btFilmThermo.Text = "Ouvrir la liste...";
            this.btFilmThermo.UseVisualStyleBackColor = true;
            this.btFilmThermo.Click += new System.EventHandler(this.btFilmThermo_Click);
            // 
            // btBarquette
            // 
            this.btBarquette.Enabled = false;
            this.btBarquette.Location = new System.Drawing.Point(421, 114);
            this.btBarquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btBarquette.Name = "btBarquette";
            this.btBarquette.Size = new System.Drawing.Size(151, 30);
            this.btBarquette.TabIndex = 0;
            this.btBarquette.Text = "Ouvrir la liste...";
            this.btBarquette.UseVisualStyleBackColor = true;
            this.btBarquette.Click += new System.EventHandler(this.btbarquette_Click);
            // 
            // btCouvercle
            // 
            this.btCouvercle.Location = new System.Drawing.Point(421, 66);
            this.btCouvercle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btCouvercle.Name = "btCouvercle";
            this.btCouvercle.Size = new System.Drawing.Size(151, 30);
            this.btCouvercle.TabIndex = 0;
            this.btCouvercle.Text = "Ouvrir la liste...";
            this.btCouvercle.UseVisualStyleBackColor = true;
            this.btCouvercle.Click += new System.EventHandler(this.btCouvercle_Click);
            // 
            // btCannetteVide
            // 
            this.btCannetteVide.Location = new System.Drawing.Point(421, 17);
            this.btCannetteVide.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btCannetteVide.Name = "btCannetteVide";
            this.btCannetteVide.Size = new System.Drawing.Size(151, 30);
            this.btCannetteVide.TabIndex = 0;
            this.btCannetteVide.Text = "Ouvrir la liste...";
            this.btCannetteVide.UseVisualStyleBackColor = true;
            this.btCannetteVide.Click += new System.EventHandler(this.btCannetteVide_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpSession);
            this.tabControl.Controls.Add(this.tpSuivi);
            this.tabControl.Location = new System.Drawing.Point(263, 12);
            this.tabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(804, 441);
            this.tabControl.TabIndex = 28;
            // 
            // tpSession
            // 
            this.tpSession.BackColor = System.Drawing.SystemColors.Control;
            this.tpSession.Controls.Add(this.groupBox3);
            this.tpSession.Controls.Add(this.btAjouterSession);
            this.tpSession.Controls.Add(this.btModifierSession);
            this.tpSession.Controls.Add(this.btSupprimerSession);
            this.tpSession.Controls.Add(this.dgvSession);
            this.tpSession.Controls.Add(this.txtFinProd);
            this.tpSession.Controls.Add(this.label16);
            this.tpSession.Controls.Add(this.txtDemarage);
            this.tpSession.Controls.Add(this.label15);
            this.tpSession.Location = new System.Drawing.Point(4, 25);
            this.tpSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpSession.Name = "tpSession";
            this.tpSession.Size = new System.Drawing.Size(796, 412);
            this.tpSession.TabIndex = 2;
            this.tpSession.Text = "Sessions de Production Canette";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtObservationSession);
            this.groupBox3.Location = new System.Drawing.Point(8, 295);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(776, 108);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Observations";
            // 
            // txtObservationSession
            // 
            this.txtObservationSession.Location = new System.Drawing.Point(3, 21);
            this.txtObservationSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtObservationSession.Multiline = true;
            this.txtObservationSession.Name = "txtObservationSession";
            this.txtObservationSession.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtObservationSession.Size = new System.Drawing.Size(765, 91);
            this.txtObservationSession.TabIndex = 0;
            // 
            // btAjouterSession
            // 
            this.btAjouterSession.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btAjouterSession.Location = new System.Drawing.Point(11, 210);
            this.btAjouterSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouterSession.Name = "btAjouterSession";
            this.btAjouterSession.Size = new System.Drawing.Size(277, 30);
            this.btAjouterSession.TabIndex = 11;
            this.btAjouterSession.Text = "Ajouter une session Canette";
            this.btAjouterSession.UseVisualStyleBackColor = false;
            this.btAjouterSession.Click += new System.EventHandler(this.button10_Click);
            // 
            // btModifierSession
            // 
            this.btModifierSession.Location = new System.Drawing.Point(440, 210);
            this.btModifierSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btModifierSession.Name = "btModifierSession";
            this.btModifierSession.Size = new System.Drawing.Size(169, 30);
            this.btModifierSession.TabIndex = 11;
            this.btModifierSession.Text = "Modifier la session";
            this.btModifierSession.UseVisualStyleBackColor = true;
            this.btModifierSession.Click += new System.EventHandler(this.btModifierSession_Click);
            // 
            // btSupprimerSession
            // 
            this.btSupprimerSession.Location = new System.Drawing.Point(621, 210);
            this.btSupprimerSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSupprimerSession.Name = "btSupprimerSession";
            this.btSupprimerSession.Size = new System.Drawing.Size(164, 30);
            this.btSupprimerSession.TabIndex = 11;
            this.btSupprimerSession.Text = "Supprimer la session";
            this.btSupprimerSession.UseVisualStyleBackColor = true;
            this.btSupprimerSession.Click += new System.EventHandler(this.btSupprimerSession_Click);
            // 
            // dgvSession
            // 
            this.dgvSession.AllowUserToAddRows = false;
            this.dgvSession.AllowUserToDeleteRows = false;
            this.dgvSession.AllowUserToResizeColumns = false;
            this.dgvSession.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSession.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSession.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSession.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvSession.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvSession.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSession.Location = new System.Drawing.Point(8, 7);
            this.dgvSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvSession.MultiSelect = false;
            this.dgvSession.Name = "dgvSession";
            this.dgvSession.ReadOnly = true;
            this.dgvSession.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSession.RowHeadersVisible = false;
            this.dgvSession.RowTemplate.Height = 16;
            this.dgvSession.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSession.Size = new System.Drawing.Size(777, 191);
            this.dgvSession.TabIndex = 10;
            // 
            // txtFinProd
            // 
            this.txtFinProd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFinProd.Location = new System.Drawing.Point(392, 263);
            this.txtFinProd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFinProd.Name = "txtFinProd";
            this.txtFinProd.ReadOnly = true;
            this.txtFinProd.Size = new System.Drawing.Size(91, 22);
            this.txtFinProd.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(249, 266);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 17);
            this.label16.TabIndex = 0;
            this.label16.Text = "Fin de Production";
            // 
            // txtDemarage
            // 
            this.txtDemarage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDemarage.Location = new System.Drawing.Point(115, 263);
            this.txtDemarage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDemarage.Name = "txtDemarage";
            this.txtDemarage.ReadOnly = true;
            this.txtDemarage.Size = new System.Drawing.Size(91, 22);
            this.txtDemarage.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 263);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(79, 17);
            this.label15.TabIndex = 0;
            this.label15.Text = "Demarrage";
            // 
            // tpSuivi
            // 
            this.tpSuivi.BackColor = System.Drawing.SystemColors.Control;
            this.tpSuivi.Controls.Add(this.txtErrSiroperie);
            this.tpSuivi.Controls.Add(this.txtErrEtiquette);
            this.tpSuivi.Controls.Add(this.txtSiropherie);
            this.tpSuivi.Controls.Add(this.txtFilmThermo);
            this.tpSuivi.Controls.Add(this.txtBarquette);
            this.tpSuivi.Controls.Add(this.txtCouvercle);
            this.tpSuivi.Controls.Add(this.txtCanette);
            this.tpSuivi.Controls.Add(this.groupBox4);
            this.tpSuivi.Controls.Add(this.btCannetteVide);
            this.tpSuivi.Controls.Add(this.label7);
            this.tpSuivi.Controls.Add(this.btCouvercle);
            this.tpSuivi.Controls.Add(this.btBarquette);
            this.tpSuivi.Controls.Add(this.label6);
            this.tpSuivi.Controls.Add(this.btFilmThermo);
            this.tpSuivi.Controls.Add(this.btSiroperie);
            this.tpSuivi.Controls.Add(this.label5);
            this.tpSuivi.Controls.Add(this.label3);
            this.tpSuivi.Controls.Add(this.label4);
            this.tpSuivi.Location = new System.Drawing.Point(4, 25);
            this.tpSuivi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpSuivi.Name = "tpSuivi";
            this.tpSuivi.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpSuivi.Size = new System.Drawing.Size(796, 412);
            this.tpSuivi.TabIndex = 0;
            this.tpSuivi.Text = "Suivi Matière Canette";
            // 
            // txtErrSiroperie
            // 
            this.txtErrSiroperie.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtErrSiroperie.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtErrSiroperie.ForeColor = System.Drawing.Color.Red;
            this.txtErrSiroperie.Location = new System.Drawing.Point(579, 217);
            this.txtErrSiroperie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtErrSiroperie.Multiline = true;
            this.txtErrSiroperie.Name = "txtErrSiroperie";
            this.txtErrSiroperie.ReadOnly = true;
            this.txtErrSiroperie.Size = new System.Drawing.Size(199, 48);
            this.txtErrSiroperie.TabIndex = 29;
            // 
            // txtErrEtiquette
            // 
            this.txtErrEtiquette.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtErrEtiquette.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtErrEtiquette.ForeColor = System.Drawing.Color.Red;
            this.txtErrEtiquette.Location = new System.Drawing.Point(579, 114);
            this.txtErrEtiquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtErrEtiquette.Multiline = true;
            this.txtErrEtiquette.Name = "txtErrEtiquette";
            this.txtErrEtiquette.ReadOnly = true;
            this.txtErrEtiquette.Size = new System.Drawing.Size(199, 48);
            this.txtErrEtiquette.TabIndex = 29;
            // 
            // txtSiropherie
            // 
            this.txtSiropherie.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSiropherie.Location = new System.Drawing.Point(183, 222);
            this.txtSiropherie.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtSiropherie.Name = "txtSiropherie";
            this.txtSiropherie.ReadOnly = true;
            this.txtSiropherie.Size = new System.Drawing.Size(199, 22);
            this.txtSiropherie.TabIndex = 29;
            // 
            // txtFilmThermo
            // 
            this.txtFilmThermo.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilmThermo.Location = new System.Drawing.Point(183, 167);
            this.txtFilmThermo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFilmThermo.Name = "txtFilmThermo";
            this.txtFilmThermo.ReadOnly = true;
            this.txtFilmThermo.Size = new System.Drawing.Size(199, 22);
            this.txtFilmThermo.TabIndex = 29;
            // 
            // txtBarquette
            // 
            this.txtBarquette.Enabled = false;
            this.txtBarquette.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBarquette.Location = new System.Drawing.Point(183, 118);
            this.txtBarquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBarquette.Name = "txtBarquette";
            this.txtBarquette.ReadOnly = true;
            this.txtBarquette.Size = new System.Drawing.Size(199, 22);
            this.txtBarquette.TabIndex = 29;
            this.txtBarquette.TextChanged += new System.EventHandler(this.txtBarquette_TextChanged);
            // 
            // txtCouvercle
            // 
            this.txtCouvercle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCouvercle.Location = new System.Drawing.Point(183, 70);
            this.txtCouvercle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCouvercle.Name = "txtCouvercle";
            this.txtCouvercle.ReadOnly = true;
            this.txtCouvercle.Size = new System.Drawing.Size(199, 22);
            this.txtCouvercle.TabIndex = 29;
            // 
            // txtCanette
            // 
            this.txtCanette.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCanette.Location = new System.Drawing.Point(183, 21);
            this.txtCanette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCanette.Name = "txtCanette";
            this.txtCanette.ReadOnly = true;
            this.txtCanette.Size = new System.Drawing.Size(199, 22);
            this.txtCanette.TabIndex = 29;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtObservationSuiviMatiere);
            this.groupBox4.Location = new System.Drawing.Point(16, 270);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(763, 132);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Observation";
            // 
            // txtObservationSuiviMatiere
            // 
            this.txtObservationSuiviMatiere.Location = new System.Drawing.Point(5, 18);
            this.txtObservationSuiviMatiere.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtObservationSuiviMatiere.Multiline = true;
            this.txtObservationSuiviMatiere.Name = "txtObservationSuiviMatiere";
            this.txtObservationSuiviMatiere.Size = new System.Drawing.Size(751, 109);
            this.txtObservationSuiviMatiere.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 29;
            this.label9.Text = "Equipe:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(939, 458);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(124, 34);
            this.btEnregistrer.TabIndex = 31;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btFermer
            // 
            this.btFermer.Location = new System.Drawing.Point(824, 458);
            this.btFermer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(109, 34);
            this.btFermer.TabIndex = 31;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // btImprimer
            // 
            this.btImprimer.Location = new System.Drawing.Point(267, 457);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(189, 34);
            this.btImprimer.TabIndex = 31;
            this.btImprimer.Text = "Imprimer le Rapport";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // bgw
            // 
            this.bgw.WorkerReportsProgress = true;
            this.bgw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_DoWork);
            this.bgw.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgw_ProgressChanged);
            this.bgw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_RunWorkerCompleted);
            // 
            // pb
            // 
            this.pb.Location = new System.Drawing.Point(0, 30);
            this.pb.Margin = new System.Windows.Forms.Padding(4);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(312, 12);
            this.pb.TabIndex = 32;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 6);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(236, 17);
            this.label17.TabIndex = 33;
            this.label17.Text = "Génération du document en cours...";
            // 
            // panProgress
            // 
            this.panProgress.Controls.Add(this.label17);
            this.panProgress.Controls.Add(this.pb);
            this.panProgress.Location = new System.Drawing.Point(481, 455);
            this.panProgress.Margin = new System.Windows.Forms.Padding(4);
            this.panProgress.Name = "panProgress";
            this.panProgress.Size = new System.Drawing.Size(321, 46);
            this.panProgress.TabIndex = 13;
            this.panProgress.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 161);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 17);
            this.label18.TabIndex = 29;
            this.label18.Text = "Cadence:";
            // 
            // txtCadence
            // 
            this.txtCadence.Location = new System.Drawing.Point(104, 158);
            this.txtCadence.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCadence.Name = "txtCadence";
            this.txtCadence.Size = new System.Drawing.Size(140, 22);
            this.txtCadence.TabIndex = 30;
            this.txtCadence.TextChanged += new System.EventHandler(this.txtCadence_TextChanged);
            this.txtCadence.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCadence_KeyPress);
            // 
            // cbEquipe
            // 
            this.cbEquipe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEquipe.FormattingEnabled = true;
            this.cbEquipe.Location = new System.Drawing.Point(104, 57);
            this.cbEquipe.Margin = new System.Windows.Forms.Padding(4);
            this.cbEquipe.Name = "cbEquipe";
            this.cbEquipe.Size = new System.Drawing.Size(140, 24);
            this.cbEquipe.TabIndex = 32;
            this.cbEquipe.SelectedIndexChanged += new System.EventHandler(this.cbEquipe_SelectedIndexChanged);
            // 
            // cbOperant
            // 
            this.cbOperant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOperant.FormattingEnabled = true;
            this.cbOperant.Location = new System.Drawing.Point(104, 90);
            this.cbOperant.Margin = new System.Windows.Forms.Padding(4);
            this.cbOperant.Name = "cbOperant";
            this.cbOperant.Size = new System.Drawing.Size(140, 24);
            this.cbOperant.TabIndex = 34;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(64, 17);
            this.label19.TabIndex = 33;
            this.label19.Text = "Operant:";
            // 
            // FrProductionCAN
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1077, 506);
            this.Controls.Add(this.cbOperant);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.cbEquipe);
            this.Controls.Add(this.panProgress);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.btFermer);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.txtCadence);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.gbProduit);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.dtDate);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrProductionCAN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fiche de Production Unite Canette";
            this.Load += new System.EventHandler(this.FrProductionPET_Load);
            this.gbProduit.ResumeLayout(false);
            this.gbProduit.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tpSession.ResumeLayout(false);
            this.tpSession.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSession)).EndInit();
            this.tpSuivi.ResumeLayout(false);
            this.tpSuivi.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panProgress.ResumeLayout(false);
            this.panProgress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbProduit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btSiroperie;
        private System.Windows.Forms.Button btFilmThermo;
        private System.Windows.Forms.Button btBarquette;
        private System.Windows.Forms.Button btCouvercle;
        private System.Windows.Forms.Button btCannetteVide;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpSuivi;
        private System.Windows.Forms.TabPage tpSession;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btSupprimerSession;
        private System.Windows.Forms.DataGridView dgvSession;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtObservationSession;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Button btAjouterSession;
        private System.Windows.Forms.Button btModifierSession;
        private System.Windows.Forms.TextBox txtSiropherie;
        private System.Windows.Forms.TextBox txtFilmThermo;
        private System.Windows.Forms.TextBox txtBarquette;
        private System.Windows.Forms.TextBox txtCouvercle;
        private System.Windows.Forms.TextBox txtCanette;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtObservationSuiviMatiere;
        private System.Windows.Forms.TextBox txtPerformance;
        private System.Windows.Forms.TextBox txtEfficience;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtProductionBouteille;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtHeureNetProduction;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtTotalArret;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtHeureTotalTravail;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFinProd;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDemarage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.TextBox txtErrEtiquette;
        private System.Windows.Forms.TextBox txtErrSiroperie;
        private System.ComponentModel.BackgroundWorker bgw;
        private System.Windows.Forms.ProgressBar pb;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panProgress;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCadence;
        private System.Windows.Forms.ComboBox cbEquipe;
        private System.Windows.Forms.ComboBox cbOperant;
        private System.Windows.Forms.Label label19;
    }
}