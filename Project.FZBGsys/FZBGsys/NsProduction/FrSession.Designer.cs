﻿namespace FZBGsys.NsProduction
{
    partial class FrSession
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panMaintenance = new System.Windows.Forms.Panel();
            this.btParcourrirMaintenance = new System.Windows.Forms.Button();
            this.txtHeureMaintenance = new System.Windows.Forms.TextBox();
            this.txtMaintenance = new System.Windows.Forms.TextBox();
            this.panCIP = new System.Windows.Forms.Panel();
            this.dtCIPD = new System.Windows.Forms.DateTimePicker();
            this.txtCIP = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtCIPF = new System.Windows.Forms.DateTimePicker();
            this.panRincage = new System.Windows.Forms.Panel();
            this.dtRincageD = new System.Windows.Forms.DateTimePicker();
            this.txtRincage = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dtRinçageF = new System.Windows.Forms.DateTimePicker();
            this.panAutre = new System.Windows.Forms.Panel();
            this.btParcourrirAutre = new System.Windows.Forms.Button();
            this.txtAutre = new System.Windows.Forms.TextBox();
            this.txtHeureAutre = new System.Windows.Forms.TextBox();
            this.txtTotalArret = new System.Windows.Forms.TextBox();
            this.chCIP = new System.Windows.Forms.CheckBox();
            this.chRinçage = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtRinçageFinProductionD = new System.Windows.Forms.DateTimePicker();
            this.dtRinçageFinProductionF = new System.Windows.Forms.DateTimePicker();
            this.txtRinçageFin = new System.Windows.Forms.TextBox();
            this.dtFinDeProd = new System.Windows.Forms.DateTimePicker();
            this.dtDemarrage = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBouteilleA = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFardA = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPaletteA = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbGoutProduit = new System.Windows.Forms.ComboBox();
            this.dtDateDemarrageAll = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonEnregistrer = new System.Windows.Forms.Button();
            this.buttonAnnuler = new System.Windows.Forms.Button();
            this.txtHeureNetProd = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHeureTotalTravail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFormat = new System.Windows.Forms.TextBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpArretes = new System.Windows.Forms.TabPage();
            this.chRinçageFinSession = new System.Windows.Forms.CheckBox();
            this.panRinçageFinSession = new System.Windows.Forms.Panel();
            this.labDateFin = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tpQuantiteProduite = new System.Windows.Forms.TabPage();
            this.panLotF = new System.Windows.Forms.Panel();
            this.txtBouteilleF = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtFardF = new System.Windows.Forms.TextBox();
            this.txtPaletteF = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.panLotE = new System.Windows.Forms.Panel();
            this.txtBouteilleE = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtFardE = new System.Windows.Forms.TextBox();
            this.txtPaletteE = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.chLotF = new System.Windows.Forms.CheckBox();
            this.chLotE = new System.Windows.Forms.CheckBox();
            this.panLotD = new System.Windows.Forms.Panel();
            this.txtBouteilleD = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtFardD = new System.Windows.Forms.TextBox();
            this.txtPaletteD = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.chLotD = new System.Windows.Forms.CheckBox();
            this.panLotC = new System.Windows.Forms.Panel();
            this.txtBouteilleC = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtFardC = new System.Windows.Forms.TextBox();
            this.txtPaletteC = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.chLotC = new System.Windows.Forms.CheckBox();
            this.panLotB = new System.Windows.Forms.Panel();
            this.txtBouteilleB = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFardB = new System.Windows.Forms.TextBox();
            this.txtPaletteB = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chLotB = new System.Windows.Forms.CheckBox();
            this.panLotA = new System.Windows.Forms.Panel();
            this.chLotA = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBouteillesTotal = new System.Windows.Forms.TextBox();
            this.txtFardeauxTotal = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPaletteTotal = new System.Windows.Forms.TextBox();
            this.txtNbrConge = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtThe = new System.Windows.Forms.TextBox();
            this.panInfos1 = new System.Windows.Forms.Panel();
            this.panInfos2 = new System.Windows.Forms.Panel();
            this.panMaintenance.SuspendLayout();
            this.panCIP.SuspendLayout();
            this.panRincage.SuspendLayout();
            this.panAutre.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpArretes.SuspendLayout();
            this.panRinçageFinSession.SuspendLayout();
            this.tpQuantiteProduite.SuspendLayout();
            this.panLotF.SuspendLayout();
            this.panLotE.SuspendLayout();
            this.panLotD.SuspendLayout();
            this.panLotC.SuspendLayout();
            this.panLotB.SuspendLayout();
            this.panLotA.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panInfos1.SuspendLayout();
            this.panInfos2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panMaintenance
            // 
            this.panMaintenance.Controls.Add(this.btParcourrirMaintenance);
            this.panMaintenance.Controls.Add(this.txtHeureMaintenance);
            this.panMaintenance.Controls.Add(this.txtMaintenance);
            this.panMaintenance.Location = new System.Drawing.Point(165, 121);
            this.panMaintenance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panMaintenance.Name = "panMaintenance";
            this.panMaintenance.Size = new System.Drawing.Size(419, 33);
            this.panMaintenance.TabIndex = 35;
            // 
            // btParcourrirMaintenance
            // 
            this.btParcourrirMaintenance.Location = new System.Drawing.Point(5, 2);
            this.btParcourrirMaintenance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btParcourrirMaintenance.Name = "btParcourrirMaintenance";
            this.btParcourrirMaintenance.Size = new System.Drawing.Size(60, 23);
            this.btParcourrirMaintenance.TabIndex = 31;
            this.btParcourrirMaintenance.Text = "...";
            this.btParcourrirMaintenance.UseVisualStyleBackColor = true;
            this.btParcourrirMaintenance.Click += new System.EventHandler(this.btParcourrirMaintenance_Click);
            // 
            // txtHeureMaintenance
            // 
            this.txtHeureMaintenance.Location = new System.Drawing.Point(69, 2);
            this.txtHeureMaintenance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeureMaintenance.Name = "txtHeureMaintenance";
            this.txtHeureMaintenance.ReadOnly = true;
            this.txtHeureMaintenance.Size = new System.Drawing.Size(203, 22);
            this.txtHeureMaintenance.TabIndex = 30;
            this.txtHeureMaintenance.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // txtMaintenance
            // 
            this.txtMaintenance.Location = new System.Drawing.Point(301, 2);
            this.txtMaintenance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMaintenance.Name = "txtMaintenance";
            this.txtMaintenance.ReadOnly = true;
            this.txtMaintenance.Size = new System.Drawing.Size(92, 22);
            this.txtMaintenance.TabIndex = 30;
            this.txtMaintenance.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // panCIP
            // 
            this.panCIP.Controls.Add(this.dtCIPD);
            this.panCIP.Controls.Add(this.txtCIP);
            this.panCIP.Controls.Add(this.label11);
            this.panCIP.Controls.Add(this.dtCIPF);
            this.panCIP.Location = new System.Drawing.Point(231, 82);
            this.panCIP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panCIP.Name = "panCIP";
            this.panCIP.Size = new System.Drawing.Size(353, 33);
            this.panCIP.TabIndex = 35;
            this.panCIP.Visible = false;
            // 
            // dtCIPD
            // 
            this.dtCIPD.CustomFormat = "HH:mm";
            this.dtCIPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtCIPD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCIPD.Location = new System.Drawing.Point(3, 2);
            this.dtCIPD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtCIPD.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtCIPD.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtCIPD.Name = "dtCIPD";
            this.dtCIPD.ShowUpDown = true;
            this.dtCIPD.Size = new System.Drawing.Size(79, 22);
            this.dtCIPD.TabIndex = 33;
            this.dtCIPD.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtCIPD.ValueChanged += new System.EventHandler(this.dtCIPD_ValueChanged);
            // 
            // txtCIP
            // 
            this.txtCIP.Location = new System.Drawing.Point(235, 2);
            this.txtCIP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCIP.Name = "txtCIP";
            this.txtCIP.ReadOnly = true;
            this.txtCIP.Size = new System.Drawing.Size(92, 22);
            this.txtCIP.TabIndex = 30;
            this.txtCIP.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(93, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 17);
            this.label11.TabIndex = 31;
            this.label11.Text = " à";
            // 
            // dtCIPF
            // 
            this.dtCIPF.CustomFormat = "HH:mm";
            this.dtCIPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtCIPF.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtCIPF.Location = new System.Drawing.Point(128, 2);
            this.dtCIPF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtCIPF.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtCIPF.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtCIPF.Name = "dtCIPF";
            this.dtCIPF.ShowUpDown = true;
            this.dtCIPF.Size = new System.Drawing.Size(79, 22);
            this.dtCIPF.TabIndex = 34;
            this.dtCIPF.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtCIPF.ValueChanged += new System.EventHandler(this.dtCIPD_ValueChanged);
            // 
            // panRincage
            // 
            this.panRincage.Controls.Add(this.dtRincageD);
            this.panRincage.Controls.Add(this.txtRincage);
            this.panRincage.Controls.Add(this.label9);
            this.panRincage.Controls.Add(this.dtRinçageF);
            this.panRincage.Location = new System.Drawing.Point(233, 47);
            this.panRincage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panRincage.Name = "panRincage";
            this.panRincage.Size = new System.Drawing.Size(353, 33);
            this.panRincage.TabIndex = 35;
            // 
            // dtRincageD
            // 
            this.dtRincageD.CustomFormat = "HH:mm";
            this.dtRincageD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtRincageD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRincageD.Location = new System.Drawing.Point(3, 2);
            this.dtRincageD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtRincageD.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtRincageD.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtRincageD.Name = "dtRincageD";
            this.dtRincageD.ShowUpDown = true;
            this.dtRincageD.Size = new System.Drawing.Size(79, 22);
            this.dtRincageD.TabIndex = 31;
            this.dtRincageD.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtRincageD.ValueChanged += new System.EventHandler(this.dtRincageD_ValueChanged);
            // 
            // txtRincage
            // 
            this.txtRincage.Location = new System.Drawing.Point(232, 2);
            this.txtRincage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRincage.Name = "txtRincage";
            this.txtRincage.ReadOnly = true;
            this.txtRincage.Size = new System.Drawing.Size(95, 22);
            this.txtRincage.TabIndex = 30;
            this.txtRincage.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(93, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 17);
            this.label9.TabIndex = 31;
            this.label9.Text = " à";
            // 
            // dtRinçageF
            // 
            this.dtRinçageF.CustomFormat = "HH:mm";
            this.dtRinçageF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtRinçageF.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRinçageF.Location = new System.Drawing.Point(128, 2);
            this.dtRinçageF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtRinçageF.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtRinçageF.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtRinçageF.Name = "dtRinçageF";
            this.dtRinçageF.ShowUpDown = true;
            this.dtRinçageF.Size = new System.Drawing.Size(79, 22);
            this.dtRinçageF.TabIndex = 32;
            this.dtRinçageF.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtRinçageF.ValueChanged += new System.EventHandler(this.dtRincageD_ValueChanged);
            // 
            // panAutre
            // 
            this.panAutre.Controls.Add(this.btParcourrirAutre);
            this.panAutre.Controls.Add(this.txtAutre);
            this.panAutre.Controls.Add(this.txtHeureAutre);
            this.panAutre.Location = new System.Drawing.Point(165, 164);
            this.panAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panAutre.Name = "panAutre";
            this.panAutre.Size = new System.Drawing.Size(421, 33);
            this.panAutre.TabIndex = 35;
            // 
            // btParcourrirAutre
            // 
            this.btParcourrirAutre.Location = new System.Drawing.Point(5, 2);
            this.btParcourrirAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btParcourrirAutre.Name = "btParcourrirAutre";
            this.btParcourrirAutre.Size = new System.Drawing.Size(60, 23);
            this.btParcourrirAutre.TabIndex = 31;
            this.btParcourrirAutre.Text = "...";
            this.btParcourrirAutre.UseVisualStyleBackColor = true;
            this.btParcourrirAutre.Click += new System.EventHandler(this.btParcourrirAutre_Click);
            // 
            // txtAutre
            // 
            this.txtAutre.Location = new System.Drawing.Point(300, 2);
            this.txtAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtAutre.Name = "txtAutre";
            this.txtAutre.ReadOnly = true;
            this.txtAutre.Size = new System.Drawing.Size(92, 22);
            this.txtAutre.TabIndex = 30;
            this.txtAutre.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // txtHeureAutre
            // 
            this.txtHeureAutre.Location = new System.Drawing.Point(72, 2);
            this.txtHeureAutre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeureAutre.Name = "txtHeureAutre";
            this.txtHeureAutre.ReadOnly = true;
            this.txtHeureAutre.Size = new System.Drawing.Size(203, 22);
            this.txtHeureAutre.TabIndex = 30;
            this.txtHeureAutre.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // txtTotalArret
            // 
            this.txtTotalArret.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalArret.ForeColor = System.Drawing.Color.Blue;
            this.txtTotalArret.Location = new System.Drawing.Point(469, 260);
            this.txtTotalArret.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTotalArret.Name = "txtTotalArret";
            this.txtTotalArret.ReadOnly = true;
            this.txtTotalArret.Size = new System.Drawing.Size(92, 22);
            this.txtTotalArret.TabIndex = 30;
            // 
            // chCIP
            // 
            this.chCIP.AutoSize = true;
            this.chCIP.Location = new System.Drawing.Point(21, 87);
            this.chCIP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chCIP.Name = "chCIP";
            this.chCIP.Size = new System.Drawing.Size(66, 21);
            this.chCIP.TabIndex = 34;
            this.chCIP.Text = "c. CIP";
            this.chCIP.UseVisualStyleBackColor = true;
            this.chCIP.CheckedChanged += new System.EventHandler(this.chCIP_CheckedChanged);
            // 
            // chRinçage
            // 
            this.chRinçage.AutoSize = true;
            this.chRinçage.Checked = true;
            this.chRinçage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chRinçage.Location = new System.Drawing.Point(21, 52);
            this.chRinçage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chRinçage.Name = "chRinçage";
            this.chRinçage.Size = new System.Drawing.Size(98, 21);
            this.chRinçage.TabIndex = 34;
            this.chRinçage.Text = "b. Rinçage";
            this.chRinçage.UseVisualStyleBackColor = true;
            this.chRinçage.CheckStateChanged += new System.EventHandler(this.chRincage_CheckStateChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(100, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 17);
            this.label18.TabIndex = 31;
            this.label18.Text = " à";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(359, 263);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total arrêts ";
            // 
            // dtRinçageFinProductionD
            // 
            this.dtRinçageFinProductionD.CustomFormat = "HH:mm";
            this.dtRinçageFinProductionD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtRinçageFinProductionD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRinçageFinProductionD.Location = new System.Drawing.Point(8, 4);
            this.dtRinçageFinProductionD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtRinçageFinProductionD.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtRinçageFinProductionD.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtRinçageFinProductionD.Name = "dtRinçageFinProductionD";
            this.dtRinçageFinProductionD.ShowUpDown = true;
            this.dtRinçageFinProductionD.Size = new System.Drawing.Size(81, 22);
            this.dtRinçageFinProductionD.TabIndex = 37;
            this.dtRinçageFinProductionD.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtRinçageFinProductionD.ValueChanged += new System.EventHandler(this.dtRinçageFinD_ValueChanged);
            // 
            // dtRinçageFinProductionF
            // 
            this.dtRinçageFinProductionF.CustomFormat = "HH:mm";
            this.dtRinçageFinProductionF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtRinçageFinProductionF.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtRinçageFinProductionF.Location = new System.Drawing.Point(136, 4);
            this.dtRinçageFinProductionF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtRinçageFinProductionF.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtRinçageFinProductionF.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtRinçageFinProductionF.Name = "dtRinçageFinProductionF";
            this.dtRinçageFinProductionF.ShowUpDown = true;
            this.dtRinçageFinProductionF.Size = new System.Drawing.Size(79, 22);
            this.dtRinçageFinProductionF.TabIndex = 38;
            this.dtRinçageFinProductionF.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtRinçageFinProductionF.ValueChanged += new System.EventHandler(this.dtRinçageFinD_ValueChanged);
            // 
            // txtRinçageFin
            // 
            this.txtRinçageFin.Location = new System.Drawing.Point(241, 4);
            this.txtRinçageFin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtRinçageFin.Name = "txtRinçageFin";
            this.txtRinçageFin.ReadOnly = true;
            this.txtRinçageFin.Size = new System.Drawing.Size(92, 22);
            this.txtRinçageFin.TabIndex = 30;
            this.txtRinçageFin.TextChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // dtFinDeProd
            // 
            this.dtFinDeProd.CustomFormat = "HH:mm";
            this.dtFinDeProd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinDeProd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtFinDeProd.Location = new System.Drawing.Point(235, 261);
            this.dtFinDeProd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtFinDeProd.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtFinDeProd.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtFinDeProd.Name = "dtFinDeProd";
            this.dtFinDeProd.ShowUpDown = true;
            this.dtFinDeProd.Size = new System.Drawing.Size(81, 22);
            this.dtFinDeProd.TabIndex = 41;
            this.dtFinDeProd.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtFinDeProd.ValueChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // dtDemarrage
            // 
            this.dtDemarrage.CustomFormat = "HH:mm";
            this.dtDemarrage.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDemarrage.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtDemarrage.Location = new System.Drawing.Point(235, 14);
            this.dtDemarrage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDemarrage.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtDemarrage.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtDemarrage.Name = "dtDemarrage";
            this.dtDemarrage.ShowUpDown = true;
            this.dtDemarrage.Size = new System.Drawing.Size(79, 22);
            this.dtDemarrage.TabIndex = 30;
            this.dtDemarrage.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtDemarrage.ValueChanged += new System.EventHandler(this.dtDemarrage_ValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(43, 262);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(115, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "g. Fin de session";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(43, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "a. Démarrage";
            // 
            // txtBouteilleA
            // 
            this.txtBouteilleA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteilleA.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteilleA.Location = new System.Drawing.Point(411, 2);
            this.txtBouteilleA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteilleA.Name = "txtBouteilleA";
            this.txtBouteilleA.ReadOnly = true;
            this.txtBouteilleA.Size = new System.Drawing.Size(91, 22);
            this.txtBouteilleA.TabIndex = 47;
            this.txtBouteilleA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(296, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Total Bouteilles";
            this.label6.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtFardA
            // 
            this.txtFardA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardA.Location = new System.Drawing.Point(224, 1);
            this.txtFardA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardA.Name = "txtFardA";
            this.txtFardA.Size = new System.Drawing.Size(65, 22);
            this.txtFardA.TabIndex = 43;
            this.txtFardA.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtFardA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(144, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Fardeaux:";
            this.label5.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtPaletteA
            // 
            this.txtPaletteA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteA.Location = new System.Drawing.Point(76, 1);
            this.txtPaletteA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteA.Name = "txtPaletteA";
            this.txtPaletteA.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteA.TabIndex = 42;
            this.txtPaletteA.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtPaletteA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Palettes:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(284, 329);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Heures nettes ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(144, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Produit:";
            // 
            // cbGoutProduit
            // 
            this.cbGoutProduit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGoutProduit.FormattingEnabled = true;
            this.cbGoutProduit.Location = new System.Drawing.Point(220, 8);
            this.cbGoutProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbGoutProduit.Name = "cbGoutProduit";
            this.cbGoutProduit.Size = new System.Drawing.Size(161, 24);
            this.cbGoutProduit.TabIndex = 4;
            this.cbGoutProduit.SelectedIndexChanged += new System.EventHandler(this.cbTypeProduit_SelectedIndexChanged);
            // 
            // dtDateDemarrageAll
            // 
            this.dtDateDemarrageAll.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDemarrageAll.Location = new System.Drawing.Point(107, 17);
            this.dtDateDemarrageAll.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateDemarrageAll.Name = "dtDateDemarrageAll";
            this.dtDateDemarrageAll.Size = new System.Drawing.Size(111, 22);
            this.dtDateDemarrageAll.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Démarrage:";
            // 
            // buttonEnregistrer
            // 
            this.buttonEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEnregistrer.Location = new System.Drawing.Point(488, 561);
            this.buttonEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEnregistrer.Name = "buttonEnregistrer";
            this.buttonEnregistrer.Size = new System.Drawing.Size(131, 32);
            this.buttonEnregistrer.TabIndex = 6;
            this.buttonEnregistrer.Text = "Enregistrer";
            this.buttonEnregistrer.UseVisualStyleBackColor = true;
            this.buttonEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // buttonAnnuler
            // 
            this.buttonAnnuler.Location = new System.Drawing.Point(352, 561);
            this.buttonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonAnnuler.Name = "buttonAnnuler";
            this.buttonAnnuler.Size = new System.Drawing.Size(129, 32);
            this.buttonAnnuler.TabIndex = 6;
            this.buttonAnnuler.Text = "Annuler";
            this.buttonAnnuler.UseVisualStyleBackColor = true;
            this.buttonAnnuler.Click += new System.EventHandler(this.buttonAnnuler_Click);
            // 
            // txtHeureNetProd
            // 
            this.txtHeureNetProd.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeureNetProd.ForeColor = System.Drawing.Color.Blue;
            this.txtHeureNetProd.Location = new System.Drawing.Point(468, 347);
            this.txtHeureNetProd.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeureNetProd.Name = "txtHeureNetProd";
            this.txtHeureNetProd.ReadOnly = true;
            this.txtHeureNetProd.Size = new System.Drawing.Size(115, 22);
            this.txtHeureNetProd.TabIndex = 30;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 329);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Heures Totales ";
            // 
            // txtHeureTotalTravail
            // 
            this.txtHeureTotalTravail.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHeureTotalTravail.ForeColor = System.Drawing.Color.Blue;
            this.txtHeureTotalTravail.Location = new System.Drawing.Point(145, 350);
            this.txtHeureTotalTravail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtHeureTotalTravail.Name = "txtHeureTotalTravail";
            this.txtHeureTotalTravail.ReadOnly = true;
            this.txtHeureTotalTravail.Size = new System.Drawing.Size(115, 22);
            this.txtHeureTotalTravail.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(52, 17);
            this.label13.TabIndex = 31;
            this.label13.Text = "Format";
            // 
            // txtFormat
            // 
            this.txtFormat.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFormat.Location = new System.Drawing.Point(67, 9);
            this.txtFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFormat.Name = "txtFormat";
            this.txtFormat.ReadOnly = true;
            this.txtFormat.Size = new System.Drawing.Size(67, 22);
            this.txtFormat.TabIndex = 32;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpArretes);
            this.tabControl.Controls.Add(this.tpQuantiteProduite);
            this.tabControl.Enabled = false;
            this.tabControl.Location = new System.Drawing.Point(19, 118);
            this.tabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(605, 427);
            this.tabControl.TabIndex = 48;
            // 
            // tpArretes
            // 
            this.tpArretes.BackColor = System.Drawing.SystemColors.Control;
            this.tpArretes.Controls.Add(this.chRinçageFinSession);
            this.tpArretes.Controls.Add(this.panRinçageFinSession);
            this.tpArretes.Controls.Add(this.labDateFin);
            this.tpArretes.Controls.Add(this.label12);
            this.tpArretes.Controls.Add(this.label38);
            this.tpArretes.Controls.Add(this.label20);
            this.tpArretes.Controls.Add(this.label10);
            this.tpArretes.Controls.Add(this.label21);
            this.tpArretes.Controls.Add(this.panMaintenance);
            this.tpArretes.Controls.Add(this.dtDemarrage);
            this.tpArretes.Controls.Add(this.dtFinDeProd);
            this.tpArretes.Controls.Add(this.panCIP);
            this.tpArretes.Controls.Add(this.label1);
            this.tpArretes.Controls.Add(this.panRincage);
            this.tpArretes.Controls.Add(this.chRinçage);
            this.tpArretes.Controls.Add(this.chCIP);
            this.tpArretes.Controls.Add(this.txtHeureTotalTravail);
            this.tpArretes.Controls.Add(this.panAutre);
            this.tpArretes.Controls.Add(this.txtTotalArret);
            this.tpArretes.Controls.Add(this.txtHeureNetProd);
            this.tpArretes.Controls.Add(this.label33);
            this.tpArretes.Controls.Add(this.label8);
            this.tpArretes.Controls.Add(this.label34);
            this.tpArretes.Controls.Add(this.label2);
            this.tpArretes.Location = new System.Drawing.Point(4, 25);
            this.tpArretes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpArretes.Name = "tpArretes";
            this.tpArretes.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpArretes.Size = new System.Drawing.Size(597, 398);
            this.tpArretes.TabIndex = 0;
            this.tpArretes.Text = "Temps d\'Arrêts";
            // 
            // chRinçageFinSession
            // 
            this.chRinçageFinSession.AutoSize = true;
            this.chRinçageFinSession.Checked = true;
            this.chRinçageFinSession.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chRinçageFinSession.Location = new System.Drawing.Point(13, 220);
            this.chRinçageFinSession.Margin = new System.Windows.Forms.Padding(4);
            this.chRinçageFinSession.Name = "chRinçageFinSession";
            this.chRinçageFinSession.Size = new System.Drawing.Size(191, 21);
            this.chRinçageFinSession.TabIndex = 51;
            this.chRinçageFinSession.Text = "f. Rinçage Fin de Session";
            this.chRinçageFinSession.UseVisualStyleBackColor = true;
            this.chRinçageFinSession.CheckedChanged += new System.EventHandler(this.chRinçageFinSession_CheckedChanged);
            // 
            // panRinçageFinSession
            // 
            this.panRinçageFinSession.Controls.Add(this.txtRinçageFin);
            this.panRinçageFinSession.Controls.Add(this.dtRinçageFinProductionD);
            this.panRinçageFinSession.Controls.Add(this.dtRinçageFinProductionF);
            this.panRinçageFinSession.Controls.Add(this.label18);
            this.panRinçageFinSession.Location = new System.Drawing.Point(225, 214);
            this.panRinçageFinSession.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panRinçageFinSession.Name = "panRinçageFinSession";
            this.panRinçageFinSession.Size = new System.Drawing.Size(353, 33);
            this.panRinçageFinSession.TabIndex = 49;
            // 
            // labDateFin
            // 
            this.labDateFin.AutoSize = true;
            this.labDateFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDateFin.ForeColor = System.Drawing.Color.Red;
            this.labDateFin.Location = new System.Drawing.Point(233, 287);
            this.labDateFin.Name = "labDateFin";
            this.labDateFin.Size = new System.Drawing.Size(0, 17);
            this.labDateFin.TabIndex = 50;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(465, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 17);
            this.label12.TabIndex = 42;
            this.label12.Text = "Durées";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(43, 169);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 17);
            this.label38.TabIndex = 0;
            this.label38.Text = "e. Autres";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(41, 126);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 17);
            this.label20.TabIndex = 0;
            this.label20.Text = "d. Maintenance";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(61, 350);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 17);
            this.label33.TabIndex = 3;
            this.label33.Text = "de travail";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(353, 350);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(95, 17);
            this.label34.TabIndex = 3;
            this.label34.Text = "de production";
            // 
            // tpQuantiteProduite
            // 
            this.tpQuantiteProduite.BackColor = System.Drawing.SystemColors.Control;
            this.tpQuantiteProduite.Controls.Add(this.panLotF);
            this.tpQuantiteProduite.Controls.Add(this.panLotE);
            this.tpQuantiteProduite.Controls.Add(this.chLotF);
            this.tpQuantiteProduite.Controls.Add(this.chLotE);
            this.tpQuantiteProduite.Controls.Add(this.panLotD);
            this.tpQuantiteProduite.Controls.Add(this.chLotD);
            this.tpQuantiteProduite.Controls.Add(this.panLotC);
            this.tpQuantiteProduite.Controls.Add(this.chLotC);
            this.tpQuantiteProduite.Controls.Add(this.panLotB);
            this.tpQuantiteProduite.Controls.Add(this.chLotB);
            this.tpQuantiteProduite.Controls.Add(this.panLotA);
            this.tpQuantiteProduite.Controls.Add(this.chLotA);
            this.tpQuantiteProduite.Controls.Add(this.groupBox1);
            this.tpQuantiteProduite.Location = new System.Drawing.Point(4, 25);
            this.tpQuantiteProduite.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpQuantiteProduite.Name = "tpQuantiteProduite";
            this.tpQuantiteProduite.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpQuantiteProduite.Size = new System.Drawing.Size(597, 398);
            this.tpQuantiteProduite.TabIndex = 1;
            this.tpQuantiteProduite.Text = "Quantite Produite";
            // 
            // panLotF
            // 
            this.panLotF.Controls.Add(this.txtBouteilleF);
            this.panLotF.Controls.Add(this.label35);
            this.panLotF.Controls.Add(this.label36);
            this.panLotF.Controls.Add(this.txtFardF);
            this.panLotF.Controls.Add(this.txtPaletteF);
            this.panLotF.Controls.Add(this.label37);
            this.panLotF.Location = new System.Drawing.Point(77, 244);
            this.panLotF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLotF.Name = "panLotF";
            this.panLotF.Size = new System.Drawing.Size(516, 30);
            this.panLotF.TabIndex = 50;
            this.panLotF.Visible = false;
            // 
            // txtBouteilleF
            // 
            this.txtBouteilleF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteilleF.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteilleF.Location = new System.Drawing.Point(412, 2);
            this.txtBouteilleF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteilleF.Name = "txtBouteilleF";
            this.txtBouteilleF.ReadOnly = true;
            this.txtBouteilleF.Size = new System.Drawing.Size(91, 22);
            this.txtBouteilleF.TabIndex = 47;
            this.txtBouteilleF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(147, 4);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(71, 17);
            this.label35.TabIndex = 0;
            this.label35.Text = "Fardeaux:";
            this.label35.Click += new System.EventHandler(this.label4_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(297, 4);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(105, 17);
            this.label36.TabIndex = 0;
            this.label36.Text = "Total Bouteilles";
            this.label36.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtFardF
            // 
            this.txtFardF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardF.Location = new System.Drawing.Point(225, 1);
            this.txtFardF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardF.Name = "txtFardF";
            this.txtFardF.Size = new System.Drawing.Size(65, 22);
            this.txtFardF.TabIndex = 43;
            this.txtFardF.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtFardF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtPaletteF
            // 
            this.txtPaletteF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteF.Location = new System.Drawing.Point(76, 1);
            this.txtPaletteF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteF.Name = "txtPaletteF";
            this.txtPaletteF.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteF.TabIndex = 42;
            this.txtPaletteF.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtPaletteF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 4);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(63, 17);
            this.label37.TabIndex = 0;
            this.label37.Text = "Palettes:";
            this.label37.Click += new System.EventHandler(this.label4_Click);
            // 
            // panLotE
            // 
            this.panLotE.Controls.Add(this.txtBouteilleE);
            this.panLotE.Controls.Add(this.label30);
            this.panLotE.Controls.Add(this.label31);
            this.panLotE.Controls.Add(this.txtFardE);
            this.panLotE.Controls.Add(this.txtPaletteE);
            this.panLotE.Controls.Add(this.label32);
            this.panLotE.Location = new System.Drawing.Point(77, 199);
            this.panLotE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLotE.Name = "panLotE";
            this.panLotE.Size = new System.Drawing.Size(516, 30);
            this.panLotE.TabIndex = 50;
            this.panLotE.Visible = false;
            // 
            // txtBouteilleE
            // 
            this.txtBouteilleE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteilleE.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteilleE.Location = new System.Drawing.Point(412, 2);
            this.txtBouteilleE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteilleE.Name = "txtBouteilleE";
            this.txtBouteilleE.ReadOnly = true;
            this.txtBouteilleE.Size = new System.Drawing.Size(91, 22);
            this.txtBouteilleE.TabIndex = 47;
            this.txtBouteilleE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(147, 4);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 17);
            this.label30.TabIndex = 0;
            this.label30.Text = "Fardeaux:";
            this.label30.Click += new System.EventHandler(this.label4_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(297, 4);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(105, 17);
            this.label31.TabIndex = 0;
            this.label31.Text = "Total Bouteilles";
            this.label31.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtFardE
            // 
            this.txtFardE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardE.Location = new System.Drawing.Point(225, 1);
            this.txtFardE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardE.Name = "txtFardE";
            this.txtFardE.Size = new System.Drawing.Size(65, 22);
            this.txtFardE.TabIndex = 43;
            this.txtFardE.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtFardE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtPaletteE
            // 
            this.txtPaletteE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteE.Location = new System.Drawing.Point(76, 1);
            this.txtPaletteE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteE.Name = "txtPaletteE";
            this.txtPaletteE.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteE.TabIndex = 42;
            this.txtPaletteE.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtPaletteE.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 4);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 17);
            this.label32.TabIndex = 0;
            this.label32.Text = "Palettes:";
            this.label32.Click += new System.EventHandler(this.label4_Click);
            // 
            // chLotF
            // 
            this.chLotF.AutoSize = true;
            this.chLotF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLotF.Location = new System.Drawing.Point(5, 249);
            this.chLotF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chLotF.Name = "chLotF";
            this.chLotF.Size = new System.Drawing.Size(72, 21);
            this.chLotF.TabIndex = 49;
            this.chLotF.Text = "Lot F:";
            this.chLotF.UseVisualStyleBackColor = true;
            this.chLotF.CheckedChanged += new System.EventHandler(this.chLotF_CheckedChanged);
            // 
            // chLotE
            // 
            this.chLotE.AutoSize = true;
            this.chLotE.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLotE.Location = new System.Drawing.Point(5, 203);
            this.chLotE.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chLotE.Name = "chLotE";
            this.chLotE.Size = new System.Drawing.Size(73, 21);
            this.chLotE.TabIndex = 49;
            this.chLotE.Text = "Lot E:";
            this.chLotE.UseVisualStyleBackColor = true;
            this.chLotE.CheckedChanged += new System.EventHandler(this.chLotE_CheckedChanged);
            // 
            // panLotD
            // 
            this.panLotD.Controls.Add(this.txtBouteilleD);
            this.panLotD.Controls.Add(this.label27);
            this.panLotD.Controls.Add(this.label28);
            this.panLotD.Controls.Add(this.txtFardD);
            this.panLotD.Controls.Add(this.txtPaletteD);
            this.panLotD.Controls.Add(this.label29);
            this.panLotD.Location = new System.Drawing.Point(77, 156);
            this.panLotD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLotD.Name = "panLotD";
            this.panLotD.Size = new System.Drawing.Size(515, 30);
            this.panLotD.TabIndex = 50;
            this.panLotD.Visible = false;
            // 
            // txtBouteilleD
            // 
            this.txtBouteilleD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteilleD.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteilleD.Location = new System.Drawing.Point(411, 2);
            this.txtBouteilleD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteilleD.Name = "txtBouteilleD";
            this.txtBouteilleD.ReadOnly = true;
            this.txtBouteilleD.Size = new System.Drawing.Size(91, 22);
            this.txtBouteilleD.TabIndex = 47;
            this.txtBouteilleD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(144, 4);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 17);
            this.label27.TabIndex = 0;
            this.label27.Text = "Fardeaux:";
            this.label27.Click += new System.EventHandler(this.label4_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(296, 4);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 17);
            this.label28.TabIndex = 0;
            this.label28.Text = "Total Bouteilles";
            this.label28.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtFardD
            // 
            this.txtFardD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardD.Location = new System.Drawing.Point(224, 1);
            this.txtFardD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardD.Name = "txtFardD";
            this.txtFardD.Size = new System.Drawing.Size(65, 22);
            this.txtFardD.TabIndex = 43;
            this.txtFardD.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtFardD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtPaletteD
            // 
            this.txtPaletteD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteD.Location = new System.Drawing.Point(76, 1);
            this.txtPaletteD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteD.Name = "txtPaletteD";
            this.txtPaletteD.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteD.TabIndex = 42;
            this.txtPaletteD.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtPaletteD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 4);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(63, 17);
            this.label29.TabIndex = 0;
            this.label29.Text = "Palettes:";
            this.label29.Click += new System.EventHandler(this.label4_Click);
            // 
            // chLotD
            // 
            this.chLotD.AutoSize = true;
            this.chLotD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLotD.Location = new System.Drawing.Point(5, 159);
            this.chLotD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chLotD.Name = "chLotD";
            this.chLotD.Size = new System.Drawing.Size(74, 21);
            this.chLotD.TabIndex = 49;
            this.chLotD.Text = "Lot D:";
            this.chLotD.UseVisualStyleBackColor = true;
            this.chLotD.CheckedChanged += new System.EventHandler(this.chLotD_CheckedChanged);
            // 
            // panLotC
            // 
            this.panLotC.Controls.Add(this.txtBouteilleC);
            this.panLotC.Controls.Add(this.label19);
            this.panLotC.Controls.Add(this.label22);
            this.panLotC.Controls.Add(this.txtFardC);
            this.panLotC.Controls.Add(this.txtPaletteC);
            this.panLotC.Controls.Add(this.label26);
            this.panLotC.Location = new System.Drawing.Point(77, 110);
            this.panLotC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLotC.Name = "panLotC";
            this.panLotC.Size = new System.Drawing.Size(516, 30);
            this.panLotC.TabIndex = 50;
            this.panLotC.Visible = false;
            // 
            // txtBouteilleC
            // 
            this.txtBouteilleC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteilleC.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteilleC.Location = new System.Drawing.Point(412, 2);
            this.txtBouteilleC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteilleC.Name = "txtBouteilleC";
            this.txtBouteilleC.ReadOnly = true;
            this.txtBouteilleC.Size = new System.Drawing.Size(91, 22);
            this.txtBouteilleC.TabIndex = 47;
            this.txtBouteilleC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(147, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 17);
            this.label19.TabIndex = 0;
            this.label19.Text = "Fardeaux:";
            this.label19.Click += new System.EventHandler(this.label4_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(297, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 17);
            this.label22.TabIndex = 0;
            this.label22.Text = "Total Bouteilles";
            this.label22.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtFardC
            // 
            this.txtFardC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardC.Location = new System.Drawing.Point(225, 1);
            this.txtFardC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardC.Name = "txtFardC";
            this.txtFardC.Size = new System.Drawing.Size(65, 22);
            this.txtFardC.TabIndex = 43;
            this.txtFardC.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtFardC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtPaletteC
            // 
            this.txtPaletteC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteC.Location = new System.Drawing.Point(76, 1);
            this.txtPaletteC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteC.Name = "txtPaletteC";
            this.txtPaletteC.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteC.TabIndex = 42;
            this.txtPaletteC.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtPaletteC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 17);
            this.label26.TabIndex = 0;
            this.label26.Text = "Palettes:";
            this.label26.Click += new System.EventHandler(this.label4_Click);
            // 
            // chLotC
            // 
            this.chLotC.AutoSize = true;
            this.chLotC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLotC.Location = new System.Drawing.Point(5, 112);
            this.chLotC.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chLotC.Name = "chLotC";
            this.chLotC.Size = new System.Drawing.Size(73, 21);
            this.chLotC.TabIndex = 49;
            this.chLotC.Text = "Lot C:";
            this.chLotC.UseVisualStyleBackColor = true;
            this.chLotC.CheckedChanged += new System.EventHandler(this.chLotC_CheckedChanged);
            // 
            // panLotB
            // 
            this.panLotB.Controls.Add(this.txtBouteilleB);
            this.panLotB.Controls.Add(this.label14);
            this.panLotB.Controls.Add(this.label16);
            this.panLotB.Controls.Add(this.txtFardB);
            this.panLotB.Controls.Add(this.txtPaletteB);
            this.panLotB.Controls.Add(this.label17);
            this.panLotB.Location = new System.Drawing.Point(77, 64);
            this.panLotB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLotB.Name = "panLotB";
            this.panLotB.Size = new System.Drawing.Size(516, 30);
            this.panLotB.TabIndex = 50;
            this.panLotB.Visible = false;
            // 
            // txtBouteilleB
            // 
            this.txtBouteilleB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteilleB.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteilleB.Location = new System.Drawing.Point(413, 2);
            this.txtBouteilleB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteilleB.Name = "txtBouteilleB";
            this.txtBouteilleB.ReadOnly = true;
            this.txtBouteilleB.Size = new System.Drawing.Size(91, 22);
            this.txtBouteilleB.TabIndex = 47;
            this.txtBouteilleB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(148, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "Fardeaux:";
            this.label14.Click += new System.EventHandler(this.label4_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(299, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 17);
            this.label16.TabIndex = 0;
            this.label16.Text = "Total Bouteilles";
            this.label16.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtFardB
            // 
            this.txtFardB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardB.Location = new System.Drawing.Point(227, 1);
            this.txtFardB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardB.Name = "txtFardB";
            this.txtFardB.Size = new System.Drawing.Size(65, 22);
            this.txtFardB.TabIndex = 43;
            this.txtFardB.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtFardB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtPaletteB
            // 
            this.txtPaletteB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteB.Location = new System.Drawing.Point(77, 1);
            this.txtPaletteB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteB.Name = "txtPaletteB";
            this.txtPaletteB.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteB.TabIndex = 42;
            this.txtPaletteB.TextChanged += new System.EventHandler(this.txtPalette_TextChanged);
            this.txtPaletteB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 17);
            this.label17.TabIndex = 0;
            this.label17.Text = "Palettes:";
            this.label17.Click += new System.EventHandler(this.label4_Click);
            // 
            // chLotB
            // 
            this.chLotB.AutoSize = true;
            this.chLotB.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLotB.Location = new System.Drawing.Point(5, 66);
            this.chLotB.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chLotB.Name = "chLotB";
            this.chLotB.Size = new System.Drawing.Size(73, 21);
            this.chLotB.TabIndex = 49;
            this.chLotB.Text = "Lot B:";
            this.chLotB.UseVisualStyleBackColor = true;
            this.chLotB.CheckedChanged += new System.EventHandler(this.chLotB_CheckedChanged);
            // 
            // panLotA
            // 
            this.panLotA.Controls.Add(this.txtBouteilleA);
            this.panLotA.Controls.Add(this.label5);
            this.panLotA.Controls.Add(this.label6);
            this.panLotA.Controls.Add(this.txtFardA);
            this.panLotA.Controls.Add(this.txtPaletteA);
            this.panLotA.Controls.Add(this.label4);
            this.panLotA.Location = new System.Drawing.Point(77, 18);
            this.panLotA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panLotA.Name = "panLotA";
            this.panLotA.Size = new System.Drawing.Size(515, 30);
            this.panLotA.TabIndex = 50;
            this.panLotA.Visible = false;
            // 
            // chLotA
            // 
            this.chLotA.AutoSize = true;
            this.chLotA.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chLotA.Location = new System.Drawing.Point(5, 22);
            this.chLotA.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chLotA.Name = "chLotA";
            this.chLotA.Size = new System.Drawing.Size(73, 21);
            this.chLotA.TabIndex = 49;
            this.chLotA.Text = "Lot A:";
            this.chLotA.UseVisualStyleBackColor = true;
            this.chLotA.CheckedChanged += new System.EventHandler(this.chLotA_CheckStateChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtBouteillesTotal);
            this.groupBox1.Controls.Add(this.txtFardeauxTotal);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.txtPaletteTotal);
            this.groupBox1.Location = new System.Drawing.Point(19, 290);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(561, 94);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Total Production de la session";
            // 
            // txtBouteillesTotal
            // 
            this.txtBouteillesTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBouteillesTotal.ForeColor = System.Drawing.Color.Blue;
            this.txtBouteillesTotal.Location = new System.Drawing.Point(456, 38);
            this.txtBouteillesTotal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBouteillesTotal.Name = "txtBouteillesTotal";
            this.txtBouteillesTotal.ReadOnly = true;
            this.txtBouteillesTotal.Size = new System.Drawing.Size(91, 22);
            this.txtBouteillesTotal.TabIndex = 47;
            this.txtBouteillesTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtFardeauxTotal
            // 
            this.txtFardeauxTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFardeauxTotal.Location = new System.Drawing.Point(239, 39);
            this.txtFardeauxTotal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtFardeauxTotal.Name = "txtFardeauxTotal";
            this.txtFardeauxTotal.ReadOnly = true;
            this.txtFardeauxTotal.Size = new System.Drawing.Size(65, 22);
            this.txtFardeauxTotal.TabIndex = 43;
            this.txtFardeauxTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(157, 42);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 17);
            this.label23.TabIndex = 0;
            this.label23.Text = "Fardeaux:";
            this.label23.Click += new System.EventHandler(this.label4_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(324, 42);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 17);
            this.label24.TabIndex = 0;
            this.label24.Text = "Total Bouteilles";
            this.label24.Click += new System.EventHandler(this.label4_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 42);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 17);
            this.label25.TabIndex = 0;
            this.label25.Text = "Palettes:";
            this.label25.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtPaletteTotal
            // 
            this.txtPaletteTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPaletteTotal.Location = new System.Drawing.Point(75, 39);
            this.txtPaletteTotal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPaletteTotal.Name = "txtPaletteTotal";
            this.txtPaletteTotal.ReadOnly = true;
            this.txtPaletteTotal.Size = new System.Drawing.Size(63, 22);
            this.txtPaletteTotal.TabIndex = 42;
            this.txtPaletteTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalette_KeyPress);
            // 
            // txtNbrConge
            // 
            this.txtNbrConge.Location = new System.Drawing.Point(168, 6);
            this.txtNbrConge.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNbrConge.Name = "txtNbrConge";
            this.txtNbrConge.Size = new System.Drawing.Size(97, 22);
            this.txtNbrConge.TabIndex = 49;
            this.txtNbrConge.TextChanged += new System.EventHandler(this.txtNbrConge_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 17);
            this.label15.TabIndex = 50;
            this.label15.Text = "Nombre de Conges:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(280, 6);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(205, 17);
            this.label39.TabIndex = 52;
            this.label39.Text = "Production Théorique en Littre:";
            // 
            // txtThe
            // 
            this.txtThe.Location = new System.Drawing.Point(504, 2);
            this.txtThe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtThe.Name = "txtThe";
            this.txtThe.ReadOnly = true;
            this.txtThe.Size = new System.Drawing.Size(88, 22);
            this.txtThe.TabIndex = 51;
            // 
            // panInfos1
            // 
            this.panInfos1.Controls.Add(this.cbGoutProduit);
            this.panInfos1.Controls.Add(this.label3);
            this.panInfos1.Controls.Add(this.label13);
            this.panInfos1.Controls.Add(this.txtFormat);
            this.panInfos1.Location = new System.Drawing.Point(229, 10);
            this.panInfos1.Name = "panInfos1";
            this.panInfos1.Size = new System.Drawing.Size(396, 45);
            this.panInfos1.TabIndex = 53;
            // 
            // panInfos2
            // 
            this.panInfos2.Controls.Add(this.label39);
            this.panInfos2.Controls.Add(this.txtNbrConge);
            this.panInfos2.Controls.Add(this.label15);
            this.panInfos2.Controls.Add(this.txtThe);
            this.panInfos2.Location = new System.Drawing.Point(18, 62);
            this.panInfos2.Name = "panInfos2";
            this.panInfos2.Size = new System.Drawing.Size(607, 39);
            this.panInfos2.TabIndex = 54;
            // 
            // FrSession
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 622);
            this.Controls.Add(this.panInfos2);
            this.Controls.Add(this.panInfos1);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.buttonAnnuler);
            this.Controls.Add(this.buttonEnregistrer);
            this.Controls.Add(this.dtDateDemarrageAll);
            this.Controls.Add(this.label7);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrSession";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Session de production";
            this.Load += new System.EventHandler(this.FrSession_Load);
            this.panMaintenance.ResumeLayout(false);
            this.panMaintenance.PerformLayout();
            this.panCIP.ResumeLayout(false);
            this.panCIP.PerformLayout();
            this.panRincage.ResumeLayout(false);
            this.panRincage.PerformLayout();
            this.panAutre.ResumeLayout(false);
            this.panAutre.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tpArretes.ResumeLayout(false);
            this.tpArretes.PerformLayout();
            this.panRinçageFinSession.ResumeLayout(false);
            this.panRinçageFinSession.PerformLayout();
            this.tpQuantiteProduite.ResumeLayout(false);
            this.tpQuantiteProduite.PerformLayout();
            this.panLotF.ResumeLayout(false);
            this.panLotF.PerformLayout();
            this.panLotE.ResumeLayout(false);
            this.panLotE.PerformLayout();
            this.panLotD.ResumeLayout(false);
            this.panLotD.PerformLayout();
            this.panLotC.ResumeLayout(false);
            this.panLotC.PerformLayout();
            this.panLotB.ResumeLayout(false);
            this.panLotB.PerformLayout();
            this.panLotA.ResumeLayout(false);
            this.panLotA.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panInfos1.ResumeLayout(false);
            this.panInfos1.PerformLayout();
            this.panInfos2.ResumeLayout(false);
            this.panInfos2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panAutre;
        private System.Windows.Forms.TextBox txtAutre;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtRinçageFinProductionD;
        private System.Windows.Forms.DateTimePicker dtRinçageFinProductionF;
        private System.Windows.Forms.TextBox txtRinçageFin;
        private System.Windows.Forms.DateTimePicker dtFinDeProd;
        private System.Windows.Forms.DateTimePicker dtDemarrage;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBouteilleA;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFardA;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPaletteA;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbGoutProduit;
        private System.Windows.Forms.DateTimePicker dtDateDemarrageAll;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTotalArret;
        private System.Windows.Forms.Button buttonEnregistrer;
        private System.Windows.Forms.Button buttonAnnuler;
        private System.Windows.Forms.TextBox txtHeureNetProd;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHeureTotalTravail;
        private System.Windows.Forms.Panel panRincage;
        private System.Windows.Forms.DateTimePicker dtRincageD;
        private System.Windows.Forms.TextBox txtRincage;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtRinçageF;
        private System.Windows.Forms.CheckBox chRinçage;
        private System.Windows.Forms.Panel panMaintenance;
        private System.Windows.Forms.TextBox txtMaintenance;
        private System.Windows.Forms.Panel panCIP;
        private System.Windows.Forms.DateTimePicker dtCIPD;
        private System.Windows.Forms.TextBox txtCIP;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtCIPF;
        private System.Windows.Forms.CheckBox chCIP;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtFormat;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpArretes;
        private System.Windows.Forms.TabPage tpQuantiteProduite;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBouteillesTotal;
        private System.Windows.Forms.TextBox txtFardeauxTotal;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPaletteTotal;
        private System.Windows.Forms.Panel panLotE;
        private System.Windows.Forms.TextBox txtBouteilleE;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtFardE;
        private System.Windows.Forms.TextBox txtPaletteE;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chLotE;
        private System.Windows.Forms.Panel panLotD;
        private System.Windows.Forms.TextBox txtBouteilleD;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtFardD;
        private System.Windows.Forms.TextBox txtPaletteD;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.CheckBox chLotD;
        private System.Windows.Forms.Panel panLotC;
        private System.Windows.Forms.TextBox txtBouteilleC;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtFardC;
        private System.Windows.Forms.TextBox txtPaletteC;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.CheckBox chLotC;
        private System.Windows.Forms.Panel panLotB;
        private System.Windows.Forms.TextBox txtBouteilleB;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFardB;
        private System.Windows.Forms.TextBox txtPaletteB;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chLotB;
        private System.Windows.Forms.Panel panLotA;
        private System.Windows.Forms.CheckBox chLotA;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panLotF;
        private System.Windows.Forms.TextBox txtBouteilleF;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox txtFardF;
        private System.Windows.Forms.TextBox txtPaletteF;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox chLotF;
        private System.Windows.Forms.Button btParcourrirMaintenance;
        private System.Windows.Forms.TextBox txtHeureMaintenance;
        private System.Windows.Forms.Button btParcourrirAutre;
        private System.Windows.Forms.TextBox txtHeureAutre;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labDateFin;
        private System.Windows.Forms.Panel panRinçageFinSession;
        private System.Windows.Forms.CheckBox chRinçageFinSession;
        private System.Windows.Forms.TextBox txtNbrConge;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtThe;
        private System.Windows.Forms.Panel panInfos1;
        private System.Windows.Forms.Panel panInfos2;
    }
}