﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrArret : Form
    {
        private EnTypeArret TypeArret;
        ModelEntities db;
        public static bool IsModifiedMaintenance = false;
        public static bool IsModifiedAutre = false;
        public FrArret(ModelEntities db, SessionProduction SessionProduction, List<SessionProductionArret> listAnnonyme, EnTypeArret TypeArret)
        {
            this.db = db;
            this.SessionProduction = SessionProduction;
            if (SessionProduction == null)
            {
                if (listAnnonyme == null)
                {
                    ListArretsAnnonyms = new List<SessionProductionArret>();
                }
                else
                {
                    ListArretsAnnonyms = listAnnonyme;
                }
            }
            InitializeComponent();
            this.TypeArret = TypeArret;
            InitialiseControls();


        }

        private void InitialiseControls()
        {

            cbCauseArret.Items.Add(new CauseArret { ID = 0, Description = "" });
            cbCauseArret.Items.AddRange(db.CauseArrets.Where(p => p.ID > 0 && p.TypeArret == (int)TypeArret).ToArray());
            cbCauseArret.DisplayMember = "Description";
            cbCauseArret.ValueMember = "ID";
            cbCauseArret.SelectedItem = null;
            RefreshGrid();

        }

        private void RefreshGrid()
        {
            if (SessionProduction != null)
            {
                dgv.DataSource = SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)TypeArret).ToList().Select(p => new
                    {
                        Debut = p.HeureDebut.Value.ToString(@"dd/MM/yy  HH\:mm"),
                        Fin = p.HeureFin.Value.ToString(@"dd/MM/yy  HH\:mm"),
                        Cause = p.Cause,
                        Durée = p.DureeMinutes.MinutesToHoursMinutesTxt(),
                    }).ToList();

            }
            else
            {
                dgv.DataSource = ListArretsAnnonyms.Select(p => new
                {
                    Debut = p.HeureDebut.Value.ToString(@"dd/MM/yy  HH\:mm"),
                    Fin = p.HeureFin.Value.ToString(@"dd/MM/yy  HH\:mm"),
                    Cause = p.Cause,
                    Durée = p.DureeMinutes.MinutesToHoursMinutesTxt(),
                }).ToList();

            }

        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            if (selectedIndex != null)
            {
                if (SessionProduction != null)
                {
                    var selected = SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)TypeArret).ElementAt(selectedIndex.Value);
                    SessionProduction.SessionProductionArrets.Remove(selected);
                    db.DeleteObject(selected);
                }
                else
                {
                    var selected = ListArretsAnnonyms.ElementAt(selectedIndex.Value);
                    ListArretsAnnonyms.Remove(selected);
                    //db.DeleteObject(selected);
                }

                if (TypeArret == EnTypeArret.Maintenance)
                {
                    IsModifiedMaintenance = true;

                }
                else
                {
                    IsModifiedAutre = true;
                }


            }

            RefreshGrid();
        }

        private void dtD_ValueChanged(object sender, EventArgs e)
        {
            txtDuree.Text = Tools.GetMinutesBetween2Times(dtF.Value, dtD.Value).MinutesToHoursMinutesTxt();
        }

        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }

            /*/ if (SessionProduction.SessionProductionArrets.Select(p => p.HeureDebut).ToList().Contains(dtD.Value.Date.TimeOfDay))
             {
                 this.ShowWarning("cette heure exist déja dans la liste");
                 return;
             }*/

          
            

            if (SessionProduction != null)
            {
                SessionProduction.SessionProductionArrets.Add(new SessionProductionArret()
                  {
                      Cause = cbCauseArret.Text.Trim(),
                      HeureDebut = dtD.Value,
                      HeureFin = dtF.Value,
                      DureeMinutes = Tools.GetMinutesBetween2Times(dtF.Value, dtD.Value),
                      TypeArret = (int)TypeArret,
                      //       SessionProduction = SessionProduction,

                  });
            }
            else
            {
                ListArretsAnnonyms.Add(new SessionProductionArret()
                    {
                        Cause = cbCauseArret.Text.Trim(),
                        HeureDebut = dtD.Value,
                        HeureFin = dtF.Value,
                        DureeMinutes = Tools.GetMinutesBetween2Times(dtF.Value, dtD.Value),
                        TypeArret = (int)TypeArret,
                        //       SessionProduction = SessionProduction,

                    });
            }

            if (TypeArret == EnTypeArret.Maintenance)
            {
                IsModifiedMaintenance = true;

            }
            else
            {
                IsModifiedAutre = true;
            }

            RefreshGrid();
            cbCauseArret.SelectedIndex = 0;
        }
        //   public List<SessionProductionArret> ListArrets { get; set; }
        private bool IsValidAll()
        {
            if (txtDuree.Text == "")
            {
                this.ShowWarning("heures invalides!");
                return false;
            }

            if (cbCauseArret.Text == "")
            {
                this.ShowWarning("Veillez spécifiez Cause d'arrêt!");
                return false;
            }

            return true;
        }

        // public SessionProduction SessionProduction { get; set; }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        //    public List<SessionProductionArret> ListArrets { get; set; }

        public SessionProduction SessionProduction { get; set; }

        public static List<SessionProductionArret> ListArretsAnnonyms { get; set; }
    }
}
