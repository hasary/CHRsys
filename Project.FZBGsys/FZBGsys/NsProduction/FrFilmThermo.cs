﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrFilmThermo : Form
    {
        ModelEntities db;//= new ModelEntities();
        Production Production = null;

        public FrFilmThermo()
        {
            db = new ModelEntities();
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionFilmThermo>();
            //     dtDate.Value = DateTime.Now;
        }

        public FrFilmThermo(Production Production, ModelEntities db)
        {
            if (db == null)
            {
                this.db = new ModelEntities();
            }
            else
            {
                this.db = db;
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionFilmThermo>();
            this.Production = Production;
            if (Production != null)
            {
                LoadProduction();
            }
        }

        private void LoadProduction()
        {

            cbFormat.SelectedItem = Production.Format;
            dtDate.Value = Production.HeureDemarrage.Value;
            ListToSave.AddRange(Production.ProductionFilmThermoes.ToList());
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
            txtNonConform.Text = Production.PoidsFilmThermoNonConform.ToAffInt();
        }

        public FrFilmThermo(DateTime date)
        {
            ListToSave = new List<ProductionFilmThermo>();
            dtDate.Value = date;
        }

        private void IntitialiseControls()
        {
            //---------------------- Fournisseur
            var TypeRessourceFilmThermo = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Film_Thermofusible);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourceFilmThermo)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;

            //---------------------- Type FilmThermo 

            cbTypeFilmThermo.Items.Add(TypeRessourceFilmThermo);
            cbTypeFilmThermo.DisplayMember = "Nom";
            cbTypeFilmThermo.ValueMember = "ID";
            cbTypeFilmThermo.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeFilmThermo.SelectedItem = TypeRessourceFilmThermo;

            //--------------------------------------------------------------------


            //-------------------------- DT Heure
            cbEpFilm.SelectedIndex = 0;
            dtHeure.Value = new DateTime(2012, 5, 1, 8, 0, 0);
            //  dtHeureFin.Value = new DateTime(2012, 5, 1, 9, 0, 0);
            //dtHeure.MaxDate = new DateTime(2012, 7, 1, 8, 0, 0);
            //dtHeure.MinDate = new DateTime(2012, 7, 1, 8, 0, 0);

            // -------------------------------------


        }

        private void FrFilmThermo_Load(object sender, EventArgs e)
        {

        }

        private void FrFilmThermo_FormClosing(object sender, FormClosingEventArgs e)
        {
            //db.Dispose();
        }
        public List<ProductionFilmThermo> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {

            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
                //       CreateProduction();
            }

            var newFilmThermo = new ProductionFilmThermo();

            //  newFilmThermo.Date = dtDate.Value.Date;
            newFilmThermo.DateHeure = Tools.GetFillDateFromDateTime(dtDate.Value, dtHeure.Value.TimeOfDay);


            newFilmThermo.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
            newFilmThermo.Epaisseur = cbEpFilm.SelectedItem.ToString();
            newFilmThermo.Poids = txtPoids.Text.ParseToDec();
            newFilmThermo.TypeRessource = (TypeRessource)cbTypeFilmThermo.SelectedItem;




            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(p => p.FournisseurID == newFilmThermo.FournisseurID && p.TypeRessourceID == (int)EnTypeRessource.Film_Thermofusible && p.EpaisseurFilmThermo == newFilmThermo.Epaisseur).OrderBy(p => p.ID);
                if (ressources.Count() == 0)
                {
                    this.ShowWarning("type de preforme n'est pas en Stock");
                    return;
                }
                var ressource = ressources.First();
                var newTransfert = new TransfertMatiere();
                newTransfert.Observation = "consomation " + newFilmThermo.DateHeure.Value.ToShortDateString() + " (auto)";
                newTransfert.DestinationSectionID = (Production.SectionID == (int)EnSection.Unité_PET) ? (int)EnSection.Unité_PET : (int)EnSection.Unité_Canette;
                newTransfert.Date = newFilmThermo.DateHeure.Value.Date;

                newTransfert.Ressource = ressource;
                newTransfert.Fournisseur = newFilmThermo.Fournisseur;
                newTransfert.QunatiteUM = newFilmThermo.Poids;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = 1; // one bobine
                newFilmThermo.TransfertMatiere = newTransfert;
                var sucess = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!sucess)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newFilmThermo);
                    return;
                }
                //db.AddToTransfertMatieres(newTransfert);

            }
            #endregion

            ListToSave.Add(newFilmThermo);
            Production.ProductionFilmThermoes.Add(newFilmThermo);

            RefreshGrid();
            UpdateControls();
            UpdateTotal();

        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
                //  dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            txtPoids.Text = "";

        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }

            if (cbEpFilm.SelectedIndex == 0)
            {
                Message += "Selectionner un Epaisseur";
            }

            if (cbFournisseur.SelectedIndex == 0)
            {
                Message += "Selectionner un Fournisseur";
            }

            /*if (cbTypeFilmThermo.SelectedIndex == 0)
            {
                Message += "\nSelectionner un Type Film ";
            }
            */


            //  decimal outInt = 0;

            if (ListToSave.Select(p => p.DateHeure).ToList().Contains(dtHeure.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }


          

            if (txtPoids.Text.Trim().ParseToDec() == null)
            {
                Message += "\nPoids Invalide!";

            }


            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            labTotal.Text = ListToSave.Count + " Bobines  " + ListToSave.Sum(p => p.Poids).ToString() + " Kg ";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Heure = p.DateHeure.Value.ToString(@"HH\:mm"),//+ " - " + p.HeureFin.Value.ToString(@"HH\:mm"),
                Type = p.TypeRessource.Nom,
                Epaisseur = p.Epaisseur,
                Fournisseur = p.Fournisseur.Nom,
                Poids = p.Poids,

            }).ToList();

            dgv.HideIDColumn();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("la liste est vide voulez-vous quand-meme enregistrer?"))
                {
                    return;
                }
            }




            if (txtNonConform.Text != "" && txtNonConform.Text.ParseToInt() == null)
            {
                this.ShowWarning("veillez saisir poids film non conformes!");
                return;
            }


            if (Production == null)
            {
                //  CreateProduction();
            }
            /* foreach (var FilmThermo in ListToSave)
             {
                 if (FilmThermo.ID == 0)
                 {
                     Production.ProductionFilmThermoes.Add(FilmThermo);
                 }
             }
             */
            UpdateProduction(); //  update total bouteilles in production object

            try
            {
                db.SaveChanges();
                Dispose();
            }
            catch (Exception exp)
            {
                this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

            }

        }

        private void UpdateProduction()
        {

            Production.NombreBobineFilmeThermo = ListToSave.Count;
            Production.PoidsFilmThermoNonConform = txtNonConform.Text.ParseToInt();
            Production.PoidsFilmThermo = ListToSave.Sum(p => p.Poids);// ListToSave.Sum(p => p.QuantitePiece);
        }

        /*private void CreateProduction()
        {
            Production = new Production()
            {
                DateDate = dtDate.Value.Date,
                Format = (Format)cbFormat.SelectedItem,
                SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
            };

          */

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            InputFilmThermo(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly 

            ListToSave.RemoveAt(selectedIndex.Value);
            var toDel = Production.ProductionFilmThermoes.ElementAt(selectedIndex.Value);
            Production.ProductionFilmThermoes.Remove(toDel);


            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }

            db.DeleteObject(toDel);
            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();

        }

        private void InputFilmThermo(ProductionFilmThermo s)
        {
            dtHeure.Value = s.DateHeure.Value;// new DateTime(2012, 5, 1, s.Heure.Value.Hours, s.Heure.Value.Minutes, 0);
            cbFournisseur.SelectedItem = s.Fournisseur;
            cbTypeFilmThermo.SelectedItem = s.TypeRessource;
            txtPoids.Text = s.Poids.ToString();


        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool EditForm { get; set; }

        private void txtNbrFilmThermo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEpaisseur_KeyPress(object sender, KeyPressEventArgs e)
        {
            var key = e.KeyChar;
            if (!char.IsNumber(key) && key != 8)
            {
                e.Handled = true;
            }

        }

        private void txtPoids_KeyPress(object sender, KeyPressEventArgs e)
        {
            var key = e.KeyChar;
            if (!char.IsNumber(key) && key != 8 && key != '.' && key != ',')
            {
                e.Handled = true;
            }
            if (key == '.')
            {
                e.KeyChar = ',';
            }
        }




    }
}
