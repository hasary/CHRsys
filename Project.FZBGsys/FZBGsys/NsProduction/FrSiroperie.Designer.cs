﻿namespace FZBGsys.NsProduction
{
    partial class FrSiroperie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panNombre = new System.Windows.Forms.Panel();
            this.labUniteDePiece = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.txtPiece = new System.Windows.Forms.TextBox();
            this.panTypeColorant = new System.Windows.Forms.Panel();
            this.cbTypeColorant = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.panTypeArrome = new System.Windows.Forms.Panel();
            this.cbTypeArrome = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTypeSiroperie = new System.Windows.Forms.ComboBox();
            this.txtQuantite = new System.Windows.Forms.TextBox();
            this.labUniteDeMesure = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCont = new System.Windows.Forms.TextBox();
            this.labContenu = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panNombre.SuspendLayout();
            this.panTypeColorant.SuspendLayout();
            this.panTypeArrome.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(263, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(746, 399);
            this.dgv.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Date:";
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(76, 22);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(166, 22);
            this.dtDate.TabIndex = 9;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(705, 415);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(304, 43);
            this.xpannel1.TabIndex = 11;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(36, 3);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(122, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(164, 3);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(263, 425);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(124, 425);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 19;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(76, 59);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(166, 24);
            this.cbFormat.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.labContenu);
            this.groupBox1.Controls.Add(this.txtCont);
            this.groupBox1.Controls.Add(this.panNombre);
            this.groupBox1.Controls.Add(this.panTypeColorant);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.panTypeArrome);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbTypeSiroperie);
            this.groupBox1.Controls.Add(this.txtQuantite);
            this.groupBox1.Controls.Add(this.labUniteDeMesure);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(15, 109);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(241, 301);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Siroperie";
            // 
            // panNombre
            // 
            this.panNombre.Controls.Add(this.labUniteDePiece);
            this.panNombre.Controls.Add(this.label99);
            this.panNombre.Controls.Add(this.txtPiece);
            this.panNombre.Location = new System.Drawing.Point(20, 192);
            this.panNombre.Name = "panNombre";
            this.panNombre.Size = new System.Drawing.Size(207, 37);
            this.panNombre.TabIndex = 28;
            // 
            // labUniteDePiece
            // 
            this.labUniteDePiece.AutoSize = true;
            this.labUniteDePiece.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDePiece.Location = new System.Drawing.Point(149, 11);
            this.labUniteDePiece.Name = "labUniteDePiece";
            this.labUniteDePiece.Size = new System.Drawing.Size(31, 17);
            this.labUniteDePiece.TabIndex = 23;
            this.labUniteDePiece.Text = "UM";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(12, 11);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(64, 17);
            this.label99.TabIndex = 23;
            this.label99.Text = "Nombre";
            // 
            // txtPiece
            // 
            this.txtPiece.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPiece.Location = new System.Drawing.Point(82, 8);
            this.txtPiece.Name = "txtPiece";
            this.txtPiece.Size = new System.Drawing.Size(61, 22);
            this.txtPiece.TabIndex = 5;
            this.txtPiece.TextChanged += new System.EventHandler(this.txtNbrSiroperie_TextChanged);
            // 
            // panTypeColorant
            // 
            this.panTypeColorant.Controls.Add(this.cbTypeColorant);
            this.panTypeColorant.Controls.Add(this.label4);
            this.panTypeColorant.Location = new System.Drawing.Point(18, 84);
            this.panTypeColorant.Name = "panTypeColorant";
            this.panTypeColorant.Size = new System.Drawing.Size(209, 52);
            this.panTypeColorant.TabIndex = 27;
            this.panTypeColorant.Visible = false;
            // 
            // cbTypeColorant
            // 
            this.cbTypeColorant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeColorant.FormattingEnabled = true;
            this.cbTypeColorant.Location = new System.Drawing.Point(113, 12);
            this.cbTypeColorant.Name = "cbTypeColorant";
            this.cbTypeColorant.Size = new System.Drawing.Size(85, 24);
            this.cbTypeColorant.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Type Colorant:";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(21, 162);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(205, 24);
            this.cbFournisseur.TabIndex = 7;
            // 
            // panTypeArrome
            // 
            this.panTypeArrome.Controls.Add(this.cbTypeArrome);
            this.panTypeArrome.Controls.Add(this.label5);
            this.panTypeArrome.Location = new System.Drawing.Point(20, 72);
            this.panTypeArrome.Name = "panTypeArrome";
            this.panTypeArrome.Size = new System.Drawing.Size(204, 43);
            this.panTypeArrome.TabIndex = 27;
            this.panTypeArrome.Visible = false;
            // 
            // cbTypeArrome
            // 
            this.cbTypeArrome.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeArrome.FormattingEnabled = true;
            this.cbTypeArrome.Location = new System.Drawing.Point(62, 12);
            this.cbTypeArrome.Name = "cbTypeArrome";
            this.cbTypeArrome.Size = new System.Drawing.Size(136, 24);
            this.cbTypeArrome.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Type:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Fournisseur:";
            // 
            // cbTypeSiroperie
            // 
            this.cbTypeSiroperie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeSiroperie.FormattingEnabled = true;
            this.cbTypeSiroperie.Location = new System.Drawing.Point(91, 33);
            this.cbTypeSiroperie.Name = "cbTypeSiroperie";
            this.cbTypeSiroperie.Size = new System.Drawing.Size(136, 24);
            this.cbTypeSiroperie.TabIndex = 1;
            this.cbTypeSiroperie.SelectedIndexChanged += new System.EventHandler(this.cbTypeSiroperie_SelectedIndexChanged);
            // 
            // txtQuantite
            // 
            this.txtQuantite.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQuantite.Location = new System.Drawing.Point(102, 270);
            this.txtQuantite.Name = "txtQuantite";
            this.txtQuantite.Size = new System.Drawing.Size(85, 22);
            this.txtQuantite.TabIndex = 5;
            this.txtQuantite.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantite_KeyPress);
            // 
            // labUniteDeMesure
            // 
            this.labUniteDeMesure.AutoSize = true;
            this.labUniteDeMesure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labUniteDeMesure.Location = new System.Drawing.Point(193, 273);
            this.labUniteDeMesure.Name = "labUniteDeMesure";
            this.labUniteDeMesure.Size = new System.Drawing.Size(31, 17);
            this.labUniteDeMesure.TabIndex = 23;
            this.labUniteDeMesure.Text = "UM";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Quantite:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Categorie:";
            // 
            // txtCont
            // 
            this.txtCont.Location = new System.Drawing.Point(102, 235);
            this.txtCont.Name = "txtCont";
            this.txtCont.Size = new System.Drawing.Size(61, 22);
            this.txtCont.TabIndex = 29;
            this.txtCont.TextChanged += new System.EventHandler(this.txtNbrSiroperie_TextChanged);
            // 
            // labContenu
            // 
            this.labContenu.AutoSize = true;
            this.labContenu.Location = new System.Drawing.Point(178, 238);
            this.labContenu.Name = "labContenu";
            this.labContenu.Size = new System.Drawing.Size(0, 17);
            this.labContenu.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(47, 237);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 17);
            this.label9.TabIndex = 31;
            this.label9.Text = "cont.";
            // 
            // FrSiroperie
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 470);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.dgv);
            this.Name = "FrSiroperie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Production Siroperie ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrSiroperie_FormClosing);
            this.Load += new System.EventHandler(this.FrSiroperie_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panNombre.ResumeLayout(false);
            this.panNombre.PerformLayout();
            this.panTypeColorant.ResumeLayout(false);
            this.panTypeColorant.PerformLayout();
            this.panTypeArrome.ResumeLayout(false);
            this.panTypeArrome.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTypeSiroperie;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtQuantite;
        private System.Windows.Forms.ComboBox cbTypeArrome;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panTypeArrome;
        private System.Windows.Forms.Panel panTypeColorant;
        private System.Windows.Forms.ComboBox cbTypeColorant;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panNombre;
        private System.Windows.Forms.Label labUniteDePiece;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox txtPiece;
        private System.Windows.Forms.Label labUniteDeMesure;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labContenu;
        private System.Windows.Forms.TextBox txtCont;
    }
}