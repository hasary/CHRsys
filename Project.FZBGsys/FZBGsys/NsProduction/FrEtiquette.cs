﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrEtiquette : Form
    {
        ModelEntities db;//= new ModelEntities();
        Production Production = null;

        public FrEtiquette()
        {
            db = new ModelEntities();
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionEtiquette>();
            //     dtDate.Value = DateTime.Now;
        }

        public FrEtiquette(Production Production, ModelEntities db = null)
        {
            this.Production = Production;// = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            if (db != null)
            {
                this.db = db;
            }
            else
            {
                this.db = new ModelEntities();
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionEtiquette>();
            this.Production = Production;// = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            if (Production != null)
            {
                LoadProduction();
            }
        }

        private void LoadProduction()
        {
            //  txtNonConform.Text = Production.BouteilleNonConform.ToString();
            cbFormat.SelectedItem = Production.Format;
            dtHeureDebut.Value = Production.HeureDemarrage.Value;
            dtHeureFin.Value = Production.HeureDemarrage.Value;
            ListToSave.AddRange(Production.ProductionEtiquettes.ToList());
          
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        public FrEtiquette(DateTime date)
        {
            ListToSave = new List<ProductionEtiquette>();
            dtHeureDebut.Value = date;
        }

        private void IntitialiseControls()
        {
            //---------------------- Fournisseur
            var TypeRessourceEtiquette = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Etiquette);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourceEtiquette)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;

            //---------------------- Type Etiquette 
            cbTypeEtiquette.Items.Add(new TypeEtiquette { ID = 0, Nom = "" });
            cbTypeEtiquette.Items.AddRange(db.TypeEtiquettes.Where(p => p.ID > 0).ToArray());
            cbTypeEtiquette.DisplayMember = "Nom";
            cbTypeEtiquette.ValueMember = "ID";
            cbTypeEtiquette.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeEtiquette.SelectedItem = null;


            //--------------------------------------------------- gout produit 
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            //cbTypeProduit.Items.Add(new GoutProduit { ID = 0, Nom = "Chréa-Pulpe" });
            var goutProduits = Production.SessionProductions.Select(p => p.GoutProduit).ToList();
            if (goutProduits.Count != 0)
            {
                cbGoutProduit.Items.AddRange(goutProduits.ToArray());
            }
            else
            {
                cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p=>p.ID !=0).ToArray());
            }
           
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;
            cbGoutProduit.SelectedItem = null;




            //-------------------------- DT Heure

            dtHeureDebut.Value = new DateTime(2012, 5, 1, 8, 0, 0);
            dtHeureFin.Value = new DateTime(2012, 5, 1, 9, 0, 0);
            //dtHeure.MaxDate = new DateTime(2012, 7, 1, 8, 0, 0);
            //dtHeure.MinDate = new DateTime(2012, 7, 1, 8, 0, 0);

            // -------------------------------------


        }

        private void FrEtiquette_Load(object sender, EventArgs e)
        {

        }

        private void FrEtiquette_FormClosing(object sender, FormClosingEventArgs e)
        {
            //db.Dispose();
        }
        public List<ProductionEtiquette> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {

            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
          //      CreateProduction();
            }

            var newEtiquette = new ProductionEtiquette();

            // newEtiquette.Date = dtDate.Value.Date;
            newEtiquette.HeureDebut = dtHeureDebut.Value;
            newEtiquette.HeureFin = dtHeureFin.Value;

            newEtiquette.DureeMinute = (int)(dtHeureFin.Value.TimeOfDay - dtHeureDebut.Value.TimeOfDay).TotalMinutes;
            newEtiquette.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;

            newEtiquette.QuantitePiece = txtNbrEtiquette.Text.ParseToInt();
            newEtiquette.TypeEtiquette = (TypeEtiquette)cbTypeEtiquette.SelectedItem;
            newEtiquette.GoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;


            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(p =>
                    p.FournisseurID == newEtiquette.FournisseurID &&
                    p.TypeRessourceID == (int)EnTypeRessource.Etiquette &&
                    p.TypeEtiquetteID == newEtiquette.TypeEtiquetteID &&
                    p.GoutProduitID == newEtiquette.GoutProduitID &&
                    p.FormatID == Production.Format.ID

                    ).OrderBy(p=>p.ID);
                if (ressources.Count() ==0 )
                {
                    this.ShowWarning("type Etiquette n'est pas en Stock");
                    return;
                }
                var ressource = ressources.First();

                var newTransfert = new TransfertMatiere();

                newTransfert.Observation = "consomation " + newEtiquette.HeureDebut.Value.ToShortDateString() + " (auto)";


                newTransfert.DestinationSectionID = (Production.SectionID == (int)EnSection.Unité_PET) ? (int)EnSection.Unité_PET : (int)EnSection.Unité_Canette;
                newTransfert.Date = newEtiquette.HeureDebut.Value;
                newTransfert.Ressource = ressource;
                newTransfert.Fournisseur = newEtiquette.Fournisseur;
                newTransfert.QunatiteUM = newEtiquette.QuantitePiece;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = 1; // one Box

                //newTransfert.StockMatiere = SelectedStockMT;
                newEtiquette.TransfertMatiere = newTransfert;
                var sucess = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!sucess)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newEtiquette);
                    return;
                }

            }
            #endregion




            ListToSave.Add(newEtiquette);
            Production.ProductionEtiquettes.Add(newEtiquette);



            RefreshGrid();
            UpdateControls();
            UpdateTotal();

        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
             //   dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            if (ListToSave != null && ListToSave.Count != 0)
            {
                dtHeureDebut.Value = dtHeureFin.Value;
                dtHeureFin.Value = dtHeureFin.Value.AddHours(1);
            }
            // txtNbrCarton.Text = "";
            txtNbrEtiquette.Text = "";

        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }

            if (cbFournisseur.SelectedIndex == 0)
            {
                Message += "Selectionner un Fournisseur";
            }

            if (cbTypeEtiquette.SelectedIndex == 0)
            {
                Message += "\nSelectionner un Type Etiquette";
            }

            if (cbGoutProduit.SelectedIndex < 1)
            {
                Message += "\nSelectionner GoutProduit";
            }

            int outInt = 0;

            if (ListToSave.Select(p => p.HeureDebut).ToList().Contains(dtHeureDebut.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }

            if (ListToSave.Select(p => p.HeureFin).ToList().Contains(dtHeureFin.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }



            if (!int.TryParse(txtNbrEtiquette.Text.Trim(), out outInt))
            {
                Message += "\nNombre Etiquettes Invalide!";

            }

            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            labTotal.Text = ListToSave.Sum(p => p.QuantitePiece).ToString() + " Etiquettes ";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Heure = p.HeureDebut.Value.ToString(@"HH\:mm") + " - " + p.HeureFin.Value.ToString(@"HH\:mm"),
                Nombre = p.QuantitePiece,
                Type = p.TypeEtiquette.Nom,
                Format = Production.Format.Volume,
                Produit = p.GoutProduit.Nom,

                Fournisseur = p.Fournisseur.Nom,

            }).ToList();

            dgv.HideIDColumn();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("la liste est vide voulez-vous quand-meme enregistrer?"))
                {
                    return;
                }
            }

            //   int nonConform = 0;
            /*  if (!int.TryParse(txtNonConform.Text.Trim(), out nonConform))
              {
                  this.ShowWarning("veillez saisir quantite boutilles non conformes!");
                  return;
              }*/


            if (Production == null)
            {
            //    CreateProduction();
            }
            /*   foreach (var Etiquette in ListToSave)
               {
                   if (Etiquette.ID == 0)
                   {
                       Production.ProductionEtiquettes.Add(Etiquette);
                   }
               }*/

            UpdateProduction(); //  update total bouteilles in production object

            try
            {
                db.SaveChanges();
                Dispose();
            }
            catch (Exception exp)
            {
                this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

            }

        }

        private void UpdateProduction()
        {

            Production.NombreEtiquette = ListToSave.Sum(p => p.QuantitePiece);
            Production.EtiquetteNonConforme = 0;// ListToSave.Sum(p => p.QuantitePiece);
        }

     /*   private void CreateProduction()
        {
            Production = new Production()
            {
                Date = dtDate.Value.Date,
                Format = (Format)cbFormat.SelectedItem,
                SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
            };

            db.AddToProductions(Production);
        }*/

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            if (selectedIndex != null)
            {
                InputEtiquette(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly  
            }

            ListToSave.RemoveAt(selectedIndex.Value);
            var toDel = Production.ProductionEtiquettes.ElementAt(selectedIndex.Value);
            Production.ProductionEtiquettes.Remove(toDel);


            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }


            db.DeleteObject(toDel);
            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();


        }

        private void InputEtiquette(ProductionEtiquette s)
        {
            dtHeureDebut.Value = s.HeureDebut.Value;
            dtHeureFin.Value = s.HeureFin.Value;


            cbFournisseur.SelectedItem = s.Fournisseur;
            cbTypeEtiquette.SelectedItem = s.TypeEtiquette;
            //dtDateFabriq.Value = s.DateFabrication.Value;
            //txtNbrCarton.Text = s.NombreCartons.ToString();
            txtNbrEtiquette.Text = s.QuantitePiece.ToString();
            try
            {
                cbGoutProduit.SelectedItem = s.GoutProduit;
            }
            catch (Exception)
            {

                cbGoutProduit.SelectedIndex = 0;
            }

        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool EditForm { get; set; }

        private void txtNbrEtiquette_TextChanged(object sender, EventArgs e)
        {

        }




    }
}
