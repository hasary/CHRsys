﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrSession : Form
    {
        public bool IsSessionArret = false;

        ModelEntities db;//= new ModelEntities();
        private bool ModifiedLotAPalette;
        private bool ModifiedLotBPalette;
        private bool ModifiedLotCPalette;
        private bool ModifiedLotDPalette;
        private bool ModifiedLotEPalette;
        private bool ModifiedLotFPalette;
        private bool ModifiedLotAFardeau;
        private bool ModifiedLotBFardeau;
        private bool ModifiedLotCFardeau;
        private bool ModifiedLotDFardeau;
        private bool ModifiedLotEFardeau;
        private bool ModifiedLotFFardeau;



        public FrSession(bool isSesstionArret, Production production, DateTime? heureDebut = null, int CurrentPaletteNo = 1)
        {
            this.IsSessionArret = isSesstionArret;
            db = new ModelEntities();
            InitializeComponent();
            this.CurrentNoPalette = CurrentPaletteNo;
            if (heureDebut == null)
            {

                this.HeureDebut = production.HeureDemarrage;

            }
            else
            {
                this.HeureDebut = heureDebut;
                chRinçage.Checked = false;
                dtDemarrage.Value = heureDebut.Value;
            }


            InitialiseControls();

            this.Production = production;
            dtDateDemarrageAll.Value = production.HeureDemarrage.Value;
            dtDateDemarrageAll.Enabled = false;

            if(!IsSessionArret)txtFormat.Text = production.Format.Volume;

            AnnonymeArrtesAutres = new List<SessionProductionArret>();
            AnnonymeArrtesMaintenance = new List<SessionProductionArret>();
        }

        public FrSession(bool isSesstionArret, SessionProduction Session, ModelEntities db, int CurrentNoPalette = 1)
        {
            this.IsSessionArret = isSesstionArret;
            this.CurrentNoPalette = CurrentNoPalette;
            SessionProduction = Session;
            this.db = db;
            this.HeureDebut = Session.HeureDemarrage.Value;
            InitializeComponent();
            InitialiseControls();
            Production = SessionProduction.Production;
            AnnonymeArrtesAutres = new List<SessionProductionArret>();
            AnnonymeArrtesMaintenance = new List<SessionProductionArret>();
            InputSession(Session);

        }

        private void InputSession(SessionProduction Session)
        {
            SessionProduction = Session;
            dtDateDemarrageAll.Value = Production.HeureDemarrage.Value;
           if(!IsSessionArret) txtFormat.Text = Production.Format.Volume;
           if (!IsSessionArret) cbGoutProduit.SelectedItem = SessionProduction.GoutProduit;
           if (!IsSessionArret) txtNbrConge.Text = SessionProduction.NombreDeConge.ToString();
            SessionProductionArret arret = null;

            dtDemarrage.Value = SessionProduction.HeureDemarrage.Value;

            arret = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.Rinçage);
            if (arret != null)
            {
                chRinçage.Checked = true;
                dtRincageD.Value = arret.HeureDebut.Value; //Tools.GetFillDateFromDateHour(Production.HeureDemarrage, arret.HeureDebut);
                dtRinçageF.Value = arret.HeureFin.Value; //Tools.GetFillDateFromDateHour(Production.HeureDemarrage, arret.HeureFin);

            }
            else
            {
                chRinçage.Checked = false;
            }

            arret = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.CIP);
            if (arret != null)
            {
                chCIP.Checked = true;
                dtCIPD.Value = arret.HeureDebut.Value; //Tools.GetFillDateFromDateHour(Production.Heure, arret.HeureDebut);
                dtCIPF.Value = arret.HeureFin.Value; //Tools.GetFillDateFromDateHour(Production.Date, arret.HeureFin);

            }
            else
            {
                chCIP.Checked = false;

            }


            arret = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.RinçageFinProd);
            if (arret != null)
            {
                dtRinçageFinProductionD.Value = arret.HeureDebut.Value;// Tools.GetFillDateFromDateHour(Production.Date, arret.HeureDebut);
                dtRinçageFinProductionF.Value = arret.HeureFin.Value;// Tools.GetFillDateFromDateHour(Production.Date, arret.HeureFin);

            }

            arret = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.RinçageFinProd);
            if (arret != null)
            {
                chRinçageFinSession.Checked = true;
                dtRinçageFinProductionD.Value = arret.HeureDebut.Value; //Tools.GetFillDateFromDateHour(Production.HeureDemarrage, arret.HeureDebut);
                dtRinçageFinProductionF.Value = arret.HeureFin.Value; //Tools.GetFillDateFromDateHour(Production.HeureDemarrage, arret.HeureFin);

            }
            else
            {
                chRinçageFinSession.Checked = false;
            }


            this.AnnonymeArrtesMaintenance.Clear();
            this.AnnonymeArrtesMaintenance.AddRange(SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance).ToList());

            this.AnnonymeArrtesAutres.Clear();
            this.AnnonymeArrtesAutres.AddRange(SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Autre).ToList());

            UpdateArretAutre();
            UpdateArretMaintenance();

            dtFinDeProd.Value = SessionProduction.HeureFinProduction.Value;// Tools.GetFillDateFromDateHour(Production.Heure, SessionProduction.HeureFinProduction);



            //-------------------------------

            if (Session.NombreBouteillesLotA != null)
            {
                chLotA.Checked = true;
                txtPaletteA.Text = SessionProduction.NombrePalettesLotA.ToString();

                txtFardA.Text = SessionProduction.NombreFardeauxLotA.ToString();
            }

            if (Session.NombreBouteillesLotB != null)
            {
                chLotB.Checked = true;
                txtPaletteB.Text = SessionProduction.NombrePalettesLotB.ToString();
                txtFardB.Text = SessionProduction.NombreFardeauxLotB.ToString();
            }

            if (Session.NombreBouteillesLotC != null)
            {
                chLotC.Checked = true;
                txtPaletteC.Text = SessionProduction.NombrePalettesLotC.ToString();
                txtFardC.Text = SessionProduction.NombreFardeauxLotC.ToString();
            }

            if (Session.NombreBouteillesLotD != null)
            {
                chLotD.Checked = true;
                txtPaletteD.Text = SessionProduction.NombrePalettesLotD.ToString();
                txtFardD.Text = SessionProduction.NombreFardeauxLotD.ToString();
            }

            if (Session.NombreBouteillesLotE != null)
            {
                chLotE.Checked = true;
                txtPaletteE.Text = SessionProduction.NombrePalettesLotE.ToString();
                txtFardE.Text = SessionProduction.NombreFardeauxLotE.ToString();
            }


            if (Session.NombreBouteillesLotF != null)
            {
                chLotF.Checked = true;
                txtPaletteF.Text = SessionProduction.NombrePalettesLotF.ToString();
                txtFardF.Text = SessionProduction.NombreFardeauxLotF.ToString();
            }

        }

        private void UpdateArretMaintenance()
        {
            txtHeureMaintenance.Text = "";
            txtMaintenance.Text = "";

            foreach (var arretMaint in AnnonymeArrtesMaintenance)
            {
                txtHeureMaintenance.Text += arretMaint.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arretMaint.HeureFin.Value.ToString(@"HH\:mm") + "   ";
            }
            txtMaintenance.Text = AnnonymeArrtesMaintenance.Sum(p => p.DureeMinutes).MinutesToHoursMinutesTxt();
        }

        private void UpdateArretAutre()
        {
            txtHeureAutre.Text = "";
            txtAutre.Text = "";

            foreach (var arretAutre in AnnonymeArrtesAutres)
            {
                txtHeureAutre.Text += arretAutre.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arretAutre.HeureFin.Value.ToString(@"HH\:mm") + "   ";
            }

            txtAutre.Text = AnnonymeArrtesAutres.Sum(p => p.DureeMinutes).MinutesToHoursMinutesTxt();


        }


        private void UpdateTotalTemps()
        {


            int? totalTravail = Tools.GetMinutesBetween2Times(dtFinDeProd.Value, dtDemarrage.Value);
            int? totalArret = 0;

            if (chRinçage.Checked && txtRincage.Text != "")
            {
                totalArret += txtRincage.Text.GetTotalMinutesFromHeureTxt();
            }

            if (chCIP.Checked && txtCIP.Text != "")
            {
                totalArret += txtCIP.Text.GetTotalMinutesFromHeureTxt();
            }

            if (txtMaintenance.Text != "")
            {
                totalArret += txtMaintenance.Text.GetTotalMinutesFromHeureTxt();
            }

            if (chRinçageFinSession.Checked && txtRinçageFin.Text != "")
            {
                totalArret += txtRinçageFin.Text.GetTotalMinutesFromHeureTxt();
            }


            if (txtAutre.Text != "")
            {
                totalArret += txtAutre.Text.GetTotalMinutesFromHeureTxt();
            }


            int? heureNetdeProd = totalTravail - totalArret;
            if (chRinçageFinSession.Checked && dtRinçageFinProductionD.Value.Hour == 00 && dtRinçageFinProductionF.Value.Hour == 0)
            {
                return;
            }

            if (dtFinDeProd.Value.CompareTo(dtDemarrage.Value) > 0)
            {
                labDateFin.Text = "";
            }
            else
            {
                labDateFin.Text = dtDateDemarrageAll.Value.AddDays(1).ToShortDateString();
            }
            //---------------------------------------------------
            txtHeureNetProd.Text = heureNetdeProd.MinutesToHoursMinutesTxt();
            txtTotalArret.Text = totalArret.MinutesToHoursMinutesTxt();
            txtHeureTotalTravail.Text = ((int?)totalTravail).MinutesToHoursMinutesTxt();
            //---------------------------------------------------
            if (labDateFin.Text != "")
            {
               if(!this.IsSessionArret) Production.HeureFinDeProd = Tools.GetFillDateFromDateTime(DateTime.Parse(labDateFin.Text), dtFinDeProd.Value.TimeOfDay);
            }
            else
            {
                Production.HeureFinDeProd = Tools.GetFillDateFromDateTime(Production.HeureDemarrage.Value.Date, dtFinDeProd.Value.TimeOfDay);
            }
        }
        private void InitialiseControls()
        {
            //--------------------------- type Produit
            cbGoutProduit.Items.Add(new GoutProduit { ID = 0, Nom = "" });
            cbGoutProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
            cbGoutProduit.SelectedIndex = 0;
            cbGoutProduit.ValueMember = "ID";
            cbGoutProduit.DisplayMember = "Nom";
            cbGoutProduit.DropDownStyle = ComboBoxStyle.DropDownList;

            //////////---------------------------- heures

            dtCIPD.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);
            dtCIPF.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);
            dtFinDeProd.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);
            dtRincageD.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);
            dtRinçageF.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);
            dtRinçageFinProductionD.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);
            dtRinçageFinProductionF.Value = Tools.GetFillDateFromDateTime(HeureDebut, dtCIPD.Value.TimeOfDay);


            if (chRinçage.Checked)
            {
                dtRincageD.Value = this.HeureDebut.Value;
            }
            else
            {
                dtDemarrage.Value = this.HeureDebut.Value;
            }
            ////////////////////////////////////////////////////////////////////////////
            panInfos2.Visible = panInfos1.Visible = !IsSessionArret;


            if (IsSessionArret)
            {
                chRinçageFinSession.Checked = false;
                chRinçageFinSession.Enabled = false;
                chRinçage.Checked = false;
                chRinçage.Enabled = false;
                chCIP.Checked = false;
                chCIP.Enabled = false;
                tabControl.TabPages.Remove(tpQuantiteProduite);
                tabControl.Enabled = true;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cbTypeProduit_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabControl.Enabled = cbGoutProduit.SelectedIndex != 0;
            UpdateProdTheorique();
        }

        public DateTime? HeureDebut { get; set; }

        private void chRincage_CheckStateChanged(object sender, EventArgs e)
        {
            panRincage.Visible = chRinçage.Checked;
            dtRincageD.Value = dtDemarrage.Value;
            UpdateTotalTemps();
        }

        private void chCIP_CheckedChanged(object sender, EventArgs e)
        {
            panCIP.Visible = chCIP.Checked;
            UpdateTotalTemps();
        }




        private void dtRinçageF_ValueChanged(object sender, EventArgs e)
        {

        }
        private void dtDemarrage_ValueChanged(object sender, EventArgs e)
        {
            UpdateTotalTemps();
        }
        private void dtRincageD_ValueChanged(object sender, EventArgs e)
        {
            txtRincage.Text = Tools.GetMinutesBetween2Times(dtRinçageF.Value, dtRincageD.Value).MinutesToHoursMinutesTxt();
            dtDemarrage.Value = dtRincageD.Value;
            //  HeureDebut = dtRincageD.Value;
        }
        private void dtCIPD_ValueChanged(object sender, EventArgs e)
        {
            txtCIP.Text = Tools.GetMinutesBetween2Times(dtCIPF.Value, dtCIPD.Value).MinutesToHoursMinutesTxt();
        }

        private void dtRinçageFinD_ValueChanged(object sender, EventArgs e)
        {
            txtRinçageFin.Text = Tools.GetMinutesBetween2Times(dtRinçageFinProductionF.Value, dtRinçageFinProductionD.Value).MinutesToHoursMinutesTxt();
            dtFinDeProd.Value = dtRinçageFinProductionF.Value;
        }

        private void txtPalette_KeyPress(object sender, KeyPressEventArgs e)
        {
            var key = e.KeyChar;

            if (!char.IsNumber(key) && key != 8)
            {
                e.Handled = true;
            }
        }

        private void SetSessisonDate()
        {
            var dateDemarrage = HeureDebut.Value.Date;
            var timeDemarrage = dtDemarrage.Value;

            SessionProduction.HeureDemarrage = Tools.GetFillDateFromDateTime(dateDemarrage, timeDemarrage.TimeOfDay);

            foreach (var arret in SessionProduction.SessionProductionArrets)
            {
                arret.HeureDebut = Tools.GetFillDateFromDateTime(dateDemarrage, arret.HeureDebut.Value.TimeOfDay); // SetDateKeepTime(dateDemarrage); =
                if (arret.HeureDebut.Value.CompareTo(timeDemarrage) < 0) arret.HeureDebut = Tools.GetFillDateFromDateTime(dateDemarrage.AddDays(1), arret.HeureDebut.Value.TimeOfDay); //.SetDateKeepTime(dateDemarrage.AddDays(1));

                arret.HeureFin = Tools.GetFillDateFromDateTime(dateDemarrage, arret.HeureFin.Value.TimeOfDay);//.SetDateKeepTime(dateDemarrage);
                if (arret.HeureFin.Value.CompareTo(timeDemarrage) < 0) arret.HeureFin = Tools.GetFillDateFromDateTime(dateDemarrage.AddDays(1), arret.HeureFin.Value.TimeOfDay); //SetDateKeepTime(dateDemarrage.AddDays(1));
            }

            SessionProduction.HeureFinProduction = Tools.GetFillDateFromDateTime(dateDemarrage, SessionProduction.HeureFinProduction.Value.TimeOfDay); // SetDateKeepTime(dateDemarrage);
            if (SessionProduction.HeureFinProduction.Value.CompareTo(timeDemarrage) < 0) SessionProduction.HeureFinProduction = Tools.GetFillDateFromDateTime(dateDemarrage.AddDays(1), SessionProduction.HeureFinProduction.Value.TimeOfDay); //.SetDateKeepTime(dateDemarrage.AddDays(1));


        }

        public SessionProduction SessionProduction { get; set; }
        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (!IsValidAll())
            {
                return;
            }

            if (this.SessionProduction == null)
            {
                SessionProduction = new SessionProduction();
                Production.SessionProductions.Add(SessionProduction);
            }

            //   SessionProduction.Date = Production.Date;
            SessionProduction.GoutProduitID = ((GoutProduit)cbGoutProduit.SelectedItem).ID;
            UpdateSessionHeures();
            UpdateSessionQuantite();
            UpdateStockPET();

            Dispose();

        }
        private void UpdateStockPET()
        {

            if (ModifiedLotAPalette)
            {
                RemoveStockPalettePET("A");
                if (chLotA.Checked) CreateStockPalettePET("A");
            }
            else
            {
                UpdateNumeroPalettes("A");
            }



            if (ModifiedLotBPalette)
            {
                RemoveStockPalettePET("B");
                if (chLotB.Checked) CreateStockPalettePET("B");
            }
            else
            {
                UpdateNumeroPalettes("B");
            }

            if (ModifiedLotCPalette)
            {
                RemoveStockPalettePET("C");
                if (chLotC.Checked) CreateStockPalettePET("C");
            }
            else
            {
                UpdateNumeroPalettes("C");
            }

            if (ModifiedLotDPalette)
            {
                RemoveStockPalettePET("D");
                if (chLotD.Checked) CreateStockPalettePET("D");
            }
            else
            {
                UpdateNumeroPalettes("D");
            }

            if (ModifiedLotEPalette)
            {
                RemoveStockPalettePET("E");
                if (chLotE.Checked) CreateStockPalettePET("E");
            }
            else
            {
                UpdateNumeroPalettes("E");
            }

            if (ModifiedLotEPalette)
            {
                RemoveStockPalettePET("F");
                if (chLotE.Checked) CreateStockPalettePET("F");
            }
            else
            {
                UpdateNumeroPalettes("F");
            }



            ///////----------------------------------------------- fardeaux

            if (ModifiedLotAFardeau)
            {
                RemoveStockFardeauPET("A");
                if (chLotA.Checked) CreateStockFardeauPET("A");
            }

            if (ModifiedLotBFardeau)
            {
                RemoveStockFardeauPET("B");
                if (chLotB.Checked) CreateStockFardeauPET("B");
            }

            if (ModifiedLotCFardeau)
            {
                RemoveStockFardeauPET("C");
                if (chLotC.Checked) CreateStockFardeauPET("C");
            }

            if (ModifiedLotDFardeau)
            {
                RemoveStockFardeauPET("D");
                if (chLotD.Checked) CreateStockFardeauPET("D");
            }

            if (ModifiedLotEFardeau)
            {
                RemoveStockFardeauPET("E");
                if (chLotE.Checked) CreateStockFardeauPET("E");
            }

            if (ModifiedLotEFardeau)
            {
                RemoveStockFardeauPET("F");
                if (chLotE.Checked) CreateStockFardeauPET("F");
            }

        }
        private void UpdateNumeroPalettes(string lot)
        {
            var StockPETs = SessionProduction.StockPETPalettes.Where(p => p.Lot == lot);
            if (StockPETs.Count() > 1)
            {
                this.ShowWarning("Impossible de modifier Numéros palettes lot " + lot + " car le lot est divisé");
                return;
            }
            else
            {
                var first = StockPETs.FirstOrDefault();
                if (first == null)
                {
                    return;
                }
                first.Numeros = CurrentNoPalette + "_" + (CurrentNoPalette + first.Nombre - 1);
                CurrentNoPalette += first.Nombre.Value;
            }


        }
        //-------------------------------------------------------------------------------- gestion de stock Palette
        private void RemoveStockPalettePET(string Lot)
        {
            var existings = SessionProduction.StockPETPalettes.Where(p => p.Lot == Lot).ToList();
            var existingsTransfered = existings.Where(p => p.EnStock == false).ToList();

            if (existingsTransfered.Count != 0)
            {
                if (Tools.AutoCreateTransfertFromProductionPF)
                {// somme are trasnferd automatiquely
                    foreach (var StockPETtransfered in existingsTransfered)
                    {// ferify all produit fini if the are in stock
                        var transfertsProduitFini = StockPETtransfered.TransfertPalettes;
                        foreach (var transfert in transfertsProduitFini)
                        {
                            var stockProduitFini = transfert.StockPETPalette;
                            if (stockProduitFini.EnStock == false)
                            {
                                this.ShowWarning("Impossible de supprimer Lot A de la production car produit fini à été vendu\nSi vous souhaiter modifier cette session il faudrait d'abord annuler le transfert manuel ou la vente associée.");
                                return;
                            }
                        }
                    }

                    foreach (var StockPETtransfered in existingsTransfered)
                    {//then process deleting
                        var transfertsProduitFini = StockPETtransfered.TransfertPalettes;
                        foreach (var transfert in transfertsProduitFini)
                        {
                            var stockProduitFini = transfert.StockPETPalette;
                            db.DeleteObject(stockProduitFini); //---> stockPET ---> tansfert ---> stockProduitFini
                            db.DeleteObject(transfert);
                        }
                        SessionProduction.StockPETPalettes.Remove(StockPETtransfered);
                        db.DeleteObject(StockPETtransfered);
                    }

                }
                else //no automatique transfert so they are manualy transfered
                {
                    this.ShowWarning("Impossible de supprimer Lot A de la production car produit fini à été transferé manuelement ou vendu\nSi vous souhaiter modifier cette session il faudrait d'abord annuler la vente associée.");
                    return;
                }

            }
            else // all PF are in stock so no transfiert ni auto ni manuel
            {
                try
                {
                    foreach (var stockPF in existings)
                    {
                        SessionProduction.StockPETPalettes.Remove(stockPF);
                        db.DeleteObject(stockPF);
                    }
                }
                catch (Exception)
                {


                }

            }



        }
        private void CreateStockPalettePET(string Lot)
        {
            int? NombrePalette = 0;   // f(Lot)
            int? TotalBouteilles = 0; // f(Lot)

            #region get Nombres
            switch (Lot)
            {
                case "A": NombrePalette = SessionProduction.NombrePalettesLotA;
                    TotalBouteilles = SessionProduction.NombreBouteillesLotA;
                    break;
                case "B": NombrePalette = SessionProduction.NombrePalettesLotB;
                    TotalBouteilles = SessionProduction.NombreBouteillesLotB;
                    break;
                case "C": NombrePalette = SessionProduction.NombrePalettesLotC;
                    TotalBouteilles = SessionProduction.NombreBouteillesLotC;
                    break;
                case "D": NombrePalette = SessionProduction.NombrePalettesLotD;
                    TotalBouteilles = SessionProduction.NombreBouteillesLotD;
                    break;
                case "E": NombrePalette = SessionProduction.NombrePalettesLotE;
                    TotalBouteilles = SessionProduction.NombreBouteillesLotE;
                    break;
                case "F": NombrePalette = SessionProduction.NombrePalettesLotF;
                    TotalBouteilles = SessionProduction.NombreBouteillesLotF;
                    break;
                default:
                    this.ShowError("invalide lot");
                    return;

            }
            #endregion

            if (NombrePalette == 0 || NombrePalette == null)
            {
                return;
            }
            var existingStockPET = SessionProduction.StockPETPalettes.Where(p => p.Lot == Lot);
            if (existingStockPET.Count() == 0)
            {
                var stockPET = new StockPETPalette()
                {
                    EnStock = true,
                    Lot = Lot,
                    DateProduction = SessionProduction.Production.HeureFinDeProd.Value,
                    Nombre = NombrePalette,
                    Numeros = CurrentNoPalette.ToString() + "_" + (NombrePalette + CurrentNoPalette - 1).ToString(),
                    TotalBouteilles = NombrePalette * SessionProduction.Production.Format.BouteillesParFardeau * SessionProduction.Production.Format.FardeauParPalette,
                };
                CurrentNoPalette += NombrePalette.Value;

                //var Date = SessionProduction.HeureFinProduction.Value.Date;
                //FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockauto(Date, db, SessionProduction, NombrePalette);
                

                /*using (var st = new ModelEntities())
                {

                    var stockman = db.StockProduitFiniManuals.First(p => p.FormatID == SessionProduction.Production.BouteileFormatID && p.GoutProduitID == SessionProduction.GoutProduitID && p.DateStock == Date);
                    stockman.NombrePalettes += stockPET.Nombre;
                    stockman.TotalBouteilles += stockPET.TotalBouteilles;
                    //st.SaveChanges();
                }*/
                


                SessionProduction.StockPETPalettes.Add(stockPET);
                               
                if (Tools.AutoCreateTransfertFromProductionPF)
                {
                    AutoTransfertPalette(stockPET);

                }
            }
        }
        private void AutoTransfertPalette(StockPETPalette stockPET)
        {

            stockPET.DateSortie = Production.HeureFinDeProd.Value.Date;
            stockPET.EnStock = false;

            var transfert = new TransfertPalette()
            {
                DateTransfert = Production.HeureFinDeProd.Value.Date,
            };

            var stockPF = new StockPalette()
            {
                DateEntree = Production.HeureFinDeProd,
                EnStock = true,
                Format = Production.Format,
                GoutProduit = SessionProduction.GoutProduit,
                Lot = stockPET.Lot,
                Nombre = stockPET.Nombre,
                Numeros = stockPET.Numeros,
                TotalBouteilles = stockPET.TotalBouteilles,

            };

            transfert.StockPalettes.Add(stockPF);
            stockPET.TransfertPalettes.Add(transfert);
            SessionProduction.StockPETPalettes.Add(stockPET);

        }
        //-------------------------------------------------------------------------------- gestion de stock fardeaux
        private void RemoveStockFardeauPET(string Lot)
        {


            var existings = SessionProduction.StockPETFardeaux.Where(p => p.Lot == Lot).ToList();
            var existingsTransfered = existings.Where(p => p.EnStock == false).ToList();

            if (existingsTransfered.Count != 0)
            {
                if (Tools.AutoCreateTransfertFromProductionPF)
                {// somme are trasnferd automatiquely
                    foreach (var StockFARtransfered in existingsTransfered)
                    {// ferify all produit fini if the are in stock
                        var transfertsFardeaux = StockFARtransfered.TransfertFardeaux;
                        foreach (var transfert in transfertsFardeaux)
                        {
                            var stockFardeax = transfert.StockPETFardeau;
                            if (stockFardeax.EnStock == false)
                            {
                                this.ShowWarning("Impossible de supprimer Lot A de la production car produit fini à été vendu\nSi vous souhaiter modifier cette session il faudrait d'abord annuler le transfert manuel ou la vente associée.");
                                return;
                            }
                        }
                    }

                    foreach (var StockPETtransfered in existingsTransfered)
                    {//then process deleting
                        var transfertsFardeaux = StockPETtransfered.TransfertFardeaux;
                        foreach (var transfert in transfertsFardeaux)
                        {
                            var stockFardeaux = transfert.StockPETFardeau;
                            db.DeleteObject(stockFardeaux); //---> stockPET ---> tansfert ---> stockProduitFini
                            db.DeleteObject(transfert);
                        }
                        SessionProduction.StockPETFardeaux.Remove(StockPETtransfered);
                        db.DeleteObject(StockPETtransfered);
                    }

                }
                else //no automatique transfert so they are manualy transfered
                {
                    this.ShowWarning("Impossible de supprimer Lot A de la production car produit fini à été transferé manuelement ou vendu\nSi vous souhaiter modifier cette session il faudrait d'abord annuler la vente associée.");
                    return;
                }

            }
            else // all PF are in stock so no transfiert ni auto ni manuel
            {
                foreach (var stockFA in existings)
                {
                    SessionProduction.StockPETFardeaux.Remove(stockFA);
                    db.DeleteObject(stockFA);
                }

            }


        }
        private void CreateStockFardeauPET(string Lot)
        {
            int? NombreFard = 0;   // f(Lot)
            int? TotalBouteilles = 0; // f(Lot)

            #region get Nombres
            switch (Lot)
            {
                case "A": NombreFard = SessionProduction.NombreFardeauxLotA;
                    break;
                case "B": NombreFard = SessionProduction.NombreFardeauxLotB;
                    break;
                case "C": NombreFard = SessionProduction.NombreFardeauxLotC.Value;
                    break;
                case "D": NombreFard = SessionProduction.NombreFardeauxLotD.Value;
                    break;
                case "E": NombreFard = SessionProduction.NombreFardeauxLotE.Value;
                    break;

                case "F": NombreFard = SessionProduction.NombreFardeauxLotF;
                    break;

                default:
                    this.ShowError("invalide lot");
                    return;

            }

            TotalBouteilles = SessionProduction.Production.Format.BouteillesParFardeau.Value * NombreFard;
            #endregion

            if (NombreFard == 0 || NombreFard == null)
            {
                return;
            }

            var existingStockPET = SessionProduction.StockPETFardeaux.Where(p => p.Lot == Lot);
            if (existingStockPET.Count() == 0)
            {
                var stockPET = new StockPETFardeau()
                {
                    DateProduction = SessionProduction.Production.HeureFinDeProd.Value,
                    EnStock = true,
                    Lot = Lot,
                    Nombre = NombreFard,
                    TotalBouteilles = TotalBouteilles,
                };
                SessionProduction.StockPETFardeaux.Add(stockPET);
                if (Tools.AutoCreateTransfertFromProductionPF)
                {
                    AutoTransfertFardeau(stockPET);

                }
            }
        }
        private void AutoTransfertFardeau(StockPETFardeau stockPET)
        {

            stockPET.DateSotie = Production.HeureFinDeProd.Value.Date;
            stockPET.EnStock = false;

            var transfert = new TransfertFardeau()
            {
                DateTransfert = Production.HeureFinDeProd.Value.Date,
            };

            var stockPF = new StockFardeau()
            {
                DateEntree = Production.HeureFinDeProd,
                EnStock = true,
                Format = Production.Format,
                GoutProduit = SessionProduction.GoutProduit,
                Lot = stockPET.Lot,
                Nombre = stockPET.Nombre,

                TotalBouteilles = stockPET.TotalBouteilles,

            };

            transfert.StockFardeaux.Add(stockPF);
            stockPET.TransfertFardeaux.Add(transfert);
            SessionProduction.StockPETFardeaux.Add(stockPET);

        }

        private void UpdateSessionHeures()
        {
            if (SessionProduction.EntityState == EntityState.Added) // new Session => use annonyme arret
            {
                foreach (var arret in AnnonymeArrtesMaintenance)
                {
                    arret.SessionProduction = SessionProduction;
                }
                foreach (var arret in AnnonymeArrtesAutres)
                {
                    arret.SessionProduction = SessionProduction;
                }
            }

            SessionProduction.HeureDemarrage = dtDemarrage.Value;

            var rinçage = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.Rinçage);
            if (chRinçage.Checked)
            {
                if (rinçage == null)
                {
                    SessionProduction.SessionProductionArrets.Add(new SessionProductionArret()
                    {
                        TypeArret = (int)EnTypeArret.Rinçage,
                        HeureDebut = dtRincageD.Value,
                        HeureFin = dtRinçageF.Value,
                        DureeMinutes = Tools.GetMinutesBetween2Times(dtRinçageF.Value, dtRincageD.Value),
                        Cause = "Rinçage",
                    });

                }
                else
                {
                    rinçage.HeureDebut = dtRincageD.Value;
                    rinçage.HeureFin = dtRinçageF.Value;
                    rinçage.DureeMinutes = Tools.GetMinutesBetween2Times(dtRinçageF.Value, dtRincageD.Value);
                }
            }
            else // non checked
            {
                if (rinçage != null)
                {
                    SessionProduction.SessionProductionArrets.Remove(rinçage);
                    db.DeleteObject(rinçage);
                }

            }

            var CIP = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.CIP);
            if (chCIP.Checked)
            {
                if (CIP == null)
                {
                    SessionProduction.SessionProductionArrets.Add(new SessionProductionArret()
                    {
                        TypeArret = (int)EnTypeArret.CIP,
                        HeureDebut = dtCIPD.Value,
                        HeureFin = dtCIPF.Value,
                        DureeMinutes = Tools.GetMinutesBetween2Times(dtRinçageF.Value, dtRincageD.Value),
                        Cause = "CIP",
                    });

                }
                else
                {
                    CIP.HeureDebut = dtCIPD.Value;
                    CIP.HeureFin = dtCIPF.Value;
                    CIP.DureeMinutes = Tools.GetMinutesBetween2Times(dtCIPF.Value, dtCIPD.Value);
                }
            }
            else // non checked
            {
                if (CIP != null)
                {
                    SessionProduction.SessionProductionArrets.Remove(CIP);
                    db.DeleteObject(CIP);
                }

            }

            var rincageFinProd = SessionProduction.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.RinçageFinProd);
            if (chRinçageFinSession.Checked)
            {
                if (rincageFinProd == null)
                {
                    SessionProduction.SessionProductionArrets.Add(new SessionProductionArret()
                    {
                        TypeArret = (int)EnTypeArret.RinçageFinProd,
                        HeureDebut = dtRinçageFinProductionD.Value,
                        HeureFin = dtRinçageFinProductionF.Value,
                        DureeMinutes = Tools.GetMinutesBetween2Times(dtRinçageFinProductionF.Value, dtRinçageFinProductionD.Value),
                        Cause = "Rinçage Fin Production",
                    });

                }
                else
                {
                    rincageFinProd.HeureDebut = dtRinçageFinProductionD.Value;
                    rincageFinProd.HeureFin = dtRinçageFinProductionF.Value;
                    rincageFinProd.DureeMinutes = Tools.GetMinutesBetween2Times(dtRinçageFinProductionF.Value, dtRinçageFinProductionD.Value);
                }
            }
            else
            {
                if (rincageFinProd != null)
                {
                    SessionProduction.SessionProductionArrets.Remove(rincageFinProd);
                    db.DeleteObject(rincageFinProd);
                }
            }


            #region removed
            /*
            if (FrArret.IsModifiedMaintenance)
            {
                var arrets = SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance).ToList();
                foreach (var arret in arrets)
                {
                    SessionProduction.SessionProductionArrets.Remove(arret);
                    db.DeleteObject(arret);
                }
                List<SessionProductionArret> arrtesMaintenance = null;
                if (SessionProduction == null)
                {
                    arrtesMaintenance = FrArret.ListArretsAnnonyms;
                }
                else
                {
                    arrtesMaintenance = SessionProduction.SessionProductionArrets.ToList();
                }

                foreach (var arret in arrtesMaintenance)
                {
                    if (arret.EntityState == EntityState.Added)
                    {
                        SessionProduction.SessionProductionArrets.Add(arret);
                    }

                }

            }

            if (FrArret.IsModifiedAutre)
            {
                var arrets = SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Autre).ToList();
                foreach (var arret in arrets)
                {
                    SessionProduction.SessionProductionArrets.Remove(arret);
                    db.DeleteObject(arret);
                }

                foreach (var arret in arrtesAutres)
                {
                    SessionProduction.SessionProductionArrets.Add(arret);
                }

            }
            */
            #endregion


            SessionProduction.HeureFinProduction = dtFinDeProd.Value;
            SetSessisonDate(); // tres important <--------------------------------------------------------


            SessionProduction.TempsTotalArret = txtTotalArret.Text.GetTotalMinutesFromHeureTxt();
            SessionProduction.TempsTotalTravail = txtHeureTotalTravail.Text.GetTotalMinutesFromHeureTxt();
            SessionProduction.TempsNetProduction = txtHeureNetProd.Text.GetTotalMinutesFromHeureTxt();
        }
        private void UpdateSessionQuantite()
        {
            
            //----- A
            ModifiedLotAPalette = SessionProduction.NombrePalettesLotA != txtPaletteA.Text.ParseToInt();
            ModifiedLotAFardeau = SessionProduction.NombreBouteillesLotA != txtBouteilleA.Text.ParseToInt();
            SessionProduction.NombrePalettesLotA = txtPaletteA.Text.ParseToInt();
            SessionProduction.NombreFardeauxLotA = txtFardA.Text.ParseToInt();
            SessionProduction.NombreBouteillesLotA = txtBouteilleA.Text.ParseToInt();

            //----- B
            ModifiedLotBFardeau = SessionProduction.NombreFardeauxLotB != txtFardB.Text.ParseToInt();
            ModifiedLotBPalette = SessionProduction.NombrePalettesLotB != txtPaletteB.Text.ParseToInt();
            SessionProduction.NombrePalettesLotB = txtPaletteB.Text.ParseToInt();
            SessionProduction.NombreFardeauxLotB = txtFardB.Text.ParseToInt();
            SessionProduction.NombreBouteillesLotB = txtBouteilleB.Text.ParseToInt();

            //----- C
            ModifiedLotCFardeau = SessionProduction.NombreFardeauxLotC != txtFardC.Text.ParseToInt();
            ModifiedLotCPalette = SessionProduction.NombrePalettesLotC != txtPaletteC.Text.ParseToInt();
            SessionProduction.NombrePalettesLotC = txtPaletteC.Text.ParseToInt();
            SessionProduction.NombreFardeauxLotC = txtFardC.Text.ParseToInt();
            SessionProduction.NombreBouteillesLotC = txtBouteilleC.Text.ParseToInt();

            //----- D
            ModifiedLotDFardeau = SessionProduction.NombreFardeauxLotD != txtFardD.Text.ParseToInt();
            ModifiedLotDPalette = SessionProduction.NombrePalettesLotD != txtPaletteD.Text.ParseToInt();
            SessionProduction.NombrePalettesLotD = txtPaletteD.Text.ParseToInt();
            SessionProduction.NombreFardeauxLotD = txtFardD.Text.ParseToInt();
            SessionProduction.NombreBouteillesLotD = txtBouteilleD.Text.ParseToInt();

            //----- E
            ModifiedLotEFardeau = SessionProduction.NombreFardeauxLotE != txtFardE.Text.ParseToInt();
            ModifiedLotEPalette = SessionProduction.NombrePalettesLotE != txtPaletteE.Text.ParseToInt();
            SessionProduction.NombrePalettesLotE = txtPaletteE.Text.ParseToInt();
            SessionProduction.NombreFardeauxLotE = txtFardE.Text.ParseToInt();
            SessionProduction.NombreBouteillesLotE = txtBouteilleE.Text.ParseToInt();

            //----- F
            ModifiedLotFFardeau = SessionProduction.NombreFardeauxLotF != txtFardF.Text.ParseToInt();
            ModifiedLotFPalette = SessionProduction.NombrePalettesLotF != txtPaletteF.Text.ParseToInt();
            SessionProduction.NombrePalettesLotF = txtPaletteF.Text.ParseToInt();
            SessionProduction.NombreFardeauxLotF = txtFardF.Text.ParseToInt();
            SessionProduction.NombreBouteillesLotF = txtBouteilleF.Text.ParseToInt();


            //----- TOTAL
            SessionProduction.NombreDeConge = txtNbrConge.Text.ParseToInt();

            SessionProduction.NombrePalettesTotal = txtPaletteTotal.Text.ParseToInt();
            SessionProduction.NombreFardeauxTotal = txtFardeauxTotal.Text.ParseToInt();
            SessionProduction.NombreBouteillesTotal = txtBouteillesTotal.Text.ParseToInt();
        }



        private bool IsValidAll()
        {
            string message = "";
            if (IsSessionArret)
            {
                if (txtTotalArret.Text == "")
                {
                    message = "\nVeillez verifier les heures d'arrets!";
                    tabControl.SelectedTab = tpArretes;
                }
            }
            else
            {


                if (cbGoutProduit.SelectedIndex == 0)
                {
                    message += "Selectionner un type produit!";
                }
                else
                {

                    if (txtBouteillesTotal.Text.ParseToInt(false) == null)
                    {
                        message += "\nNombre de bouteilles invalide!";
                        tabControl.SelectedTab = tpQuantiteProduite;
                    }

                    if (txtHeureNetProd.Text == "")
                    {
                        message += "\nVeillez verifier les heures de production!";
                        tabControl.SelectedTab = tpArretes;
                    }


                    if (txtTotalArret.Text == "")
                    {
                        message += "\nVeillez verifier les heures d'arrets!";
                        tabControl.SelectedTab = tpArretes;
                    }

                    if (txtNbrConge.Text.ParseToInt() == null)
                    {
                        message += "\nVeillez verifier le nobre de conges!";
                    }
                }
            }
            if (message != "")
            {
                this.ShowWarning(message);
                return false;
            }

            return true;
        }
        public Production Production { get; set; }
        private void txtPalette_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalBouteille();
        }


        private void UpdateTotalBouteille()
        {
            int totalBouteillesA = 0;

            var paletteA = txtPaletteA.Text.ParseToInt();
            var fardeauA = txtFardA.Text.ParseToInt();


            if (paletteA != null)
            {
                totalBouteillesA += (paletteA * Production.Format.FardeauParPalette * Production.Format.BouteillesParFardeau).Value;

            }


            if (fardeauA != null)
            {
                totalBouteillesA += (fardeauA * Production.Format.BouteillesParFardeau).Value;

            }

            if (totalBouteillesA != 0)
            {
                txtBouteilleA.Text = totalBouteillesA.ToString();
            }
            else
            {
                txtBouteilleA.Text = "";
            }

            ///////////////////////////////////////////////////////////////// B
            int totalBouteillesB = 0;
            var paletteB = txtPaletteB.Text.ParseToInt();
            var fardeauB = txtFardB.Text.ParseToInt();

            if (paletteB != null)
            {
                totalBouteillesB += (paletteB * Production.Format.FardeauParPalette * Production.Format.BouteillesParFardeau).Value;
            }


            if (fardeauB != null)
            {
                totalBouteillesB += (fardeauB * Production.Format.BouteillesParFardeau).Value;
            }

            if (totalBouteillesB != 0)
            {
                txtBouteilleB.Text = totalBouteillesB.ToString();
            }
            else
            {
                txtBouteilleB.Text = "";
            }
            ///////////////////////////////////////////////////////////////// C
            int totalBouteillesC = 0;
            var paletteC = txtPaletteC.Text.ParseToInt();
            var fardeauC = txtFardC.Text.ParseToInt();


            if (paletteC != null)
            {
                totalBouteillesC += (paletteC * Production.Format.FardeauParPalette * Production.Format.BouteillesParFardeau).Value;

            }


            if (fardeauC != null)
            {
                totalBouteillesC += (fardeauC * Production.Format.BouteillesParFardeau).Value;

            }

            if (totalBouteillesC != 0)
            {
                txtBouteilleC.Text = totalBouteillesC.ToString();
            }
            else
            {
                txtBouteilleC.Text = "";
            }

            ///////////////////////////////////////////////////////////////// D

            int totalBouteillesD = 0;
            var paletteD = txtPaletteD.Text.ParseToInt();
            var fardeauD = txtFardD.Text.ParseToInt();


            if (paletteD != null)
            {
                totalBouteillesD += (paletteD * Production.Format.FardeauParPalette * Production.Format.BouteillesParFardeau).Value;

            }


            if (fardeauD != null)
            {
                totalBouteillesD += (fardeauD * Production.Format.BouteillesParFardeau).Value;

            }

            if (totalBouteillesD != 0)
            {
                txtBouteilleD.Text = totalBouteillesD.ToString();
            }
            else
            {
                txtBouteilleD.Text = "";
            }
            //-/////////////////////////////////////////////////////////////////// E
            int totalBouteillesE = 0;
            var paletteE = txtPaletteE.Text.ParseToInt();
            var fardeauE = txtFardE.Text.ParseToInt();


            if (paletteE != null)
            {
                totalBouteillesE += (paletteE * Production.Format.FardeauParPalette * Production.Format.BouteillesParFardeau).Value;

            }


            if (fardeauE != null)
            {
                totalBouteillesE += (fardeauE * Production.Format.BouteillesParFardeau).Value;

            }

            if (totalBouteillesE != 0)
            {
                txtBouteilleE.Text = totalBouteillesE.ToString();
            }
            else
            {
                txtBouteilleE.Text = "";
            }

            //---------------------------------------------------------------------
            //////////////////////////////////
            int totalBouteillesF = 0;
            var paletteF = txtPaletteF.Text.ParseToInt();
            var fardeauF = txtFardF.Text.ParseToInt();


            if (paletteF != null)
            {
                totalBouteillesF += (paletteF * Production.Format.FardeauParPalette * Production.Format.BouteillesParFardeau).Value;
            }

            if (fardeauF != null)
            {
                totalBouteillesF += (fardeauF * Production.Format.BouteillesParFardeau).Value;
            }

            if (totalBouteillesF != 0)
            {
                txtBouteilleF.Text = totalBouteillesF.ToString();
            }
            else
            {
                txtBouteilleF.Text = "";
            }
            //-----------------------------------------------------------------------------

            int totalBouteilles = 0;
            var palette = 0;
            var fardeau = 0;

            if (paletteA != null)
            {
                palette += paletteA.Value;
            }
            //-------------------------------------  
            if (paletteB != null)
            {
                palette += paletteB.Value;
            }

            if (paletteC != null)
            {
                palette += paletteC.Value;
            }
            if (paletteD != null)
            {
                palette += paletteD.Value;
            }
            if (paletteE != null)
            {
                palette += paletteE.Value;
            }

            if (paletteF != null)
            {
                palette += paletteF.Value;
            }
            //-------------------------------------
            if (fardeauA != null)
            {
                fardeau += fardeauA.Value;
            }
            //-------------------------------------
            if (fardeauB != null)
            {
                fardeau += fardeauB.Value;
            }

            //-------------------------------------
            if (fardeauC != null)
            {
                fardeau += fardeauC.Value;
            }

            //-------------------------------------
            if (fardeauD != null)
            {
                fardeau += fardeauD.Value;
            }

            //-------------------------------------
            if (fardeauE != null)
            {
                fardeau += fardeauE.Value;
            }

            if (fardeauF != null)
            {
                fardeau += fardeauF.Value;
            }

            totalBouteilles = totalBouteillesA + totalBouteillesB + totalBouteillesC + totalBouteillesD + totalBouteillesE + totalBouteillesF;

            txtPaletteTotal.Text = (palette != 0) ? palette.ToString() : "";
            txtFardeauxTotal.Text = (fardeau != 0) ? fardeau.ToString() : "";
            txtBouteillesTotal.Text = (totalBouteilles != 0) ? totalBouteilles.ToString() : "";



        }


        private void buttonAnnuler_Click(object sender, EventArgs e)
        {

            Dispose();

        }

        private void chLotA_CheckStateChanged(object sender, EventArgs e)
        {
            panLotA.Visible = chLotA.Checked;
            txtPaletteA.Text = txtFardA.Text = "";
        }

        private void chLotB_CheckedChanged(object sender, EventArgs e)
        {
            panLotB.Visible = chLotB.Checked;
            txtPaletteB.Text = txtFardB.Text = "";
        }

        private void chLotC_CheckedChanged(object sender, EventArgs e)
        {
            panLotC.Visible = chLotC.Checked;
            txtPaletteC.Text = txtFardC.Text = "";
        }

        private void chLotD_CheckedChanged(object sender, EventArgs e)
        {
            panLotD.Visible = chLotD.Checked;
            txtPaletteD.Text = txtFardD.Text = "";
        }

        private void chLotE_CheckedChanged(object sender, EventArgs e)
        {
            panLotE.Visible = chLotE.Checked;
            txtPaletteE.Text = txtFardE.Text = "";
        }



        public int CurrentNoPalette { get; set; }

        private void chLotF_CheckedChanged(object sender, EventArgs e)
        {
            panLotF.Visible = chLotF.Checked;
            txtPaletteF.Text = txtFardF.Text = "";
        }

        private void btParcourrirMaintenance_Click(object sender, EventArgs e)
        {

            new FrArret(db, SessionProduction, AnnonymeArrtesMaintenance, EnTypeArret.Maintenance).ShowDialog();

            if (SessionProduction != null)
            {
                AnnonymeArrtesMaintenance = SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance).ToList();
            }
            /* else
             {
                 AnnonymeArrtesMaintenance.Clear();
                 AnnonymeArrtesMaintenance.AddRange(FrArret.ListArretsAnnonyms);
             }*/

            UpdateArretMaintenance();
        }

        private void btParcourrirAutre_Click(object sender, EventArgs e)
        {
            new FrArret(db, SessionProduction, AnnonymeArrtesAutres, EnTypeArret.Autre).ShowDialog();

            if (SessionProduction != null)
            {
                AnnonymeArrtesAutres = SessionProduction.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Autre).ToList();

            }
            /* else
             {
                 AnnonymeArrtesAutres.Clear();
                 AnnonymeArrtesAutres.AddRange(FrArret.ListArretsAnnonyms);
             }
             */
            UpdateArretAutre();
        }

        private void FrSession_Load(object sender, EventArgs e)
        {

        }

        public List<SessionProductionArret> AnnonymeArrtesMaintenance { get; set; }
        public List<SessionProductionArret> AnnonymeArrtesAutres { get; set; }

        private void chRinçageFinSession_CheckedChanged(object sender, EventArgs e)
        {
            panRinçageFinSession.Visible = chRinçageFinSession.Checked;
            UpdateTotalTemps();
        }

        private void txtNbrConge_TextChanged(object sender, EventArgs e)
        {
            UpdateProdTheorique();
        }

        private void UpdateProdTheorique()
        {
            var nbrConge = txtNbrConge.Text.Trim().ParseToInt();
            var selectedGoutProduit = (GoutProduit)cbGoutProduit.SelectedItem;
            if (nbrConge != null && selectedGoutProduit != null)
            {
                txtThe.Text = (nbrConge * selectedGoutProduit.ProdTeoriqueLittreParConge).ToString();
            }
            else
            {
                txtThe.Text = "";
            }
        }


    }
}
