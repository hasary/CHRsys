﻿namespace FZBGsys.NsProduction
{
    partial class FrEtiquette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbGoutProduit = new System.Windows.Forms.ComboBox();
            this.cbTypeEtiquette = new System.Windows.Forms.ComboBox();
            this.txtNbrEtiquette = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            this.dtHeureDebut = new System.Windows.Forms.DateTimePicker();
            this.dtHeureFin = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(263, 18);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(747, 391);
            this.dgv.TabIndex = 8;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(705, 415);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(304, 43);
            this.xpannel1.TabIndex = 11;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(36, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(164, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(263, 425);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(124, 425);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 19;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(73, 16);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(167, 24);
            this.cbFormat.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbGoutProduit);
            this.groupBox1.Controls.Add(this.cbTypeEtiquette);
            this.groupBox1.Controls.Add(this.txtNbrEtiquette);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(15, 180);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(241, 239);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Etiquette";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(21, 146);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(205, 24);
            this.cbFournisseur.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Fournisseur:";
            // 
            // cbGoutProduit
            // 
            this.cbGoutProduit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGoutProduit.FormattingEnabled = true;
            this.cbGoutProduit.Location = new System.Drawing.Point(83, 84);
            this.cbGoutProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbGoutProduit.Name = "cbGoutProduit";
            this.cbGoutProduit.Size = new System.Drawing.Size(145, 24);
            this.cbGoutProduit.TabIndex = 1;
            // 
            // cbTypeEtiquette
            // 
            this.cbTypeEtiquette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeEtiquette.FormattingEnabled = true;
            this.cbTypeEtiquette.Location = new System.Drawing.Point(123, 33);
            this.cbTypeEtiquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeEtiquette.Name = "cbTypeEtiquette";
            this.cbTypeEtiquette.Size = new System.Drawing.Size(104, 24);
            this.cbTypeEtiquette.TabIndex = 1;
            // 
            // txtNbrEtiquette
            // 
            this.txtNbrEtiquette.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNbrEtiquette.Location = new System.Drawing.Point(140, 198);
            this.txtNbrEtiquette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNbrEtiquette.Name = "txtNbrEtiquette";
            this.txtNbrEtiquette.Size = new System.Drawing.Size(85, 22);
            this.txtNbrEtiquette.TabIndex = 5;
            this.txtNbrEtiquette.TextChanged += new System.EventHandler(this.txtNbrEtiquette_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Produit:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Nbr Etiquettes:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Type Etiquette:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(409, 418);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Total Général:";
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(537, 418);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(24, 17);
            this.labTotal.TabIndex = 8;
            this.labTotal.Text = "00";
            // 
            // dtHeureDebut
            // 
            this.dtHeureDebut.CustomFormat = "dd/MM/yyyy  HH:mm ";
            this.dtHeureDebut.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtHeureDebut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHeureDebut.Location = new System.Drawing.Point(41, 28);
            this.dtHeureDebut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtHeureDebut.MaxDate = new System.DateTime(2200, 12, 25, 0, 0, 0, 0);
            this.dtHeureDebut.MinDate = new System.DateTime(2000, 5, 1, 0, 0, 0, 0);
            this.dtHeureDebut.Name = "dtHeureDebut";
            this.dtHeureDebut.Size = new System.Drawing.Size(184, 22);
            this.dtHeureDebut.TabIndex = 29;
            this.dtHeureDebut.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // dtHeureFin
            // 
            this.dtHeureFin.CustomFormat = "dd/MM/yyyy  HH:mm ";
            this.dtHeureFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtHeureFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHeureFin.Location = new System.Drawing.Point(41, 73);
            this.dtHeureFin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtHeureFin.MaxDate = new System.DateTime(2020, 5, 2, 0, 0, 0, 0);
            this.dtHeureFin.MinDate = new System.DateTime(2000, 5, 1, 0, 0, 0, 0);
            this.dtHeureFin.Name = "dtHeureFin";
            this.dtHeureFin.Size = new System.Drawing.Size(184, 22);
            this.dtHeureFin.TabIndex = 29;
            this.dtHeureFin.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtHeureDebut);
            this.groupBox2.Controls.Add(this.dtHeureFin);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(15, 57);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(243, 114);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Heure de Production";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "à";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "de";
            // 
            // FrEtiquette
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 470);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labTotal);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrEtiquette";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consomation Etiquette ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrEtiquette_FormClosing);
            this.Load += new System.EventHandler(this.FrEtiquette_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTypeEtiquette;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNbrEtiquette;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.DateTimePicker dtHeureDebut;
        private System.Windows.Forms.ComboBox cbGoutProduit;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtHeureFin;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
    }
}