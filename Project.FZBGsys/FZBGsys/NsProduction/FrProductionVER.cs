﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

using FZBGsys.NsTools;
using FZBGsys.NsReports.Production;
using FZBGsys.NsReports;


namespace FZBGsys.NsProduction
{
    public partial class FrProductionVER : Form
    {
        #region not used
        /*
        public static void RestoreMT(Production Production, ModelEntities db)
        {
            Ressource Ressource;
            #region bouchon
            var ProductionBouchons = Production.ProductionBouchons.ToList();

            foreach (var productionBouchon in ProductionBouchons)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Bouchon,
                    TypeBouchonID = productionBouchon.TypeBouchonID,
                    CouleurBouchon = productionBouchon.CouleurBouchon,
                    FournisseurID = productionBouchon.FournisseurID,
                };

                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = productionBouchon.NombreCartons,
                    Quantite = productionBouchon.QuantitePiece,

                },

               db, Production.Date);

            }
            #endregion
            #region soufflage
            var ProductionSoufflages = Production.ProductionSoufflages.ToList();

            foreach (var porudcionSoufflage in ProductionSoufflages)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Preforme,
                    TypePreformeID = porudcionSoufflage.PreformeGammeID,
                    FournisseurID = porudcionSoufflage.FournisseurID,

                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = 1, // One Box
                    Quantite = porudcionSoufflage.QuantitePieces,

                }, db, Production.Date);

            }
            #endregion
            #region Etiquettes
            var ProductionEtiquettes = Production.ProductionEtiquettes.ToList();

            foreach (var ProductionEtiquette in ProductionEtiquettes)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Etiquette,
                    TypeArromeID = ProductionEtiquette.TypeArromeID,
                    TypeEtiquetteID = ProductionEtiquette.TypeEtiquetteID,
                    FournisseurID = ProductionEtiquette.FournisseurID,

                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = 1, // One Carton
                    Quantite = ProductionEtiquette.QuantitePiece,

                }, db, Production.Date);

            }
            #endregion
            #region FilmThermo
            var ProductionFilmThermos = Production.ProductionFilmThermoes.ToList();

            foreach (var ProductionFilmThermo in ProductionFilmThermos)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Film_Thermofusible,
                    FournisseurID = ProductionFilmThermo.FournisseurID,
                    EpaisseurFilmThermo = ProductionFilmThermo.Epaisseur,
                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = 1, // One Bobine
                    Quantite = ProductionFilmThermo.Poids,

                }, db, Production.Date);

            }
            #endregion
            #region  Siropherie
            var ProductionSiroperies = Production.ProductionSiroperies.ToList();

            foreach (var ProductionSiroperie in ProductionSiroperies)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = ProductionSiroperie.TypeRessourceID, // aha!

                    TypeArromeID = ProductionSiroperie.TypeArromeID,
                    TypeColorantID = ProductionSiroperie.TypeColorantID,
                    FournisseurID = ProductionSiroperie.FournisseurID,

                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = ProductionSiroperie.QuantitePiece, // dependOnTypeRessourceID
                    Quantite = ProductionSiroperie.QuantiteUM,

                }, db, Production.Date);

            }

            #endregion

        }

        public static void ProcessMT(Production Production, ModelEntities db)
        {
            Ressource Ressource;
            #region bouchon
            var ProductionBouchons = Production.ProductionBouchons.ToList();

            foreach (var productionBouchon in ProductionBouchons)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Bouchon,
                    TypeBouchonID = productionBouchon.TypeBouchonID,
                    CouleurBouchon = productionBouchon.CouleurBouchon,
                    FournisseurID = productionBouchon.FournisseurID,
                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = productionBouchon.NombreCartons,
                    Quantite = productionBouchon.QuantitePiece,

                },

               db, Production.Date);

            }
            #endregion
            #region soufflage
            var ProductionSoufflages = Production.ProductionSoufflages.ToList();

            foreach (var porudcionSoufflage in ProductionSoufflages)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Preforme,
                    TypePreformeID = porudcionSoufflage.PreformeGammeID,
                    FournisseurID = porudcionSoufflage.FournisseurID,

                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = 1, // One Box
                    Quantite = porudcionSoufflage.QuantitePieces,

                }, db, Production.Date);

            }
            #endregion
            #region Etiquettes
            var ProductionEtiquettes = Production.ProductionEtiquettes.ToList();

            foreach (var ProductionEtiquette in ProductionEtiquettes)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Etiquette,
                    TypeArromeID = ProductionEtiquette.TypeArromeID,
                    TypeEtiquetteID = ProductionEtiquette.TypeEtiquetteID,
                    FournisseurID = ProductionEtiquette.FournisseurID,

                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = 1, // One Carton
                    Quantite = ProductionEtiquette.QuantitePiece,

                }, db, Production.Date);

            }
            #endregion
            #region FilmThermo
            var ProductionFilmThermos = Production.ProductionFilmThermoes.ToList();

            foreach (var ProductionFilmThermo in ProductionFilmThermos)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = (int)EnTypeRessource.Film_Thermofusible,
                    FournisseurID = ProductionFilmThermo.FournisseurID,
                    EpaisseurFilmThermo = ProductionFilmThermo.Epaisseur,
                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = 1, // One Bobine
                    Quantite = ProductionFilmThermo.Poids,

                }, db, Production.Date);

            }
            #endregion
            #region  Siropherie
            var ProductionSiroperies = Production.ProductionSiroperies.ToList();

            foreach (var ProductionSiroperie in ProductionSiroperies)
            {
                Ressource = new Ressource()
                {
                    TypeRessourceID = ProductionSiroperie.TypeRessourceID, // aha!

                    TypeArromeID = ProductionSiroperie.TypeArromeID,
                    TypeColorantID = ProductionSiroperie.TypeColorantID,
                    FournisseurID = ProductionSiroperie.FournisseurID,

                };

                FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(new StockMatiere()
                {
                    Ressource = Ressource,
                    Piece = ProductionSiroperie.QuantitePiece, // dependOnTypeRessourceID
                    Quantite = ProductionSiroperie.QuantiteUM,

                }, db, Production.Date);

            }

            #endregion

        }

        public bool isUpdSoufflage { get; set; }
        public bool usUpdBouchon { get; set; }
        public bool isUpdEtiquette { get; set; }
        public bool isUpdFilmThermo { get; set; }
        public bool isUpdSiroperie { get; set; }
        */

        #endregion

        ModelEntities db = new ModelEntities();

        private Production Production { set; get; }

        public FrProductionVER()
        {
            Production = new Production();
            Production.SectionID = (int)EnSection.Unité_Verre;
            InitializeComponent();
            InitialiseControls();

        }
        private void SetProductionDate()
        {
            var date = dtDate.Value.Date;
            if (Production.HeureDemarrage == null)
            {
                return; // new Production
            }

            var anDateDemarrage = Production.HeureDemarrage.Value.Date;
            var anDateFinProd = Production.HeureFinDeProd.Value.Date;

            if (anDateDemarrage == anDateFinProd)
            {
                Production.HeureDemarrage = Tools.GetFillDateFromDateTime(date, Production.HeureDemarrage.Value.TimeOfDay);//.SetDateKeepTime(date);
                Production.HeureFinDeProd = Tools.GetFillDateFromDateTime(date, Production.HeureFinDeProd.Value.TimeOfDay);//.SetDateKeepTime(date); //.SetDateKeepTime(date);
            }
            else
            {
                Production.HeureDemarrage = Tools.GetFillDateFromDateTime(date, Production.HeureDemarrage.Value.TimeOfDay);//.SetDateKeepTime(date);
                Production.HeureFinDeProd = Tools.GetFillDateFromDateTime(date.AddDays(1), Production.HeureFinDeProd.Value.TimeOfDay);//.SetDateKeepTime(date); //.SetDateKeepTime(date);
            }

            foreach (var session in Production.SessionProductions)
            {
                session.HeureDemarrage = (session.HeureDemarrage.Value.Date == anDateDemarrage) ?
                    Tools.GetFillDateFromDateTime(date, session.HeureDemarrage.Value.TimeOfDay) :
                    Tools.GetFillDateFromDateTime(date.AddDays(1), session.HeureDemarrage.Value.TimeOfDay);

                session.HeureFinProduction = (session.HeureFinProduction.Value.Date == anDateDemarrage) ?
                    Tools.GetFillDateFromDateTime(date, session.HeureFinProduction.Value.TimeOfDay) :
                    Tools.GetFillDateFromDateTime(date.AddDays(1), session.HeureFinProduction.Value.TimeOfDay);

                foreach (var arret in session.SessionProductionArrets)
                {
                    arret.HeureDebut = (arret.HeureDebut.Value.Date == anDateDemarrage) ?
                        Tools.GetFillDateFromDateTime(date, arret.HeureDebut.Value.TimeOfDay) :
                        Tools.GetFillDateFromDateTime(date.AddDays(1), arret.HeureDebut.Value.TimeOfDay);

                    arret.HeureFin = (arret.HeureFin.Value.Date == anDateDemarrage) ?
                        Tools.GetFillDateFromDateTime(date, arret.HeureFin.Value.TimeOfDay) :
                        Tools.GetFillDateFromDateTime(date.AddDays(1), arret.HeureFin.Value.TimeOfDay);
                }

            }


        }

        private void InitialiseControls()
        {
            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0 && p.SectionProductionID == (int)EnSection.Unité_Verre).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;
            cbFormat.SelectedIndex = 0;

            //---------------------- Equipe 
            cbEquipe.Items.Add(new Equipe { ID = 0, Nom = "" });
            cbEquipe.Items.AddRange(db.Equipes.Where(p => p.ID > 0).ToArray());
            cbEquipe.DisplayMember = "Nom";
            cbEquipe.ValueMember = "ID";
            cbEquipe.DropDownStyle = ComboBoxStyle.DropDownList;
            cbEquipe.SelectedIndex = 0;

            //---------------------- Operand 
            cbOperant.Items.Add(new Employer { Matricule = "0", Nom = "" });
            cbOperant.Items.AddRange(db.Employers.Where(p => p.SectionID == (int)EnSection.Unité_PET).ToArray()); // always
            cbOperant.DisplayMember = "Nom";
            cbOperant.ValueMember = "Matricule";
            cbOperant.DropDownStyle = ComboBoxStyle.DropDownList;
            cbOperant.SelectedIndex = 0;
        }


        public FrProductionVER(int ProductionID)
        {//-------------3105
            InitializeComponent();
            InitialiseControls();
            var production = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            if (production == null)
            {
                this.ShowError("impossible de trouver les donnés à charger");
                Dispose();
            }

            this.Production = production;
            dtDate.Value = Production.HeureDemarrage.Value;
            InputProduction();
        }




        private void InputProduction()
        {
            dtDate.Value = Production.HeureDemarrage.Value;
            cbFormat.SelectedItem = Production.Format;
            txtCadence.Text = Production.Cadence.ToString();
            cbEquipe.SelectedItem = Production.Equipe;
            cbOperant.SelectedItem = Production.Employer;

            txtObservationSession.Text = Production.ObservationSessions;
            txtObservationSuiviMatiere.Text = Production.ObservationMatiere;

            //------------------------------- matieres
            RefreshGridMatiere();
            RefreshGridSession();

        }



        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (cbEquipe.SelectedIndex != 0 && cbOperant.SelectedIndex != 0 && cbFormat.SelectedIndex != 0 && txtCadence.Text.Trim().ParseToInt() != null)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //  Production.HeureDemarrage = dtDate.Value.Date;

                if (Production.SessionProductions.Count == 0)
                {
                    new FZBGsys.NsProduction.FrSession(false,Production).ShowDialog();
                }
                else
                {
                    var LastSession = Production.SessionProductions.OrderByDescending(p => p.HeureDemarrage).First();
                    var heureDebut = LastSession.HeureFinProduction;// Tools.GetFillDateFromDateHour(Production.Date, LastSession.HeureFinProduction);

                    new FZBGsys.NsProduction.FrSession(false,Production, heureDebut).ShowDialog();
                }

                UpdateTotalSession();
                RefreshGridSession();
            }
            else
            {
                this.ShowInformation("Selectionner d'abord une Equipe, un Operant et un Format, et Verifier la Cadence");
            }
        }

        private void UpdateTotalSession()
        {
            var SessionProductions = Production.SessionProductions.ToList();
            if (SessionProductions.Count == 0)
            {
                return;
            }
            var Observation = "";
            if (SessionProductions.Count > 1)
            {
                for (int i = 0; i < SessionProductions.Count - 1; i++)
                {
                    foreach (var arret in SessionProductions.ElementAt(i).SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance || p.TypeArret == (int)EnTypeArret.Autre))
                    {
                        Observation += arret.Cause + " de " + arret.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arret.HeureFin.Value.ToString(@"HH\:mm") + "\r\n";
                    }
                    var lastArret = SessionProductions.ElementAt(i).SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.RinçageFinProd);
                    DateTime? heureFin = (lastArret == null) ? SessionProductions.ElementAt(i).HeureFinProduction : lastArret.HeureDebut;

                    Observation += "Fin de production " + SessionProductions.ElementAt(i).GoutProduit.Nom + " à " + heureFin.Value.ToString(@"HH\:mm") +
                        " et démarrage " + SessionProductions.ElementAt(i + 1).GoutProduit.Nom + " à " + SessionProductions.ElementAt(i + 1).HeureDemarrage.Value.ToString(@"HH\:mm") + "\r\n";

                }
            }

            {
                foreach (var arret in SessionProductions.Last().SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance || p.TypeArret == (int)EnTypeArret.Autre))
                {
                    Observation += arret.Cause + " de " + arret.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arret.HeureFin.Value.ToString(@"HH\:mm") + "\r\n";
                }

            }

            /*var ObservationArrets = "";

            foreach (var session in SessionProductions)
            {
                foreach (var arret in session.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance || p.TypeArret == (int)EnTypeArret.Autre))
                {
                    ObservationArrets += arret.Cause + " de " + arret.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arret.HeureFin.Value.ToString(@"HH\:mm") + "\r\n";
                }
            }*/


            txtObservationSession.Text = "";
            if (Observation != "")
            {
                txtObservationSession.Text += Observation;
            }

            /* if (ObservationArrets != "")
             {
                 txtObservationSession.Text += ObservationArrets;
             }*/

            var FirstSession = Production.SessionProductions.OrderBy(p => p.HeureDemarrage).First();
            var LastSession = Production.SessionProductions.OrderByDescending(p => p.HeureDemarrage).First();


            Production.HeureDemarrage = FirstSession.HeureDemarrage;
            Production.HeureFinDeProd = LastSession.HeureFinProduction;

            Production.TempsTotalTravail = Production.SessionProductions.Sum(p => p.TempsTotalTravail);
            Production.TempsArrets = Production.SessionProductions.Sum(p => p.TempsTotalArret);
            Production.TempsNetProduction = Production.SessionProductions.Sum(p => p.TempsNetProduction);

            //////--------------------------------------

            Production.NombrePalettes = Production.SessionProductions.Sum(p => p.NombrePalettesTotal);
            Production.NombreFardeaux = Production.SessionProductions.Sum(p => p.NombreFardeauxTotal);
            Production.TotalBouteilles = Production.SessionProductions.Sum(p => p.NombreBouteillesTotal);
            //////--------------------------------------



            var cadence = txtCadence.Text.ParseToInt();



            Production.Efficience = decimal.Round(
                (decimal)Production.TotalBouteilles /
                ((decimal)cadence * ((decimal)Production.TempsNetProduction / 60)) * 100, 2);


            Production.Performance = decimal.Round(
                (decimal)Production.TotalBouteilles /
                ((decimal)cadence * ((decimal)Production.TempsTotalTravail / 60)) * 100, 2);


        }
        private void btBoutvideVide_Click(object sender, EventArgs e)
        {//--------------------------------0106OK
            if (cbFormat.SelectedIndex != 0)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //   Production.HeureDemarrage = dtDate.Value;
                new FrBouteilleVide(Production, db).ShowDialog();
                RefreshGridMatiere();

            }
            else
            {
                this.ShowInformation("Selectionner d'abord un Format");

            }


        }

        private void RefreshGridMatiere()
        {//----------------------------------------------------- 0106OK
            if (Production.NombreVerre != null) txtVerre.Text = Production.NombreVerre.ToString() + " bouteilles (" + Production.VerreNonConform + " N.C)";
            if (Production.NombreBouchon != null) txtBouchon.Text = Production.NombreBouchon.ToString() + " bouchons";
            if (Production.NombreEtiquette != null) txtEtiquettes.Text = Production.NombreEtiquette.ToString() + " Etiquettes";
            if (Production.NombreBarquette != null) txtBarquette.Text = Production.NombreBarquette.ToString() + " Barquettes";
            if (Production.NombreBobineFilmeThermo != null) txtFilmThermo.Text = Production.NombreBobineFilmeThermo.ToString() + " bobines (" + Production.PoidsFilmThermo.ToString() + " kg)";
            txtSiropherie.Text = "[multiples matières]";
            //txtSiropherie.Text = " Sucre " + Production.ProductionSiroperies.Where(p => p.TypeRessourceID == (int)EnTypeRessource.Sucre).Sum(p => p.QuantitePiece).ToString() + " sacs ......";
            VerifyMatieres();

        }

        private void VerifyMatieres()
        {
            var goutsproduit = Production.SessionProductions.Select(p => p.GoutProduitID).ToList();
            var ProductionEtiquettes = Production.ProductionEtiquettes.ToList();
            var ProductionSiroperieArromes = Production.ProductionSiroperies.Where(p => p.TypeRessourceID == (int)EnTypeRessource.Arrome).ToList();

            bool inexistGoutProduit = false;
            foreach (var ProductionEtiquette in ProductionEtiquettes)
            {
                inexistGoutProduit |= !goutsproduit.Contains(ProductionEtiquette.GoutProduitID);
            }
            if (inexistGoutProduit)
            {
                txtErrEtiquette.Text = "Certains type produits ne sont pas conformes avec les sessions de production";
            }
            else
            {
                txtErrEtiquette.Text = "";
            }


            bool inexistArrome = false;
            foreach (var ProductionArromes in ProductionSiroperieArromes)
            {
                var gouts = ProductionArromes.TypeArrome.GoutProduits.Select(p => p.ID);
                foreach (var gout in gouts)
                {
                    inexistArrome |= !goutsproduit.Contains(gout);
                }

            }
            if (inexistArrome)
            {
                txtErrSiroperie.Text = "Certains type produits ne sont pas conformes avec les sessions de production";
            }
            else
            {
                txtErrSiroperie.Text = "";
            }


        }

        private void btBouchon_Click(object sender, EventArgs e)
        {//-------------0106OK
            if (cbFormat.SelectedIndex != 0 && cbEquipe.SelectedIndex != 0)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //   Production.HeureDemarrage = dtDate.Value;
                new FrBouchon(Production, db).ShowDialog();
                RefreshGridMatiere();

            }
            else
            {
                this.ShowInformation("Selectionner d'abord un Format & une Equipe");

            }
        }

        private void btEtiquettes_Click(object sender, EventArgs e)
        {//-------------0106OK
            if (cbFormat.SelectedIndex != 0 && cbEquipe.SelectedIndex != 0)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //    Production.HeureDemarrage = dtDate.Value;
                new FrEtiquette(Production, db).ShowDialog();
                RefreshGridMatiere();

            }
            else
            {
                this.ShowInformation("Selectionner d'abord un Format & une Equipe");

            }
        }

        private void btFilmThermo_Click(object sender, EventArgs e)
        {//-------------0106OK
            if (cbFormat.SelectedIndex != 0 && cbEquipe.SelectedIndex != 0)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //  Production.HeureDemarrage = dtDate.Value;
                new FrFilmThermo(Production, db).ShowDialog();
                RefreshGridMatiere();

            }
            else
            {
                this.ShowInformation("Selectionner d'abord un Format & une Equipe");

            }
        }

        private void btSiroperie_Click(object sender, EventArgs e)
        {//-------------0106OK
            if (cbFormat.SelectedIndex != 0 && cbEquipe.SelectedIndex != 0)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //  Production.HeureDemarrage = dtDate.Value;
                new FrSiroperie(Production, db).ShowDialog();
                RefreshGridMatiere();

            }
            else
            {
                this.ShowInformation("Selectionner d'abord un Format & une Equipe");

            }
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("\nEtes vous sure de vouloir fermer sans enregistrer ?"))
            {
                Dispose();
            }
        }

        private void btModifierSession_Click(object sender, EventArgs e)
        {
            var selectedSessionIndex = dgvSession.GetSelectedIndex();
            if (selectedSessionIndex != null)
            {
                var SelectedSession = Production.SessionProductions.ElementAt(selectedSessionIndex.Value);

                new FrSession(false,SelectedSession, db).ShowDialog();
                ModifierSession = true;
                UpdateTotalSession();
                RefreshGridSession();
            }


        }

        private void RefreshGridSession()
        {
            dgvSession.DataSource = Production.SessionProductions.ToList().Select(p => new
            {
                Produit = p.GoutProduit.Nom,
                Debut = p.HeureDemarrage.Value.ToString(@"dd/MM/yyyy HH\:mm"),
                Fin = p.HeureFinProduction.Value.ToString(@"dd/MM/yyyy HH\:mm"),
                Nette = p.TempsNetProduction.MinutesToHoursMinutesTxt(),
                Arrets = p.TempsTotalArret.MinutesToHoursMinutesTxt(),
                Total = p.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                Quantite = ((p.NombrePalettesTotal != null) ? p.NombrePalettesTotal + " P   " : "") + ((p.NombreFardeauxTotal != null) ? p.NombreFardeauxTotal + " F" : ""),
                Bouteilles = p.NombreBouteillesTotal

            }).ToList();

            List<string[]> rep = new List<string[]>();

            txtHeureTotalTravail.Text = Production.TempsTotalTravail.MinutesToHoursMinutesTxt();
            txtHeureNetProduction.Text = Production.TempsNetProduction.MinutesToHoursMinutesTxt();

            txtTotalArret.Text = Production.TempsArrets.MinutesToHoursMinutesTxt();
            txtProductionBouteille.Text = Production.TotalBouteilles.ToString();

            txtEfficience.Text = Production.Efficience.ToString() + " %";
            txtPerformance.Text = Production.Performance.ToString() + " %";

            txtDemarage.Text = (Production.HeureFinDeProd != null) ? Production.HeureDemarrage.Value.ToString(@"HH\:mm") : "";
            txtFinProd.Text = (Production.HeureFinDeProd != null) ? Production.HeureFinDeProd.Value.ToString(@"HH\:mm") : "";

            VerifyMatieres();

        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {

            if (Production.SessionProductions.Count != 0)
            {
                SaveProdcution(); // with dispose 
            }
            else
            {
                this.ShowError("vous devez Ajouter une session");
            }

        }
        bool ModifierSession =false;
        bool AddProduction = false;
        private void SaveProdcution(bool Dispose = true)
        {
            var equipe = (Equipe)cbEquipe.SelectedItem;
            var existing = db.Productions.ToList().SingleOrDefault(
                p => p.HeureDemarrage.Value.Date == dtDate.Value.Date &&
                p.EquipeID == equipe.ID &&
                p.SectionID == (int)EnSection.Unité_Verre
                );
            if (existing != null)
            {
                if (existing.ID != Production.ID)
                {
                    this.ShowError("Cette journée est déja inscrite,\nchoisissez une autre date / Equipe ou selectionner la journée dans Liste de Production si vous souhaiter modifier.");
                    return;
                }

            }
            Production.Cadence = txtCadence.Text.Trim().ParseToInt();
            //     Production.NombreEquipes = txtNombreEquipe.Text.ParseToInt();
            Production.Equipe = equipe;
            Production.Employer = (Employer)cbOperant.SelectedItem;
            Production.InscriptionUID = Tools.CurrentUserID;
            if (Production.Employer.Matricule == "0")
            {
                this.ShowWarning("Selectionner d'abord un Operand");
                return;
            }
            Production.ObservationMatiere = txtObservationSuiviMatiere.Text.Trim();
            Production.ObservationSessions = txtObservationSession.Text.Trim();
            Production.Format = (Format)cbFormat.SelectedItem;
            Production.SectionID = (int)EnSection.Unité_Verre; //------------------------------------- changer pour uca



            // Production.HeureDemarrage = dtDate.Value.Date;
            SetProductionDate();
            UpdateTotalSession();

            var listsession = Production.SessionProductions.ToList();
           
            if (Production.EntityState == EntityState.Added) AddProduction = true;
            FZBGsys.NsStock.StockProduitFini.FrStockProduitFiniManual.stockauto(Production.HeureFinDeProd.Value.Date, db, listsession, ModifierSession, AddProduction);//, Production.NombrePalettes.Value);
                                   

            try
            {
                db.SaveChanges();
                if (Dispose) this.Dispose();
            }
            catch (Exception ew)
            {
                this.ShowError(ew.AllMessages("impossible d'enregistrer!"));
            }
        }



        private void btSupprimerSession_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("Voulez-vous vraiment supprimer cette Session?"))
            {
                var objectsToDel = new List<object>();
                var selectedSessionIndex = dgvSession.GetSelectedIndex();
                if (selectedSessionIndex != null)
                {
                    var toDelSession = Production.SessionProductions.ElementAt(selectedSessionIndex.Value);

                    var arrets = toDelSession.SessionProductionArrets.ToList();
                    foreach (var arret in arrets)
                    {
                        objectsToDel.Add(arret);
                    }

                    var stockPETPalettes = toDelSession.StockPETPalettes.ToList();
                    foreach (var stock in stockPETPalettes)
                    {
                        if (stock.EnStock == true)
                        {
                            objectsToDel.Add(stock);
                        }
                        else
                        {
                            var transferts = stock.TransfertPalettes.ToList();
                            foreach (var transfert in transferts)
                            {
                                if (transfert.IsAuto == true)
                                {
                                    var stockMagazins = transfert.StockPalettes.ToList();
                                    foreach (var stockMagazin in stockMagazins)
                                    {
                                        if (stockMagazin.EnStock == true)
                                        {
                                            objectsToDel.Add(stockMagazin);
                                        }
                                        else
                                        {
                                            this.ShowWarning("Impossible de supprimer session car produit fini associé n'est plus en stock");
                                            return;
                                        }
                                    }

                                }
                                else
                                {
                                    this.ShowWarning("Impossible de supprimer session car produit fini associé n'est plus en stock");
                                    return;
                                }
                            }

                        }
                    }

                    var stockPETFardeaux = toDelSession.StockPETFardeaux.ToList();
                    foreach (var stock in stockPETFardeaux)
                    {
                        if (stock.EnStock == true)
                        {
                            objectsToDel.Add(stock);
                        }
                        else
                        {
                            var transferts = stock.TransfertFardeaux.ToList();
                            foreach (var transfert in transferts)
                            {
                                if (transfert.IsAuto == true)
                                {
                                    var stockMagazins = transfert.StockFardeaux.ToList();
                                    foreach (var stockMagazin in stockMagazins)
                                    {
                                        if (stockMagazin.EnStock == true)
                                        {
                                            objectsToDel.Add(stockMagazin);
                                        }
                                        else
                                        {
                                            this.ShowWarning("Impossible de supprimer session car produit fini associé n'est plus en stock");
                                            return;
                                        }
                                    }

                                }
                                else
                                {
                                    this.ShowWarning("Impossible de supprimer session car produit fini associé n'est plus en stock");
                                    return;
                                }
                            }

                        }
                    }


                    Production.SessionProductions.Remove(toDelSession);
                    foreach (var item in objectsToDel)
                    {
                        db.DeleteObject(item);
                    }
                    db.DeleteObject(toDelSession);
                    UpdateTotalSession();
                    RefreshGridSession();
                }
            }
        }

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
            SetProductionDate();

        }

        public static void PrintTempRapportProduction(Production Production)
        {
            Tools.ClearPrintTemp();

            using (var db = new ModelEntities())
            {

                var SessionProductions = Production.SessionProductions;
                var TypeProduit = "";

                var RincageTxt = "";
                var CIPTxt = "";
                var MaintenanceTxt = "";
                var RincageFinTxt = "";
                var AutreTxt = "";
                //  var Observation = "";
                foreach (var session in SessionProductions)
                {
                    TypeProduit += " & " + session.GoutProduit.Nom;

                    #region initialise values
                    var rinçage = session.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.Rinçage);
                    var cip = session.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.CIP);
                    var rinçageFinProd = session.SessionProductionArrets.SingleOrDefault(p => p.TypeArret == (int)EnTypeArret.RinçageFinProd);
                    var maintenances = session.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Maintenance);
                    var autres = session.SessionProductionArrets.Where(p => p.TypeArret == (int)EnTypeArret.Autre);

                    if (rinçage != null)
                    {
                        RincageTxt += "  &  " + rinçage.HeureDebut.Value.ToString(@"HH\:mm") + " à " + rinçage.HeureFin.Value.ToString(@"HH\:mm");
                    }

                    if (cip != null)
                    {
                        CIPTxt += "  &  " + cip.HeureDebut.Value.ToString(@"HH\:mm") + " à " + cip.HeureFin.Value.ToString(@"HH\:mm");
                    }
                    foreach (var arret in maintenances)
                    {
                        MaintenanceTxt += "  &  " + arret.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arret.HeureFin.Value.ToString(@"HH\:mm");
                    }
                    if (rinçageFinProd != null)
                    {
                        RincageFinTxt += "  &  " + rinçageFinProd.HeureDebut.Value.ToString(@"HH\:mm") + " à " + rinçageFinProd.HeureFin.Value.ToString(@"HH\:mm");
                    }

                    foreach (var arret in autres)
                    {
                        AutreTxt += "  &  " + arret.HeureDebut.Value.ToString(@"HH\:mm") + " à " + arret.HeureFin.Value.ToString(@"HH\:mm");
                    }
                    #endregion
                }


                TypeProduit = TypeProduit.Substring(2);

                var printTemp = new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    Propriete = "a. Démarrage",
                    val1 = Production.HeureDemarrage.Value.ToString(@"dd/MM à HH\:mm"),


                };
                db.AddToPrintTemps(printTemp);


                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    Propriete = "g. Fin de Production",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = Production.HeureFinDeProd.Value.ToString(@"dd/MM à HH\:mm"),


                });

                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Total Arrêts",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = Production.TempsArrets.MinutesToHoursMinutesTxt(),

                });

                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Heures nettes de Production",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = Production.TempsNetProduction.MinutesToHoursMinutesTxt()

                });


                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Production en Bouteilles",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = Production.TotalBouteilles.ToString(),

                });

                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Efficience",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = Production.Efficience.ToString() + "%",

                });

                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Performance",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = Production.Performance.ToString() + "%",

                });





                //---------------------------------------------------------


                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    Propriete = "b. Rinçage",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = (RincageTxt != "") ? RincageTxt.Substring(5) : "/",

                });


                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    Propriete = "c. CIP",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = (CIPTxt != "") ? CIPTxt.Substring(5) : "/"
                });



                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    Propriete = "d. Maintenance",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = (MaintenanceTxt != "") ? MaintenanceTxt.Substring(5) : "/",


                });




                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    Propriete = "e. Rinçage fin de production",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = (RincageFinTxt != "") ? RincageFinTxt.Substring(5) : "/",


                });



                db.AddToPrintTemps(new PrintTemp()
                {
                    UID = Tools.CurrentUserID,
                    Groupe = "Arrêts",
                    Propriete = "f. Autres",
                    TypeProduit = TypeProduit,
                    val4 = Production.ObservationMatiere,
                    val3 = Production.ObservationSessions,
                    Temps = Production.TempsTotalTravail.MinutesToHoursMinutesTxt(),
                    Format = Production.Format.Volume,
                    val2 = (Production.HeureDemarrage.Value.Date.CompareTo(Production.HeureFinDeProd.Value.Date) != 0) ?
                    Production.HeureDemarrage.Value.ToShortDateString() + " -- " + Production.HeureFinDeProd.Value.Date.ToShortDateString() :
                    Production.HeureFinDeProd.Value.Date.ToShortDateString(),
                    val1 = (AutreTxt != "") ? AutreTxt.Substring(5) : "/",



                });


                db.SaveChanges();



            }
        }



        private void btImprimer_Click(object sender, EventArgs e)
        {
            if (this.ConfirmWarning("Vous allez Enregistrer les données avant les imprimer"))
            {
                SaveProdcution(false); // no dispose
                btImprimer.Enabled = false;
                panProgress.Visible = true;
                pb.Maximum = 9;
                pb.Value = 0;

                bgw.RunWorkerAsync();
            }


        }

        public static string GenerateProductionPDFRepport(BackgroundWorker bgw, Production Production)
        {
            var CNom = Production.Employer.Nom; // chef de production
            var UNom = Production.ApplicationUtilisateur.Employer.Nom; //employer

            if (bgw != null)
            {
                bgw.ReportProgress(1);
            }

   ReportController.ExportPDF = true;


            var listFiles = new List<string>();
            //1
            PrintTempRapportProduction(Production);

            ReportController.ExportFileName = "Suivi de Production";
            ProductionReportController.PrintSuiviJournalierProduction(CNom, UNom, "SUIVI JOURNALIER DE PRODUCTION", "bouteille verre", "Production VERRE " + Production.Equipe.Nom);
            listFiles.Add(ReportController.ExportFileName);


            if (bgw != null)
            {
                bgw.ReportProgress(2);
            }
            //2



            ReportController.ExportFileName = "bouteille Vide";
            ProductionReportController.PrintBouteilleVide(Production.ID);
            listFiles.Add(ReportController.ExportFileName);




            if (bgw != null)
            {
                bgw.ReportProgress(3);
            }
            //3
            //TODO:Couvercle



            ReportController.ExportFileName = "Bouchon";
            ProductionReportController.PrintBouchon(Production.ID);
            listFiles.Add(ReportController.ExportFileName);



            if (bgw != null)
            {
                bgw.ReportProgress(4);
            }
            //4 etiquette 


            ReportController.ExportFileName = "Etiquette";
            ProductionReportController.PrintEtiquette(Production.ID);
            listFiles.Add(ReportController.ExportFileName);



           // ReportController.ExportFileName = "Barquette";
         //   FZBGsys.NsReports.FrViewer.PrintBarquette(Production.ID);
          //  listFiles.Add(ReportController.ExportFileName);




            if (bgw != null)
            {
                bgw.ReportProgress(5);
            }
            //5

            ReportController.ExportFileName = "Film Thermofusible";
            ProductionReportController.PrintFilmThermo(Production.ID);
            listFiles.Add(ReportController.ExportFileName);


            if (bgw != null)
            {
                bgw.ReportProgress(6);
            }

            //6

            ReportController.ExportFileName = "Siroperie";
            ProductionReportController.PrintSiropherie(Production.ID);
            listFiles.Add(ReportController.ExportFileName);


            if (bgw != null)
            {
                bgw.ReportProgress(7);
            }

            //7

            ReportController.ExportFileName = "Rapport de Production";
            ProductionReportController.PrintRapoortJournalierProduction(Production.ID);
            listFiles.Add(ReportController.ExportFileName);


            if (bgw != null)
            {
                bgw.ReportProgress(8);

            }
            //8

            MergeEx ex = new MergeEx();

            FileInfo fn = new FileInfo(ReportController.ExportFileName);

            ex.SourceFolder = fn.Directory.ToString() + "\\";
            ex.DestinationFile = fn.Directory + "\\" + "Production_VERRE_" + Production.Equipe.Nom.Replace(" ", "_") + "_" + Production.HeureDemarrage.Value.ToString("dd_MM_yyyy") + ".pdf";

            foreach (var file in listFiles)
            {
                if (fn != null)
                {
                    fn = new FileInfo(file);
                    ex.AddFile(fn.Name);
                }
            }



            ex.Execute();

            if (bgw != null)
            {
                bgw.ReportProgress(9);
            }
            return ex.DestinationFile;
        }

        private void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            this.ReportFileName = GenerateProductionPDFRepport(bgw, Production);
        }

        private void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pb.Value = e.ProgressPercentage;
        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            panProgress.Visible = false;
            btImprimer.Enabled = true;
            if (!string.IsNullOrEmpty(ReportFileName))
            {
                Process.Start(ReportFileName);
            }
            else
            {
                Tools.ShowError("Impossible de générer le rapport!");
            }
        }

        private void txtCadence_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar != 8 && !char.IsNumber(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            var format = (Format)cbFormat.SelectedItem;
            txtCadence.Text = format.CadenceMoyenne.ToString();
        }

        private void FrProductionPET_Load(object sender, EventArgs e)
        {

        }

        private void cbEquipe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Production != null && Production.SessionProductions.Count == 0 && cbEquipe.SelectedIndex != 0)
            {
                //   Production.HeureDemarrage = dtDate.Value;
                var equipe = (Equipe)cbEquipe.SelectedItem;
                Production.HeureDemarrage = Tools.GetFillDateFromDateTime(dtDate.Value, equipe.DefaultHeureDemarrage);
            }

        }

        private void txtCadence_TextChanged(object sender, EventArgs e)
        {
            if (txtCadence.Text.ParseToDec() != null)
            {
                UpdateTotalSession();
                RefreshGridSession();
            }
        }



        public string ReportFileName { get; set; }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void btBarquette_Click(object sender, EventArgs e)
        {
            if (cbFormat.SelectedIndex != 0 && cbEquipe.SelectedIndex != 0)
            {
                Production.Format = (Format)cbFormat.SelectedItem;
                //    Production.HeureDemarrage = dtDate.Value;
                new FrBarquette(Production, EnSection.Unité_Verre, db).ShowDialog();
                RefreshGridMatiere();

            }
            else
            {
                this.ShowInformation("Selectionner d'abord un Format & une Equipe");

            }
        }
    }
}
