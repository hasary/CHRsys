﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrBouchon : Form
    {
        ModelEntities db;//= new ModelEntities();
        Production Production = null;

        public FrBouchon()
        {
            db = new ModelEntities();
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionBouchon>();
            //     dtDate.Value = DateTime.Now;
        }

        public FrBouchon(Production Production, ModelEntities db = null)
        {
            if (db != null)
            {
                this.db = db;
            }
            else
            {
                this.db = new ModelEntities();
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionBouchon>();
            //  Production = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            this.Production = Production;
            if (Production != null)
            {
                LoadProduction();
            }
        }

        private void LoadProduction()
        {
            //  txtNonConform.Text = Production.BouteilleNonConform.ToString();
            cbFormat.SelectedItem = Production.Format;
            dtDate.Value = Production.HeureDemarrage.Value;
            var list = Production.ProductionBouchons.ToList();
            ListToSave.AddRange(list);
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        public FrBouchon(DateTime date)
        {
            ListToSave = new List<ProductionBouchon>();
            dtDate.Value = date;
        }

        private void IntitialiseControls()
        {
            //---------------------- Fournisseur
            var TypeRessourceBouchon = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Bouchon);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourceBouchon)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;

            //---------------------- Type Bouchon 
            cbCouleurBouchon.Items.Add("");
            cbCouleurBouchon.Items.Add("Bleu");
            cbCouleurBouchon.Items.Add("Blanc");
            cbCouleurBouchon.Items.Add("Rouge");
            cbCouleurBouchon.Items.Add("Noir");
            cbCouleurBouchon.Items.Add("Orange");
            cbCouleurBouchon.Items.Add("Jaune");
            cbCouleurBouchon.Items.Add("Vert");
            cbCouleurBouchon.Items.Add("Violet");
            cbCouleurBouchon.Items.Add("Gris");
            cbCouleurBouchon.SelectedIndex = 0;

            //-------------------------- DT Heure


            cbTypeBouchon.Items.Add(new TypeBouchon { ID = 0, Nom = "" });
            cbTypeBouchon.Items.AddRange(db.TypeBouchons.Where(p => p.ID > 0).ToArray());
            cbTypeBouchon.DisplayMember = "Nom";
            cbTypeBouchon.ValueMember = "ID";
            cbTypeBouchon.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeBouchon.SelectedItem = null;




            dtHeure.Value = new DateTime(2012, 5, 1, 8, 0, 0);
            dtHeure.MaxDate = new DateTime(2020, 7, 1, 8, 0, 0);
            dtHeure.MinDate = new DateTime(2011, 7, 1, 8, 0, 0);

            // -------------------------------------



        }

        private void FrBouchon_Load(object sender, EventArgs e)
        {

        }

        private void FrBouchon_FormClosing(object sender, FormClosingEventArgs e)
        {
            // db.Dispose();
        }
        public List<ProductionBouchon> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {

            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
                //       CreateProduction();
            }

            var newBouchon = new ProductionBouchon();

            // newBouchon.Date = dtDate.Value.Date;
            newBouchon.DateHeure = Tools.GetFillDateFromDateTime(dtDate.Value, dtHeure.Value.TimeOfDay);
            if (chDateFabriq.Checked)
            {
                newBouchon.DateFabrication = dtDateFabriq.Value.Date;
            }
            newBouchon.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;
            newBouchon.NombreCartons = txtNbrCarton.Text.ParseToInt();
            newBouchon.NombreBouchonParCarton = txtNbrBouchon.Text.ParseToInt();
            newBouchon.QuantitePiece = newBouchon.NombreCartons * newBouchon.NombreBouchonParCarton;
            if (panCouleur.Visible)
            {
                newBouchon.CouleurBouchon = cbCouleurBouchon.SelectedItem.ToString();
            }
            newBouchon.NoLot = txtLot.Text.Trim();
            newBouchon.TypeBouchon = (TypeBouchon)cbTypeBouchon.SelectedItem; // ------- plastique !


            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(p => p.FournisseurID == newBouchon.FournisseurID
                    && p.TypeRessourceID == (int)EnTypeRessource.Bouchon &&
                    p.TypeBouchonID == newBouchon.TypeBouchonID
                    //  &&   (p.TypeBouchonID != 2 || (p.TypeBouchonID == 2 && p.CouleurBouchon == newBouchon.CouleurBouchon))
                    );
                if (newBouchon.TypeBouchonID ==1)
                {
                    ressources = ressources.Where(p => p.CouleurBouchon == newBouchon.CouleurBouchon);
                }
                ressources = ressources.OrderBy(p => p.ID);

                if (ressources.Count() == 0)
                {
                    this.ShowWarning("type de bouchon n'est pas en Stock");
                    return;
                }
                var ressource = ressources.First();
                var newTransfert = new TransfertMatiere();
                newTransfert.Observation = "consomation " + newBouchon.DateHeure.Value.ToShortDateString() + " (auto)";
                newTransfert.DestinationSectionID = (Production.SectionID == (int)EnSection.Unité_PET) ? (int)EnSection.Unité_PET : (int)EnSection.Unité_Canette;
                newTransfert.Date = newBouchon.DateHeure.Value.Date;
                //newTransfert.DateArrivage = newSoufflage.d;
                newTransfert.DateFabrication = newBouchon.DateFabrication;
                newTransfert.Ressource = ressource;
                newTransfert.Fournisseur = newBouchon.Fournisseur;
                newTransfert.QunatiteUM = newBouchon.QuantitePiece;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = newBouchon.NombreCartons; // 

                //newTransfert.StockMatiere = SelectedStockMT;
                newBouchon.TransfertMatiere = newTransfert;
                var sucess = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!sucess)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newBouchon);
                    return;
                }

            }
            #endregion


            ListToSave.Add(newBouchon);

            Production.ProductionBouchons.Add(newBouchon);


            RefreshGrid();
            UpdateControls();
            UpdateTotal();

        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
                // dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            txtNbrCarton.Text = "";
            //   txtNbrBouchon.Text = "";
        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }
            if (cbTypeBouchon.SelectedIndex == 0)
            {
                Message += "Selectionner un Type";
            }

            if (cbFournisseur.SelectedIndex == 0)
            {
                //Message += "Selectionner un Fournisseur";
            }

            if (panCouleur.Visible && cbCouleurBouchon.SelectedIndex == 0)
            {
                Message += "\nSelectionner une Couleur";
            }
            if (txtLot.Text.Trim() == "")
            {
                Message += "\nEntrer le numéro de lot!";
            }

            int outInt = 0;


            if (ListToSave.Select(p => p.DateHeure).ToList().Contains(dtHeure.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }

            if (!int.TryParse(txtNbrCarton.Text.Trim(), out outInt))
            {
                Message += "\nNombre carton invalide!";

            }
            else if (true)
            {

            }

            if (!int.TryParse(txtNbrBouchon.Text.Trim(), out outInt))
            {
                Message += "\nNombre Bouchons Invalide!";

            }

            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            labTotal.Text = ListToSave.Sum(p => p.NombreCartons).ToString() + " cartons  " + ListToSave.Sum(p => p.QuantitePiece) + " bouchons";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Heure = p.DateHeure.Value.ToString(@"HH\:mm"),
                Couleur = p.CouleurBouchon,
                Cartons = p.NombreCartons,
                bouchons = p.NombreBouchonParCarton,
                Total = p.QuantitePiece,
                Date_Fabr = p.DateFabrication,
                Fournisseur = p.Fournisseur.Nom,

            }).ToList();

            dgv.HideIDColumn();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("la liste est vide voulez-vous quand-meme enregistrer?"))
                {
                    return;
                }
            }

            //   int nonConform = 0;
            /*  if (!int.TryParse(txtNonConform.Text.Trim(), out nonConform))
              {
                  this.ShowWarning("veillez saisir quantite boutilles non conformes!");
                  return;
              }*/


            if (Production == null)
            {
                //    CreateProduction();
            }
            /*foreach (var bouchons in ListToSave)
            {
                if (bouchons.ID == 0)
                {
                    Production.ProductionBouchons.Add(bouchons);
                }
            }*/

            UpdateProduction(); //  update total bouteilles in production object
            Dispose();
            /*   try
               {
                   db.SaveChanges();
                   Dispose();
               }
               catch (Exception exp)
               {
                   this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

               }
               */
        }

        private void UpdateProduction()
        {
            Production.CartonBouchon = ListToSave.Sum(p => p.NombreCartons);
            Production.NombreBouchon = ListToSave.Sum(p => p.QuantitePiece);
        }

        /* private void CreateProduction()
         {
             Production = new Production()
             {
                 Heur  = dtDate.Value.Date,
                 Format = (Format)cbFormat.SelectedItem,
                 SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
             };

             //   db.AddToProductions(Production);
         }*/

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {


        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            InputBouchon(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly 

            ListToSave.RemoveAt(selectedIndex.Value);
            var toDel = Production.ProductionBouchons.ElementAt(selectedIndex.Value);
            Production.ProductionBouchons.Remove(toDel);

            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }



            db.DeleteObject(toDel);
            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();

        }

        private void InputBouchon(ProductionBouchon s)
        {
            dtDate.Value = s.DateHeure.Value;
            dtHeure.Value = s.DateHeure.Value;
            cbFournisseur.SelectedItem = s.Fournisseur;
            cbCouleurBouchon.SelectedItem = s.CouleurBouchon;
            dtDateFabriq.Value = s.DateFabrication.Value;
            txtNbrCarton.Text = s.NombreCartons.ToString();
            txtNbrBouchon.Text = s.NombreBouchonParCarton.ToString();
            txtLot.Text = s.NoLot.ToString();
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool EditForm { get; set; }

        private void txtNbrBox_TextChanged(object sender, EventArgs e)
        {
            UpdateTotalCarton();
        }

        private void UpdateTotalCarton()
        {
            int? outCart = txtNbrCarton.Text.ParseToInt();
            int? outBouch = txtNbrBouchon.Text.ParseToInt();

            if (outCart != null && outBouch != null)
            {
                txtQttTotal.Text = (outBouch * outCart).ToString();
            }
            else
            {
                txtQttTotal.Text = "";
            }

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            var key = e.KeyChar;
            if (key != 8 && !char.IsLetter(key))
            {
                e.Handled = true;
            }

            if (char.IsLower(key))
            {
                e.KeyChar = char.ToUpper(key);
            }
        }

        private void cbTypeBouchon_SelectedIndexChanged(object sender, EventArgs e)
        {
            panCouleur.Visible = cbTypeBouchon.SelectedIndex == 1;
        }

        private void chDateFabriq_CheckedChanged(object sender, EventArgs e)
        {
            dtDateFabriq.Visible = chDateFabriq.Checked;
        }
    }
}
