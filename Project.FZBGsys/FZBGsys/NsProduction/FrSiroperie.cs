﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrSiroperie : Form
    {
        ModelEntities db;//= new ModelEntities();
        Production Production = null;

        public FrSiroperie()
        {
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionSiroperie>();
            //     dtDate.Value = DateTime.Now;
        }

        public FrSiroperie(Production Production, ModelEntities db = null)
        {
            this.Production = Production;//= db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            if (db == null)
            {
                this.db = new ModelEntities();
            }
            else
            {
                this.db = db;
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionSiroperie>();

            if (Production != null)
            {
                LoadProduction();
            }
        }

        private void LoadProduction()
        {

            //  txtNonConform.Text = Production.BouteilleNonConform.ToString();
            cbFormat.SelectedItem = Production.Format;
            //  dtDate.Value = Production.Date.Value;
            ListToSave.AddRange(Production.ProductionSiroperies.ToList());
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        public FrSiroperie(DateTime date)
        {
            ListToSave = new List<ProductionSiroperie>();
            dtDate.Value = date;
        }

        private void IntitialiseControls()
        {
            //---------------------- Fournisseur
            //  var TypeRessourceSiroperie = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Siroperie);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            ///   cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourceSiroperie)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;

            //---------------------- Type Siroperie 
            cbTypeSiroperie.Items.Add(new TypeRessource { ID = 0, Nom = "" });
            cbTypeSiroperie.Items.AddRange(db.TypeRessources.Where(p => p.ID > 0 && p.ClassRessourceID == (int)EnClassRessource.Siroperie).ToArray());
            cbTypeSiroperie.DisplayMember = "Nom";
            cbTypeSiroperie.ValueMember = "ID";
            cbTypeSiroperie.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeSiroperie.SelectedItem = null;


            //----------------------------------------------- type arome
            var goutProduits = Production.SessionProductions.Select(p => p.GoutProduit).ToList();
            var typeArromes = new List<TypeArrome>();
            // var allAromes = db.TypeArromes;
            foreach (var item in goutProduits)
            {
                typeArromes.AddRange(item.TypeArromes);

            }

            

            //-------------------------------- type produit
            
            cbTypeArrome.Items.Add(db.TypeArromes.Single(p => p.ID == 0));
            if (typeArromes.Count !=0)
            {
                cbTypeArrome.Items.AddRange(typeArromes.Distinct().ToArray());
            }
            else
            {
                cbTypeArrome.Items.AddRange(db.TypeArromes.Where(p=>p.ID !=0) .ToArray());
            }
            cbTypeArrome.DisplayMember = "Nom";
            cbTypeArrome.ValueMember = "ID";
            cbTypeArrome.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeArrome.SelectedIndex = 0;


            //--------------------------------- type colorant

            cbTypeColorant.Items.Add(db.TypeColorants.Single(p => p.ID == 0));
            cbTypeColorant.Items.AddRange(db.TypeColorants.Where(p => p.ID > 0).ToArray());
            cbTypeColorant.DisplayMember = "Code";
            cbTypeColorant.ValueMember = "ID";
            cbTypeColorant.DropDownStyle = ComboBoxStyle.DropDownList;
            cbTypeColorant.SelectedIndex = 0;


            dtDate.Value = Production.HeureDemarrage.Value;

        }

        private void FrSiroperie_Load(object sender, EventArgs e)
        {

        }

        private void FrSiroperie_FormClosing(object sender, FormClosingEventArgs e)
        {
            //db.Dispose();
        }
        public List<ProductionSiroperie> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {
            //var SectionProducion = EnSection.Unité_PET;
            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
                //     CreateProduction();
               
            }

            var newSiroperie = new ProductionSiroperie();
            Production.ProductionSiroperies.Add(newSiroperie);

            newSiroperie.DateHeure = dtDate.Value.Date;
            newSiroperie.Fournisseur = (Fournisseur)cbFournisseur.SelectedItem;

            // newSiroperie.QuantiteUM = txtQuantite.Text.ParseToInt();
            newSiroperie.TypeRessource = (TypeRessource)cbTypeSiroperie.SelectedItem;

            newSiroperie.TypeArrome = (TypeArrome)cbTypeArrome.SelectedItem;
            newSiroperie.TypeColorant = (TypeColorant)cbTypeColorant.SelectedItem;

            newSiroperie.QuantiteUM = txtQuantite.Text.ParseToDec();
            newSiroperie.QuantitePiece = txtPiece.Text.ParseToInt();


            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(
                    p =>
                       p.FournisseurID == newSiroperie.FournisseurID
                    && p.TypeRessourceID == newSiroperie.TypeRessourceID
                    && p.TypeArromeID == newSiroperie.TypeArromeID
                    && p.TypeColorantID == newSiroperie.TypeColorantID).OrderBy(p => p.ID);
                
                if (ressources.Count() == 0)
                {
                    this.ShowWarning("type de Syroperie n'est pas en Stock");
                    return;
                }
                var ressource = ressources.First();

                var newTransfert = new TransfertMatiere();

                newTransfert.Observation = "consomation " + newSiroperie.Production.HeureDemarrage.Value.ToShortDateString() + " (auto)";


                newTransfert.DestinationSectionID = (Production.SectionID == (int)EnSection.Unité_PET) ? (int)EnSection.Unité_PET : (int)EnSection.Unité_Canette;
                newTransfert.Date = newSiroperie.Production.HeureDemarrage;
                newTransfert.Ressource = ressource;
                newTransfert.Fournisseur = newSiroperie.Fournisseur;
                newTransfert.QunatiteUM = newSiroperie.QuantiteUM;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = newSiroperie.QuantitePiece; // one Box
                newSiroperie.TransfertMatiere = newTransfert;
                var success = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!success)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newSiroperie);
                    return;

                }
                //db.AddToTransfertMatieres(newTransfert);

            }
            #endregion

            ListToSave.Add(newSiroperie);


            RefreshGrid();
            UpdateControls();
            UpdateTotal();

        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
                //     dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            txtPiece.Text = "";
            txtQuantite.Text = "";

        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }

            /*if (cbFournisseur.SelectedIndex == 0)
            {
                Message += "Selectionner un Fournisseur";
            }
            */
            if (cbTypeSiroperie.SelectedIndex == 0)
            {
                Message += "\nSelectionner un Type Siroperie";
            }

            if (SelectedTypeRessource.ID == (int)EnTypeRessource.Arrome && cbTypeArrome.SelectedIndex == 0)
            {
                Message += "\nSelectionner un Type Arrome";
            }

            if (SelectedTypeRessource.ID == (int)EnTypeRessource.Colorant && cbTypeColorant.SelectedIndex == 0)
            {
                Message += "\nSelectionner un Type Colorant";
            }


            if (SelectedTypeRessource.ID == (int)EnTypeRessource.Sucre && txtPiece.Text.ParseToInt(false) == null)
            {
                Message += "\nNombre " + SelectedTypeRessource.UniteDePiece + " Invalide!";

            }

            if (txtQuantite.Text.ParseToDec(false) == null)
            {
                Message += "\nQuantite invalide!";
            }

            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            //      labTotal.Text = ListToSave.Sum(p => p.QuantitePiece).ToString() + " Siroperies ";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                //  Heure = p.HeureDebut.Value.ToString(@"HH\:mm") + " - " + p.HeureFin.Value.ToString(@"HH\:mm"),
                Categorie = p.TypeRessource.Nom + ((p.TypeArrome != null) ? " " + p.TypeArrome.Nom : "") + ((p.TypeColorant != null) ? " " + p.TypeColorant.Code : ""),
                Quantite = ((p.QuantitePiece != null) ? p.QuantitePiece + " " + p.TypeRessource.UniteDePiece : ""),
                Total = p.QuantiteUM + " " + p.TypeRessource.UniteDeMesure,
                Fournisseur = (p.Fournisseur != null) ? p.Fournisseur.Nom : "/",

            }).ToList();

            dgv.HideIDColumn();
        }

        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("la liste est vide voulez-vous quand-meme enregistrer?"))
                {
                    return;
                }
            }

            if (Production == null)
            {
                //     CreateProduction();
            }
            /*   foreach (var Siroperie in ListToSave)
               {
                   if (Siroperie.ID == 0)
                   {
                       Production.ProductionSiroperies.Add(Siroperie);
                   }
               }
               */
            UpdateProduction(); //  update total bouteilles in production object

            try
            {
                db.SaveChanges();
                Dispose();
            }
            catch (Exception exp)
            {
                this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

            }

        }

        private void UpdateProduction()
        {

            //     Production.NombreSiroperie = ListToSave.Sum(p => p.QuantitePiece);
            //   Production.SiroperieNonConforme = 0;// ListToSave.Sum(p => p.QuantitePiece);
        }

        /*  private void CreateProduction()
          {
              Production = new Production()
              {
                  Date = dtDate.Value.Date,
                  Format = (Format)cbFormat.SelectedItem,
                  SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
              };

              db.AddToProductions(Production);
          }*/

        private void dtDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            InputSiroperie(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly 

            ListToSave.RemoveAt(selectedIndex.Value);
            var toDel = Production.ProductionSiroperies.ElementAt(selectedIndex.Value);
            Production.ProductionSiroperies.Remove(toDel);

            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }

            db.DeleteObject(toDel);
            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();

        }

        private void InputSiroperie(ProductionSiroperie s)
        {

            cbTypeSiroperie.SelectedItem = s.TypeRessource;

            try
            {
                cbTypeArrome.SelectedItem = s.TypeArrome;
            }
            catch (Exception)
            {
                cbTypeArrome.SelectedIndex = 0;
            }
            cbTypeColorant.SelectedItem = s.TypeColorant;

            txtQuantite.Text = s.QuantiteUM.ToString();
            if (s.QuantitePiece != null)
            {
                txtPiece.Text = s.QuantitePiece.ToString();
            }

            cbFournisseur.SelectedItem = s.Fournisseur;
        }

        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public bool EditForm { get; set; }

        private void txtNbrSiroperie_TextChanged(object sender, EventArgs e)
        {
            var ressource = (TypeRessource)cbTypeSiroperie.SelectedItem;
            var Qtt = txtCont.Text.ParseToInt() * txtPiece.Text.ParseToInt();

            txtQuantite.Text = Qtt.ToString();
        }

        private void cbTypeSiroperie_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectedTypeRessource = (TypeRessource)cbTypeSiroperie.SelectedItem;
            labUniteDeMesure.Text = SelectedTypeRessource.UniteDeMesure;
            labUniteDePiece.Text = SelectedTypeRessource.UniteDePiece;
            txtCont.Text = SelectedTypeRessource.QuantiteParPiece.ToAffInt();
            cbTypeColorant.SelectedIndex = 0;
            cbTypeArrome.SelectedIndex = 0;

            panTypeColorant.Visible = false;
            panTypeArrome.Visible = false;
            txtQuantite.ReadOnly = false;
            panNombre.Visible = false;

            cbTypeArrome.SelectedIndex = 0;
            cbTypeColorant.SelectedIndex = 0;


            cbFournisseur.Items.Clear();
            cbFournisseur.Items.Add(db.Fournisseurs.Single(p => p.ID == 0));
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.TypeRessources.Contains(SelectedTypeRessource)).ToArray());
            cbFournisseur.SelectedIndex = 0;

            if (cbTypeSiroperie.SelectedItem == null)
            {
                return;
            }


            labUniteDePiece.Text = SelectedTypeRessource.UniteDePiece;
            labUniteDeMesure.Text = SelectedTypeRessource.UniteDeMesure;
            labContenu.Text = SelectedTypeRessource.UniteDeMesure + " / " + SelectedTypeRessource.UniteDePiece;
            switch ((EnTypeRessource)SelectedTypeRessource.ID)
            {
                case EnTypeRessource.Sucre:
                    panNombre.Visible = true;
                    txtQuantite.ReadOnly = true;
                    break;

                case EnTypeRessource.Colorant:
                    panTypeColorant.Visible = true;
                    break;
                case EnTypeRessource.Arrome:
                    panTypeArrome.Visible = true;
                    break;
                case EnTypeRessource.Acide_Citrique:
                    break;
                case EnTypeRessource.Consontre_Orange:
                    panNombre.Visible = true;
                    txtQuantite.ReadOnly = true;
                    break;
                case EnTypeRessource.CO2:
                    break;
                default:
                    break;
            }
        }


        public TypeRessource SelectedTypeRessource { get; set; }

        private void txtQuantite_KeyPress(object sender, KeyPressEventArgs e)
        {
            var key = e.KeyChar;
            if (!char.IsNumber(key) && key != 8 && key != '.' && key != ',')
            {
                e.Handled = true;
            }
            if (key == '.')
            {
                e.KeyChar = ',';
            }
        }
    }
}
