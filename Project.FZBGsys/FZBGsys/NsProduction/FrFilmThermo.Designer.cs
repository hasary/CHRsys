﻿namespace FZBGsys.NsProduction
{
    partial class FrFilmThermo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbEpFilm = new System.Windows.Forms.ComboBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTypeFilmThermo = new System.Windows.Forms.ComboBox();
            this.txtPoids = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtHeure = new System.Windows.Forms.DateTimePicker();
            this.txtNonConform = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(263, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(747, 399);
            this.dgv.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Date:";
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(76, 22);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(167, 22);
            this.dtDate.TabIndex = 9;
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(705, 415);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(304, 43);
            this.xpannel1.TabIndex = 11;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(36, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(164, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(263, 425);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(124, 425);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 19;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(76, 59);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(167, 24);
            this.cbFormat.TabIndex = 21;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbEpFilm);
            this.groupBox1.Controls.Add(this.cbFournisseur);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cbTypeFilmThermo);
            this.groupBox1.Controls.Add(this.txtPoids);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 153);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(241, 239);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "FilmThermo";
            // 
            // cbEpFilm
            // 
            this.cbEpFilm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEpFilm.FormattingEnabled = true;
            this.cbEpFilm.Items.AddRange(new object[] {
            "",
            "450",
            "550",
            "560"});
            this.cbEpFilm.Location = new System.Drawing.Point(96, 84);
            this.cbEpFilm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbEpFilm.Name = "cbEpFilm";
            this.cbEpFilm.Size = new System.Drawing.Size(88, 24);
            this.cbEpFilm.TabIndex = 26;
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(21, 146);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(205, 24);
            this.cbFournisseur.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Fournisseur:";
            // 
            // cbTypeFilmThermo
            // 
            this.cbTypeFilmThermo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeFilmThermo.FormattingEnabled = true;
            this.cbTypeFilmThermo.Location = new System.Drawing.Point(96, 33);
            this.cbTypeFilmThermo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeFilmThermo.Name = "cbTypeFilmThermo";
            this.cbTypeFilmThermo.Size = new System.Drawing.Size(129, 24);
            this.cbTypeFilmThermo.TabIndex = 1;
            // 
            // txtPoids
            // 
            this.txtPoids.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPoids.Location = new System.Drawing.Point(111, 201);
            this.txtPoids.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPoids.Name = "txtPoids";
            this.txtPoids.Size = new System.Drawing.Size(85, 22);
            this.txtPoids.TabIndex = 25;
            this.txtPoids.TextChanged += new System.EventHandler(this.txtNbrFilmThermo_TextChanged);
            this.txtPoids.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPoids_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Kg";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(195, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "mm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Epaisseur:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(44, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Poids:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Type Film:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(380, 416);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Total Général:";
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(533, 415);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(24, 17);
            this.labTotal.TabIndex = 8;
            this.labTotal.Text = "00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(99, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "Heure:";
            // 
            // dtHeure
            // 
            this.dtHeure.CustomFormat = "HH:mm";
            this.dtHeure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtHeure.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHeure.Location = new System.Drawing.Point(168, 126);
            this.dtHeure.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtHeure.MaxDate = new System.DateTime(2222, 5, 2, 0, 0, 0, 0);
            this.dtHeure.MinDate = new System.DateTime(2000, 5, 1, 0, 0, 0, 0);
            this.dtHeure.Name = "dtHeure";
            this.dtHeure.ShowUpDown = true;
            this.dtHeure.Size = new System.Drawing.Size(73, 22);
            this.dtHeure.TabIndex = 23;
            this.dtHeure.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // txtNonConform
            // 
            this.txtNonConform.Location = new System.Drawing.Point(536, 438);
            this.txtNonConform.Name = "txtNonConform";
            this.txtNonConform.Size = new System.Drawing.Size(75, 22);
            this.txtNonConform.TabIndex = 29;
            this.txtNonConform.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPoids_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(380, 441);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Film Non Conforme:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(626, 443);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(25, 17);
            this.label10.TabIndex = 31;
            this.label10.Text = "Kg";
            // 
            // FrFilmThermo
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 470);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtNonConform);
            this.Controls.Add(this.dtHeure);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.labTotal);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrFilmThermo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Production Film Thermofusible ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrFilmThermo_FormClosing);
            this.Load += new System.EventHandler(this.FrFilmThermo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTypeFilmThermo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPoids;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtHeure;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNonConform;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbEpFilm;
        private System.Windows.Forms.Label label4;
    }
}