﻿namespace FZBGsys.NsProduction
{
    partial class FrBouchon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.xpannel1 = new System.Windows.Forms.Panel();
            this.btAnnuler = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNbrCarton = new System.Windows.Forms.TextBox();
            this.dtDateFabriq = new System.Windows.Forms.DateTimePicker();
            this.cbCouleurBouchon = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQttTotal = new System.Windows.Forms.TextBox();
            this.txtNbrBouchon = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.labTotal = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtHeure = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.txtLot = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbTypeBouchon = new System.Windows.Forms.ComboBox();
            this.panCouleur = new System.Windows.Forms.Panel();
            this.chDateFabriq = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.xpannel1.SuspendLayout();
            this.panCouleur.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(263, 11);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(747, 431);
            this.dgv.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Date:";
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(76, 22);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(167, 22);
            this.dtDate.TabIndex = 9;
            this.dtDate.ValueChanged += new System.EventHandler(this.dtDate_ValueChanged);
            // 
            // xpannel1
            // 
            this.xpannel1.Controls.Add(this.btAnnuler);
            this.xpannel1.Controls.Add(this.btEnregistrer);
            this.xpannel1.Location = new System.Drawing.Point(705, 455);
            this.xpannel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.xpannel1.Name = "xpannel1";
            this.xpannel1.Size = new System.Drawing.Size(304, 43);
            this.xpannel1.TabIndex = 11;
            // 
            // btAnnuler
            // 
            this.btAnnuler.Location = new System.Drawing.Point(36, 2);
            this.btAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAnnuler.Name = "btAnnuler";
            this.btAnnuler.Size = new System.Drawing.Size(123, 33);
            this.btAnnuler.TabIndex = 4;
            this.btAnnuler.Text = "Annuler";
            this.btAnnuler.UseVisualStyleBackColor = true;
            this.btAnnuler.Click += new System.EventHandler(this.btAnnuler_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(164, 2);
            this.btEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(127, 33);
            this.btEnregistrer.TabIndex = 3;
            this.btEnregistrer.Text = "Enregistrer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(263, 459);
            this.btEnlever.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 18;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(124, 459);
            this.btAjouter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(133, 31);
            this.btAjouter.TabIndex = 19;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 20;
            this.label2.Text = "Format:";
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(76, 59);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(167, 24);
            this.cbFormat.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(19, 420);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 17);
            this.label8.TabIndex = 24;
            this.label8.Text = "Total Cartons:";
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(27, 302);
            this.cbFournisseur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(205, 24);
            this.cbFournisseur.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 281);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Fournisseur:";
            // 
            // txtNbrCarton
            // 
            this.txtNbrCarton.Location = new System.Drawing.Point(153, 345);
            this.txtNbrCarton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNbrCarton.Name = "txtNbrCarton";
            this.txtNbrCarton.Size = new System.Drawing.Size(71, 22);
            this.txtNbrCarton.TabIndex = 4;
            this.txtNbrCarton.TextChanged += new System.EventHandler(this.txtNbrBox_TextChanged);
            // 
            // dtDateFabriq
            // 
            this.dtDateFabriq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateFabriq.Location = new System.Drawing.Point(116, 245);
            this.dtDateFabriq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateFabriq.Name = "dtDateFabriq";
            this.dtDateFabriq.Size = new System.Drawing.Size(117, 22);
            this.dtDateFabriq.TabIndex = 3;
            this.dtDateFabriq.Value = new System.DateTime(2012, 5, 1, 8, 2, 0, 0);
            this.dtDateFabriq.Visible = false;
            // 
            // cbCouleurBouchon
            // 
            this.cbCouleurBouchon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCouleurBouchon.FormattingEnabled = true;
            this.cbCouleurBouchon.Location = new System.Drawing.Point(81, 2);
            this.cbCouleurBouchon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbCouleurBouchon.Name = "cbCouleurBouchon";
            this.cbCouleurBouchon.Size = new System.Drawing.Size(137, 24);
            this.cbCouleurBouchon.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 348);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Nombre Cartons:";
            // 
            // txtQttTotal
            // 
            this.txtQttTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQttTotal.Location = new System.Drawing.Point(151, 420);
            this.txtQttTotal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtQttTotal.Name = "txtQttTotal";
            this.txtQttTotal.ReadOnly = true;
            this.txtQttTotal.Size = new System.Drawing.Size(71, 22);
            this.txtQttTotal.TabIndex = 6;
            // 
            // txtNbrBouchon
            // 
            this.txtNbrBouchon.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNbrBouchon.Location = new System.Drawing.Point(153, 380);
            this.txtNbrBouchon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNbrBouchon.Name = "txtNbrBouchon";
            this.txtNbrBouchon.Size = new System.Drawing.Size(71, 22);
            this.txtNbrBouchon.TabIndex = 5;
            this.txtNbrBouchon.TextChanged += new System.EventHandler(this.txtNbrBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(19, 380);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Nombre Bouchon:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Couleur:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(409, 458);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(113, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Total Général:";
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(467, 484);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(24, 17);
            this.labTotal.TabIndex = 8;
            this.labTotal.Text = "00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 17);
            this.label11.TabIndex = 28;
            this.label11.Text = "Heure:";
            // 
            // dtHeure
            // 
            this.dtHeure.CustomFormat = "HH:mm";
            this.dtHeure.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtHeure.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtHeure.Location = new System.Drawing.Point(76, 106);
            this.dtHeure.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtHeure.MaxDate = new System.DateTime(2222, 5, 2, 0, 0, 0, 0);
            this.dtHeure.MinDate = new System.DateTime(2000, 5, 1, 0, 0, 0, 0);
            this.dtHeure.Name = "dtHeure";
            this.dtHeure.ShowUpDown = true;
            this.dtHeure.Size = new System.Drawing.Size(73, 22);
            this.dtHeure.TabIndex = 29;
            this.dtHeure.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(156, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Lot:";
            // 
            // txtLot
            // 
            this.txtLot.Location = new System.Drawing.Point(196, 106);
            this.txtLot.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtLot.Name = "txtLot";
            this.txtLot.Size = new System.Drawing.Size(45, 22);
            this.txtLot.TabIndex = 31;
            this.txtLot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 159);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Type:";
            // 
            // cbTypeBouchon
            // 
            this.cbTypeBouchon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeBouchon.FormattingEnabled = true;
            this.cbTypeBouchon.Location = new System.Drawing.Point(104, 155);
            this.cbTypeBouchon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeBouchon.Name = "cbTypeBouchon";
            this.cbTypeBouchon.Size = new System.Drawing.Size(137, 24);
            this.cbTypeBouchon.TabIndex = 1;
            this.cbTypeBouchon.SelectedIndexChanged += new System.EventHandler(this.cbTypeBouchon_SelectedIndexChanged);
            // 
            // panCouleur
            // 
            this.panCouleur.Controls.Add(this.cbCouleurBouchon);
            this.panCouleur.Controls.Add(this.label3);
            this.panCouleur.Location = new System.Drawing.Point(23, 194);
            this.panCouleur.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panCouleur.Name = "panCouleur";
            this.panCouleur.Size = new System.Drawing.Size(224, 32);
            this.panCouleur.TabIndex = 32;
            // 
            // chDateFabriq
            // 
            this.chDateFabriq.AutoSize = true;
            this.chDateFabriq.Location = new System.Drawing.Point(12, 245);
            this.chDateFabriq.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chDateFabriq.Name = "chDateFabriq";
            this.chDateFabriq.Size = new System.Drawing.Size(96, 21);
            this.chDateFabriq.TabIndex = 33;
            this.chDateFabriq.Text = "Date Fab.:";
            this.chDateFabriq.UseVisualStyleBackColor = true;
            this.chDateFabriq.CheckedChanged += new System.EventHandler(this.chDateFabriq_CheckedChanged);
            // 
            // FrBouchon
            // 
            this.AcceptButton = this.btAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 519);
            this.Controls.Add(this.chDateFabriq);
            this.Controls.Add(this.panCouleur);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtLot);
            this.Controls.Add(this.cbFournisseur);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dtHeure);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtNbrCarton);
            this.Controls.Add(this.labTotal);
            this.Controls.Add(this.dtDateFabriq);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.cbTypeBouchon);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtQttTotal);
            this.Controls.Add(this.cbFormat);
            this.Controls.Add(this.txtNbrBouchon);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.xpannel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.dgv);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrBouchon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Production Bouchon ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrBouchon_FormClosing);
            this.Load += new System.EventHandler(this.FrBouchon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.xpannel1.ResumeLayout(false);
            this.panCouleur.ResumeLayout(false);
            this.panCouleur.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.Panel xpannel1;
        private System.Windows.Forms.Button btAnnuler;
        private System.Windows.Forms.Button btEnregistrer;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNbrCarton;
        private System.Windows.Forms.DateTimePicker dtDateFabriq;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbCouleurBouchon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtNbrBouchon;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtHeure;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtQttTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtLot;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbTypeBouchon;
        private System.Windows.Forms.Panel panCouleur;
        private System.Windows.Forms.CheckBox chDateFabriq;
    }
}