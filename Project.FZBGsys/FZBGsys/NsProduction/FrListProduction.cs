﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrListProduction : Form
    {
        public FrListProduction()
        {
            InitializeComponent();
            InitialiseControls();
        }

        private void InitialiseControls()
        {
            using (var db = new ModelEntities())
            {
                //--------------------------- type Produit
                cbTypeProduit.Items.Add(new GoutProduit { ID = 0, Nom = "[Tout]" });
                cbTypeProduit.Items.AddRange(db.GoutProduits.Where(p => p.ID > 0).ToArray());
                cbTypeProduit.SelectedIndex = 0;
                cbTypeProduit.ValueMember = "ID";
                cbTypeProduit.DisplayMember = "Nom";
                cbTypeProduit.DropDownStyle = ComboBoxStyle.DropDownList;

                //---------------------- Format (Bouteilles)
                cbFormat.Items.Add(new Format { ID = 0, Volume = "[Tout]" });
                cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
                cbFormat.SelectedIndex = 0;
                cbFormat.DisplayMember = "Volume";
                cbFormat.ValueMember = "ID";
                cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;


                //------------- datetimes
                dtDate.Value = DateTime.Now;
                dtDateAu.Value = DateTime.Now;
                dtDateAu.MaxDate = DateTime.Now;
                dtDateDu.MinDate = DateTime.Parse("01/01/2012");
                dtDateDu.Value = DateTime.Parse("01/01/2012");

            }
        }

        private void chRechercheAvance_CheckedChanged(object sender, EventArgs e)
        {
            panRechercheAvance.Enabled = chRechercheAvance.Checked;
            if (ListSessionsFound != null)
            {
                ListSessionsFound.Clear();
                RefreshGrid();
            }


        }

        private void btRecherche_Click(object sender, EventArgs e)
        {
            Recherche();
        }
        private List<Production> ListProductionFound { set; get; }
        private List<SessionProduction> ListSessionsFound { set; get; }
        private void Recherche()
        {
            using (var db = new ModelEntities())
            {
                IEnumerable<SessionProduction> listSession = db.SessionProductions;
                IEnumerable<Production> listProduction = db.Productions;

                string filterDate = "", filterProduit = "", filterFormat = "";
                if (!chRechercheAvance.Checked)
                {
                    listSession = listSession.ToList().Where(p => p.HeureDemarrage.Value.Date == dtDate.Value.Date);
                    listProduction = listProduction.ToList().Where(p => p.HeureDemarrage.Value.Date == dtDate.Value.Date);
                    filterDate = "Journée du " + dtDate.Value.Date.ToLongDateString();
                }
                else if (!chUniteCanette.Checked && !chUnitePET.Checked)
                {
                    this.ShowWarning("Aucune Unité n'est séléctionnée!");
                    return;
                }
                else
                {
                    if (dtDateDu.MinDate != dtDateDu.Value.Date || dtDateAu.MaxDate != dtDateAu.Value.Date)
                    {
                        filterDate = "période du " + dtDateDu.Value.ToShortDateString() + " au " + dtDateAu.Value.ToShortDateString();
                        listSession = listSession.Where(p => p.HeureDemarrage >= dtDateDu.Value && p.HeureFinProduction <= dtDateAu.Value);
                        listProduction = listProduction.Where(p => p.HeureDemarrage >= dtDateDu.Value && p.HeureFinDeProd <= dtDateAu.Value);
                    }

                    if (!chUniteCanette.Checked)
                    {
                        listSession = listSession.Where(p => p.Production.SectionID != (int)EnSection.Unité_Canette);
                        listProduction = listProduction.Where(p => p.SectionID != (int)EnSection.Unité_Canette);
                    }

                    if (!chUnitePET.Checked)
                    {
                        listSession = listSession.Where(p => p.Production.SectionID != (int)EnSection.Unité_PET);
                        listProduction = listProduction.Where(p => p.SectionID != (int)EnSection.Unité_PET);
                    }

                    if (!chUniteVerre.Checked)
                    {
                        listSession = listSession.Where(p => p.Production.SectionID != (int)EnSection.Unité_Verre);
                        listProduction = listProduction.Where(p => p.SectionID != (int)EnSection.Unité_Verre);
                    }


                    if (cbFormat.SelectedIndex != 0)
                    {
                        filterFormat = "Format  " + ((Format)cbFormat.SelectedItem).Volume;
                        listSession = listSession.Where(p => p.Production.BouteileFormatID == ((Format)cbFormat.SelectedItem).ID);
                        listProduction = listProduction.Where(p => p.BouteileFormatID == ((Format)cbFormat.SelectedItem).ID);
                    }

                    if (cbTypeProduit.SelectedIndex != 0)
                    {
                        filterProduit = "Produit " + ((GoutProduit)cbTypeProduit.SelectedItem).Nom;
                        listSession = listSession.Where(p => p.GoutProduitID == ((GoutProduit)cbTypeProduit.SelectedItem).ID);
                        ListProductionFound = listProduction.Where(p => p.SessionProductions.Count != 0).ToList().Where(p => p.SessionProductions.Select(k => k.GoutProduitID).ToList().Contains(((GoutProduit)cbTypeProduit.SelectedItem).ID)).ToList();

                    }
                    else
                    {
                        ListProductionFound = listProduction.ToList();
                    }
                }
                if (ListProductionFound == null)
                {
                    ListProductionFound = listProduction.ToList();
                }
                ListSessionsFound = listSession.ToList();
                RefreshGrid();
                UpdateTotals();

                labFilter.Text = filterDate + " " + filterFormat + " " + filterProduit;
            }
        }

        private void UpdateTotals()
        {
            labTotal.Text = ListSessionsFound.Sum(p => p.NombreBouteillesTotal).ToString() + " bouteilles en " + ListSessionsFound.Sum(p => p.TempsNetProduction).MinutesToHoursMinutesTxt();
        }

        private void RefreshGrid()
        {
            if (ListSessionsFound.Count > 100)
            {
                Tools.ShowInformation("Plus de 100 résultats ne serons pas affichés, mais les totaux son globaux");
            }

            var arrets = ListSessionsFound.Where(p => p.GoutProduit == null).Select(t => new

            {
                ID = t.ID,
                Unite = t.Production.Section.Nom,
                Equipe = t.Production.Equipe.Nom,
                Produit = "Arret",
                Format = "/",
                Debut = t.HeureDemarrage.Value.ToString(@"dd/MM/yy  HH\:mm"),
                Fin = t.HeureFinProduction.Value.ToString(@"dd/MM/yy  HH\:mm"),
                Durée = t.TempsNetProduction.MinutesToHoursMinutesTxt(),
                Arrets = t.TempsTotalArret.MinutesToHoursMinutesTxt(),
                Bouteilles = 0,
                Section = t.Production.SectionID,

            }).ToList();
            var prod = ListSessionsFound.Where(p => p.GoutProduit != null).Take(100).Select(p => new
            {
                ID = p.ID,
                Unite = p.Production.Section.Nom,
                Equipe = p.Production.Equipe.Nom,
                Produit = p.GoutProduit.Nom,
                Format = p.Production.Format.Volume,
                Debut = p.HeureDemarrage.Value.ToString(@"dd/MM/yy  HH\:mm"),
                Fin = p.HeureFinProduction.Value.ToString(@"dd/MM/yy  HH\:mm"),
                Durée = p.TempsNetProduction.MinutesToHoursMinutesTxt(),
                Arrets = p.TempsTotalArret.MinutesToHoursMinutesTxt(),
                Bouteilles = p.NombreBouteillesTotal.Value,//+  " "+((p.Production.SectionID == (int)EnSection.Unité_PET) p. )
                Section = p.Production.SectionID,
            }).ToList();

            dgvSessions.DataSource = prod.Union(arrets).ToList();
            
            

            dgvSessions.HideIDColumn();
            dgvSessions.HideIDColumn("Section");

            for (int i = 0; i < dgvSessions.Rows.Count; i++)
            {
                switch ((EnSection)dgvSessions.Rows[i].Cells["Section"].Value.ToString().ParseToInt())
                {
                    case EnSection.Unité_PET:
                        // dgvSessions.Rows[1].DefaultCellStyle.ForeColor = Color.Blue;
                        break;
                    case EnSection.Unité_Verre:
                        dgvSessions.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                        break;

                    case EnSection.Unité_Canette:
                        dgvSessions.Rows[i].DefaultCellStyle.ForeColor = Color.BlueViolet;
                        break;

                    default:
                        break;
                }

            }


            dgvProduction.DataSource = ListProductionFound.Where(p => p.TotalBouteilles != null).Take(100).Select(p => new
            {
                ID = p.ID,

                Unite = p.Section.Nom,
                Equipe = p.Equipe.Nom,
                Sessions = p.SessionProductions.Count,
                Produit = p.TypeProduitTxt(),
                Format = p.Format.Volume,
                Debut = (p.HeureDemarrage == null) ? "" : p.HeureDemarrage.Value.ToString(@"dd/MM/yy  HH\:mm"),
                Fin = (p.HeureDemarrage == null) ? "" : p.HeureFinDeProd.Value.ToString(@"dd/MM/yy  HH\:mm"),
                Durée = p.TempsNetProduction.MinutesToHoursMinutesTxt(),
                Arrets = p.TempsArrets.MinutesToHoursMinutesTxt(),
                Bouteilles = p.TotalBouteilles,
                Section = p.SectionID

            }).ToList();

            dgvProduction.HideIDColumn();
            dgvProduction.HideIDColumn("Section");
            for (int i = 0; i < dgvProduction.Rows.Count; i++)
            {
                switch ((EnSection)dgvProduction.Rows[i].Cells["Section"].Value.ToString().ParseToInt())
                {
                    case EnSection.Unité_PET:
                        // dgvSessions.Rows[1].DefaultCellStyle.ForeColor = Color.Blue;
                        break;
                    case EnSection.Unité_Verre:
                        dgvProduction.Rows[i].DefaultCellStyle.ForeColor = Color.Green;
                        break;

                    case EnSection.Unité_Canette:
                        dgvProduction.Rows[i].DefaultCellStyle.ForeColor = Color.BlueViolet;
                        break;

                    default:
                        break;
                }

            }
        }

        private void btFermer_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btImprimer_Click(object sender, EventArgs e)
        {
            if (ListSessionsFound.Count == 0)
            {
                this.ShowInformation("Il n'y à rien à imprimer, éfféctuez d'abord une recherche");
                return;
            }

            if (chRechercheAvance.Checked)
            {
                PrintCustumList();
            }
            else
            {
                //    PrintJourneeProduction();
            }
            //----------------------------------------------------------


            //----------------------------------------------------------



        }

        private void PrintCustumList()
        {

            using (var db = new ModelEntities())
            {
                if (ListSessionsFound == null || ListSessionsFound.Count == 0)
                {
                    this.ShowInformation("Il n'y a rien à imprimer!");
                    return;
                }

                Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before

                foreach (var session in ListSessionsFound)
                {
                    db.AddToPrintTemps(new PrintTemp()
                    {/*
                       Date = p.Production.Date,
                Unite = p.Production.Section.Nom,
                Produit = p.GoutProduit.Nom,
                Format = p.Production.Format.Volume,
                Debut = p.HeureDemarrage.Value.ToString(@"HH\:mm"),
                Fin = p.HeureFinProduction.Value.ToString(@"HH\:mm"),
                Durée = p.TempsNetProduction.MinutesToHoursMinutesTxt(),
                Arrets = p.TempsTotalArret.MinutesToHoursMinutesTxt(),
                Bouteilles = p.NombreBouteilles.Value,//+  " "+((p.Pr
                      */

                        val4 = session.Production.HeureDemarrage.Value.ToString(@"dd/MM/yyyy HH\:mm") + " -- " + session.Production.HeureFinDeProd.Value.ToString(@"dd/MM/yyyy HH\:mm"),
                        val1 = session.Production.Section.Nom,
                        val2 = session.NombrePalettesTotal.ToString(),
                        TypeProduit = session.GoutProduit.Nom,
                        Format = session.Production.Format.Volume,
                        Temps = session.TempsNetProduction.MinutesToHoursMinutesTxt(),
                        // Heure = session.HeureDemarrage.Value.ToString(@"HH\:mm") + " à " + session.HeureFinProduction.Value.ToString(@"HH\:mm"),
                        UID = Tools.CurrentUserID,
                        Piece = session.NombreBouteillesTotal,


                    });


                }

                db.SaveChanges();


                FZBGsys.NsReports.Production.ProductionReportController.PrintProductions(labFilter.Text);

            }
        }

        private void btModifier_Click(object sender, EventArgs e)
        {

            using (var db = new ModelEntities())
            {
                Production production = null;
                if (tabControl.SelectedTab == tpSessions)
                {

                    var selectedID = dgvSessions.GetSelectedID();

                    if (selectedID == null)
                    {
                        return;
                    }
                    var sessionProduction = db.SessionProductions.Single(p => p.ID == selectedID);
                    production = sessionProduction.Production;


                }
                else
                {
                    var selectedID = dgvProduction.GetSelectedID();

                    if (selectedID == null)
                    {
                        return;
                    }
                    production = db.Productions.Single(p => p.ID == selectedID);

                }

                switch ((EnSection)production.SectionID)
                {

                    case EnSection.Unité_PET: new FrProductionPET(production.ID).ShowDialog();
                        break;
                    case EnSection.Unité_Canette: new FrProductionCAN(production.ID).ShowDialog();
                        break;

                    case EnSection.Unité_Verre: new FrProductionVER(production.ID).ShowDialog();
                        break;

                    default:
                        break;
                }

                Recherche();
            }

        }

        private void btSupprimer_Click(object sender, EventArgs e)
        {
            using (var db = new ModelEntities())
            {
                Production production = null;
                if (tabControl.SelectedTab == tpSessions)
                {
                    var selectedID = dgvSessions.GetSelectedID();
                    var sessionProduction = db.SessionProductions.Single(p => p.ID == selectedID);
                    production = sessionProduction.Production;
                }
                else
                {
                    var selectedID = dgvProduction.GetSelectedID();
                    if (selectedID == null) return;

                    production = db.Productions.Single(p => p.ID == selectedID);
                }

                if (this.ConfirmWarning("Etes vous sure de vouloir supprimer la journée " + production.Equipe.Nom + " " + production.HeureDemarrage.Value.ToShortDateString() + " ?"))
                {
                    var soufflage = db.ProductionSoufflages.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var item in soufflage)
                    {
                        if (item.TransfertMatiere != null)
                        {
                            var toDelTransfert = item.TransfertMatiere;
                            var toDelMatiere = toDelTransfert.StockMatiere;
                            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                            db.DeleteObject(toDelMatiere);
                            db.DeleteObject(toDelTransfert);
                        }

                        db.DeleteObject(item);

                    }

                    var bouteilleVide = db.ProductionBouteilles.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var item in bouteilleVide)
                    {
                        if (item.TransfertMatiere != null)
                        {
                            var toDelTransfert = item.TransfertMatiere;
                            var toDelMatiere = toDelTransfert.StockMatiere;
                            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                            db.DeleteObject(toDelMatiere);
                            db.DeleteObject(toDelTransfert);
                        }

                        db.DeleteObject(item);

                    }




                    var Etiquettes = db.ProductionEtiquettes.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var item in Etiquettes)
                    {
                        if (item.TransfertMatiere != null)
                        {
                            var toDelTransfert = item.TransfertMatiere;
                            var toDelMatiere = toDelTransfert.StockMatiere;
                            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                            db.DeleteObject(toDelMatiere);
                            db.DeleteObject(toDelTransfert);
                        }
                        db.DeleteObject(item);
                    }

                    var Bouchons = db.ProductionBouchons.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var item in Bouchons)
                    {
                        if (item.TransfertMatiere != null)
                        {
                            var toDelTransfert = item.TransfertMatiere;
                            var toDelMatiere = toDelTransfert.StockMatiere;
                            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                            db.DeleteObject(toDelMatiere);
                            db.DeleteObject(toDelTransfert);
                        }
                        db.DeleteObject(item);
                    }

                    var Siropherie = db.ProductionSiroperies.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var item in Siropherie)
                    {
                        if (item.TransfertMatiere != null)
                        {
                            var toDelTransfert = item.TransfertMatiere;
                            var toDelMatiere = toDelTransfert.StockMatiere;
                            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                            db.DeleteObject(toDelMatiere);
                            db.DeleteObject(toDelTransfert);
                        }
                        db.DeleteObject(item);
                    }

                    var FilmThermo = db.ProductionFilmThermoes.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var item in FilmThermo)
                    {
                        if (item.TransfertMatiere != null)
                        {
                            var toDelTransfert = item.TransfertMatiere;
                            var toDelMatiere = toDelTransfert.StockMatiere;
                            FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                            db.DeleteObject(toDelMatiere);
                            db.DeleteObject(toDelTransfert);
                        }
                        db.DeleteObject(item);
                    }

                    //var toDel = .Production;
                    var Sessions = db.SessionProductions.Where(p => p.ProductionID == production.ID).ToList();
                    foreach (var session in Sessions)
                    {
                        var stockPETPalette = session.StockPETPalettes.ToList();
                        foreach (var stock in stockPETPalette)
                        {
                            if (stock.EnStock == true)
                            {
                                db.DeleteObject(stock);
                            }
                            else
                            {
                                this.ShowError("Impossible de supprimer le stock associé");
                            }
                        }

                        var stockPETFardeau = session.StockPETFardeaux.ToList();
                        foreach (var stock in stockPETFardeau)
                        {
                            if (stock.EnStock == true)
                            {
                                db.DeleteObject(stock);
                            }
                            else
                            {
                                this.ShowError("Impossible de supprimer le stock associé");
                            }
                        }

                        var arrets = session.SessionProductionArrets.ToList();
                        foreach (var arret in arrets)
                        {
                            db.DeleteObject(arret);
                        }

                        db.DeleteObject(session);
                    }
                    db.DeleteObject(production);

                    try
                    {
                        db.SaveChanges();
                        Recherche();
                    }
                    catch (Exception ew)
                    {

                        this.ShowError(ew.AllMessages("Impossible d'éffectuer les changements!!"));
                    }


                }
            }
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            btSupprimer.Enabled = tabControl.SelectedIndex == 1;
        }
    }
}
