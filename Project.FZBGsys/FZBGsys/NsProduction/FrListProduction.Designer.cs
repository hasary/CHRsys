﻿namespace FZBGsys.NsProduction
{
    partial class FrListProduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtDateDu = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtDateAu = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.btModifier = new System.Windows.Forms.Button();
            this.btSupprimer = new System.Windows.Forms.Button();
            this.btFermer = new System.Windows.Forms.Button();
            this.labTotal = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbTypeProduit = new System.Windows.Forms.ComboBox();
            this.cbFormat = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btRecherche = new System.Windows.Forms.Button();
            this.chUnitePET = new System.Windows.Forms.CheckBox();
            this.chUniteCanette = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chUniteVerre = new System.Windows.Forms.CheckBox();
            this.panRechercheAvance = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.dtDate = new System.Windows.Forms.DateTimePicker();
            this.chRechercheAvance = new System.Windows.Forms.CheckBox();
            this.btImprimer = new System.Windows.Forms.Button();
            this.gbEdit = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tpSessions = new System.Windows.Forms.TabPage();
            this.dgvSessions = new System.Windows.Forms.DataGridView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpJournee = new System.Windows.Forms.TabPage();
            this.dgvProduction = new System.Windows.Forms.DataGridView();
            this.labFilter = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panRechercheAvance.SuspendLayout();
            this.gbEdit.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tpSessions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSessions)).BeginInit();
            this.tabControl.SuspendLayout();
            this.tpJournee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduction)).BeginInit();
            this.SuspendLayout();
            // 
            // dtDateDu
            // 
            this.dtDateDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateDu.Location = new System.Drawing.Point(55, 26);
            this.dtDateDu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateDu.Name = "dtDateDu";
            this.dtDateDu.Size = new System.Drawing.Size(151, 22);
            this.dtDateDu.TabIndex = 10;
            this.dtDateDu.Value = new System.DateTime(2012, 1, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "du:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtDateAu);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dtDateDu);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(212, 94);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Période";
            // 
            // dtDateAu
            // 
            this.dtDateAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateAu.Location = new System.Drawing.Point(55, 59);
            this.dtDateAu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDateAu.Name = "dtDateAu";
            this.dtDateAu.Size = new System.Drawing.Size(151, 22);
            this.dtDateAu.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Au:";
            // 
            // btModifier
            // 
            this.btModifier.Location = new System.Drawing.Point(51, 20);
            this.btModifier.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btModifier.Name = "btModifier";
            this.btModifier.Size = new System.Drawing.Size(108, 34);
            this.btModifier.TabIndex = 0;
            this.btModifier.Text = "Ouvrir...";
            this.btModifier.UseVisualStyleBackColor = true;
            this.btModifier.Click += new System.EventHandler(this.btModifier_Click);
            // 
            // btSupprimer
            // 
            this.btSupprimer.Location = new System.Drawing.Point(179, 20);
            this.btSupprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btSupprimer.Name = "btSupprimer";
            this.btSupprimer.Size = new System.Drawing.Size(104, 34);
            this.btSupprimer.TabIndex = 0;
            this.btSupprimer.Text = "Supprimer";
            this.btSupprimer.UseVisualStyleBackColor = true;
            this.btSupprimer.Click += new System.EventHandler(this.btSupprimer_Click);
            // 
            // btFermer
            // 
            this.btFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btFermer.Location = new System.Drawing.Point(1005, 434);
            this.btFermer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btFermer.Name = "btFermer";
            this.btFermer.Size = new System.Drawing.Size(105, 38);
            this.btFermer.TabIndex = 14;
            this.btFermer.Text = "Fermer";
            this.btFermer.UseVisualStyleBackColor = true;
            this.btFermer.Click += new System.EventHandler(this.btFermer_Click);
            // 
            // labTotal
            // 
            this.labTotal.AutoSize = true;
            this.labTotal.Location = new System.Drawing.Point(31, 28);
            this.labTotal.Name = "labTotal";
            this.labTotal.Size = new System.Drawing.Size(0, 17);
            this.labTotal.TabIndex = 16;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cbTypeProduit);
            this.groupBox2.Controls.Add(this.cbFormat);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(11, 203);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(211, 121);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Produit";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Produit:";
            // 
            // cbTypeProduit
            // 
            this.cbTypeProduit.FormattingEnabled = true;
            this.cbTypeProduit.Location = new System.Drawing.Point(77, 82);
            this.cbTypeProduit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbTypeProduit.Name = "cbTypeProduit";
            this.cbTypeProduit.Size = new System.Drawing.Size(121, 24);
            this.cbTypeProduit.TabIndex = 1;
            // 
            // cbFormat
            // 
            this.cbFormat.FormattingEnabled = true;
            this.cbFormat.Location = new System.Drawing.Point(77, 41);
            this.cbFormat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbFormat.Name = "cbFormat";
            this.cbFormat.Size = new System.Drawing.Size(121, 24);
            this.cbFormat.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 44);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Format:";
            // 
            // btRecherche
            // 
            this.btRecherche.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btRecherche.Location = new System.Drawing.Point(13, 447);
            this.btRecherche.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btRecherche.Name = "btRecherche";
            this.btRecherche.Size = new System.Drawing.Size(132, 30);
            this.btRecherche.TabIndex = 18;
            this.btRecherche.Text = "Recherche";
            this.btRecherche.UseVisualStyleBackColor = true;
            this.btRecherche.Click += new System.EventHandler(this.btRecherche_Click);
            // 
            // chUnitePET
            // 
            this.chUnitePET.AutoSize = true;
            this.chUnitePET.Checked = true;
            this.chUnitePET.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chUnitePET.Location = new System.Drawing.Point(19, 30);
            this.chUnitePET.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chUnitePET.Name = "chUnitePET";
            this.chUnitePET.Size = new System.Drawing.Size(57, 21);
            this.chUnitePET.TabIndex = 19;
            this.chUnitePET.Text = "PET";
            this.chUnitePET.UseVisualStyleBackColor = true;
            // 
            // chUniteCanette
            // 
            this.chUniteCanette.AutoSize = true;
            this.chUniteCanette.Checked = true;
            this.chUniteCanette.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chUniteCanette.Location = new System.Drawing.Point(101, 30);
            this.chUniteCanette.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chUniteCanette.Name = "chUniteCanette";
            this.chUniteCanette.Size = new System.Drawing.Size(79, 21);
            this.chUniteCanette.TabIndex = 19;
            this.chUniteCanette.Text = "Canette";
            this.chUniteCanette.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chUniteVerre);
            this.groupBox3.Controls.Add(this.chUnitePET);
            this.groupBox3.Controls.Add(this.chUniteCanette);
            this.groupBox3.Location = new System.Drawing.Point(11, 105);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(211, 92);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Unite de Production";
            // 
            // chUniteVerre
            // 
            this.chUniteVerre.AutoSize = true;
            this.chUniteVerre.Checked = true;
            this.chUniteVerre.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chUniteVerre.Location = new System.Drawing.Point(19, 55);
            this.chUniteVerre.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chUniteVerre.Name = "chUniteVerre";
            this.chUniteVerre.Size = new System.Drawing.Size(123, 21);
            this.chUniteVerre.TabIndex = 20;
            this.chUniteVerre.Text = "Bouteille Verre";
            this.chUniteVerre.UseVisualStyleBackColor = true;
            // 
            // panRechercheAvance
            // 
            this.panRechercheAvance.Controls.Add(this.groupBox1);
            this.panRechercheAvance.Controls.Add(this.groupBox3);
            this.panRechercheAvance.Controls.Add(this.groupBox2);
            this.panRechercheAvance.Enabled = false;
            this.panRechercheAvance.Location = new System.Drawing.Point(5, 96);
            this.panRechercheAvance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panRechercheAvance.Name = "panRechercheAvance";
            this.panRechercheAvance.Size = new System.Drawing.Size(231, 346);
            this.panRechercheAvance.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 17);
            this.label7.TabIndex = 22;
            this.label7.Text = "Journée:";
            // 
            // dtDate
            // 
            this.dtDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDate.Location = new System.Drawing.Point(91, 21);
            this.dtDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtDate.Name = "dtDate";
            this.dtDate.Size = new System.Drawing.Size(135, 22);
            this.dtDate.TabIndex = 10;
            this.dtDate.Value = new System.DateTime(2012, 6, 1, 0, 0, 0, 0);
            // 
            // chRechercheAvance
            // 
            this.chRechercheAvance.AutoSize = true;
            this.chRechercheAvance.Location = new System.Drawing.Point(19, 69);
            this.chRechercheAvance.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chRechercheAvance.Name = "chRechercheAvance";
            this.chRechercheAvance.Size = new System.Drawing.Size(150, 21);
            this.chRechercheAvance.TabIndex = 23;
            this.chRechercheAvance.Text = "Recherche Avancé";
            this.chRechercheAvance.UseVisualStyleBackColor = true;
            this.chRechercheAvance.CheckedChanged += new System.EventHandler(this.chRechercheAvance_CheckedChanged);
            // 
            // btImprimer
            // 
            this.btImprimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btImprimer.Location = new System.Drawing.Point(832, 434);
            this.btImprimer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btImprimer.Name = "btImprimer";
            this.btImprimer.Size = new System.Drawing.Size(167, 38);
            this.btImprimer.TabIndex = 14;
            this.btImprimer.Text = "Imprimer la liste";
            this.btImprimer.UseVisualStyleBackColor = true;
            this.btImprimer.Click += new System.EventHandler(this.btImprimer_Click);
            // 
            // gbEdit
            // 
            this.gbEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbEdit.Controls.Add(this.btSupprimer);
            this.gbEdit.Controls.Add(this.btModifier);
            this.gbEdit.Location = new System.Drawing.Point(254, 425);
            this.gbEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbEdit.Name = "gbEdit";
            this.gbEdit.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbEdit.Size = new System.Drawing.Size(288, 63);
            this.gbEdit.TabIndex = 24;
            this.gbEdit.TabStop = false;
            this.gbEdit.Text = "Journée de production";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.labTotal);
            this.groupBox4.Location = new System.Drawing.Point(558, 425);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(268, 62);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Total:";
            // 
            // tpSessions
            // 
            this.tpSessions.Controls.Add(this.dgvSessions);
            this.tpSessions.Location = new System.Drawing.Point(4, 25);
            this.tpSessions.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpSessions.Name = "tpSessions";
            this.tpSessions.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpSessions.Size = new System.Drawing.Size(853, 371);
            this.tpSessions.TabIndex = 0;
            this.tpSessions.Text = "Sessions de production";
            this.tpSessions.UseVisualStyleBackColor = true;
            // 
            // dgvSessions
            // 
            this.dgvSessions.AllowUserToAddRows = false;
            this.dgvSessions.AllowUserToDeleteRows = false;
            this.dgvSessions.AllowUserToResizeColumns = false;
            this.dgvSessions.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvSessions.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvSessions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvSessions.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvSessions.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvSessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSessions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSessions.Location = new System.Drawing.Point(3, 2);
            this.dgvSessions.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvSessions.MultiSelect = false;
            this.dgvSessions.Name = "dgvSessions";
            this.dgvSessions.ReadOnly = true;
            this.dgvSessions.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSessions.RowHeadersVisible = false;
            this.dgvSessions.RowTemplate.Height = 16;
            this.dgvSessions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSessions.Size = new System.Drawing.Size(847, 367);
            this.dgvSessions.TabIndex = 9;
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tpSessions);
            this.tabControl.Controls.Add(this.tpJournee);
            this.tabControl.Location = new System.Drawing.Point(253, 21);
            this.tabControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(861, 400);
            this.tabControl.TabIndex = 27;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tpJournee
            // 
            this.tpJournee.Controls.Add(this.dgvProduction);
            this.tpJournee.Location = new System.Drawing.Point(4, 25);
            this.tpJournee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tpJournee.Name = "tpJournee";
            this.tpJournee.Size = new System.Drawing.Size(853, 353);
            this.tpJournee.TabIndex = 1;
            this.tpJournee.Text = "Journées de Production";
            this.tpJournee.UseVisualStyleBackColor = true;
            // 
            // dgvProduction
            // 
            this.dgvProduction.AllowUserToAddRows = false;
            this.dgvProduction.AllowUserToDeleteRows = false;
            this.dgvProduction.AllowUserToResizeColumns = false;
            this.dgvProduction.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvProduction.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProduction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProduction.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvProduction.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvProduction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduction.Location = new System.Drawing.Point(0, 0);
            this.dgvProduction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvProduction.MultiSelect = false;
            this.dgvProduction.Name = "dgvProduction";
            this.dgvProduction.ReadOnly = true;
            this.dgvProduction.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvProduction.RowHeadersVisible = false;
            this.dgvProduction.RowTemplate.Height = 16;
            this.dgvProduction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduction.Size = new System.Drawing.Size(853, 353);
            this.dgvProduction.TabIndex = 10;
            // 
            // labFilter
            // 
            this.labFilter.AutoSize = true;
            this.labFilter.Location = new System.Drawing.Point(251, 20);
            this.labFilter.Name = "labFilter";
            this.labFilter.Size = new System.Drawing.Size(0, 17);
            this.labFilter.TabIndex = 28;
            // 
            // FrListProduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1131, 500);
            this.Controls.Add(this.labFilter);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.gbEdit);
            this.Controls.Add(this.chRechercheAvance);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panRechercheAvance);
            this.Controls.Add(this.dtDate);
            this.Controls.Add(this.btRecherche);
            this.Controls.Add(this.btImprimer);
            this.Controls.Add(this.btFermer);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrListProduction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Suivi de Production";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panRechercheAvance.ResumeLayout(false);
            this.gbEdit.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tpSessions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSessions)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.tpJournee.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtDateDu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtDateAu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btModifier;
        private System.Windows.Forms.Button btSupprimer;
        private System.Windows.Forms.Button btFermer;
        private System.Windows.Forms.Label labTotal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbTypeProduit;
        private System.Windows.Forms.ComboBox cbFormat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btRecherche;
        private System.Windows.Forms.CheckBox chUnitePET;
        private System.Windows.Forms.CheckBox chUniteCanette;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panRechercheAvance;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtDate;
        private System.Windows.Forms.CheckBox chRechercheAvance;
        private System.Windows.Forms.Button btImprimer;
        private System.Windows.Forms.GroupBox gbEdit;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TabPage tpSessions;
        private System.Windows.Forms.DataGridView dgvSessions;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.Label labFilter;
        private System.Windows.Forms.TabPage tpJournee;
        private System.Windows.Forms.DataGridView dgvProduction;
        private System.Windows.Forms.CheckBox chUniteVerre;
    }
}