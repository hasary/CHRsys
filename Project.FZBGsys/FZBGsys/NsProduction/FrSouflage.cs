﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrSouflage : Form
    {
        ModelEntities db;//;= new ModelEntities();
        Production Production = null;

        public FrSouflage()
        {
            db = new ModelEntities();
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionSoufflage>();
            //     dtDate.Value = DateTime.Now;
        }

        public FrSouflage(int ProductionID, ModelEntities db)
        {
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionSoufflage>();
            Production = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            if (Production != null)
            {
                LoadSouflage();
            }
        }

        public FrSouflage(Production Production, ModelEntities db = null)
        {
            if (db == null)
            {
                this.db = new ModelEntities();
            }
            else
            {
                this.db = db;
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionSoufflage>();
            this.Production = Production;
            if (Production != null)
            {
                LoadSouflage();
            }
        }



        private void LoadSouflage()
        {
            txtNonConforme.Text = Production.BouteilleNonConform.ToString();
            txtSoufllees.Text = Production.BouteilleSoufflees.ToString();
            var index = cbFormat.Items.IndexOf(Production.Format);
            cbFormat.SelectedItem = Production.Format;
            dtDate.Value = Production.HeureDemarrage.Value;
            ListToSave.Clear();
            ListToSave.AddRange(Production.ProductionSoufflages.ToList());
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        public FrSouflage(DateTime date)
        {
            ListToSave = new List<ProductionSoufflage>();
            dtDate.Value = date;
        }

        private void IntitialiseControls()
        {


            //---------------------- Fournisseur
            var TypeRessourcePreform = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Preforme);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourcePreform)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;

            //---------------------- Gamme Bouteille
            cbGamme.Items.Add(new TypePreforme { ID = 0, GRS = "" });
            cbGamme.Items.AddRange(db.TypePreformes.Where(p => p.ID > 0).ToArray());
            cbGamme.DisplayMember = "GRS";
            cbGamme.ValueMember = "ID";
            cbGamme.DropDownStyle = ComboBoxStyle.DropDownList;
            cbGamme.SelectedItem = null;

            //-------------------------- DT Heure

          //  dtHeure.Value = new DateTime(2012, 5, 1, 8, 0, 0);


            // -------------------------------------



        }

        private void FrSouflage_Load(object sender, EventArgs e)
        {

        }

        private void FrSouflage_FormClosing(object sender, FormClosingEventArgs e)
        {
            //db.Dispose();
        }
        public List<ProductionSoufflage> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
              //  CreateProduction();
            }

            var newSoufflage = new ProductionSoufflage();

            //   newSoufflage.Date = dtDate.Value.Date;
            newSoufflage.DateHeure = Tools.GetFillDateFromDateTime(dtDate.Value, dtHeure.Value.TimeOfDay);
           if(chDateFabriq.Checked) newSoufflage.DateFabrication = dtDateFabriq.Value.Date;

            newSoufflage.FournisseurID = ((Fournisseur)cbFournisseur.SelectedItem).ID;
            newSoufflage.NoBox = txtNoBox.Text.ParseToInt();
            newSoufflage.QuantitePieces = txtQuantite.Text.ParseToInt();
            newSoufflage.PreformeGammeID = ((TypePreforme)cbGamme.SelectedItem).ID;


            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(p =>
                    p.FournisseurID == newSoufflage.FournisseurID &&
                    p.TypeRessourceID == (int)EnTypeRessource.Preforme &&
                    p.TypePreformeID == newSoufflage.PreformeGammeID).OrderBy(p=>p.ID);


                if (ressources.Count() == 0)
                {
                    this.ShowWarning("type de Ressource introuvable");
                    return;
                }
                var ressource = ressources.First();

                var newTransfert = new TransfertMatiere();

                newTransfert.Observation = "consomation " + newSoufflage.DateHeure.Value.ToShortDateString() + " (auto)";


                newTransfert.DestinationSectionID = (int)EnSection.Unité_PET;
                newTransfert.Date = newSoufflage.DateHeure.Value.Date;
                //newTransfert.DateArrivage = newSoufflage.d;
                newTransfert.DateFabrication = newSoufflage.DateFabrication;
                newTransfert.Ressource = ressource;
                newTransfert.FournisseurID = newSoufflage.FournisseurID;
                newTransfert.QunatiteUM = newSoufflage.QuantitePieces;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = 1; // one Box

                //newTransfert.StockMatiere = SelectedStockMT;
                newSoufflage.TransfertMatiere = newTransfert;
                var success = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!success)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newSoufflage);
                    return;
                }


            }
            #endregion


            ListToSave.Add(newSoufflage);
            Production.ProductionSoufflages.Add(newSoufflage);
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
             //   dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            txtNoBox.Text = "";
            //  txtQuantite.Text = "";
        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }

            if (cbFournisseur.SelectedIndex == 0)
            {
                //Message += "Selectionner un Fournisseur";
            }

            if (cbGamme.SelectedIndex == 0)
            {
                Message += "Selectionner un type de préfome";
            }


            if (ListToSave.Select(p => p.DateHeure).ToList().Contains(dtHeure.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }


            int outInt = 0;

            if (!int.TryParse(txtNoBox.Text.Trim(), out outInt))
            {
                Message += "Numero de Box Invalide!";

            }
            else if (ListToSave.Where(p => p.DateFabrication == dtDateFabriq.Value.Date).Select(p => p.NoBox).ToList().Contains(txtNoBox.Text.ParseToInt()))
            {
                Message += "\nNuméro de box déja inscrit dans la liste!";
            }


            if (!int.TryParse(txtQuantite.Text.Trim(), out outInt))
            {
                Message += "Quantite Invalide!";

            }

            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            txtTotalBox.Text = ListToSave.Count().ToString() + " boxes  " + ListToSave.Sum(p => p.QuantitePieces) + " pieces";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Heure = p.DateHeure.Value.ToString(@"HH\:mm"),
                Preforme = p.TypePreforme.GRS,
                Quantite = p.QuantitePieces,
                Box = p.NoBox,
                Date_Fabr = p.DateFabrication,
                Fournisseur = p.Fournisseur.Nom,

            }).ToList();

            dgv.HideIDColumn();
        }


        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("Enregistrer Liste Vide?"))
                {
                    return;

                }

            }

            int nonConform = 0;
            if (!int.TryParse(txtSoufllees.Text.Trim(), out nonConform))
            {
                //this.ShowWarning("veillez saisir quantite boutilles Soufflées!");
                //return;
            }

            if (!int.TryParse(txtNonConforme.Text.Trim(), out nonConform))
            {
                //this.ShowWarning("veillez saisir quantite boutilles non conformes!");
                //return;
            }

            if (Production == null)
            {
            //    CreateProduction();
            }


            /* foreach (var soufflage in ListToSave)
             {
                 if (soufflage.ID == 0)
                 {
                     Production.ProductionSoufflages.Add(soufflage);
                 }
             }*/

            UpdateProduction(); //  update total bouteilles in production object
            Dispose();
            /*  try
              {
                  db.SaveChanges();
                  Dispose();
              }
              catch (Exception exp)
              {
                  this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

              }
  */
        }
        private void UpdateProduction()
        {
            Production.NombrePreformeConsome = ListToSave.Sum(p => p.QuantitePieces);
            Production.BouteilleSoufflees = txtSoufllees.Text.Trim().ParseToInt();
            Production.BouteilleNonConform = txtNonConforme.Text.Trim().ParseToInt();
        }
       /* private void CreateProduction()
        {
            Production = new Production()
            {
                Date = dtDate.Value.Date,
                Format = (Format)cbFormat.SelectedItem,
                SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
            };

            //  db.AddToProductions(Production);
        }*/
        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
        }
        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            InputSoufflage(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly 

            ListToSave.RemoveAt(selectedIndex.Value);

            var toDel = Production.ProductionSoufflages.ElementAt(selectedIndex.Value);
            Production.ProductionSoufflages.Remove(toDel);

            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }

            db.DeleteObject(toDel);


            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();


        }
        private void InputSoufflage(ProductionSoufflage s)
        {
            dtHeure.Value = s.DateHeure.Value;// new DateTime(2012, 5, 1, s.DateHeure.Value.Hours, s.Heure.Value.Minutes, 0);
            cbFournisseur.SelectedItem = s.Fournisseur;
            cbGamme.SelectedItem = s.TypePreforme;
            if (s.DateFabrication != null) dtDateFabriq.Value = s.DateFabrication.Value;
            txtNoBox.Text = s.NoBox.ToString();
            txtQuantite.Text = s.QuantitePieces.ToString();
        }
        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        public bool EditForm { get; set; }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void xpannel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbDateFabriq_CheckedChanged(object sender, EventArgs e)
        {
            dtDateFabriq.Visible = chDateFabriq.Checked;
        }
    }
}
