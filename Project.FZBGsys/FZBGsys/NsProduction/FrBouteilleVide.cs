﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsProduction
{
    public partial class FrBouteilleVide : Form
    {
        ModelEntities db;//;= new ModelEntities();
        Production Production = null;

        public FrBouteilleVide()
        {
            db = new ModelEntities();
            EditForm = false;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionBouteille>();
            //     dtDate.Value = DateTime.Now;

        }

        public FrBouteilleVide(int ProductionID, ModelEntities db)
        {
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionBouteille>();
            Production = db.Productions.SingleOrDefault(p => p.ID == ProductionID);
            if (Production != null)
            {
                LoadVerre();
            }
        }

        public FrBouteilleVide(Production Production, ModelEntities db = null)
        {
            if (db == null)
            {
                this.db = new ModelEntities();
            }
            else
            {
                this.db = db;
            }
            this.EditForm = true;
            InitializeComponent();
            IntitialiseControls();
            ListToSave = new List<ProductionBouteille>();
            this.Production = Production;
            if (Production != null)
            {
                LoadVerre();
            }
        }



        private void LoadVerre()
        {

            txtNonConforme.Text = Production.CannetteNonConform.ToString();

            var index = cbFormat.Items.IndexOf(Production.Format);
            cbFormat.SelectedItem = Production.Format;
            dtDate.Value = Production.HeureDemarrage.Value;
            ListToSave.Clear();
            ListToSave.AddRange(Production.ProductionBouteilles.ToList());
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        public FrBouteilleVide(DateTime date)
        {
            ListToSave = new List<ProductionBouteille>();
            dtDate.Value = date;
        }

        private void IntitialiseControls()
        {


            //---------------------- Fournisseur
            var TypeRessourceBouteille = db.TypeRessources.Single(p => p.ID == (int)EnTypeRessource.Bouteille_Verre);

            cbFournisseur.Items.Add(new Fournisseur { ID = 0, Nom = "" });
            cbFournisseur.Items.AddRange(db.Fournisseurs.ToList().Where(p => p.ID > 0 && p.TypeRessources.Contains(TypeRessourceBouteille)).ToArray());
            cbFournisseur.DisplayMember = "Nom";
            cbFournisseur.ValueMember = "ID";
            cbFournisseur.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFournisseur.SelectedIndex = 0;


            //---------------------- Format (Bouteilles)
            cbFormat.Items.Add(new Format { ID = 0, Volume = "" });
            cbFormat.Items.AddRange(db.Formats.Where(p => p.ID > 0).ToArray());
            cbFormat.DisplayMember = "Volume";
            cbFormat.ValueMember = "ID";
            cbFormat.DropDownStyle = ComboBoxStyle.DropDownList;
            cbFormat.SelectedItem = null;




            //-------------------------------- type produit
            /*      cbProduit.Items.Add(new GoutProduit {ID=0, Nom ="" });
                  cbProduit.Items.AddRange(db.GoutProduits.Where(p=>p.ID>0).ToArray());
                  cbProduit.DisplayMember = "Nom";
                  cbProduit.ValueMember = "ID";
                  cbProduit.DropDownStyle = ComboBoxStyle.DropDownList;
                  cbProduit.SelectedIndex = 0;
                 */
            //-------------------------- DT Heure

            //  dtHeure.Value = new DateTime(2012, 5, 1, 8, 0, 0);


            // -------------------------------------



        }

        private void FrSouflage_Load(object sender, EventArgs e)
        {

        }

        private void FrSouflage_FormClosing(object sender, FormClosingEventArgs e)
        {
            //db.Dispose();
        }
        public List<ProductionBouteille> ListToSave { get; set; }
        private void btAjouter_Click(object sender, EventArgs e)
        {
            if (!isValidAll())
            {
                return;
            }

            if (Production == null)
            {
                //  CreateProduction();
            }

            var newBouteille = new ProductionBouteille();

            //   newSoufflage.Date = dtDate.Value.Date;
            newBouteille.DateHeure = Tools.GetFillDateFromDateTime(dtDate.Value, dtHeure.Value.TimeOfDay);
            if (chDateFabriq.Checked) newBouteille.DateFabrication = dtDateFabriq.Value.Date;

            newBouteille.FournisseurID = ((Fournisseur)cbFournisseur.SelectedItem).ID;
            newBouteille.NoPalette = txtNoBox.Text.ParseToInt();
            newBouteille.QuantitePieces = txtQuantite.Text.ParseToInt();
            //   newCannette.GoutProduitID = ((GoutProduit)cbProduit.SelectedItem).ID;


            #region auto transfert
            if (Tools.AutoCreateTransfertFromProductionMP)
            {
                var ressources = db.Ressources.Where(p =>
                    p.FournisseurID == newBouteille.FournisseurID &&
                    p.TypeRessourceID == (int)EnTypeRessource.Bouteille_Verre &&
                    p.FormatID == ((Format)cbFormat.SelectedItem).ID
                   ).OrderBy(p => p.ID);

                if (ressources.Count() == 0)
                {
                    this.ShowWarning("type de Ressource introuvable");
                    return;
                }

                var ressource = ressources.First();
                var newTransfert = new TransfertMatiere();

                newTransfert.Observation = "consomation " + newBouteille.DateHeure.Value.ToShortDateString() + " (auto)";


                newTransfert.DestinationSectionID = (int)EnSection.Unité_Canette;
                newTransfert.Date = newBouteille.DateHeure.Value.Date;
                //newTransfert.DateArrivage = newSoufflage.d;
                newTransfert.DateFabrication = newBouteille.DateFabrication;
                newTransfert.Ressource = ressource;
                newTransfert.FournisseurID = newBouteille.FournisseurID;
                newTransfert.QunatiteUM = newBouteille.QuantitePieces;
                if (ressource.TypeRessource.DefaultUnite == (int)EnUniteParDefault.Piece) newTransfert.QuantitePiece = 1; // one Box

                //newTransfert.StockMatiere = SelectedStockMT;
                newBouteille.TransfertMatiere = newTransfert;
                var success = FZBGsys.NsStock.FrStockMatiere.RemoveFromStockMT(newTransfert, db);

                if (!success)
                {
                    db.DeleteObject(newTransfert);
                    db.DeleteObject(newBouteille);
                    return;
                }


            }
            #endregion


            ListToSave.Add(newBouteille);
            Production.ProductionBouteilles.Add(newBouteille);
            RefreshGrid();
            UpdateControls();
            UpdateTotal();
        }

        private void UpdateControls()
        {
            if (Production != null || (ListToSave != null && ListToSave.Count != 0))
            {
                //   dtDate.Enabled = false;
                cbFormat.Enabled = false;

            }

            txtNoBox.Text = "";
            //  txtQuantite.Text = "";
        }
        private bool isValidAll()
        {
            string Message = "";
            if (cbFormat.SelectedIndex == 0)
            {
                Message += "Selectionner un Format";
            }

            if (cbFournisseur.SelectedIndex == 0)
            {
                //Message += "Selectionner un Fournisseur";
            }

            /* if (cbProduit.SelectedIndex == 0)
             {
                 Message += "Selectionner un produit";
             }
             */

            if (ListToSave.Select(p => p.DateHeure).ToList().Contains(dtHeure.Value))
            {
                Message += "\nHeure existe deja dans la liste, veuiller le verifier!";
            }


            int outInt = 0;

            if (!int.TryParse(txtNoBox.Text.Trim(), out outInt))
            {
                Message += "Numero de Box Invalide!";

            }
            else if (ListToSave.Where(p => p.DateFabrication == dtDateFabriq.Value.Date).Select(p => p.NoPalette).ToList().Contains(txtNoBox.Text.ParseToInt()))
            {
                Message += "\nNuméro de box déja inscrit dans la liste!";
            }


            if (!int.TryParse(txtQuantite.Text.Trim(), out outInt))
            {
                Message += "Quantite Invalide!";

            }

            if (Message != "")
            {
                this.ShowWarning(Message);
                return false;
            }

            return true;
        }
        private void UpdateTotal()
        {
            txtTotalCanette.Text = ListToSave.Count().ToString() + " palettes  " + ListToSave.Sum(p => p.QuantitePieces) + " pieces";

        }
        private void RefreshGrid()
        {
            dgv.DataSource = ListToSave.Select(p => new
            {
                ID = p.ID,
                Heure = p.DateHeure.Value.ToString(@"HH\:mm"),
                //  Produit = p.GoutProduit.Nom,
                Quantite = p.QuantitePieces,
                PaletteNum = p.NoPalette,
                Date_Fabr = p.DateFabrication,
                Fournisseur = p.Fournisseur.Nom,

            }).ToList();

            dgv.HideIDColumn();
        }


        private void btEnregistrer_Click(object sender, EventArgs e)
        {
            if (ListToSave == null || ListToSave.Count == 0)
            {
                if (!this.ConfirmWarning("Enregistrer Liste Vide?"))
                {
                    return;

                }

            }

            int nonConform = 0;
            /* if (!int.TryParse(txtSoufllees².Text.Trim(), out nonConform))
             {
                 //this.ShowWarning("veillez saisir quantite boutilles Soufflées!");
                 //return;
             }*/

            if (!int.TryParse(txtNonConforme.Text.Trim(), out nonConform))
            {
                //this.ShowWarning("veillez saisir quantite boutilles non conformes!");
                //return;
            }

            if (Production == null)
            {
                //    CreateProduction();
            }


            /* foreach (var soufflage in ListToSave)
             {
                 if (soufflage.ID == 0)
                 {
                     Production.ProductionSoufflages.Add(soufflage);
                 }
             }*/

            UpdateProduction(); //  update total bouteilles in production object
            Dispose();
            /*  try
              {
                  db.SaveChanges();
                  Dispose();
              }
              catch (Exception exp)
              {
                  this.ShowError(exp.AllMessages("Impossible d'enregistrer"));

              }
  */
        }
        private void UpdateProduction()
        {
            //   Production.NombrePreformeConsome = ListToSave.Sum(p => p.QuantitePieces);
            //      Production.BouteilleSoufflees = txtSoufllees.Text.Trim().ParseToInt();
            Production.VerreNonConform = txtNonConforme.Text.Trim().ParseToInt();
            Production.NombreVerre = ListToSave.Sum(p => p.QuantitePieces);
        }
        /* private void CreateProduction()
         {
             Production = new Production()
             {
                 Date = dtDate.Value.Date,
                 Format = (Format)cbFormat.SelectedItem,
                 SectionID = (int)EnSection.Unité_PET // ----------------------------------- unite PET
             };

             //  db.AddToProductions(Production);
         }*/
        private void dtDate_ValueChanged(object sender, EventArgs e)
        {
        }
        private void btEnlever_Click(object sender, EventArgs e)
        {
            var selectedIndex = dgv.GetSelectedIndex();
            InputVerre(ListToSave.ElementAt(selectedIndex.Value)); // input in controls sow it can modify quiquly 

            ListToSave.RemoveAt(selectedIndex.Value);

            var toDel = Production.ProductionCannettes.ElementAt(selectedIndex.Value);
            Production.ProductionCannettes.Remove(toDel);

            if (toDel.TransfertMatiere != null)
            {
                var toDelTransfert = toDel.TransfertMatiere;
                var toDelMatiere = toDelTransfert.StockMatiere;
                FZBGsys.NsStock.FrStockMatiere.AddToStockMT(toDelMatiere, db);
                db.DeleteObject(toDelMatiere);
                db.DeleteObject(toDelTransfert);
            }

            db.DeleteObject(toDel);


            RefreshGrid();
            UpdateControls();
            UpdateProduction();
            UpdateTotal();


        }
        private void InputVerre(ProductionBouteille s)
        {
            dtHeure.Value = s.DateHeure.Value;// new DateTime(2012, 5, 1, s.DateHeure.Value.Hours, s.Heure.Value.Minutes, 0);
            cbFournisseur.SelectedItem = s.Fournisseur;
            // cbProduit.SelectedItem = s.GoutProduit;
            if (s.DateFabrication != null)
            {
                dtDateFabriq.Value = s.DateFabrication.Value;
            }
            txtNoBox.Text = s.NoPalette.ToString();
            txtQuantite.Text = s.QuantitePieces.ToString();
        }
        private void btAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        public bool EditForm { get; set; }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void xpannel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbDateFabriq_CheckedChanged(object sender, EventArgs e)
        {
            dtDateFabriq.Visible = chDateFabriq.Checked;
        }
    }
}
