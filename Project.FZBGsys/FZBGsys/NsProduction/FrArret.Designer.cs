﻿namespace FZBGsys.NsProduction
{
    partial class FrArret
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dtD = new System.Windows.Forms.DateTimePicker();
            this.txtDuree = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtF = new System.Windows.Forms.DateTimePicker();
            this.cbCauseArret = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.btEnlever = new System.Windows.Forms.Button();
            this.btAjouter = new System.Windows.Forms.Button();
            this.btEnregistrer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dtD
            // 
            this.dtD.CustomFormat = "HH:mm";
            this.dtD.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtD.Location = new System.Drawing.Point(108, 31);
            this.dtD.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtD.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtD.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtD.Name = "dtD";
            this.dtD.ShowUpDown = true;
            this.dtD.Size = new System.Drawing.Size(79, 22);
            this.dtD.TabIndex = 33;
            this.dtD.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtD.ValueChanged += new System.EventHandler(this.dtD_ValueChanged);
            // 
            // txtDuree
            // 
            this.txtDuree.Location = new System.Drawing.Point(327, 31);
            this.txtDuree.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDuree.Name = "txtDuree";
            this.txtDuree.ReadOnly = true;
            this.txtDuree.Size = new System.Drawing.Size(92, 22);
            this.txtDuree.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(198, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 17);
            this.label11.TabIndex = 31;
            this.label11.Text = " à";
            // 
            // dtF
            // 
            this.dtF.CustomFormat = "HH:mm";
            this.dtF.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtF.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtF.Location = new System.Drawing.Point(233, 31);
            this.dtF.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtF.MaxDate = new System.DateTime(2035, 2, 22, 0, 0, 0, 0);
            this.dtF.MinDate = new System.DateTime(2000, 2, 1, 0, 0, 0, 0);
            this.dtF.Name = "dtF";
            this.dtF.ShowUpDown = true;
            this.dtF.Size = new System.Drawing.Size(79, 22);
            this.dtF.TabIndex = 34;
            this.dtF.Value = new System.DateTime(2012, 5, 1, 0, 0, 0, 0);
            this.dtF.ValueChanged += new System.EventHandler(this.dtD_ValueChanged);
            // 
            // cbCauseArret
            // 
            this.cbCauseArret.FormattingEnabled = true;
            this.cbCauseArret.Location = new System.Drawing.Point(108, 68);
            this.cbCauseArret.Name = "cbCauseArret";
            this.cbCauseArret.Size = new System.Drawing.Size(204, 24);
            this.cbCauseArret.TabIndex = 37;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 39;
            this.label1.Text = "Cause Arrêt:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 17);
            this.label2.TabIndex = 40;
            this.label2.Text = "Heure:";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeColumns = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(12, 100);
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 16;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(413, 153);
            this.dgv.TabIndex = 42;
            // 
            // btEnlever
            // 
            this.btEnlever.Location = new System.Drawing.Point(12, 260);
            this.btEnlever.Name = "btEnlever";
            this.btEnlever.Size = new System.Drawing.Size(111, 31);
            this.btEnlever.TabIndex = 44;
            this.btEnlever.Text = "<< Enlever";
            this.btEnlever.UseVisualStyleBackColor = true;
            this.btEnlever.Click += new System.EventHandler(this.btEnlever_Click);
            // 
            // btAjouter
            // 
            this.btAjouter.Location = new System.Drawing.Point(327, 65);
            this.btAjouter.Name = "btAjouter";
            this.btAjouter.Size = new System.Drawing.Size(98, 28);
            this.btAjouter.TabIndex = 45;
            this.btAjouter.Text = "Ajouter >>";
            this.btAjouter.UseVisualStyleBackColor = true;
            this.btAjouter.Click += new System.EventHandler(this.btAjouter_Click);
            // 
            // btEnregistrer
            // 
            this.btEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEnregistrer.Location = new System.Drawing.Point(314, 257);
            this.btEnregistrer.Name = "btEnregistrer";
            this.btEnregistrer.Size = new System.Drawing.Size(111, 33);
            this.btEnregistrer.TabIndex = 43;
            this.btEnregistrer.Text = "Fermer";
            this.btEnregistrer.UseVisualStyleBackColor = true;
            this.btEnregistrer.Click += new System.EventHandler(this.btEnregistrer_Click);
            // 
            // FrArret
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 302);
            this.Controls.Add(this.btEnlever);
            this.Controls.Add(this.btAjouter);
            this.Controls.Add(this.btEnregistrer);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.dtD);
            this.Controls.Add(this.txtDuree);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtF);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbCauseArret);
            this.Name = "FrArret";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Arrêts Maintenance / Autres";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtD;
        private System.Windows.Forms.TextBox txtDuree;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtF;
        private System.Windows.Forms.ComboBox cbCauseArret;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Button btEnlever;
        private System.Windows.Forms.Button btAjouter;
        private System.Windows.Forms.Button btEnregistrer;
    }
}