﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


namespace FZBGsys.NsReports
{
    class ReportController
    {
        public static bool ExportPDF = false;
        public static string ExportFileName { get; set; }




        internal static void PrintdummyReport()
        {


            CR__Empty__ report = new CR__Empty__();
            report.Load();

        }

        public static string ExportToPDF(ReportClass report)
        {

            try
            {

                string fileName = System.IO.Path.GetTempPath() + ExportFileName + ".pdf";
                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = fileName;
                CrExportOptions = report.ExportOptions;

                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;

                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                report.Export();
                return CrDiskFileDestinationOptions.DiskFileName;
            }
            catch (Exception ex)
            {
                return null;

            }

        }

        internal static void PrintReport(ReportClass report, bool print = false)
        {

            if (Tools.ParametreAutorisation == null)
            {
                using (var db = new ModelEntities())
                {
                    Tools.ParametreAutorisation = db.ParametreAutorisations.Single(
                        p => p.UtilisateurID == Tools.CurrentUserID && p.ParametreID == 1);
                    
                }
            }

            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;


            crConnectionInfo.ServerName = Tools.SQL_Server;
            crConnectionInfo.DatabaseName = Tools.SQL_DataBase;
            crConnectionInfo.UserID = Tools.SQL_UserName;
            crConnectionInfo.Password = Tools.SQL_Password;

            CrTables = report.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }



            var form = new FrViewer(groupTree: false);

            form.SetReportSource(report);

            if (!ExportPDF)
            {
                form.ShowDialog();
            }
            else
            {
                ExportFileName = ExportToPDF(report);
            }

            report.Dispose();
        }
    }
}
