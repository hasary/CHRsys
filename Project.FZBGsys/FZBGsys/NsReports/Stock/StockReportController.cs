﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FZBGsys.NsReports.Stock
{
    class StockReportController: ReportController
    {

        public static void PrintCustomStockMp(List<StockMatiere> ListFound, string filterTxt, ModelEntities db)
        {
            PrepareCustumStockMp(ListFound, db);

            var rapport = new CR_CustomStockMp();
            rapport.SetParameterValue("FilterTxt", filterTxt);
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport);
        }

        private static void PrepareCustumStockMp(List<StockMatiere> ListFound, ModelEntities db)
        {
            Tools.ClearPrintTemp(); //------------------------------------------- must clear printTemp before

            foreach (var stock in ListFound)
            {
                db.AddToPrintTemps(new PrintTemp()
                {
                    Date = stock.DateSortie,
                    Description = stock.Ressource.ToDescription(),
                    Nom = stock.Ressource.Fournisseur.Nom,
                    Piece = stock.Piece,
                    Quantite = stock.Quantite,
                    UID = Tools.CurrentUserID,
                    val1 = stock.Ressource.TypeRessource.UniteDeMesure,
                    val2 = stock.Ressource.TypeRessource.UniteDePiece,
                    EnStock = stock.EnStock,
                    val3 = (stock.EnStock == true) ? "En Stock" : " Sortie le: " + stock.DateSortie.Value.ToShortDateString(),
                    TypeProduit = stock.Ressource.TypeRessource.Nom

                });


            }

            db.SaveChanges();
        }
        internal static void PrintEvalFournisseur(DateTime from, DateTime to, bool All, int? FournisseurID, bool modeYear)
        {
            var report = new CR_EvalFournisseur();


            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            report.SetParameterValue("@All", All);
            report.SetParameterValue("@FournisseurID", FournisseurID);
            report.SetParameterValue("@ModeYear", modeYear);

            PrintReport(report);
        }

        internal static void PrintEvalFournisseurAnalyse(DateTime from, DateTime to)
        {
            var report = new CR_EvalFournisseurAnalyse();


            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);


            PrintReport(report);
        }

        public static void PrintSuiviStockPeriode(DateTime From, DateTime To)
        {

            PrepareSuiviStockPeriode(From, To);

            var rapport = new CR_SuiviStockPeriode();

            rapport.SetParameterValue("@UID", Tools.CurrentUserID);
            PrintReport(rapport);
        }

        private static void PrepareSuiviStockPeriode(DateTime From, DateTime To)
        {
            using (ModelEntities db = new ModelEntities())
            {
                var StockMP = db.Ressources.OrderBy(p => p.TypeRessourceID).ToList();
                Tools.ClearPrintTemp();
                foreach (var list in StockMP)
                {
                    var EntreeQt = db.Arrivages.Where(p => (p.DateArrivage >= From && p.DateArrivage <= To) && list.ID == p.RessourceID).Sum(p => p.Quantite);
                    var EntreeP = db.Arrivages.Where(p => (p.DateArrivage >= From && p.DateArrivage <= To) && list.ID == p.RessourceID).Sum(p => p.Piece);
                    var SortieQt = db.StockMatieres.Where(p => (p.DateSortie >= From && p.DateSortie <= To) && list.ID == p.RessourceID).Sum(p => p.Quantite);
                    var SortieP = db.StockMatieres.Where(p => (p.DateSortie >= From && p.DateSortie <= To) && list.ID == p.RessourceID).Sum(p => p.Piece);
                    var StockMPFini = db.StockMatieres.Where(p => p.EnStock == true && list.ID == p.RessourceID).Sum(p => p.Quantite);
                    if (EntreeQt == null && SortieP == null && StockMPFini == null) continue;
                    db.AddToPrintTemps(new PrintTemp()
                    {
                        Description = list.ToDescription(),
                        Nom = list.Fournisseur.Nom,
                        Quantite = EntreeQt,
                        Piece = EntreeP,
                        MontantC = SortieQt,
                        MontantD = SortieP,
                        Montant = StockMPFini,
                        UID = Tools.CurrentUserID,
                        val1 = list.TypeRessource.UniteDeMesure,
                        val2 = list.TypeRessource.UniteDePiece,
                        //EnStock = stock.EnStock,
                        //val3 = (list.EnStock == true) ? "En Stock" : " Sortie le: " + list.DateSortie.Value.ToShortDateString(),
                        TypeProduit = list.TypeRessource.Nom

                    });


                }
                db.SaveChangeTry();
            }
        }


        //------------------------- liste vente custum




        //--------------------------------------------------- Stock Produit Fini
        internal static void PrintStockProduitFini()
        {
            CR_StockProduitFini rapport = new CR_StockProduitFini();
            rapport.SetParameterValue("@UID", Tools.CurrentUserID);
            PrintReport(rapport, print: true);

        }


        internal static void PrintStockProduitFiniManual(DateTime date)
        {
            CR_StockProduitFiniManuel rapport = new CR_StockProduitFiniManuel();
            rapport.SetParameterValue("@Date", date);
            PrintReport(rapport, print: true);

        }
        //-------------------------------------------------- Liste arrivage
        internal static void PrintArrivages(string FilterTxt)
        {
            var rapport = new CR_Arrivages();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport, print: true);

        }


        //----------------------------------------------------------------
        internal static void PrintDemandeMatiere(int BonID)
        {
            PrepareDemandeMatiere(BonID);

            CR_DemanedeMatiere rapport = new CR_DemanedeMatiere();
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport, print: true);

        }

        private static void PrepareDemandeMatiere(int BonID)
        {
            Tools.ClearPrintTemp(); // ------------------------------------------- must clear printTemp before
            using (var db = new ModelEntities())
            {
                var Bon = db.BonDemandes.Single(p => p.ID == BonID);
                foreach (var demande in Bon.DemandeMatieres)
                {
                    db.AddToPrintTemps(new PrintTemp()
                    {
                        Date = demande.BonDemande.Date,
                        Description = demande.Ressource.ToDescription(),
                        Nom = demande.Ressource.Fournisseur.Nom,
                        Piece = demande.QuantitePiece,
                        Quantite = demande.QunatiteUM,
                        UID = Tools.CurrentUserID,
                        val1 = demande.Ressource.TypeRessource.UniteDeMesure,
                        val2 = demande.Ressource.TypeRessource.UniteDePiece,
                        val3 = BonID.ToString(),
                    });
                }

                db.SaveChanges();


            }
        }


        
        internal static void PrintTransferts(string FilterTxt, EnSection DestinationTxt)
        {
            var rapport = new CR_Transferts();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("DestinationTxt", DestinationTxt.ToSectionTxt());
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport, print: true);
        }

        internal static void PrintRecapTransfert(string FilterTxt, EnSection DestinationTxt)
        {
            CR_RecapTransfert rapport = new CR_RecapTransfert();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("DestinationTxt", DestinationTxt.ToSectionTxt());
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport, print: true);
        }
    }
}
