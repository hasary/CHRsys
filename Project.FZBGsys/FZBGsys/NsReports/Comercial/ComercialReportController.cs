﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FZBGsys.NsReports.Comercial
{
    class ComercialReportController : ReportController
    {
        public static void PrintCustomVentes(List<Vente> ListVentes, string filterTxt, ModelEntities db)
        {
            PrepareCustomVente(ListVentes, db);
            var report = new CR_CustomVentes();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            report.SetParameterValue("filterTxt", filterTxt);
            PrintReport(report);


        }



        private static void PrepareCustomVente(List<Vente> ListVentes, ModelEntities db)
        {
            Tools.ClearPrintTemp();  //---------------------------------- Custum ventes

            foreach (var vente in ListVentes)
            {
                db.AddToPrintTemps(new PrintTemp()
                {
                    Date = vente.BonAttribution.Date,
                    Description = vente.GoutProduit.Nom + " " + vente.Format.Volume,
                    UID = Tools.CurrentUserID,
                    Nom = vente.BonAttribution.Client.NomClientAndParent(),
                    Piece = vente.TotalBouteilles,
                    val1 = vente.NombrePalettes.ToString(),
                    val2 = vente.NombreFardeaux.ToString(),
                    val3 = vente.Unite.ToString(),
                    val4 = vente.GoutProduit.TypeProduit.Nom,
                    Montant = vente.PrixUnitaire * vente.TotalBouteilles,
                });

            }

            db.SaveChanges();
        }

        //---------------------------------------------------- liste vente journee
        internal static void PrintJournalVentes(DateTime from, DateTime to)
        {
            var report = new CR_JournalVentes();
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            PrintReport(report);
        }




        internal static void PrintFlotte(DateTime from, DateTime to)
        {
            var report = new CR_Flote();


            report.SetParameterValue("@fromDate", from);
            report.SetParameterValue("@toDate", to);

            report.SetParameterValue("@UID", 1); //osef

            PrintReport(report);
        }


        internal static void PrintSatisfactionClient(DateTime from, DateTime to, int All, int? ClientID, int mode)
        {
            var report = new CR_SatstfactionClient();


            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            report.SetParameterValue("@All", All);
            report.SetParameterValue("@ClientID", ClientID);
            report.SetParameterValue("@Mode", mode);

            PrintReport(report);
        }

        internal static void PrintSatisfactionClientAnalyse(DateTime from, DateTime to)
        {
            var report = new CR_SatstfactionClientAnalyse();


            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);


            PrintReport(report);
        }
        //PrintSatisfactionClientAnalyse




        internal static void PrintActiviteClientJournee(DateTime date)
        {

            PrepareActiviteClientJournee(date);

            var report = new CR_ActiviteClientJournee();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            report.SetParameterValue("dateTxt", date.ToShortDateString());
            report.SetParameterValue("yesTxt", date.AddDays(-1).ToShortDateString());
            PrintReport(report);
        }

        public static void PrepareActiviteClientJournee(DateTime date)
        {
            //var date = DateTime.Parse("2012-08-12").Date;
            var yesterday = date.AddDays(-1).Date;
            using (var db = new ModelEntities())
            {
                var factures = db.Factures.Where(p => p.Date == date && p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré).ToList();
                var operations = db.Operations.Where(p => p.Date == date && p.Sen == "C" && p.ClientID != 0).ToList();

                var clients = new List<Client>();
                #region fill clients
                foreach (var facture in factures)
                {
                    if (!clients.Contains(facture.Client))
                    {
                        clients.Add(facture.Client);
                    }
                }

                foreach (var operation in operations)
                {
                    if (!clients.Contains(operation.Client))
                    {
                        clients.Add(operation.Client);
                    }
                }

                #endregion

                Tools.ClearPrintTemp();
                foreach (var client in clients)
                {

                    if (client.ID == 3296)
                    {

                    }
                    var operaionsClient = operations.Where(p => p.ClientID == client.ID);
                    var facturesClient = factures.Where(p => p.ClientID == client.ID);

                    var versement = operaionsClient.Sum(p => p.Montant);


                    var achatComptant = facturesClient.Where(p => p.ModePayement == (int)EnModePayement.Especes || p.ModePayement == (int)EnModePayement.Chèque || p.ModePayement == (int)EnModePayement.Virement).Sum(p => p.MontantTTC);
                    var achatAvance = facturesClient.Where(p => p.ModePayement == (int)EnModePayement.Avance || p.ModePayement == (int)EnModePayement.Traite).Sum(p => p.MontantTTC);
                    var achatAterme = facturesClient.Where(p => p.ModePayement == (int)EnModePayement.a_Terme).Sum(p => p.MontantTTC);


                    var soldeYesterday = client.SoldeHistory;
                    var dateHi = client.DateHistory;
                    if (dateHi != null)
                    {
                        soldeYesterday -= client.Factures.Where(
                            p => p.Etat != (int)EnEtatFacture.Annulée &&
                            p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré &&
                            p.Date > dateHi && p.Date <= yesterday).Sum(p => p.MontantTTC);

                        soldeYesterday -= client.Operations.Where(
                                p => p.Sen == "D" && p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client &&
                                p.Date > dateHi &&
                                p.Date <= yesterday).Sum(p => p.Montant);

                        soldeYesterday += client.Operations.Where(
                                p => p.Sen == "C" &&
                                p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client &&
                                p.Date > dateHi && p.Date <= yesterday).Sum(p => p.Montant);


                    }

                    var newSolde = soldeYesterday + versement - achatAterme - achatAvance - achatComptant;
                    db.AddToPrintTemps(new PrintTemp
                    {
                        UID = Tools.CurrentUserID,
                        Nom = client.NomClientAndParent(),
                        MontantC = soldeYesterday,
                        dec4 = versement,
                        dec1 = achatComptant,
                        dec2 = achatAvance,
                        dec3 = achatAterme,
                        MontantD = newSolde,
                    });

                }
                db.SaveChangeTry();


            }
        }

        internal static void PrintJoureeVente(DateTime date)
        {
            CR_JourneeVente report = new CR_JourneeVente();
            report.SetParameterValue("@from", date);
            report.SetParameterValue("@to", date);
            PrintReport(report, print: true);
        }


        public static void PrintReleverClient(Client Client, decimal? soldeBugin, DateTime From, DateTime To)
        {

            CR_ReleveClient report = new CR_ReleveClient();
            report.SetParameterValue("@from", From);
            report.SetParameterValue("@to", To);
            report.SetParameterValue("SoldeBegin", (soldeBugin != null) ? soldeBugin : 0);
            report.SetParameterValue("NomClient", Client.Nom);
            report.SetParameterValue("@ClientID", Client.ID);
            report.SetParameterValue("NoSolde", soldeBugin == null);

            PrintReport(report, print: true);
        }

        public static void PrepareSituationComerciale(DateTime to, System.ComponentModel.BackgroundWorker bkw = null)
        {

            if (to == null)
            {
                to = Tools.GetServerDateTime().Date;
            }
            using (var db = new ModelEntities())
            {
                var clientsRep = db.Clients.Where(p => p.ID != 0 && p.ClientTypeID != 4).ToList();
                Tools.ClearPrintTemp();
                int i = 0;
                foreach (var client in clientsRep)
                {
                    if (client.ID == 3332)//3226)
                    {
                        //  continue;
                    }

                    i++;
                    FZBGsys.NsAdministration.FrMaintenance.GenerateSoldeClient(db, to, true, client);

                    var dernierVers = client.Operations.Where(p => p.Sen == "C" &&
                        p.Date <= to).OrderByDescending(p => p.Date).FirstOrDefault();

                    var dernierFact = client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée &&
                        p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré &&
                        p.Date <= to).OrderByDescending(p => p.Date).FirstOrDefault();

                    decimal? dernierVersMontant = (dernierVers == null) ? null :
                        client.Operations.Where(p => p.Sen == "C" && p.TypeOperaitonID != (int)EnTypeOpration.Pret_Client && p.TypeOperaitonID != (int)EnTypeOpration.Rembourssement_Pret_Client
                        && p.Date == dernierVers.Date).Sum(p => p.Montant);

                    decimal? dernierFactMontantTTC = (dernierFact == null) ? null :
                        client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée &&
                        p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré &&
                        p.Date == dernierFact.Date).Sum(p => p.MontantTTC);
                    DateTime? DateDernierMouvement;



                    if (dernierVers == null && dernierFact != null)
                    {
                        DateDernierMouvement = dernierFact.Date;
                    }
                    else if (dernierFact == null && dernierVers != null)
                    {
                        DateDernierMouvement = dernierVers.Date;
                    }
                    else if (dernierVers != null && dernierFact != null)
                    {
                        DateDernierMouvement = (DateTime.Compare(dernierFact.Date.Value, dernierVers.Date.Value) > 0) ? dernierFact.Date.Value : dernierVers.Date.Value;
                    }
                    else
                    {
                        DateDernierMouvement = null;
                    }

                    var DateNow = Tools.GetServerDateTime();
                    if (DateDernierMouvement == null)
                    {
                        client.take = false;
                    }
                    else
                    {
                        client.take = (Tools.GetMinutesBetween2Times(DateNow, DateDernierMouvement.Value) / 60 / 24 / 30) < Tools.MonthBeforeClientNonActif;
                    }



                    if (client.XSolde != 0)
                    {
                        db.AddToPrintTemps(new PrintTemp()
                                    {
                                        Date = to,
                                        UID = Tools.CurrentUserID,
                                        Nom = client.NomClientAndParent(),
                                        Description = (client.CategorieClient == null) ? "autre" : client.CategorieClient.Nom,
                                        MontantC = (client.XSolde > 0) ? client.XSolde : 0,
                                        MontantD = (client.XSolde < 0) ? client.XSolde : 0,

                                        val1 = (dernierVers != null) ? dernierVersMontant.ToString() : "0",
                                        val3 = (dernierVers != null) ? dernierVers.Date.Value.ToShortDateString() : "",


                                        val2 = (dernierFact != null) ? dernierFactMontantTTC.ToString() : "0",
                                        val4 = (dernierFact != null) ? dernierFact.Date.Value.ToShortDateString() : "",

                                        Etat = (client.take == true) ? "Clients Actifs (durant ces derniers  " + Tools.MonthBeforeClientNonActif + " mois)" : "Clients non actifs (pas d'activité depuis " + Tools.MonthBeforeClientNonActif + " mois)",
                                    });
                    }

                    if (bkw != null)
                    {
                        bkw.ReportProgress(i);
                    }
                }

                db.SaveChangeTry();
            }


        }

        public static void PrepareSituationComercialeLight()
        {
            var to = DateTime.Now;
            using (var db = new ModelEntities())
            {

                var clientsRep = db.Clients.Where(p => p.ID != 0 && p.ClientTypeID != 4 && p.Solde != null && p.Solde != 0 && p.isExpire != true && p.IsCompteBloque != true).ToList();
                Tools.ClearPrintTemp();
                //   int i = 0;
                foreach (var client in clientsRep)
                {

                    db.AddToPrintTemps(new PrintTemp()
                    {
                        UID = Tools.CurrentUserID,
                        Nom = client.NomClientAndParent(),
                        MontantC = (client.Solde > 0) ? client.XSolde : 0,
                        MontantD = (client.Solde < 0) ? client.XSolde : 0,

                        val1 = "",//(dernierVers != null) ? dernierVers.Montant.ToString() : "0",
                        val3 = (client.DateDernierVersement != null) ? client.DateDernierVersement.Value.ToShortDateString() : "",
                        val2 = "",// (dernierFact != null) ? dernierFact.MontantTTC.ToString() : "0",
                        val4 = (client.DateDernierChargement != null) ? client.DateDernierChargement.Value.ToShortDateString() : "",

                        Etat = (client.take == true) ? "Clients Actifs (durant ces derniers  " + Tools.MonthBeforeClientNonActif + " mois)" : "Clients non actifs (pas d'activité depuis " + Tools.MonthBeforeClientNonActif + " mois)",
                    });



                }

                db.SaveChangeTry();
            }

        }




        public static void PrintSituationCommerciale()
        {
             //PrepareSituationComerciale(to, bk);  ----------------- prepare  in DoWork
            var report = new CR_SituationComerciale();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            PrintReport(report);
        }


        public static void PrintSituationCommercialeLight()
        {
            PrepareSituationComercialeLight();
            var report = new CR_SituationComerciale();
            report.SetParameterValue("@UID", Tools.CurrentUserID);


            PrintReport(report);
        }


        //---------------------------------------------------- liste vente journee
        internal static void PrintStatiClients(DateTime from, DateTime to)
        {
            var report = new CR_StatClients();
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            PrintReport(report, print: true);
        }

        internal static void PrintStatiClients(DateTime from, DateTime to, string wilaya)
        {
            var report = new CR_StatClientsWil();
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            report.SetParameterValue("@wilaya", wilaya);
            PrintReport(report, print: true);
        }




        internal static void PrintStatArticles(DateTime from, DateTime to)
        {
            var report = new CR_StatArticles();
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            PrintReport(report, print: true);
        }
        //---------------------------------------------------- liste vente journee
        internal static void PrintVenteJournee(DateTime journee)
        {
            var report = new CR_VentesJournee();
            report.SetParameterValue("@DateVente", journee);

            PrintReport(report);
        }


        //---------------------------------------------------- bon Sortie
        internal static void PrintBonSortie(int BonID)
        {
            CR_BonSortie report = new CR_BonSortie();
            report.SetParameterValue("@ID", BonID);
            PrintReport(report, print: true);
        }

        //--------------------------------------------------------Facture
        internal static void PrintFacture(int BonID, string TotalFactTxt, string remisetxt)
        {
            CR_Facture report = new CR_Facture();
            report.SetParameterValue("@ID", BonID);
            report.SetParameterValue("TotalFactureTxt", TotalFactTxt);
            report.SetParameterValue("RemiseTxt", remisetxt);
            PrintReport(report, print: true);

        }


        internal static void PrintFactureRetour(int BonID, string TotalFactTxt, string remisetxt)
        {
            CR_FactureRetour report = new CR_FactureRetour();
            report.SetParameterValue("@ID", BonID);
            report.SetParameterValue("TotalFactureTxt", TotalFactTxt);
            report.SetParameterValue("RemiseTxt", remisetxt);
            PrintReport(report, print: true);

        }
        //----------------------------------------------------Bon Livraison 2
        internal static void PrintBonLivraison2(int BonID)
        {
            CR_BonLivraison2 report = new CR_BonLivraison2();
            report.SetParameterValue("@ID", BonID);
            PrintReport(report);

        }

        internal static void PrintBonLivraison3(int BonID)
        {
            CR_BonLivraison3 report = new CR_BonLivraison3();
            report.SetParameterValue("@ID", BonID);

            report.SetParameterValue("TotalFactureTxt", "");
            report.SetParameterValue("RemiseTxt", "");

            PrintReport(report);

        }

        //---------------------------------------------------- Bon Livraison
      



        internal static void PrintBonLivraison(int BonID)
        {
            CR_BonLivraison2 report = new CR_BonLivraison2();
            report.SetParameterValue("@ID", BonID);
            PrintReport(report, print: true);

        }

        //---------------------------------------------------Bon D'atribution
        internal static void PrintBonAttribution(int BonID)
        {
            CR_BonAttribution report = new CR_BonAttribution();
            report.SetParameterValue("@ID", BonID);
            //  report.SetParameterValue("IDtxt", BonID.ToString().PadLeft(5, '0'));
            PrintReport(report, print: true);

        }

        internal static void PrintBonAttribution2(int BonID)
        {
            CR_BonAttribution2 report = new CR_BonAttribution2();
            report.SetParameterValue("@ID", BonID);
            //  report.SetParameterValue("IDtxt", BonID.ToString().PadLeft(5, '0'));
            PrintReport(report, print: true);

        }


        internal static void PrintBonRetour(int BonID)
        {
            CR_BonRetour report = new CR_BonRetour();
            report.SetParameterValue("@ID", BonID);
            //  report.SetParameterValue("IDtxt", BonID.ToString().PadLeft(5, '0'));
            PrintReport(report, print: true);

        }

        internal static void PrintSituationComercialePeriode(DateTime From, DateTime to)
        {
            PrepareSituationCommercialePeriode(From, to);

            CR_SituationComerciale report = new CR_SituationComerciale();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            report.SetParameterValue("DateTxt", to.ToShortDateString());

            PrintReport(report);
        }

        private static void PrepareSituationCommercialePeriode(DateTime From, DateTime to)
        {
            using (var db = new ModelEntities())
            {
                #region generation des soldes
                if (true)
                {
                    var dateForm = DateTime.Parse("2012-07-23");

                    var clients = new List<Client>();

                    var facts = db.Factures.Where(p => p.Date <= to && p.Date >= From).ToList();
                    foreach (var fact in facts)
                    {
                        if (!clients.Contains(fact.Client)) clients.Add(fact.Client);
                    }

                    var opers = db.Operations.Where(p => p.Date <= to && p.Date >= From && p.ClientID != null).ToList();
                    foreach (var oper in opers)
                    {
                        if (!clients.Contains(oper.Client)) clients.Add(oper.Client);
                    }


                    foreach (var client in clients)
                    {
                        if (client.ID == 3317)
                        {

                        }
                        var factures = client.Factures.Where(p => p.Date > dateForm && p.Date <= to && p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré);
                        var operationD = client.Operations.Where(p => p.Date > dateForm && p.Sen == "D" && p.Date <= to);
                        var operationC = client.Operations.Where(p => p.Date > dateForm && p.Sen == "C" && p.Date <= to);
                        client.DateHistory = DateTime.Parse("2012-07-23");

                        var soldeClient = db.Soldes23072012.SingleOrDefault(p => p.code == client.CodeSage);
                        if (soldeClient == null)
                        {
                            client.SoldeHistory = 0;
                        }
                        else
                        {
                            if (soldeClient.debit != null && soldeClient.debit.ParseToDec() != 0)
                            {
                                decimal? deb = soldeClient.debit.Replace(".", ",").ParseToDec();
                                client.SoldeHistory = -deb;
                            }
                            else if (soldeClient.credit != null && soldeClient.credit.ParseToDec() != 0)
                            {
                                decimal? cred = soldeClient.credit.Replace(".", ",").ParseToDec();
                                client.SoldeHistory = cred;
                            }
                            else
                            {

                            }
                        }



                        if (client.SoldeHistory == null)
                        {
                            client.SoldeHistory = 0;
                        }

                        client.Solde = client.SoldeHistory + operationC.Sum(p => p.Montant) - operationD.Sum(p => p.Montant) - factures.Sum(p => p.MontantTTC);
                        client.DateMajSoldeHistory = DateTime.Now;
                    }
                    db.SaveChanges();
                }

                #endregion

                var clientsRep = db.Clients.Where(p => p.Solde != 0 && p.ID != 0 && p.IsSoldable == true && p.ClientTypeID != 3/*divers*/).ToList();
                Tools.ClearPrintTemp();
                foreach (var client in clientsRep)
                {
                    var dernierVers = client.Operations.Where(p => p.Sen == "C" && p.Date <= to).OrderByDescending(p => p.Date).FirstOrDefault();
                    var dernierFact = client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré && p.Date <= to).OrderByDescending(p => p.Date).FirstOrDefault();
                    var dernierVersMontant = client.Operations.Where(p => p.Sen == "C" && p.Date == dernierVers.Date).Sum(p => p.Montant);
                    var dernierFactMontantTTC = client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré && p.Date == dernierFact.Date).Sum(p => p.MontantTTC);

                    db.AddToPrintTemps(new PrintTemp()
                    {
                        UID = Tools.CurrentUserID,
                        Nom = client.NomClientAndParent(),
                        MontantC = (client.Solde > 0) ? client.Solde : 0,
                        MontantD = (client.Solde < 0) ? client.Solde : 0,
                        val3 = (dernierVers != null) ? dernierVers.Date.Value.ToShortDateString() : "",
                        val1 = (dernierVers != null) ? dernierVersMontant.ToString() : "0",
                        val4 = (dernierFact != null) ? dernierFact.Date.Value.ToShortDateString() : "",
                        val2 = (dernierFact != null) ? dernierFactMontantTTC.ToString() : "0",
                        Etat = (client.take == true) ? "Clients Actifs" : "Autres Clients"
                    });
                }

                db.SaveChangeTry();
            }
        }

        internal static void PrintExigenceClient(ExigenceClient exi)
        {
            CR_ExigenceClient ex = new CR_ExigenceClient();
            ex.SetParameterValue("@ClientID", exi.ClientID);
            ex.SetParameterValue("@Annee", exi.DateDebut.Year);
            PrintReport(ex);
        }


    }
}
