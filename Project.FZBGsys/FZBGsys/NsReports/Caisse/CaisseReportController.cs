﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FZBGsys.NsTools;
using System.IO;
using FZBGsys.NsReports.Activite;


namespace FZBGsys.NsReports.Caisse
{
    class CaisseReportController : ReportController
    {

        internal static void PrintOperationsCustom(string filterTxt)
        {
            var report = new CR_OperationsCustom();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            report.SetParameterValue("filterTxt", filterTxt);
           PrintReport(report);
        }

        internal static void PrintCaisseJournee(DateTime day, decimal? anSolde, string dateAN)
        {
            CR_JourneeCiasse report = new CR_JourneeCiasse();
            var To = day;
            var From = day;
            report.SetParameterValue("@To", To);
            report.SetParameterValue("@From", From);
            if (anSolde != null)
            {
                report.SetParameterValue("SoldeAN", anSolde);
            }
            else
            {
                report.SetParameterValue("SoldeAN", 0);
            }

            report.SetParameterValue("DateAn", dateAN);
           PrintReport(report);

        }

        internal static void PrintSortieCaisseJournee(DateTime day, decimal? anSolde, string dateAN)
        {
            CR_EtatSortieCaisse report = new CR_EtatSortieCaisse();
            report.SetParameterValue("@date", day);
            if (anSolde != null)
            {
                report.SetParameterValue("SoldeAN", anSolde);
            }
            else
            {
                report.SetParameterValue("SoldeAN", 0);
            }

            report.SetParameterValue("DateAn", dateAN);
           PrintReport(report, print: true);

        }



        internal static void PrintOperationsPeriode(DateTime from, DateTime to)
        {
            var report = new CR_OperationsPeriode();
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);

           PrintReport(report);

        }





        internal static void PrintMouvementCaisseJournee(DateTime day, decimal? anSolde, string dateAN)
        {
            CR_EtatMouvementCaisseJour report = new CR_EtatMouvementCaisseJour();
            report.SetParameterValue("@date", day);
            if (anSolde != null)
            {
                report.SetParameterValue("SoldeAN", anSolde);
            }
            else
            {
                report.SetParameterValue("SoldeAN", 0);
            }

            report.SetParameterValue("DateAn", dateAN);
           PrintReport(report);

        }

        internal static void PrintMouvementRecette(DateTime From, DateTime To)
        {
            CR_EtatMouvementRecette report = new CR_EtatMouvementRecette();
            report.SetParameterValue("@From", From);
            report.SetParameterValue("@To", To);
            //report.SetParameterValue("SoldeAN", 0);
            //report.SetParameterValue("DateAn", dateAN);
           PrintReport(report);

        }

        internal static void PrintMouvementRecette(DateTime day, decimal? anSolde, string dateAN)
        {
            CR_EtatMouvementRecette report = new CR_EtatMouvementRecette();
            report.SetParameterValue("@date", day);
            if (anSolde != null)
            {
                report.SetParameterValue("SoldeAN", anSolde);
            }
            else
            {
                report.SetParameterValue("SoldeAN", 0);
            }

            report.SetParameterValue("DateAn", dateAN);
           PrintReport(report);

        }

    }
}
