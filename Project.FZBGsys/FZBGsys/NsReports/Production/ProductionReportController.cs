﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FZBGsys.NsReports.Production
{
    class ProductionReportController: ReportController
    {
        //---------------------------------------------------------- List Production 
        internal static void PrintProductions(string FilterTxt)
        {
            var rapport = new CR_Productions();
            rapport.SetParameterValue("FilterTxt", FilterTxt);
            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            PrintReport(rapport, print: true);
        }


        //------------------------------------ Souflage

        internal static void PrintSoufflage(int productionID)
        {
            CR_Soufflage rapport = new CR_Soufflage();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        internal static void PrintCanette(int productionID)
        {
            CR_CanetteVide rapport = new CR_CanetteVide();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }
        internal static void PrintBarquette(int productionID)
        {
            CR_Barquette rapport = new CR_Barquette();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }
        internal static void PrintCouvercle(int productionID)
        {
            CR_Couvercle rapport = new CR_Couvercle();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }
        internal static void PrintBouteilleVide(int productionID)
        {
            CR_BouteilleVide rapport = new CR_BouteilleVide();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }


        //------------------------------ bouchon
        internal static void PrintBouchon(int productionID)
        {
            CR_Bouchon rapport = new CR_Bouchon();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }


        //------------------------------ Etiquette
        internal static void PrintEtiquette(int productionID)
        {
            CR_Etiquette rapport = new CR_Etiquette();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //------------------------------ Siroperie
        internal static void PrintSiropherie(int productionID)
        {
            CR_Siroperie rapport = new CR_Siroperie();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //------------------------------ FilmThermo
        internal static void PrintFilmThermo(int productionID)
        {
            CR_FilmThermo rapport = new CR_FilmThermo();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }

        //---------------------------- Suivi jouranlier production
        internal static void PrintSuiviJournalierProduction(string CNom, string UNom, string title, string ligne, string equipeTxt)
        {
            CR_SuiviJournalierProduction rapport = new CR_SuiviJournalierProduction();

            rapport.SetParameterValue("@UtilisateurID", Tools.CurrentUserID);
            rapport.SetParameterValue("CNom", CNom);
            rapport.SetParameterValue("Title", title);
            rapport.SetParameterValue("UNom", UNom);
            rapport.SetParameterValue("ligne", ligne);
            rapport.SetParameterValue("equipeTxt", equipeTxt);
            PrintReport(rapport);
        }


        //--------------------------- Rapport production
        internal static void PrintRapoortJournalierProduction(int productionID)
        {
            CR_RapoortJournalierProduction rapport = new CR_RapoortJournalierProduction();
            rapport.SetParameterValue("@ProductionID", productionID);
            PrintReport(rapport);
        }


        #region activite report method



        #endregion



        internal static void PrintProductionCustom(DateTime from, DateTime to, int section, string sectionNOm)
        {
            var report = new CR_ProductionCustom();
            report.SetParameterValue("@Section", section);
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);
            report.SetParameterValue("SectionNom", sectionNOm);
            PrintReport(report);

        }

    }
}
