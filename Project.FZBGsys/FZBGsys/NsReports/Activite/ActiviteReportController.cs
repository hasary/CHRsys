﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using FZBGsys.NsTools;
using System.IO;
using System.Linq;

namespace FZBGsys.NsReports.Activite
{
    class ActiviteReportController : ReportController
    {
        public static string GeneratePDFRapportActivite(BackgroundWorker bgw, DateTime from, DateTime to)
        {

            if (bgw != null)
            {
                bgw.ReportProgress(1);
            }
            MergeEx.TakeFirstPageOnly = new List<bool>();
            ExportPDF = true;

            var listFiles = new List<string>();
            //0 
            var mainpage = new CR_AC_Main();

            ExportFileName = "Sommaire";
            mainpage.SetParameterValue("MonthName", from.ToString("MMMM yyyy").ToUpper());
            PrintReport(mainpage);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(true);
            if (bgw != null)
            {
                bgw.ReportProgress(1);
            }

            //1


            ExportFileName = "Stock Initial Magazin";
            PrintAC_StockInitialMatiere(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(2);
            }


            ExportFileName = "Arrivage Matière Première";
            PrintAC_ArrivageMatiere(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(3);
            }


            //3
            ExportFileName = "Consomation matière prmière";
            PrintAC_SuiviMatiereProduction(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(4);
            }


            ExportFileName = "Stock Final Magazin";
            PrintAC_StockFinalMatiere(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(5);
            }

            //2
            ExportFileName = "Temps d'arrets et de Production";
            PrintAC_SuiviTempsProduction(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);

            if (bgw != null)
            {
                bgw.ReportProgress(6);
            }

            //4
            ExportFileName = "Produit fini réalisé";
            PrintAC_SuiviQttProduction(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(7);
            }
            //5
            ExportFileName = "Etat des Ventes";
            PrintAC_Ventes(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(8);
            }

            //6
            ExportFileName = "Statistiques Clients";
            PrintAC_StatistiquesClient(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(true);
            if (bgw != null)
            {
                bgw.ReportProgress(9);
            }

            //7
            ExportFileName = "Activie des Clients Importants";
            PrintAC_ActiviteComerciale(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(10);

            }
            //8

            ExportFileName = "Mouvements de Caisse Espèce";
            PrintAC_Caisse(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(11);

            }

            ExportFileName = "Mouvements de Caisse Global";
            PrintACRecapMouvementCaisse(from, to);
            listFiles.Add(ExportFileName);
            MergeEx.TakeFirstPageOnly.Add(false);
            if (bgw != null)
            {
                bgw.ReportProgress(12);

            }


            MergeEx ex = new MergeEx();


            FileInfo fn = new FileInfo(ExportFileName);
            ex.SourceFolder = fn.Directory.ToString() + "\\";
            ex.DestinationFile = fn.Directory + "\\" + "Rapport_Mensuel_FZBG_" + from.ToString("MMMM_yyyy") + ".pdf";

            foreach (var file in listFiles)
            {
                if (fn != null)
                {
                    fn = new FileInfo(file);
                    ex.AddFile(fn.Name);
                }
            }



            ex.Execute();

            if (bgw != null)
            {
                bgw.ReportProgress(13);
            }
            return ex.DestinationFile;
        }




        internal static void PrintAC_ArrivageMatiere(DateTime from, DateTime to)
        {
            var repport = new CR_AC_ArrivageMatiere();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_StockInitialMatiere(DateTime from, DateTime to)
        {
            var repport = new CR_AC_StockInitialMatiere();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_StockFinalMatiere(DateTime from, DateTime to)
        {
            var repport = new CR_AC_StockFinalMatiere();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }



        internal static void PrintAC_Caisse(DateTime from, DateTime to)
        {
            var repport = new CR_AC_Caisse();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_StatistiquesClient(DateTime from, DateTime to)
        {
            var repport = new CR_AC_StatistiquesClient();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_SuiviMatiereProduction(DateTime from, DateTime to)
        {
            var repport = new CR_AC_SuiviMatiereProduction();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_SuiviQttProduction(DateTime from, DateTime to)
        {
            var repport = new CR_AC_SuiviQttProduction();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_SuiviTempsProduction(DateTime from, DateTime to)
        {
            var repport = new CR_AC_SuiviTempsProduction();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintAC_Ventes(DateTime from, DateTime to)
        {
            var repport = new CR_AC_Ventes();
            repport.SetParameterValue("@from", from);
            repport.SetParameterValue("@to", to);
            PrintReport(repport);
        }

        internal static void PrintACRecapMouvementCaisse(DateTime from, DateTime to)
        {
            var report = new CR_AC_CaisseGlobal();
            report.SetParameterValue("@from", from);
            report.SetParameterValue("@to", to);

            PrintReport(report);

        }

        internal static void PrintAC_ActiviteComerciale(DateTime from, DateTime to)
        {
            PrepareACActiviteClient(from, to);
            var report = new CR_AC_ActiviteComerciale();
            report.SetParameterValue("@UID", Tools.CurrentUserID);
            report.SetParameterValue("dateTxt", to.ToShortDateString());
            report.SetParameterValue("yesTxt", from.AddDays(-1).ToShortDateString());
            PrintReport(report);
        }


        private static void PrepareACActiviteClient(DateTime from, DateTime to)
        {

            //var date = DateTime.Parse("2012-08-12").Date;
            var fromDate = from; // date.AddDays(-1).Date;

            using (var db = new ModelEntities())
            {
                var clients = new List<Client>();

                var factures = db.Factures.Where(p => p.Date <= to && p.Date >= from && p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré).ToList();
                foreach (var fact in factures)
                {
                    if (!clients.Contains(fact.Client)) clients.Add(fact.Client);
                }

                var operations = db.Operations.Where(p => p.Date <= to && p.Date >= from && p.ClientID != null && p.ClientID != 0).ToList();
                foreach (var oper in operations)
                {
                    if (!clients.Contains(oper.Client)) clients.Add(oper.Client);
                }


                Tools.ClearPrintTemp();
                foreach (var client in clients)
                {

                    if (client.ID == 3296)
                    {

                    }
                    var operaionsClient = operations.Where(p => p.ClientID == client.ID);
                    var facturesClient = factures.Where(p => p.ClientID == client.ID);

                    var versement = operaionsClient.Sum(p => p.Montant);


                    var achatComptant = facturesClient.Where(p => p.ModePayement == (int)EnModePayement.Especes || p.ModePayement == (int)EnModePayement.Chèque || p.ModePayement == (int)EnModePayement.Virement).Sum(p => p.MontantTTC);
                    var achatAvance = facturesClient.Where(p => p.ModePayement == (int)EnModePayement.Avance || p.ModePayement == (int)EnModePayement.Traite).Sum(p => p.MontantTTC);
                    var achatAterme = facturesClient.Where(p => p.ModePayement == (int)EnModePayement.a_Terme).Sum(p => p.MontantTTC);


                    var soldeYesterday = client.SoldeHistory;
                    var dateHi = client.DateHistory;
                    if (dateHi != null)
                    {
                        soldeYesterday -= client.Factures.Where(p => p.Etat != (int)EnEtatFacture.Annulée && p.BonAttribution.Etat == (int)EnEtatBonAttribution.Livré && p.Date > dateHi && p.Date <= fromDate).Sum(p => p.MontantTTC);
                        soldeYesterday -= client.Operations.Where(p => p.Sen == "D" && p.Date > dateHi && p.Date <= fromDate).Sum(p => p.Montant);
                        soldeYesterday += client.Operations.Where(p => p.Sen == "C" && p.Date > dateHi && p.Date <= fromDate).Sum(p => p.Montant);
                    }
                    if (soldeYesterday.Value == -79413740.49m) //-79413740.49
                    {

                    }
                    var newSolde = soldeYesterday + versement - achatAterme - achatAvance - achatComptant;
                    db.AddToPrintTemps(new PrintTemp
                    {
                        UID = Tools.CurrentUserID,
                        Nom = client.NomClientAndParent(),
                        MontantC = soldeYesterday,
                        dec4 = versement,
                        dec1 = achatComptant,
                        dec2 = achatAvance,
                        dec3 = achatAterme,
                        MontantD = newSolde,
                    });

                }
                db.SaveChangeTry();

            }
        }
    }
}
