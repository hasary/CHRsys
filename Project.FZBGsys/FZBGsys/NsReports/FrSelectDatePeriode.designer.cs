﻿namespace FZBGsys.NsReports
{
    partial class FrSelectDatePeriode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerJourn = new System.Windows.Forms.DateTimePicker();
            this.radioButtonDate = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriode = new System.Windows.Forms.RadioButton();
            this.panelPeriode = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePickerDu = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerAu = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButtonMon = new System.Windows.Forms.RadioButton();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerMonth = new System.Windows.Forms.DateTimePicker();
            this.cbwilaya = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panW = new System.Windows.Forms.Panel();
            this.panelPeriode.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panW.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimePickerJourn
            // 
            this.dateTimePickerJourn.CustomFormat = "dd MMMM yyyy";
            this.dateTimePickerJourn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerJourn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerJourn.Location = new System.Drawing.Point(132, 32);
            this.dateTimePickerJourn.Name = "dateTimePickerJourn";
            this.dateTimePickerJourn.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerJourn.TabIndex = 27;
            // 
            // radioButtonDate
            // 
            this.radioButtonDate.AutoSize = true;
            this.radioButtonDate.Checked = true;
            this.radioButtonDate.Location = new System.Drawing.Point(22, 32);
            this.radioButtonDate.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonDate.Name = "radioButtonDate";
            this.radioButtonDate.Size = new System.Drawing.Size(79, 17);
            this.radioButtonDate.TabIndex = 29;
            this.radioButtonDate.TabStop = true;
            this.radioButtonDate.Text = "Journalière:";
            this.radioButtonDate.UseVisualStyleBackColor = true;
            this.radioButtonDate.CheckedChanged += new System.EventHandler(this.radioButtonDate_CheckedChanged);
            // 
            // radioButtonPeriode
            // 
            this.radioButtonPeriode.AutoSize = true;
            this.radioButtonPeriode.Location = new System.Drawing.Point(22, 98);
            this.radioButtonPeriode.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonPeriode.Name = "radioButtonPeriode";
            this.radioButtonPeriode.Size = new System.Drawing.Size(78, 17);
            this.radioButtonPeriode.TabIndex = 30;
            this.radioButtonPeriode.Text = "Périodique:";
            this.radioButtonPeriode.UseVisualStyleBackColor = true;
            this.radioButtonPeriode.CheckedChanged += new System.EventHandler(this.radioButtonPeriode_CheckedChanged);
            // 
            // panelPeriode
            // 
            this.panelPeriode.Controls.Add(this.label3);
            this.panelPeriode.Controls.Add(this.dateTimePickerDu);
            this.panelPeriode.Controls.Add(this.dateTimePickerAu);
            this.panelPeriode.Controls.Add(this.label5);
            this.panelPeriode.Location = new System.Drawing.Point(58, 119);
            this.panelPeriode.Margin = new System.Windows.Forms.Padding(2);
            this.panelPeriode.Name = "panelPeriode";
            this.panelPeriode.Size = new System.Drawing.Size(237, 53);
            this.panelPeriode.TabIndex = 28;
            this.panelPeriode.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Au:";
            // 
            // dateTimePickerDu
            // 
            this.dateTimePickerDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerDu.Location = new System.Drawing.Point(74, 5);
            this.dateTimePickerDu.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerDu.Name = "dateTimePickerDu";
            this.dateTimePickerDu.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerDu.TabIndex = 0;
            // 
            // dateTimePickerAu
            // 
            this.dateTimePickerAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerAu.Location = new System.Drawing.Point(74, 30);
            this.dateTimePickerAu.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerAu.Name = "dateTimePickerAu";
            this.dateTimePickerAu.Size = new System.Drawing.Size(151, 20);
            this.dateTimePickerAu.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Du:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(214, 277);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 24);
            this.button1.TabIndex = 31;
            this.button1.Text = "Générer le Rapport";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButtonMon
            // 
            this.radioButtonMon.AutoSize = true;
            this.radioButtonMon.Location = new System.Drawing.Point(22, 66);
            this.radioButtonMon.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonMon.Name = "radioButtonMon";
            this.radioButtonMon.Size = new System.Drawing.Size(76, 17);
            this.radioButtonMon.TabIndex = 33;
            this.radioButtonMon.TabStop = true;
            this.radioButtonMon.Text = "Mensuelle:";
            this.radioButtonMon.UseVisualStyleBackColor = true;
            this.radioButtonMon.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(15, 278);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar1.MarqueeAnimationSpeed = 20;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(184, 19);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 34;
            this.progressBar1.Value = 20;
            this.progressBar1.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panW);
            this.groupBox1.Controls.Add(this.dateTimePickerMonth);
            this.groupBox1.Controls.Add(this.radioButtonPeriode);
            this.groupBox1.Controls.Add(this.panelPeriode);
            this.groupBox1.Controls.Add(this.radioButtonMon);
            this.groupBox1.Controls.Add(this.dateTimePickerJourn);
            this.groupBox1.Controls.Add(this.radioButtonDate);
            this.groupBox1.Location = new System.Drawing.Point(15, 26);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(308, 237);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selection de dates / période";
            // 
            // dateTimePickerMonth
            // 
            this.dateTimePickerMonth.CustomFormat = "MMMM yyyy";
            this.dateTimePickerMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMonth.Location = new System.Drawing.Point(132, 66);
            this.dateTimePickerMonth.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePickerMonth.Name = "dateTimePickerMonth";
            this.dateTimePickerMonth.ShowUpDown = true;
            this.dateTimePickerMonth.Size = new System.Drawing.Size(122, 20);
            this.dateTimePickerMonth.TabIndex = 34;
            this.dateTimePickerMonth.Visible = false;
            // 
            // cbwilaya
            // 
            this.cbwilaya.FormattingEnabled = true;
            this.cbwilaya.Items.AddRange(new object[] {
            "Tout",
            "Adrar",
            "Chlef",
            "Laghouat",
            "Oum-El-Bouaghi",
            "Batna",
            "Bejaïa",
            "Biskra",
            "Bechar",
            "Blida",
            "Bouira",
            "Tamanrasset",
            "Tebessa",
            "Tlemcen",
            "Tiaret",
            "Tizi-Ouzou",
            "Alger",
            "Djelfa",
            "Jijel",
            "Sétif",
            "Saïda",
            "Skikda",
            "Sidi Bel Abbès",
            "Annaba",
            "Guelma",
            "Constantine",
            "Médéa",
            "Mostaganem",
            "M\'sila",
            "Mascara",
            "Ouargla",
            "Oran",
            "El Bayadh",
            "Illizi",
            "Bordj Bou Arréridj",
            "Boumerdès",
            "El Tarf",
            "Tindouf",
            "Tissemsilt",
            "El Oued",
            "Khenchela",
            "Souk Ahras",
            "Tipaza",
            "Mila",
            "Aïn Defla",
            "Naama",
            "Aïn Temouchent",
            "Ghardaïa",
            "Relizane"});
            this.cbwilaya.Location = new System.Drawing.Point(110, 10);
            this.cbwilaya.Name = "cbwilaya";
            this.cbwilaya.Size = new System.Drawing.Size(146, 21);
            this.cbwilaya.TabIndex = 35;
            this.cbwilaya.SelectedIndexChanged += new System.EventHandler(this.cbwilaya_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "WILAYA";
            // 
            // panW
            // 
            this.panW.Controls.Add(this.cbwilaya);
            this.panW.Controls.Add(this.label1);
            this.panW.Location = new System.Drawing.Point(22, 182);
            this.panW.Name = "panW";
            this.panW.Size = new System.Drawing.Size(274, 39);
            this.panW.TabIndex = 37;
            this.panW.Visible = false;
            // 
            // FrSelectDatePeriode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 315);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrSelectDatePeriode";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rapport Personalisé";
            this.panelPeriode.ResumeLayout(false);
            this.panelPeriode.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panW.ResumeLayout(false);
            this.panW.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerJourn;
        private System.Windows.Forms.RadioButton radioButtonDate;
        private System.Windows.Forms.RadioButton radioButtonPeriode;
        private System.Windows.Forms.Panel panelPeriode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePickerDu;
        private System.Windows.Forms.DateTimePicker dateTimePickerAu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButtonMon;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerMonth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbwilaya;
        private System.Windows.Forms.Panel panW;
    }
}