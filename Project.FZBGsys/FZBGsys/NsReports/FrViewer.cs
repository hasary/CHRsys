﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FZBGsys.NsReports;
using System.IO;
using System.Diagnostics;
using FZBGsys.NsTools;


namespace FZBGsys.NsReports
{

    public partial class FrViewer : Form
    {

        public FrViewer(bool groupTree = false)
        {
            InitializeComponent();
            AutoriseImpression();
            if (groupTree)
            {
                crystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.GroupTree;


            }
        }
        public void SetReportSource(CrystalDecisions.CrystalReports.Engine.ReportClass report)
        {
            this.crystalReportViewer1.ReportSource = report;
        }
        private void AutoriseImpression()
        {


            if (Tools.ParametreAutorisation.Autorisation == true)
            {
                crystalReportViewer1.ShowPrintButton = true;
                crystalReportViewer1.ShowExportButton = true;
                crystalReportViewer1.ShowCopyButton = true;
            }
            else
            {
                crystalReportViewer1.ShowPrintButton = false;
                crystalReportViewer1.ShowExportButton = false;
                crystalReportViewer1.ShowCopyButton = false;
            }

        }








    }
}
