﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsReports
{
    public partial class FrSelectDatePeriode : Form
    {
        int wil=0;
        public FrSelectDatePeriode(bool monthOnly = false)
        {
            InitializeComponent();

            InitialiseDate();
            SelectionDone = false;
            if (monthOnly)
            {


            }
            
            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        public FrSelectDatePeriode(int Only)
        {
            InitializeComponent();

            InitialiseDate();
            SelectionDone = false;
            radioButtonDate.Visible = radioButtonDate.Checked = Only == 0;
            radioButtonMon.Visible = radioButtonMon.Checked = Only == 1;
            radioButtonPeriode.Visible = radioButtonPeriode.Checked = Only == 1;



            //   DateTimePicker dateTimePicker1 = new DateTimePicker();


        }

        private void InitialiseDate()
        {
            string[] monthText = { "Ja", "" };
            Dictionary<string, string> months = new Dictionary<string, string>();

            DateTime fin = DateTime.Now;
            DateTime debut = DateTime.Parse("2012-05-01");

            for (DateTime i = debut; i < fin; i = i.AddMonths(1))
            {
                // months.Add(i.Month,
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerMonth.Visible = radioButtonMon.Checked;
        }

        private void radioButtonPeriode_CheckedChanged(object sender, EventArgs e)
        {
            panelPeriode.Visible = radioButtonPeriode.Checked;
        }

        private void radioButtonDate_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePickerJourn.Visible = radioButtonDate.Checked;
        }

        public static DateTime From { get; set; }
        public static DateTime To { get; set; }
        public static String W { get; set; }
        public static bool SelectionDone = false;
        private void button1_Click(object sender, EventArgs e)
        {
            SelectionDone = true;
            button1.Enabled = false;
            DateTime du = DateTime.Now;
            DateTime au = DateTime.Now;
            
            
            if (radioButtonDate.Checked)
            {
                du = au = dateTimePickerJourn.Value;

            }

            if (radioButtonPeriode.Checked)
            {
                du = dateTimePickerDu.Value;
                au = dateTimePickerAu.Value;
            }
            if (radioButtonMon.Checked)
            {
                var dt = dateTimePickerMonth.Value;
                au = new DateTime(dt.Year, dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));
                du = new DateTime(dt.Year, dt.Month, 1);
            }
            
            //   Actions.Rapports.FrViewer.PrintRecapShowroom(du, au);
            From = du;
            To = au;
            //    progressBar1.Visible = false;
            button1.Enabled = true;

            Dispose();

        }


        public static DateTime[] SelectPeriode(bool MonthOnly = false)
        {
            int ID = Tools.CurrentUserID;
            using (ModelEntities db = new ModelEntities())
            {
                var Periodique = db.ParametreAutorisations.Single(p => p.UtilisateurID == ID && p.ParametreID == (int)EnParametre.RapportPeriodique);
                var Mensual = db.ParametreAutorisations.Single(p => p.UtilisateurID == ID && p.ParametreID == (int)EnParametre.RapportMensual);
                if (!Periodique.Autorisation && !Mensual.Autorisation)
                    return new DateTime[2] { Tools.CurrentDateTime.Value.Date, Tools.CurrentDateTime.Value.Date };


                /*  if (Tools.CurrentUser.GroupeID != 1)
                      return new DateTime[2] { Tools.CurrentDateTime.Value.Date, Tools.CurrentDateTime.Value.Date };*/

                FrSelectDatePeriode form = new FrSelectDatePeriode();
                form.radioButtonDate.Enabled = true;
                form.radioButtonMon.Checked = true;
                if (!Periodique.Autorisation) MonthOnly = true;
                if (MonthOnly)
                {
                    form.radioButtonMon.Checked = true;

                    form.radioButtonPeriode.Enabled = false;
                }

                form.ShowDialog();
                if (!SelectionDone)
                {
                    return null;
                }


                return new DateTime[2] { FrSelectDatePeriode.From, FrSelectDatePeriode.To };

            }
        }

        public static DateTime[] SelectPeriodeWilaya(bool MonthOnly = false)
        {
            FrSelectDatePeriode form = new FrSelectDatePeriode();
            form.radioButtonDate.Enabled = true;
            form.radioButtonMon.Checked = true;
            if (MonthOnly)
            {
                form.radioButtonMon.Checked = true;

                form.radioButtonPeriode.Enabled = false;
            }
            form.panW.Visible = true;

            form.ShowDialog();
            if (!SelectionDone)
            {
                return null;
            }


            return new DateTime[2] { FrSelectDatePeriode.From, FrSelectDatePeriode.To };


        }

        
        public static DateTime[] SelectDay()
        {
            FrSelectDatePeriode form = new FrSelectDatePeriode(0);
            /*  if (MonthOnly)
              {
                  form.radioButtonMon.Checked = true;
                  form.radioButtonDate.Enabled = false;
                  form.radioButtonPeriode.Enabled = false;
              }*/
            form.ShowDialog();
            if (!SelectionDone)
            {
                return null;
            }
            return new DateTime[2] { FrSelectDatePeriode.From, FrSelectDatePeriode.To };


        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void cbwilaya_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
                 W = cbwilaya.SelectedItem.ToString();
        }

        
    }
}
