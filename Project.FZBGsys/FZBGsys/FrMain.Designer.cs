﻿namespace FZBGsys
{
    partial class FrMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrMain));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.Fichier = new System.Windows.Forms.ToolStripMenuItem();
            this.FichierConnecter = new System.Windows.Forms.ToolStripMenuItem();
            this.MainChangePasse = new System.Windows.Forms.ToolStripMenuItem();
            this.FichierDeconnecter = new System.Windows.Forms.ToolStripMenuItem();
            this.FichierQuitter = new System.Windows.Forms.ToolStripMenuItem();
            this.Arrivage = new System.Windows.Forms.ToolStripMenuItem();
            this.ArrivageMP = new System.Windows.Forms.ToolStripMenuItem();
            this.ArrivageListMP = new System.Windows.Forms.ToolStripMenuItem();
            this.ArrivagelistFourni = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesFournisseursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evalusationFournisseursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesMatieresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesMatieresPrimièreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesArromesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.couleursBouchonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeColorantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Production = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionPET = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionCan = new System.Windows.Forms.ToolStripMenuItem();
            this.productuionVer = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductionListPET = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.gestionDesProduitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesProduitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesForamtsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesPrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Transfert = new System.Windows.Forms.ToolStripMenuItem();
            this.TransfertMT = new System.Windows.Forms.ToolStripMenuItem();
            this.versUnitéPETToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versUnitéCanetteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versAutresDéstinationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TransfertListMP = new System.Windows.Forms.ToolStripMenuItem();
            this.TransfertPF = new System.Windows.Forms.ToolStripMenuItem();
            this.listTransfertsPF = new System.Windows.Forms.ToolStripMenuItem();
            this.Stock = new System.Windows.Forms.ToolStripMenuItem();
            this.StockMP = new System.Windows.Forms.ToolStripMenuItem();
            this.etatDuStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.demandeBesoinToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesDemandesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StockPF = new System.Windows.Forms.ToolStripMenuItem();
            this.Dist = new System.Windows.Forms.ToolStripMenuItem();
            this.mouvementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesMouvementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Comercial = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialCommand = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvelleCommandeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesCommandesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialBonAttrib = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialBonSort = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialFacture = new System.Windows.Forms.ToolStripMenuItem();
            this.facturationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transfertVersSageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialBonLiv = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesChaffeursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ComercialBonRetour = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialFactRetour = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.CommercialListeBon = new System.Windows.Forms.ToolStripMenuItem();
            this.transfertSageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ComercialListVentes = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ComercialClient = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesClientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.satisfactionClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.listeDesChauffeurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Caisse = new System.Windows.Forms.ToolStripMenuItem();
            this.CaisseOpClient = new System.Windows.Forms.ToolStripMenuItem();
            this.CaisseOpDivers = new System.Windows.Forms.ToolStripMenuItem();
            this.exportVersSageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Rapports = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportMP = new System.Windows.Forms.ToolStripMenuItem();
            this.matièrePremièreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evaluationFournissersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suivieDeStockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportPF = new System.Windows.Forms.ToolStripMenuItem();
            this.etatDesStocksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etatDesMouvementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportProduction = new System.Windows.Forms.ToolStripMenuItem();
            this.transfertMatièrePremièreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unitéVerreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportEtatVente = new System.Windows.Forms.ToolStripMenuItem();
            this.journalDesVentesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistiquesDesArticlesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistiquesDesClientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.situationFlotteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.satisfactionClientToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.releverCompteClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.situationComericaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etatJournalierDesMouvementsClientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportActivite = new System.Windows.Forms.ToolStripMenuItem();
            this.RapportCaisse = new System.Windows.Forms.ToolStripMenuItem();
            this.etatJournalierDesDépensesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etatJournalierDesMouvementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etatRécapitulatifDesMouvementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.releverCompteClientToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.rapportVente = new System.Windows.Forms.ToolStripMenuItem();
            this.etatJournalierDesMouvementsClientsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.releveCompteClientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistiquesDesArticlesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.situationFlotteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.situationComericaleToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.satisfactionClientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Administration = new System.Windows.Forms.ToolStripMenuItem();
            this.AdministrationUtilisateur = new System.Windows.Forms.ToolStripMenuItem();
            this.AdministrationAutorisation = new System.Windows.Forms.ToolStripMenuItem();
            this.AdministrationMaintenance = new System.Windows.Forms.ToolStripMenuItem();

            this.parèmetresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AdministrationBackup = new System.Windows.Forms.ToolStripMenuItem();
            this.backupDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.Aide = new System.Windows.Forms.ToolStripMenuItem();
            this.AideApropos = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxInfos = new System.Windows.Forms.GroupBox();
            this.panTraitement = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.progressMain = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.labelUserSection = new System.Windows.Forms.Label();
            this.labeluserRole = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();

            this.parèmetresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.bgwSituationCommercial = new System.ComponentModel.BackgroundWorker();
            this.bk1 = new System.ComponentModel.BackgroundWorker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();

            this.menuMain.SuspendLayout();
            this.groupBoxInfos.SuspendLayout();
            this.panTraitement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Fichier,
            this.Arrivage,
            this.Production,
            this.Transfert,
            this.Stock,
            this.Comercial,
            this.Rapports,
            this.Administration,
            this.Aide});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuMain.Size = new System.Drawing.Size(764, 24);
            this.menuMain.TabIndex = 0;
            this.menuMain.Text = "menuStrip1";
            this.menuMain.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuMain_ItemClicked);
            // 
            // Fichier
            // 
            this.Fichier.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FichierConnecter,
            this.MainChangePasse,
            this.FichierDeconnecter,
            this.FichierQuitter});
            this.Fichier.Name = "Fichier";
            this.Fichier.Size = new System.Drawing.Size(54, 20);
            this.Fichier.Text = "Fichier";
            // 
            // FichierConnecter
            // 
            this.FichierConnecter.Name = "FichierConnecter";
            this.FichierConnecter.Size = new System.Drawing.Size(216, 22);
            this.FichierConnecter.Text = "Connecter";
            this.FichierConnecter.Click += new System.EventHandler(this.FichierConnecter_Click);
            // 
            // MainChangePasse
            // 
            this.MainChangePasse.Name = "MainChangePasse";
            this.MainChangePasse.Size = new System.Drawing.Size(216, 22);
            this.MainChangePasse.Text = "Changement mot de passe";
            this.MainChangePasse.Click += new System.EventHandler(this.MainChangePasse_Click);
            // 
            // FichierDeconnecter
            // 
            this.FichierDeconnecter.Name = "FichierDeconnecter";
            this.FichierDeconnecter.Size = new System.Drawing.Size(216, 22);
            this.FichierDeconnecter.Text = "Deconnecter";
            this.FichierDeconnecter.Click += new System.EventHandler(this.FichierDeconnecter_Click);
            // 
            // FichierQuitter
            // 
            this.FichierQuitter.Name = "FichierQuitter";
            this.FichierQuitter.Size = new System.Drawing.Size(216, 22);
            this.FichierQuitter.Text = "Quitter";
            this.FichierQuitter.Click += new System.EventHandler(this.FichierQuitter_Click);
            // 
            // Arrivage
            // 
            this.Arrivage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ArrivageMP,
            this.ArrivageListMP,
            this.ArrivagelistFourni,
            this.gestionDesMatieresToolStripMenuItem});
            this.Arrivage.Name = "Arrivage";
            this.Arrivage.Size = new System.Drawing.Size(63, 20);
            this.Arrivage.Text = "Arrivage";
            // 
            // ArrivageMP
            // 
            this.ArrivageMP.Enabled = false;
            this.ArrivageMP.Name = "ArrivageMP";
            this.ArrivageMP.Size = new System.Drawing.Size(211, 22);
            this.ArrivageMP.Text = "Arrivage Matiere Première";
            this.ArrivageMP.Click += new System.EventHandler(this.matierePremiereToolStripMenuItem_Click);
            // 
            // ArrivageListMP
            // 
            this.ArrivageListMP.Enabled = false;
            this.ArrivageListMP.Name = "ArrivageListMP";
            this.ArrivageListMP.Size = new System.Drawing.Size(211, 22);
            this.ArrivageListMP.Text = "Liste Arrivage MP";
            this.ArrivageListMP.Click += new System.EventHandler(this.ArrivageList_Click);
            // 
            // ArrivagelistFourni
            // 
            this.ArrivagelistFourni.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesFournisseursToolStripMenuItem,
            this.evalusationFournisseursToolStripMenuItem});
            this.ArrivagelistFourni.Name = "ArrivagelistFourni";
            this.ArrivagelistFourni.Size = new System.Drawing.Size(211, 22);
            this.ArrivagelistFourni.Text = "Gestion des Fournisseurs";
            this.ArrivagelistFourni.Click += new System.EventHandler(this.ArrivagelistFourni_Click);
            // 
            // listeDesFournisseursToolStripMenuItem
            // 
            this.listeDesFournisseursToolStripMenuItem.Name = "listeDesFournisseursToolStripMenuItem";
            this.listeDesFournisseursToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.listeDesFournisseursToolStripMenuItem.Text = "Liste des Fournisseurs";
            this.listeDesFournisseursToolStripMenuItem.Click += new System.EventHandler(this.listeDesFournisseursToolStripMenuItem_Click);
            // 
            // evalusationFournisseursToolStripMenuItem
            // 
            this.evalusationFournisseursToolStripMenuItem.Name = "evalusationFournisseursToolStripMenuItem";
            this.evalusationFournisseursToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.evalusationFournisseursToolStripMenuItem.Text = "Evalusation Fournisseurs";
            this.evalusationFournisseursToolStripMenuItem.Click += new System.EventHandler(this.evalusationFournisseursToolStripMenuItem_Click);
            // 
            // gestionDesMatieresToolStripMenuItem
            // 
            this.gestionDesMatieresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesMatieresPrimièreToolStripMenuItem,
            this.listeDesArromesToolStripMenuItem,
            this.couleursBouchonsToolStripMenuItem,
            this.typeColorantsToolStripMenuItem,
            this.listeDesToolStripMenuItem});
            this.gestionDesMatieresToolStripMenuItem.Name = "gestionDesMatieresToolStripMenuItem";
            this.gestionDesMatieresToolStripMenuItem.Size = new System.Drawing.Size(211, 22);
            this.gestionDesMatieresToolStripMenuItem.Text = "Gestion des Matieres";
            this.gestionDesMatieresToolStripMenuItem.Click += new System.EventHandler(this.gestionDesMatieresToolStripMenuItem_Click);
            // 
            // listeDesMatieresPrimièreToolStripMenuItem
            // 
            this.listeDesMatieresPrimièreToolStripMenuItem.Name = "listeDesMatieresPrimièreToolStripMenuItem";
            this.listeDesMatieresPrimièreToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.listeDesMatieresPrimièreToolStripMenuItem.Text = "Liste des Matieres Primières";
            this.listeDesMatieresPrimièreToolStripMenuItem.Click += new System.EventHandler(this.listeDesMatieresPrimièreToolStripMenuItem_Click);
            // 
            // listeDesArromesToolStripMenuItem
            // 
            this.listeDesArromesToolStripMenuItem.Name = "listeDesArromesToolStripMenuItem";
            this.listeDesArromesToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.listeDesArromesToolStripMenuItem.Text = "Liste des Arromes";
            this.listeDesArromesToolStripMenuItem.Click += new System.EventHandler(this.listeDesArromesToolStripMenuItem_Click);
            // 
            // couleursBouchonsToolStripMenuItem
            // 
            this.couleursBouchonsToolStripMenuItem.Name = "couleursBouchonsToolStripMenuItem";
            this.couleursBouchonsToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.couleursBouchonsToolStripMenuItem.Text = "Couleurs Bouchons";
            // 
            // typeColorantsToolStripMenuItem
            // 
            this.typeColorantsToolStripMenuItem.Name = "typeColorantsToolStripMenuItem";
            this.typeColorantsToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.typeColorantsToolStripMenuItem.Text = "Liste des Colorants";
            // 
            // listeDesToolStripMenuItem
            // 
            this.listeDesToolStripMenuItem.Name = "listeDesToolStripMenuItem";
            this.listeDesToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.listeDesToolStripMenuItem.Text = "Liste des Préforme";
            // 
            // Production
            // 
            this.Production.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ProductionPET,
            this.ProductionCan,
            this.productuionVer,
            this.ProductionListPET,
            this.toolStripSeparator3,
            this.gestionDesProduitToolStripMenuItem});
            this.Production.Enabled = false;
            this.Production.Name = "Production";
            this.Production.Size = new System.Drawing.Size(78, 20);
            this.Production.Text = "Production";
            // 
            // ProductionPET
            // 
            this.ProductionPET.Name = "ProductionPET";
            this.ProductionPET.Size = new System.Drawing.Size(208, 22);
            this.ProductionPET.Text = "Production Unité PET";
            this.ProductionPET.Click += new System.EventHandler(this.ProductionPET_Click);
            // 
            // ProductionCan
            // 
            this.ProductionCan.Name = "ProductionCan";
            this.ProductionCan.Size = new System.Drawing.Size(208, 22);
            this.ProductionCan.Text = "Production Unité Canette";
            this.ProductionCan.Click += new System.EventHandler(this.ProductionCan_Click);
            // 
            // productuionVer
            // 
            this.productuionVer.Name = "productuionVer";
            this.productuionVer.Size = new System.Drawing.Size(208, 22);
            this.productuionVer.Text = "Production Unité Verre";
            this.productuionVer.Click += new System.EventHandler(this.productuionVer_Click);
            // 
            // ProductionListPET
            // 
            this.ProductionListPET.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ProductionListPET.Name = "ProductionListPET";
            this.ProductionListPET.Size = new System.Drawing.Size(208, 22);
            this.ProductionListPET.Text = "Liste des Productions";
            this.ProductionListPET.Click += new System.EventHandler(this.ProductionListPET_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(205, 6);
            // 
            // gestionDesProduitToolStripMenuItem
            // 
            this.gestionDesProduitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesProduitsToolStripMenuItem,
            this.listeDesForamtsToolStripMenuItem,
            this.listeDesPrixToolStripMenuItem});
            this.gestionDesProduitToolStripMenuItem.Name = "gestionDesProduitToolStripMenuItem";
            this.gestionDesProduitToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.gestionDesProduitToolStripMenuItem.Text = "Gestion des Produits";
            this.gestionDesProduitToolStripMenuItem.Click += new System.EventHandler(this.gestionDesProduitToolStripMenuItem_Click);
            // 
            // listeDesProduitsToolStripMenuItem
            // 
            this.listeDesProduitsToolStripMenuItem.Name = "listeDesProduitsToolStripMenuItem";
            this.listeDesProduitsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.listeDesProduitsToolStripMenuItem.Text = "Liste des Produits";
            this.listeDesProduitsToolStripMenuItem.Click += new System.EventHandler(this.listeDesProduitsToolStripMenuItem_Click);
            // 
            // listeDesForamtsToolStripMenuItem
            // 
            this.listeDesForamtsToolStripMenuItem.Name = "listeDesForamtsToolStripMenuItem";
            this.listeDesForamtsToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.listeDesForamtsToolStripMenuItem.Text = "Liste des Formats";
            this.listeDesForamtsToolStripMenuItem.Click += new System.EventHandler(this.listeDesForamtsToolStripMenuItem_Click);
            // 
            // listeDesPrixToolStripMenuItem
            // 
            this.listeDesPrixToolStripMenuItem.Name = "listeDesPrixToolStripMenuItem";
            this.listeDesPrixToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.listeDesPrixToolStripMenuItem.Text = "Liste des Prix";
            this.listeDesPrixToolStripMenuItem.Click += new System.EventHandler(this.listeDesPrixToolStripMenuItem_Click);
            // 
            // Transfert
            // 
            this.Transfert.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TransfertMT,
            this.TransfertListMP,
            this.TransfertPF,
            this.listTransfertsPF});
            this.Transfert.Name = "Transfert";

            this.Transfert.Size = new System.Drawing.Size(65, 20);

            this.Transfert.Text = "Transfert";
            // 
            // TransfertMT
            // 
            this.TransfertMT.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versUnitéPETToolStripMenuItem,
            this.versUnitéCanetteToolStripMenuItem,
            this.versAutresDéstinationsToolStripMenuItem});
            this.TransfertMT.Enabled = false;
            this.TransfertMT.Name = "TransfertMT";

            this.TransfertMT.Size = new System.Drawing.Size(213, 22);

            this.TransfertMT.Text = "Transfert Matiere Premiere";
            // 
            // versUnitéPETToolStripMenuItem
            // 
            this.versUnitéPETToolStripMenuItem.Name = "versUnitéPETToolStripMenuItem";

            this.versUnitéPETToolStripMenuItem.Size = new System.Drawing.Size(201, 22);

            this.versUnitéPETToolStripMenuItem.Text = "Vers Unité PET";
            this.versUnitéPETToolStripMenuItem.Click += new System.EventHandler(this.versUnitéPETToolStripMenuItem_Click);
            // 
            // versUnitéCanetteToolStripMenuItem
            // 
            this.versUnitéCanetteToolStripMenuItem.Name = "versUnitéCanetteToolStripMenuItem";

            this.versUnitéCanetteToolStripMenuItem.Size = new System.Drawing.Size(201, 22);

            this.versUnitéCanetteToolStripMenuItem.Text = "Vers Unité Canette";
            this.versUnitéCanetteToolStripMenuItem.Click += new System.EventHandler(this.versUnitéCanetteToolStripMenuItem_Click);
            // 
            // versAutresDéstinationsToolStripMenuItem
            // 
            this.versAutresDéstinationsToolStripMenuItem.Name = "versAutresDéstinationsToolStripMenuItem";

            this.versAutresDéstinationsToolStripMenuItem.Size = new System.Drawing.Size(201, 22);

            this.versAutresDéstinationsToolStripMenuItem.Text = "Vers Autres Déstinations";
            this.versAutresDéstinationsToolStripMenuItem.Click += new System.EventHandler(this.versAutresDéstinationsToolStripMenuItem_Click);
            // 
            // TransfertListMP
            // 
            this.TransfertListMP.Enabled = false;
            this.TransfertListMP.Name = "TransfertListMP";

            this.TransfertListMP.Size = new System.Drawing.Size(214, 22);

            this.TransfertListMP.Text = "Liste des Transfets MP ";
            this.TransfertListMP.Click += new System.EventHandler(this.TransfertListMP_Click);
            // 
            // TransfertPF
            // 
            this.TransfertPF.Name = "TransfertPF";

            this.TransfertPF.Size = new System.Drawing.Size(214, 22);

            this.TransfertPF.Text = "Récéption Produit Fini";
            this.TransfertPF.Click += new System.EventHandler(this.TransfertPF_Click);
            // 
            // listTransfertsPF
            // 
            this.listTransfertsPF.Name = "listTransfertsPF";

            this.listTransfertsPF.Size = new System.Drawing.Size(214, 22);

            this.listTransfertsPF.Text = "Liste des Récéption PF";
            this.listTransfertsPF.Click += new System.EventHandler(this.listTransfertsPF_Click);
            // 
            // Stock
            // 
            this.Stock.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StockMP,
            this.StockPF,
            this.Dist});
            this.Stock.Name = "Stock";
            this.Stock.Size = new System.Drawing.Size(48, 20);
            this.Stock.Text = "Stock";
            // 
            // StockMP
            // 
            this.StockMP.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etatDuStockToolStripMenuItem,
            this.demandeBesoinToolStripMenuItem,
            this.listeDesDemandesToolStripMenuItem});
            this.StockMP.Name = "StockMP";
            this.StockMP.Size = new System.Drawing.Size(196, 22);
            this.StockMP.Text = "Stock Matiere Première";
            this.StockMP.Click += new System.EventHandler(this.StockMP_Click);
            // 
            // etatDuStockToolStripMenuItem
            // 
            this.etatDuStockToolStripMenuItem.Name = "etatDuStockToolStripMenuItem";
            this.etatDuStockToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.etatDuStockToolStripMenuItem.Text = "Etat du Stock";
            this.etatDuStockToolStripMenuItem.Click += new System.EventHandler(this.etatDuStockToolStripMenuItem_Click);
            // 
            // demandeBesoinToolStripMenuItem
            // 
            this.demandeBesoinToolStripMenuItem.Name = "demandeBesoinToolStripMenuItem";
            this.demandeBesoinToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.demandeBesoinToolStripMenuItem.Text = "Demande Besoin";
            this.demandeBesoinToolStripMenuItem.Click += new System.EventHandler(this.demandeBesoinToolStripMenuItem_Click);
            // 
            // listeDesDemandesToolStripMenuItem
            // 
            this.listeDesDemandesToolStripMenuItem.Name = "listeDesDemandesToolStripMenuItem";
            this.listeDesDemandesToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.listeDesDemandesToolStripMenuItem.Text = "Liste des demandes ";
            this.listeDesDemandesToolStripMenuItem.Click += new System.EventHandler(this.listeDesDemandesToolStripMenuItem_Click);
            // 
            // StockPF
            // 
            this.StockPF.Name = "StockPF";
            this.StockPF.Size = new System.Drawing.Size(196, 22);
            this.StockPF.Text = "Stock Produit Fini";
            this.StockPF.Click += new System.EventHandler(this.StockPF_Click);
            // 
            // Dist
            // 
            this.Dist.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mouvementToolStripMenuItem,
            this.listeDesMouvementToolStripMenuItem});
            this.Dist.Name = "Dist";
            this.Dist.Size = new System.Drawing.Size(196, 22);
            this.Dist.Text = "Distribution";
            // 
            // mouvementToolStripMenuItem
            // 
            this.mouvementToolStripMenuItem.Name = "mouvementToolStripMenuItem";
            this.mouvementToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.mouvementToolStripMenuItem.Text = "Mouvement";
            this.mouvementToolStripMenuItem.Click += new System.EventHandler(this.mouvementToolStripMenuItem_Click);
            // 
            // listeDesMouvementToolStripMenuItem
            // 
            this.listeDesMouvementToolStripMenuItem.Name = "listeDesMouvementToolStripMenuItem";
            this.listeDesMouvementToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.listeDesMouvementToolStripMenuItem.Text = "Liste des mouvement";
            this.listeDesMouvementToolStripMenuItem.Click += new System.EventHandler(this.listeDesMouvementToolStripMenuItem_Click);
            // 
            // Comercial
            // 
            this.Comercial.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ComercialCommand,
            this.ComercialBonAttrib,
            this.ComercialBonSort,
            this.ComercialFacture,
            this.ComercialBonLiv,
            this.toolStripSeparator1,
            this.ComercialBonRetour,
            this.ComercialFactRetour,
            this.toolStripSeparator4,
            this.CommercialListeBon,
            this.ComercialListVentes,
            this.toolStripSeparator2,
            this.ComercialClient,
            this.Caisse});
            this.Comercial.Name = "Comercial";
            this.Comercial.Size = new System.Drawing.Size(73, 20);
            this.Comercial.Text = "Comercial";
            this.Comercial.Click += new System.EventHandler(this.Comercial_Click);
            // 
            // ComercialCommand
            // 
            this.ComercialCommand.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvelleCommandeToolStripMenuItem,
            this.listeDesCommandesToolStripMenuItem});
            this.ComercialCommand.Name = "ComercialCommand";
            this.ComercialCommand.Size = new System.Drawing.Size(220, 22);
            this.ComercialCommand.Text = "Commandes / Engagement";
            this.ComercialCommand.Click += new System.EventHandler(this.ComercialCommand_Click);
            // 
            // nouvelleCommandeToolStripMenuItem
            // 
            this.nouvelleCommandeToolStripMenuItem.Name = "nouvelleCommandeToolStripMenuItem";
            this.nouvelleCommandeToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.nouvelleCommandeToolStripMenuItem.Text = "Nouvelle Commande";
            this.nouvelleCommandeToolStripMenuItem.Click += new System.EventHandler(this.nouvelleCommandeToolStripMenuItem_Click);
            // 
            // listeDesCommandesToolStripMenuItem
            // 
            this.listeDesCommandesToolStripMenuItem.Name = "listeDesCommandesToolStripMenuItem";
            this.listeDesCommandesToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.listeDesCommandesToolStripMenuItem.Text = "Liste des Commandes";
            this.listeDesCommandesToolStripMenuItem.Click += new System.EventHandler(this.listeDesCommandesToolStripMenuItem_Click);
            // 
            // ComercialBonAttrib
            // 
            this.ComercialBonAttrib.Name = "ComercialBonAttrib";
            this.ComercialBonAttrib.Size = new System.Drawing.Size(220, 22);
            this.ComercialBonAttrib.Text = "Bon d\'Attribution";
            this.ComercialBonAttrib.Click += new System.EventHandler(this.ComercialBonAttrib_Click);
            // 
            // ComercialBonSort
            // 
            this.ComercialBonSort.Name = "ComercialBonSort";
            this.ComercialBonSort.Size = new System.Drawing.Size(220, 22);
            this.ComercialBonSort.Text = "Bon de Sortie";
            this.ComercialBonSort.Click += new System.EventHandler(this.ComercialBonSort_Click);
            // 
            // ComercialFacture
            // 
            this.ComercialFacture.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.facturationToolStripMenuItem,
            this.transfertVersSageToolStripMenuItem});
            this.ComercialFacture.Name = "ComercialFacture";
            this.ComercialFacture.Size = new System.Drawing.Size(220, 22);
            this.ComercialFacture.Text = "Paiement";
            // 
            // facturationToolStripMenuItem
            // 
            this.facturationToolStripMenuItem.Name = "facturationToolStripMenuItem";
            this.facturationToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.facturationToolStripMenuItem.Text = "Paiement Bon Attribution";
            this.facturationToolStripMenuItem.Click += new System.EventHandler(this.facturationToolStripMenuItem_Click);
            // 
            // transfertVersSageToolStripMenuItem
            // 
            this.transfertVersSageToolStripMenuItem.Name = "transfertVersSageToolStripMenuItem";
            this.transfertVersSageToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.transfertVersSageToolStripMenuItem.Text = "Transfert Vers Sage";
            this.transfertVersSageToolStripMenuItem.Click += new System.EventHandler(this.transfertVersSageToolStripMenuItem_Click);
            // 
            // ComercialBonLiv
            // 
            this.ComercialBonLiv.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesChaffeursToolStripMenuItem});
            this.ComercialBonLiv.Name = "ComercialBonLiv";
            this.ComercialBonLiv.Size = new System.Drawing.Size(220, 22);
            this.ComercialBonLiv.Text = "Bon de Livraison";
            this.ComercialBonLiv.Click += new System.EventHandler(this.ComercialBonLiv_Click);
            // 
            // listeDesChaffeursToolStripMenuItem
            // 
            this.listeDesChaffeursToolStripMenuItem.Name = "listeDesChaffeursToolStripMenuItem";
            this.listeDesChaffeursToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.listeDesChaffeursToolStripMenuItem.Text = "Liste des Chaffeurs";
            this.listeDesChaffeursToolStripMenuItem.Click += new System.EventHandler(this.listeDesChaffeursToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(217, 6);
            // 
            // ComercialBonRetour
            // 
            this.ComercialBonRetour.Name = "ComercialBonRetour";
            this.ComercialBonRetour.Size = new System.Drawing.Size(220, 22);
            this.ComercialBonRetour.Text = "Bon de Retour";
            this.ComercialBonRetour.Click += new System.EventHandler(this.ComercialBonRetour_Click);
            // 
            // ComercialFactRetour
            // 
            this.ComercialFactRetour.Name = "ComercialFactRetour";
            this.ComercialFactRetour.Size = new System.Drawing.Size(220, 22);
            this.ComercialFactRetour.Text = "Facture de Retour";
            this.ComercialFactRetour.Click += new System.EventHandler(this.ComercialFactRetour_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(217, 6);
            // 
            // CommercialListeBon
            // 
            this.CommercialListeBon.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transfertSageToolStripMenuItem});
            this.CommercialListeBon.Name = "CommercialListeBon";
            this.CommercialListeBon.Size = new System.Drawing.Size(220, 22);
            this.CommercialListeBon.Text = "Liste des Bons";
            this.CommercialListeBon.Click += new System.EventHandler(this.CommercialListeBon_Click);
            // 
            // transfertSageToolStripMenuItem
            // 
            this.transfertSageToolStripMenuItem.Name = "transfertSageToolStripMenuItem";

            this.transfertSageToolStripMenuItem.Size = new System.Drawing.Size(149, 22);

            this.transfertSageToolStripMenuItem.Text = "Transfert Sage";
            this.transfertSageToolStripMenuItem.Click += new System.EventHandler(this.transfertSageToolStripMenuItem_Click);
            // 
            // ComercialListVentes
            // 
            this.ComercialListVentes.Name = "ComercialListVentes";
            this.ComercialListVentes.Size = new System.Drawing.Size(220, 22);
            this.ComercialListVentes.Text = "Liste des Ventes";
            this.ComercialListVentes.Click += new System.EventHandler(this.ComercialListVentes_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(217, 6);
            // 
            // ComercialClient
            // 
            this.ComercialClient.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauClientToolStripMenuItem,
            this.listeDesClientsToolStripMenuItem,
            this.toolStripSeparator5,
            this.satisfactionClientToolStripMenuItem,
            this.toolStripSeparator6,
            this.listeDesChauffeurToolStripMenuItem});
            this.ComercialClient.Name = "ComercialClient";
            this.ComercialClient.Size = new System.Drawing.Size(220, 22);
            this.ComercialClient.Text = "Gestion des Client";
            this.ComercialClient.Click += new System.EventHandler(this.ComercialClient_Click);
            // 
            // nouveauClientToolStripMenuItem
            // 
            this.nouveauClientToolStripMenuItem.Name = "nouveauClientToolStripMenuItem";
            this.nouveauClientToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.nouveauClientToolStripMenuItem.Text = "Nouveau Client";
            this.nouveauClientToolStripMenuItem.Click += new System.EventHandler(this.nouveauClientToolStripMenuItem_Click);
            // 
            // listeDesClientsToolStripMenuItem
            // 
            this.listeDesClientsToolStripMenuItem.Name = "listeDesClientsToolStripMenuItem";
            this.listeDesClientsToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.listeDesClientsToolStripMenuItem.Text = "Liste des Clients";
            this.listeDesClientsToolStripMenuItem.Click += new System.EventHandler(this.listeDesClientsToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(172, 6);
            // 
            // satisfactionClientToolStripMenuItem
            // 
            this.satisfactionClientToolStripMenuItem.Name = "satisfactionClientToolStripMenuItem";
            this.satisfactionClientToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.satisfactionClientToolStripMenuItem.Text = "Satisfaction Client";
            this.satisfactionClientToolStripMenuItem.Click += new System.EventHandler(this.satisfactionClientToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(172, 6);
            // 
            // listeDesChauffeurToolStripMenuItem
            // 
            this.listeDesChauffeurToolStripMenuItem.Name = "listeDesChauffeurToolStripMenuItem";
            this.listeDesChauffeurToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.listeDesChauffeurToolStripMenuItem.Text = "Liste des Chauffeur";
            this.listeDesChauffeurToolStripMenuItem.Click += new System.EventHandler(this.listeDesChauffeurToolStripMenuItem_Click);
            // 
            // Caisse
            // 
            this.Caisse.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CaisseOpClient,
            this.CaisseOpDivers,
            this.exportVersSageToolStripMenuItem});
            this.Caisse.Name = "Caisse";
            this.Caisse.Size = new System.Drawing.Size(220, 22);
            this.Caisse.Text = "Caisse";
            this.Caisse.Click += new System.EventHandler(this.Caisse_Click);
            // 
            // CaisseOpClient
            // 
            this.CaisseOpClient.Name = "CaisseOpClient";
            this.CaisseOpClient.Size = new System.Drawing.Size(192, 22);
            this.CaisseOpClient.Text = "Mouvement Caisse";
            this.CaisseOpClient.Click += new System.EventHandler(this.versementsToolStripMenuItem_Click);
            // 
            // CaisseOpDivers
            // 
            this.CaisseOpDivers.Name = "CaisseOpDivers";
            this.CaisseOpDivers.Size = new System.Drawing.Size(192, 22);
            this.CaisseOpDivers.Text = "Liste des Mouvements";
            this.CaisseOpDivers.Click += new System.EventHandler(this.CaisseOpDivers_Click);
            // 
            // exportVersSageToolStripMenuItem
            // 
            this.exportVersSageToolStripMenuItem.Name = "exportVersSageToolStripMenuItem";
            this.exportVersSageToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.exportVersSageToolStripMenuItem.Text = "Tranfert vers Sage";
            this.exportVersSageToolStripMenuItem.Click += new System.EventHandler(this.exportVersSageToolStripMenuItem_Click);
            // 
            // Rapports
            // 
            this.Rapports.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RapportMP,
            this.RapportPF,
            this.RapportProduction,
            this.RapportEtatVente,
            this.RapportActivite,
            this.RapportCaisse,
            this.rapportVente});
            this.Rapports.Name = "Rapports";
            this.Rapports.Size = new System.Drawing.Size(61, 20);
            this.Rapports.Text = "Rapport";
            // 
            // RapportMP
            // 
            this.RapportMP.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.matièrePremièreToolStripMenuItem,
            this.evaluationFournissersToolStripMenuItem,
            this.suivieDeStockToolStripMenuItem});
            this.RapportMP.Name = "RapportMP";
            this.RapportMP.Size = new System.Drawing.Size(190, 22);
            this.RapportMP.Text = "Matière Première ";
            this.RapportMP.Click += new System.EventHandler(this.RapportMP_Click);
            // 
            // matièrePremièreToolStripMenuItem
            // 
            this.matièrePremièreToolStripMenuItem.Name = "matièrePremièreToolStripMenuItem";
            this.matièrePremièreToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.matièrePremièreToolStripMenuItem.Text = "Etat du stock";
            this.matièrePremièreToolStripMenuItem.Click += new System.EventHandler(this.matièrePremièreToolStripMenuItem_Click);
            // 
            // evaluationFournissersToolStripMenuItem
            // 
            this.evaluationFournissersToolStripMenuItem.Name = "evaluationFournissersToolStripMenuItem";
            this.evaluationFournissersToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.evaluationFournissersToolStripMenuItem.Text = "Evaluation Fournissers";
            this.evaluationFournissersToolStripMenuItem.Click += new System.EventHandler(this.evaluationFournissersToolStripMenuItem_Click);
            // 
            // suivieDeStockToolStripMenuItem
            // 
            this.suivieDeStockToolStripMenuItem.Name = "suivieDeStockToolStripMenuItem";
            this.suivieDeStockToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.suivieDeStockToolStripMenuItem.Text = "Suivie de stock";
            this.suivieDeStockToolStripMenuItem.Click += new System.EventHandler(this.suivieDeStockToolStripMenuItem_Click);
            // 
            // RapportPF
            // 
            this.RapportPF.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etatDesStocksToolStripMenuItem,
            this.etatDesMouvementsToolStripMenuItem});
            this.RapportPF.Name = "RapportPF";
            this.RapportPF.Size = new System.Drawing.Size(190, 22);
            this.RapportPF.Text = "Produit Fini";
            this.RapportPF.Click += new System.EventHandler(this.RapportPF_Click);
            // 
            // etatDesStocksToolStripMenuItem
            // 
            this.etatDesStocksToolStripMenuItem.Name = "etatDesStocksToolStripMenuItem";
            this.etatDesStocksToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.etatDesStocksToolStripMenuItem.Text = "Etat des Stocks";
            this.etatDesStocksToolStripMenuItem.Click += new System.EventHandler(this.etatDesStocksToolStripMenuItem_Click);
            // 
            // etatDesMouvementsToolStripMenuItem
            // 
            this.etatDesMouvementsToolStripMenuItem.Name = "etatDesMouvementsToolStripMenuItem";
            this.etatDesMouvementsToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.etatDesMouvementsToolStripMenuItem.Text = "Etat des Mouvements";
            this.etatDesMouvementsToolStripMenuItem.Click += new System.EventHandler(this.etatDesMouvementsToolStripMenuItem_Click);
            // 
            // RapportProduction
            // 
            this.RapportProduction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transfertMatièrePremièreToolStripMenuItem,
            this.transToolStripMenuItem,
            this.unitéVerreToolStripMenuItem});
            this.RapportProduction.Name = "RapportProduction";
            this.RapportProduction.Size = new System.Drawing.Size(190, 22);
            this.RapportProduction.Text = "Production";
            this.RapportProduction.Click += new System.EventHandler(this.RapportProduction_Click);
            // 
            // transfertMatièrePremièreToolStripMenuItem
            // 
            this.transfertMatièrePremièreToolStripMenuItem.Name = "transfertMatièrePremièreToolStripMenuItem";
            this.transfertMatièrePremièreToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.transfertMatièrePremièreToolStripMenuItem.Text = "Unité PET";
            this.transfertMatièrePremièreToolStripMenuItem.Click += new System.EventHandler(this.transfertMatièrePremièreToolStripMenuItem_Click);
            // 
            // transToolStripMenuItem
            // 
            this.transToolStripMenuItem.Name = "transToolStripMenuItem";
            this.transToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.transToolStripMenuItem.Text = "Unité Cannette";
            this.transToolStripMenuItem.Click += new System.EventHandler(this.transToolStripMenuItem_Click);
            // 
            // unitéVerreToolStripMenuItem
            // 
            this.unitéVerreToolStripMenuItem.Name = "unitéVerreToolStripMenuItem";
            this.unitéVerreToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.unitéVerreToolStripMenuItem.Text = "Unité Verre";
            this.unitéVerreToolStripMenuItem.Click += new System.EventHandler(this.unitéVerreToolStripMenuItem_Click);
            // 
            // RapportEtatVente
            // 
            this.RapportEtatVente.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.journalDesVentesToolStripMenuItem,
            this.statistiquesDesArticlesToolStripMenuItem,
            this.statistiquesDesClientsToolStripMenuItem,
            this.situationFlotteToolStripMenuItem,
            this.satisfactionClientToolStripMenuItem1,
            this.releverCompteClientToolStripMenuItem,
            this.situationComericaleToolStripMenuItem,
            this.etatJournalierDesMouvementsClientsToolStripMenuItem});
            this.RapportEtatVente.Name = "RapportEtatVente";
            this.RapportEtatVente.Size = new System.Drawing.Size(190, 22);
            this.RapportEtatVente.Text = "Commercial";
            this.RapportEtatVente.Click += new System.EventHandler(this.RapportEtatVente_Click);
            // 
            // journalDesVentesToolStripMenuItem
            // 
            this.journalDesVentesToolStripMenuItem.Name = "journalDesVentesToolStripMenuItem";
            this.journalDesVentesToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.journalDesVentesToolStripMenuItem.Text = "Journal des Ventes";
            this.journalDesVentesToolStripMenuItem.Click += new System.EventHandler(this.journalDesVentesToolStripMenuItem_Click);
            // 
            // statistiquesDesArticlesToolStripMenuItem
            // 
            this.statistiquesDesArticlesToolStripMenuItem.Name = "statistiquesDesArticlesToolStripMenuItem";
            this.statistiquesDesArticlesToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.statistiquesDesArticlesToolStripMenuItem.Text = "Statistiques des Articles";
            this.statistiquesDesArticlesToolStripMenuItem.Click += new System.EventHandler(this.statistiquesDesArticlesToolStripMenuItem_Click);
            // 
            // statistiquesDesClientsToolStripMenuItem
            // 
            this.statistiquesDesClientsToolStripMenuItem.Name = "statistiquesDesClientsToolStripMenuItem";
            this.statistiquesDesClientsToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.statistiquesDesClientsToolStripMenuItem.Text = "Statistiques des Clients";
            this.statistiquesDesClientsToolStripMenuItem.Click += new System.EventHandler(this.statistiquesDesClientsToolStripMenuItem_Click);
            // 
            // situationFlotteToolStripMenuItem
            // 
            this.situationFlotteToolStripMenuItem.Name = "situationFlotteToolStripMenuItem";
            this.situationFlotteToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.situationFlotteToolStripMenuItem.Text = "Situation Flotte";
            this.situationFlotteToolStripMenuItem.Click += new System.EventHandler(this.situationFlotteToolStripMenuItem_Click);
            // 
            // satisfactionClientToolStripMenuItem1
            // 
            this.satisfactionClientToolStripMenuItem1.Name = "satisfactionClientToolStripMenuItem1";
            this.satisfactionClientToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.satisfactionClientToolStripMenuItem1.Text = "Satisfaction Clients";
            this.satisfactionClientToolStripMenuItem1.Click += new System.EventHandler(this.satisfactionClientToolStripMenuItem1_Click);
            // 
            // releverCompteClientToolStripMenuItem
            // 
            this.releverCompteClientToolStripMenuItem.Name = "releverCompteClientToolStripMenuItem";
            this.releverCompteClientToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.releverCompteClientToolStripMenuItem.Text = "Releve Compte Client";
            this.releverCompteClientToolStripMenuItem.Click += new System.EventHandler(this.releverCompteClientToolStripMenuItem_Click);
            // 
            // situationComericaleToolStripMenuItem
            // 
            this.situationComericaleToolStripMenuItem.Name = "situationComericaleToolStripMenuItem";
            this.situationComericaleToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.situationComericaleToolStripMenuItem.Text = "Situation Comericale";
            this.situationComericaleToolStripMenuItem.Click += new System.EventHandler(this.situationComericaleToolStripMenuItem_Click);
            // 
            // etatJournalierDesMouvementsClientsToolStripMenuItem
            // 
            this.etatJournalierDesMouvementsClientsToolStripMenuItem.Name = "etatJournalierDesMouvementsClientsToolStripMenuItem";
            this.etatJournalierDesMouvementsClientsToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.etatJournalierDesMouvementsClientsToolStripMenuItem.Text = "Etat Journalier Des Mouvements Clients";
            this.etatJournalierDesMouvementsClientsToolStripMenuItem.Click += new System.EventHandler(this.etatJournalierDesMouvementsClientsToolStripMenuItem_Click);
            // 
            // RapportActivite
            // 
            this.RapportActivite.Name = "RapportActivite";
            this.RapportActivite.Size = new System.Drawing.Size(190, 22);
            this.RapportActivite.Text = "Rapport d\'activité";
            this.RapportActivite.Click += new System.EventHandler(this.RapportActivite_Click);
            // 
            // RapportCaisse
            // 
            this.RapportCaisse.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etatJournalierDesDépensesToolStripMenuItem,
            this.etatJournalierDesMouvementsToolStripMenuItem,
            this.etatRécapitulatifDesMouvementsToolStripMenuItem,
            this.releverCompteClientToolStripMenuItem1});
            this.RapportCaisse.Name = "RapportCaisse";
            this.RapportCaisse.Size = new System.Drawing.Size(190, 22);
            this.RapportCaisse.Text = "Caisse";
            // 
            // etatJournalierDesDépensesToolStripMenuItem
            // 
            this.etatJournalierDesDépensesToolStripMenuItem.Name = "etatJournalierDesDépensesToolStripMenuItem";
            this.etatJournalierDesDépensesToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.etatJournalierDesDépensesToolStripMenuItem.Text = "Etat Journalier des Dépenses";
            this.etatJournalierDesDépensesToolStripMenuItem.Click += new System.EventHandler(this.etatJournalierDesDépensesToolStripMenuItem_Click);
            // 
            // etatJournalierDesMouvementsToolStripMenuItem
            // 
            this.etatJournalierDesMouvementsToolStripMenuItem.Name = "etatJournalierDesMouvementsToolStripMenuItem";
            this.etatJournalierDesMouvementsToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.etatJournalierDesMouvementsToolStripMenuItem.Text = "Etat Journalier des Mouvements";
            this.etatJournalierDesMouvementsToolStripMenuItem.Click += new System.EventHandler(this.etatJournalierDesMouvementsToolStripMenuItem_Click);
            // 
            // etatRécapitulatifDesMouvementsToolStripMenuItem
            // 
            this.etatRécapitulatifDesMouvementsToolStripMenuItem.Name = "etatRécapitulatifDesMouvementsToolStripMenuItem";
            this.etatRécapitulatifDesMouvementsToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.etatRécapitulatifDesMouvementsToolStripMenuItem.Text = "Etat Récapitulatif des Mouvements";
            this.etatRécapitulatifDesMouvementsToolStripMenuItem.Click += new System.EventHandler(this.etatRécapitulatifDesMouvementsToolStripMenuItem_Click);
            // 
            // releverCompteClientToolStripMenuItem1
            // 
            this.releverCompteClientToolStripMenuItem1.Name = "releverCompteClientToolStripMenuItem1";
            this.releverCompteClientToolStripMenuItem1.Size = new System.Drawing.Size(257, 22);
            this.releverCompteClientToolStripMenuItem1.Text = "Relever Compte Client";
            this.releverCompteClientToolStripMenuItem1.Click += new System.EventHandler(this.releverCompteClientToolStripMenuItem1_Click_1);
            // 
            // rapportVente
            // 
            this.rapportVente.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etatJournalierDesMouvementsClientsToolStripMenuItem1,
            this.releveCompteClientToolStripMenuItem,
            this.statistiquesDesArticlesToolStripMenuItem1,
            this.situationFlotteToolStripMenuItem1,
            this.situationComericaleToolStripMenuItem1,
            this.satisfactionClientsToolStripMenuItem});
            this.rapportVente.Name = "rapportVente";
            this.rapportVente.Size = new System.Drawing.Size(190, 22);
            this.rapportVente.Text = "Direction Commercial";
            // 
            // etatJournalierDesMouvementsClientsToolStripMenuItem1
            // 
            this.etatJournalierDesMouvementsClientsToolStripMenuItem1.Name = "etatJournalierDesMouvementsClientsToolStripMenuItem1";
            this.etatJournalierDesMouvementsClientsToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.etatJournalierDesMouvementsClientsToolStripMenuItem1.Text = "Etat Journalier Des Mouvements Clients";
            this.etatJournalierDesMouvementsClientsToolStripMenuItem1.Click += new System.EventHandler(this.etatJournalierDesMouvementsClientsToolStripMenuItem1_Click);
            // 
            // releveCompteClientToolStripMenuItem
            // 
            this.releveCompteClientToolStripMenuItem.Name = "releveCompteClientToolStripMenuItem";
            this.releveCompteClientToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.releveCompteClientToolStripMenuItem.Text = "Releve Compte Client";
            this.releveCompteClientToolStripMenuItem.Click += new System.EventHandler(this.releveCompteClientToolStripMenuItem_Click);
            // 
            // statistiquesDesArticlesToolStripMenuItem1
            // 
            this.statistiquesDesArticlesToolStripMenuItem1.Name = "statistiquesDesArticlesToolStripMenuItem1";
            this.statistiquesDesArticlesToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.statistiquesDesArticlesToolStripMenuItem1.Text = "Statistiques des Articles";
            this.statistiquesDesArticlesToolStripMenuItem1.Click += new System.EventHandler(this.statistiquesDesArticlesToolStripMenuItem1_Click);
            // 
            // situationFlotteToolStripMenuItem1
            // 
            this.situationFlotteToolStripMenuItem1.Name = "situationFlotteToolStripMenuItem1";
            this.situationFlotteToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.situationFlotteToolStripMenuItem1.Text = "Situation Flotte";
            this.situationFlotteToolStripMenuItem1.Click += new System.EventHandler(this.situationFlotteToolStripMenuItem1_Click);
            // 
            // situationComericaleToolStripMenuItem1
            // 
            this.situationComericaleToolStripMenuItem1.Name = "situationComericaleToolStripMenuItem1";
            this.situationComericaleToolStripMenuItem1.Size = new System.Drawing.Size(282, 22);
            this.situationComericaleToolStripMenuItem1.Text = "Situation Comericale";
            this.situationComericaleToolStripMenuItem1.Click += new System.EventHandler(this.situationComericaleToolStripMenuItem1_Click);
            // 
            // satisfactionClientsToolStripMenuItem
            // 
            this.satisfactionClientsToolStripMenuItem.Name = "satisfactionClientsToolStripMenuItem";
            this.satisfactionClientsToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.satisfactionClientsToolStripMenuItem.Text = "Satisfaction Clients";
            this.satisfactionClientsToolStripMenuItem.Click += new System.EventHandler(this.satisfactionClientsToolStripMenuItem_Click);
            // 
            // Administration
            // 
            this.Administration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AdministrationUtilisateur,
            this.AdministrationAutorisation,
            this.AdministrationMaintenance,

            this.parèmetresToolStripMenuItem,
            this.AdministrationBackup});

            this.Administration.ForeColor = System.Drawing.Color.Crimson;
            this.Administration.Name = "Administration";
            this.Administration.Size = new System.Drawing.Size(98, 20);
            this.Administration.Text = "Administration";
            // 
            // AdministrationUtilisateur
            // 
            this.AdministrationUtilisateur.Name = "AdministrationUtilisateur";

            this.AdministrationUtilisateur.Size = new System.Drawing.Size(152, 22);

            this.AdministrationUtilisateur.Text = "Utilisateurs";
            this.AdministrationUtilisateur.Click += new System.EventHandler(this.utilisateursToolStripMenuItem_Click);
            // 
            // AdministrationAutorisation
            // 
            this.AdministrationAutorisation.Name = "AdministrationAutorisation";

            this.AdministrationAutorisation.Size = new System.Drawing.Size(152, 22);

            this.AdministrationAutorisation.Text = "Autorisations";
            this.AdministrationAutorisation.Click += new System.EventHandler(this.groupesToolStripMenuItem_Click);
            // 
            // AdministrationMaintenance
            // 
            this.AdministrationMaintenance.Name = "AdministrationMaintenance";

            this.AdministrationMaintenance.Size = new System.Drawing.Size(144, 22);
            this.AdministrationMaintenance.Text = "Maintenance";
            this.AdministrationMaintenance.Click += new System.EventHandler(this.AdministrationMaintenance_Click);
            // 
            // parèmetresToolStripMenuItem
            // 
            this.parèmetresToolStripMenuItem.Name = "parèmetresToolStripMenuItem";
            this.parèmetresToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.parèmetresToolStripMenuItem.Text = "Parèmetres";
            this.parèmetresToolStripMenuItem.Click += new System.EventHandler(this.parèmetresToolStripMenuItem_Click);
            // 
            // AdministrationBackup
            // 
            this.AdministrationBackup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.backupDatabaseToolStripMenuItem,
            this.restoreDatabaseToolStripMenuItem});
            this.AdministrationBackup.Name = "AdministrationBackup";
            this.AdministrationBackup.Size = new System.Drawing.Size(144, 22);
            this.AdministrationBackup.Text = "Backup";
            // 
            // backupDatabaseToolStripMenuItem
            // 
            this.backupDatabaseToolStripMenuItem.Name = "backupDatabaseToolStripMenuItem";
            this.backupDatabaseToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.backupDatabaseToolStripMenuItem.Text = "Backup database";
            this.backupDatabaseToolStripMenuItem.Click += new System.EventHandler(this.backupDatabaseToolStripMenuItem_Click);
            // 
            // restoreDatabaseToolStripMenuItem
            // 
            this.restoreDatabaseToolStripMenuItem.Enabled = false;
            this.restoreDatabaseToolStripMenuItem.Name = "restoreDatabaseToolStripMenuItem";
            this.restoreDatabaseToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.restoreDatabaseToolStripMenuItem.Text = "Restore database";
            this.restoreDatabaseToolStripMenuItem.Click += new System.EventHandler(this.restoreDatabaseToolStripMenuItem_Click);
            // 

            // Aide
            // 
            this.Aide.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AideApropos});
            this.Aide.Name = "Aide";
            this.Aide.Size = new System.Drawing.Size(43, 20);
            this.Aide.Text = "Aide";
            // 
            // AideApropos
            // 
            this.AideApropos.Name = "AideApropos";
            this.AideApropos.Size = new System.Drawing.Size(120, 22);
            this.AideApropos.Text = "à propos";
            this.AideApropos.Click += new System.EventHandler(this.AideApropos_Click);
            // 
            // groupBoxInfos
            // 
            this.groupBoxInfos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxInfos.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxInfos.BackColor = System.Drawing.Color.White;
            this.groupBoxInfos.Controls.Add(this.panTraitement);
            this.groupBoxInfos.Controls.Add(this.button1);
            this.groupBoxInfos.Controls.Add(this.labelUserSection);
            this.groupBoxInfos.Controls.Add(this.labeluserRole);
            this.groupBoxInfos.Controls.Add(this.labelUserName);
            this.groupBoxInfos.Location = new System.Drawing.Point(0, 392);
            this.groupBoxInfos.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxInfos.Name = "groupBoxInfos";
            this.groupBoxInfos.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxInfos.Size = new System.Drawing.Size(764, 110);
            this.groupBoxInfos.TabIndex = 2;
            this.groupBoxInfos.TabStop = false;
            this.groupBoxInfos.Text = "Connecté";
            this.groupBoxInfos.Enter += new System.EventHandler(this.groupBoxInfos_Enter);
            // 
            // panTraitement
            // 
            this.panTraitement.Controls.Add(this.label1);
            this.panTraitement.Controls.Add(this.progressMain);
            this.panTraitement.Location = new System.Drawing.Point(209, 36);
            this.panTraitement.Margin = new System.Windows.Forms.Padding(2);
            this.panTraitement.Name = "panTraitement";
            this.panTraitement.Size = new System.Drawing.Size(433, 61);
            this.panTraitement.TabIndex = 3;
            this.panTraitement.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Traitement En Cours:";
            // 
            // progressMain
            // 
            this.progressMain.Location = new System.Drawing.Point(10, 28);
            this.progressMain.Margin = new System.Windows.Forms.Padding(2);
            this.progressMain.Name = "progressMain";
            this.progressMain.Size = new System.Drawing.Size(411, 19);
            this.progressMain.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(647, 63);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 24);
            this.button1.TabIndex = 3;
            this.button1.Text = "Deconnexion";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labelUserSection
            // 
            this.labelUserSection.AutoSize = true;
            this.labelUserSection.Location = new System.Drawing.Point(8, 68);
            this.labelUserSection.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelUserSection.Name = "labelUserSection";
            this.labelUserSection.Size = new System.Drawing.Size(43, 13);
            this.labelUserSection.TabIndex = 2;
            this.labelUserSection.Text = "Section";
            // 
            // labeluserRole
            // 
            this.labeluserRole.AutoSize = true;
            this.labeluserRole.Location = new System.Drawing.Point(8, 54);
            this.labeluserRole.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labeluserRole.Name = "labeluserRole";
            this.labeluserRole.Size = new System.Drawing.Size(49, 13);
            this.labeluserRole.TabIndex = 1;
            this.labeluserRole.Text = "userRole";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserName.Location = new System.Drawing.Point(9, 31);
            this.labelUserName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(90, 20);
            this.labelUserName.TabIndex = 0;
            this.labelUserName.Text = "userName";
            // 

            // bgwSituationCommercial
            // 
            this.bgwSituationCommercial.WorkerReportsProgress = true;
            this.bgwSituationCommercial.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorker_DoWork);
            this.bgwSituationCommercial.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWorker_ProgressChanged);
            this.bgwSituationCommercial.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorker_RunWorkerCompleted);

            // 
            // bk1
            // 
            this.bk1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bk1_DoWork);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(764, 477);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 

            // parèmetresToolStripMenuItem
            // 
            this.parèmetresToolStripMenuItem.Name = "parèmetresToolStripMenuItem";
            this.parèmetresToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.parèmetresToolStripMenuItem.Text = "Parèmetres";
            this.parèmetresToolStripMenuItem.Click += new System.EventHandler(this.parèmetresToolStripMenuItem_Click);
            // 

            // FrMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 501);
            this.Controls.Add(this.groupBoxInfos);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuMain);
            this.MainMenuStrip = this.menuMain;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrMain";
            this.Text = "G. Chréa system";
            this.Shown += new System.EventHandler(this.FrMain_Shown);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.groupBoxInfos.ResumeLayout(false);
            this.groupBoxInfos.PerformLayout();
            this.panTraitement.ResumeLayout(false);
            this.panTraitement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem Fichier;
        private System.Windows.Forms.ToolStripMenuItem Arrivage;
        private System.Windows.Forms.ToolStripMenuItem ArrivageMP;
        private System.Windows.Forms.ToolStripMenuItem Production;
        private System.Windows.Forms.ToolStripMenuItem Transfert;
        private System.Windows.Forms.ToolStripMenuItem Rapports;
        private System.Windows.Forms.ToolStripMenuItem Comercial;
        private System.Windows.Forms.ToolStripMenuItem Administration;
        private System.Windows.Forms.ToolStripMenuItem Aide;
        private System.Windows.Forms.ToolStripMenuItem FichierConnecter;
        private System.Windows.Forms.ToolStripMenuItem FichierDeconnecter;
        private System.Windows.Forms.ToolStripMenuItem FichierQuitter;
        private System.Windows.Forms.ToolStripMenuItem AideApropos;
        private System.Windows.Forms.ToolStripMenuItem ArrivageListMP;
        private System.Windows.Forms.ToolStripMenuItem AdministrationUtilisateur;
        private System.Windows.Forms.ToolStripMenuItem AdministrationAutorisation;
        private System.Windows.Forms.GroupBox groupBoxInfos;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelUserSection;
        private System.Windows.Forms.Label labeluserRole;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.ToolStripMenuItem ProductionPET;
        private System.Windows.Forms.ToolStripMenuItem ProductionListPET;
        private System.Windows.Forms.ToolStripMenuItem ProductionCan;
        private System.Windows.Forms.ToolStripMenuItem TransfertMT;
        private System.Windows.Forms.ToolStripMenuItem TransfertListMP;
        private System.Windows.Forms.ToolStripMenuItem Stock;
        private System.Windows.Forms.ToolStripMenuItem StockMP;
        private System.Windows.Forms.ToolStripMenuItem StockPF;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonAttrib;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonSort;
        private System.Windows.Forms.ToolStripMenuItem ComercialFacture;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonLiv;
        private System.Windows.Forms.ToolStripMenuItem ComercialListVentes;
        private System.Windows.Forms.ToolStripMenuItem ComercialCommand;
        private System.Windows.Forms.ToolStripMenuItem TransfertPF;
        private System.Windows.Forms.ToolStripMenuItem listTransfertsPF;
        private System.Windows.Forms.ToolStripMenuItem versUnitéPETToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versUnitéCanetteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versAutresDéstinationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ComercialClient;
        private System.Windows.Forms.ToolStripMenuItem nouveauClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesClientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesChauffeurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportMP;
        private System.Windows.Forms.ToolStripMenuItem matièrePremièreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportProduction;
        private System.Windows.Forms.ToolStripMenuItem transfertMatièrePremièreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportEtatVente;
        private System.Windows.Forms.ToolStripMenuItem RapportActivite;
        private System.Windows.Forms.ToolStripMenuItem AdministrationMaintenance;
        private System.Windows.Forms.ToolStripMenuItem CommercialListeBon;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem RapportPF;
        private System.Windows.Forms.ToolStripMenuItem etatDesStocksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatDesMouvementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ArrivagelistFourni;
        private System.Windows.Forms.ToolStripMenuItem Caisse;
        private System.Windows.Forms.ToolStripMenuItem CaisseOpClient;
        private System.Windows.Forms.ToolStripMenuItem CaisseOpDivers;
        private System.Windows.Forms.ToolStripMenuItem journalDesVentesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistiquesDesClientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem facturationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transfertVersSageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem releverCompteClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem situationComericaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RapportCaisse;
        private System.Windows.Forms.ToolStripMenuItem etatJournalierDesDépensesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatJournalierDesMouvementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatRécapitulatifDesMouvementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem productuionVer;
        private System.Windows.Forms.ToolStripMenuItem ComercialBonRetour;
        private System.Windows.Forms.ToolStripMenuItem ComercialFactRetour;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem etatDuStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem demandeBesoinToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistiquesDesArticlesToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem releverCompteClientToolStripMenuItem1;
        private System.Windows.Forms.Panel panTraitement;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressMain;

        private System.ComponentModel.BackgroundWorker bgwSituationCommercial;

        private System.Windows.Forms.ToolStripMenuItem exportVersSageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transfertSageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evalusationFournisseursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesFournisseursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evaluationFournissersToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem satisfactionClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem satisfactionClientToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listeDesDemandesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvelleCommandeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesCommandesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unitéVerreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MainChangePasse;
        private System.Windows.Forms.ToolStripMenuItem listeDesChaffeursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem situationFlotteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesMatieresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesProduitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesMatieresPrimièreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesArromesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem couleursBouchonsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesProduitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesForamtsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem typeColorantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker bk1;
        private System.Windows.Forms.ToolStripMenuItem listeDesPrixToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Dist;
        private System.Windows.Forms.ToolStripMenuItem mouvementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesMouvementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatJournalierDesMouvementsClientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suivieDeStockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rapportVente;
        private System.Windows.Forms.ToolStripMenuItem releveCompteClientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etatJournalierDesMouvementsClientsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem statistiquesDesArticlesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem situationFlotteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem situationComericaleToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem satisfactionClientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parèmetresToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem AdministrationBackup;
        private System.Windows.Forms.ToolStripMenuItem backupDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreDatabaseToolStripMenuItem;

    }
}

