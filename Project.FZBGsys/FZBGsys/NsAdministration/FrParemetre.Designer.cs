﻿namespace FZBGsys.NsAdministration
{
    partial class FrParemetre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tcParametre = new System.Windows.Forms.TabControl();
            this.tpImpression = new System.Windows.Forms.TabPage();
            this.dgv1 = new System.Windows.Forms.DataGridView();
            this.tpPeriode = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.dgv2 = new System.Windows.Forms.DataGridView();
            this.dgv3 = new System.Windows.Forms.DataGridView();
            this.tcParametre.SuspendLayout();
            this.tpImpression.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).BeginInit();
            this.tpPeriode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv3)).BeginInit();
            this.SuspendLayout();
            // 
            // tcParametre
            // 
            this.tcParametre.Controls.Add(this.tpImpression);
            this.tcParametre.Controls.Add(this.tpPeriode);
            this.tcParametre.Location = new System.Drawing.Point(1, 2);
            this.tcParametre.Name = "tcParametre";
            this.tcParametre.SelectedIndex = 0;
            this.tcParametre.Size = new System.Drawing.Size(709, 249);
            this.tcParametre.TabIndex = 0;
            this.tcParametre.SelectedIndexChanged += new System.EventHandler(this.tcParametre_SelectedIndexChanged);
            // 
            // tpImpression
            // 
            this.tpImpression.Controls.Add(this.dgv1);
            this.tpImpression.Location = new System.Drawing.Point(4, 22);
            this.tpImpression.Name = "tpImpression";
            this.tpImpression.Padding = new System.Windows.Forms.Padding(3);
            this.tpImpression.Size = new System.Drawing.Size(701, 223);
            this.tpImpression.TabIndex = 0;
            this.tpImpression.Text = "Impression";
            this.tpImpression.UseVisualStyleBackColor = true;
            // 
            // dgv1
            // 
            this.dgv1.AllowUserToAddRows = false;
            this.dgv1.AllowUserToDeleteRows = false;
            this.dgv1.AllowUserToResizeColumns = false;
            this.dgv1.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv1.Location = new System.Drawing.Point(3, 21);
            this.dgv1.MultiSelect = false;
            this.dgv1.Name = "dgv1";
            this.dgv1.RowHeadersVisible = false;
            this.dgv1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv1.Size = new System.Drawing.Size(340, 199);
            this.dgv1.TabIndex = 0;
            this.dgv1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv1_CellDoubleClick);
            // 
            // tpPeriode
            // 
            this.tpPeriode.Controls.Add(this.dgv3);
            this.tpPeriode.Controls.Add(this.dgv2);
            this.tpPeriode.Location = new System.Drawing.Point(4, 22);
            this.tpPeriode.Name = "tpPeriode";
            this.tpPeriode.Padding = new System.Windows.Forms.Padding(3);
            this.tpPeriode.Size = new System.Drawing.Size(701, 223);
            this.tpPeriode.TabIndex = 1;
            this.tpPeriode.Text = "Période des Rapports";
            this.tpPeriode.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(347, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgv2
            // 
            this.dgv2.AllowUserToAddRows = false;
            this.dgv2.AllowUserToDeleteRows = false;
            this.dgv2.AllowUserToResizeColumns = false;
            this.dgv2.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv2.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv2.Location = new System.Drawing.Point(2, 22);
            this.dgv2.MultiSelect = false;
            this.dgv2.Name = "dgv2";
            this.dgv2.RowHeadersVisible = false;
            this.dgv2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv2.Size = new System.Drawing.Size(340, 199);
            this.dgv2.TabIndex = 1;
            this.dgv2.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv2_CellContentDoubleClick);
            // 
            // dgv3
            // 
            this.dgv3.AllowUserToAddRows = false;
            this.dgv3.AllowUserToDeleteRows = false;
            this.dgv3.AllowUserToResizeColumns = false;
            this.dgv3.AllowUserToResizeRows = false;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv3.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv3.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv3.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgv3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv3.Location = new System.Drawing.Point(349, 22);
            this.dgv3.MultiSelect = false;
            this.dgv3.Name = "dgv3";
            this.dgv3.RowHeadersVisible = false;
            this.dgv3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgv3.Size = new System.Drawing.Size(340, 199);
            this.dgv3.TabIndex = 2;
            this.dgv3.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv3_CellContentDoubleClick);
            // 
            // FrParemetre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 292);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tcParametre);
            this.Name = "FrParemetre";
            this.Text = "Paramètres";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrParemetre_FormClosing);
            this.Load += new System.EventHandler(this.FrParemetre_Load);
            this.tcParametre.ResumeLayout(false);
            this.tpImpression.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv1)).EndInit();
            this.tpPeriode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcParametre;
        private System.Windows.Forms.TabPage tpImpression;
        private System.Windows.Forms.DataGridView dgv1;
        private System.Windows.Forms.TabPage tpPeriode;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgv3;
        private System.Windows.Forms.DataGridView dgv2;
    }
}