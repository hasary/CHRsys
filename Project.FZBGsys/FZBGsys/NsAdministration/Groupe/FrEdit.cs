﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIL.FieldWorks.Common.Controls;

namespace FZBGsys.NsAdministration.NsGroupe
{
    public partial class FrEdit : Form
    {
        ModelEntities db = new ModelEntities();
        ApplicationGroupe Model;


        public FrEdit(ApplicationGroupe Groupe)
        {
            this.Model = Groupe;
            InitializeComponent();
            InitializeData();
            treeView1.Select();
        }

        private void InitializeData()
        {
            txtNom.Text = this.Model.Nom;

            treeView1.Nodes.Clear();


            var Actions = db.ApplicationActions.OrderBy(u => u.Nom).ToList();
            foreach (var rootNode in Actions.Where(r =>
               r.ParentActionID == null && r.IsPublic != true
                ).OrderBy(a => a.Position))
            {
                treeView1.Nodes.Add(rootNode.ID.ToString(), rootNode.Text).ToolTipText = rootNode.Description;

                foreach (var childNode in Actions.Where(r => r.ParentActionID == rootNode.ID).OrderBy(a => a.Position))
                {
                    treeView1.Nodes[rootNode.ID.ToString()].Nodes.Add(childNode.ID.ToString(), childNode.Text).ToolTipText = childNode.Description;

                }
            }

            var Autorisations = db.ApplicationAutorisations.Where(au => au.GroupeID == this.Model.ID).Select(a => a.ApplicationAction.ID).ToList();
            foreach (TreeNode rootNode in treeView1.Nodes)
            {

                foreach (TreeNode childNode in rootNode.Nodes)
                {
                    if (Autorisations.Contains(int.Parse(childNode.Name)))
                    {
                        treeView1.SetChecked(childNode, TriStateTreeView.CheckState.Checked);
                    }

                }
            }
        }

        private void buttonAnnuler_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        private void AdminGroupeCreate_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void buttonEnregistre_Click(object sender, EventArgs e)
        {

            /* autorisations for the new Groupe
             * seulement les actions qui sont coché (ou GrayCoché) sont insrit dans la table avec autorisation = 1 
                 
             */
            foreach (TreeNode rootNodes in treeView1.Nodes)
            {
                if (IsValidAll())
                {
                    var Groupe = db.ApplicationGroupes.Single(s => s.ID == Model.ID);
                    Groupe.Nom = txtNom.Text;

                }
                else
                {

                    return;

                }
                // notde.Name is ID
                var id = int.Parse(rootNodes.Name);
                bool autorisationInTree = (treeView1.GetChecked(rootNodes) != TriStateTreeView.CheckState.Unchecked); // GrayCheked is cheaked

                var AutorisationInTable = db.ApplicationAutorisations.SingleOrDefault(au => au.ApplicationActionID == id && au.GroupeID == Model.ID);
                // traitement root 
                if (AutorisationInTable == null && autorisationInTree) // if not exist then create
                    db.AddToApplicationAutorisations(new ApplicationAutorisation() { ApplicationActionID = id, GroupeID = Model.ID, Autorisation = true });
                if (AutorisationInTable != null && !autorisationInTree)  // if exist then delete
                    db.DeleteObject(AutorisationInTable);

                // traitement child
                foreach (TreeNode ChildNode in rootNodes.Nodes)
                {
                    id = int.Parse(ChildNode.Name);
                    autorisationInTree = (treeView1.GetChecked(ChildNode) == TriStateTreeView.CheckState.Checked);
                    AutorisationInTable = db.ApplicationAutorisations.SingleOrDefault(au => au.ApplicationActionID == id && au.GroupeID == Model.ID);

                    if (AutorisationInTable == null && autorisationInTree) // if not exist then create
                        db.AddToApplicationAutorisations(new ApplicationAutorisation() { ApplicationActionID = id, GroupeID = Model.ID, Autorisation = true });

                    if (AutorisationInTable != null && !autorisationInTree)
                        db.DeleteObject(AutorisationInTable);
                    else if (AutorisationInTable != null)
                    {
                        AutorisationInTable.Autorisation = true;
                    }
                }


            }

            db.SaveChanges();
            Dispose();

        }

        private bool IsValidAll()
        {
            string ErrorMessage = String.Empty;
            if (txtNom.Text.Length < 2 || txtNom.Text.Length > 30)
            {
                ErrorMessage += "Ce Nom est Invalide!(doit faire entre 2 et 30 Caractères).";

            }

            var Exist = db.ApplicationGroupes.SingleOrDefault(gr => gr.Nom == txtNom.Text && gr.ID != Model.ID);

            if (Exist != null)
            {

                ErrorMessage += "\nCe Groupe Existe déja, choisir un autre Nom.";
            }

            if (ErrorMessage != String.Empty)
            {
                Tools.ShowError(ErrorMessage);
                return false;
            }

            return true;

        }



    }
}
