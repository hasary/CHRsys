﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsAdministration
{
    public partial class FrParemetre : Form
    {
        ModelEntities db = new ModelEntities();
        public FrParemetre()
        {
            InitializeComponent();
            InitialiseData();
        }
        private void InitialiseData()
        {
            
           if (tcParametre.SelectedTab == tpImpression) LoadImpression();
           if (tcParametre.SelectedTab == tpPeriode) LoadPeriode();
             
                  
        }

        private void LoadImpression()
        {
            dgv1.DataSource = db.ParametreAutorisations.Where(p => p.ParametreID == 1).Select(p => new
            {
                Utilisateur = p.ApplicationUtilisateur.UserName,
                Nom = p.ApplicationUtilisateur.Employer.Nom,
                Autorisation = (p.Autorisation) ? "Activé" : "Désactivé"
            });
            DataGridViewCellStyle Style = new DataGridViewCellStyle();
            Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgv1.DefaultCellStyle = Style;
            DataGridViewCellStyle AutorisationGreen = new DataGridViewCellStyle();
            AutorisationGreen.ForeColor = Color.Green;
            DataGridViewCellStyle AutorisationRed = new DataGridViewCellStyle();
            AutorisationRed.ForeColor = Color.Red;

            for (int i = 0; i < dgv1.RowCount; i++)
            {
                if (dgv1.Rows[i].Cells["Autorisation"].Value.ToString() == "Activé")
                    dgv1.Rows[i].Cells[2].Style = AutorisationGreen;
                else
                    dgv1.Rows[i].Cells[2].Style = AutorisationRed;
            }


        }

        private void LoadPeriode()
        {
            DataGridViewCellStyle Style = new DataGridViewCellStyle();
            Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DataGridViewCellStyle AutorisationGreen = new DataGridViewCellStyle();
            AutorisationGreen.ForeColor = Color.Green;
            DataGridViewCellStyle AutorisationRed = new DataGridViewCellStyle();
            AutorisationRed.ForeColor = Color.Red;

            // Remplir la table de la selection periodique
            dgv2.DataSource = db.ParametreAutorisations.Where(p => p.ParametreID == 2).Select(p => new
            {
                Utilisateur = p.ApplicationUtilisateur.UserName,
                Nom = p.ApplicationUtilisateur.Employer.Nom,
                Autorisation = (p.Autorisation) ? "Activé" : "Désactivé"
            });
           
            dgv2.DefaultCellStyle = Style;
           

            for (int i = 0; i < dgv1.RowCount; i++)
            {
                if (dgv2.Rows[i].Cells["Autorisation"].Value.ToString() == "Activé")
                    dgv2.Rows[i].Cells[2].Style = AutorisationGreen;
                else                    
                    dgv2.Rows[i].Cells[2].Style = AutorisationRed;
            }

            // Rmplir la table de la selection mensual

            dgv3.DataSource = db.ParametreAutorisations.Where(p => p.ParametreID == 4).Select(p => new
            {
                Utilisateur = p.ApplicationUtilisateur.UserName,
                Nom = p.ApplicationUtilisateur.Employer.Nom,
                Autorisation = (p.Autorisation) ? "Activé" : "Désactivé"
            });

            dgv3.DefaultCellStyle = Style;


            for (int i = 0; i < dgv1.RowCount; i++)
            {
                if (dgv3.Rows[i].Cells["Autorisation"].Value.ToString() == "Activé")
                    dgv3.Rows[i].Cells[2].Style = AutorisationGreen;
                else
                    dgv3.Rows[i].Cells[2].Style = AutorisationRed;
            }
        }
                
        private void dgv1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var username = dgv1.Rows[e.RowIndex].Cells[0].Value.ToString();
            var ID = db.ApplicationUtilisateurs.Single(p => p.UserName == username).ID;
            if (dgv1.Columns[e.ColumnIndex].Name.ToString() == "Autorisation")
            {
                var Autorise = db.ParametreAutorisations.Single(p => p.UtilisateurID == ID && p.ParametreID == 1);
                Autorise.Autorisation = !Autorise.Autorisation;
                db.SaveChanges();
                InitialiseData();
                dgv1.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected = true;
            }

        }

        private void FrParemetre_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.Dispose();
            Dispose();
        }

        private void tcParametre_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitialiseData();
        }

        private void dgv2_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var username = dgv2.Rows[e.RowIndex].Cells[0].Value.ToString();
            var ID = db.ApplicationUtilisateurs.Single(p => p.UserName == username).ID;
            if (dgv2.Columns[e.ColumnIndex].Name.ToString() == "Autorisation")
            {
                var Autorise = db.ParametreAutorisations.Single(p => p.UtilisateurID == ID && p.ParametreID == 2);
                Autorise.Autorisation = !Autorise.Autorisation;
                db.SaveChanges();
                InitialiseData();
                dgv2.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected = true;
            }
        }

        private void dgv3_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var username = dgv3.Rows[e.RowIndex].Cells[0].Value.ToString();
            var ID = db.ApplicationUtilisateurs.Single(p => p.UserName == username).ID;
            if (dgv3.Columns[e.ColumnIndex].Name.ToString() == "Autorisation")
            {
                var Autorise = db.ParametreAutorisations.Single(p => p.UtilisateurID == ID && p.ParametreID == 4);
                Autorise.Autorisation = !Autorise.Autorisation;
                db.SaveChanges();
                InitialiseData();
                dgv3.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected = true;
            }
        }

        private void FrParemetre_Load(object sender, EventArgs e)
        {
            InitialiseData();
        }
    }
}
