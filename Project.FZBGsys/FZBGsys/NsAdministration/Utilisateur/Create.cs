﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FZBGsys.NsAdministration.NsUtilisateur
{
    public partial class FrCreate : Form
    {
        ModelEntities db = new ModelEntities();
        public FrCreate()
        {
            InitializeComponent();
            InitialiseData();
        }
        private void InitialiseData()
        {
          
           
            cbGroupe.Items.Clear();
            cbGroupe.Items.Add(new ApplicationGroupe() { ID = 0, Nom = "[Choisir un Groupe]" });
            cbGroupe.Items.AddRange(db.ApplicationGroupes.Where(u => u.ID > 0).ToArray());
            cbGroupe.SelectedIndex = 0;
            cbGroupe.DropDownStyle = ComboBoxStyle.DropDownList;
            cbGroupe.ValueMember = "ID";
            cbGroupe.DisplayMember = "Nom";

        }
        private void buttonEnregistrer_Click(object sender, EventArgs e)
        {
            if (IsValidateInputs())
            {

                  db.AddToApplicationUtilisateurs(new ApplicationUtilisateur(){

                      UserName = txtUserName.Text.Trim(),
                      EmployerMatricule = txtUserName.Text.Trim(),
                      GroupeID = ((ApplicationGroupe)cbGroupe.SelectedItem).ID,
                      Password = NsApplication.FrLoginForm.Encrypt(txtPass.Text),
                      Enabled = chActive.Checked,
                      WindowsUserName = this.WindowsUserName
                });
                  db.SaveChanges();
                  this.ShowInformation("Utilisateur inscrit et peut se connecter au programme \n avec son matricule et mot de passe");
                  Dispose();
            }


        }
        private bool IsValidateInputs()
        {
            string ErrorMessage = "";

            #region validations

           // int matricule;

            //if (txtUserName.Text.Length > 40 || txtUserName.Text.Length < 2 || !int.TryParse(txtUserName.Text, out matricule))
            if (txtNom.Text.Contains("Introuvable"))
            {
                ErrorMessage += "\nLe matricule est introuvable ! (aucun Employer trouvé)";
                
            }
            else
            {

              //  matricule = int.Parse(txtUserName.Text);
              //  var exist = db.ApplicationUtilisateurs.SingleOrDefault(u => u.EmployerMatricule == matricule);
              //  if (exist != null)
                if(txtNom.Text.Contains("déja"))
                {
                    ErrorMessage += "\nCet Utilisateur(Matricule) '" + txtUserName.Text + "' Existe déja";
                   
                }

            }
            if (txtPass.Text.Length < 4 || txtPass.Text.Length > 20)
                ErrorMessage += "\nLe Nom mot de passe doit faire entre 4 et 20 caractères ";

            if (txtPass.Text != txtPassRe.Text)
                ErrorMessage += "\nLes mots de passe ne correspondent pas ";

            if (cbGroupe.SelectedIndex == 0)
                ErrorMessage += "\nAucun Groupe n'est selectionné ";
/*
            if (txtNom.Text.Length > 40 || txtNom.Text.Length < 1)
                ErrorMessage += "\nLe Nom / Prénom est Invalide ";

*/
            #endregion


            if (ErrorMessage != "")
            {

                Tools.ShowError(ErrorMessage);

           //     MessageBox.Show(ErrorMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }
        private void AdminUserNew_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void ButtonAnnuler_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        private void buttonParcourirMatricule_Click(object sender, EventArgs e)
        {
            var NewEmployer = FZBGsys.NsRessources.NsEmployer.FrList.SelectEmployerNotUtilisateurDialog(); // ne pas ajout "aucun" a la liste

            if (NewEmployer != null)
            {

                this.txtUserName.Text = NewEmployer.Matricule.ToString();
               txtUserName_Leave( sender,  e);


            }
        }
        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text == String.Empty) return;
           var matricule =txtUserName.Text;
            var Emp = db.Employers.FirstOrDefault(em => em.Matricule == matricule);
            var Uti = db.ApplicationUtilisateurs.FirstOrDefault(em => em.EmployerMatricule == matricule);
            if (Emp == null)
            {
                txtNom.Text = "(Matricule Introuvable)";
                return;
            }
            else if (Uti != null)
            {
                txtNom.Text = "(Utilisateur déja Inscrit)";
                return;
            }

            txtNom.Text = Emp.Nom;
        }

        private void buttonThis_Click(object sender, EventArgs e)
        {
            txtWinUser.Text = Environment.UserName;
            this.WindowsUserName = Environment.UserDomainName + '_' + Environment.UserName;
        }

        public string WindowsUserName { get; set; }
    }
}
