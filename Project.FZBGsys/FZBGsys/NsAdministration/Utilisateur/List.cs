﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Objects;

namespace FZBGsys.NsAdministration.NsUtilisateur
{
    public partial class FrList : Form
    {
        ModelEntities db = new ModelEntities();
        public FrList()
        {
            InitializeComponent();

            InitialiseData();
        }
        private void InitialiseData()
        {
            dgv1.DataSource = db.ApplicationUtilisateurs.Where(u => u.ID > 0).Select(u => new { Utilisateur = u.UserName, Nom = u.Employer.Nom, Groupe = u.ApplicationGroupe.Nom, Compte = (u.Enabled ) ? "Activé" : "Désactivé" });

          



        }
        private void AdminUsers_FormClosing(object sender, FormClosingEventArgs e)
        {
            db.Dispose();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            new FrCreate().ShowDialog();
            InitialiseData();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            var username = dgv1.SelectedRows[0].Cells[0].Value.ToString();
            
            var selected = db.ApplicationUtilisateurs.Single(u => u.UserName == username);
            db.Refresh(RefreshMode.StoreWins,selected);
            new FrEdit(selected).ShowDialog();
            InitialiseData();
        }
        private void button3_Click(object sender, EventArgs e)
        {
             var username = dgv1.SelectedRows[0].Cells[0].Value.ToString();
            if (this.ConfirmWarning("Supprimer L'utilisateur " + username + " ?")) { 
            
                var userDeleted = db.ApplicationUtilisateurs.Single(u => u.UserName == username);
                db.ApplicationUtilisateurs.DeleteObject(userDeleted);
                db.SaveChanges();
                InitialiseData();
            
            }
        }

      



    }
}
