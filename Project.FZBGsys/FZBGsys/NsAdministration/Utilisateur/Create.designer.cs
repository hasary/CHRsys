﻿
namespace FZBGsys.NsAdministration.NsUtilisateur
{
    partial class FrCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUserName = new FZBGsys.TextBoxx();
            this.labelGroupe = new System.Windows.Forms.Label();
            this.cbGroupe = new System.Windows.Forms.ComboBox();
            this.ButtonAnnuler = new System.Windows.Forms.Button();
            this.buttonEnregistrer = new System.Windows.Forms.Button();
            this.txtNom = new FZBGsys.TextBoxx();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonNewMatricule = new System.Windows.Forms.Button();
            this.txtPass = new FZBGsys.TextBoxx();
            this.txtPassRe = new FZBGsys.TextBoxx();
            this.chActive = new System.Windows.Forms.CheckBox();
            this.txtWinUser = new FZBGsys.TextBoxx();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonThis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mot de passe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Re-Mot de passe";
            // 
            // txtUserName
            // 
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Location = new System.Drawing.Point(141, 25);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(157, 22);
            this.txtUserName.TabIndex = 1;
            this.txtUserName.Leave += new System.EventHandler(this.txtUserName_Leave);
            // 
            // labelGroupe
            // 
            this.labelGroupe.AutoSize = true;
            this.labelGroupe.Location = new System.Drawing.Point(15, 204);
            this.labelGroupe.Name = "labelGroupe";
            this.labelGroupe.Size = new System.Drawing.Size(90, 17);
            this.labelGroupe.TabIndex = 5;
            this.labelGroupe.Text = "Autorisations";
            // 
            // cbGroupe
            // 
            this.cbGroupe.DisplayMember = "Text";
            this.cbGroupe.FormattingEnabled = true;
            this.cbGroupe.Location = new System.Drawing.Point(141, 201);
            this.cbGroupe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbGroupe.Name = "cbGroupe";
            this.cbGroupe.Size = new System.Drawing.Size(216, 24);
            this.cbGroupe.TabIndex = 5;
            this.cbGroupe.ValueMember = "Value";
            // 
            // ButtonAnnuler
            // 
            this.ButtonAnnuler.Location = new System.Drawing.Point(132, 287);
            this.ButtonAnnuler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonAnnuler.Name = "ButtonAnnuler";
            this.ButtonAnnuler.Size = new System.Drawing.Size(103, 32);
            this.ButtonAnnuler.TabIndex = 7;
            this.ButtonAnnuler.Text = "Annuler";
            this.ButtonAnnuler.UseVisualStyleBackColor = true;
            this.ButtonAnnuler.Click += new System.EventHandler(this.ButtonAnnuler_Click);
            // 
            // buttonEnregistrer
            // 
            this.buttonEnregistrer.Location = new System.Drawing.Point(243, 287);
            this.buttonEnregistrer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonEnregistrer.Name = "buttonEnregistrer";
            this.buttonEnregistrer.Size = new System.Drawing.Size(116, 32);
            this.buttonEnregistrer.TabIndex = 6;
            this.buttonEnregistrer.Text = "Enregistrer";
            this.buttonEnregistrer.UseVisualStyleBackColor = true;
            this.buttonEnregistrer.Click += new System.EventHandler(this.buttonEnregistrer_Click);
            // 
            // txtNom
            // 
            this.txtNom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(141, 94);
            this.txtNom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = true;
            this.txtNom.Size = new System.Drawing.Size(215, 22);
            this.txtNom.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nom / Prenom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Matricule";
            // 
            // buttonNewMatricule
            // 
            this.buttonNewMatricule.Location = new System.Drawing.Point(304, 25);
            this.buttonNewMatricule.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonNewMatricule.Name = "buttonNewMatricule";
            this.buttonNewMatricule.Size = new System.Drawing.Size(53, 25);
            this.buttonNewMatricule.TabIndex = 2;
            this.buttonNewMatricule.Text = "...";
            this.buttonNewMatricule.UseVisualStyleBackColor = true;
            this.buttonNewMatricule.Click += new System.EventHandler(this.buttonParcourirMatricule_Click);
            // 
            // txtPass
            // 
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPass.Location = new System.Drawing.Point(141, 124);
            this.txtPass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '#';
            this.txtPass.Size = new System.Drawing.Size(215, 22);
            this.txtPass.TabIndex = 3;
            this.txtPass.UseSystemPasswordChar = true;
            // 
            // txtPassRe
            // 
            this.txtPassRe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassRe.Location = new System.Drawing.Point(141, 154);
            this.txtPassRe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtPassRe.Name = "txtPassRe";
            this.txtPassRe.Size = new System.Drawing.Size(215, 22);
            this.txtPassRe.TabIndex = 4;
            this.txtPassRe.UseSystemPasswordChar = true;
            // 
            // chActive
            // 
            this.chActive.AutoSize = true;
            this.chActive.Checked = true;
            this.chActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chActive.Location = new System.Drawing.Point(19, 257);
            this.chActive.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chActive.Name = "chActive";
            this.chActive.Size = new System.Drawing.Size(161, 21);
            this.chActive.TabIndex = 12;
            this.chActive.Text = "Le compte est Activé";
            this.chActive.UseVisualStyleBackColor = true;
            // 
            // txtWinUser
            // 
            this.txtWinUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWinUser.Location = new System.Drawing.Point(141, 57);
            this.txtWinUser.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtWinUser.Name = "txtWinUser";
            this.txtWinUser.Size = new System.Drawing.Size(157, 22);
            this.txtWinUser.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Utilisateur Session:";
            // 
            // buttonThis
            // 
            this.buttonThis.Location = new System.Drawing.Point(304, 55);
            this.buttonThis.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonThis.Name = "buttonThis";
            this.buttonThis.Size = new System.Drawing.Size(53, 23);
            this.buttonThis.TabIndex = 15;
            this.buttonThis.Text = "this";
            this.buttonThis.UseVisualStyleBackColor = true;
            this.buttonThis.Click += new System.EventHandler(this.buttonThis_Click);
            // 
            // FrCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 332);
            this.Controls.Add(this.buttonThis);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWinUser);
            this.Controls.Add(this.chActive);
            this.Controls.Add(this.txtPassRe);
            this.Controls.Add(this.txtPass);
            this.Controls.Add(this.buttonNewMatricule);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.buttonEnregistrer);
            this.Controls.Add(this.ButtonAnnuler);
            this.Controls.Add(this.cbGroupe);
            this.Controls.Add(this.labelGroupe);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrCreate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nouvel Utilisateur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AdminUserNew_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelGroupe;
        private System.Windows.Forms.ComboBox cbGroupe;
        private System.Windows.Forms.Button ButtonAnnuler;
        private System.Windows.Forms.Button buttonEnregistrer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonNewMatricule;
        private System.Windows.Forms.CheckBox chActive;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonThis;
        private TextBoxx txtUserName;
        private TextBoxx txtNom;
        private TextBoxx txtPass;
        private TextBoxx txtPassRe;
        private TextBoxx txtWinUser;
       // private Noogen.Validation.ValidationProvider validationProvider1;
    }
}